<?php
ob_start();
include('index.php');
ob_end_clean();
class Cronjobcalling{
	function __construct(){
		$this->CI =& get_instance();
		$this->CI->load->model('Cronjobs');
	}

	/*
	* 3 Months not active delayed mail
	*/
	public function notActive3Months(){
    	$purchaseDate = date("Y-m-d", strtotime("-3 Month"));
    	$filters = array("conditions"=>array("opt_option_status"=>0, "pyt_txn_status !="=>"Pending", "opt_option_type !="=>5, "opt_option_purchase_date <="=>$purchaseDate, "opt_notactivate_3month"=>0));
    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);
    	if(!empty($details)){
    		//Fetch Email Templates
		        $templateheader = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
		        $templatecenter = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>30));
		        $templatefooter = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
		    //Fetch Email Templates End 
	        $subject = $templatecenter['tmplate_subject'];
    		foreach($details as $user){		    	
		        $dummy = array("%%Firstname%%",
		                       "%%LastName%%", 
		                       "%%Email%%", 
		                       "%%BusinessName%%", 
		                       "%%CompanyNumber%%", 
		                       "%%Address%%", 
		                       "%%Phone%%", 
		                       "%%Gender%%", 
		                       "%%OptionName%%", 
		                       "%%OptionsDescription%%", 
		                       "%%PurchaseDate%%", 
		                       "%%OptionDuration%%", 
		                       "%%OptionPrice%%", 
		                       "%%OptionsVisibleCity%%", 
		                       "%%OptionActiveStartDate%%",
		                       "%%OptionActiveEndDate%%"
		                   	);
		        $real = array($user['user_firstname'], 
		                      $user['user_lastname'],
		                      $user["user_email"],
		                      $user["user_company_name"],
		                      $user['user_company_number'],
		                      $user["user_company_address"],
		                      $user['user_phone'],
		                      $user['user_gender'],
		                      $user['opt_name'],
		                      $user['opt_description'],
		                      $user['opt_option_purchase_date'],
		                      $user['otp_option_duration'],
		                      $user['opt_option_price'],
		                      $user['otp_search_city'],
		                      $user['opt_option_active_date'],
		                      $user['opt_option_end_date']
		                    );
		        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
		        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
		        $x= $this->CI->Cronjobs->send_mail($user["user_email"], $subject, $msg);        
		        $y= $this->CI->Cronjobs->send_mail(ADMIN_EMAIL, $subject, $msg);
		        if($x && $y){
		        	$this->CI->Cronjobs->updateUserOption(array("opt_notactivate_3month"=>1), array("opt_user_option_id"=>$user['opt_user_option_id']));
		        }
		    }		    
	    }
	}

	/*
	* 5 Months and 28 Days not activa delayed mail
	*/
	public function notActive6Months(){
		$purchaseDate = date("Y-m-d", strtotime("-5 Month -28 Day"));
    	$filters = array("conditions"=>array("opt_option_status"=>0, "pyt_txn_status !="=>"Pending", "opt_option_type !="=>5, "opt_option_purchase_date <="=>$purchaseDate, "opt_notactivate_6month"=>0));
    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);
    	if(!empty($details)){
    		//Fetch Email Templates
		        $templateheader = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
		        $templatecenter = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>31));
		        $templatefooter = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
		    //Fetch Email Templates End 
	        $subject = $templatecenter['tmplate_subject'];
    		foreach($details as $user){		    	
		        $dummy = array("%%Firstname%%",
		                       "%%LastName%%", 
		                       "%%Email%%", 
		                       "%%BusinessName%%", 
		                       "%%CompanyNumber%%", 
		                       "%%Address%%", 
		                       "%%Phone%%", 
		                       "%%Gender%%", 
		                       "%%OptionName%%", 
		                       "%%OptionsDescription%%", 
		                       "%%PurchaseDate%%", 
		                       "%%OptionDuration%%", 
		                       "%%OptionPrice%%", 
		                       "%%OptionsVisibleCity%%", 
		                       "%%OptionActiveStartDate%%",
		                       "%%OptionActiveEndDate%%"
		                   	);
		        $real = array($user['user_firstname'], 
		                      $user['user_lastname'],
		                      $user["user_email"],
		                      $user["user_company_name"],
		                      $user['user_company_number'],
		                      $user["user_company_address"],
		                      $user['user_phone'],
		                      $user['user_gender'],
		                      $user['opt_name'],
		                      $user['opt_description'],
		                      $user['opt_option_purchase_date'],
		                      $user['otp_option_duration'],
		                      $user['opt_option_price'],
		                      $user['otp_search_city'],
		                      $user['opt_option_active_date'],
		                      $user['opt_option_end_date']
		                    );
		        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
		        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
		        $x= $this->CI->Cronjobs->send_mail($user["user_email"], $subject, $msg);
		        $y= $this->CI->Cronjobs->send_mail(ADMIN_EMAIL, $subject, $msg);
		        if($x && $y){
		        	$this->CI->Cronjobs->updateUserOption(array("opt_notactivate_6month"=>1), array("opt_user_option_id"=>$user['opt_user_option_id']));
		        }
		    }
	    }
	}

	/*
	* Deactivate user option which is not activated last 6 Months
	*/
	public function deactivateNotActiveUserOption(){
		$purchaseDate = date("Y-m-d", strtotime("-2 Month"));
    	$filters = array("conditions"=>array("opt_option_status"=>0, "opt_option_type !="=>5, "opt_option_purchase_date <"=>$purchaseDate));
    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);
    	if(!empty($details)){
    		foreach($details as $user){		    	
		       	$this->CI->Cronjobs->updateUserOption(array("opt_option_status"=>2), array("opt_user_option_id"=>$user['opt_user_option_id']));
		    }
	    }
	}

	/*
	* 7 Days before alert to renew option
	*/
	public function optionRenewReminder(){
		$purchaseDate = date("Y-m-d", strtotime("+1 Week"));		
    	$filters = array("conditions"=>array("opt_option_status"=>1, "opt_option_type !="=>5, "opt_option_end_date"=>$purchaseDate, "opt_7daysremaining"=>0), "wherein"=>array("otp_option_duration"=>array("6 Months", "1 Month")));
    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);    	
    	if(!empty($details)){
    		//Fetch Email Templates
		        $templateheader = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
		        $templatecenter = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>32));
		        $templatefooter = $this->CI->Cronjobs->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
		    //Fetch Email Templates End 
	        $subject = $templatecenter['tmplate_subject'];
    		foreach($details as $user){
		        $dummy = array("%%Firstname%%",
		                       "%%LastName%%", 
		                       "%%Email%%", 
		                       "%%BusinessName%%", 
		                       "%%CompanyNumber%%", 
		                       "%%Address%%", 
		                       "%%Phone%%", 
		                       "%%Gender%%", 
		                       "%%OptionName%%", 
		                       "%%OptionsDescription%%", 
		                       "%%PurchaseDate%%", 
		                       "%%OptionDuration%%", 
		                       "%%OptionPrice%%", 
		                       "%%OptionsVisibleCity%%", 
		                       "%%OptionActiveStartDate%%",
		                       "%%OptionActiveEndDate%%"
		                   	);
		        $real = array($user['user_firstname'], 
		                      $user['user_lastname'],
		                      $user["user_email"],
		                      $user["user_company_name"],
		                      $user['user_company_number'],
		                      $user["user_company_address"],
		                      $user['user_phone'],
		                      $user['user_gender'],
		                      $user['opt_name'],
		                      $user['opt_description'],
		                      $user['opt_option_purchase_date'],
		                      $user['otp_option_duration'],
		                      $user['opt_option_price'],
		                      $user['otp_search_city'],
		                      $user['opt_option_active_date'],
		                      $user['opt_option_end_date']
		                    );
		        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
		        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
		        $x= $this->CI->Cronjobs->send_mail($user["user_email"], $subject, $msg);
		        $y= $this->CI->Cronjobs->send_mail(ADMIN_EMAIL, $subject, $msg);
		        if($x && $y){
		        	$this->CI->Cronjobs->updateUserOption(array("opt_7daysremaining"=>1), array("opt_user_option_id"=>$user['opt_user_option_id']));
		        }
		    }
	    }
	}

	/*
	* Check option which has expiry date today and deactivate it
	*/
	public function checkExpiryOptions(){
		$purchaseDate = date("Y-m-d");
    	$filters = array("conditions"=>array("opt_option_status"=>1, "opt_option_type !="=>5, "opt_option_end_date"=>$purchaseDate));
    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);    	
    	if(!empty($details)){
    		foreach($details as $user){
    			if($user['opt_option_type'] == 4){
    				$this->CI->Cronjobs->updateUserOption(array("opt_option_status"=>3), array("opt_pkg_id"=>$user['opt_user_option_id']));
    			}
		        $this->CI->Cronjobs->updateUserOption(array("opt_option_status"=>3), array("opt_user_option_id"=>$user['opt_user_option_id']));		        
		    }
	    }
	}

	/*
	* Check package's option which has expiry date today.
	*/
	public function activeDeactivePackageOptions(){
		$purchaseDate = date("Y-m-d");

		//Activate Package Options
		    $filters = array("conditions"=>array("opt_option_status"=>0, "opt_option_type"=>5, "opt_option_active_date <="=>$purchaseDate));    	
	    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);
	    	if(!empty($details)){
	    		foreach($details as $user){
	    			$arr = array("opt_option_status"=>1);
			        $this->CI->Cronjobs->updateUserOption($arr, array("opt_user_option_id"=>$user['opt_user_option_id']));		        
			    }
		    }
		//Activate Package Options end

		//Deactivate Package Options
	    	$filters = array("conditions"=>array("opt_option_status"=>1, "opt_option_type"=>5, "opt_option_end_date"=>$purchaseDate));    	
	    	$details = $this->CI->Cronjobs->getUserOptionRows($filters);    	    	
	    	if(!empty($details)){
	    		foreach($details as $user){
	    			$duration = $user['otp_option_duration'];
	    			$nextdate = date("Y-m-d", strtotime($user['opt_option_active_date']."+1 Month"));
	                $exp_date = date("Y-m-d", strtotime($nextdate." + $duration"));                
	                $arr = array("opt_option_active_date"=>$nextdate,
	                             "opt_option_end_date"=>$exp_date,
	                             "opt_option_status"=>0
	                            );
			        $this->CI->Cronjobs->updateUserOption($arr, array("opt_user_option_id"=>$user['opt_user_option_id']));		        
			    }
		    }
		//Deactivate Package Options end		
	}
}

$clsobj = new Cronjobcalling();
$action = $_REQUEST['action'];
//Functions calling
	if(!empty($action)){
		if($action == "deactivateUserOption"){ // 6 Months completed not activated yet
			$clsobj->deactivateNotActiveUserOption();
		}else if($action == "3MonthsReminder"){		
			$clsobj->notActive3Months();
		}else if($action == "6MonthsReminder"){		
			$clsobj->notActive6Months();
		}else if($action == "optionRenewReminder"){
			$clsobj->optionRenewReminder();
		}else if($action == "deactivateTodayExpireOptions"){
			$clsobj->checkExpiryOptions();
		}else if($action == "checkPackagesOptionsActivation"){
			$clsobj->activeDeactivePackageOptions();
		}else{
			$clsobj->notActive3Months();
		}
	}
//Function Calling
?>