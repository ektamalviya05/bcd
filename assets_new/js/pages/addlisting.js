$(document).ready(function () {

    $.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than {0}');

jQuery(function ($) {
    "use strict";
    $("#editlisting").validate({
        rules: {
            title: {
                required: true
            },
            email: {
                required: true,
                email: true  
            },
            mobile:{
                required: true,
                number: true,
                maxlength:8,
                
            },
            price:{
                required: true,
                number: true                
            },
            // location:{
            //     required: true
                
            // },
            category:{
                required: true
            },

            userfile:{
    
                 extension: "jpg,jpeg,png",
                filesize: 600000,
            },

             userfile2:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },

             userfile3:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
             userfile4:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile5:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile6:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile7:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile8:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile9:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
             userfile10:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            
            description:{
                required: true
            },
            
        },
        messages: {
            title: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                email: "Please Enter Valid Email Id" // <-- removed underscore
            },
            mobile:{
                required: "This field is required",
                number: "This field accepts only numbers",
                maxlength: "Please insert 8 digit phone number",
                
            },
            price:{
                required: "This field is required",
                number: "This field accepts only numbers"
                
            },
            // location:{
            //     required: "This field is required"
            // },
            category:{
                required: "This field is required"
            },

            userfile:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile2:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile3:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
             userfile4:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile5:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
            userfile6:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile7:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile8:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
             userfile9:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
            userfile10:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
           
            description:{
                required: "This field is required"
            },
            
        },
        submitHandler: function (form) { // for demo
            //alert('valid form');  // for demo
            return true;  // for demo
        }
    });
});

    $("#addservices").validate({
                rules: {
                    business_id: {
                        required: true,
                        //extension: "jpg|jpeg|png",
                        //filesize: 20971520, 
                    },
                    // "userfile[]": {
                    //     required: true,
                    //     //extension: "png",
                    //     //filesize: 20971520, 
                    // },

                    "userfile[]": 
                    { required: true, }

                    
                    
                },

                submitHandler: function (form) { // for demo
            //alert('valid form');  // for demo
            return true;  // for demo
        }
                
    });

    $.validator.addMethod("greaterThan", 
        function(value, element, params) {

    if (!/Invalid|NaN/.test(new Date(value))) {
        return new Date(value) > new Date($(params).val());
    }

    return isNaN(value) && isNaN($(params).val()) 
        || (parseFloat(value) > parseFloat($(params).val())); 
},'Must be greater than {0}.');

    $("#addoffer").validate({
                rules: {
                    business_id: {
                        required: true,
                        //extension: "jpg|jpeg|png",
                        //filesize: 20971520, 
                    },

                     start_date: {
                        required: true,
                    },

                    end_date: {
                        required: true,
                        greaterThan: "#start_date"
                    },
                    // "userfile[]": {
                    //     required: true,
                    //     //extension: "png",
                    //     //filesize: 20971520, 
                    // },

                    "userfile[]": 
                    { required: true, }

                    
                    
                },

                submitHandler: function (form) { // for demo
            //alert('valid form');  // for demo
            return true;  // for demo
        }
                
    });
	
jQuery(function ($) {
    "use strict";
    $("#addlisting").validate({
        rules: {
            title: {
                required: true
            },
            email: {
                required: true,
                email: true  
            },
            mobile:{
            	required: true,
            	number: true,
            	maxlength:8
            },
            price:{
            	required: true,
            	number: true            	
            },
            // location:{
            // 	required: true
            	
            // },
            category:{
            	required: true
            },
            userfile:{
            	required: true,
                 extension: "jpg,jpeg,png",
                filesize: 600000,
            },

            userfile2:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },

             userfile3:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
             userfile4:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile5:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile6:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile7:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile8:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
            userfile9:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },
             userfile10:{
                extension: "jpg,jpeg,png",
                filesize: 600000,
            },

            description:{
                required: true
            },
            
        },
        messages: {
            title: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                email: "Please Enter Valid Email Id" // <-- removed underscore
            },
            mobile:{
            	required: "This field is required",
            	number: "This field accepts only numbers",
            	
            	maxlength:"Please insert 8 digit phone number"
            },
            price:{
            	required: "This field is required",
            	number: "This field accepts only numbers"
            	
            },
            // location:{
            // 	required: "This field is required"
            // },
            category:{
            	required: "This field is required"
            },
            userfile:{
               	required: "This field is required",
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile2:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile3:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
             userfile4:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile5:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
            userfile6:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile7:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            userfile8:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
             userfile9:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },
            userfile10:{
                extension: "Please upload .jpg or .png  of notice.",
                filesize: "file size must be less than 500 KB.",
            },

            description:{
                required: "This field is required"
            },
            
        },
        submitHandler: function (form) { // for demo
            //alert('valid form');  // for demo
            return true;  // for demo
        }
    });
});
	
	
	
    var max_fieldsn      = 10; //maximum input boxes allowed
    var imagefieldn      = $(".fileupload"); //Fields wrapper
        
    var x = 1; //initlal text box count
    $(".image").click(function(e){ //on add input button click
        e.preventDefault();
        //alert("test");
        if(x < max_fieldsn){ //max input box allowed
            x++; //text box increment
            $(imagefieldn).append('<div class="imgupload"><img id="output'+x+'" class="imgclas" ><label><input type="file" name="userfile'+x+'" class="multiple_input" multiple="" onchange="loadFile'+x+'(event)"></label></div>'); //add input box
        }else{
			$(".image").hide();
		}
    });


    var max_fields      = 100; //maximum input boxes allowed
    var imagefield      = $(".addfileds"); //Fields wrapper
        
    var x = 1; //initlal text box count
    $(".imageservices").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            
            $(imagefield).append('<label><input type="text" name="userfile[]"></label>'); //add input box
             //text box increment
            
           x++;
        }else{
            $(".imageservices").hide();
        }
    });


    var max_fields1      = 100; //maximum input boxes allowed
    var imagefield1      = $(".upload-sec1"); //Fields wrapper
        
    var x = 1; //initlal text box count
    $(".imageoffer").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields1){ //max input box allowed
            
            $(imagefield1).append('<label><div class="img-prw-boxtwo"><img id="output'+x+'" class="imgclas" ></div><p class="aa hidden">"output'+x+'"</p><input type="file" name="userfile[]" class="multiple_input previw" multiple="" onchange="loadFil(output'+x+')" data-id="output'+x+'"></label>'); //add input box
             //text box increment
            
           x++;
        }else{
            $(".imageoffer").hide();
        }
    });
	
	
	
	var max_fields_service      = 4;
	var servicefield    = $("#service"); //Fields wrapper
    
    var i = 1; //initlal text box count
    $(".service").click(function(e){ //on add input button click
        e.preventDefault();
        if(i < max_fields_service){ //max input box allowed
            i++; //text box increment
            $(servicefield).append('<label><input type="text" name="service'+i+'" ></label>'); //add input box
        }else{
			$(".service").hide();
		}
    });
    
});