<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <base href="<?php echo base_url();?>"></base>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ilbait</title>
<link rel="icon" href="assets_new/images/favicon.ico" type="image/ico" sizes="16x16">
<link rel="stylesheet" type="text/css" href="assets_new/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/master.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/animate.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets_new/css/component.css" />
<script src="assets_new/js/modernizr.custom.js"></script>
</head>
<body class="home_page">
<article class="slider-arti">
  <header id="main_header_id">
    <nav class="navbar navbar-default  navbar-static-top menu-header wow fadeInDown animated" id="nav-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="home"><img src="assets_new/images/logo.png"></a> </div>
        <div class="collapse navbar-collapse text-right" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav main-nav">
            <li><a href="home" class="active">Home</a></li>
            <li><a href="javascript:;">Explore</a></li>
            <li><a href="javascript:;">Listings</a></li>
          </ul>
          <div class="box_headeRight">
            <div class="box_logSign">
              <ul>
                   <?php if(!$this->session->userdata('isUserLoggedIn')){?> 
                <li class="login"><a class="log-op" href="javascript:void(0)"><img src="assets_new/images/icon_login.png"> Login <b class="caret"></b></a>
                    <div class="login-drop" style="display: none;">
                        <form id="loginform" name="loginform" action="do_login" method="post">
                        <div class="f-login">
                            <input type="eamil" name="email" id="email" placeholder="Email"><?php echo form_error('email', '<span class="error">', '</span>'); ?>
                        </div>
                        <div class="f-login">
                            <input type="Password" name="password" id="password" placeholder="Password"><?php echo form_error('password', '<span class="error">', '</span>'); ?>
                        </div>
                        <div class="submit-login">
                              <input type="submit" name="" value="Login">
                        </div>
                        <div class="f-login">
                          <ul class="social">
                            <li>
                              <a href="javascript:;"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                              <a href="javascript:;"><i class="fa fa-facebook"></i></a>
                            </li>
                          </ul>
                          <ul>
                            <li><a href="signup">Sign Up</a></li>
                            <li><a href="forgetpassword">Forgot password</a></li>
                          </ul>
                        </div>
                      </form>
                    </div>                  
                </li>
                <li><a href="signup"><img src="assets_new/images/icon_signUp.png"> Sign up</a></li>
                  <?php } ?>
                <li class="dropdown "><a href="javascript:;" id="drop1" data-toggle="dropdown" class="dropdown-toggle" role="button"><img src="assets_new/images/icon_map.png">  Language <b class="caret"></b></a>
                  <ul role="menu" class="dropdown-menu" aria-labelledby="drop1">
                    <li role="presentation"><a href="javascript:;" role="menuitem">English</a></li>
                    <li role="presentation"><a href="javascript:;" role="menuitem">Arabic</a></li>
                  </ul>
                </li>
              </ul>
            </div>
              <?php if($this->session->userdata('isUserLoggedIn')){?> 
              <div class="add_listing"> <a href="dashboard" class="btn_addListing"> My Profile</a> </div> 
              <?php } ?>
              <?php if($this->session->userdata('isUserLoggedIn')){?>
              <div class="add_listing"> <a href="<?php echo base_url();?>addlisting " class="btn_addListing"> <i class="fa fa-plus" aria-hidden="true"></i> Add Listings</a> </div> 
              <?php }else if(!$this->session->userdata('isUserLoggedIn')){ ?>
                <div class="add_listing"> <a href="javascript:void(0);" class="btn_addListing "> <i class="fa fa-plus" aria-hidden="true"></i> Add Listings</a> </div> 
              <?php }else{} ?>
          </div>
        </div>
      </div>
    </nav>
  </header>
</article>
 <?php echo $contents;?>
<?php include 'footer.php';?>

