<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">            
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Banner/trashListing"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Trash Images</p>
                        </div>
                    </a>
                </div>
            </div>            
          </div>
      </div>

      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">                        
            <div class="col-md-12">
                <div class="dc-sub">
                    <form method="post" enctype="multipart/form-data" action="" id="uploadbanner">
                      <div class="form-group">                        
                        <div class="col-md-10 col-sm-10 col-xs-10">
                          <input type="file" name="ban_file" class="form-control" />
                          <div id="uplmsg"></div>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2">
                          <button type="submit" id="bansub" class="btn btn-success">Upload</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span> 
                        </div>                        
                      </div>
                      <br>
                    </form>

                </div>
            </div>
          </div>
      </div>


    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Page List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th>No.</th>
                  <th>Image</th>                                
                  <th>Status</th>                                
                  <th>Date Created</th>
                  <th>Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($details)){
                  foreach($details as $testi){
                  ?>
                  <tr>
                    <td><?php echo $count; ?></td>
                    <td>
                      <?php $path = $testi['bn_path']; ?>
                      <img src="<?php echo base_url("assets/img/Banners/$path"); ?>" height="100px" width="150px" />  
                    </td>  
                    <td>
                      <input type="checkbox" class="banner_status" modal-aria="<?php echo $testi['bn_id']; ?>" <?php if($testi['bn_status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">                                                          
                    </td>                                
                    <td><?php echo date("d F, Y H:i", strtotime($testi['bn_date_created'])); ?></td>
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['bn_id']; ?>
                          <div class="tooltip-2"><a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="trash-banner" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Move to Trash</span>
                          </div>                                      
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>