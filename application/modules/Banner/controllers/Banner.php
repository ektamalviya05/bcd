<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class Banner extends MX_Controller{
	function __construct(){
		parent::__construct();
		$userid = $this->session->userdata("user_id");
		if(empty($userid))
			redirect("Admin");
		$this->load->model("Banners");
		$this->load->library("session");		
		$this->load->library("form_validation");

	}

	function index(){		
		$data = array();
		$con['conditions'] = array("bn_status !="=>3);
		$con['sorting'] = array("bn_id"=>"DESC");
		$data['details'] = $this->Banners->getRows($con);
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Banner Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
	}

	function add(){
		if(!empty($_FILES['ban_file']['name'])){
          $random = $this->generateRandomString(10);
          $ext = pathinfo($_FILES['ban_file']['name'], PATHINFO_EXTENSION);        
          $file_name = $random.".".$ext;
          $dirpath = './././assets/img/Banners/'.$file_name;
          if($ext == "png" || $ext == "jpg" || $ext == "gif" || $ext == "jpeg" || $ext == "JPEG" ||$ext == "PNG" || $ext == "GIF" || $ext == "JPG"){
            if(move_uploaded_file($_FILES['ban_file']['tmp_name'], $dirpath)){
            	$para = array("bn_path"=>$file_name, "bn_status"=>0);
                $check = $this->Banners->insertTable(1, $para);
                if($check){
                	$cont = "<label class='text-success'>Image uploaded successfully.</label>";
                	echo json_encode(array("status"=>200, "msg"=>$cont));
                }
                else{
                	$cont = "<label class='text-danger'>Image not uploaded, please try after some time.</label>";
                	echo json_encode(array("status"=>1, "msg"=>$cont));
                }
            }
          }
          else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only png|jpg|gif|jpeg files</label>"));
            exit;
          }
        }
        else{

        }
	}

	/*
	* Move Banner to trash
	*/
	function trashBanner(){
		$id = $this->input->post("id");
		if(!empty($id)){
			$con['conditions'] = array("bn_id"=>$id);
			$check = $this->Banners->getRows($con);
			if(count($check) > 0){
				$array = array("bn_status"=>$this->input->post("mode"));
				$opr = 2; //Update mode
				$check = $this->Banners->insertTable($opr, $array, array("bn_id"=>$id));
				if($check)
					echo json_encode(array("status"=>200));
			}
			else{
				echo json_encode(array("status"=>1));
			}
		}
		else{
			echo json_encode(array("status"=>1));
		}
	}

	/*
	* Trash Listing
	*/
	function trashListing(){
		$data = array();
		$con['conditions'] = array("bn_status"=>3);
		$con['sorting'] = array("bn_id"=>"DESC");
		$data['details'] = $this->Banners->getRows($con);
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Content Management');
	    $this->template->load('admin_layout', 'contents' , 'trashList', $data);
	}

	public function generateRandomString($length = 12) {
        // $length = 12;
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }
}

?>