    <!--  Second Top Row   -->      
      <div class="import-create dash-counter"> 
        <h2>Search Filter</h2>       
        <form action="<?php echo base_url("Slimpay/setFilterDelimiter"); ?>" method="post">
          <div class="row">
            <div class="form-group col-md-4" >
              <input type="text" autocomplete="off" class="form-control" name="user_company_name" id="user_company_name"  placeholder="Company Name" value="<?php echo $this->session->userdata("slimcompanyname"); ?>">    
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="user_company_number" id="user_company_number"  placeholder="Company Number" value="<?php echo $this->session->userdata("slimcompanynumber"); ?>">
            </div>  
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="user_company_address" id="user_company_address"  placeholder="Company Address" value="<?php echo $this->session->userdata("slimcompanyaddress"); ?>">
            </div>      
          </div>
          <div class="row">
            <div class="form-group col-md-4">
              <input type="email" autocomplete="off" class="form-control" name="user_email" id="user_email"  placeholder="Email" value="<?php echo $this->session->userdata("slimemail"); ?>">
              <input type="hidden" name="clearfilter" id="clearfilter"  value="">
            </div>
            <div class="form-group col-md-4" >
              <input type="text" class="form-control" name="mandatereference" id="mandatereference"  placeholder="Mandate Reference"  value="<?php echo $this->session->userdata("slimmandateref"); ?>">
            </div>
            <div class="form-group col-md-4">
              <input type="text" class="form-control" name="clientreference" id="clientreference"  placeholder="Client Reference" value="<?php echo $this->session->userdata("slimclientref"); ?>">
            </div>
          </div>
          <div class="row">            
            <div class="form-group col-md-2">
              <input type="submit"  name="filterSubmit" class="btn-primary form-control" value="Filter"/>           
            </div>
            <div class="form-group col-md-2">            
              <a href="<?php echo base_url("Slimpay/clearFilter"); ?>" class="btn btn-danger" >Clear Filter</a>
            </div>                
          </div>
        </form>
      </div>
    <!--  Main Containt  -->
      <div class="dash-counter users-main">
        <?php
          if(!empty($this->session->flashdata("success"))){
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <?php echo $this->session->flashdata("success"); ?>
              </div>
            </div>
          </div>
          <?php
          }
          if(!empty($this->session->flashdata("error"))){
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> <?php echo $this->session->flashdata("error"); ?>
              </div>
            </div>
          </div>
          <?php
          }
          ?>
        <h2>Business Mandate List</h2>
        <div class="user-table table-responsive">
          <table class="table table-striped table-bordered">
            <tbody>                              
              <tr >
                <th>No.</th>
                <th>Company Name</th>
                <th>Mandate Reference</th>
                <th>Client Reference</th>                                
                <th>Date Created</th>
                <th>Mandate State</th>
                <th>Action</th>
              </tr>
              <?php
              $count = 1;
              if(!empty($details)){
                foreach($details as $testi){
                ?>
                <tr>
                  <td ><?php echo $count; ?></td>
                  <td><?php echo $testi['user_company_name']; ?></td>
                  <td><?php echo $testi['slmref_mandateref']; ?></td>
                  <td><?php echo $testi['slmref_subscriber']; ?></td>
                  <td><?php echo date("d m, Y H:i:s", strtotime($testi['slmref_datecreated'])); ?></td>
                  <td>
                    <div class="displaymandstate"></div>
                    <button class="btn btn-xs btn-primary mandatestate" refer="<?php echo $testi['slmref_mandateref']?>">Check State</button>
                  </td>
                  <td>
                    <div class="link-del-view">
                      <?php $tstid = $testi['slmref_id']; ?>
                      <div class="tooltip-2"><a href="<?php echo base_url("Slimpay/view/$tstid"); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <span class="tooltiptext">View</span>
                      </div>
                    </div>
                  </td>
                </tr>
                <?php
                  $count++;
                }
              }
              ?>
            </tbody>
          </table>
          <div class="read-more">
            <?php echo $links; ?>
          </div>
        </div>          
      </div>                    
  </div>