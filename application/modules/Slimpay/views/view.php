
<div class="toppad">
  <div class="panel panel-custom user-panel">
    <div class="panel-heading">
      <h3 class="panel-title"><?php 
        if(!empty($details['bs_name']))
          $username = ucwords($details['bs_name']);
        else
          $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
          echo $username;
        ?>
      </h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3 col-md-3">
          <div class="profile_tag">
            <img alt="User Pic" src="<?php echo base_url("assets/profile_pics/").$details['user_profile_pic']; ?>" class="img-responsive main-pro_img">
          </div>
        </div>
        <div class="col-sm-9 col-md-9">
          <table class="table table-user-information">
            <tbody>
              <tr>
                <td>Name:</td>
                <td>
                  <a href="<?php echo base_url("Admin/edituser/").$details['slmref_userid']; ?>" target="_blank">
                    <?php 
                      if(!empty($details['bs_name']))
                        $username = ucwords($details['bs_name']);
                      else
                        $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
                        echo $username;
                    ?>
                  </a>
                </td>
              </tr>
              <tr>
                <td>Mandate Reference:</td>
                <td>
                  <?php 
                    echo $details['slmref_mandateref'];
                  ?>
                </td>
              </tr>
              <tr>
                <td>Client/Subscriber Reference:</td>
                <td>
                  <?php 
                    echo $details['slmref_subscriber'];
                  ?>                        
                </td>
              </tr>
              <tr>
                <td>Mandate State:</td>
                <td>
                  <?php 
                    echo $state;
                  ?>                        
                </td>
              </tr>
              <tr>
                <td>Sign Date: </td>
                <td>
                  <?php echo date("Y-m-d, h:i A", strtotime($details['slmref_datecreated'])); ?>
                </td>
              </tr>              
              <!-- <tr>
                <td colspan="2" >
                  <form id="slimpaydocupload" method="post" enctype="multipart/form-data" action="<?php echo base_url("Slimpay/uploadDocument"); ?>">
                    <div class="form-group">                      
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <input type="file" name="manddoc" class="form-control"/>
                        <input type="hidden" name="mandate" value="<?php echo $details['slmref_mandateref']; ?>">
                        <div id="uplddcmsg"></div>
                      </div>
                      <div class="col-md-6 col-sm-6 col-xs-6">
                        <button type="submit" class="btn btn-primary manddocbtn">
                          <?php
                          if(!empty($details['slmref_document'])){
                            echo "Replace Document";
                          }else{
                            echo "Upload Document";
                          }
                          ?>
                        </button>
                      </div>                      
                    </div>
                  </form>
                </td>
              </tr> -->
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel-body">
      <div class="panel-heading">
        <h3 class="panel-title">
          Order Items
        </h3>
      </div>
      <div class="row">
        <div class="col-sm-12 col-md-12">
          <table class="table table-user-information">

            <tbody>
              <tr>
                <th>Order Id</th>
                <th>Option Name</th>
                <th>Reference Id</th>
                <th>Date</th>
                <th>State</th>
                <th>Action</th>                
              </tr>
              <?php
              $orders = $this->Slimpay_model->getOrderRows(array( "conditions"=>array("slmref_mandateref"=>$details['slmref_mandateref'], "pyt_txn_status !="=>"Cancelled")));
              if(!empty($orders)){
                foreach($orders as $ords){
                  ?>
                  <tr>
                    <td><?php echo $ords['opt_user_option_id']; ?></td>
                    <td><?php echo $ords['opt_name']; ?></td>
                    <td><?php echo $ords['py_slmref_id']; ?></td>
                    <td><?php echo date("d m, Y h:i A", strtotime($ords['py_slm_datecreated'])) ?></td>
                    <td><?php echo $ords['py_slm_state']; ?></td>
                    <td><?php //echo $ords['slmref_mandateref']; ?></td>
                  </tr>
                  <?php
                }
              }
              ?>              
            </tbody>            
          </table>          
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <span class="pull-right">
          <a href="<?php echo base_url('Slimpay'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
      </span>
      <span class="clearfix"></span>
    </div>
  </div>
</div>