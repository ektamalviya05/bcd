<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Slimpay_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->mandateTbl = "otd_user_slimpayreference";
		$this->usrTbl = "users";
		$this->bsTbl = "otd_business_details";
		$this->usroptTbl = "otd_user_option";
		$this->pytTbl = "otd_user_option_payment";
        $this->usoptMaster = "otd_option_master";
	}

	function getMandateRows($params = array()){
		$this->db->select("*");
		$this->db->from($this->mandateTbl);
		$this->db->join($this->usrTbl, "$this->usrTbl.user_id = $this->mandateTbl.slmref_userid");
		$this->db->join($this->bsTbl, "$this->bsTbl.bs_user = $this->usrTbl.user_id", "left");

        if(!empty($this->session->userdata("slimcompanyname"))){
            $this->db->like("user_company_name", $this->session->userdata("slimcompanyname"));
        }
        if(!empty($this->session->userdata("slimcompanynumber"))){
            $this->db->like("user_company_number", $this->session->userdata("slimcompanynumber"));
        }
        if(!empty($this->session->userdata("slimcompanyaddress"))){
            $this->db->like("user_company_address", $this->session->userdata("slimcompanyaddress"));
        }
        if(!empty($this->session->userdata("slimemail"))){
            $this->db->like("user_email", $this->session->userdata("slimemail"));
        }
        if(!empty($this->session->userdata("slimmandateref"))){
            $this->db->like("slmref_mandateref", $this->session->userdata("slimmandateref"));
        }
        if(!empty($this->session->userdata("slimclientref"))){
            $this->db->like("slmref_subscriber", $this->session->userdata("slimclientref"));
        }
		

        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("slmref_id",$params)){
            $this->db->where('slmref_id',$params['slmref_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
	}

    function getOrderRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usroptTbl);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usroptTbl.opt_option_id");
        $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usroptTbl.opt_tran_id");
        $this->db->join($this->mandateTbl, "$this->mandateTbl.slmref_mandateref = $this->pytTbl.py_slmMandate_ref");
        $this->db->group_by("opt_user_option_id");
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("opt_user_option_id",$params)){
            $this->db->where('opt_user_option_id',$params['opt_user_option_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }       
        return $result;
    }

    function getMandate($mandate = array()){
        return $this->db->get_where($this->mandateTbl, $mandate)->row_array();
    }

    function updateMandate($params = array(), $where = array()){
        $this->db->update($this->mandateTbl, $params, $where);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
}