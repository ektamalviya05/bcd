<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Slimpay extends MX_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Slimpay_model");
		$this->load->library('pagination');
		$ses = $this->session->userdata("user_id");
		if(empty($ses)){
			redirect(base_url("Admin/Login"));
		}
	}

	public function index(){
		$data = array();
		$con['returnType'] = 'count';
		$totalcount = $this->Slimpay_model->getMandateRows($con);
		$config['base_url'] = base_url()."Slimpay/index";
		$config['total_rows'] = $totalcount;
		$config['per_page'] = 20;
		$config['uri_segment'] = 3;		
		$config['num_links'] = 2;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '<b> Next </b>';
		$config['prev_link'] = '<b> Previous </b>';		
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

		$con['limit'] = $config['per_page'];
		$start = ($page-1) * $config['per_page'];

		if($start == 0)
		{
		  $start = 0;
		}
		else
		{
		  $start = ($start/$config['per_page'] )+1;
		}

		$con['start'] = $start;		
		if($totalcount > 0){
			$con = array();
			$con['start'] = $start;
			$con['limit'] = $config['per_page'];			
		    $data['details'] = $this->Slimpay_model->getMandateRows($con);
		    // echo "<pre>";
		    // print_r($data['details']);
		    // exit;
	    }
	    else
	    {
	      	$details['details'] = array();
	    }

		$data["links"] = $this->pagination->create_links();
		// $data['details'] = $this->Slimpay_model->getMandateRows();
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Slimpay Mandate List');
		$this->template->load('admin_layout', 'contents' , 'list', $data);
	}

	public function setFilterDelimiter(){
		$this->session->set_userdata("slimcompanyname", $this->input->post("user_company_name"));
		$this->session->set_userdata("slimcompanynumber", $this->input->post("user_company_number"));
		$this->session->set_userdata("slimcompanyaddress", $this->input->post("user_company_address"));
		$this->session->set_userdata("slimemail", $this->input->post("user_email"));
		$this->session->set_userdata("slimmandateref", $this->input->post("mandatereference"));
		$this->session->set_userdata("slimclientref", $this->input->post("clientreference"));
		redirect(base_url("Slimpay"));
	}

	public function clearFilter(){
		$this->session->unset_userdata("slimcompanyname");
		$this->session->unset_userdata("slimcompanynumber");
		$this->session->unset_userdata("slimcompanyaddress");
		$this->session->unset_userdata("slimemail");
		$this->session->unset_userdata("slimmandateref");
		$this->session->unset_userdata("slimclientref");
		redirect(base_url("Slimpay"));
	}

	public function view($args = null){
		if(!empty($args)){			
			$data['details'] = $this->Slimpay_model->getMandateRows(array("slmref_id"=>$args));
			if( !empty(($data['details']))){
				$this->load->library("Slimpay_lib");
				$mandatestate = $this->slimpay_lib->getMandateState(array("reference"=>$data['details']['slmref_mandateref']));
				if(!empty($mandatestate)){
					$data['state'] = $mandatestate['state'];
				}else{
					$data['state'] = "N/A";
				}
				header("Access-Control-Allow-Origin: *");
				$this->template->set('title', 'Slimpay Mandate List');
				$this->template->load('admin_layout', 'contents' , 'view', $data);
			}else{
				$this->session->set_flashdata("error", "Record not found");
				redirect(base_url("Slimpay"));
			}
		}else{
			$this->session->set_flashdata("error", "Record not found");
			redirect(base_url("Slimpay"));
		}
	}

	public function uploadDocument(){
		if(!empty($this->session->userdata("user_id"))){
			$random = $this->generateRandomString(30);
	        $ext = pathinfo($_FILES['manddoc']['name'], PATHINFO_EXTENSION);        
	        $file_name = $random.".".$ext;
	        $dirpath = './././assets/img/BusinessMandate/'.$file_name;
	        if($ext == "pdf" ){
	            $where = array();
	            if(move_uploaded_file($_FILES['manddoc']['tmp_name'], $dirpath)){                
	                $params = array("slmref_document"=>$file_name);
	                $mandateref = $this->Slimpay_model->getMandate(array("slmref_mandateref"=>$this->input->post("mandate")));                
	                if(!empty($mandateref)){
	                    $filepath = $mandateref['slmref_document'];
	                    if(!empty($mandateref['slmref_document']))
	                        unlink("./././assets/img/BusinessMandate/$filepath");
	                    $mode = 2;
	                    $where["slmref_id"] = $mandateref['slmref_id'];
	                }else{
	                    $params["slmref_id"] = $mandateref['slmref_id'];
	                }
	                $check = $this->Slimpay_model->updateMandate($params, $where);
	                if($check){
	                    echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Document uploaded successfully.</label>"));
	                }
	                else{
	                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Document not uploaded, Please try again.</label>"));
	                }
	            }
	            else{
	                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Document not uploaded, Please try again.</label>"));
	            }
	        }
	        else{
	            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please upload only pdf file.</label>"));
	        }
	    }else{
	    	echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Invalid access.</label>"));
	    }
	}

	public function checkMandateState(){
		$ses = $this->session->userdata("user_id");
		if(!empty($ses)){
			$this->load->library("Slimpay_lib");
			$mandateref = $this->input->post("reference");
			$mandatestate = $this->slimpay_lib->getMandateState(array("reference"=>$mandateref));
			if(!empty($mandatestate)){
				echo json_encode(array("status"=>200, "state"=>$mandatestate['state']));
			}else{
				echo json_encode(array("status"=>1, "state"=>"Something went wrong, Please try again."));
			}
		}
	}

	public function generateRandomString($length = 12) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}