<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class Boxidea extends MX_Controller{
	function __construct(){
		parent::__construct();
		$userid = $this->session->userdata("user_id");
		if(empty($userid))
			redirect("Admin");
		$this->load->model("Boxidea_model");
		$this->load->library("session");		
		$this->load->library("form_validation");
	}

	function index(){
		$data = array();
		$con['sorting'] = array("bx_id"=>"DESC");
		$data['details'] = $this->Boxidea_model->getRelationalRows($con);
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Box Idea Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
	    $this->Boxidea_model->insertTable(2, array("bx_flag"=>1), array("bx_flag"=>0)); // to update messages as read
	}

	function view($args){
		$data = array();
		$con['bx_id'] = $args;
		$data['details'] = $this->Boxidea_model->getRelationalRows($con);		
		if(!empty($data['details']) && count($data['details']) > 0){
			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Box Idea Management');
		    $this->template->load('admin_layout', 'contents' , 'view', $data);
		}
		else{
			redirect(base_url("Boxidea"));
		}
	}
}
?>