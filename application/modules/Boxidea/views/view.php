<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="row">
        <div class="col-sm-10">
          <div class="title_left">
            <h3>View Message</h3>
          </div>
        </div>
        
        <div class="col-sm-2">
          <div class="title_right">
            <div class="top_search">
              <div class="input-group pull-right">
                <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
              </div>
            </div>
          </div>              
        </div>
      </div>
    </div>

    <div class="clearfix"></div>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <!-- <div class="x_title">
              <h2> <small></small></h2>                   
              <div class="clearfix"></div>
            </div> -->
            <div class="x_content">
              <br />
                <?php
                if(!empty($details)){
                 // print_r($details);
                  ?>                        
                  <table>
                    <tr>
                      <td><label>Name:</label></td>
                      <td><?php echo ucwords($details['user_firstname']." ".$details['user_lastname']); ?></td>
                    </tr>
                    <tr>
                      <td><label>Email:</label></td>
                      <td><?php echo $details['user_email']; ?></td>
                    </tr>
                    <tr>
                      <td><label>Title:</label></td>
                      <td><?php echo $details['bx_title']; ?></td>
                    </tr>
                    <tr>
                      <td><label>Message:</label></td>
                      <td><?php echo $details['bx_content']; ?></td>
                    </tr>
                  </table>
                  <?php
                  }
                ?>
                <!-- <div class="form-group">
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <label>Cateogry Name:</label>

                  </div>
                  <div class="col-md-12 col-sm-12 col-xs-12">
                    <input class="form-control" name="name" id="name" value="<?php echo set_value("name"); ?>"/>
                    <?php echo form_error('name'); ?>
                  </div>
                </div> -->                    
            </div>
          </div>
        </div>
      </div>           
    </div>
  </div>
  <!-- /page content -->