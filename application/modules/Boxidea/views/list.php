    <!--  Second Top Row   -->
      <!-- <div class="import-create dash-counter">
          <h2>Create</h2> 
          <div class="row">            
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Banner/trashListing"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Trash Images</p>
                        </div>
                    </a>
                </div>
            </div>            
          </div>
      </div> -->
    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Inquiry</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th>No.</th>
                  <th>User</th>                                
                  <th>Title</th>                                
                  <th>Message</th>
                  <th>Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($details)){
                  foreach($details as $testi){
                  ?>
                  <tr>
                    <td><?php echo $count; ?></td>
                    <td>  
                      <?php
                      echo ucwords($testi['user_firstname']." ".$testi['user_lastname']);
                      ?>                    
                    </td>  
                    <td>
                      <?php echo $testi['bx_title']; ?>
                    </td>                                
                    <td><?php echo substr($testi['bx_content'], 0, 100); ?></td>
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['bx_id']; ?>
                          <div class="tooltip-2">
                            <a href="<?php echo base_url("Boxidea/view/$tstid"); ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <span class="tooltiptext">View</span>
                          </div>
                          <!-- <div class="tooltip-2">
                            <a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="trash-banner" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Move to Trash</span>
                          </div> -->
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>