<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExtraFilters extends MX_Controller {

	public function __construct(){
	 	parent:: __construct();

	 	$userid = $this->session->userdata("user_id");
		if(empty($userid)) {redirect("Admin");}
    	$this->load->model("Extra_model");
	//	$this->load->model("Admin/Admin_model");
		$this->load->helper('url');
    	$this->load->helper('csv');
    	$this->load->library('pagination');
		$this->load->library("form_validation");
		$this->load->library('session');
  	}

  	public function index(){
	
		$data = array();
		$con['conditions'] = array("cat_status !="=>3, "type!="=>1);
		//$con['sorting'] = array("otd_business_category.cat_id"=>"DESC");

		$con['returnType'] = 'count';
		$cat_count  = $this->Extra_model->getCatRows($con);

		if($cat_count > 0){
			$con = null;
			$con = array();
			$data["cat_count"] = $cat_count;
			$con['returnType'] = '';
			$con['conditions'] = array("cat_status !="=>3, "type!="=>1);
			$data['details'] = $this->Extra_model->getCatRows($con);
		}
		else
		{
		 $data['details'] = array();
		}



		$config['base_url'] = base_url()."ExtraFilters/index";
		$config['total_rows'] = $cat_count;
		$config['per_page'] = 25;
		$config['uri_segment'] = 3;
		$config['num_links'] = $cat_count / 25;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '<b> Next </b>';
		$config['prev_link'] = '<b> Previous </b>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

		$con['limit'] = $config['per_page'];
		$start =($page-1) * $config['per_page'];

		if($start == 0)
		{
		  $start = 0;
		}
		else
		{
		  $start = ($start/$config['per_page'] )+1;
		}

		$con['start'] = $start;
		if($cat_count > 0){
			$data['details'] = $this->Extra_model->getCatRows($con);				      
			$data["record_found"] = $cat_count; 
	    }
	    else
	    {
	      	$data["record_found"] = 0; 
	    }

		$data["links"] = $this->pagination->create_links();
		$data['page'] = $page;
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Extra Filters Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
  	}

  	public function add(){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter %s"));  		
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
			$con['conditions'] = array("otd_business_category.cat_status !="=>3);
			$con['sorting'] = array("otd_business_category.cat_id"=>"DESC");
			$data['catlist'] = $this->Extra_model->getRows($con);
			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Category Management');
		    $this->template->load('admin_layout', 'contents' , 'add', $data);
		}
		else{
			$data = array("cat_name"=>$this->input->post('name'),
						  "type"=>$this->input->post('type'),
						  "cat_description"=>$this->input->post('description'),
						  "cat_status"=>$this->input->post('status'),						  
						  "cat_by_user"=>$this->session->userdata("user_id")
						 );
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						$data['cat_img_path'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$mode = 1;
			$check = $this->Extra_model->insertTable($mode, $data);
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Extra Filter added successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Extra Filter not added, Please try again.</label>"));
			}
		}
  	}

  	public function edit($args = NULL){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter %s"));  		
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
			$con['conditions'] = array("otd_business_category.cat_status !="=>3);
			$con['sorting'] = array("otd_business_category.cat_id"=>"DESC");
			$data['catlist'] = $this->Extra_model->getRows($con);
			$data['cat'] = $this->Extra_model->getRows(array("cat_id"=>$args));
			if(!empty($data['cat'])){
				header("Access-Control-Allow-Origin: *");
				$this->template->set('title', 'Category Management');
			    $this->template->load('admin_layout', 'contents' , 'edit', $data);
			}
			else{
				redirect(base_url("Category"));
			}
		}
		else{			
			$data = array("cat_name"=>$this->input->post('name'),
						  "type"=>$this->input->post('type'),
						  "cat_description"=>$this->input->post('description'),
						  "cat_status"=>$this->input->post('status'),						  
						  "cat_by_user"=>$this->session->userdata("user_id")
						 );
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						$data['cat_img_path'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$query = $this->Extra_model->getRows(array("cat_id"=>$args));
          	if(!empty($data['cat_img_path'])){
	            if(!empty($query['cat_img_path']) && $query['cat_img_path'] != "default.png"){
	              $dirpath = './././assets/img/category/'.$query['cat_img_path'];
	              unlink($dirpath);
	            }
	        }
			$mode = 2;			
			$check = $this->Extra_model->insertTable($mode, $data, array("cat_id"=>$args));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Extra Filter updated successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Extra Filter not updated, Please try again.</label>"));
			}
		}
  	}

  	public function trashListing(){
  		$data = array();
		$con['conditions'] = array("cat_status"=>3, "type!="=>1);
		// $con['sorting'] = array("otd_business_category.cat_id"=>"DESC");
		$data['details'] = $this->Extra_model->getCatRows($con);		
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Extra Filters Trash Management');
	    $this->template->load('admin_layout', 'contents' , 'trashlist', $data);
  	}

  	public function trashCategory(){
  		$id = $this->input->post("id");
		if(!empty($id)){
			$con['conditions'] = array("cat_id"=>$id);
			$check = $this->Extra_model->getRows($con);
			if(count($check) > 0){
				$array = array("cat_status"=>$this->input->post("mode"));
				$opr = 2; //Update mode
				$check = $this->Extra_model->insertTable($opr, $array, array("cat_id"=>$id));
				if($check)
					echo json_encode(array("status"=>200));
			}
			else{
				echo json_encode(array("status"=>1));
			}
		}
		else{
			echo json_encode(array("status"=>1));
		}
  	}

  	public function generateRandomString($length = 12) {
        // $length = 12;
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
    }


    public function import_csv()
{
$data = array();
$uploaded_file = "";
$csvs = array();
$result= "";
$cat_count = 0;

if($this->input->post('ImportSubmit'))
{
  $config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
  $config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
  $config['max_size']    = '20480'; //The max size of the image in kb's
  $config['overwrite'] = TRUE; 

  $this->load->library('upload', $config); //Load the upload CI library
      if (!$this->upload->do_upload('userfile'))
                {
                 $error = $this->upload->display_errors();
                      
                }
                else
                {
                 $file_info = $this->upload->data();
                 $file_name = $file_info['file_name']; 
                // $uploaded_file = "assets/content_csv/";                }
                 }
           $result =  $this->str_import_data(base_url()."assets/content_csv/". $file_name);
            }else
            {
            $data["error"]="File not found";
            }

if($result == 1)
{
  $data['import_done'] = "Categories are uploaded";
}
else
{
  $data['import_done'] = "Categories are NOT uploaded";
}

$con = null;
$con = array();
$con['returnType'] = 'count';
$cat_count  = $this->Extra_model->getCatRows($con);

if($cat_count > 0){
$con = null;
$con = array();
$data["cat_count"] = $cat_count;
$con['returnType'] = '';
$data['catlist'] = $this->Extra_model->getCatRows($con);
}
else
{
 $data['catlist'] = array();
}


$config['base_url'] = base_url()."Category/import_csv/";
$config['total_rows'] = $cat_count;
$config['per_page'] = 10;
$config['uri_segment'] = 3;
$config['num_links'] = $cat_count / 10;
$config['cur_tag_open'] = '&nbsp;<a class="current">';
$config['cur_tag_close'] = '</a>';
$config['next_link'] = '<b> Next </b>';
$config['prev_link'] = '<b> Previous </b>';
$choice = $config["total_rows"] / $config["per_page"];
$config["num_links"] = round($choice);
$this->pagination->initialize($config);

$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

$con['limit'] = $config['per_page'];
$start =($page-1) * $config['per_page'];

if($start == 0)
{
  $start = 0;
}
else
{
  $start = ($start/$config['per_page'] )+1;
}

$con['start'] = $start;
 if($cat_count > 0){
      $data['catlist'] = $this->Extra_model->getCatRows($con);
      $data["record_found"] = $cat_count; 
    }
    else
    {
      $data["record_found"] = 0; 
    }

$data["links"] = $this->pagination->create_links();
$data['page'] = $page;


        $data['import_type']="Categories"; 
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Import CSV File'); 
        $this->template->load('admin_layout', 'contents' , 'import_csv', $data);
 
}



}

?>