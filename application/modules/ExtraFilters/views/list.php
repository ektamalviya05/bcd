<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("ExtraFilters/add"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Add</h3>
                            <p>New Extra Filter</p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("ExtraFilters/trashListing"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Trash Extra Filters</p>
                        </div>
                    </a>
                </div>
            </div>                            
          </div>
      </div>


    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Extra Filters List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th >No.</th>
                  <th >Name</th>
                  <th>Short Description</th>
                  <th>Type</th>
                  <th align="center">Status</th>
                  <th >Date Created</th>
                  <th >Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($details)){
                  foreach($details as $testi){
                  ?>
                  <tr>
                    <td ><?php echo $count; ?></td>
                    <td ><?php echo $testi['cat_name'] ?></td>
                    <td><?php echo substr($testi['cat_description'], 0, 100); ?></td>
                    <td>
                      <?php 
                      if($testi['type'] == 2)
                        echo "Service";
                      else if($testi['type'] == 3)
                        echo "Product";
                      else if($testi['type'] == 4)
                        echo "Other";
                      ?>                        
                    </td>
                    <td align="center">
                      <input type="checkbox" class="cat_status" modal-aria="<?php echo $testi['cat_id']; ?>" <?php if($testi['cat_status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">                                                          
                    </td>                                
                    <td><?php echo date("d F, Y H:i", strtotime($testi['cat_date_created'])); ?></td>
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['cat_id']; ?>
                          <div class="tooltip-2"><a href="<?php echo base_url("ExtraFilters/edit/$tstid"); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Edit</span>
                          </div>
                          <div class="tooltip-2"><a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="trash-cat" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Move to Trash</span>
                          </div>
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
             <div class="read-more">
                       <?php echo $links ;  ?>
                        </div>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>