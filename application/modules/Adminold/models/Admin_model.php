<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model{
    function __construct() {
        $this->userTbl = 'users';
        $this->subsTbl = "subscribers";
    }

    
    /*
     * get rows from the users table
     */
    function getRows($params = array()){

      $this->db->select('*');
        $this->db->from($this->userTbl);
         //fetch data by conditions
         $this->db->where("status !=", 3);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
         //$this->db->where('user_type !=', 'Admin');
        $order_mode = $this->session->userdata('order_mode');
        
                if($order_mode == 'ASC')
                {
                    $order_mode = "DESC";
                    $this->session->set_userdata('order_mode',$order_mode);
                }
                else
                {
                  $order_mode = "ASC";   
                    $this->session->set_userdata('order_mode',$order_mode);
                }

          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

  // echo $this->db->last_query();
        return $result;
    }


 /*
     * get rows from the users table
     */
    function getSubsRows($params = array()){
      $this->db->select('*');
        $this->db->from($this->subsTbl);
         //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
$order_mode = $this->session->userdata('order_mode');
                if($order_mode == 'ASC')
                {
                    $order_mode = "DESC";
  $this->session->set_userdata('order_mode',$order_mode);
                }
                else
                {
                  $order_mode = "ASC";   
                    $this->session->set_userdata('order_mode',$order_mode);
                }

          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("subs_id",$params)){
            $this->db->where('subs_id',$params['subs_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

   //echo $this->db->last_query();
        return $result;
    }


 function downloadRows($params = array()){
        
 $this->db->select('user_id, user_firstname, user_lastname, user_email, user_type, user_address, user_gender, created, last_login, user_phone');

        $this->db->from($this->userTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        // $this->db->where('user_type !=', 'Admin');        
$order_mode = $this->session->userdata('order_mode');
$value = $this->session->userdata('order_by');
$this->db->order_by($value, $order_mode);
        


        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            
            

            $query = $this->db->get();

        }
        return $query;
    }


function downloadRowsSubs($params = array()){
        
 $this->db->select('*');

        $this->db->from($this->subsTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        // $this->db->where('user_type !=', 'Admin');        
$order_mode = $this->session->userdata('order_mode');
$value = $this->session->userdata('order_by');
$this->db->order_by($value, $order_mode);
        


        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            
            

            $query = $this->db->get();

        }
        return $query;
    }


     function getPersonalUsers($params = array()){
        $this->db->select('user_id, user_firstname, user_lastname, user_email, user_type, user_address, user_gender, created, last_login');
        $this->db->from($this->userTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
         $this->db->where('user_type !=', 'Admin');   
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

   
        return $result;
    }
    
    /*
     * Insert user information
     */
    public function insert($data = array()) {
        if(!array_key_exists("created", $data)){
            $data['created'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified", $data)){
            $data['modified'] = date("Y-m-d H:i:s");
        }
        $insert = $this->db->insert($this->userTbl, $data);
        
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }

    public function update($data = array(), $id=0) {

       $this->db->where('user_id', $id);
        $x = $this->db->update($this->userTbl, $data);

        //echo  $this->db->last_query();
        return $x;

    }

 public function delete($data = array(), $id=0) {

       $this->db->where('user_id', $id);
        return $this->db->update($this->userTbl, $data);

    }

}