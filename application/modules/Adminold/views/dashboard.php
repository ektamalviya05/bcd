
    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<p>&nbsp;</p>
<h1><?php 
if($usertype !="")
{
    echo strtoupper($usertype). " USERS LIST"; 
}
else
{
   $usertype="All";
   echo "Personal USERS LIST"; 
}
?></h1>
<hr>


<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Deleted");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDel').modal();
}  
</script>

  <?php
}
?>

<?php
if(isset($activated) && trim($activated) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Activated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaAct').modal();
}  
</script>

  <?php
}
else if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
 //alert( "Deactivated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>

  <?php
}
?>


<div style="float:right; margin-bottom:10px;"><a href="javascript:void(0)" class="btn btn-primary" id="export-to-csv">Download List</a></div>

<?php
$downurl = base_url(). "Admin/user_list_download/".$usertype;
?>
<form action="<?php echo $downurl; ?>" method="post" id="export-form">
                    <input type="hidden" value='' id='hidden-type' name='ExportType'/>
                  </form>


<?php
$fields = array('First Name', 'Last Name', 'Email', 'Type', 'Signup Time', 'Last Login', 'Action');
echo "<table class='table table-striped table-bordered'><tr>";
/*foreach ($fields as $k) 
{
echo "<th>".$k."</th>";
}
echo "</tr>";
*/
$bur = base_url();

echo "<tr>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/firstname'>First Name</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/lastname'>Last Name</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/email'>Email</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/type'>Type</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/signup'>Signup Time</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/lastlogin'>Last Login</a></th>";
echo "<th>Action</th>";
echo "</tr>";


foreach ($userlist as $rows) 
{
echo "<tr>";
echo "<td>".$rows['user_firstname']."</td>";
echo "<td>".$rows['user_lastname']."</td>";
echo "<td>".$rows['user_email']."</td>";
echo "<td>".$rows['user_type']."</td>";
echo "<td>".$rows['created']."</td>";
echo "<td>".$rows['last_login']."</td>";

if($rows['user_type'] == "Personal")
{
echo "<td><a href='". $bur."Admin/edituser/".$rows['user_id']."'>View</a> | <a href='". $bur."Admin/edituser/".$rows['user_id']."/updt'>Edit</a>";
}
else
{
	echo "<td><a href='". $bur."Admin/edituserpro/".$rows['user_id']."'>View</a> | <a href='". $bur."Admin/edituserpro/".$rows['user_id']."/updt'>Edit</a>";
}

echo "| <a href='". $bur."Admin/deleteuser/".$rows['user_id']."'>Delete</a> | ";


if($rows['status'] == 0)
{
echo "<a href='". $bur."Admin/activateuser/".$rows['user_id']."/1'>Activate</a>";
}
else
{
echo "<a href='". $bur."Admin/activateuser/".$rows['user_id']."/0'>Deactivate</a>";	
}
echo "</td></tr>";
}
echo "</table>";



?>


<script  type="text/javascript">
$(document).ready(function() {
jQuery('#export-to-csv').bind("click", function() {
var target = $(this).attr('id');
switch(target) {
    case 'export-to-csv' :
    $('#hidden-type').val(target);
    //alert($('#hidden-type').val());
    $('#export-form').submit();
    $('#hidden-type').val('');
    break
}
});
    });
</script>