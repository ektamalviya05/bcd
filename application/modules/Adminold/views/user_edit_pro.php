<br><br><h1><?php echo $user[0]['user_type']; ?> User Details</h2>
<br><br>
<form action="<?php echo base_url() ?>Admin/edituser" method="post">
<span>(*) Required Fields</span>

 <input type="hidden" class="form-control" name="user_id" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_id']; ?>">
 <input type="hidden" class="form-control" name="user_type" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_type']; ?>">
 
    

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="gender-main">*GENRE</label>
                    <div class="for-radio">
                      <div class="btn-group" data-toggle="buttons">
                        
 <?php
            if(!empty($user[0]['user_gender']) && $user[0]['user_gender'] == 'Female'){
                $fcheck = 'checked="checked"';
                $mcheck = '';
            }else{
                $mcheck = 'checked="checked"';
                $fcheck = '';
            }
            ?>

                        <label class="btn btn-default active">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male">Homme
                        </label>
                        <label class="btn btn-default">
                          <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female">Femme
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                
                </div>
              </div>






 <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <label>First Name</label>
                      <input type="text" class="form-control " name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_firstname']; ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                       <label>Last Name</label>
                      <input type="text" class="form-control " name="user_lastname" placeholder="NOM"  value="<?php echo $user[0]['user_lastname']; ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                       <label>Email</label>
                      <input type="email" readonly="" class="form-control" name="user_email"  placeholder="*Email"  value="<?php echo $user[0]['user_email']; ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label>Phone No.</label>
                      <input type="text" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo $user[0]['user_phone']; ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div>


<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                        
            <label>Address</label>
<input id="autocomplete" placeholder="*ADRESSE" 
  name="user_address" class="form-control" onFocus="geolocate()" type="text"  value="<?php echo $user[0]['user_address']; ?>"></input>
   
         <?php echo form_error('user_address','<span class="help-block">','</span>'); ?>
</div>
                </div>
 </div>


    
     <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" class="form-control " readonly=""  name="user_company_name" placeholder="*NOM DE L'ENTREPRISE"  value="<?php echo $user[0]['user_company_name']; ?>">
          <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" readonly="" class="form-control" name="user_company_number" placeholder="*NUMÉRO DE SIRET" value="<?php echo $user[0]['user_company_number']; ?>">
                       <?php echo form_error('user_company_number','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>

<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="language-main">LANGUE</label>
                    <div class="check-box-inline">

<?php

$xpld = explode('|', $user[0]['user_language']);
$arr_lang = array('2'=>'English', '3'=>'French','4'=>'Spanish','5'=>'Arab','6'=>'Italian','7'=>'Germany');
  
 $selectedarr = array_intersect_key($arr_lang, array_flip($xpld));

$nonselecetedarr=array_diff($arr_lang,$selectedarr);

foreach ($selectedarr as $key => $value) {
?>
<label class="checkbox-inline">
                          <input type="checkbox" value="<?php echo $key; ?>" checked
                           name="user_language[]"><?php echo $value; ?>
                        </label>

    <?php
  }                    

foreach ($nonselecetedarr as $key => $value) {
?>
<label class="checkbox-inline">
                          <input type="checkbox" value="<?php echo $key; ?>"
                           name="user_language[]"><?php echo $value; ?>
                        </label>

    <?php
  }                    
?>

                    

                    </div>
                  </div>
                </div>
              </div>


               <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
              <input type="text" class="form-control" value="<?php echo $user[0]['user_categories']; ?>" name="user_categories" placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)">

              

                  </div>
                </div>
              </div>
<?php
if($act=="updt")
{
  ?>
    <div class="row">
                <div class="col-md-12">

               
      <br>
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" value="Update" name="updateSubmit" type="submit">Update</button>

                  </div>
                </div>
              </div>
<?php
}
?>
     



   </form>