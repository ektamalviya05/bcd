<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

	public function __construct(){	 

	 	parent:: __construct();
		$this->load->library('form_validation','session', 'pagination');
    $this->load->model('Admin_model');
    $this->load->helper('url');
    $this->load->helper('csv');


	 }
	 	
function index()
{
     $data = array();
        if($this->session->userdata('isUserLoggedIn')){
        $data['user'] = $this->Admin_model->getRows(array('user_id'=>$this->session->userdata('user_id')));
    
if($data['user']['user_type'] == 'Admin')
{
             header("Access-Control-Allow-Origin: *");
                     
                     $this->template->set('title', 'Signup');
                     $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

}
else
{
     redirect(base_url().'Users/account');
}
        }else{
            redirect(base_url().'Admin/login');
        }

}




function user_list()
{
$order_by = "";
$order="";
$order = $this->uri->segment(4);

if($order == 'firstname')
{
$order_by = "user_firstname";
}
else if($order == 'lastname')
{
$order_by = "user_lastname";
}
else if($order == 'email')
{
$order_by = "user_email";
}
else if($order == 'type')
{
$order_by = "user_type";
}
else if($order == 'signup')
{
$order_by = "created";
}
else if($order == 'lastlogin')
{
$order_by = "last_login";
}



 $con['sorting'] = array(
            'order'=>$order_by
                  
                );

     $data = array();
    
     $type =  $this->uri->segment(3);
     $data['usertype'] = $type;
     $con['conditions'] = array(
            'user_type !='=> 'Admin'
            
      );
if($type != "" && $type != "All" )
{
          $con['conditions'] = array(
            'user_type'=> $type
             );
}
    if($this->session->userdata('isUserLoggedIn'))
        {
           
       $data['userlist'] = $this->Admin_model->getRows($con);
       //print_r($data);
       //echo $order_by;

      header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'User List');
        $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

    }else{
            redirect(base_url().'Admin/login');
    }

}


function subs_list()
{

$order_by = "";
$order="";
$order = $this->uri->segment(3);

if($order == 'postcode')
{
$order_by = "user_postcode";
}
else if($order == 'email')
{
$order_by = "user_email";
}
else if($order == 'signup')
{
$order_by = "subscription_date";
}
else
{
  $order_by = "subs_id";
}



 $con['sorting'] = array(
            'order'=>$order_by
                  
                );

     $data = array();
     
     

    if($this->session->userdata('isUserLoggedIn'))
        {
    $data['subslist'] = $this->Admin_model->getSubsRows($con);


      header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Subscribers List');
        $this->template->load('admin_layout', 'contents' , 'subscribers', $data);

    }else{
            redirect(base_url().'Admin/login');
    }

}

function subs_list_download()
{
$order_by = "";
$order="";


 $con['sorting'] = array(
            'order'=>$order_by
                 
              );

     $data = array();
$query = $this->Admin_model->downloadRowsSubs($con);
 query_to_csv($query,TRUE,'Subscribers_'.date('dMy').'.csv');


}

function user_list_download()
{

$order_by = "";
$order="";


 $con['sorting'] = array(
            'order'=>$order_by
                  
                );

     $data = array();
    // $data['usertype'] = "";


       $type =  $this->uri->segment(3);

     if($type != "")
{
          $con['conditions'] = array(
            'user_type'=> $type
            
      );
}

   $query = $this->Admin_model->downloadRows($con);
 query_to_csv($query,TRUE,'Users_'.date('dMy').'.csv');

}



    /*
     * User login
     */
    public function login(){

        $data = array();
        $data['error_msg']=""; 
        $con['returnType'] = 'single';
       
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        if($this->input->post('loginSubmit'))
        {
        //$this->form_validation->set_rules('user_email', 'Email', 'required');
        //$this->form_validation->set_rules('user_password', 'password', 'required');
        //if ($this->form_validation->run() == true) {
                //$con['returnType'] = 'single';

          $con['user_login'] = array("yes");
                $con['conditions'] = array(
                    'user_email'=>$this->input->post('user_email'),
                    'user_password' => md5($this->input->post('user_password')),
                    'status' => '1'
                );

                $checkLogin = $this->Admin_model->getRows($con);


                if($checkLogin){
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('user_id',$checkLogin[0]['user_id']);
                   // redirect(base_url().'Admin/dashboard');
                           redirect(base_url().'Admin/user_list');
                    
                }else{
                    $data['error_msg'] = 'Wrong email or password, please try again.';
                }
            //}
               
        }

        header("Access-Control-Allow-Origin: *");
                    // $data = array();
                     $this->template->set('title', 'Admin Login');
                     $this->template->load('admin_login_layout', 'contents' , 'login_page', $data);

    }
    
    
    /*
     * User logout
     */
    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_Id');
        $this->session->sess_destroy();
        redirect(base_url().'Admin/');
    }
    
    /*
     * Existing email check during validation
     */
    public function email_check($str){
         $con['returnType'] = 'count';
        $con['conditions'] = array('user_email'=>$str);
        $checkEmail = $this->Admin_model->getRows($con);
        if($checkEmail > 0){
            $this->form_validation->set_message('email_check', 'The given email already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
	
public function edituser()
  {
    $data = array();
    $data['error_msg']=""; 
    $userData = array();
$id = "";
 if($this->input->post('updateSubmit'))
 {

  /*
  $this->form_validation->set_rules('user_address', 'User Address', 'required');
  $this->form_validation->set_rules('user_firstname', 'First Name', 'required');
  $this->form_validation->set_rules('user_lastname', 'Last Name', 'alpha');
  $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',
        array(
                'required'      => 'You have not provided %s.',
                'numeric'     => 'This %s not numeric value.',
                'is_natural'     => 'Only natural numbers can be accepted.'
        ));
*/
  $userData = null;
  $userlang = "";

            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_gender' => $this->input->post('user_gender'),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                  );

 
$id = $this->input->post('user_id');
       //if($this->form_validation->run() == true){
                $update = $this->Admin_model->update($userData, $id);
                if($update >=1){
                    $this->session->set_userdata('success_msg', 'Data Updated.');
                    $this->session->set_userdata('upd_success', 'yes');
                    redirect(base_url()."Admin/user_list");
            }
          //}
        }
      
      $uid = $id = "";
      if($this->uri->segment(3)){
$uid = ($this->uri->segment(3)) ;
}

 if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;
$data['act'] = $action;
}
else
{
 $data['act'] = "view"; 
}

 $con['conditions'] = array(
            'user_id'=> $uid
      );


    $data['user'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    
          $this->template->set('title', 'Professional Account Creation'); 
          $this->template->load('admin_layout', 'contents' , 'user_edit', $data);
  

  }


  public function edituserpro()
  {
     $data = array();
    $data['error_msg']=""; 
    $userData = array();
$id = "";
 if($this->input->post('updateSubmit'))
 {

  /*
  $this->form_validation->set_rules('user_address', 'User Address', 'required');
  $this->form_validation->set_rules('user_firstname', 'First Name', 'required');
  $this->form_validation->set_rules('user_lastname', 'Last Name', 'alpha');
  $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',
        array(
                'required'      => 'You have not provided %s.',
                'numeric'     => 'This %s not numeric value.',
                'is_natural'     => 'Only natural numbers can be accepted.'
        ));
*/
  $userData = null;
  $userlang = "";
if($this->input->post('user_type')=="Personal")
{
            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_gender' => $this->input->post('user_gender'),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                  );

 }
 else{
  $userlang = implode("|", $this->input->post('user_language'));

   $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_gender' => $this->input->post('user_gender'),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_language' => $userlang
                  );
 }           
$id = $this->input->post('user_id');
       //if($this->form_validation->run() == true){
                $update = $this->Admin_model->update($userData, $id);
                if($update >=1){
                  //echo $update. "hsjdhfjd";
                    $this->session->set_userdata('success_msg', 'Data Updated.');
                    $this->session->set_userdata('upd_success', 'yes');
                    redirect(base_url()."Admin/user_list");
            }
          //}
        }
      
      $uid = $id = "";
      if($this->uri->segment(3)){
$uid = ($this->uri->segment(3)) ;
}

 if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;
$data['act'] = $action;
}
else
{
 $data['act'] = "view"; 
}

 $con['conditions'] = array(
            'user_id'=> $uid
      );


    $data['user'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    

    $this->template->load('admin_layout', 'contents' , 'user_edit_pro', $data);
}



public function deleteuser()
  {
  
  $data = array();
  $data['error_msg']=""; 
  $userData = array();
  $id = "";
  if($this->uri->segment(3)){
$id = ($this->uri->segment(3)) ;
}

if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;
$data['act'] = $action;
}
else
{
 $data['act'] = "view"; 
}

 

 $con['conditions'] = array(
            'user_id'=> $id
      );

$userData = array(
                'status' => '3'
                );
   $rec = $this->Admin_model->update($userData, $id);

if($rec > 0)
{
  $data['deleted'] = "yes";
}


 $con['conditions'] = array( );
  $data['usertype'] = "";
    $data['userlist'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    
          $this->template->set('title', 'Professional Account Creation'); 
          $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

// redirect(base_url()."Admin/user_list");  

  }


public function activateuser()
  {
  
  $data = array();
  $data['error_msg']=""; 
  $userData = array();
  $action="";
  $id = "";
  if($this->uri->segment(3)){
$id = ($this->uri->segment(3)) ;
}

if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;

}
echo $action;
 $con['conditions'] = array(
            'user_id'=> $id
      );


$userData = array(
                'status' => $action
                );
$rec = $this->Admin_model->update($userData, $id);

if($rec > 0 && $action==1)
{
  $data['activated'] = "yes";
}
else if($rec > 0 && $action==0)
{
  $data['activated'] = "no";
}


 $con['conditions'] = array( );
  $data['usertype'] = "";
    $data['userlist'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    
          $this->template->set('title', 'Professional Account Creation'); 
          $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

// redirect(base_url()."Admin/user_list");  

  }


}
