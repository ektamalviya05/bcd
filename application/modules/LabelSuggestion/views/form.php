<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Suggestion Messages</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($listing)){                      
                      ?>
                      <form id="UpdateSuggestion" enctype="multipart/form-data" method="post" action='<?php echo base_url("LabelSuggestion/updateSuggestion"); ?>' data-parsley-validate class="form-horizontal form-label-left">
                        <div class="x_title">
                          <h2>Mes Informations <small>Informations personnelles</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>First Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label1" id="label1" value="<?php echo $listing["label1"]; ?>"/>
                            <?php echo form_error('label1'); ?>
                          </div>
                        </div>                      
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Last Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label2" id="label2" value="<?php echo $listing["label2"]; ?>"/>
                            <?php echo form_error('label2'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Languages:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label3" id="label3" value="<?php echo $listing["label3"]; ?>"/>
                            <?php echo form_error('label3'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Gender:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label4" id="label4" value="<?php echo $listing["label4"]; ?>"/>
                            <?php echo form_error('label4'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Date Of Birth:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label5" id="label5" value="<?php echo $listing["label5"]; ?>"/>
                            <?php echo form_error('label5'); ?>
                          </div>
                        </div>
                        <hr>
                        <div class="x_title">
                          <h2>Mes Informations <small>Account parameter</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <h4 class="sub-heading">Change Email</h4>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>New Email:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label6" id="label6" value="<?php echo $listing["label6"]; ?>"/>
                            <?php echo form_error('label6'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label7" id="label7" value="<?php echo $listing["label7"]; ?>"/>
                            <?php echo form_error('label7'); ?>
                          </div>
                        </div>
                        <hr/>
                        <h4 class="sub-heading">Change Password</h4>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Old Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label8" id="label8" value="<?php echo $listing["label8"]; ?>"/>
                            <?php echo form_error('label8'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>New Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label9" id="label9" value="<?php echo $listing["label9"]; ?>"/>
                            <?php echo form_error('label9'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Confirm New Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label10" id="label10" value="<?php echo $listing["label10"]; ?>"/>
                            <?php echo form_error('label10'); ?>
                          </div>
                        </div>
                        <hr/>
                        <h4 class="sub-heading">Delete My Account</h4>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label11" id="label11" value="<?php echo $listing["label11"]; ?>"/>
                            <?php echo form_error('label11'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Informations <small>Mon entreprise</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Nom de l'entreprise:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label12" id="label12" value="<?php echo $listing["label12"]; ?>"/>
                            <?php echo form_error('label12'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Numéro de SIRET:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label13" id="label13" value="<?php echo $listing["label13"]; ?>"/>
                            <?php echo form_error('label13'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Site Web:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label14" id="label14" value="<?php echo $listing["label14"]; ?>"/>
                            <?php echo form_error('label14'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Date de création:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label15" id="label15" value="<?php echo $listing["label15"]; ?>"/>
                            <?php echo form_error('label15'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Telephone:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label16" id="label16" value="<?php echo $listing["label16"]; ?>"/>
                            <?php echo form_error('label16'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Identifiant Twitter:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label17" id="label17" value="<?php echo $listing["label17"]; ?>"/>
                            <?php echo form_error('label17'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Adresse mail du parrain:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label18" id="label18" value="<?php echo $listing["label18"]; ?>"/>
                            <?php echo form_error('label18'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Choix de votre/vos catégroie(s):</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label19" id="label19" value="<?php echo $listing["label19"]; ?>"/>
                            <?php echo form_error('label19'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Vos prestations:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label20" id="label20" value="<?php echo $listing["label20"]; ?>"/>
                            <?php echo form_error('label20'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Veuillez saisir votre adresse:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label21" id="label21" value="<?php echo $listing["label21"]; ?>"/>
                            <?php echo form_error('label21'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Decrire votre entreprise:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label22" id="label22" value="<?php echo $listing["label22"]; ?>"/>
                            <?php echo form_error('label22'); ?>
                          </div>
                        </div>                      
                        <hr/>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Prices:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label23" id="label23" value="<?php echo $listing["label23"]; ?>"/>
                            <?php echo form_error('label23'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Vos horaires d’ouverture:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label24" id="label24" value="<?php echo $listing["label24"]; ?>"/>
                            <?php echo form_error('label24'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Monday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label25" id="label25" value="<?php echo $listing["label25"]; ?>"/>
                            <?php echo form_error('label25'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Tuesday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label26" id="label26" value="<?php echo $listing["label26"]; ?>"/>
                            <?php echo form_error('label26'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Wednesday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label27" id="label27" value="<?php echo $listing["label27"]; ?>"/>
                            <?php echo form_error('label27'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Thursday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label28" id="label28" value="<?php echo $listing["label28"]; ?>"/>
                            <?php echo form_error('label28'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Friday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label29" id="label29" value="<?php echo $listing["label29"]; ?>"/>
                            <?php echo form_error('label29'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Saturday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label30" id="label30" value="<?php echo $listing["label30"]; ?>"/>
                            <?php echo form_error('label30'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Sunday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label31" id="label31" value="<?php echo $listing["label31"]; ?>"/>
                            <?php echo form_error('label31'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Ajoutez vos photos:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label32" id="label32" value="<?php echo $listing["label32"]; ?>"/>
                            <?php echo form_error('label32'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Informations <small>Boite à idée</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Titre:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label33" id="label33" value="<?php echo $listing["label33"]; ?>"/>
                            <?php echo form_error('label33'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Message:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label34" id="label34" value="<?php echo $listing["label34"]; ?>"/>
                            <?php echo form_error('label34'); ?>
                          </div>
                        </div>

                        <hr/>
                        <div class="x_title">
                          <h2>Create Event <small></small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label35" id="label35" value="<?php echo $listing["label35"]; ?>"/>
                            <?php echo form_error('label35'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Type:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label36" id="label36" value="<?php echo $listing["label36"]; ?>"/>
                            <?php echo form_error('label36'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Start Publishing :</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label37" id="label37" value="<?php echo $listing["label37"]; ?>"/>
                            <?php echo form_error('label37'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event End Publishing:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label38" id="label38" value="<?php echo $listing["label38"]; ?>"/>
                            <?php echo form_error('label38'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Start Date:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label39" id="label39" value="<?php echo $listing["label39"]; ?>"/>
                            <?php echo form_error('label39'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event End Date:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label40" id="label40" value="<?php echo $listing["label40"]; ?>"/>
                            <?php echo form_error('label40'); ?>
                          </div>
                        </div>                        
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Number Of Persons:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label41" id="label41" value="<?php echo $listing["label41"]; ?>"/>
                            <?php echo form_error('label41'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Price:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label42" id="label42" value="<?php echo $listing["label42"]; ?>"/>
                            <?php echo form_error('label42'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Address:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label43" id="label43" value="<?php echo $listing["label43"]; ?>"/>
                            <?php echo form_error('label43'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Images:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label45" id="label45" value="<?php echo $listing["label45"]; ?>"/>
                            <?php echo form_error('label45'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event details:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label44" id="label44" value="<?php echo $listing["label44"]; ?>"/>
                            <?php echo form_error('label44'); ?>
                          </div>
                        </div>

                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <button type="submit" id="catsub" class="btn btn-success">Update</button>
                            <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>                          
                            <div id="catupmsg">
                            </div>
                          </div>
                        </div>
                      </form>
                      <?php                      
                    }
                    ?>
                    
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content