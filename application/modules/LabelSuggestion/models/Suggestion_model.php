<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Suggestion_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->labelTbl = "otd_label_suggestion_list";        
	}

	/*
	* get rows from label suggestion table
	*/
    function getRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->labelTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("label_id",$params)){
            $this->db->where('label_id',$params['label_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert / Update into testimonals
    */
    function insertTable($opr = 1, $para = array(), $where = array()){
    	if($opr == 1){ // insert testimonial
    		$this->db->insert($this->labelTbl, $para);
    		$insert_id = $this->db->insert_id();
    		if($this->db->affected_rows() > 0)
    			return $insert_id;
    		else
    			return false;
    	}
    	else{ // update existing testimonial
    		$this->db->update($this->labelTbl, $para, $where);
    		return true;
    	}
    }
}