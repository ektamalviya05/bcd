<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LabelSuggestion extends MX_Controller {

	public function __construct(){
	 	parent:: __construct();
	 	$userid = $this->session->userdata("user_id");
		if(empty($userid)){redirect("Admin");}
		$this->load->model("Suggestion_model");
		$this->load->library("form_validation");
		$this->load->library('session');
  	}

  	function index(){
  		$data['listing'] = $this->Suggestion_model->getRows(array("label_id"=>1));
  		// echo "<pre>";
  		// print_r($data['listing']);
  		// exit;
  		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Label Suggestion Management');
	    $this->template->load('admin_layout', 'contents' , 'form', $data);
  	}

  	function updateSuggestion(){
  		if(!empty($this->input->post())){
	  		$check = $this->Suggestion_model->insertTable(2, $this->input->post(), array("label_id"=>1));
	  		if($check)
	  			echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Suggestion Messages updated successfully.</label>"));
	  		else
	  			echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Suggestion Messages not updated, Please try again.</label>"));
	  	}else{
	  		echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Required parameters missing.</label>"));
	  	}
  	}
}