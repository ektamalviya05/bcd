<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rating extends MX_Controller
{
    
 function __construct() {
        parent::__construct();
        $this->load->model('Rating_model', 'blog');
    }
 
    function index() {
        //the hard-coded blog id value 1 should come from UI
        $profile_id = 1;
        $user_id=1;
        $vote_results = $this->blog->get_blog_rating($profile_id);
        $data['blog_vote_overall_rows'] = $vote_results['vote_rows'];
        $data['blog_vote_overall_rate'] = $vote_results['vote_rate'];
        $data['blog_vote_overall_dec_rate'] = $vote_results['vote_dec_rate'];
        $vote_results = $this->blog->get_blog_rating_from_user($profile_id, $user_id);
        $data['blog_vote_ip_rate'] = $vote_results['vote_rate'];


         header("Access-Control-Allow-Origin: *");                     
        $this->template->set('title', 'Rating Test');
        $this->template->load('inner_layout', 'contents' , 'rating_view', $data);      

      //  $this->load->view('rating_view', $data);
    }
 
    function rate_blog() {



        if (isset($_POST)) {
            $profile_id = $_POST['blog_id'];
            $rating = $_POST['rating'];
            $user_id = 1;

            $vote_results = $this->blog->rate_blog($profile_id, $user_id, $rating);

            $blog_vote_overall_rows = $vote_results['vote_rows'];
            $blog_vote_overall_rate = $vote_results['vote_rate'];
            $blog_vote_overall_dec_rate = $vote_results['vote_dec_rate'];
            $blog_vote_ip_rate = $vote_results['vote_curr_rate'];
           
            $stars = '';
           
            for ($i = 1; $i <= floor($blog_vote_overall_rate); $i++) {
                $stars .= '<div class="star" id="' . $i . '"></div>';
            }


      
            //THE OVERALL RATING (THE OPAQUE STARS)
          echo '<div id="aa" class="r"><div id="cc" class="rating">' . $stars . '</div>' .
            '<div class="transparent">
                <div class="star" id="1"></div>
                <div class="star" id="2"></div>
                <div class="star" id="3"></div>
                <div class="star" id="4"></div>
                <div class="star" id="5"></div>
                
              </div>
            </div>';
        }
        

    }
 
}

/*<div class="votes">(' . $blog_vote_overall_dec_rate . '/5, ' . $blog_vote_overall_rows . ' ' . ($blog_vote_overall_rows > 1 ? ' votes' : ' vote') . ') ' . ($blog_vote_ip_rate > 0 ? '<strong>You rated this: <span style="color:#39C;">' . $blog_vote_ip_rate . '</span></strong>' : '') . '</div>*/