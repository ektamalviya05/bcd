<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tbName = "otd_business_category";
        $this->homeCatTbl = "otd_home_cat_tab";
        $this->userTbl = "users";
        $this->usCatTbl = "otd_user_business_category";
        $this->subCatTbl = "otd_business_category";
        
	}

	/*
	* get rows from the testimonail table
	*/
    function getRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->tbName);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get relational rows
    */
    public function getRelationalRows($params = array()){


    	$this->db->select("$this->tbName.*, t1.cat_name AS parentName, t1.cat_id AS Parentid");
        $this->db->from($this->tbName);
        $this->db->join($this->tbName." AS t1", "t1.cat_id = $this->tbName.cat_parent", "left");
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['bn_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert / Update into testimonals
    */

    function insertTable($opr = 1, $para = array(), $where = array()){
    	if($opr == 1){ // insert testimonial
    		$this->db->insert($this->tbName, $para);
    		$insert_id = $this->db->insert_id();
    		if($this->db->affected_rows() > 0)
    			return $insert_id;
    		else
    			return false;
    	}
    	else{ // update existing testimonial
    		$this->db->update($this->tbName, $para, $where);
    		return true;
    	}
    }

    /*
     * get rows from the users table
     */
    function getCatRows($params = array()){
      $this->db->select('*');
        $this->db->from($this->tbName);
         //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


        if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("like",$params)){
            foreach ($params['like'] as $key => $value) {
                $this->db->like($key,$value);                
            }
        }
        $order_mode = "DESC";
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

              $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        //$this->db->where("status !=", "3");
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{  
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

   // echo $this->db->last_query();
        return $result;
    }
    
    /*
    * get rows from the testimonail table
    */
    function getHomeTabRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->homeCatTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }
    /*
    * get rows from the testimonail table
    */
    function getSubTabRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->subCatTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    public function updateHomeTab($params = array(), $where = array()){
        $this->db->update($this->homeCatTbl, $params, $where);
        // if($this->db->affected_rows() > 0){
            return true;
        // }else{
        //     return false;
        // }
    }

public function isCat($cat_name, $data)
{
    $sql = "select cat_id from otd_business_category where cat_name like '%".$cat_name."%'";
    $query = $this->db->query($sql);
    $id = $query->row_array();
    if(!empty($id))
    {
       return $id; 
    }
    else {
        $this->db->insert("otd_business_category", $data);
        $id = $this->db->insert_id();
    }
return $id;
}
      /*
     * Insert user information
     */
     public function insert($data = array()) {
        
                $auth_check_subs = 0;
                $sms_check_subs = 0;
                $user_email_subs =  null;
                $user_postcode_subs =  null;
        
                //  //add created and modified data if not included
                // if(!array_key_exists("created", $data)){
                //     $data['created'] = date("Y-m-d H:i:s");
                // }
                // if(!array_key_exists("modified", $data)){
                //     $data['modified'] = date("Y-m-d H:i:s");
                // }
        
                // if(array_key_exists("auth_check", $data)){
                //     $auth_check_subs = 1;
                // }
                //  if(array_key_exists("sms_check", $data)){
                //     $sms_check_subs = 1;
                // }
        
                //  if(array_key_exists("user_email", $data)){
                //     $user_email_subs =  $data['user_email'];
                // }
        
                //  if(array_key_exists("user_postcode", $data)){
                //     $user_postcode_subs =  $data['user_postcode'];
                // }
        
        
                //insert user data to users table
                $this->db->insert($this->userTbl, $data);
                $insert = $this->db->insert_id();
                
                //return the status
                if($insert){
                    // $subsData = array();
                    // if($auth_check_subs == 1 || $sms_check_subs == 1)
                    // {
                    //     $subsData = array('user_id' => $this->db->insert_id(),
                    //                       'user_email'=>$user_email_subs,
                    //                       'user_postcode' => $user_postcode_subs,
                    //                       'promo_mail' => $auth_check_subs,
                    //                       'promo_sms' => $sms_check_subs,
                    //                     );
                    //   $insert1 = $this->db->insert($this->userTblSubs, $subsData);
                    // }
                    return $insert;
                }else{
                    return false;
                }
            }

             /*
     * Insert user information
     */
     public function insert_profile($data = array()) {
                       $this->db->insert("otd_business_details", $data);
                $insert = $this->db->insert_id();
                if($insert){
                 return $insert;
                }else{
                    return false;
                }
            }

    /*
    * insert user categories
    */
    public function insertUsercategories($params = array()){
        $this->db->insert($this->usCatTbl, $params);
        return $this->db->insert_id();        
    }

    function highlightkeyword($str, $search) {
        $highlightcolor = "#daa732";
        $occurrences = substr_count(strtolower($str), strtolower($search));
        $newstring = $str;
        $match = array();
     
        for ($i=0;$i<$occurrences;$i++) {
            $match[$i] = stripos($str, $search, $i);
            $match[$i] = substr($str, $match[$i], strlen($search));
            $newstring = str_replace($match[$i], '[#]'.$match[$i].'[@]', strip_tags($newstring));
        }
     
        $newstring = str_replace('[#]', '<b style="color: '.$highlightcolor.';">', $newstring);
        $newstring = str_replace('[@]', '</b>', $newstring);
        return $newstring;   
    }
}
?>