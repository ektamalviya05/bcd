<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class Lang_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->pageHeadTbl = "otd_languages";
	}

	/*
     * get rows from the heading content table
     */
    function getRows($params = array()){

        $this->db->select($params['select']);
        $this->db->from($this->pageHeadTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("grouping",$params)){            
            $this->db->group_by($params['grouping']);            
        }
        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
 

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        
        return $result;
    }

    /*
    * Insert / Update into heading content table
    */

    function insertTable($opr = 1, $para = array(), $where = array()){
    	if($opr == 1){ // insert testimonial
    		$this->db->insert($this->pageHeadTbl, $para);
    		$insert_id = $this->db->insert_id();
    		if($this->db->affected_rows() > 0)
    			return $insert_id;
    		else
    			return false;
    	}
    	else{ // update existing testimonial
    		$this->db->update($this->pageHeadTbl, $para, $where);
    		return true;
    	}
    }
	function deleteTable($id){ 
	       $this->db->where("lang_id",$id);
	       $this->db->delete($this->pageHeadTbl);
		   return true;
	
	}
    function get_language($status){
        
        $this -> db -> select('*');
        $this -> db -> from('language'); 
        if($status==1){
        $this -> db -> where('lang_status = ' , 1 ); 
        }
        
        $this->db->order_by("lang_id", "desc");
        $query = $this -> db -> get();
        return $query->result_array();
    }
    function get_language1($lang){
         $this -> db -> select('*');
        $this -> db -> from('language'); 
        //if($status==1){
        $this -> db -> where('lang_id = ' , $lang ); 
        //}
        
        $this->db->order_by("lang_id", "desc");
        $query = $this -> db -> get();
        return $query->result_array();
    }
    function get_edit_language($id){
        $this -> db -> select('*');
        $this -> db -> from('language'); 
        $this -> db -> where('lang_id = ' ,$id ); 
        $query = $this -> db -> get();
        $result =$query->result();
        
         $data['lang_name'] = $result[0]->lang_name;
         $data['lang_id'] = $result[0]->lang_id;
         $data['lang_code'] = $result[0]->lang_code;
         $data['lang_status'] = $result[0]->lang_status;
         $data['image'] = $result[0]->image;
         return $data;
         
        
    }
    function edit_language($data,$id){
         $this->db->where('lang_id',$id);
         $query = $this->db->update('language', $data);
         
         return $query;
         
        
        
    }
}
?>