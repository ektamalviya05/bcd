<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<!-- page content -->
        <div class="right_col" role="main">          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Category Tab <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <form id="editcategorytab" enctype="multipart/form-data" method="post" action='<?php echo base_url("Category/edit"); ?>' data-parsley-validate class="form-horizontal form-label-left">                                            
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Name:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="name" id="name" value="<?php echo $cat['tab_name']; ?>">
                          <?php echo form_error('name'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Image:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="cat_file" min="1" class="form-control"/>                          
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">                          
                          <input type="hidden" name="editid" value="<?php echo $cat['tab_id']; ?>">
                          <button type="submit" id="catsub" class="btn btn-success">Save</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                          <div id="catupmsg">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->