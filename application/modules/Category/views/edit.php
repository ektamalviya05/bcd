<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<!-- page content -->
        <div class="right_col" role="main">
          <input type="hidden" id="editid" value="<?php echo $cat['cat_id']; ?>">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Category <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
              <!-- <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div> -->
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <form id="editcategory" enctype="multipart/form-data" method="post" action='<?php echo base_url("Category/edit"); ?>' data-parsley-validate class="form-horizontal form-label-left">                                            
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Category name in English:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="name" id="name" value="<?php echo $cat['cat_name']; ?>">
                          <?php echo form_error('name'); ?>
                        </div>
                      </div> 

                      <?php 
                      if($cat['cat_parent'])
                      {
                      ?>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Choose Category:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                             <select name="parentcategory" class="form-control">
                               
                                <option value="0">Make as main Category</option>
                                <?php foreach ($parentcategoryname as $value) {
                                ?>
                                <option value="<?php echo $value['cat_id']; ?>" <?php if($cat['cat_parent'] == $value['cat_id']){ echo "selected" ;} ?> ><?php echo  $value['cat_name'] ?></option>
                                <?php  
                                } ?>
                              </select>
                          </div>
                      </div>
                      <?php
                      }
                      else{
                      ?>


                      <?php
                      }
                      ?>



                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Category Description in English:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="description" class="form-control" placeholder="Description"><?php echo $cat['cat_description']; ?></textarea>
                          <?php echo form_error("description"); ?>
                        </div>
                      </div>
                       <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Category name in Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="namea" id="name" value="<?php echo $cat['cat_namea']; ?>">
                          <?php echo form_error('name'); ?>
                        </div>
                      </div> 
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Category Description in Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="descriptiona" class="form-control" placeholder="Description"><?php echo $cat['cat_descriptiona']; ?></textarea>
                          <?php echo form_error("descriptiona"); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Image:</label>(Only png image)
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="cat_file" class="form-control"/>
                          <div id="flupmsg"></div>                          
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Status:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="th-sele">
                            <select name="status" class="form-control">
                              <option value="">Select Status</option>
                              <option value="1" <?php if($cat['cat_status'] == 1) echo "selected"; ?>>Publish</option>
                              <option value="0" <?php if($cat['cat_status'] == 0) echo "selected"; ?>>Unpublish</option>
                            </select>
                            <?php echo form_error('status'); ?>
                          </div>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Show on Home Under Search:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" value="1" name="displayhome" <?php // if($cat['cat_homedisplay'] == 1) echo "checked";?>> Yes
                          <input type="radio" value="0" name="displayhome" <?php // if($cat['cat_homedisplay'] == 0) echo "checked";?>> No
                          <?php // echo form_error('displayhome'); ?>
                        </div>
                      </div> -->
                      <!-- <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Publish on Home Page:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="radio" value="1" <?php if($cat['cat_hometabdisplay'] == 1) echo "checked"; ?> name="displayhomecattab" > Yes
                          <input type="radio" value="0" <?php if($cat['cat_hometabdisplay'] == 0) echo "checked"; ?> name="displayhomecattab"> No
                          <?php echo form_error('displayhomecattab'); ?>
                        </div>
                      </div> -->
                     <!--  <div class="form-group displaytabdiv" style="<?php if($cat['cat_hometabdisplay'] != 1) echo "display:none;"; ?>">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Select Tab to show:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select class="form-control" name="displaytab">
                            <option value="">Select Display Tab</option>
                          <?php                          
                          foreach($tablist as $hmt){
                            ?>
                            <option value="<?php echo $hmt["cat_id"]; ?>" <?php if($cat['cat_parent'] == $hmt['cat_id']) echo "selected"; ?>><?php echo $hmt['cat_name'];  ?></option>
                            <?php
                          }
                          ?>
                          </select>
                          <?php echo form_error('displayhomecattab'); ?>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <input type="hidden" value="1" name="type">
                          <button type="submit" id="catsub" class="btn btn-success">Save</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>                          
                          <div id="catupmsg">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->