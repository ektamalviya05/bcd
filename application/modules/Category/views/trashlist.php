<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Category List</p>
                        </div>
                    </a>
                </div>
            </div>                                      
          </div>
      </div>


    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Category List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th >No.</th>
                  <th >Name</th>
                  <th>Short Description</th>
                  <th >Date Created</th>
                  <th >Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($details)){
                  foreach($details as $testi){
                  ?>
                  <tr>
                    <td ><?php echo $count; ?></td>
                    <td ><?php echo $testi['cat_name'] ?></td>
                    <td><?php echo substr($testi['cat_description'], 0, 100); ?></td>
                    <td><?php echo date("d F, Y H:i", strtotime($testi['cat_date_created'])); ?></td>
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['cat_id']; ?>
                          <div class="tooltip-2"><a href="javascript:;"   data-placement="top" class="trashback-cat" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-reply" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Restore</span>
                          </div>
                          <div class="tooltip-2"><a href="javascript:;"   data-placement="top" class="trashback-publishcat" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-window-restore" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Restore & Publish</span>
                          </div>
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>