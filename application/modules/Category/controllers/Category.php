<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends MX_Controller {

	public function __construct(){
	 	parent:: __construct();

	 	$userid = $this->session->userdata("user_id");
		if(empty($userid)) {redirect("Admin");}
    	$this->load->model("Categories");
    	$this->load->model("Lang_model");
		$this->load->helper('url');
    	$this->load->helper('csv');
    	$this->load->library('pagination');
		$this->load->library("form_validation");
		$this->load->library('session');
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
  	}

  	public function index(){
	
		$data = array();
		$con['conditions'] = array("otd_business_category.cat_status !="=>3 , "type"=>1);
		//$con['sorting'] = array("otd_business_category.cat_id"=>"DESC");

		$con['returnType'] = 'count';
		if(!empty($this->session->userdata("filtercategory"))){
			$con['like'] = array("cat_name"=>$this->session->userdata("filtercategory"));
		}
		$cat_count  = $this->Categories->getCatRows($con);		

		if($cat_count > 0){
			$con = null;
			$con = array();
			$data["cat_count"] = $cat_count;
			$con['returnType'] = '';
			$con['conditions'] = array("otd_business_category.cat_status !="=>3 , "type"=>1);
			if(!empty($this->session->userdata("filtercategory"))){
				$con['like'] = array("cat_name"=>$this->session->userdata("filtercategory"));
			}
			$data['details'] = $this->Categories->getCatRows($con);
		}
		else
		{
			$data['details'] = array();
		}



		$config['base_url'] = base_url()."Category/index";
		$config['total_rows'] = $cat_count;
		$config['per_page'] = 10;
		$config['uri_segment'] = 3;
		//$config['num_links'] = $cat_count / 50;
		$config['num_links'] = 2;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '<b> Next </b>';
		$config['prev_link'] = '<b> Previous </b>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

		$con['limit'] = $config['per_page'];
		$start =($page-1) * $config['per_page'];

		if($start == 0)
		{
		  $start = 0;
		}
		else
		{
		  $start = ($start/$config['per_page'] )+1;
		}

		$con['start'] = $start;
		if(!empty($this->session->userdata("filtercategory"))){
			$con['like'] = array("cat_name"=>$this->session->userdata("filtercategory"));
		}
		if($cat_count > 0){
		    $data['details'] = $this->Categories->getCatRows($con);		
		    $data["record_found"] = $cat_count; 
	    }
	    else
	    {
	      	$data["record_found"] = 0; 
	    }

		$data["links"] = $this->pagination->create_links();
		$data['page'] = $page;
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Category Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
  	}

  	function setFilterDelimiter(){
  		$category = $this->input->post("category");
  		if(!empty($category))
  			$this->session->set_userdata("filtercategory", $category);
  		redirect(base_url("Category"));
  	}

  	function clearFilterDelimiter(){
  		$this->session->unset_userdata("filtercategory");
  		redirect(base_url("Category"));
  	}

  	public function add(){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		//$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("type", "type", "trim|required", array("required"=>"Please enter %s"));
  		//$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter %s"));
  		//$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
	  		$con['conditions'] = array("otd_business_category.cat_status !="=>3 , "cat_parent"=>0);					
			$data['tablist'] = $this->Categories->getSubTabRows($con);
			$data['languages'] = $this->Lang_model->get_language(1);
			$newcon['conditions'] = array("otd_business_category.cat_status !="=>3 , "type"=>1, "cat_parent"=>0);
			$data['parentcategoryname']  = $this->Categories->getCatRows($newcon);
           

			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Category Management');
		    $this->template->load('admin_layout', 'contents' , 'add', $data);
		}
		else{
			$parent = $this->input->post("parentcategory");
			if($parent){
				$cat_parent =$this->input->post("parentcategory");
			}
			else{
				$cat_parent = 0;
			}
			$data = array("cat_name"=>$this->input->post('name'),
				          "cat_namea"=>$this->input->post('namea'),
						  "type"=>$this->input->post('type'),
						  "cat_description"=>$this->input->post('description'),
						  "cat_descriptiona"=>$this->input->post('descriptiona'),
						  "cat_status"=>$this->input->post('status'),
						  "cat_hometabdisplay" => '0',
						  "parentcategory"=>$this->input->post("parentcategory"),
						  //"cat_hometabdisplay"=>$this->input->post("displayhomecattab"),
						  "cat_parent"=>$cat_parent,
						  "cat_by_user"=>$this->session->userdata("user_id"),
						  "lang_id"=>$this->input->post('language_id'),
						 );
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						//$data['cat_img_path'] = $file_name;
						$detail['cat_img_path'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$mode = 1;
			//$count = count($this->input->post('name'));
			//for ($i=0; $i<$count; $i++){
			    $detail['cat_name'] = $data['cat_name'];
			    $detail['cat_namea'] = $data['cat_namea'];
				$detail['type']     = $data['type'];
				$detail['cat_description']     = $data['cat_description'];
				$detail['cat_descriptiona']     = $data['cat_descriptiona'];
				$detail['cat_status']     = $data['cat_status'];
				$detail['cat_hometabdisplay']     = $data['cat_hometabdisplay'];
				$detail['cat_parent']     = $data['cat_parent'];
				$detail['cat_by_user']     = $data['cat_by_user'];
				//$detail['lang_id']     = $data['lang_id'][$i];
				$check = $this->Categories->insertTable($mode, $detail);
			    //}
			//$check = $this->Categories->insertTable($mode, $data);
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Category added successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Category not added, Please try again.</label>"));
			}
		}
  	}

  	public function edit($args = NULL,$lang_id = NULL){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("type", "type", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
			// $con['conditions'] = array("otd_business_category.cat_status !="=>3);
			// $con['sorting'] = array("otd_business_category.cat_id"=>"DESC");
			// $data['catlist'] = $this->Categories->getRows($con);

			$con['conditions'] = array("otd_business_category.cat_status !="=>3 , "cat_parent"=>0);
			$data['tablist'] = $this->Categories->getSubTabRows($con);
			$data['cat'] = $this->Categories->getRows(array("cat_id"=>$args));
			$data['languages'] = $this->Lang_model->get_language1($lang_id);
			$newcon['conditions'] = array("otd_business_category.cat_status !="=>3 , "type"=>1, "cat_parent"=>0);
			$data['parentcategoryname']  = $this->Categories->getCatRows($newcon);
			if(!empty($data['cat'])){
				header("Access-Control-Allow-Origin: *");
				$this->template->set('title', 'Category Management');
			    $this->template->load('admin_layout', 'contents' , 'edit', $data);
			}
			else{
				redirect(base_url("Category"));
			}
		}
		else{	

			$data = array("cat_name"=>$this->input->post('name'),
				           "cat_namea"=>$this->input->post('namea'),
						  "type"=>$this->input->post('type'),
						  "cat_description"=>$this->input->post('description'),
						  "cat_descriptiona"=>$this->input->post('descriptiona'),
						  "cat_status"=>$this->input->post('status'),
						  //"parentcategory"=>$this->input->post("parentcategory"),
						  // "cat_homedisplay"=>$this->input->post("displayhome"),
						  "cat_hometabdisplay"=>$this->input->post("displayhomecattab"),
						  "cat_parent"=>$this->input->post("parentcategory"),
						  "cat_by_user"=>$this->session->userdata("user_id")
						 );
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						$data['cat_img_path'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$query = $this->Categories->getRows(array("cat_id"=>$args));
          	if(!empty($data['cat_img_path'])){
	            if(!empty($query['cat_img_path']) && $query['cat_img_path'] != "default.png"){
	              $dirpath = './././assets/img/category/'.$query['cat_img_path'];
	              unlink($dirpath);
	            }
	        }
			$mode = 2;			
			$check = $this->Categories->insertTable($mode, $data, array("cat_id"=>$args));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Category updated successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Category not updated, Please try again.</label>"));
			}
		}
  	}

  	public function view($args = NULL){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("type", "type", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
			// $con['conditions'] = array("otd_business_category.cat_status !="=>3);
			// $con['sorting'] = array("otd_business_category.cat_id"=>"DESC");
			// $data['catlist'] = $this->Categories->getRows($con);

			$con['conditions'] = array("otd_business_category.cat_status !="=>3 , "cat_parent"=>0);
			$data['tablist'] = $this->Categories->getSubTabRows($con);
			$data['cat'] = $this->Categories->getRows(array("cat_id"=>$args));
			if(!empty($data['cat'])){
				header("Access-Control-Allow-Origin: *");
				$this->template->set('title', 'Category View');
			    $this->template->load('admin_layout', 'contents' , 'view', $data);
			}
			else{
				redirect(base_url("Category"));
			}
		}
		else{	

			$data = array("cat_name"=>$this->input->post('name'),
						  "type"=>$this->input->post('type'),
						  "cat_description"=>$this->input->post('description'),
						  "cat_status"=>$this->input->post('status'),
						  // "cat_homedisplay"=>$this->input->post("displayhome"),
						  "cat_hometabdisplay"=>$this->input->post("displayhomecattab"),
						  "cat_parent"=>$this->input->post("displaytab"),
						  "cat_by_user"=>$this->session->userdata("user_id")
						 );
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						$data['cat_img_path'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$query = $this->Categories->getRows(array("cat_id"=>$args));
          	if(!empty($data['cat_img_path'])){
	            if(!empty($query['cat_img_path']) && $query['cat_img_path'] != "default.png"){
	              $dirpath = './././assets/img/category/'.$query['cat_img_path'];
	              unlink($dirpath);
	            }
	        }
			$mode = 2;			
			$check = $this->Categories->insertTable($mode, $data, array("cat_id"=>$args));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Category updated successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Category not updated, Please try again.</label>"));
			}
		}
  	}




  	public function trashListing(){
  		$data = array();
		$con['conditions'] = array("otd_business_category.cat_status"=>3);
		$con['sorting'] = array("otd_business_category.cat_id"=>"DESC");
		$data['details'] = $this->Categories->getRelationalRows($con);		
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Category Management');
	    $this->template->load('admin_layout', 'contents' , 'trashlist', $data);
  	}

  	public function trashCategory(){
  		$id = $this->input->post("id");
		if(!empty($id)){
			$con['conditions'] = array("cat_id"=>$id);
			$check = $this->Categories->getRows($con);
			if(count($check) > 0){
				$array = array("cat_status"=>$this->input->post("mode"));
				$opr = 2; //Update mode
				$check = $this->Categories->insertTable($opr, $array, array("cat_id"=>$id));
				if($check)
					echo json_encode(array("status"=>200));
			}
			else{
				echo json_encode(array("status"=>1));
			}
		}
		else{
			echo json_encode(array("status"=>1));
		}
  	}

  	public function trashCategory1(){
  		$id = $this->input->post("id");
		if(!empty($id)){
			// $con['conditions'] = array("cat_id"=>$id,
				                      // "cat_parent" => 0
		                              //);



			$con['conditions'] = array("cat_id" =>$id,"cat_parent"=>0);



			$check = $this->Categories->getRows($con);
           
			if($check){
				//$array = array("cat_status"=>$this->input->post("mode"));
				$mode =$this->input->post("mode");
				if($mode == 0)
				{
					$catid = array("cat_id"=>$id);
					$array = array("cat_status"=>$this->input->post("mode"));
					$this->db->where("cat_id=",$id);
					$sql =$this->db->update("otd_business_category",$array);
					if($sql)
					{
						$array = array("cat_status"=>$this->input->post("mode"));
						$this->db->where("cat_parent=",$id);
					    $query =$this->db->update("otd_business_category",$array);
					}
					if($sql)
					{
						$status = array("status"=>0);
						$this->db->where("cat_id=",$id);
					    $sq =$this->db->update("bdc_business",$status);

					}
				}
				if($mode == 1)
				{
					$array = array("cat_status"=>$this->input->post("mode"));
					$this->db->where("cat_id=",$id);
					$sql =$this->db->update("otd_business_category",$array);

				}
				
                
				
				
				//$check = $this->Categories->insertTable($opr, $array, array("cat_id"=>$id))
				echo json_encode(array("status"=>200));
			}
			else
			{
				$array = array("cat_status"=>$this->input->post("mode"));
				$this->db->where("cat_id=",$id);
			    $query =$this->db->update("otd_business_category",$array);
			    if($query)
			    {
			    	$status = array("status"=>0);
					$this->db->where("sub_cat_id=",$id);
					$sq =$this->db->update("bdc_business",$status);
					
			    }
			   

			    echo json_encode(array("status"=>200));

			}
		}
		else{
			echo json_encode(array("status"=>1));
		}
  	}


  	public function homeTab(){
  		$data = array();
		$data['details'] = $this->Categories->getHomeTabRows();
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Category Home Tab Management');
	    $this->template->load('admin_layout', 'contents' , 'cathometablist', $data);
  	}

  	public function editCatTab($args = NULL){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
  		$this->form_validation->set_rules("name", "name", "required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("editid", "tab id", "required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
			$data['cat'] = $this->Categories->getHomeTabRows(array("tab_id"=>$args));		
			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Category Home Tab Management');
		    $this->template->load('admin_layout', 'contents' , 'editcattab', $data);
		}else{
			$data = array("tab_name"=>$this->input->post('name'));
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						$data['tab_image'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$query = $this->Categories->getHomeTabRows(array("tab_id"=>$args));
          	if(!empty($data['tab_image'])){
	            if(!empty($query['tab_image']) && $query['tab_image'] != "default.png"){
	              $dirpath = './././assets/img/category/'.$query['tab_image'];
	              unlink($dirpath);
	            }
	        }			
			$check = $this->Categories->updateHomeTab($data, array("tab_id"=>$this->input->post("editid")));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Category updated successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Category not updated, Please try again.</label>"));
			}
		}
  	}

  	public function generateRandomString($length = 12) {
        // $length = 12;
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
    }


    public function import_csv()
	{
		$data = array();
		$uploaded_file = "";
		$csvs = array();
		$result= "";
		$cat_count = 0;

		if($this->input->post('ImportSubmit'))
		{
		  $config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
		  $config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
		  $config['max_size']    = '20480'; //The max size of the image in kb's
		  $config['overwrite'] = TRUE; 

		  $this->load->library('upload', $config); //Load the upload CI library
		
	
		  	if (!$this->upload->do_upload('userfile'))
			{
				$error = $this->upload->display_errors();
				echo $error;
			}
			else
			{
				$file_info = $this->upload->data();
				$file_name = $file_info['file_name']; 
			}
					
			// echo base_url()."assets/content_csv/". $file_name;
				$result =  $this->uploadCSVData(base_url()."assets/content_csv/". $file_name);
				
				$data["success"] = $result["success"];
				//print_r($result);
				 //die("here");   
				}else
		            {
		            $data["error"]="File not found";
		            }

		if($result == 1)
		{
		  $data['import_done'] = "Categories are uploaded";
		}
		else
		{
		  $data['import_done'] = "Categories are NOT uploaded";
		}

		$con = null;
		$con = array();
		$con['returnType'] = 'count';
		$cat_count  = $this->Categories->getCatRows($con);

		if($cat_count > 0){
		$con = null;
		$con = array();
		$data["cat_count"] = $cat_count;
		$con['returnType'] = '';
		$data['catlist'] = $this->Categories->getCatRows($con);
		}
		else
		{
		 $data['catlist'] = array();
		}


		$config['base_url'] = base_url()."Category/import_csv/";
		$config['total_rows'] = $cat_count;
		$config['per_page'] = 50;
		$config['uri_segment'] = 3;
		//$config['num_links'] = $cat_count / 50;
		$config['num_links'] = 2;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '<b> Next </b>';
		$config['prev_link'] = '<b> Previous </b>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

		$con['limit'] = $config['per_page'];
		$start =($page-1) * $config['per_page'];

		if($start == 0)
		{
		  $start = 0;
		}
		else
		{
		  $start = ($start/$config['per_page'] )+1;
		}

		$con['start'] = $start;
		 if($cat_count > 0){
		      $data['catlist'] = $this->Categories->getCatRows($con);
		      $data["record_found"] = $cat_count; 
		    }
		    else
		    {
		      $data["record_found"] = 0; 
		    }

		$data["links"] = $this->pagination->create_links();
		$data['page'] = $page;


		        $data['import_type']="Categories"; 
		        header("Access-Control-Allow-Origin: *");
		        $this->template->set('title', 'Import CSV File'); 
		        $this->template->load('admin_layout', 'contents' , 'import_csv', $data);
		 
	}

	function uploadCSVData($fp)
    {
        $count=0;
        $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
        while($csv_line = fgetcsv($fp,1024))
        {
			// echo "<pre>";
			// print_r($csv_line);
			// echo "</pre>";
            $count++;
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
            for($i = 0, $j = count($csv_line); $i < $j; $i++)
            {

				// echo "<br>".$csv_line[0]. "<br>";
                $insert_csv = array();
             
				$insert_csv['cat_name'] = $csv_line[0];
                $insert_csv['cat_description'] = $csv_line[0];
				$insert_csv['cat_parent'] = 0;
				$insert_csv['cat_img_path'] = 'default.png';
				$insert_csv['cat_homedisplay'] = 0;
				$insert_csv['cat_status'] = 1;
				$insert_csv['cat_by_user'] = 0;
				$insert_csv['type'] = 1;
				$insert_csv['cat_hometabdisplay'] = 0;
				$insert_csv['cat_hometab_id'] = 0;
            }
			$i++;


            $data = array(
                'cat_name' => $insert_csv['cat_name'],
				'cat_description' => $insert_csv['cat_description'],
				'cat_parent' => $insert_csv['cat_parent'],
				'cat_img_path' => $insert_csv['cat_img_path'],
				'cat_homedisplay' => $insert_csv['cat_homedisplay'],
				'cat_status' => $insert_csv['cat_status'],
				'cat_by_user' => $insert_csv['cat_by_user'],
				'type' => $insert_csv['type'],
				'cat_hometabdisplay' => $insert_csv['cat_hometabdisplay'],
				'cat_hometab_id' => $insert_csv['cat_hometab_id']
			
			);

			$x = $this->check_cat($insert_csv['cat_name']);

			if($x >= 1)
			{

			}
			else {
				$data['inserted_csv']=$this->db->insert('otd_business_category', $data);
			}
		
			
		}
	
        fclose($fp) or die("can't close file");
        $data['success']="success";
        return $data;
    }


	function check_cat($cat_name)
	{
		$check = $this->Categories->getRows(array("conditions"=>array("cat_name"=>$cat_name), "returnType"=>"count"));
		return $check;
	}

	function uploadBusinessCSVData($fp)
    {
        $count=0;
      
        $handle = fopen($fp,'r') or die("can't open file");        
        $usertest = array();
        while (($csv_line = fgetcsv($handle, 0, ",")) !== FALSE) {
  
			// echo "<pre>";
			// print_r($csv_line);
			// echo "</pre>";
			// echo "<br>".$csv_line[0]. " - ".$csv_line[1]. " - ".$csv_line[2]. " - ".$csv_line[3]. " - ".$csv_line[4]. " - ".$csv_line[5]. " - ".$csv_line[6]. " - ".$csv_line[7];			
            $count++;


            // continue;            
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
           // if(empty($csv_line[1]) && empty($csv_line[2])){
            	// 	continue;
            	// }

            
             
			  	$insert_csv = array();             
				$insert_csv['businessID'] = $csv_line[0];
				$insert_csv['Company_Name'] = $csv_line[1];
				$insert_csv['Company_Number'] = $csv_line[2];
				$insert_csv['Address'] = $csv_line[3];
				$insert_csv['Category'] = $csv_line[4];
				$insert_csv['Phone_Number'] = $csv_line[5];
				$insert_csv['status'] = $csv_line[6];
				$insert_csv['lat'] = $csv_line[7];
				$insert_csv['long'] = $csv_line[8];
          
  
            $data = array(
				'user_firstname' => '',
				'user_lastname' => '',
				'user_email' => '',
				'user_phone' => '',
				'user_address' =>'',
				'user_password' => '',
				'user_gender' => '',
				'user_type' => 'Professional',
				'auth_check' => '',
				'sms_check' => '',
				'user_language' => '',  
                'businessID' => $insert_csv['businessID'],
				'user_company_name' => $insert_csv['Company_Name'],
				'user_company_number' => $insert_csv['Company_Number'],
				'user_company_address' => $insert_csv['Address'],
				'status' => $insert_csv['status'],
				'user_lat' => $insert_csv['lat'],
				'user_long' => $insert_csv['long'],
			);
			  
 
			//'Phone_Number' => $insert_csv['Phone_Number'],
			//'Category' => $insert_csv['Category'],

			$business_phone = explode("::", $insert_csv['Phone_Number']);
			$business_Category = explode("::", $insert_csv['Category']);			
			$insert = $this->Categories->insert($data);
		
		 	$dataprofile = array(
				'bs_user' => $insert,
				'bs_name' => $insert_csv['Company_Name'],
				'bs_comp_number' => $insert_csv['Company_Number'],
				'bs_email' => '',
				'bs_phone' => '',
				'bs_twitter' => '',
				'bs_website' => '',
				'bs_address' => $insert_csv['Address'],
				'bs_desc' => ''			
			);
			
			if($insert > 0){
				// if(!empty(strip_tags($this->input->post('user_categories'))))
				//     $business_category = explode(",", strip_tags($this->input->post('user_categories')));
				// if(count($business_category) > 0){
				//     $this->Categories->insertUserCategories(1, $business_category, $insert);
				// }
			
				$this->Categories->insert_profile($dataprofile);
				if(!empty($business_Category)){
					$this->insertCategories($insert, $business_Category);
				} 
				if(!empty($business_phone)){
					$this->insertPhonenumber($insert, $business_phone);
				}
				$data1['success']="success";
			}else{
				$data1['error_msg'] = 'Some problems occured, please try again.';			   
			}  			
          unset($data);
		}	
		 	
        fclose($handle) or die("can't close file");
        return $data1;
    }

    function CheckBusinessCSVBeforeUpload(){
    	$result = 0;
    	if(!empty($_FILES) && !empty($_FILES['userfile']['name'])){    		
		  	$config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
		  	$config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
		  	$config['max_size']    = '20480'; //The max size of the image in kb's
		  	$config['overwrite'] = TRUE; 
		  	$new_name = time().$_FILES["userfile"]['name'];
			$config['file_name'] = $new_name;
		  	$this->load->library('upload', $config); //Load the upload CI library
		  	if (!$this->upload->do_upload('userfile'))
			{							
				$error = $this->upload->display_errors();
				echo $error;	
			}else{
				$file_info = $this->upload->data();
				$file_name = $file_info['file_name']; 
			}			
			$result =  $this->uploadBusinessCSVData("./././assets/content_csv/$new_name");			
			$data["success"] = $result["success"];
			if(file_exists("./././assets/content_csv/$new_name")){
                unlink("./././assets/content_csv/$new_name");
            }            
    	}else{
            
        }
        redirect(base_url("Category/import_businesscsv"));
    }

    public function import_businesscsv(){
    	$data = array();
    	header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Import CSV File'); 
        $this->template->load('admin_layout', 'contents' , 'import_businesscsv', $data);
    }

	// public function import_businesscsv()
	// {
	// 	$data = array();
	// 	$uploaded_file = "";
	// 	$csvs = array();
	// 	$result= "";
	// 	$cat_count = 0;

	// 	if($this->input->post('ImportSubmit'))
	// 	{
	// 	  	$config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
	// 	  	$config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
	// 	  	$config['max_size']    = '20480'; //The max size of the image in kb's
	// 	  	$config['overwrite'] = TRUE; 
	// 	  	$new_name = time().$_FILES["userfile"]['name'];
	// 		$config['file_name'] = $new_name;
	// 	  	$this->load->library('upload', $config); //Load the upload CI library
	// 	  	if (!$this->upload->do_upload('userfile'))
	// 		{							
	// 			$error = $this->upload->display_errors();
	// 			echo $error;	
	// 		}else{
	// 			$file_info = $this->upload->data();
	// 			$file_name = $file_info['file_name']; 
	// 		}           
	// 		$result =  $this->uploadBusinessCSVData("./././assets/content_csv/$new_name");
	// 		$data["success"] = $result["success"];
	// 		if(file_exists("./././assets/content_csv/$new_name")){
 //                unlink("./././assets/content_csv/$new_name");
 //            }            
	// 	}else{
 //            $data["error"]="File not found";
 //        }

	// 	if($result == 1)
	// 	{
	// 	  $data['import_done'] = "Busienss profiles are uploaded successfully";
	// 	}
	// 	else
	// 	{
	// 	  $data['import_done'] = "Busienss profiles are not uploaded, Please try again";
	// 	}
 //        $data['import_type']="Categories"; 
 //        header("Access-Control-Allow-Origin: *");
 //        $this->template->set('title', 'Import CSV File'); 
 //        $this->template->load('admin_layout', 'contents' , 'import_businesscsv', $data);		 
	// }

	function insertPhonenumber($user= NULL, $params = array()){
		if(!empty($params)){

			foreach($params as $pt){
				$business_phone2 = explode("==", $pt);
				$arr = array("phn_user"=>$user,
							 "phn_number"=> $business_phone2[1],
							 "phn_label"=> $business_phone2[0],
			);
			//print_r($arr);
	
				$this->db->insert("otd_user_phone", $arr);
			}
		
		}        
	}

	function insertCategories($user= NULL, $params = array()){
		if(!empty($params)){
			foreach($params as $pt){

				$checkcat = $this->check_cat($pt);

				if($checkcat > 0){
					$catdetails = $this->Categories->getRows(array("conditions"=>array("cat_name"=>$pt), "returnType"=>"single"));
					$catid = $catdetails['cat_id'];
					$usrarr = array("user_id"=>$user,
									"category_name"=> $pt,
									"category_id"=> $catid,
								    "type" =>1,
								);

		  			$this->Categories->insertUsercategories($usrarr);
				}else{
					$data = array(  'cat_name' => $pt,
									'cat_description' => $pt,
									'cat_parent' => 0,
									'cat_img_path' => 'default.png',
									'cat_homedisplay' => 0,
									'cat_status' => 1,
									'cat_by_user' => 0,
									'type' => 1,
									'cat_hometabdisplay' => 0,
									'cat_hometab_id' => 0								
								);

					$catid = $this->Categories->insertTable(1, $data);
					$usrarr = array("user_id"=>$user,
									"category_name"=> $pt,
									"category_id"=> $catid,
								    "type" =>1,
								);
		  			$chk = $this->Categories->insertUsercategories($usrarr);
				}

				// $business_cat = explode(",", $pt);
				// $arr = array("cat_by_user"=>$user,
				// 			 "cat_name"=> $business_cat[0],
				// 			 "cat_description"=> $business_cat[1],
				// 			 "type"=>1
				// 			);
				// if(empty($business_cat[1]))
				// {
				// 	if($business_cat[1] == 'produits')
				// 	{
				// 		$arr['type'] = 1;
				// 	}
				// 	else {
				// 		$arr['type'] = 2;
				// 	}
				// }else
				// {
				// 	$arr['type'] = 3;
				// }

				// $cat_id=$this->db->isCat($business_cat[0], $params);
				// $usrarr = array("user_id"=>$user,
				// 				"cat_name"=> $business_cat[0],
				// 				"category_id"=> $cat_id,
				// 			    "Type" =>$arr['type'],
				// );
			 //  	$this->db->insert("otd_user_business_category", $usrarr);
			}		
		}        
	}
	public function csvpro_registration()
	{
            $data = array();
            $data['error_msg']=""; 
           // $userData = array();
           /* $userData = array(
                    'user_firstname' => '',
                    'user_lastname' => '',
                    'user_email' => '',
                    'user_phone' => '',
                    'user_address' =>'',
                    'user_password' => '',
                    'user_gender' => '',
                    'user_type' => 'Professional',
                    'auth_check' => '',
                    'sms_check' => '',
                    'user_language' => '',                    
                    'user_company_name' => '',
                    'user_company_address' => '',
					'user_company_number' => '',
					'user_lat' => '',
                    'user_long' => '',
                );
  */
             //   $latlong = $this->User->get_lat_long($userData['user_company_address']);
              //  $userData['user_lat'] = $latlong['lat'];
               // $userData['user_long'] = $latlong['lng'];

               
                $insert = $this->Categories->insert($userData);

                if($insert > 0){
                    // if(!empty(strip_tags($this->input->post('user_categories'))))
                    //     $business_category = explode(",", strip_tags($this->input->post('user_categories')));
                    // if(count($business_category) > 0){
                    //     $this->Categories->insertUserCategories(1, $business_category, $insert);
                    // }
                   
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                   
                }
                        
                   
      
	 }
}

?>