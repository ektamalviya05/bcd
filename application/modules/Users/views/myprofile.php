<div class="tab-pane  active" id="myprofile">
    <div class="row">
        <div class="col-md-3">
            <div class="faq-left">
                <div class="social-profile"> 
                    <?php if(empty($user_detail['user_profile_pic'])){?>
                    <img class="img-fluid" src="assets_new/images/avatar.png" alt=""> 
                    <?php } else{ ?>
                        <img class="img-fluid" src="uploads/profile/<?php echo $user_detail['user_profile_pic'];?>" alt=""> 
                   <?php }?>
                </div>
                <div class="card-block">
                    <h4><?php echo !empty($user_detail['user_firstname'])?$user_detail['user_firstname'].' '.$user_detail['user_lastname']:"";?></h4>
<!--                    <h5>Plumber</h5>-->
                    <div class="faq-profile-btn">
                        <button type="button" class="faq-profBtn">Follows</button>
                        <button type="button" class="faq-profBtn">Message</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="general-info">
                <div class="card-header">
                    <h5 class="card-header-text">Profile Information</h5>
                </div>
                <div class="rrow">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="f_name">Full Name :</div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_firstname'])?$user_detail['user_firstname'].' '.$user_detail['user_lastname']:"";?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="f_name">Email :</div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_email'])?$user_detail['user_email']:"";?></div>
                            </div>
                        </div>
                    </div>													
                </div>

                <div class="rrow">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="f_name">Phone:</div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_phone'])?$user_detail['user_phone']:"";?></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="f_name">Website Url :</div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="f_nameOne"><a href="javascript:;"><?php echo !empty($user_detail['website'])?$user_detail['website']:"";?></a></div>
                            </div>
                        </div>
                    </div>													
                </div>

                <div class="rrow">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-5 col-sm-5">
                                <div class="f_name">Zip Code:</div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_zipcode'])?$user_detail['user_zipcode']:"";?></div>
                            </div>
                        </div>
                    </div>
                   
                </div>	
                <div class="rrow">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <div class="f_name">Address :</div>
                                </div>
                                <div class="col-md-9 col-sm-9">
                                    <div class="f_nameOne"><?php echo !empty($user_detail['user_address'])?$user_detail['user_address']:"";?></div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="general-info">
                        <div class="card-header">
                            <h5 class="card-header-text">About</h5>
                        </div>
                        <p>
                           <?php echo !empty($user_detail['about'])?$user_detail['about']:"";?>
                        </p>														
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
