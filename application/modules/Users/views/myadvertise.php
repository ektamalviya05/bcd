<div class="tab-pane" id="myad">
    <div class="my-add-tab">
        <div class="dash-counter">
            <div class="panel">
                <div class="panel-heading inner-tab-bar">
                    <ul class="nav panel-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab">Featured Ads</a></li>
                        <li><a href="#tab2" data-toggle="tab">Active Ads</a></li>
                        <li><a href="#tab3" data-toggle="tab">Favourite Ads</a></li>
                        <li><a href="#tab4" data-toggle="tab">Inactive Ads</a></li>
                        <li><a href="#tab5" data-toggle="tab">Expired Ads</a></li>
                    </ul>								                    
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1">
                            <div class="row">
                                <div class="rtin-main-cols col-md-4 col-sm-6 col-xs-12">
                                    <div class="rt-course-box">
                                        <div class="rtin-thumbnail hvr-bounce-to-right"> <img src="assets_new/images/img_add1.jpg" class="" alt=""> <a href="javascript:;" title=""><i class="fa fa-link" aria-hidden="true"></i></a>
                                            <div class="rtin-price">
                                                <div class="course-price"> <span class="price">FEATURED</span></div>
                                            </div>
                                        </div>
                                        <div class="rtin-content-wrap">
                                            <div class="rtin-content">
                                                <div class="rtin-author"><a href="javascript:;">Lorem Ipsum is simply</a></div>
                                                <h3 class="rtin-title"><a href="javascript:;" title="">xyz Property 2015</a></h3>
                                                <div class="btn_price"> <span class="btn_pr">$9,000.00</span> <a href="javascript:;" class="btn_dis">10% Off</a> </div>
                                                <div class="rtin-map"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> Chicago, IL, United States</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="rtin-main-cols col-md-4 col-sm-6 col-xs-12">
                                    <div class="rt-course-box">
                                        <div class="rtin-thumbnail hvr-bounce-to-right"> <img src="assets_new/images/img_add2.jpg" class="" alt=""> <a href="javascript:;" title=""><i class="fa fa-link" aria-hidden="true"></i></a>
                                            <div class="rtin-price">
                                                <div class="course-price"> <span class="price">FEATURED</span></div>
                                            </div>
                                        </div>
                                        <div class="rtin-content-wrap">
                                            <div class="rtin-content">
                                                <div class="rtin-author"><a href="javascript:;">Lorem Ipsum is simply</a></div>
                                                <h3 class="rtin-title"><a href="javascript:;" title="">xyz Property 2015</a></h3>
                                                <div class="btn_price"> <span class="btn_pr">$9,000.00</span> <a href="javascript:;" class="btn_dis">10% Off</a> </div>
                                                <div class="rtin-map"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> Chicago, IL, United States</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="rtin-main-cols col-md-4 col-sm-6 col-xs-12">
                                    <div class="rt-course-box">
                                        <div class="rtin-thumbnail hvr-bounce-to-right"> <img src="assets_new/images/img_add3.jpg" class="" alt=""> <a href="javascript:;" title=""><i class="fa fa-link" aria-hidden="true"></i></a>
                                            <div class="rtin-price">
                                                <div class="course-price"> <span class="price">FEATURED</span></div>
                                            </div>
                                        </div>
                                        <div class="rtin-content-wrap">
                                            <div class="rtin-content">
                                                <div class="rtin-author"><a href="javascript:;">Lorem Ipsum is simply</a></div>
                                                <h3 class="rtin-title"><a href="javascript:;" title="">xyz Property 2015</a></h3>
                                                <div class="btn_price"> <span class="btn_pr">$9,000.00</span> <a href="javascript:;" class="btn_dis">10% Off</a> </div>
                                                <div class="rtin-map"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> Chicago, IL, United States</span> </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab2">
                            <div class="row">
                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">					
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like">Active</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">							
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like">Active</button>
                                        </div>
                                    </div>
                                </div>		


                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like">Active</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like">Active</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like">Active</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab3">
                            <div class="row">
                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">					
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like"><i class="fa fa-heart-o"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">							
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like"><i class="fa fa-heart-o"></i></button>
                                        </div>
                                    </div>
                                </div>		


                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like"><i class="fa fa-heart-o"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like"><i class="fa fa-heart-o"></i></button>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default like"><i class="fa fa-heart-o"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab4">
                            <div class="row">
                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">					
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default inactive">Inactive</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">							
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default inactive">Inactive</button>
                                        </div>
                                    </div>
                                </div>		


                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default inactive">Inactive</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default inactive">Inactive</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i> Lorem Ipsum available</span>
                                            <button class="btn btn-default inactive">Inactive</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="tab-pane" id="tab5">
                            <div class="row">
                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">					
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="exp"><i class="fa fa-exclamation-triangle"></i>Expired 1 hour ago</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="featured-box listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <span class="feat-ad">FEATURED</span>
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">							
                                                <span class="discount-tab">$900 with Discount 10%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="exp"><i class="fa fa-exclamation-triangle"></i>Expired Today 1:00 AM</span>
                                        </div>
                                    </div>
                                </div>		


                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="exp"><i class="fa fa-exclamation-triangle"></i>Expired 1 day ago</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="exp"><i class="fa fa-exclamation-triangle"></i>Expired 3 day ago</span>
                                        </div>
                                    </div>
                                </div>

                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="assets_new/images/listing1.jpg">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <span class="pro-img"><img src="assets_new/images/profile.jpg"></span>
                                                <span class="discount-tab">Discount 30%</span>
                                            </div>
                                            <h3>Property</h3>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l">There are many variations of passages of Lorem Ipsum available..... </p>
                                        <div class="dsply-num">
                                            <span class="exp"><i class="fa fa-exclamation-triangle"></i>Expired 5 day ago</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>