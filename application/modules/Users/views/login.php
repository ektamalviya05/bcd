 <?php
              if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 $email  = "Email";
                                 $password = "Password";
                                 //$forget_password = "?";

                                 //$category_desc  = $categories['cat_description'];
                               } else {
                                 $email  = "البريد الإلكتروني";
                                 $password  = "كلمه السر";
                                 //$forget_password = "؟";
                                 //$category_desc  = $categories['cat_descriptiona'];
                               }
              ?> 
<div class="main-content">
    <div class="log-in">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-box log-in-form">
                        <h2 class="text-center"><?php echo $top_content[0]['Login']; ?></h2>
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-success message">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <?php echo $this->session->flashdata('success'); ?></div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-danger message">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <?php echo $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                        <form name="loginforms" id="loginforms" method="post" action="do_login">
                            <div class="f-login">
                                <input type="eamil" name="email" id="email" placeholder="<?php echo $email;?>">
                            </div>
                            <div class="f-login">
                                <input type="Password" name="password" id="password" placeholder="<?php echo $password;?>">
                            </div>
                            <div class="submit-login">
                                <input type="submit" name="" value="<?php echo $top_content['0']['Login']?>">
                            </div>
                            <div class="f-login">
                                <ul class="social">
                                     <li>
                                        <a href="<?php echo base_url();?>Users/google_login"><i class="fa fa-google-plus"></i></a>
                                    </li> 
                                     <li>
                                        <a href="<?php  echo $authUrl ; ?>"><i class="fa fa-facebook"></i></a>
                                    </li> 
                                </ul>
                                <ul class="log-rag">
                                    <li><a href="signup"><?php echo $top_content[0]['Sign_up']; ?></a></li>
                                    <li><a href="forgetpassword"><?php echo $top_content[0]['forget_password'];?></a></li>
                                </ul>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
