<div class="col-md-3 col-sm-4">
    <div class="dash-link-sub">
        <ul class="nav panel-tabs">
            <li class="active"><a href="#myprofile" data-toggle="tab"><i class="fa fa-user"></i> My Profile</a></li>
            <li><a href="#editprofile" data-toggle="tab"><i class="fa fa-pencil"></i> Edit Profile</a></li>
            <li><a href="#myad" data-toggle="tab"><i class="fa fa-address-card-o"></i> My Add</a></li>								
            <li><a href="#pricelisting" data-toggle="tab"><i class="fa fa-hourglass-end"></i>Price Listing</a></li>
            <li><a href="#enquiry" data-toggle="tab"><i class="fa fa-hourglass-end"></i> Enquiry</a></li>
            <li><a href="logout"><i class="fa fa-sign-out"></i> Logout</a></li>
        </ul>
    </div>
</div>