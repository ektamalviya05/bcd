<div class="tab-pane" id="editprofile">
    <div class="dash-counter">
        <div class="Schedule_main_one">
            <div class="users-main">
                <h2>Edit Profile</h2>
                <form class="form-horizontal"  method="post" name="editprofile" id="editprofiles"  enctype="multipart/form-data">
                  

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">First Name<span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_firstname" placeholder="First Name" name="user_firstname" value="<?php echo !empty($user_detail['user_firstname'])?$user_detail['user_firstname']:"";?>">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Last Name<span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_lastname" placeholder="Last Name" name="user_lastname" value="<?php echo !empty($user_detail['user_lastname'])?$user_detail['user_lastname']:"";?>">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                      <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Email :</label>
                                    <input type="text" class="form-control" id="user_email" placeholder="Email" name="user_email" value="<?php echo !empty($user_detail['user_email'])?$user_detail['user_email']:"";?>" readonly>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Password :</label>
                                    <input type="password" class="form-control" id="password" placeholder="" name="password" autocomplete="off">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Website url<span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="website" placeholder="" name="website" value="<?php echo !empty($user_detail['website'])?$user_detail['website']:"";?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Address <span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="autocomplete" placeholder="Address" name="user_address" value="<?php echo !empty($user_detail['user_address'])?$user_detail['user_address']:"";?>">
                                </div>
                            </div>
                        </div>
                    </div>
                     <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Phone Number<span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_phone" placeholder="Phone Number" name="user_phone" value="<?php echo !empty($user_detail['user_phone'])?$user_detail['user_phone']:"";?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="">Zip code<span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_zipcode" placeholder="Zip code" name="user_zipcode" value="<?php echo !empty($user_detail['user_zipcode'])?$user_detail['user_zipcode']:"";?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group box_txtAre">
                                <div class="col-md-12">
                                    <label for="">About<span style="color: #be2d63">*</span> :</label>
                                    <div class="te-area">
                                        <textarea maxlength="500" name="about" id="about"><?php echo !empty($user_detail['about'])?$user_detail['about']:"";?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="uploader-first">
                                        <div class="upload-sec">
                                            <span>Profile Picture :</span>
                                            <label>
                                                <input type="file" id="pictureprofiles" name="picture" class="multiple_input" >
                                                <span id="lblError" style="color: red;"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                       <?php if(empty($user_detail['user_profile_pic'])){?>
                                    <img  width="50" height="50" src="assets_new/images/avatar.png" alt=""> 
                    <?php } else{ ?>
                        <img width="50" height="50"  src="uploads/profile/<?php echo $user_detail['user_profile_pic'];?>" alt=""> 
                   <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
             
                    <div class="form-group">
                        <div class="col-sm-12 sbt-btn">															
                            <input type="submit" class="btn btn-default up_but" id="editprofilebutton" name="submit" value="Save">
                        </div>
                    </div>
                </form>
            </div>
        </div>										
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closebtn close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="center">Your Profile is Update Successfully</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="closebtn btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
