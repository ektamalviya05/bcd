<section class="single_event">
  <div class="container">    
    <div class="row">
      <?php
        foreach($details as $evnt){
      ?>
      <form method="post" id="editevent" action="<?php echo base_url("Users/editEvent/"); ?>" autocomplete="off" >
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label"><?php echo $labels['label35']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label35']; ?></span>
                </span>
              </label>
              <input type="text" name="ename" class="form-control" placeholder="Event Name" value="<?php echo $evnt['event_name']; ?>">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label36']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label36']; ?></span>
                </span>
              </label>
              <select name="eventtype" class="form-control">
                <option value="">Select Type</option>
                <?php
                if(!empty($eventTypeList)){
                  foreach($eventTypeList as $evttype){
                    ?>
                    <option value="<?php echo $evttype['evt_type_id']; ?>" <?php if($evnt['event_type_id'] == $evttype['evt_type_id']) echo "selected"; ?>><?php echo $evttype['evt_type']; ?></option>
                    <?php
                  }
                }
                ?>
              </select>
            </div>
          </div>
        </div>
        <div class='row publicationdiv'>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label"><?php echo $labels['label37']; ?>:<span class="tooltip">?<span class="tooltiptext"><?php echo $suggestion['label37']; ?></span></span></label>
              <input type="text" readonly name="pesdate" class="form-control edatepickerfuture" value="<?php echo $evnt['event_publish_start']; ?>" placeholder="Publishing Start Date">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label38']; ?>:<span class="tooltip">?<span class="tooltiptext"><?php echo $suggestion['label38']; ?></span></span></label>
              <input type="text" readonly name="peedate" class="form-control edatepickerfuture" value="<?php echo $evnt['event_publish_end']; ?>" placeholder="Publishing End Date">
            </div>
          </div>
        </div>        
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label"><?php echo $labels['label39']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label39']; ?></span>
                </span>
              </label>
              <input type="text" readonly name="esdate" class="form-control datetimepickerfuture" placeholder="Start Date" value="<?php echo date("Y-m-d H:i", strtotime($evnt['event_start_date'])); ?>">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label40']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label40']; ?></span>
                </span>
              </label>
              <input type="text" readonly name="eedate" class="form-control datetimepickerfuture" placeholder="End Date" value="<?php echo date("Y-m-d H:i", strtotime($evnt['event_end_date'])); ?>">
            </div>
          </div>                                                                    
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label41']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label41']; ?></span>
                </span>
              </label>
              <input type="number" min="1" name="noperson" class="form-control" placeholder="Number Of Persons" value="<?php echo $evnt['event_per_ltd']; ?>">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label42']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label42']; ?></span>
                </span>
              </label>
              <input type="text" name="eventprice" class="form-control" placeholder="Price Per Person" value="<?php echo $evnt['event_price']; ?>"/>                                                        
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label43']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label43']; ?></span>
                </span>
              </label>
              <input type="text" name="evtaddress" class="form-control citycomplete" onfocus="geolocate()" placeholder="Event Address" value="<?php echo $evnt['event_address']; ?>" />                    
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label confm-pass"><?php echo $labels['label59']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label45']; ?></span>
                </span>
              </label>
              <input type="file" multiple="true" name="evtfile[]" id="evtfile" class="form-control" />
              <div id="evtimgmsg"></div>
              <div id="evtimgpreview">
                <ul>
                  <?php
                  if(!empty($evnt['event_img1']) && file_exists("./././assets/img/events/".$evnt['event_img1'])){
                  ?>
                  <li>
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img1']; ?>" height="100px" width="100px" />
                  </li>
                  <?php
                  }
                  ?>
                  <?php
                  if(!empty($evnt['event_img2']) && file_exists("./././assets/img/events/".$evnt['event_img2'])){
                  ?>
                  <li>                    
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img2']; ?>" height="100px" width="100px"/>                      
                  </li>
                  <?php
                  }
                  ?>
                  <?php
                  if(!empty($evnt['event_img3']) && file_exists("./././assets/img/events/".$evnt['event_img3'])){
                  ?>
                  <li>
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img3']; ?>" height="100px" width="100px"/>                      
                  </li>
                  <?php
                  }
                  ?>
                  <?php
                  if(!empty($evnt['event_img4']) && file_exists("./././assets/img/events/".$evnt['event_img4'])){
                  ?>
                  <li>
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img4']; ?>" height="100px" width="100px"/>                     
                  </li>
                  <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label"><?php echo $labels['label44']; ?>:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label44']; ?></span>
                </span>
              </label>
              <textarea class="textarea-main" name="description" placeholder="Description"><?php echo $evnt['event_description']; ?></textarea>
            </div>
          </div>                        
          <div class="col-md-12"> 
            <button type="submit" class="buyopt upps"> <?php echo $labels['label63']; ?></button>
            <a href="<?php echo base_url("Users/myEvents/").$evnt['event_id']; ?>" class="btn btn-default cancelevent"> <i class="fa fa-arrow-left"></i> <?php echo $labels['label64']; ?></a>
            <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
            <div class="eventmsg"></div>
          </div>                      
        </div>                   
      </form>
      <?php
      }
      ?>
    </div>
  </div>
</section>
    