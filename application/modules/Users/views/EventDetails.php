<?php
 $txtcnt = $this->User->getTextContent(19);
?>
<section class="single_event">
	<div class="container">
		<?php 
		if(!empty($evnt)){
			$picpath = $evnt['user_profile_pic'];
			?>
			<div class="row">
				<div class="col-sm-9">
					<div class="slider">
						<div id="demo">      
				            <div id="Event-slider" class="event-slider owl-carousel">
				              <?php
				              if(!empty($evnt['event_img1']) && file_exists("./././assets/img/events/".$evnt['event_img1'])){
				              ?>

				              <div class="item">
				              	<div class="event-img">
				              		<a href="<?php echo base_url("assets/img/events/").$evnt['event_img1']; ?>" data-imagelightbox="demo" data-ilb2-caption="Caption 1">
				              			<img src="<?php echo base_url("assets/img/events/").$evnt['event_img1']; ?>" alt="The Last of us">
				              		</a>				              		
				              	</div>
				              </div>
				              <?php
				          		}
				              if(!empty($evnt['event_img2']) && file_exists("./././assets/img/events/".$evnt['event_img2'])){
				              ?>

				              <div class="item">
				              	<div class="event-img">
				              		<a href="<?php echo base_url("assets/img/events/").$evnt['event_img2']; ?>" data-imagelightbox="demo" data-ilb2-caption="Caption 1">
				              			<img src="<?php echo base_url("assets/img/events/").$evnt['event_img2']; ?>" alt="The Last of us">
				              		</a>
				              	</div>
				              </div>
				              <?php
				          		}				              
				              if(!empty($evnt['event_img3']) && file_exists("./././assets/img/events/".$evnt['event_img3'])){
				              ?>

				              <div class="item">
				              	<div class="event-img">
				              		<a href="<?php echo base_url("assets/img/events/").$evnt['event_img3']; ?>" data-imagelightbox="demo" data-ilb2-caption="Caption 1">
					              		<img src="<?php echo base_url("assets/img/events/").$evnt['event_img3']; ?>" alt="The Last of us">
					              	</a>
				              	</div>
				              </div>
				              <?php
				          		}				              
				              if(!empty($evnt['event_img4']) && file_exists("./././assets/img/events/".$evnt['event_img4'])){
				              ?>

				              <div class="item">
				              	<div class="event-img">
				              		<a href="<?php echo base_url("assets/img/events/").$evnt['event_img4']; ?>" data-imagelightbox="demo" data-ilb2-caption="Caption 1">
					              		<img src="<?php echo base_url("assets/img/events/").$evnt['event_img4']; ?>" alt="The Last of us">
					              	</a>
				              	</div>
				              </div>
				              <?php
				          		}
				              ?>

				             
				            </div>			         
				    	</div>
					</div>

					<div class="event-contant">
						<div class="event-share-logo">
							<div class="event-name">														
								<div class="print-evt">
									<ul>
										<li><a target="_blank" href="http://www.facebook.com/share.php?u=<?php echo base_url(uri_string()); ?>" style="background: #3b5998;"><i class="fa fa-facebook"></i> <?php echo $txtcnt['label10']; ?></a></li>
										<li><a target="_blank" href="https://twitter.com/share?url=<?php echo base_url(uri_string()); ?>" style="background: #55acee;"><i class="fa fa-twitter"></i> <?php echo $txtcnt['label11']; ?></a></li>
										<li><a target="_blank" href="https://plus.google.com/share?url=<?php echo base_url(uri_string()); ?>" style="background: #dd4b39;"><i class="fa fa-google-plus"></i> <?php echo $txtcnt['label12']; ?></a></li>
										<li>
											<button type="button" class="prntevt" atr-desc="<?php echo $evnt['opt_user_option_id']; ?>"> <i class="fa fa-print"></i> <?php echo $txtcnt['label13']; ?></button>
										</li>
									</ul>
								</div>
								<div class="event-type">
									<button type="button" class="evttypedisplay"><?php echo ucwords($evnt['evt_type']); ?></button>	
								</div>
								<h3><?php echo ucwords($evnt['event_name']); ?></h3>
							</div>			
						</div>

						<div class="event-details">						
							<div class="event-date">
								<i class="fa fa-calendar"></i>
								<h2><?php echo $txtcnt['label1']; ?></h2>
								<p>
									<span><?php echo date("d M, Y H:i", strtotime($evnt['event_start_date'])); ?></span>
									To
									<span><?php echo date("d M, Y H:i", strtotime($evnt['event_end_date'])); ?></span>
								</p>
							</div>	
						</div>

						<div class="event-details">						
							<div class="event-date">
								<i class="fa fa-map-marker"></i>
								<h2><?php echo $txtcnt['label2']; ?></h2>
								<p><?php echo $evnt['event_address']; ?></p>
	                            <!-- <a href="javascript:;" id="open-map-reltv"> <?php //echo $txtcnt['label3']; ?></a> -->
							</div>	
						</div>

						<div class="event-details">						
							<div class="event-date">
								<i class="fa fa-eur"></i>
								<h2><?php echo $txtcnt['label4']; ?></h2>
								<p><?php echo $evnt['event_price']; ?></p>
							</div>	
						</div>

						<div class="event-details">						
							<div class="event-date">
								<i class="fa fa-infofa fa-eur"></i>
								<h2><?php echo $txtcnt['label5']; ?></h2>
								<p style="text-align: justify;"><?php echo $evnt['event_description']; ?></p>
							</div>	
						</div>
	                    <div id="reletiv-map" class="embd-map">	                    	
	                    	<iframe
							  width="100%"
							  height="350"
							  frameborder="0" style="border:0"
							  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyBGs74N-gMM6cj6CzJRiKYO3g8qdumaETY&q=<?php echo urlencode($evnt['event_address']); ?>" allowfullscreen>
							</iframe>
	                    </div>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="right-sid_bar">
						<div class="profile-right">
							<div class="p-picture">
	                        <h4><?php if($evnt['user_type'] == "Professional"){
	                            	if(!empty($evnt['bs_name']))
	                              		echo ucwords($evnt['bs_name']);
	                              	else
	                              		echo ucwords($evnt['user_firstname']." ".$evnt['user_lastname']);
	                              }
	                            else
	                              echo ucwords($evnt['user_firstname']." ".$evnt['user_lastname']); ?></h4>
								<div class="img">
									<img src="<?php echo base_url("assets/profile_pics/$picpath"); ?>">
								</div>
	                            <div class="online-chat1">
	                            	<a href="<?php echo base_url('Users/user_profile/').$evnt['user_id']; ?>"><?php echo $txtcnt['label6']; ?></a>	                            	
	                            </div>
	                            <?php
	                            // this is for live or offline chat button
	                            if(!empty($this->session->userdata("user_id")) && $evnt['user_id'] != $this->session->userdata("user_id") && $evnt['is_online']){
	                            ?>
		                            <div class="online-chat1">
		                            	<button class="online-chat" modal-aria="<?php echo $evnt['user_id']; ?>"><i class="fa fa-comment"></i> <?php echo $txtcnt['label8']; ?></button>
		                            </div>
	                            <?php
	                        	}else if(!empty($this->session->userdata("user_id")) && $evnt['user_id'] != $this->session->userdata("user_id") && !$evnt['is_online']){
	                        		?>
	                        		<div class="online-chat1">
		                            	<button class="online-chat" modal-aria="<?php echo $evnt['user_id']; ?>"><i class="fa fa-comment"></i> <?php echo $txtcnt['label9']; ?></button>
		                            </div>
		                            <?php
	                        	}else if(empty($this->session->userdata("user_id")) && $evnt['is_online']){
	                        		?>
	                        		<div class="online-chat1">
		                            	<button data-dismiss="modal" data-toggle="modal" data-target="#myModal2"><i class="fa fa-comment"></i> <?php echo $txtcnt['label8']; ?></button>
		                            </div>
	                        		<?php
	                        	}else if(empty($this->session->userdata("user_id")) && !$evnt['is_online']){
	                        		?>
	                        		<div class="online-chat1">
		                            	<button data-dismiss="modal" data-toggle="modal" data-target="#myModal2"><i class="fa fa-comment"></i> <?php echo $txtcnt['label9']; ?></button>
		                            </div>
	                        		<?php
	                        	}

	                        	//this is for edit event only 
	                        	if(!empty($this->session->userdata("user_id")) && $evnt['created_by_user'] == $this->session->userdata("user_id") && $evnt['event_is_free']){
	                        		?>
	                        		<div class="online-chat1">
		                            	<a href="<?php echo base_url('Users/editEvent'); ?>"><i class="fa fa-pencil"></i> <?php echo $txtcnt['label7']; ?></a>
		                            </div>
		                            <?php
	                        	}
	                            ?>
								
							</div>

							<div class="google-add">
								<div class="add-img">
									<!-- <img src="http://votivephp.in/VotiveYellowPages/assets/img/android_work2.png"> -->
									Google adds coming soon.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div id="evt-content-div" style="display: none;">
				<body style="font-family: 'open Sans';font-size: 13px; line-height:20px;">
					<div style="padding: 0 10px;">
						<h1><img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAO4AAAA4CAYAAAD6icQQAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAGipJREFUeNrsnXuUFdWd7z+7qs6r3928kWd3A/JoogOJGsWMGYwxGvKYgUz0ZsZZMzaTyY1GGhYkcye5k5VZgRFIch/O0BPXuMwko3RcZoxejbZGwIhRm6iNRKRpFJqmAel393lV1b5/1D5SfbrOk4MBPN+1ap1TVbt2/WrX/u7fYz9KSCm5ELBp06ZMSWqA3nQJNm/eTBFFfBigXSRyfgXoAr5XfGVFFHHhE1cHfgg8CISAvweeVNq3iCI+tDDOk1lbCHN2EtACfCLp+KeB3wJfBNovIHmLKOKi1LihAmrCZcCrHqRNoB54EfhSge43F7isWB2K+DASdxvwrjJtz4UEtwF7gFkZ0pUBDwH/rEzqfNAA/DtwSOWlF6tEEZesqeyBW4Cvqv93q/8/Ae4FDuaQT40i5DdyvP8C4EAO6a8BNgGrXMeuU8f+qVgtivgwEHeq0lpu+IG/VtvDisBtAGwPpsjmG+B09zTnLUn6vBP+8aY0Jvg/As8Be4tVo4hLnbj/jhNISsawIsBBdd4PxFJlsnnyDz+I5z0O/Aw4qrRufdJ5HfgpcCUwUKweRVyqxL1baTGADkXUF9R2oEAyNiiS9QJHVINwMM+82tWW0OqTVN7ubS7wv4G/KFaPIi5Y4ubSVZLUNeIHKoFbFWF7CyjXp4E7gE+m0OYxYBfwFHD/OWjH08BjanP7vwkCH0kczLVLqdiNVMQHpXHLlDbLFjHguwWW5ws4o6MWZUjnB25U27eVD70ZsAogw948fdxcy6+IIgpC3IeAjylN9gLwjJe523z1fdRm7Klx0LhmXS6VfgdOV1CuqFRk/zSwGuhJTlD7/Ky8C6j56vtofOnvvE4twIlEf0L9VgKTC9R4FFFEVsStURVfB/5MbQlTsllps5y1SfPO7X7lox5tXLPudIpkIeBxUkd6s8V1wNPAVUA4DcnrlT+er3n9VZzos5cJf41q9NJCCDFmX24LrAY24gw8AScCv0U0RVsuhUomtwXen8kimqLiIpB3o6rzAJtEU3RLqrT169vdacdRANjUsbWh73wR9zbGDz7YpUzQJ/LQgHfgjGq6xkVilM/YrvzSpxrXrIspTfuJAj1PA/Bj4HZ1zxvVs33Mw/w+CPxcFe7RHO7xL8DLOOOmv5B07pZsiJtUSRpVGbixDNgptwXWiqZoc4qKlSDClqL+uWDRqN7l8vNJXJSZ9zCwnUS/qwf+7ZZPvf//zieedp+6W5mtZSkunau2VUDvb1//7E+u+sgvv5JSOt0P874IU66E0ikw+h6c2geHHoN4SgPgtreOXPXq7le+9HeM7+5JNnX/Xm3bge/kYFW04YyVXoDTL/wV1fDdAnwzB9Ku9CCtGzvktkCnaIq2Jh13t/BF4l7YWFa/vr2xY2tDc6GJO1dpox+pCnw0j3x0nAkBX8jhmpqZ0966O+XZ+s/BDVuhfOb4cytOwq6N8NZDnpeWBAe3AbmYZOtwAl1/otyDbHEQ+CucIN0mZWnMyqEMN2aRZgdQV6z/F73mLShxNaVlZ+MMLzqaZz47ciQtJcEhpk06nJq0qx72Ji042vczD8CSOzxPz5x6UPh9kXzM7P8iv/HKR4C1QC1OxDsbbbsMWJlF0lqlmYu4uLVudaE1bs5kTTKPG5SmGYeq2VXMu3kBwaoQdtyi6+VjvLvnHQAmVh9DCNsjcqPDJ3+QnSCf2AJvPQxmOCn4YzO55ihdJ+cDMH35ZcxZMRc9YGBFTY7+5l26Xj5GiuDSbTjjrPPB8RzSrk7a71Rau1qZwtVJaVuL9f+DgYob5OOCNCvLM6FlVyfFLQr2Do3kgQLzNuxPTnMD8JfAskc27A+y4nqpzNAe4NG79r54dcA0x2gpoQmu+vrHWfrlj4zJaPHqBnoPn+GppieoKk9hkU5YCGXTs5M+UAlTl0PXnvGNRkUPJwcXs/L7NzHz6rHdQUu+tJSul4/R+q1fERseOwpTwvqtK64fUQReAuj3btgP/DcLeAd4BNhJUlT60L1Lci17txbtA5aLpmif0sZtON1x1clp3RFa935ytFZp9I2q8nSqStWcuIcryDUueup1PPmYknkzTvR7iyvP1ep4raqom7IIzm1U6duAtaIp2ub1jArLVfkn8l+jju9U5dSnZG5Ocb+NilS1imTNyTGEXKLKyY1vx9aGVoD69e1tSp5qN3Hr17e7n6UmIXfH1oYx709Fq8fImcg7YSqnwjxVeZ5TxF2igj3z1O91wLZRn+9Pky9cvLphHGnfl7RuAp/50Sr8/hSmrBXLrfqb3vmEgsOs2PTH40ibwIyPzeTaphUeza1Yatj2I8CfquBTvdoWADcpArRztsssZ8htgWrOdv2gKn+fq8VvS2rxa9U12ea/Gmc+c6LFr1UV8ZkCNfiJoFq1R7AtQSp3unSk3eFKvwx4RjU66dwyd/6b1bFE41atgnq1Hvd71dWoJCyZZ1R5FRSqCyhTd95OL3epfn27p5z169tXZyLucuClbHywUDw+Lo8lqxvSXlM1u4qyqSkCz31vO1s2GO6Gk/s8T/mDNvU3zUt7ef1N8/GFfGN5KyUl8XimO89UL+Vb+fo8HiYWGY4ty6FRSEWWZe6upHMkbrbBtmVp5PTq/0x1PFV+qz3cjnGuiHruVGW4I5eGMQd05lqOStOmlDPhK3sRdwrwCzxWs5hUbrBkRgifflarR43x8xSEljmga+mVqU8+87XMmlda8Oxdzq8HYnbm9yA0gdDHyxrVz1r+C6cHmVGTMt70T8CX8whoubVBm1vburRuH2O75GpziGCme/iN56OS5hBsy0bOlV4aMwWqsyTFxgx5NF4gLnZWcnoR929JWsFi9kQ/D9w5hxe/fTmP3l3Hgc2LeeDOOUyv8jEYCIzL4HBrR1rJRs+M0t3hS52gaw/svBF6XvE+/95+eOQWOPx4yixO91TQ/Wr6WNGJ33WP83EjhoFe6mf7bTM4sHkxj91Tz6+/OZ/H7qnnytklXtl8Kw+z2V3Z2tKka/O4JjnA0Zp0bHWGVr86R4Llq4X71JZtcK41w/lcUZvkOlRnKJfVH0CZpLcAHVM4Kzm9iDtmHaeSgMZP/3Yu184fa9peO7+MR79Rx8Dk8Q3eaw/u48Tvur014XCM577zDD3dU9M/xYnfws9WOAR+9i5443547h6HsA8uh6PPp7285725PP+9Z+l/t9/zfO/hM/z6H8cH+U5UVrLza7V89sqqMZbFwulBHlw7h7mTxjVUC4EV58mc6vTQxDcm7yeOKS3lNrOaRVO0jvEjdwpF3E4XOZODbXVq68vC5G1Vz9CchYXRhtP1loxNSeSvzRAIrEvyQZfloOWzIWGyBZLuPbemkrNja8M4OevXt9d6zcctd+98akkFUyq9tWNNqUHdDbVw6J1x5HzirseoWzmPupX1lExwNNWxl47y+1+8yXDPMFDJ8Gg1ZSUZhnF27fGMGqdD78A0ItEy6Bnm0TtamH/r5dR+sg5fyMfomVGO/uZdDj15kHh4vC+rNVzGgmneK2kEfRq3X1PD9x474T6sA6UXiJlVm/TiNyWCXXJbYIvLDCtEBW0RTdE1KayILa4I+ZZkn9WjX7rF9duYQcZNoinaqrToShfxt6ho/MoM5bLFFbVem6Rpa7PwS9Nq2Pr17alM3lT5Lu/Y2tDmJafr+Dg5vYjbB8zIVlJ7ejWdNTXU9o6djmvHbQ49eZBDT6ae897ZtZSl83cVvPYePnrl+//j4ThvtrTzZkvmlVz7QiH66qblejsTGLxAiJusxfqSWvWNBdS4m9LdO4mUmSYnVysyV2fSuK6um1Y3cRPn5LZAJpO11R1HkNsC7nzOta91ZYqy7XSRcEwAMum4p5wdWxv66te3j5HTy1T+L/fO0/sH6e73jrIORSweebWf52rrsETukz46XQQrJI50Lc3ruudq63j+cIQjp6PevnnU5qd7e73MxVfOkx90LgRrS1HhC4E20RRNqZnc/bAp0iX7V4muqp15BONyQnIfMR/MwJaWDCbyOHgQfUxaL+I+BMTdlXXVDzp46o2xSuWNY2HuaH6HkwNx+kIhXp0xI+enOdU7ixOnCzsM91jP5fQPTc69EampobOmhrgl+fyPDvPE6wPjnvfzPzrsReoHGbuCRq6kWpalBm3jwsG5ylLLhwd9pB6nnHc5epnKbwIPAHcmDgyMWnz9J0fRNcHC6UE6T0cZjY4drvjirNnM7e1j8khu03ZffuMWPvcn/6tgpbT/7etzvmbU5+NX8+aN0azf+I9jNP2sK+XzJtoJ4L48TOW+ZDPRY/ROsunYdy7kKHAXUFo/UG4LVHt1cV0IjZDcFqhNsgKqzzNpb+zY2tCZQqt2pglu1Sadr86kcROO9bhREJYt2d8V9qzEpqbxi0WLiBi5rT938swcjp5YVJBSOtN/Gcd6Ls/tRQrBYwsXMewf7xule16Fr+ZBKC9zbWOKd/B+BfC4JlODsDKN2d2Zgei1eVbScfdKMSopucyalc+cvJ2PhiZduZzrhPdWl+xrgboUvu05y6mleQm3kuMEhIFgkEcXL8bUcvtAwivtNyPluX9UYe9rq3K+5sn58zlWWZnP7e4k90UGUvksK+W2wA5XZd+YKlCRw4uvTSJNYwbirpbbAtVKM+fTp9mW4l6rs2i4OkVTNDHmuUU1VFvOE3Ebk6yaZdlaEvXr21cmtlTvtGNrwxa1Neex8oWnnOp+Y+RMpx4PATfjrBKxMNs7d1VU8otFi/jCm2+iZ/nt3TP9l3H42BUn6mftm5bvm+ntn364+9S8nBzmZ+vqeXPylFxvFcNZMODHBWid3RWgUVWkPg+/tzWN6bcTWCmaojUeUdXEUL6VWTQE1Tjjm/M1HzuTGqKNqYjrCtgkzm1UXTmdKkC1LMUCAoUo62WqzFoZG+3uy+J+7rHe52MJnjFy1q9v95SzY2tDayY1dwD4OM6qGFnjSHUNv1y4KGYJkfXCaa/uv/nH5L/Ea6yza+k3c7nghdlzTu2bPj3X+3QAfwz8awFeUnMKc3WZh/WTbrB68mibliQy7vAgT0sKX7P2HAJHyXltJn03UGuSnM8Ah13Pv6PAhEgusx1pyu0PhazlzMY+7Qf+HGelh5FsJTg0YYJ84vKFT2dLxsHhCT8m928GJfDd5UueasnStI8Bf7531qxcZ9o/gPOFg4J8nkQFb7IxB7d4BHpac2wQxlzrCs4UMkjUkouPqKbddX4QbFCmeWsW8v9BofzhrOTUcqy4S9WFWdnABydOPKk0diZC7W1cs+4ozgT2h3N83jZXy/6zDGmHgU+pe9hZ5n8AZ42sv6Lw6yZvIfNY5eYMZmlyJW3NUAk3JTUerVkEjwrZEHnKkuO5fJAuv5YC93OfNzkTc3JzjQh1btizew3O2NynMxBYArJxzbqDOGNln0qT9n7X/7U5BMViilAJkzwd6V9TGjMxVCtTF85hJcsfAb88T5qgD2cieFsK0t6YolulOQPR1noQsg9Y4xEY2pJlY5Gt+d/mIUuq529Jcb7gS9Oq517rUVYt6WT8A2ndjHLm9e2gDXt2/wa46d4V198A/E9FZOFBqjCAWlP55uad2zfhrALpngZ3OklTDuB8t+dZMk+X+zqur9I3rln3WvPO7bsYv9zrvwBNjF1v+WSKPN8BfgD864Y9u2MA9664/ry9KGW2LlcR4IR/2Zmu4qqxx3Uu37XZo0G40RU17VNaxWv6YKvcFljuCoo0u65xm9NtLm3QlqYhWq4myFfjjLBqTdeHLJqizWrY4WqXKd+WhRZKJc+mLO6V6CNvTdHNlkveGcslV0uiY2tDs2uIYzVOtHrsSDiZFPn1WLommbTjjt274vqP4kwH/BwwQR0+Adx/aNryfxjTJO/cfh3O6KzE1MHvNq5Z9x2PW30/w0PeD/zNuCZ/5/ZVnB22OaDS/NxD5vs4+03fGM7HtHcAj2/YszuclDazT5/70jVFFJE3CvJF+g17dr+yYc/uv8ZZ3uUOpS1bcRYOH4PGNeteABbjrGO8F2dZWC/8jzTmdRvg+V2QxjXrHuPsN3kXN65Z9/MUeewC9uEsrbp0w57dKzfs2d2STNoiirgQMU7jZnRirr4v67Q5fDsoFSqVJmxIMq2vJIcVFdVXFPJGim8HFVHExa1xzyMGcAaBHHft30puy6AWUURR48LZj1adOtGJlBJN6AhNQ9oWlmUyNDxKZWUVMt5L3DLQ9BBgoetg+KqwrCjYo+hWFzoD6JpEw8SMR7H1iUitCs3qJhwOI4ILmfLIdQuADcD2M7d3HIibEk1YiJGnGTqzj/LJNzBiXEGpvxywQMYRmoG0LYSmYVuSvr5eAgE/ZeXlSNum90wvlzfk/kmXfMqriCIuKOJ2H30bw2eABKFpgAApiUYjxOImmtCQaGi6DzM2QlVZFD1+gKG+Diqq6xBGBaasYiTqp6Rs+oKek+9VGIZGSUkIKx7G5+OKwaG44QuUCF0DDA0DHdu2kVIyMhp/vSQUihvaezIc6SJk98dmNny1HZCnTnRJQRyh61imRTwWQQgdv99geNgZRzJ3XkORuEV8+Ih75NB+dF3g8/nx+fzYtonQdDShEYlZ+LQwAXG8zIr1Xw7WbNMOThbS/EjUDE42SufPQQTrTMuqMONRystK6OvrJxAMEgoFkbZJNBolFreoqChH2hJdN4gNd6IZIfBNRBMCW0psGwzdh8EJZOQQmu4jJqdGTG3GWzbasdHhvtORcPh1fyDQK237UNfxE50rb/rse2Q5kKRI3CIuOeJKKQkGg/h8BrZtz5WSj5uWda2hWx+tkC8sjWm1fktMxtYqkNIATLBjgIZmGAwMDFASDGD4AvT391NRUQYygtBCDA2PUFpSgpTOFLugz8IcfRctMBmpV6HrGgnxhQChBYjHYmgM4pfHMWQPYfsypG8usTjYVgzLtkBKyspKsWxeC4+OvGFa1m8ikdiLDVd87Pdk+DB1kbhFXPw+bvc7jIbD9bou/sLv9/2ZrhsLHTPWIMjbmLEwEeMK4pFBAsEAfr8fJEhpY9sWti05NngYAmEmBWdSFZyOZcWwzTCWrWPZEl9QIx4xEcKHT/RjRc6ghWYhtABCaVwB2NLGMHxYZtz5/pC0IX4Cn9aPxiCWqMHWazHtEgQRBBLNMBgZHsG2JaYZp6S0pCsWjbX0nDz10DXXfbINZ1ikLBK3iEuGuKe7j4Ri8ejmSCT+tcqqKl1Ky6nUEoQmKYnvYVi7CtvS6O3rZ+KkiWjKD5a2jdAEmmbQG+khbPcTiJejmyVoPp2AzyAYCmFbpsMaIdB1P2Z0EOxh0KvQNN05J22QFhKBP1BGPB5BSoEV6UGzh0ALovkq8Bth7IH/x0i8En/1rY65rRZTF5pGPB5H1w0ENgMDQwihtTz5q6fv+e93NfW4tXCRuEVc1MQ9efzwveFweL3f58MfDCCEgW2bWHaAUrEfyxwhoi1jaOgMJaEQPp8BCITQkNJG03THvrUlIOkfHGZkJIxtxwkEgui6hqYJKivK0TSdeDyGYfjRdR+WFUPifHtMyCh2+CjoJfiCEzHjw/QPa5QEfQQDBiNDZ5AigL9kIphn8MljBPURhvUVCGkiNOlYEPIsKUdGRigrK2VoaPTJugUNX8aZXGAViVvEhQIj3wtHR8O7SstK1w8NDmFZNrpPxzACyOgBouFd2JW3E4uNEgwECQRDyuCU2HZCeUksM45EIIDq6ioqK8scUxfQdZ8iJ0QiIwgpGDDfwxAhygI12NJGAOGIiSHK8RklWLZGLC5BSkIlldh2FCM0kWgkxshQH2XlVdj6NAz5a/xiiJgZxLYkmga6boBtoht+qqp8xOIx3jr41j6cdaYjmXzfIor4IJH3AIy585Y89dJLL9+o6/rrtpREoxZopQQ4jskkpCgh4DcIhoLYqn93NBxleHiUkXCU3qFRhodHCIcjSCmxrLhjZgvdMYOljbRMACwzBkIyHBuge+RdekYPEjH7QdoYPj9Ru4pI3IcQkkjMTygUwpYW2GH8BlRX11BVVYmuAdIkbhnY5iiarmEYuuMr25JwJMp7p09z6r3Tnf/5n4+su2XV6oedaBpFNVvEJROcEjhfX6/cuOGexZ/7zNVfnD5Ju25CZXRpxLhOA4Ftm2iahm1bjmmMQEoLU/MzFLMol1E09YEtTXNMaBAITVeEjWMYPoQmsE2L3tGTnBw5goVFpTGVKWVzkQKENDFNm4rKKmzLQmIihA5oxKJhJI5pblsWmu6j3HqOiHEVtlbuRKMRDPT3H3y3q+vFxx9/8pntP/w/vwdGcUZqDSqNK4umchGXAnET5DUUgUNAqKa6quw73/6HZYsWzfvIxAkT60Kh4ORAIDChrLS0DvCZZhy/PwBSYkmJbTnRZZ/PGViBAIEJaAhhMDA4RHl5GT4jSH+km1H60SyDUl8N5aGpmLFRJxhlWeiGjhAaINEEmKZJJBJB0xxzPBqLE7fMg+WxXw2ejn+0+/SZ8LGDbx9q//7me1/v7j4xytmpiIktmqxxi8Qt4qIn7kjn/0XqAeJ2iOq5t+uKxAbgU2T2q/8GoH90+R9VfPHzq+pmzJwxpbq6cuK0qVPnCfBrhh4qKymdLQRouk8YmlUVCAanxOI2IyMjTJgwEdO0lEbWMU0bTZcEA0HC4XBvOBI7Le0ogjgjEboxrQF7JEp3/5nf9/b1DnQe7ux6fs8Lx5/ftadP+aqWImRii6ktrvbjeHQFFYlbxCVB3HeOH2Gq/QQBrZeIVcWg/hkQAaZMnymU/6yrTY2HfP+/of4niK650guX/+3eTwWpSGa59i11zFZEtFy/7nO261orG1+2SNwiLnri7n30KUJVIaKX1THXt48q3wni+kzCYh6mHUAIDWENIuNnEP5JRGSQWTPmCBdRhetXJAXLNLJfAlO6NjeZpeu/DdinDvxOhvyvMGBcy4w5i3N+9iJxi7gQ8P8HAAApjZTbFMI+AAAAAElFTkSuQmCC"/> </h1><hr/>
					</div>
				    <div style="padding: 0 10px;">
				        <div style="max-width:750px;width:85%;padding:10px;margin:30px auto;border:1px solid #e2e2e2;overflow:hidden;">
				            <div style="padding:10px;border:2px solid #fff;float: left;">
				            	<?php
					            if(!empty($evnt['event_img1']) && file_exists("./././assets/img/events/".$evnt['event_img1'])){
					            ?>
					                <div style="width: 100%;padding: 8px 0;float:left;border-bottom: 1px solid #ccc">					                    	
	 				                    	<?php
					                    	$path = base_url("assets/img/events/").$evnt['event_img1'];
											$type = pathinfo($path, PATHINFO_EXTENSION);
											$data = file_get_contents($path);
											$base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
					                    	?>
					                    	<img src="<?php echo $base64; ?>" style="width:100%;min-height:352px;" alt="eventimage"/>
					                </div>
					            <?php
					        	}
					            ?>
				                <div style="display:block;overflow:hidden;float: left;">
				                    <h1 style="margin:0 0 15px;"><?php echo ucwords($evnt['event_name']); ?></h1>
				                </div>

				                <div style="width: 100%;padding: 8px 0;float:left;border-bottom: 1px solid #ccc">
				                    <label style="width: 50%;float: left;font-size: 14px;font-weight: 600;">Type d'évènement</label><span style="width: 50%;float: left;font-size: 14px;"><?php echo ucwords($evnt['evt_type']); ?></span>
				                </div>

				                <div style="width: 100%;padding: 8px 0;float:left;border-bottom: 1px solid #ccc">
				                    <label style="width: 50%;float: left;font-size: 14px;font-weight: 600;"><?php echo $txtcnt['label1']; ?></label><span style="width: 50%;float: left;font-size: 14px;"><?php echo date("d M, Y H:i", strtotime($evnt['event_start_date'])); ?> To <?php echo date("d M, Y H:i", strtotime($evnt['event_end_date'])); ?></span>
				                </div>

				                <div style="width: 100%;padding: 8px 0;float:left;border-bottom: 1px solid #ccc">
				                    <label style="width: 50%;float: left;font-size: 14px;font-weight: 600;"><?php echo $txtcnt['label2']; ?></label><span style="width: 50%;float: left;font-size: 14px;"><?php echo $evnt['event_address']; ?></span>
				                </div>

				                <div style="width: 100%;padding: 8px 0;float:left;border-bottom: 1px solid #ccc">
				                    <label style="width: 50%;float: left;font-size: 14px;font-weight: 600;"><?php echo $txtcnt['label4']; ?></label><span style="width: 50%;float: left;font-size: 14px;"><?php echo $evnt['event_price']; ?></span>
				                </div>

				                <div style="width: 100%;padding: 8px 0;float:left;">
				                    <label style="width: 100%;float: left;font-size: 14px;font-weight: 600;margin-bottom: 10px;"><?php echo $txtcnt['label5']; ?></label><span style="width: 100%;float: left;font-size: 14px;"><?php echo $evnt['event_description']; ?></span>
				                </div>
				            </div>
				        </div>
				    </div>
				</body>
			</div>
			<?php			
		}else{
			?>
			<div class="row">
				<div class="col-sm-9">
					<p>Sorry, Event is removed or deactivated by user. <a href="<?php echo base_url(); ?>">Go To Home Page</a></p>
				</div>
				<div class="col-sm-3">
					<div class="right-sid_bar">
						<div class="profile-right">
							<div class="google-add">
								<div class="add-img">
									<img src="http://votivephp.in/VotiveYellowPages/assets/img/android_work2.png">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
		}
		?>
	</div>
</section>
