<section>
  <div class="usr-map-section">
    <div class="busines-map">
      <div style="height: 250px; background: #ccc; overflow: visible!important;" id="map"></div>
    </div>
  </div>
</section>
<div class="usr-profile-wrapper">
<div class="container">
  <div class="col-md-12">
    <div class="usr-busns-dtl">
      <div class="usr-profile-dtl-left">
        <div class="pg-list-1-left">
         <a href="#"><h3><?php echo $business['user_company_name']; ?></h3></a>
          
          <?php                  
                  if(!empty($TotalReview['total']))
                    $overall_vote_rate = round($TotalReview['sum'] / $TotalReview['total']);
                  else
                    $overall_vote_rate = 0;
                  $stars = '';
                  ?>
          <div class="list-rat-ch"> <span style="width:30px;"><?php echo floor($overall_vote_rate); ?></span>
            <?php
                    for ($i = 1; $i <= $overall_vote_rate; $i++) {
                      $stars .= '<i class="fa fa-star" id="' . $i . ' aria-hidden="true"></i>';
                    }
                    echo $stars;
                  ?>
          </div>
          <h4><a href="<?php echo base_url(); ?>Users/full_map/<?php echo $business['user_id']; ?>"><?php echo $business['user_company_address']; ?></a></h4>
          <!--	<p><b>Address:</b> 28800 Orchard Lake Road, Suite 180 Farmington Hills, Los Angeles, USA.</p>-->
          
          <div class="shop-open" <?php if($isopen =='We are closed') {echo 'style="color: #ff8d07 !important"';} ?>> <span  <?php if($isopen =='We are closed') {echo 'style="background: #ff8d07 !important"';} ?>></span> <?php echo $isopen; ?> </div>
          <div class="tdy-opning-time">
          	<span>Today 8.30am - 4.30pm</span>
          </div>
          
          <div class="list-number pag-p1-phone">
            <ul>
              <?php /*?><li><a href="#"><i class="fa fa-phone" ></i><?php echo $business['user_business_phone']; ?></a></li><?php */?>
              <?php /*?><li><a href="#"><i class="fa fa-envelope" ></i> <?php echo $business['user_company_email']; ?></a></li><?php */?>
              <?php /*?><li><a href="#"><i class="fa fa-user" ></i> <?php echo $business['user_firstname']. '  '. $business['user_lastname']; ?></a></li><?php */?>
              <li>
                <div class="busns-dtl-pera">
                  <div class="years-in-business">
                    <div class="count">
                      <div class="number">
                        <?php
  list($OpenYear,$OpenMonth,$OpenDay) = explode("-", $business['business_opening']);
  // Find the differences
  $YearDiff = date("Y") - $OpenYear;
  echo $YearDiff <1? 1: $YearDiff;
                  ?>
                      </div>
                    </div>
                    <span><b>YEARS</b> IN BUSINESS</span> </div>
                </div>
              </li>
             
              <?php /*?><li><a href="#"><i class="fa fa-clock-o"></i>
        <?php
         echo $business['business_opening']; ?> 

        </a></li><?php */?>
            </ul>
          </div>
          
        </div>
      </div>
      <div class="usr-busns-img">
        <?php 
$pro_pic="default/default.jpg";
                  if($business['user_profile_pic'] != "")
                  {
                    $pro_pic= $business['user_profile_pic'];
                  }
                    ?>
        <img src="<?php echo base_url("assets/profile_pics/").$pro_pic; ?>"> </div>
      <div class="ful-bus-wrpr">

        <div class="more-bus-info">
          <div class="vst-my-site">
              <a href="<?php echo $business['user_webste']; ?>"><i class="fa fa-globe"></i><?php /*?><?php echo $business['user_webste']; ?><?php */?> Visit Website </a>
          </div>
          <ul>
            <li id="follow_li"> <span><i class="fa fa-share"></i></span> <a href="javascript:;" id="follow" onclick="return follow(<?php echo $business['user_id'] ;?>, '<?php echo $followed == "yes"?2:1; ?>'); "><?php echo $followed == "yes"?"Unfollow":"Follow"; ?> the Merchant</a> </li>
            <li> <span><i class="fa fa-thumbs-up"></i></span> <a href="#" onclick="recommend(<?php echo $business['user_id']; ?>);">Reccomend the Merchant</a><b id="rec_count">(<?php echo $rec_count; ?>)</b> </li>
            <li> <span><i class="fa fa-envelope"></i></span>
              <?php
                            if($this->session->userdata('isUserLoggedIn')){
                            ?>
              <a href="javascript:;" data-toggle="modal" data-target="#chat-modal">Send Message</a>
              <?php 
                            }
                            else{
                            ?>
              <a id="loginmodal" href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Send Message</a>
              <?php
                            }
                            ?>
            </li>
            
            
            
          </ul>
          <div class="share-btn">
                <dl class="lines">
                  <dt>Share On</dt>
                  <dd> 
                    <div class="sharethis-inline-share-buttons"></div>
                  </dd>
                </dl>
              </div>
          <div class="usr-live-chat"> <span> <i class="fa fa-comment" <?php if($is_online == 0) {echo 'style="color:red!important"';}?>></i></span> <a href="#" class="online-chat">Live Chat</a> </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-8">
    <div class="usr-profile-dtl">
    <div class="usr-busns-dtl">
    	<div class="clem-busns"><span>Is this your business? Customize this page.</span><a id="registrationmodal" href="javascript:;" data-toggle="modal" data-target="#myModaComp"><i class="fa fa-gavel"></i>Claim this Bussiness</a></div>
    </div>	
    
      <div class="usr-busns-dtl">
        <div class="busns-dtl-lable">Business Description</div>
        <div class="busns-dtl-pera"> <?php echo $business['business_description']; ?> </div>
        <div class="busns-ctgry"><dl class="lines">
            <dt>Categories</dt>
            <dd>
              <?php
$arrcat = explode(",", $business['categories']);
$str="";
foreach ($arrcat as $key => $value) {
  $str .= '<a href="#">'.$value.'</a>'.',';
}

 $str = rtrim($str," "); 
 echo $str;
?>
              
              <!-- <a href="#">Categories1</a> <a href="#">Categories2</a> <a href="#">Categories3</a> --></dd>
          </dl></div>
      </div>
      <div class="usr-busns-dtl">
        <div class="busns-dtl-lable">Products, Services & Other</div>
        <div class="busns-dtl-pera">
          <div id="services">
            <?php
   if(!empty($products_services))
                {
                  //print_r($products_services);
        foreach ($products_services as $rows) {
          ?>
            <div class="items"> <span class="serv_img"> <a href=""> <img src="http://votivephp.in/VotiveYellowPages/assets/img/category/<?php echo $rows['cat_img_path']; ?>"></a></span> <span class="serv_title"> <?php echo $rows['cat_name']; ?> </span></div>
            <?php
}
}
                     ?>
          </div>
        </div>
      </div>
      <div class="usr-busns-dtl">
        <div class="busns-dtl-lable"> Picture </div>
        <div class="busns-dtl-pera">
          <div class="popu-con-sub-owl">
            <div id="owl-demo-picture" class="owl-carousel">
              <?php
                if(!empty($pics))
                {
                  //print_r($pics);
        foreach ($pics as $rows) {
          ?>
              <div class="item">
                <div class="popu-img-owl"><a href="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg" data-imagelightbox="demo" data-ilb2-caption="Caption 1"> <img src="http://votivephp.in/VotiveYellowPages/assets/photogallery/<?php echo $rows['ph_path']; ?>"></a> </div>
              </div>
              <!-- 
                    <div class="item"><div class="popu-img-owl"><a href="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg" data-imagelightbox="demo" data-ilb2-caption="Caption 1"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg"></a>
                      
                    </div></div>
                    <div class="item"><div class="popu-img-owl"><a href="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg" data-imagelightbox="demo" data-ilb2-caption="Caption 1"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg"></a>
                      
                    </div></div>
                    <div class="item"><div class="popu-img-owl"><a href="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg" data-imagelightbox="demo" data-ilb2-caption="Caption 1"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg"></a>
                      
                    </div></div>
                    <div class="item"><div class="popu-img-owl"><a href="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg" data-imagelightbox="demo" data-ilb2-caption="Caption 1"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/popu.jpg"></a>
                      
                    </div></div>  -->
              
              <?php
                }
              }
                ?>
            </div>
          </div>
        </div>
        
      
      </div>
      <div class="usr-busns-dtl">
        <div class="bsns-evnt">
          <div class="pbsns-evnt-hed">
            <h3><span>Event</span> </h3>
          </div>
          <div class="bsns-evnt-hed">
            <ul>
              <?php
                                   //print_r($events);
                                   if(!empty($events)) 
                                     {
                                       foreach ($events as $event) {
                                       //  print_r($event);
                                     ?>
              <li>
                <div class="list-pg-guar-img"> <i class="fa fa-list-alt"></i> </div>
                <div class="list-pg-guar-box">
                  <h4><a href="#"><?php echo $event['event_name'];?></a></h4>
                  <p><?php echo $event['event_date'];?></p>
                  <span><?php echo $event['event_description'];?></span> </div>
              </li>
              <?php
                                        
                                       }
                                     }


                                   ?>
              <!-- <li>
                                      <div class="list-pg-guar-img"> <i class="fa fa-list-alt"></i> </div>
                                      <div class="list-pg-guar-box">
                                        <h4><a href="#">Event Name  </a></h4>
                                        <p>12 Jan 2017</p>
                                        <span>Votive Php is a web development company. Votive Php is a web development company. Votive Php is a web development company.</span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="list-pg-guar-img"> <i class="fa fa-list-alt"></i> </div>
                                      <div class="list-pg-guar-box">
                                        <h4><a href="#">Event Name  </a></h4>
                                        <p>12 Jan 2017</p>
                                        <span>Votive Php is a web development company. Votive Php is a web development company. Votive Php is a web development company.</span>
                                      </div>
                                  </li>
                                  <li>
                                      <div class="list-pg-guar-img"> <i class="fa fa-list-alt"></i> </div>
                                      <div class="list-pg-guar-box">
                                        <h4><a href="#">Event Name  </a></h4>
                                        <p>12 Jan 2017</p>
                                        <span>Votive Php is a web development company. Votive Php is a web development company. Votive Php is a web development company.</span>
                                      </div>
                                  </li> -->
            </ul>
          </div>
        </div>
      </div>
      <?php /*?><div class="usr-busns-dtl">
                	<div class="bsns-comnt">
							<div class="bsns-comnt-hed">
								<h3><span>Write Your</span> Reviews</h3> </div>
							<div class="bsns-comnt-inn">
								<div class="bsns-comnt-write">
									<form class="col" name="comment" method="">
										<p>Writing great reviews may help others discover the places that are just apt for them. Here are a few tips to write a good review:</p>
										<div class="bsns-comnt-row">
										<!-- 	<div class="input-field ">
												<input id="re_name" type="text" class="validate" placeholder="Full Name">
												
											</div>
											<div class="input-field ">
												<input id="re_mob" type="number" class="validate" placeholder="Mobile">
												
											</div> -->
										</div>
										<div class="bsns-comnt-row">
											<!-- <div class="input-field ">
												<input id="re_mail" type="email" class="validate" placeholder="Email id">
												
											</div>
											<div class="input-field ">
												<input id="re_city" type="text" class="validate" placeholder="City">
												
											</div> -->
										</div>
										<div class="bsns-comnt-row textarea-filed">
											<div class="input-field ">
												<textarea id="re_msg" class="materialize-textarea" placeholder="Write Review"></textarea>
												
											</div>
										</div>
										<div class="bsns-comnt-row">
											<div class="input-field col s12"> <a class="busn-btn" href="#!">Submit Review</a> </div>
										</div>
									</form>
								</div>
							</div>
						</div>
                </div>     <?php */?>
      <div class="usr-busns-dtl">
        <div class="bsns-comnt">
          <div class="bsns-comnt-hed">
            <h3><span>Reviews</span> </h3>
          </div>
          <div class="bsns-comnt-row-btn">
            <div class="input-field">
              <?php
                        if($this->session->userdata('isUserLoggedIn')){
                        ?>
              <a class="busn-btn" href="javascript:;" data-toggle="modal" data-target="#review-modal">Submit Review</a>
              <?php 
                        }
                        else{
                        ?>
              <a id="loginmodal" class="busn-btn" href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Submit Review</a>
              <?php
                        }
                        ?>
            </div>
          </div>
          <div class="bsns-comnt-inn">
            <?php
                      if(!empty($reviewlist)){
                        foreach($reviewlist as $rvw){

                          ?>
            <div class="bsns-comnt-read">
              <div class="bsns-comnt-img">
                <?php
                                if($rvw['user_profile_pic'] !="")
                                {
                                ?>
                <img src="<?php echo base_url("assets/profile_pics/").$rvw['user_profile_pic']; ?>">
                <?php
                              }
                              else
                              {
                                ?>
                <img src="<?php echo base_url("assets/profile_pics/default/default.jpg"); ?>">
                <?php


                              }

                                ?>
              </div>
              <div class="bsns-comnt-user">
                <h4><?php echo $rvw['user_firstname']. " ". $rvw['user_lastname']; ?> <span><?php echo $rvw['blog_vote']; ?> <i class="fa fa-star"></i> </span> </h4>
                <span class="lr-revi-date"><?php echo $rvw['review_date']; ?></span>
                <p><?php echo $rvw['review_text']; ?></p>
              </div>
            </div>
            <?php
                        }
                      }else{
                        ?>
            <div class="bsns-comnt-read no-review">
              <div class="bsns-comnt-user">
                <p>No review available.</p>
              </div>
            </div>
            <?php
                      }
                      ?>
            
            <!-- <div class="bsns-comnt-read">
      									<div class="bsns-comnt-img">
                        	<img src="<?php echo base_url("assets/img/3.png"); ?>">
                        </div>
                        <div class="bsns-comnt-user">
                        	<h4>Jacob Michael <span>4.5 <i class="fa fa-star"></i></span></h4>
                            <span class="lr-revi-date">19th January, 2017</span>
                        	<p>Good service... nice and clean rooms... </p> 
                        </div>
      								</div>
                      <div class="bsns-comnt-read">
      									<div class="bsns-comnt-img">
                        	<img src="<?php echo base_url("assets/img/3.png"); ?>">
                        </div>
                        <div class="bsns-comnt-user">
                        	<h4>Jacob Michael <span>4.5 <i class="fa fa-star"></i></span></h4>
                            <span class="lr-revi-date">19th January, 2017</span>
                        	<p>Located in heart of city and easy to reach any places in a short distance. </p> 
                        </div>
      								</div>
                      <div class="bsns-comnt-read">
      									<div class="bsns-comnt-img">
                        	<img src="<?php echo base_url("assets/img/3.png"); ?>">
                        </div>
                        <div class="bsns-comnt-user">
                        	<h4>Jacob Michael <span>4.5 <i class="fa fa-star"></i></span></h4>
                            <span class="lr-revi-date">19th January, 2017</span>
                        	<p>very good spread of buffet and friendly staffs.</p> 
                        </div>
      								</div> --> 
          </div>
        </div>
      </div>
      <div class="usr-busns-dtl">
        <div class="busns-dtl-lable">Similar profiles </div>

        <div id="owl-demo-pro" class="owl-carousel">
          <?php 
        if(!empty($similarprofile)){
        foreach ($similarprofile as $rows) {
            ?>
          <div class="item">
            <div class="popu-con-sub">
              <div class="popu-img"> <img src="<?php echo base_url();?>assets/img/popu.jpg">
                <div class="popu-ovrly"> <a href="<?php echo base_url("Users/user_profile/").$rows['user_id']; ?>">
                  <h3><?php echo $rows['user_company_name']; ?></h3>
                  </a> </div>
              </div>
              <div class="popu-txt">
                <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                <div class="like-comm">
                  <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['total_followers']; ?></div>
                  <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>
                </div>
              </div>
            </div>
          </div>
          <?php
          }
        }
          ?>
        </div>
        

     </div>
    </div>
  </div>
  <div class="col-md-4">
    <div class="usr-profile">
      <div class="usr-busns-dtl">
        <div class="bsns-evnt">
          <div class="pbsns-evnt-hed">
            <h3><span>Phone Number</span> </h3>
          </div>
          <?php
$openphone = "";
$openphone = $this->uri->segment(4)!=""?$this->uri->segment(4):"";

if($this->session->userdata('isUserLoggedIn'))
{
//$openphone == "openphonemodal";  
}
else
{
$openphone = "openphonemodal";  
}
if($openphone == "openphone")
{
 

?>
          <div class="bsns-evnt-hed hlp-number"  id='dsply-num'> <a class="btn btn-default"  href="javascript:;">
            <ul>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Front Desk </h4>
                  <span> <b>
                  <div class="dsply-text"> <i class="fa fa-phone"></i>9999999999 </div>
                  </b></span> </div>
              </li>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Service Center</h4>
                  <span> <b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>00 00 000000 </div>
                  </b><b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>11 11 111111 </div>
                  </b></span> </div>
              </li>
            </ul>
            </a> </div>
          <div class="bsns-evnt-hed hlp-number"  style="display: none" id='dsply-text'> <a class="btn btn-default"  href="javascript:;" onclick="showphone();">
            <ul>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Front Desk </h4>
                  <span> <b>
                  <div class="dsply-text"> <i class="fa fa-phone"></i>Show Number </div>
                  </b></span> </div>
              </li>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Service Center</h4>
                  <span> <b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>Show Number </div>
                  </b><b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>Show Number </div>
                  </b></span> </div>
              </li>
            </ul>
            </a> </div>
          <?php

}
else if($openphone == "openphonemodal")
{
?>
          <div class="bsns-evnt-hed hlp-number" style="display: none" id='dsply-num'> <a class="btn btn-default"  href="javascript:;" data-toggle="modal" data-target="#myModal">
            <ul>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Front Desk </h4>
                  <span> <b>
                  <div class="dsply-text"> <i class="fa fa-phone"></i>9999999999 </div>
                  </b></span> </div>
              </li>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Service Center</h4>
                  <span> <b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>00 00 000000 </div>
                  </b><b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>11 11 111111 </div>
                  </b></span> </div>
              </li>
            </ul>
            </a> </div>
          <div class="bsns-evnt-hed hlp-number"  id='dsply-text'> <a class="btn btn-default"  href="javascript:;" data-toggle="modal" data-target="#myModal">
            <ul>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Front Desk </h4>
                  <span> <b>
                  <div class="dsply-text"> <i class="fa fa-phone"></i>Show Number </div>
                  </b></span> </div>
              </li>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Service Center</h4>
                  <span> <b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>Show Number </div>
                  </b><b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>Show Number </div>
                  </b></span> </div>
              </li>
            </ul>
            </a> </div>
          <?php
}

else
{
  ?>
          <div class="bsns-evnt-hed hlp-number" style="display: none" id='dsply-num'> <a class="btn btn-default"  href="javascript:;" >
            <ul>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Front Desk </h4>
                  <span> <b>
                  <div class="dsply-text"> <i class="fa fa-phone"></i>9999999999 </div>
                  </b></span> </div>
              </li>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Service Center</h4>
                  <span> <b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>00 00 000000 </div>
                  </b><b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>11 11 111111 </div>
                  </b></span> </div>
              </li>
            </ul>
            </a> </div>
          <div class="bsns-evnt-hed hlp-number"  id='dsply-text'> <a class="btn btn-default"  href="javascript:;" onclick="showphone();">
            <ul>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Front Desk </h4>
                  <span> <b>
                  <div class="dsply-text"> <i class="fa fa-phone"></i>Show Number </div>
                  </b></span> </div>
              </li>
              <li>
                <div class="list-pg-guar-box">
                  <h4>Service Center</h4>
                  <span> <b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>Show Number </div>
                  </b><b>
                  <div class="dsply-num"> <i class="fa fa-phone"></i>Show Number </div>
                  </b></span> </div>
              </li>
            </ul>
            </a> </div>
          <?php

}

                      
 
?>
        </div>
        <div class="usr-busns-dtl">
          <div class="busns-pric"><a href="<?php if(!empty($prstable) && !empty($prstable['pr_pdfpath'])) echo base_url("assets/priceLists/").$prstable['pr_pdfpath']; else echo base_url('assets/priceLists/pricelistdemo.pdf'); ?>"  target="_blank"><i class="fa fa-file-pdf-o"></i>Price</a></div>
        </div>
        <div class="usr-busns-dtl">
          <div class="bsns-evnt">
            <div class="pbsns-evnt-hed">
              <h3><span>Twitter</span> </h3>
            </div>
            <div class="bsns-evnt-hed">
              <?php
$twitter_id = "votivemanager";
//TwitterDev
//echo $twitter_id;
if($business['user_business_twitter'] != "")
{
$twitter_id = $business['user_business_twitter'];
}


?>
              <a class="twitter-timeline" data-lang="en" data-width="100%" data-height="500" data-link-color="#000" href="https://twitter.com/<?php echo $twitter_id; ?>">TWITTER</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> 
            </div>
          </div>
        </div>
        <div class="usr-busns-dtl">
          <div class="busns-dtl-lable">OPENING HOURS</div>
          <div class="busns-dtl-pera">
            <dl class="lines">
              <dt>Monday</dt>
              <dd><?php echo $openhrs[0]['mon1']. "-". $openhrs[0]['mon2']. ", ". $openhrs[0]['mon3'] ."-". $openhrs[0]['mon4'] ; ?></dd>
              <dt>Tuesday</dt>
              <dd><?php echo $openhrs[0]['tues1']. "-". $openhrs[0]['tues2']. ", ". $openhrs[0]['tues3'] ."-". $openhrs[0]['tues4'] ; ?></dd>
              <dt>Wednesday</dt>
              <dd><?php echo $openhrs[0]['wed1']. "-". $openhrs[0]['wed2']. ", ". $openhrs[0]['wed3'] ."-". $openhrs[0]['wed4'] ; ?></dd>
              <dt>Thursday</dt>
              <dd><?php echo $openhrs[0]['thur1']. "-". $openhrs[0]['thur2']. ", ". $openhrs[0]['thur3'] ."-". $openhrs[0]['thur4'] ; ?></dd>
              <dt>Friday</dt>
              <dd><?php echo $openhrs[0]['fri1']. "-". $openhrs[0]['fri2']. ", ". $openhrs[0]['fri3'] ."-". $openhrs[0]['fri4'] ; ?></dd>
              <dt>Saturday</dt>
              <dd><?php echo $openhrs[0]['sat1']. "-". $openhrs[0]['sat2']. ", ". $openhrs[0]['sat3'] ."-". $openhrs[0]['sat4'] ; ?></dd>
            </dl>
          </div>
        </div>
        <div class="usr-busns-dtl">
          <div class="bsns-evnt">
            <div class="pbsns-evnt-hed">
              <h3><span>More about Company</span> </h3>
            </div>
            <div class="bsns-evnt-hed hlp-number">
              <ul>
                <li>
                  <div class="list-pg-guar-box">
                    <h4>Manager :</h4>
                    <span> <b><i class="fa fa-user" ></i> <?php echo $business['user_firstname']. '  '. $business['user_lastname']; ?></b></span> </div>
                </li>
                <li>
                  <div class="list-pg-guar-box">
                    <h4>Creation Date :</h4>
                    <span> <b><?php echo $business['business_opening']; ?></b></span> </div>
                </li>
                <li>
                  <div class="list-pg-guar-box">
                    <h4>Company Number :</h4>
                    <span> <b><?php echo $business['user_company_number']; ?></b></span> </div>
                </li>
                <li>
                  <div class="list-pg-guar-box">
                    <h4>Spoken Languages :</h4>
                    <span> <b>
                    <?php
 $languages  = $this->User->getUserLanguages($business['user_language']);
 $lngstr="";
 foreach ($languages as $key => $value) {
  $lngstr .=  $value['lang_name']. ", ";
 }

  $lngstr = rtrim($lngstr,", ");
 echo $lngstr;


?>
                    </b></span> </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<!-- Pradeep  --> 
<span id="shownumberexits" data-dismiss="modal" data-toggle="modal" data-target="#myModaComp"></span> <span id="shownumberexits_comp" data-dismiss="modal" data-toggle="modal" data-target="#myModal"></span>
<div class="modal fade" id="myModaComp" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Claim Your Business</h4>
        <div class="title-imge"> <img src="<?php echo base_url();?>assets/img/logo.png"> </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <div class="row">
            <div class="col-md-12">
              <form action="<?php echo base_url().'Users/claim_user_profile';?>" method="post" name="myForm" >
                <span>(*) Required Fields</span>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" class="form-control " readonly="" name="user_company_name" value="<?php echo $business['user_company_name']; ?>">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" readonly="" class="form-control" name="user_company_number" value="<?php echo $business['user_company_number']; ?>">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" title="Please enter first name." class="form-control" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo set_value('user_firstname'); ?>">
                      <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?> </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" title="Please enter last name."  class="form-control"  name="user_lastname" placeholder="NOM"  value="<?php echo set_value('user_lastname'); ?>">
                      <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="email" class="form-control" title="Please enter a valid email."   name="user_email"  placeholder="*Email"  value="<?php echo set_value('user_email'); ?>">
                      <?php echo form_error('user_email','<span class="help-block">','</span>'); ?> </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <input type="text" title="Please enter telephone number."  required="" maxlength="10" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
                      <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input id="autocomplete" placeholder="*VEUILLEZ SAISIR VOTRE ADRESSE" required=""
  name="user_company_address" class="form-control " onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>">
                      </input>
                      <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?> </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="confidential" required="" id="personalconfidential"  value="1">
                        <span>*Acceptez les conditions générales</span> </label>
                    </div>
                    <div class="g-recaptcha" data-sitekey="6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M"></div>
                    <?php echo form_error('g-recaptcha-response','<span class="help-block">','</span>'); ?> </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="btn-div">
                      <button class="btn btn-default form-btn" value="S'enregistrer" name="claimSubmit" type="submit">S'enregistrer</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer"> </div>
    </div>
  </div>
</div>
<!-- User Chat model -->
<div id="chat-modal" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="prof-msg-details">
          <div class="row">
            <div class="col-xs-12">
              <div class="msg-pro-pic-sec">
                <div class="msg-profile-pic"> 
                  <!-- <i class="fa fa-user"></i> --> 
                  <img src="<?php echo base_url("assets/profile_pics/").$business['user_profile_pic']; ?>"/> </div>
                <div class="msh-prof-details"> <a href="#">
                  <h3><?php echo $business['user_company_name']; ?></h3>
                  </a> </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body messagebody" data-mcs-theme="dark">
        <div class="usermessageslisting"> </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="reply-sec-msg">
              <form method="post" action="" id="sendusermessage">
                <div class="form-group">
                  <textarea class="form-control mCustomScrollbar" wrap="off" rows="2" id="comment" placeholder="Enter Your Message..." name="usermessage"></textarea>
                </div>
                <input type="hidden" id="tosend" name="tosend" value="<?php echo $business['user_id']; ?>" />
                <button type="submit" class="btn btn-default msgsubmit">Reply</button>
                <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- User Chat Model end --> 

<!-- Review model -->
<div id="review-modal" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="prof-msg-details">
          <div class="row">
            <div class="col-xs-12">
              <div class="msg-pro-pic-sec">
                <div class="msg-profile-pic"> 
                  <!-- <i class="fa fa-user"></i> --> 
                  <img src="<?php echo base_url("assets/profile_pics/").$business['user_profile_pic']; ?>"/> </div>
                <div class="msh-prof-details"> <a href="">
                  <h3><?php echo $business['user_company_name']; ?></h3>
                  </a> </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body review-body messagebody" data-mcs-theme="dark">
        <div class="col-md-12">
          <div class="reply-sec-msg">
            <form method="post" action="" id="submitreviewforuser">
              <div class="form-group">
                <label>Write a Review</label>
              </div>
              <div class="form-group">
                <textarea class="form-control mCustomScrollbar" wrap="off" rows="2" id="comment" placeholder="Write a review..." name="userreview"></textarea>
              </div>
              <div class="form-group">
                <label>Select Rating</label>
              </div>
              <div class="form-group text-left">
                <div class="stars starrr" data-rating="0"></div>
              </div>
              <input id="ratings-hidden" name="rating" type="hidden">
              <input type="hidden" name="reviewto" value="<?php echo $business['user_id']; ?>" />
              <button type="submit" class="btn btn-default msgsubmit">Submit</button>
              <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>
            </form>
          </div>
        </div>
      </div>
      <!-- <div class="modal-footer">
          </div> --> 
    </div>
  </div>
</div>
<!-- Review Model end --> 



<!--     Thanks follow Model -->
<div class="modal fade" id="thanksModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Thank You</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          Thank You for following business profile.
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!--     Thanks follow Model -->
<div class="modal fade" id="unthanksModal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Thank You</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          Hope you will follow us again.
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

