<section class="review-sec">
	<div class="review-main">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="usr-busns-dtl">
				        <div class="bsns-comnt">
					        <div class="bsns-comnt-hed">
					           <h3><span>Reviews</span> </h3>
					        </div>
					        
					        <div class="bsns-comnt-row-btn">
					            <div class="input-field">
					            	<?php   
					                if(!empty($TotalReview['total']))
					                    $overall_vote_rate = round($TotalReview['sum'] / $TotalReview['total']);
					                  //printer_draw_elipse(printer_handle, ul_x, ul_y, lr_x, lr_y)
					                    else
					                      $overall_vote_rate = 0;
					                  $stars = '';
					                  $stars1 = '';
					                  
					                  ?>						           
					                <a class="busn-btn" href="javascript:;">Overall Review: <?php echo $overall_vote_rate."/5"; ?> <i class="fa fa-star"></i></a>
					            </div>
					        </div>
					        
					        <div class="bsns-comnt-inn">
					        	<?php
					        	if(!empty($details)){
					        		foreach($details as $rvw){
					        		?>
						            <div class="bsns-comnt-read">
						        	    <div class="bsns-comnt-img">
						        	    	<?php
						        	    	if($rvw['user_profile_pic'] !=""){
			                                ?>
							                	<img src="<?php echo base_url("assets/profile_pics/").$rvw['user_profile_pic']; ?>">
							                <?php
				                            }
				                            else
				                            {
				                            ?>
							                	<img src="<?php echo base_url("assets/profile_pics/default/default.jpg"); ?>">
							                <?php
							            	}
							            	?>
						                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
						                </div>						            	
						            	<div class="bsns-comnt-user">
						                	<h4><?php echo $rvw['user_firstname']. " ". $rvw['user_lastname']; ?> <span><?php echo $rvw['blog_vote']; ?> <i class="fa fa-star"></i> </span> </h4>
						                	<span class="lr-revi-date"><?php echo date("d F, Y H:i", strtotime($rvw['review_date'])); ?></span>
						                	<p><?php echo $rvw['review_text']; ?></p>
						                	<p><a href='javascript:showemail(<?php echo $rvw['vote_id']; ?>);' data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="<?php echo $rvw['vote_id'];; ?>">Report this review</a></p>
						              	</div>
						            </div>
					            	<?php
					        		}
					        	}
					            ?>

					            <!-- <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 

					            <div class="bsns-comnt-read">
					        	    <div class="bsns-comnt-img">
					                    <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg">
					                </div>
					            	
					            	<div class="bsns-comnt-user">
					                	<h4>John-Lloyd HAMLET <span>0 <i class="fa fa-star"></i> </span> </h4>
					                	<span class="lr-revi-date">2017-08-09 08:22:03</span>
					                	<p>test</p>
					                	<p><a href="javascript:showemail(14);" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="14" data-original-title="" title="">Report this review</a></p>
					              	</div>
					            </div> 	 -->		           

					        </div>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</div>
</section>
