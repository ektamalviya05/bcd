
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>

<?php
if( trim($claimed) == "yes")
{
?>
<div class="row">
<div class="col-md-12">
<div class="reg-form-main">
<br><br>
<center>
<h3><?php echo $texts['label1']; ?></h3><br>
<span>
<strong><?php echo $texts['label2']; ?>
</strong>
</span>
</center>
<br><br>

<center><a href="<?php echo base_url(); ?>" class="btn btn-warning"><?php echo $texts['label3']; ?></a></center>
<br>
<br><br>

</div>
</div>
</div>

<?php
}
else
{
?>


<div class="row">
        <div class="col-md-12">
        

<div class="reg-form-main">
<form action="<?php echo base_url().'Users/claim_user_profile';?>" method="post" name="myForm" >
<span>(*) Required Fields</span>
<div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label class="gender-main">*GENRE</label>
                    <div class="for-radio">
                      <div class="btn-group" data-toggle="buttons">
          <?php
            if(!empty($user['user_gender']) && $user['user_gender'] == 'Female'){
                $fcheck = 'checked="checked"';
                $mcheck = '';
            }else{
                $mcheck = 'checked="checked"';
                $fcheck = '';
            }
          ?>

                        <label class="btn btn-default active">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male">Homme
                        </label>
                        <label class="btn btn-default">
                          <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female">Femme
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>

 <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter first name." class="form-control" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo set_value('user_firstname'); ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="text" title="Please enter last name."  class="form-control"  name="user_lastname" placeholder="NOM"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                 
                     </div>
                </div>
              </div>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="email" class="form-control" title="Please enter a valid email."   name="user_email"  placeholder="*Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter telephone number."  required="" maxlength="10" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div>
          <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="confidential" required="" id="personalconfidential"  value="1"><span>*Acceptez les conditions générales</span>
                    </label>
                  </div>
                    <div class="g-recaptcha" data-sitekey="6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M"></div>
 <?php echo form_error('g-recaptcha-response','<span class="help-block">','</span>'); ?>
                   <?php

//echo '<span class="help-block">'. $error_msg_cap.'</span>'
                   ?>

                </div>
              </div>

    <div class="row">
                <div class="col-md-12">
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" value="S'enregistrer" name="claimSubmit" type="submit">S'enregistrer</button>

                  </div>
                </div>
              </div>

    </form>
    </div>
    </div>
    </div>

   <?php
   }
   ?> 

<div class="modal fade" id="myModaThanks" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Thank You</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
            <div class="row">
              <div class="col-md-12">
             <h3>Thanks for Claim Your Profile</h3><br>
<span>
<strong>Our Administrator witll review your claim and will contact your soon.
</strong>
</span>
</div>
</div>
</div>
      </div>

    <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>


<script type="text/javascript">
    $(function() {
  $("form[name='myForm']").validate({
    rules: {
      user_firstname: "required",
      user_lastname: "required",
      user_email: {
        required: true,
        email: true
      },
       },
    // Specify validation error messages
    messages: {
      user_firstname: "Please enter your firstname",
      user_lastname: "Please enter your lastname",
      user_email: "Please enter a valid email address"
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    