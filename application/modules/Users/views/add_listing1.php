




<div >
	
	<section>
		<div class="container">
		<div class="dash-board-inner">
			<div class="row">
				<?php include 'sidemenu.php';
				?>
				<div class="col-md-9 col-sm-8 bg-col">
					<div class="panel-body panel-bodyy">
					
				<!--div class="col-sm-9"-->
					<div class="listing-add">
						<form action="<?php echo base_url();?>Users/add_listing" enctype= "multipart/form-data" method="post" name="addlisting" id="addlisting">
							<?php 
							/* if(isset($this->session->flashdata('success'))){
								echo $this->session->flashdata('success');								
							}else if(isset($this->session->flashdata('error'))){
								echo $this->session->flashdata('error');
							}else{
								echo "";
							} */
							
							?>
							<div class="row">
								<div class="col-sm-6">
									<label>Title</label>
									<input type="text" name="title" id="title" value="" >
								</div>
								
								<div class="col-sm-6">
									<label>Email</label>
									<input type="email" name="email" id="email" value="">
								</div>

							</div>

							<div class="row">
								<div class="col-sm-6">
									<label>Mobile</label>
									<input type="text" name="mobile" id="mobile" value="">
								</div>
								
								<div class="col-sm-6">
									<label>Price</label>
									<input type="text" name="price" id="price" value="">
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<label>Location</label>
									<input type="text" name="location" id="location" value="">
								</div>
								<div class="f-login col-sm-6">
									<label>Category</label>
									
									<div class="cat-sel">
										<select name="category">
											<?php 
												$count= count($category);
												for($i=0; $i<$count; $i++){
											?>
											<option value="<?php 
															if(isset($category[$i]->cat_id))
															{
																echo $category[$i]->cat_id;
															}else{
																echo "";
															}
															?>">
															<?php 
															if(isset($category[$i]->cat_name))
															{
																echo $category[$i]->cat_name;
															}else{
																echo "No category";
															}
															?>
											</option>
												<?php } ?>											
										</select>
									</div>
									
								</div>
							</div>
							
							<div class="row">
							
								<div class="f-login col-sm-6">
								  <label>State</label>
								  <div class="cat-sel">
									<select>
									  <option>State 1</option>
									  <option>State 2</option>
									  <option>State 3</option>
									  <option>State 4</option>
									  <option>State 5</option>
									  <option>State 6</option>
									  <option>State 7</option>
									  <option>State 8</option>
									</select>
								  </div>
								</div>
								<div class="col-sm-6">
									<label>Features</label>
										<input type="radio" id="features1" name="features" value="1">
										<label for="yes">Yes</label>
										<input type="radio" id="features2" name="features" value="0">
										<label for="no">No</label>														
									
								</div>
							  						
							</div>
							<div class="row">
								<div class="f-login col-sm-6">
								  <label>City</label>
								  <div class="cat-sel">
									<select>
									  <option>City 1</option>
									  <option>City 2</option>
									  <option>City 3</option>
									  <option>City 4</option>
									  <option>City 5</option>
									  <option>City 6</option>
									  <option>City 7</option>
									  <option>City 8</option>
									</select>
								  </div>
								</div>	
								<div class="f-login col-sm-6">
								  <label>Country</label>
								  <div class="cat-sel">
									<select>
									  <option>Country 1</option>
									  <option>Country 2</option>
									  <option>Country 3</option>
									  <option>Country 4</option>
									  <option>Country 5</option>
									  <option>Country 6</option>
									  <option>Country 7</option>
									  <option>Country 8</option>
									</select>
								  </div>
								</div>
							</div>	
							<div class="row">
								<div class="col-sm-12">
									<div class="uploader-first">
									  <div class="upload-sec">
										<span>Upload Related images</span>
										  <label>
											  <input type="file" name="userfile" class="multiple_input" multiple="">
										  </label>
									  </div>
									  <button class="add-more-img">Add More Images</button>
									</div>
								</div>
							</div>
							
							
							<!--div class="row">
								<div class="col-sm-6">
									<div class="uploader-first">
				                      <div class="upload-sec">
				                      	<span>Upload Related images</span>
				                          <label>
				                              <input type="file" name="userfile" class="multiple_input" multiple="">
				                          </label>
				                      </div>
				                  </div>
								</div>
								
							</div-->
							<div class="row">
								<div class="col-sm-12">
								  <label>Service Name</label>
								  <input type="text" name="">
								  <button class="add-more-img label-more">Add More Services</button>                  
								</div>
							  </div>
							


							<div class="row">
								<div class="col-sm-12">
									<div class="f-login text-box">
										<label>Description</label>
										<textarea class="te-box" name="description" id="description"></textarea>
									</div>
								</div>
							</div>
							

			              <div class="row">
			                <div class="col-sm-12 sbt-btn">
			                  <input type="submit" name="submit" value="Add Listing">
							 
			                </div>
			              </div>
						</form>
					</div>
				
					</div>
				</div>
				<div class="col-sm-3">
					<div class="right-add">
						<img src="<?php echo base_url();?>/assets/img/Property-Ad.jpg">
					</div>
				</div>
			</div>
		</div>
		</div>
	</section>
</div>