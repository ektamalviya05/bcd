<style>
	.print-btn-box {
	    max-width: 850px;
	    margin: 0 auto;
	}
	a.btn.print-btn-main {
	    background: #dd7c08;
	    color: #fff;
	    padding: 8px 15px;
	    font-weight: 600;
	    margin-top: -70px;
	}
	a.btn.print-btn-main i.fa {
	    margin-right: 10px;
	}
	a.btn.print-btn-main.top-print {
	    margin: 0 0 -75px;
	    position: relative;
	    z-index: 999;
	}
</style>
<section>
	<div class="thnkyou-sec print-reciept">
		<div class="container">
			<div class="row">
				<div class="col-sm-12" align="right">
					<div class="print-btn-box">
						<a class="btn print-btn-main top-print" href="javascript:;" onclick="printDiv('printablediv');">
				      		<i class="fa fa-print"></i> Print
				      	</a>  
					</div>
				 </div>
		    </div>

			<div class="row">
				<div class="col-sm-12">			     
					<div class="about-content" id="printablediv">
						<?php					
		                   echo $msg;
						?>					
					</div>
				</div>
			</div>

			<div class="col-sm-12" align="center">
		        <a class="btn print-btn-main" href="javascript:;" onclick="printDiv('printablediv');">
		        	<i class="fa fa-print"></i> Print
		     	</a>
			</div>
		</div>
    </div>
</section>
<script>
function printDiv(divID) {
	//Get the HTML of div
	var divElements = document.getElementById(divID).innerHTML;
	//Get the HTML of whole page
	var oldPage = document.body.innerHTML;

	//Reset the page's HTML with div's HTML only
	document.body.innerHTML = 
	  "<html><head><title></title></head><body>" + 
	  divElements + "</body>";

	//Print Page
	window.print();

	//Restore orignal HTML
	document.body.innerHTML = oldPage;

  
}
</script>