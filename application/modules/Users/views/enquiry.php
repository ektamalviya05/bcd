<div class="tab-pane" id="enquiry">
    <div class="mes-messages">
        <ul>
            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Lloyd Hamlet <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="javascript:;">											
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Rupert Expert <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Lloyd Hamlet <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>	
                </a>		                            
            </li>

            <li>
                <a href="javascript:;">			                            
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Rupert Expert <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Lloyd Hamlet <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>	
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list read">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">John Admin <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list read">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Michael One <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>


            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list read">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">John Admin <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list read">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Michael One <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list read">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">John Admin <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>					                       

            <li>
                <a href="javascript:;">
                    <div class="msg-sub-list read">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-open-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name">Michael One <span class="date">13/4/2018</span></h4>
                            <p class="msg-show">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                        </div>
                    </div>
                </a>
            </li>
        </ul>
        <nav aria-label="Page navigation example" class="servic_pagi">
            <ul class="pagination">
                <li class="page-item"> <a class="page-link" href="javascript:;" aria-label="Previous"> <span><i class="fa fa-angle-left"></i></span> <span class="sr-only">Previous</span> </a> </li>
                <li class="page-item"><a class="page-link" href="javascript:;">1</a></li>
                <li class="page-item"><a class="page-link" href="javascript:;">2</a></li>
                <li class="page-item"><a class="page-link" href="javascript:;">3</a></li>
                <li class="page-item"><a class="page-link" href="javascript:;">4</a></li>
                <li class="page-item"><a class="page-link" href="javascript:;">5</a></li>
                <li class="page-item"> <a class="page-link" href="javascript:;" aria-label="Next"> <span><i class="fa fa-angle-right"></i></span> <span class="sr-only">Next</span> </a> </li>
            </ul>
        </nav>
    </div>
</div>