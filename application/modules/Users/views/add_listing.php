<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<?php


   //print_r("<pre/>");
   //print_r($listing_content);
   //die;
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
      $title  = $listing_content[0]['title_field_english'];
      $email  = $listing_content[0]['email_field_english'];
      $mobile  = $listing_content[0]['mobile_field_english'];
      $feature  = $listing_content[0]['feature_field_en'];
      $category_text  = $listing_content[0]['category_field_en'];
      $related_image_text  = $listing_content[0]['related_image_en'];
      $description  = $listing_content[0]['descripiton_field_en'];
      $offer_image  = $listing_content[0]['offer_image_en'];
      $yes_field  = $listing_content[0]['yes_field_en'];
      $no_field  = $listing_content[0]['no_field_en'];
      $location_field  = $listing_content[0]['location_field_en'];
      $by_location_field  = $listing_content[0]['by_location_field_en'];
      $by_location_google_field  = $listing_content[0]['by_google_field_en'];
      $business_address  = $listing_content[0]['business_address_en'];
      $country_field  = $listing_content[0]['country_en'];
      $city_field  = $listing_content[0]['city_en'];
      $add_more_field  = $listing_content[0]['add_more_image_button_en'];
      $add_button_field  = $listing_content[0]['add_listing_button_en'];

      $select_business_category  = "Select Business Category";
      $select_business_sub_category  = "Select Business Sub Category";

      $select_country  = "Select Country";

      $business_sub_category_text  = "Business Sub Category";

      $main_image = "Main image";

      $business_image_one = "Business Image One";
      $optional_image = "Optional Images";
      //$enter_location = "Optional Images";

   } else {
      $title  = $listing_content[0]['title_field_ar'];  
      $email  = $listing_content[0]['email_field_ar'];
      $mobile  = $listing_content[0]['mobile_field_ar'];

      $feature  = $listing_content[0]['feature_field_ar'];
      $category_text  = $listing_content[0]['category_field_ar'];
      $related_image_text  = $listing_content[0]['related_image_ar'];
      $description  = $listing_content[0]['descripiton_field_ar'];
      $offer_image  = $listing_content[0]['offer_image_ar'];
      $yes_field  = $listing_content[0]['yes_field_ar'];
      $no_field  = $listing_content[0]['no_field_ar'];
      $location_field  = $listing_content[0]['location_field_ar'];
      $by_location_field  = $listing_content[0]['by_location_field_ar'];
      $by_location_google_field  = $listing_content[0]['by_google_field_ar'];
      $business_address  = $listing_content[0]['business_address_ar'];
      $country_field  = $listing_content[0]['country_ar'];
      $city_field  = $listing_content[0]['city_ar'];
      $add_more_field  = $listing_content[0]['add_more_image_button_ar'];
      $add_button_field  = $listing_content[0]['add_listing_button_ar'];

      $select_business_category  = "اختر فئة العمل";
      $select_business_sub_category  = "حدد فئة الأعمال الفرعية";

      $select_country  = "حدد الدولة";

      $business_sub_category_text  = "فئة فرعية الأعمال";

      $main_image = "الصورة الرئيسية";

      $business_image_one = "صورة أعمال واحدة";

      $optional_image = "صور اختيارية";
  }
?>
<div style="padding:50px 0;background: #fff;">
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<?php 
						 //if($this->session->flashdata('error')){
								//echo $this->session->flashdata('error');								
							//}

							?>
					<div class="listing-add">
						<form action="<?php echo base_url();?>Users/add_listing" enctype= "multipart/form-data" method="post" name="addlisting" id="addlisting">
							<?php 

								/* if(isset($this->session->flashdata('success'))){
									echo $this->session->flashdata('success');								
								}else if(isset($this->session->flashdata('error'))){
									echo $this->session->flashdata('error');
								}else{
									echo "";
								} */

								if($this->session->flashdata('error')){
								echo "<span class='error err_txt'>";
								echo $this->session->flashdata('error');
									echo "</span>" ;
								}
								else{
									echo "";
								}
							
							
							?>
							<div class="row">
								<div class="col-sm-6">
									<label><?php echo $title;?><span style="color: #be2d63">*</span></label>

									<input type="text" name="title" id="title" value="" >
								</div>
								
								<div class="col-sm-6">
									<label><?php echo $email;?><span style="color: #be2d63">*</span></label>
									<input type="email" name="email" id="email" value="">
								</div>

							</div>

							<div class="row">
								<div class="col-sm-6">
									<label><?php echo $mobile;?><span style="color: #be2d63">*</span></label>
									<input type="text" name="mobile" id="mobile" value="">
								</div>
								 
								<!-- <div class="col-sm-6 hidden">
									<label>Price</label>
									<input type="text" name="price" id="price" value="">
								</div> -->
								<!-- <div class="col-sm-6">
									<label><?php echo $feature;?></label>
									<input type="radio" id="features1" name="features" value="1" >
									<label for="yes"><?PHP echo $yes_field;?></label>
									<input type="radio" id="features2" name="features" value="0" checked="checked">
									<label for="no"><?PHP echo $no_field;?></label>				
								</div> -->
							</div>

							<div class="row">
								<!--div class="col-sm-6">
									<label>Location</label>
									<input type="text" name="location" id="location" value="">
								</div-->
								<div class="f-login col-sm-6">

									<label><?php echo $category_text;?> <span style="color: #be2d63">*</span></label>
									
									<div class="cat-sel">

										<?php
										    //print_r("<pre/>");
										    //print_r($category);
										?>

										<select name="category" id="cat_id">
											<option value=""><? echo $select_business_category;?></option>
											<?php 
												$count= count($category);
												for($i=0; $i<$count; $i++){

													if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $category_name  = $category[$i]->cat_name;
                                 
                                                                  } else{
                                                                    $category_name  = $category[$i]->cat_namea;

                                                             }

													
											?>

											<option value="<?php 
															if(isset($category[$i]->cat_id))
															{
																echo $category[$i]->cat_id;
															}else{
																echo "";
															}
															?>">
															<?php 
															if(isset($category[$i]->cat_name))
															{
																echo $category_name;
															}else{
																echo "No category";
															}
															?>
											</option>
												<?php } ?>											
										</select>
									</div>									
								</div>

								<div class="f-login col-sm-6">
                                  <label><?php echo $business_sub_category_text;?>:</label>
                                  <div class="cat-sel">
                                    <select class="" id="sub_cat_id" name="sub_cat_id">
                                       <option value=''><?php echo $select_business_sub_category;?></option>
                                    </select>
                                   </div>
                                </div>
                                
								
								<!--div class="row">
								
									<div class="f-login col-sm-6">
									  <label>State</label>
									  <div class="cat-sel">
										<select>
										  <option>State 1</option>
										  <option>State 2</option>
										  <option>State 3</option>
										  <option>State 4</option>
										  <option>State 5</option>
										  <option>State 6</option>
										  <option>State 7</option>
										  <option>State 8</option>
										</select>
									  </div>
									</div>
									<div class="col-sm-6">
										<label>Features</label>
											<input type="radio" id="features1" name="features" value="1">
											<label for="yes">Yes</label>
											<input type="radio" id="features2" name="features" value="0">
											<label for="no">No</label>														
										
									</div>
								  						
								</div-->
								<!--div class="row">
									<div class="f-login col-sm-6">
									  <label>City</label>
									  <div class="cat-sel">
										<select>
										  <option>City 1</option>
										  <option>City 2</option>
										  <option>City 3</option>
										  <option>City 4</option>
										  <option>City 5</option>
										  <option>City 6</option>
										  <option>City 7</option>
										  <option>City 8</option>
										</select>
									  </div>
									</div>	
									<div class="f-login col-sm-6">
									  <label>Country</label>
									  <div class="cat-sel">
										<select>
										  <option>Country 1</option>
										  <option>Country 2</option>
										  <option>Country 3</option>
										  <option>Country 4</option>
										  <option>Country 5</option>
										  <option>Country 6</option>
										  <option>Country 7</option>
										  <option>Country 8</option>
										</select>
									  </div>
									</div>
								</div-->	
								</div>
                                
                                <div class="row">
                                    <div class="col-md-12">
								        <label>Image Icon: </label>
								        <label class="uplodfirstimg">
								            <input type="file" name="imageIcon" class="multiple_input valid" multiple="" aria-required="true" aria-invalid="false">
								        </label>
									  </div>
                                </div>

								<div class="row">
									<div class="f-login col-sm-6">
										<label><?php echo $location_field;?> <span style="color: #be2d63">*</span></label>
										<div id="myRadioGroup" class="text-left">
											<span>
												<input type="radio" name="cars" checked="checked" value="2"  />
												<label for="yes"><?php echo $by_location_field;?></label>
											</span>

	   									    <span>
	   									    	<input type="radio" name="cars" value="3" />
	   									    	<label for="yes"><?php echo $by_location_google_field;?></label>
	   									   	</span>
	   									</div>
	   								</div>

	   								<div class="col-md-12">
	   								<div class="myRadioGroup-box">

	   								<div id="Cars2" class="desc">
							    		<div class="row">
							    		<div class="col-sm-12">
									    	<label><?php echo $business_address;?> </label>
									    	<input name="business_address" id="business_address" type="text" />
							    		</div>
							    	</div>
							    	<div class="row">
								    	<div class="f-login col-sm-6">
								    		<label><?php echo $country_field; ?></label>
										<div class="cat-sel">
											<select name="business_country" id="business_country">
												<option><?php echo $select_country;?></option>
											
												 <?php 
												//print_r($country);
												 foreach($country as $value1) {


												 	if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $country_name  = $value1['name'];
                                 
                                                                  } else{
                                                                    $country_name  = $value1['arabic'];

                                                                  }
												?>
												<option value="<?php echo $value1['name']; ?>"><?php  echo $country_name; ?></option>
												<?php
												}
												?> 
									        </select>
									    </div>
								    	</div>

								    	<div class="f-login col-sm-6">
								    		<label><?php echo $city_field;?></label>
										<div class="cat-sel">
											<!-- <select name="business_city" id="business_city">
												<option value="">Select city</option>
											</select> -->
											<input type="text" name="business_city" id="business_city">
									    </div>
								    	</div>
							    	</div>							        
							    </div>

							    <div id="Cars3" class="desc" style="display: none;">
							        <input name="location" id="location" type="text"/>
							    </div>

	   								</div>
	   							</div>
	   							
	   							</div>

							   
									
							
							<div class="row">
								<div class="col-sm-12">
									<div class="uploader-first all-reletiv-img">
										<span><?php echo $main_image;?> *</span>
										
									  <div class="upload-sec uplod-prwv-img">

										
										<div class="imguploadfirst">
											<div class="add-mor-first">
												<h2><?php echo $business_image_one;?>: </h2>
												
											</div>
											<div class="img-prw-box">
												<img id="output" class="imgclas">
											</div>
											  <label class="uplodfirstimg">											  	
												  <input type="file" name="userfile" class="multiple_input" multiple="" onchange="loadFile(event)">
											  </label>
										</div>

										
										
										<div class="add-mor-sub">
												<h2><?php echo $optional_image; ?></h2>
												<button class="add-more-img image"><?php echo $add_more_field;?></button>
										</div>

										<div class="fileupload"></div>
									  </div>
									 
									</div>
								</div>
							</div>
							
							
							<!--div class="row">
								<div class="col-sm-6">
									<div class="uploader-first">
				                      <div class="upload-sec">
				                      	<span>Upload Related images</span>
				                          <label>
				                              <input type="file" name="userfile" class="multiple_input" multiple="">
				                          </label>
				                      </div>
				                  </div>
								</div>
								
							</div-->
							<!-- <div class="row">
								<div class="col-sm-12 " id="service">
								  <label>Service Name</label>
								  <input type="text" name="">
								  <button class="add-more-img service label-more">Add More Services</button>                  
								</div>
							  </div> -->						


							<div class="row">
								<div class="col-sm-12">
									<div class="text-box">
										<label><?php echo $description;?> <span style="color: #be2d63">*</span></label>
										<textarea class="te-box" name="description" id="description"></textarea>
									</div>
								</div>
							</div>
							<!-- <div class="row">
								<div class="col-sm-12">
									<div class="uploader-first">
										<span>Offer image</span>
										  <label>
											  <input type="file" name="offer" class="multiple_input">
										  </label>
									</div>
								</div>
							</div> -->
							<!-- <div class="prvw-hed"><h2><?php //echo $offer_image;?></h2></div>
							<div class="imgp-prwv-box">
							<div class="row">
								<div class="">
									<div class="uploader-first-new">
									  <div class="upload-sec-one">		
									  <div class="imgp-prwv-box2">								
											<img id="output5" class="imgclas">
										</div>
										  <label>										  		
											  <input type="file" name="offer" class="multiple_input" onchange="loadFile5(event)">
										  </label>
									  </div>
			
									</div>
								</div>
							</div>
						</div> -->

			              <div class="row">
			                <div class="col-sm-12 sbt-btn tooltip-div">
			                  <input type="submit" name="submit" value="<?php echo $add_button_field;?>">							 
			                </div>
			              </div>
						</form>
					</div>
				
				</div>
				<div class="col-sm-3">
					<div class="right-add">
						<!-- Google map auto fill code start -->
						<div id="map_canvas"></div>
						<!-- Google map auto fill code close -->

						<img src="<?php echo base_url();?>assets/img/Property-Ad.jpg">
					</div>
				</div>
			</div>
			
		</div>		
	</section>

	<!-- Google map auto fill code start -->
    <style>
        #map_canvas {         
            height: 400px;         
            width: 400px;         
            margin: 0.6em;       
        }
    </style>
    <!-- Google map auto fill code close -->

</div>
<script>
	$(function() {
            var e;
            e = $('.search-form input[name="location"]')[0];
            return new google.maps.places.Autocomplete(e, {
                types: ["geocode"]
            })

    });

    function loadFile (event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile2 (event) {
    var output2 = document.getElementById('output2');
    output2.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile3 (event) {
    var output3 = document.getElementById('output3');
    output3.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile4 (event) {
    var output4 = document.getElementById('output4');
    output4.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile5 (event) {
    var output5 = document.getElementById('output5');
    output5.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile6 (event) {
    var output6 = document.getElementById('output6');
    output6.src = URL.createObjectURL(event.target.files[0]);
  };

   function loadFile7 (event) {
    var output7 = document.getElementById('output7');
    output7.src = URL.createObjectURL(event.target.files[0]);
  };

   function loadFile8 (event) {
    var output8 = document.getElementById('output8');
    output8.src = URL.createObjectURL(event.target.files[0]);
  };

   function loadFile9 (event) {
    var output9 = document.getElementById('output9');
    output9.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile10 (event) {
    var output10 = document.getElementById('output10');
    output10.src = URL.createObjectURL(event.target.files[0]);
  };
	</script>


