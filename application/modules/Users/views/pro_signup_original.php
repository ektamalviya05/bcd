    <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M'
        });
      };
    </script>
    
<div class="form_container">
    <form action="" method="post">
<span>(*) Required Fields</span>

      <div class="form-group">
      <label class="lbl_block">Gender</label>
            <?php
            if(!empty($user['user_gender']) && $user['user_gender'] == 'Female'){
                $fcheck = 'checked="checked"';
                $mcheck = '';
            }else{
                $mcheck = 'checked="checked"';
                $fcheck = '';
            }
            ?>
            <div class="radio inline_block">
                <label >
                <input type="radio" name="user_gender" value="Male" <?php echo $mcheck; ?>>
                Male
                </label>
            
                <label >
                  <input type="radio" name="user_gender" value="Female" <?php echo $fcheck; ?>>
                  Female
                </label>
                                <input type="hidden" name="user_type" value="Professional">
            </div>
        </div>

        <div class="form-group">
           <label class="lbl_block">*First Name</label>
            <input type="text" class="form-control inline_block" name="user_firstname" placeholder="First Name"  value="<?php echo set_value('user_firstname'); ?>">
          <?php echo form_error('user_firstname','<span class="help-block">','</span>'); ?>
        </div>

         <div class="form-group">
            <label class="lbl_block">Last Name</label>
            <input type="text" class="form-control inline_block" name="user_lastname" placeholder="Last Name"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
        </div>


<div class="form-group">
            <label class="lbl_block">*Company Name</label>
            <input type="text" class="form-control inline_block" name="user_company_name" placeholder="Company Name"  value="<?php echo set_value('user_company_name'); ?>">
          <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>
        </div>



<div class="form-group">
            <label class="lbl_block">*Company Address</label>
           
  <input id="autocomplete" placeholder="Enter Company Address" name="user_company_address" class="form-control inline_block" onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>"></input>
   
         <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?>
        </div>

<div class="form-group">
            <label class="lbl_block">Your Category</label>
                 
<?php
$categories[1] = "Category 1";
$categories[2] = "Category 2";
$categories[3] = "Category 3";
$categories[4] = "Category 4";
$categories[5] = "Category 5";
$categories[6] = "Category 6";
$categories[7] = "Category 7";
$categories[8] = "Category 8";
?>
<?php 
echo form_dropdown('user_categories', $categories, 'Category 1', 'class="dropdown_box1 form-control inline_block"');
    ?>

                                              


        </div>


<div class="form-group">
<label class="lbl_block">Company Language</label>

<input type="checkbox" name="user_language[]" value="2" checked="checked"> English
<input type="checkbox" name="user_language[]" value="3"> French
<input type="checkbox" name="user_language[]" value="4"> Spanish
<input type="checkbox" name="user_language[]" value="5"> Arab
<input type="checkbox" name="user_language[]" value="6"> Italian
<input type="checkbox" name="user_language[]" value="7"> German


        </div>







        <div class="form-group">
        <label class="lbl_block">*Email</label>
            <input type="email" class="form-control inline_block" name="user_email"  placeholder="Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
        </div>

        <div class="form-group">
        <label class="lbl_block">*Phone</label>
            <input type="text" class="form-control inline_block" name="user_phone"  placeholder="Phone" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>
        </div>

<!--<div class="form-group">
<label class="lbl_block">*Address</label>
            <input type="text" class="form-control inline_block" name="user_address"  placeholder="Your Address" value="<?php echo set_value('user_address'); ?>">
            <?php echo form_error('user_address','<span class="help-block">','</span>'); ?>
        </div>
-->

        <div class="form-group">
        <label class="lbl_block">*Password</label>
          <input type="password" class="form-control inline_block" name="user_password" placeholder="Password" >
          <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
        </div>

        <div class="form-group">
        <label class="lbl_block">Confirm Password</label>
          <input type="password" class="form-control inline_block" name="conf_password" placeholder="Confirm password" >
          <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?>
        </div>
<div><br><p>&nbsp;</p></div>

        <div class="form-group">
      <input type="checkbox" name="auth_check" id="auth_check" value="1" checked="checked">

Would you like to receive promotional offers from Otourdemoi and our partners? (Good plans, promotions, exceptional discounts ...)
</div>


      <div class="form-group">
      <input type="checkbox" name="sms_check" id="sms_check" value="1" checked="checked">

Would you like to receive promotional offers from Otourdemoi and our partners on your mobile? (Good plans, promotions, exceptional discounts ...)
</div>

    <div class="form-group">
<input type="checkbox" name="confidential" required="" id="confidential"  value="1"> *Accept Terms & Conditions
</div>

    
    <div id="html_element"></div>
      <br>
        <div class="form-group">
            <input type="submit" name="regisSubmit" class="btn-primary" value="Signup Now"/>
        </div>
    </form>
    </div>
    <p class="footInfo">Already have an account? <a href="<?php echo base_url(); ?>Users/login">Login here</a></p>          