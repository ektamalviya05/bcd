<div class="main-content">
    <div class="login-page">
        <div class="container">
            <div class="row">

                <div class="col-sm-12">

                     <?php
              if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 $firstname  = "First Name";
                                 $last_name = "Last Name";
                                 $email = "Email";
                                 $address = "Address";
                                 $password = "Password";
                                 $confirm_password = "Confirm Password";
                                 $phone_number = "Mobile No.";
                                 $zip_code = "Zip code";

                                 $already_have_account = "Already Have an account?";

                                 $log_in_here = "Log in here";

                                 
                                 //$category_desc  = $categories['cat_description'];
                               } else {
                                 $firstname  = "الاسم الاول";
                                 $last_name = "الكنية";
                                 $email = "البريد الإلكتروني";
                                 $address = "عنوان";
                                 $password = "كلمه السر";
                                 $confirm_password = "تأكيد كلمة المرور";
                                 $phone_number = "رقم المحمول";
                                 $zip_code = "الرمز البريدي";

                                 $already_have_account = "هل لديك حساب؟";

                                 $log_in_here = "سجل الدخول هنا";
                                 //$category_desc  = $categories['cat_descriptiona'];
                               }
              ?> 
                    
                    <div class="form-box">
                        
                        <?php if ($this->session->flashdata('success')) { ?>
                            <div class="alert alert-success message">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <?php echo $this->session->flashdata('success'); ?></div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('error')) { ?>
                            <div class="alert alert-danger message">
                                <button type="button" class="close" data-dismiss="alert">x</button>
                                <?php echo $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                        
                         <h2><?php echo $top_content[0]['Sign_up']; ?></h2>
                        <form method="post" name="signup" action="signup">
                            <div class="row">
                                <div class="f-login col-sm-6">
                                    <label><?php echo $firstname; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="text" name="user_firstname" id="user_firstname" placeholder="<?php echo $firstname; ?>" value="<?php echo set_value('user_firstname'); ?>"><?php echo form_error('user_firstname', '<span class="error">', '</span>'); ?> 
                                </div>
                                <div class="f-login col-sm-6">
                                    <label><?php echo $last_name; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="text" name="user_lastname" id="user_lastname" placeholder="<?php echo $last_name; ?>" value="<?php echo set_value('user_lastname'); ?>"><?php echo form_error('user_lastname', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="f-login col-sm-6">
                                    <label><?php echo $email; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="text" name="user_email" id="user_email" placeholder="<?php echo $email; ?>" value="<?php echo set_value('user_email'); ?>"><?php echo form_error('user_email', '<span class="error">', '</span>'); ?>
                                </div>
                                <!-- <div class="f-login col-sm-6">
                                    <label><?php echo $address; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="text" name="user_address" id="autocomplete" placeholder="<?php echo $address; ?>" value="<?php echo set_value('user_address'); ?>"><?php echo form_error('user_address', '<span class="error">', '</span>'); ?>
                                </div> -->

                                <div class="f-login col-sm-6">
                                    <label><?php echo $phone_number; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="text" name="user_phone" id="user_phone" placeholder="<?php echo $phone_number; ?>" value="<?php echo set_value('user_phone'); ?>"><?php echo form_error('user_phone', '<span class="error">', '</span>'); ?>
                                </div>

                            </div>
                            <div class="row">
                                <div class="f-login col-sm-6">
                                    <label><?php echo $password; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="password" name="user_password" id="user_password" placeholder="<?php echo $password; ?>" value="<?php echo set_value('user_password'); ?>"><?php echo form_error('user_password', '<span class="error">', '</span>'); ?>
                                </div>
                                <div class="f-login col-sm-6">
                                    <label><?php echo $confirm_password; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="password" name="conf_password" id="conf_password" placeholder="<?php echo $confirm_password; ?>" value="<?php echo set_value('conf_password'); ?>"><?php echo form_error('conf_password', '<span class="error">', '</span>'); ?>
                                </div>
                            </div>

                            <div class="row">
                                
                                <!-- <div class="f-login col-sm-6">
                                    <label><?php echo $zip_code; ?> <span style="color: #be2d63">*</span></label>
                                    <input type="text" name="user_zipcode" id="user_zipcode" placeholder="<?php echo $zip_code; ?>" value="<?php echo set_value('user_zipcode'); ?>"><?php echo form_error('user_zipcode', '<span class="error">', '</span>'); ?>
                                </div> -->
                            </div>
                            <div class="submit-login">
                                <input type="submit" name="" value="<?php echo $top_content[0]['Sign_up']; ?>">
                            <p class="sign_txt"><?php echo $already_have_account;?> <a href="<?php echo base_url();?>login"><?php echo $log_in_here;?></a></p>    
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
