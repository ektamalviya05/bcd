
<div class="subscription">
  <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="mySubs">
    <div class="newsletter-main-head">
      <div class="news-letter-head">
        <h4><?php echo $pagecontent['label1']; ?></h4>
        <p><?php echo $pagecontent['label2']; ?></p>
    
      <div class="row news-letter-form">
        <div class="col-md-5">
          <input type="text" title="E-mail" placeholder="<?php echo $pagecontent['label3']; ?>" value="<?php echo set_value('user_email'); ?>" style="width:34%" name="user_email" required="" class="inputbox">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
        </div>
        <div class="col-md-5">
          <input type="text" title="Postal Code" placeholder="<?php echo $pagecontent['label4']; ?>" value="<?php echo set_value('user_postcode'); ?>" style="width:34%" name="user_postcode" class="inputbox">
            <?php echo form_error('user_postcode','<span class="help-block">','</span>'); ?>
        </div>
        <div class="col-md-2">
          <input type="submit" name="subscribeSubmit" onClick="" value="<?php echo $pagecontent['label5']; ?>" class="submit-main">
        </div>
      </div>
      <div class="row agree-sec">
        <div class="col-md-12">
          <input type="checkbox" title="Terms and Conditions" name="terms">
          <span><?php echo $pagecontent['label6']; ?></span>
        </div>
      </div>
      </div>
    </div>
  </form>
</div>