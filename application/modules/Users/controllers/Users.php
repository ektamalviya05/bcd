<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->library('form_validation', 'session');
        $this->load->model('User');
        $this->load->model('Page');
        //is_user_not_login();

        $this->load->library('facebook');



       //error_reporting(E_ALL);
       //ini_set('display_errors', 1);


        //is_user_not_login();
        require_once APPPATH.'third_party/src/Google_Client.php';
        require_once APPPATH.'third_party/src/contrib/Google_Oauth2Service.php';
    }

    /* With ajax */

    public function forgetpassword() {




        
        $data = array();
        if ($this->input->post()) {
            $email = $this->input->post('email');
            //$con['returnType'] = 'single';
            $con['conditions'] = array(
                'user_email' => $email,
                'status' => '1'
            );
            $con['returnType'] = "single";
            $checkUser = $this->User->getRows($con);

            if (!empty($checkUser)) {

                $temppass = $this->generateRandomPass();
                $temppass_md5 = md5($temppass);
                $upd = $this->User->update_password($email, $temppass_md5);

                if ($upd >= 1)
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id" => 26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id" => 13));
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id" => 25)); // Email Footer
                //$email = $userData['user_email'];
                $subject = $templatecenter['tmplate_subject'];
                $msginner = "Please find your temporary password below<br><br><b>" . $temppass . "</b><br><br>Please login to your dashboard with temp password and Change your password. <br><br>
				<a href='" . base_url('login') . "'> Login Here</a><br><br>";

                $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%Address%%", "%%Phone%%", "%%TempPassword%%");
                $real = array($checkUser['user_firstname'], $checkUser['user_lastname'], $checkUser['user_email'], $checkUser['user_address'], $checkUser['user_phone'], $temppass);
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content']);
                $msg = $templateheader['tmplate_content'] . " " . $msg . " " . $templatefooter['tmplate_content'];
                $x = $this->send_mail($email, $subject, $msg);
                $this->session->set_flashdata('success', 'Your Password has been sent to your email. Please Check and Login to your account.');
                redirect(base_url('forgetpassword'));
            }else {
                $this->session->set_flashdata('error', 'Please check your email address.');
                redirect(base_url('forgetpassword'));
            }
        }
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Forget Password');
        $data['top_content']        = $this->Page->getTopContent();
        $this->template->load('home_layout', 'contents', 'forgetpassword', $data);
    }

     public function google_login()
    {


        
        $clientId = '481318897331-2dobke5f40vnb9tjus59kmi1mkvhmm2j.apps.googleusercontent.com'; //Google client ID
        $clientSecret = 'IjxvA6H1k63GrJPcqkurMCzF'; //Google client secret
        $redirectURL = base_url()."Users/google_login";

        //Call Google API
        $gClient = new Google_Client();
        $gClient->setApplicationName('Login');
        $gClient->setClientId($clientId);
        $gClient->setClientSecret($clientSecret);
        $gClient->setRedirectUri($redirectURL);
        $google_oauthV2 = new Google_Oauth2Service($gClient);
        
        if(isset($_GET['code']))
        {
            $gClient->authenticate($_GET['code']);
            $_SESSION['token'] = $gClient->getAccessToken();
            header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
        }

        if (isset($_SESSION['token'])) 
        {
            $gClient->setAccessToken($_SESSION['token']);
        }
        
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
            //echo "<pre>";
            //print_r($userProfile);
            //die;
            $email = $userProfile['email'];
            $user_firstname = $userProfile['given_name'];
            $user_lastname = $userProfile['family_name'];
             $this->db->select('*');
            $this->db->from('users');
            $this->db->where('user_email=',$email);
            $query = $this->db->get();
            $result= $query->num_rows();
            if($result>0)
            {
             $con['conditions'] = array(
                    'user_email' => $email,
                    
                    'user_type' =>'Personal',
                );
             $checkLogin = $this->User->getRows($con);
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                        $this->session->set_userdata('user_id', $checkLogin[0]['user_id']);
                        $this->session->set_userdata('timeout', time());
                        $uu_id = $checkLogin[0]['user_id'];
                        $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                        $ci_session['timestamp'] = time();
                        $ci_session['status'] = 0;
                        if (empty($ci_session_check)) {
                            $ci_session['user_id'] = $uu_id;
                            $this->db->insert("ci_sessions", $ci_session);
                        } else {
                            $this->db->where("user_id", $uu_id);
                            $this->db->update("ci_sessions", $ci_session);
                        }
             redirect(base_url('dashboard'));
            }
            else
            {
                $data= array(
                        'user_email' => $email,
                        'user_firstname' => $user_firstname,
                        'user_lastname' => $user_lastname,
                        'user_type'  => 'Personal',
                        'status' => '1',
                        );
                $insert = $this->db->insert('users',$data);
                if($insert)
                {
                    $con['conditions'] = array(
                    'user_email' => $email,
                    
                    'user_type' =>'Personal',
                    );
                    $checkLogin = $this->User->getRows($con);
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                        $this->session->set_userdata('user_id', $checkLogin[0]['user_id']);
                        $this->session->set_userdata('timeout', time());
                        $uu_id = $checkLogin[0]['user_id'];
                        $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                        $ci_session['timestamp'] = time();
                        $ci_session['status'] = 0;
                        if (empty($ci_session_check)) {
                            $ci_session['user_id'] = $uu_id;
                            $this->db->insert("ci_sessions", $ci_session);
                        } else {
                            $this->db->where("user_id", $uu_id);
                            $this->db->update("ci_sessions", $ci_session);
                        }
                     redirect(base_url('dashboard'));
                }

            }
        } 
        else 
        {
            $url = $gClient->createAuthUrl();
            header("Location: $url");
            exit;
        }
        
       
    } 

    public function fb_login()
    {
      $userData = array();
        if($this->facebook->is_authenticated()){
            $userProfile = $this->facebook->request('get', '/me?fields=id,first_name,last_name,email,gender,locale,picture');
            
            //print_r($userProfile);
            //die;
            $email = $userProfile['email'];
            $user_firstname = $userProfile['first_name'];
            $user_lastname = $userProfile['last_name'];
             $this->db->select('*');
            $this->db->from('users');
            $this->db->where('user_email=',$email);
            $query = $this->db->get();
            $result= $query->num_rows();
            if($result>0)
            {
             $con['conditions'] = array(
                    'user_email' => $email,
                    
                    'user_type' =>'Personal',
                );
             $checkLogin = $this->User->getRows($con);
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                        $this->session->set_userdata('user_id', $checkLogin[0]['user_id']);
                        $this->session->set_userdata('timeout', time());
                        $uu_id = $checkLogin[0]['user_id'];
                        $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                        $ci_session['timestamp'] = time();
                        $ci_session['status'] = 0;
                        if (empty($ci_session_check)) {
                            $ci_session['user_id'] = $uu_id;
                            $this->db->insert("ci_sessions", $ci_session);
                        } else {
                            $this->db->where("user_id", $uu_id);
                            $this->db->update("ci_sessions", $ci_session);
                        }
             
             redirect('dashboard');
            }
             else
            {
                $data= array(
                        'user_email' => $email,
                        'user_firstname' => $user_firstname,
                        'user_lastname' => $user_lastname,
                        'user_type'  => 'Personal',
                        'status' => '1',
                        );
                $insert = $this->db->insert('users',$data);
                if($insert)
                {
                    $con['conditions'] = array(
                    'user_email' => $email,
                    
                    'user_type' =>'Personal',
                    );
                    $checkLogin = $this->User->getRows($con);
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                        $this->session->set_userdata('user_id', $checkLogin[0]['user_id']);
                        $this->session->set_userdata('timeout', time());
                        $uu_id = $checkLogin[0]['user_id'];
                        $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                        $ci_session['timestamp'] = time();
                        $ci_session['status'] = 0;
                        if (empty($ci_session_check)) {
                            $ci_session['user_id'] = $uu_id;
                            $this->db->insert("ci_sessions", $ci_session);
                        } else {
                            $this->db->where("user_id", $uu_id);
                            $this->db->update("ci_sessions", $ci_session);
                        }
                     redirect('dashboard');
                }

            }
        }



    }  

    function send_mail($email, $subject = "Test Mail", $message = "Testing mail") {

        $from = SENDER_EMAIL;

        //echo $message;die;
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                    $headers .= 'From: <"' . $from . '">' . "\r\n";
                    if(mail($email, $subject, $message, $headers)) {
                        echo "Message accepted";
                    } else {
                        print_r(error_get_last());
                    }


                    





//        $this->load->library('email');
//        $config['protocol'] = 'ssl';
//        $config['smtp_host'] = 'ssl://mail.gandi.net';
//        $config['smtp_port'] = 587;
//        $config['smtp_timeout'] = '7';
//        $config['smtp_user'] = $from;
//        $config['smtp_pass'] = 'puR1-7,XifsArlyV!5';
//        $config['charset'] = 'utf-8';
//        $config['newline'] = "\r\n";
//        $config['mailtype'] = 'html';
//        $config['validation'] = TRUE;
//        $this->email->initialize($config);
//        $fromname = SENDER_NAME;
//        $fromemail = SENDER_EMAIL;
//        $to = $email;
//        $subject = $subject;
//        $message = $msg;
//        $cc = false;
//        $this->email->from($fromemail, $fromname);
//        $this->email->to($to);
//        $this->email->subject($subject);
//        $this->email->message($msg);
//        if ($cc) {
//            $this->email->cc($cc);
//        }
//
//        if (!$this->email->send()) {
//            return $this->email->print_debugger();
//        } else {
//            return true;
//        }
    }

    /*
     * User login_do
     */

    public function login() {
        //echo "test";die;
        $data = array();
        header("Access-Control-Allow-Origin: *");
        $data['top_content']        = $this->Page->getTopContent();
        $this->template->set('title', 'Sign Up');
        $data['active_tab_login'] = "active";
        $data['authUrl'] =  $this->facebook->login_url();
        $this->template->load('home_layout', 'contents', 'login', $data);
    }

    public function do_login() {
        if ($this->input->post()) {
            $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
            $this->form_validation->set_rules("email", "email", "trim|required|valid_email", array("required" => "Please enter %s", "valid_email" => "Please enter valid %s"));
            $this->form_validation->set_rules("password", "password", "required", array("required" => "Please enter password"));

            if ($this->form_validation->run() == TRUE) {

                $data = array();
                $con['conditions'] = array(
                    'user_email' => $this->input->post('email'),
                    'user_password' => md5($this->input->post('password')),
                    'user_type' =>'Personal',
                );

                $redirecturl = $this->input->post('redirecturl') ;

                $checkLogin = $this->User->getRows($con);
                if ($checkLogin) {
                    if ($checkLogin[0]['status'] == 0) {
                        $this->session->set_flashdata('error', 'Your account is not activated yet, please check your mails or contact our administrator.');
                        redirect(base_url('login'));
                    } else if ($checkLogin[0]['status'] == 3) {
                        $this->session->set_flashdata('error', 'Your account is disabled');
                        redirect(base_url('login'));
                    } else {
                        $this->session->set_userdata('isUserLoggedIn', TRUE);
                        $this->session->set_userdata('user_id', $checkLogin[0]['user_id']);
                        $this->session->set_userdata('timeout', time());
                        $uu_id = $checkLogin[0]['user_id'];
                        $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                        $ci_session['timestamp'] = time();
                        $ci_session['status'] = 0;
                        if (empty($ci_session_check)) {
                            $ci_session['user_id'] = $uu_id;
                            $this->db->insert("ci_sessions", $ci_session);
                        } else {
                            $this->db->where("user_id", $uu_id);
                            $this->db->update("ci_sessions", $ci_session);
                        }

                        if($redirecturl)
                        {
                            redirect($redirecturl);
                        }
                        else
                        {
                            redirect(base_url('dashboard'));
                        }

                        
                    }
                } else {
                    $this->session->set_flashdata('error', 'Oops, please check your email or password.');
                    redirect(base_url('login'));
                }
            }
        }
    }

    /*
     * User registration
     */

    public function signup() {

        
        $data = array();
        if ($this->input->post()) {
            $this->form_validation->set_rules('user_firstname', 'First name', 'required', array(
                'required' => '%s is required.'
            ));
            $this->form_validation->set_rules('user_lastname', 'Last name', 'required', array(
                'required' => '%s is required.'
            ));
           /* $this->form_validation->set_rules('user_address', 'Address', 'required', array(
                'required' => '%s is required.'
            ));*/
            $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[users.user_email]', array(
                'required' => '%s is required.',
                'valid_email' => 'This %s not valid.',
                'is_unique' => 'This %s already exists.'
            ));
            $this->form_validation->set_rules('user_phone', 'Mobile number', 'required|numeric|is_natural|max_length[8]', array(
                'required' => '%s is required.',
                'numeric' => 'This %s not numeric value.',
                'is_natural' => 'Only natural numbers can be accepted.'
            ));
           /* $this->form_validation->set_rules('user_zipcode', 'Zip code', 'required|numeric|is_natural', array(
                'required' => '%s is required.',
                'numeric' => 'This %s not numeric value.',
                'is_natural' => 'Only natural numbers can be accepted.'
            ));*/
            $this->form_validation->set_rules('user_password', 'Password', 'required|min_length[6]', array(
                'required' => '%s is required.',
                'min_length' => 'This %s can not be less than 6 digit.'
            ));
            $this->form_validation->set_rules('conf_password', 'Confirm password', 'required|matches[user_password]', array(
                'required' => '%s is required.'
            ));


            $ip=$_SERVER['REMOTE_ADDR'];
            $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
            $lat = $geo["geoplugin_latitude"];
            $long = $geo["geoplugin_longitude"];

            $address  = $this->getAddress($lat,$long);
            //print_r("<pre/>");
            //print_r($address);die;

            //print_r("<pre/>");
            //print_r($geo);
            //die;

            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_email' => strip_tags($this->input->post('user_email')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => $address,
                'user_password' => md5($this->input->post('user_password')),
                //'user_zipcode' => $this->input->post('user_zipcode'),
                'user_plan' => 1,
                'status' => 0
            );
            if ($this->form_validation->run() == true) {
                $latlong = $this->User->get_lat_long($userData['user_address']);
                $userData['user_lat'] = $lat;
                $userData['user_long'] = $long;
                $userData['activation_code'] = $this->random_string(25);
                $insert = $this->User->insert($userData);
                if ($insert >= 0) {
                    $register_url = base_url('activate/' . $userData['activation_code']);
                    $from = 'ilbait@gmail.com';
                    $subject = "Email verification";
                    $email = strip_tags($userData['user_email']);
                    $message = "<p>Dear " . $userData['user_firstname'] . ",</p>";
                    $message .= "<p>Email verification <a href=" . $register_url . " target='_blank'>Click Here</a> .</p>";
                    $message .= "<p>If not able to click registration verification link copy link and page on browser:  '" . $register_url . "'</p>";
                    $message .= "<p><br></p>";
                    $message .= "<p>Thank You,</p>";
                    $message .= "<p>Team Ilbait</p>";
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                    $headers .= 'From: <"' . $from . '">' . "\r\n";
                    if (mail($email, $subject, $message, $headers)) {
                        echo "Message accepted";
                    } else {
                        print_r(error_get_last());
                    }
                    $this->session->set_flashdata('success', 'Your registration was successfully. Sent mail in your mail box please verify to login your account.');
                    redirect(base_url('signup'));
                } else {
                    $this->session->set_flashdata('error', 'Your registration was successfully. Sent mail in your mail box please verify to login your account.');
                    redirect(base_url('signup'));
                }
            }
        }
        header("Access-Control-Allow-Origin: *");
        $data['top_content']        = $this->Page->getTopContent();
        $this->template->set('title', 'Sign Up');
        //echo "test";die;
        $data['active_tab_signup'] = "active";
        $this->template->load('home_layout', 'contents', 'signup', $data);
    }
     function getAddress($latitude, $longitude) {
        if (!empty($latitude) && !empty($longitude)) {
            //Send request and receive json data by address
            $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false');
            $output = json_decode($geocodeFromLatLong);
            $status = $output->status;
            //Get address from json data
            $address = ($status == "OK") ? $output->results[1]->formatted_address : '';
            //Return address of the given latitude and longitude
            if (!empty($address)) {
                return $address;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function random_string($length) {
        $key = '';
        $keys = array_merge(range(0, 9), range('a', 'z'));

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        return $key;
    }

    /*
     * Existing email check during validation
     */

    public function email_check($str) {
        $con['returnType'] = 'count';
        $con['conditions'] = array('user_email' => $str);
        $checkEmail = $this->User->getRows($con);
        if ($checkEmail > 0) {
            $this->form_validation->set_message('email_check', 'The given email already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    /*
     * User registration
     */

    public function activate($str = "") {

        header("Access-Control-Allow-Origin: *");
        $str = $this->uri->segment(3);
        $active_key = $this->User->activateUser($str);
        if ($active_key) {

            $this->session->set_flashdata('success', 'Thanks for activating your account. Now you can login to your Dashboard');
            redirect(base_url('signup'));
        } else {
            $this->session->set_flashdata('error', "You'r account is already Activated");
            redirect(base_url('signup'));
        }
    }

    /*
     * Login with facebook
     */

    function fblogin() {
        $con = array(
            'user_email' => $this->input->post('email'),
                // 'social_id' =>$this->input->post('id'),
                // 'status' => '1'
        );

        $checkLogin = $this->User->socialLogin($con);


        if ($checkLogin->num_rows() > 0) {
            foreach ($checkLogin->result() as $user) {
                $userid = $user->user_id;
                $status = $user->status;
                $user_type = $user->user_type;
            }

            if ($status == 1 && $user_type == "Personal") {
                $this->session->set_userdata('isUserLoggedIn', TRUE);
                $this->session->set_userdata('user_id', $userid);
                $this->session->set_userdata("social_login", array("is_social" => true, "social_type" => "facebook"));
                $this->session->set_userdata('timeout', time());
                $array = array("is_online" => 1);
                $upd = $this->User->update($array, array($userid));
                $uu_id = $userid;
                $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                $ci_session['timestamp'] = time();
                $ci_session['status'] = 0;
                if (empty($ci_session_check)) {
                    $ci_session['user_id'] = $uu_id;
                    $this->db->insert("ci_sessions", $ci_session);
                } else {
                    $this->db->where("user_id", $uu_id);
                    $this->db->update("ci_sessions", $ci_session);
                }

                echo json_encode(array("flag" => 200));
            } else {
                echo json_encode(array("flag" => 1, "msg" => "This Email already exists."));
            }
        } else {
            $data = array("social_id" => $this->input->post("id"),
                "user_firstname" => $this->input->post("first_name"),
                "user_lastname" => $this->input->post("last_name"),
                "user_email" => $this->input->post("email"),
                "user_gender" => $this->input->post("gender"),
                "user_signup_method" => "Facebook",
                'created' => date("Y-m-d H:i:s"),
                "status" => 1
            );
            $this->db->insert("users", $data);
            $user_id = $this->db->insert_id();
            $this->createPasswordLink($user_id);
            $this->session->set_userdata('isUserLoggedIn', TRUE);
            $this->session->set_userdata('user_id', $user_id);
            $this->session->set_userdata("social_login", array("is_social" => true, "social_type" => "facebook"));
            $array = array("is_online" => 1);
            $upd = $this->User->update($array, array("user_id" => $user_id));

            $this->session->set_userdata('timeout', time());

            $uu_id = $user_id;
            $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
            $ci_session['timestamp'] = time();
            $ci_session['status'] = 0;

            if (empty($ci_session_check)) {
                $ci_session['user_id'] = $uu_id;
                $this->db->insert("ci_sessions", $ci_session);
            } else {

                $this->db->where("user_id", $uu_id);
                $this->db->update("ci_sessions", $ci_session);
            }

            echo json_encode(array("flag" => 200, "name" => "pradeep"));
        }
    }

    /*
     * Login with Google+
     */

    function googleLogin() {
        $login = $this->session->userdata("user_id");
        if (!empty($login)) {
            echo json_encode(array("flag" => 2));
        } else {
            $con = array(
                'user_email' => $this->input->post('email'),
                    // 'social_id' =>$this->input->post('id'),
                    // 'status' => '1'
            );

            $checkLogin = $this->User->socialLogin($con);
            if ($checkLogin->num_rows() > 0) {
                foreach ($checkLogin->result() as $user) {
                    $userid = $user->user_id;
                    $status = $user->status;
                    $user_type = $user->user_type;
                }
                if ($status == 1 && $user_type == "Personal") {
                    $this->session->set_userdata('isUserLoggedIn', TRUE);
                    $this->session->set_userdata('user_id', $userid);
                    $this->session->set_userdata("social_login", array("is_social" => true, "social_type" => "google"));
                    echo json_encode(array("flag" => 200));

                    $this->session->set_userdata('timeout', time());
                    $array = array("is_online" => 1);
                    $upd = $this->User->update($array, array($userid));
                    $uu_id = $userid;
                    $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                    $ci_session['timestamp'] = time();
                    $ci_session['status'] = 0;

                    if (empty($ci_session_check)) {
                        $ci_session['user_id'] = $uu_id;
                        $this->db->insert("ci_sessions", $ci_session);
                    } else {
                        $this->db->where("user_id", $uu_id);
                        $this->db->update("ci_sessions", $ci_session);
                    }
                    echo json_encode(array("flag" => 200));
                } else {
                    echo json_encode(array("flag" => 1, "msg" => "This Email already exists."));
                }
            } else {
                $data = array("social_id" => $this->input->post("id"),
                    "user_firstname" => $this->input->post("first_name"),
                    "user_lastname" => $this->input->post("last_name"),
                    "user_email" => $this->input->post("email"),
                    "user_signup_method" => "Google",
                    'created' => date("Y-m-d H:i:s"),
                    "status" => 1
                );

                $this->db->insert("users", $data);
                $user_id = $this->db->insert_id();
                $this->createPasswordLink($user_id);
                $this->session->set_userdata('isUserLoggedIn', TRUE);
                $this->session->set_userdata('user_id', $user_id);
                $this->session->set_userdata("social_login", array("is_social" => true, "social_type" => "google"));
                $array = array("is_online" => 1);
                $upd = $this->User->update($array, array("user_id" => $user_id));

                $this->session->set_userdata('timeout', time());

                $uu_id = $user_id;
                $ci_session_check = $this->db->get_where("ci_sessions", array("user_id" => $uu_id))->row();
                $ci_session['timestamp'] = time();
                $ci_session['status'] = 0;
                if (empty($ci_session_check)) {
                    $ci_session['user_id'] = $uu_id;
                    $this->db->insert("ci_sessions", $ci_session);
                } else {
                    $this->db->where("user_id", $uu_id);
                    $this->db->update("ci_sessions", $ci_session);
                }
                echo json_encode(array("flag" => 200));
            }
        }
    }

    /*
     * Send password to login with social media users
     */

    Function createPasswordLink($userid) {
        $con['conditions'] = array(
            'user_id' => $userid,
            'status' => '1'
        );
        $con['returnType'] = "single";
        $checkUser = $this->User->getRows($con);

        if ($checkUser) {
            $email = $checkUser['user_email'];
            $temppass = $this->generateRandomPass();
            $temppass_md5 = md5($temppass);
            $upd = $this->User->update_password($email, $temppass_md5);
            if ($upd >= 1) {
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id" => 26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id" => 14));
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id" => 25)); //Email Footer                
                $emailadmin = ADMIN_EMAIL;
                $subject = $templatecenter['tmplate_subject'];
                $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%Gender%%", "%%TempPassword%%");
                $real = array($checkUser['user_firstname'],
                    $checkUser['user_lastname'],
                    $checkUser["user_email"],
                    $checkUser['user_gender'],
                    $temppass
                );
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content']);
                $msg = $templateheader['tmplate_content'] . " " . $msg . " " . $templatefooter['tmplate_content'];
                //Send mail to userr
                $this->send_mail($email, $subject, $msg);
                $this->send_mail(ADMIN_EMAIL, $subject, $msg);
            }
        }
    }

    /*
     * User Social logout
     */

    public function socialLogout() {
        $uu_id = $this->session->userdata('user_id');
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('social_login');
        $this->session->sess_destroy();
        $ci_session['status'] = 1;
        $this->db->where("user_id", $uu_id);
        $this->db->update("ci_sessions", $ci_session);


        echo json_encode(array("status" => 200));
    }

    public function user_profile() {
        // die("Ok");
        $data = array();
        $is_claiming = "";
        $data['is_claiming'] = "";
        $user_id = $this->uri->segment(3);
        // $ses = $this->session->userdata("user_id");
        // if(!empty($ses)){
        //     if($user_id == $ses)
        //         redirect(base_url("Users/account"));
        // }
        $is_claiming = $this->uri->segment(4);

        if ($is_claiming == "claim_profile") {
            $data['is_claiming'] = "yes";
        }

        $data['user_phone'] = $this->User->getUserPhones($user_id);
        // $data['business'] = $this->User->getBusinessProfile(array('user_id'=>$user_id));
        $data['business'] = $this->User->getBusinessProfile(array("conditions" => array('user_id' => $user_id, "status!=" => 3), "returnType" => "single"));
        if (empty($data['business']))
            redirect(base_url());
        $data['similarprofile'] = $this->User->getSimilarBusinessProfile($user_id);
        $data['pics'] = $this->User->getProfilePics($user_id);

        $data['products'] = $this->User->getProducstServices($user_id, 2);
        $data['services'] = $this->User->getProducstServices($user_id, 3);
        $data['other'] = $this->User->getProducstServices($user_id, 4);

        $data['openhrs'] = $this->User->getOpeningHrs($user_id);
        $data["isopen"] = $this->is_open($data['openhrs']);
        $data["today_hrs"] = $this->today_hrs($data['openhrs']);

        $yes_online = $this->User->isBusinessOnline($user_id);
        $data['is_online'] = $yes_online;

        $data['prstable'] = $this->User->getBusinessPriceRows(array('user_id' => $user_id));
        $data['reviewlist'] = $this->User->getReviewRows(array("conditions" => array("voted_user_id" => $user_id, "vote_status" => 1)));
        $data['TotalReview'] = $this->User->getTotalReview($user_id);
        $data['pagecontent'] = $this->User->getTextContent(10); // get page content        
        $data['rccontent'] = $this->User->getTextContent(16); // get Recommend content        
        $data['flcontent'] = $this->User->getTextContent(17); // get Follow content        
        $data['msgcontent'] = $this->User->getTextContent(18); // get messagepopup text content        
        $data['events'] = $this->User->getEvents($user_id);

        $follower_id = $this->session->userdata('user_id') != "" ? $this->session->userdata('user_id') : 0;

        $fol = $this->User->isFollowing($user_id, $follower_id);
        if ($fol == true) {
            // echo "Wow";
            $data['followed'] = "yes";
        } else {
            //echo "ohh";
            $data['followed'] = "no";
        }

        $data['rec_count'] = $this->User->getrecommendUser($user_id);

        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Business Listing');
        $this->template->load('home_layout', 'contents', 'profile', $data);
    }

    public function generateRandomPass() {
        $length = 8;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&()}{][';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
	
	public function addListingView() {



        $user_id = $this->session->userdata('user_id');
        $this->db->select('user_id');
        $this->db->from('users');
        $this->db->where('user_id',$user_id);
        $que = $this->db->get();
        $result = $que->result();



        $this->db->select('business_email');
        $this->db->from('bdc_business');
        //$this->db->where('status!',3);
        $this->db->where('user_id',$result[0]->user_id);

        $this->db->where('status!=',3);
        $que1 = $this->db->get();
        $result1 = $que1->result();
        $count = count($result1);

        //echo $count;die;

        if($count){
            $this->session->set_flashdata('error', 'You have already added listing, if you want add more listing ,create new users');
            redirect(base_url('dashboard'));
        } 

		$data = array(); 
		$data['category'] = $this->User->showcategory();
        $data['country'] = $this->User->showcountry();
        $data['top_content']        = $this->Page->getTopContent();
        $data['listing_content']        = $this->Page->getListingContent();

        //print_r("<pre/>");
        //print_r($data['listing_content']);
        //die;
		//sidemenu.php
        $this->template->load('home_layout', 'contents', 'add_listing', $data);
    }
    public function plan(){

        $user_id = $this->session->userdata('user_id');


        $data['packageListings']             = $this->Page->getPackageDetails();
       $data['top_content']        = $this->Page->getTopContent();
        $data['listing_content']        = $this->Page->getListingContent();
        $data['dashboard_content']        = $this->Page->getDashboardContent();

        $data['user_detail'] = $this->Page->getProfileDetail($user_id);
        
        //print_r("<pre/>"); 
        //print_r($data['dashboard_content']);die;
        $this->template->load('home_layout', 'contents', 'add_plan',$data);
    }

    public function viewcity() {
        $data = array();
        $hps_level = $this->input->post('hps_level');
        $data['view'] = $this->User->viewcity($hps_level);
      } 
	
	public function add_listing(){
	    
	    
	    
	    //$address  = $this->input->post("location");
	    
	    //$resArr = getLocationLatLng($address);
	    //print_r("<pre/>");
	   // print_r($resArr);
	    //die;

       $user_id = $this->session->userdata('user_id');


        $this->db->select('user_email');
        $this->db->from('users');
        $this->db->where('user_id',$user_id);
        $que = $this->db->get();
        $result = $que->result();

       // print_r($result[0]->user_email);die;


        $this->db->select('business_email');
        $this->db->from('bdc_business');
        $this->db->where('business_email',$result[0]->user_email);
        $que1 = $this->db->get();
        $result1 = $que1->result();

        //print_r("<pre/>");
        //print_r($result1);
        //die;



        
        //print_r("<pre/>");
        //print_r($result[0]->user_email);die;

        $count = count($result1);



        $this->db->select('userid');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $que2 = $this->db->get();
        $result2 = $que2->result();

        $count1 = count($result2);

        
        //if($count){
            // $this->session->set_flashdata('error', 'You have already added listing, if you want add more listing ,create new users');
            // redirect(base_url('addlisting'));

        //}
        if(!$count1){
            redirect(base_url('dashboard'));
        }

        $address_type = $this->input->post('cars');

         if($address_type == 3)
            {
                $address_typ = 1;
                $autoloc = $this->input->post("location");
                if($autoloc)
                {
                    $autolocation = explode(', ', $autoloc);
                    $count =count($autolocation);
                    $len =$count-3;
                    $full_address = $this->input->post("location");
                    $cityId = $autolocation[$len];
                    $country = end($autolocation); 
                        $this->db->select('*');
                        $this->db->from('countries');
                        $this->db->where('name', $country);
                        $query = $this->db->get();
                        $listingCount = $query->num_rows();
                        if($listingCount)
                       {
                            $country_arry = $query->result_array();

                            foreach ($country_arry as  $valuecountry) {
                                $countyId = $valuecountry['id'];
                            }
                      }
                        else
                      {
                            $this->session->set_flashdata('error', 'This Country Not in Country Management!!');
                            redirect(base_url('addlisting'));
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'Please Enter Address!!');
                      redirect(base_url('addlisting'));
                }
               

            }
        else 
            
            {
                $address_typ = 0;
                if($this->input->post("business_city") && $this->input->post("business_country") && $this->input->post("business_address") )
                {
                    $cityId  = $this->input->post("business_city");
                    $country = $this->input->post("business_country");
                    $full_address = $this->input->post("business_address");
                }
                else
                {
                    $this->session->set_flashdata('error', 'Please Enter Address , Country , City !!');
                      redirect(base_url('addlisting'));

                }

            }



        // $data['upload_path'] = 'uploads/business/'; 
        // $data['allowed_types'] = 'jpg|jpeg|png|gif';
        // $data['encrypt_name'] = true;
        // $this->load->library('upload',$data);
        // $target_dir = "uploads/business/";
        // $size =$_FILES['offer']['size'] ;
        // $target_file = $target_dir . basename($_FILES["offer"]["name"]);
        // //if($size  <   1000 ){
        //     if (move_uploaded_file($_FILES["offer"]["tmp_name"], $target_file)) {
        //        $offerFile = $_FILES["offer"]["name"];
               

        //     }
       // }

        // else{
        //         $this->session->set_flashdata('error', 'offer image too large!!');
        //     redirect(base_url('addlisting'));
        //   }

		
		$data['upload_path'] = 'uploads/business/'; 
		$data['allowed_types'] = 'jpg|jpeg|png|gif';
		$data['encrypt_name'] = true;

		$this->load->library('upload',$data);

		$uploadfile ="";
		$uploadfile2 ="";
		$uploadfile3 ="";
		$uploadfile4 ="";
		if($this->upload->do_upload('userfile'))
		{ 
			$attachment_data = array('upload_data' => $this->upload->data());
			$uploadfile = $attachment_data['upload_data']['file_name'];
		  		 
		}		
		if($this->upload->do_upload('userfile2'))
		{ 
			$attachment_data = array('upload_data' => $this->upload->data());
			$uploadfile2 = $attachment_data['upload_data']['file_name'];
		  		 
		}
		if($this->upload->do_upload('userfile3'))
		{ 
			$attachment_data = array('upload_data' => $this->upload->data());
			$uploadfile3 = $attachment_data['upload_data']['file_name'];
		  		 
		}
		if($this->upload->do_upload('userfile4'))
		{ 
			$attachment_data = array('upload_data' => $this->upload->data());
			$uploadfile4 = $attachment_data['upload_data']['file_name'];
		  		 
		}


        if($this->upload->do_upload('userfile5'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile5 = $attachment_data['upload_data']['file_name'];
                 
        }

        if($this->upload->do_upload('userfile6'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile6 = $attachment_data['upload_data']['file_name'];
                 
        }

        if($this->upload->do_upload('userfile7'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile7 = $attachment_data['upload_data']['file_name'];
                 
        }

         if($this->upload->do_upload('userfile8'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile8 = $attachment_data['upload_data']['file_name'];
                 
        }

         if($this->upload->do_upload('userfile9'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile9 = $attachment_data['upload_data']['file_name'];
                 
        }

          if($this->upload->do_upload('userfile10'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile10 = $attachment_data['upload_data']['file_name'];
                 
        }

          if($this->upload->do_upload('imageIcon'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $imageIcon = $attachment_data['upload_data']['file_name'];
                 
        }

		$data = $this->User->add_listing1($uploadfile, $uploadfile2, $uploadfile3, $uploadfile4 ,$uploadfile5 ,$uploadfile6,$uploadfile7,$uploadfile8,$uploadfile9,$uploadfile10,$imageIcon ,$address_typ,$cityId,$country,$full_address);
		if($data){		

            $business_id  = base64_encode($data);
            //if($this->input->post('features')==1){
			    //redirect(base_url('Users/featured_plan/'.$business_id));
            //}else{
                $this->session->set_flashdata('success', 'Listing has been added successfully');
                 redirect(base_url('dashboard'));
            //}     
		}else{
			$this->session->set_flashdata('error', 'Something went wrong');
			redirect(base_url('addlisting'));
		}
		
	}
     public function featured_plan($id=false){
        $business_id = base64_decode($id);
        $user_id = $this->session->userdata('user_id');
        $data['packageListings']             = $this->Page->getPackageDetails1();

        $data['top_content']        = $this->Page->getTopContent();
        $data['listing_content']        = $this->Page->getListingContent();
        $data['dashboard_content']        = $this->Page->getDashboardContent();
        $data['user_detail'] = $this->Page->getProfileDetail($user_id);
        $data['business_id'] = $business_id;
        $data['freeplanpurches'] = $this->User->freeplanpurches($user_id);


        $data['getranstion'] = $this->User->fetch_transactionRecord1($user_id);
       

        $this->db->select('*');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $this->db->where('transaction_table.package_type=',4);
        $checkFreePlanQuery = $this->db->get();
        $checkFreePlanResult =  $checkFreePlanQuery->result();
        $data['checkFreePlan'] = $checkFreePlanResult;


        $this->template->load('home_layout', 'contents', 'add_plan3',$data);
        //echo $user_id;die;
    }

       public function paymentsuccessfeature()
    {

        //$q = 5;
        //$p = '+ '.$q.' days';
        //$Timestamp = strtotime(date('Y-m-d'));
        //$TotalTimeStamp = strtotime($p, $Timestamp);
        //echo date('d-m-Y', $TotalTimeStamp);die;


        $amt = $_GET['amt'];
        $cc = $_GET['cc'];
        $cm = $_GET['cm'];
        $var = explode("-",$cm);
         $userid =  $var[0];
         $packageid =  $var[1];
         $duration =  $var[2];
         $business_id =  $var[3];

        $item_name = $_GET['item_name'];
        $st = $_GET['st'];
        $tx = $_GET['tx'];

        //$duration = 10;


        //cho "test";die;

        $p = '+ '.$duration.' days';
        $Timestamp = strtotime(date('Y-m-d'));
        $TotalTimeStamp = strtotime($p, $Timestamp);
        $free_end = date('Y-m-d', $TotalTimeStamp);

       //echo $free_end;die;

        $data = array(
                 'amt' => $amt,
                 'cc' =>  $cc,
                 'userid' => $userid,
                 'packageid' => $packageid,
                 'item_name' => $item_name,
                 'st' => $st,
                 'transaction_id' => $tx,
                 'free_start' => date('Y-m-d'),
                 'free_end' => $free_end,
                 'business_id' => $business_id,
                 'package_type' => 4,
                 'plan_valid'=>1,
         );
         $sql = $this->db->insert('transaction_table', $data);
         if($sql)
         {
            $this->session->set_flashdata('success', 'Your Payment is done');
            redirect(base_url("dashboard"));
         }

    }

}
