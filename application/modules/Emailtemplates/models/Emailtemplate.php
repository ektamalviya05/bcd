<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailtemplate_model extends CI_Model {

	public function insertData($table,$datainsert)
	{
		$this->db->insert($table,$datainsert);
		return $this->db->insert_id();
	}
	
	public function updateData($table,$data,$where)
	{
	   $this->db->update($table,$data,$where);
	   return;
	}
	

	public function getsingle($table,$where=null)
	{
		$q = $this->db->get_where($table,$where);
		return $q->row();
	}
	
	
		
}