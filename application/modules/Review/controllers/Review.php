<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Review extends MX_Controller
{
    
 function __construct() {
        parent::__construct();
        
        $this->load->library('session');
      $this->load->model('Review_model');
      $this->load->helper('url');
      $this->load->helper('csv');
      $this->load->library('form_validation');  
      $this->load->library('pagination');
    }
    /* function index() {
        
    }*/
    public function index() {
         $data= array();
         //$con = array();
         $data['getreviewlist'] = $this->Review_model->getreviewlist();
         $count = count($data['getreviewlist']);

         $this->template->set('title', 'Review Section');   
         $this->template->load('admin_layout', 'contents' , 'list', $data);
    }
 
    public function fetch_review()
    {
        $data =array();
        $data['notification'] = $this->Review_model->getnotification();
        //print_r( $data['notification']);
        $i=0;
        foreach ($data['notification'] as $value)
        {
            if($i <= 5) 
            {
        ?>
     
                <li>
                    <a href="<?php echo base_url(); ?>Review/update/<?php echo $value['id'] ?>"><i class="fa fa-fw fa-user"></i><?php echo $value['user_firstname'].' '.$value['user_lastname'];  ?> Give Review on <?php echo $value['business_title'];  ?> </a>
                </li>
                               
            <?php
            }
            $i++;
        }

    }
    public function section() 
    {
        
        $data= array();
        $data['getratingstatus'] = $this->Review_model->getratingstatus();
        header("Access-Control-Allow-Origin: *");
  		$this->template->set('title', 'Review Section');
        $this->template->load('admin_layout', 'contents' , 'notification', $data);
    }



    public function update($var) {

        $data= array();
        
        $data['getreviewlist'] = $this->Review_model->getreviewlistuser($var);
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Review List');
        $this->template->load('admin_layout', 'contents' , 'list', $data);

    }


    public function countnotification() {
     $data =array();
     $data['countnotification'] = $this->Review_model->countnotification();

    }

    public function changestatus(){
        $id = $this->input->post("id");
        if(!empty($id)){
            $con['conditions'] = array("module_id"=>$id);
            //$check = $this->Categories->getRow($con);
            if(1){
                $array = array("show_front"=>$this->input->post("mode"));
                $opr = 2; //Update mode
                $check = $this->Review_model->insertTable($opr, $array, array("module_id"=>$id));
                if($check)
                    echo json_encode(array("status"=>200));
            }
            else{
                echo json_encode(array("status"=>1));
            }
        }
        else{
            echo json_encode(array("status"=>1));
        }
    }


     public function changereviewstatus(){
        $id = $this->input->post("id");
        if(!empty($id)){
            $con['conditions'] = array("module_id"=>$id);
            //$check = $this->Categories->getRow($con);
            if(1){
                $array = array("status"=>$this->input->post("mode"));
                $opr = 2; //Update mode
                $check = $this->Review_model->changeTable($opr, $array, array("id"=>$id));
                if($check)
                    echo json_encode(array("status"=>200));
            }
            else{
                echo json_encode(array("status"=>1));
            }
        }
        else{
            echo json_encode(array("status"=>1));
        }
    }
    public function reviewlist_bkup(){
         $data= array();
         //$con = array();
         $data['getreviewlist'] = $this->Review_model->getreviewlist();
         $count = count($data['getreviewlist']);

         $this->template->set('title', 'Review Section');   
         $this->template->load('admin_layout', 'contents' , 'list', $data);
    }

     public function reviewlist(){
        $data= array();
        $config = array();
        $config["base_url"] = base_url() . "Review/reviewlist";
        $config["total_rows"] = $this->Review_model->fetch_reviewlistone();
        //print_r($config["total_rows"]);
        //die();
        $config["per_page"] = 10;
        $config["uri_segment"] = 3;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '<b> Next </b>';
        $config['prev_link'] = '<b> Previous </b>';

        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
        $this->db->limit( $config['per_page'], $page );

        $data["results"] = $this->Review_model->fetch_reviewlist($config['per_page'], $page );

        //print_r("<pre/>");
        //print_r($data["results"]);
        //die();

        $data["links"] = $this->pagination->create_links();
        
      

         $this->template->set('title', 'Review Section');   
         $this->template->load('admin_layout', 'contents' , 'list', $data);
    }

    public function enquiry_bkup(){
         $data= array();
          
         $data['getenquirylist'] = $this->Review_model->getenquirylist();
         $this->template->set('title', 'Enquiry Section');   
         $this->template->load('admin_layout', 'contents' , 'enquirylist', $data);
    }

    public function enquiry(){
        $data =array();
        $config = array();
        $config["base_url"] = base_url() . "enquiry";
        $config["total_rows"] = $this->Review_model->record_count();
        $config["per_page"] = 5;
        $config["uri_segment"] = 2;

        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '<b> Next </b>';
        $config['prev_link'] = '<b> Previous </b>';


        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["results"] = $this->Review_model->fetch_enquiry($config["per_page"], $page);
        $data["links"] = $this->pagination->create_links();
        $this->template->set('title', 'Enquiry Section');   
        $this->template->load('admin_layout', 'contents' , 'enquirylist', $data);
    }

    public function transactionRecord(){


         $filter = '';
        if(!empty($this->session->userdata("filterpackage"))){
            $filter['item_name'] = $this->session->userdata("filterpackage");
        }
        if(!empty($this->session->userdata("filterusername"))){

            $filter['user_firstname'] =  $this->session->userdata("filterusername");
        }
        if(!empty($this->session->userdata("filterdate"))){
             $dateTime = date("Y-m-d ", strtotime($this->session->userdata("filterdate"))); 
             $filter['create_date'] =  $dateTime;

        }
        $data =array();
        $config = array();
        $config["base_url"] = base_url() . "transactionRecord";
        $config["total_rows"] = $this->Review_model->record_count_transactionRecord($filter);
        $config["per_page"] = 10;
        $config["uri_segment"] = 2;

        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '<b> Next </b>';
        $config['prev_link'] = '<b> Previous </b>';


        $this->pagination->initialize($config);
        $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : 0;
        $data["results"] = $this->Review_model->fetch_transactionRecord($config["per_page"], $page,$filter);
        //print_r($data["results"]);
        //die();
        $data["links"] = $this->pagination->create_links();
         $this->template->set('title', 'Transaction Record');   
        $this->template->load('admin_layout', 'contents' , 'transactionRecord', $data);
    }
    function setFilterDelimiter(){


        $package = $this->input->post("package");
        $username = $this->input->post("username");
        $date = $this->input->post("date");
        //echo $package;die;
        if(!empty($package)) {
            $this->session->set_userdata("filterpackage", $package);
        }
        if(!empty($username))  {
            $this->session->set_userdata("filterusername", $username);
        }
         if(!empty($date))  {
            $this->session->set_userdata("filterdate", $date);
        }

        redirect(base_url("transactionRecord"));
    }
    function clearFilterDelimiter(){
        $this->session->unset_userdata("filterpackage");
        $this->session->unset_userdata("filterusername");
        $this->session->unset_userdata("filterdate");
        redirect(base_url("transactionRecord"));
    }
 
 
}

