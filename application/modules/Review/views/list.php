<div class="dash-counter users-main">
  <h2>Review From Users</h2>
  <div class="user-table table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>                              
        <tr >
          <th>No.</th>
          <th>Info User & Listing</th>
          <th>Review </th>
          <th>Rating</th>                                
          <th>Date</th>
          <th >Action</th>
        </tr>
        <?php //echo "<pre>";print_r($getreviewlist); ?>
        <?php 
        if($results)
        {


        ?>
        <?php $i=1;
        foreach ($results as $value) {
        
        ?>
        <tr>
          <td ><?php echo $i; ?></td>
          <td ><?php echo '<b style="text-transform: capitalize;">'.$value->user_firstname.' '.$value->user_lastname.'</b> give review on <b style="text-transform: capitalize;">'.$value->business_title.'</b>'; ?></td>
          <td ><?php echo $value->description;?></td>
          <td ><?php echo $value->rating.' Star'; ?></td>
          <td ><?php echo date("d F, Y H:i", strtotime($value->create_date)); ?></td>
          <td ><input type="checkbox" class="review_ed" modal-aria="<?php echo $value->id; ?>" <?php if($value->status) echo "checked"; ?> data-on="Approve" data-off="Disapprove" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning"></td>
        <?php
        $i++;
        } 

        ?>  
          
       </tr>
       <?php
       }
       else
       {
         echo "Sorry !! No record found .";
       }
       ?>
      </tbody>
    </table>

    <div class="read-more">
      <?php echo $links; ?>
    </div>
    
  </div>
</div>