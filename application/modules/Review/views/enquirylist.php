<div class="dash-counter users-main">
  <h2>Enquiry Detail </h2>
  <div class="user-table table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>                              
        <tr >
          <th>No.</th>
          <th>Name</th>
          <th>Email</th>
          <th>Message</th>                                
          <th>Listing info</th>
        </tr>
        <?php 
        if($results)
        {
        $i=1;
        foreach ($results as $value) {
        $dateTime = date("d F, Y H:i", strtotime($value->created));
        ?>
        <tr>
          <td ><?php echo $i; ?></td>
          <td ><?php echo '<b style="text-transform: capitalize;">'.$value->name.'</b>'; ?></td>
          <td ><?php echo $value->email;?></td>
          <td ><?php echo $value->message; 
                     echo '<br/>';
                     echo '<h5>Date/Time :</h5><b style="text-transform: capitalize;">'.$dateTime.'</b>';

          ?></td>
          <td><?php echo '<h5>Listing Name :</h5><b style="text-transform: capitalize;">'.$value->business_title.'</b>';
                    echo '<br/>';
                    echo '<h5>Listing Email :</h5><b style="text-transform: capitalize;">'.$value->business_email.'</b>';
                    echo '<br/>';
                    echo '<h5>Listing Phone :</h5><b style="text-transform: capitalize;">'.$value->business_mobile.'</b>';
                    echo '<br/>';
                    echo '<h5>Listing Address :</h5><b style="text-transform: capitalize;">'.$value->business_address.'</b>';
          ?>
            
          </td>
         
        <?php
        $i++;
        } 
        ?>
        <tr>
        <?php 

        }else
        {
          echo "<span class='resultfound' style='position: absolute;top: 57px;'>Sorry No Result Found !!</span>";
        }
          ?>  
          
        
      </tbody>

    </table>
    <div class="read-more">
      <?php echo $links; ?>
    </div>
    
  </div>
</div>