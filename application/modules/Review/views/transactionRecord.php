 <div class="import-create dash-counter">        
        <form action="<?php echo base_url("Review/setFilterDelimiter"); ?>" method="post" id="filterForm">
          <div class="row">
            <div class="form-group col-md-4" >
              <input type="text" autocomplete="off" class="form-control" name="package" id="category"  placeholder="Package Name" value="<?php echo $this->session->userdata('filterpackage'); ?>">
            </div>
            <div class="form-group col-md-4" >
                <input type="text" class="form-control" name="username" id="user_firstname"  placeholder="User Name"  
                       value="<?php echo $this->session->userdata('filterusername') ?>">

            </div>
            <div class="form-group col-md-4" >
                <input type="text" class="form-control" name="date" id="user_firstname"  placeholder="Date"  
                       value="<?php echo $this->session->userdata('filterdate') ?>">

            </div>
            <div class="form-group col-md-5">
              <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn btn-primary th-primary" value="Filter"/>               
                       
              <a href="<?php echo base_url("Review/clearFilterDelimiter"); ?>"  class="btn btn-danger th-danger">Clear Filter</a>
            </div>
          </div>
        </form>        
      </div>
<div class="dash-counter users-main">
  <h2>Transaction History </h2>
  <div class="user-table table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>                              
        <tr >
           <th>Serial No.</th>
          <th>Transaction Date</th>
          <th>User Name</th>
          <th>Package Name</th>
          <th>Transaction No.</th>                                
          <th>Transaction Status</th>
        </tr>
        <?php
         $segment  = $this->uri->segment(2);
         
         $i=1;
         if($segment){
                  $i = $segment+1;
                }
        foreach ($results as $value) {
        $dateTime = date("d F, Y ", strtotime($value->create_date));
        ?>
        <tr>
          <td ><?php echo $i; ?></td>
          <td ><?php echo $dateTime; ?></td>
          <td ><?php echo '<b style="text-transform: capitalize;">'.$value->user_firstname.'</b>'; ?></td>
          <td ><?php echo $value->item_name;?></td>
          <td ><?php echo $value->transaction_id; 

          ?></td>
          <td><?php echo '<b style="text-transform: capitalize;">'.$value->st.'</b>';
            
          ?>
            
          </td>
         
        <?php
        $i++;
        } 
        ?>  
          
       </tr>
      </tbody>
    </table>
    <div class="read-more">
      <?php echo $links; ?>
    </div>
    
  </div>
</div>