<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Review_model extends CI_Model {

  

    function __construct() {

        $this->userTbl = 'users';
        $this->businessReview = 'business_review';
        
    }

    function getnotification()
    {
        $this->db->select("*");
        $this->db->from('users');
        $this->db->join('business_review','business_review.user_id =users.user_id');
        $this->db->join('bdc_business','business_review.business_id =bdc_business.business_id');
        $this->db->where('business_review.status=',0);
        $this->db->order_by('business_review.id', "DESC");
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //echo "<pre>";
        //print_r($result);
        //die();
        return $result;
    }

    function getreviewlist()
    {
        $this->db->select("users.user_firstname,
							users.user_lastname,
							bdc_business.business_title,
							business_review.description,
							business_review.rating,
							business_review.create_date,
							business_review.id,
							business_review.status");
        $this->db->from('users');
        $this->db->join('business_review','business_review.user_id =users.user_id');
        $this->db->join('bdc_business','business_review.business_id =bdc_business.business_id');
        //$this->db->where('business_review.status=',0);
        $this->db->order_by('business_review.id', "DESC");

        $query = $this->db->get();
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
        }
        $result = $query->result_array();
        //echo $this->db->last_query();
        //echo "<pre>";
        //print_r($result);
        //die();
        return $result;
    }

    function getreviewlistuser($var=0)
    {
        $this->db->select("users.user_firstname,
							users.user_lastname,
							bdc_business.business_title,
							business_review.description,
							business_review.rating,
							business_review.create_date,
							business_review.id,
							business_review.status");
        $this->db->from('users');
        $this->db->join('business_review','business_review.user_id =users.user_id');
        $this->db->join('bdc_business','business_review.business_id =bdc_business.business_id');
        $this->db->where('business_review.id=',$var);
        $this->db->order_by('business_review.id', "DESC");
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //echo "<pre>";
        //print_r($result);
        //die();
        return $result;
    }
    public function countnotification()
    {
        $this->db->select("*");
        $this->db->from('users');
        $this->db->join('business_review','business_review.user_id =users.user_id');
        $this->db->where('business_review.status=',0);
        $this->db->order_by('business_review.id', "DESC");
        $query = $this->db->get();
        $result= $query->num_rows();
        
         echo $result;
        
    }

    function insertTable($opr = 1, $para = array(), $where = array()){
        if($opr == 1){ // insert testimonial
            $this->db->insert($this->tbName, $para);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ // update existing testimonial
            $this->db->update('show_module', $para, $where);
            return true;
        }
    }

    function changeTable($opr = 1, $para = array(), $where = array()){
        if($opr == 1){ // insert testimonial
            $this->db->insert($this->tbName, $para);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ // update existing testimonial
            $this->db->update('business_review', $para, $where);
            echo $this->db->last_query();
            die();
            return true;
        }
    }

    function getratingstatus()
    {
        $this->db->select("*");
        $this->db->from('show_module');
        $this->db->where('module_id',1);
        $query = $this->db->get();
        $result= $query->result_array();
        return $result;

    }


    function getenquirylist()
    {
        $this->db->select("bdc_enquiry.created,
                           bdc_enquiry.name,
                           bdc_enquiry.email,
                           bdc_enquiry.message,
                           bdc_business.business_title,
                           bdc_business.business_email,
                           bdc_business.business_mobile,
                           bdc_business.business_address,
            ");
        $this->db->from('bdc_enquiry');
        $this->db->join('bdc_business' ,'bdc_business.business_id = bdc_enquiry.business_id');
        $query = $this->db->get();
        $result= $query->result_array();
        //echo $this->db->last_query();
        //die();
        return $result;

    }

    public function record_count() {
    //return $this->db->count_all("bdc_enquiry");
       $this->db->limit($limit, $start);
        $this->db->select("bdc_enquiry.created,
                           bdc_enquiry.name,
                           bdc_enquiry.email,
                           bdc_enquiry.message,
                           bdc_business.business_title,
                           bdc_business.business_email,
                           bdc_business.business_mobile,
                           bdc_business.business_address,
            ");
        $this->db->from('bdc_enquiry');
        $this->db->join('bdc_business' ,'bdc_business.business_id = bdc_enquiry.business_id');
        $query = $this->db->get();
        return $query->num_rows();
    }

     public function record_count_review() {
      $this->db->select('*');
      $this->db->from('business_review');
      $this->db->join('users','business_review.user_id =users.user_id');
      //$this->db->where('users.status=','1');
      $query = $this->db->get();
      return $query->num_rows();
      //return $this->db->count_all("business_review");


    }

    //  public function record_count_transactionRecord() {
    // return $this->db->count_all("transaction_table");
    // }
    public function record_count_transactionRecord($filter) 
    {
      $this->db->select('*');
      $this->db->from('transaction_table');
      $this->db->join('users','transaction_table.userid =users.user_id');
      //$this->db->where('users.status=','1');

       if($filter){
       $this->db->where($filter);  
      }
      $query = $this->db->get();
      return $query->num_rows();

    }

    public function fetch_enquiry($limit, $start) {
 
       $this->db->limit($limit, $start);
        $this->db->select("bdc_enquiry.created,
                           bdc_enquiry.name,
                           bdc_enquiry.email,
                           bdc_enquiry.message,
                           bdc_business.business_title,
                           bdc_business.business_email,
                           bdc_business.business_mobile,
                           bdc_business.business_address,
            ");
        $this->db->from('bdc_enquiry');
        $this->db->join('bdc_business' ,'bdc_business.business_id = bdc_enquiry.business_id');
        $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
           foreach ($query->result() as $row) {
 
               $data[] = $row;
 
           }
 
           return $data;
 
       }
 
       return false;
 
   }


   public function fetch_reviewlist($limit, $start) {
 
      $this->db->limit($limit, $start);
       $this->db->select("users.user_firstname,
                            users.user_lastname,
                            bdc_business.business_title,
                            business_review.description,
                            business_review.rating,
                            business_review.create_date,
                            business_review.id,
                            business_review.status");
        $this->db->from('users');
        $this->db->join('business_review','business_review.user_id =users.user_id');
        $this->db->join('bdc_business','business_review.business_id =bdc_business.business_id');
        
        $this->db->order_by('business_review.id', "DESC");
        $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
           foreach ($query->result() as $row) {
 
               $data[] = $row;
 
           }
 
           return $data;
 
       }
 
       return false;
 
   }

   public function fetch_reviewlistone() {
 
      //$this->db->limit($limit, $start);
       $this->db->select("users.user_firstname,
                            users.user_lastname,
                            bdc_business.business_title,
                            business_review.description,
                            business_review.rating,
                            business_review.create_date,
                            business_review.id,
                            business_review.status");
        $this->db->from('users');
        $this->db->join('business_review','business_review.user_id =users.user_id');
        $this->db->join('bdc_business','business_review.business_id =bdc_business.business_id');
        
        $this->db->order_by('business_review.id', "DESC");
        $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
              //foreach ($query->result() as $row) {
 
               //$data[] = $row;
 
           //}
 
           return $query->num_rows();
 
       }
 
       return false;
 
   }


   public function fetch_transactionRecord($limit, $start,$filter) {
 
       $this->db->limit($limit, $start);
        $this->db->select("users.user_firstname,
                          users.user_lastname,
                         
                          transaction_table.transaction_id,
                         transaction_table.amt,
                         transaction_table.create_date,
                         transaction_table.st,
                         transaction_table.item_name,
                        ");
        $this->db->from('transaction_table');
        $this->db->join('users','users.user_id = transaction_table.userid');
        //$this->db->join('otd_option_master','otd_option_master.opt_id = transaction_table.packageid');
        //$this->db->where('otd_option_master.opt_status=','1')
       //  $this->db->order_by('transaction_table.create_date', "DESC");

         if($filter){

         //print_r
          $this->db->where($filter);
        }
       $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
           foreach ($query->result() as $row) {
 
               $data[] = $row;
 
           }
 
           return $data;
 
       }
 
       return false;
 
   }
    
 

    

 
}
