<?php
defined("BASEPATH") OR exit("No direct script access allowed");
class Testimonial extends MX_Controller{
	function __construct(){
		parent::__construct();
		$this->load->library("session");
		$this->load->model("Testimonials");
		$this->load->library("form_validation");
	}

	/*
	* Show Testimonials listing
	*/
	function index(){
		$data = array();
		$con['conditions'] = array("tst_status !="=>3);
		$cont['sorting'] = array("tst_date"=>"DESC");
		$data['details'] = $this->Testimonials->getRows($con);
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Testimonials');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
	}

	/*
	* Add Testimonials
	*/
	function add(){
		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
		$this->form_validation->set_rules("testi", "testimonial", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("name", "user name", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("designation", "designation", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("status", "status", "required", array("required"=>"Please enter %s"));
		if($this->form_validation->run() == FALSE){
			$data = array();
			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Content Management');
		    $this->template->load('admin_layout', 'contents' , 'add', $data);
		}
		else{
			$array = array("tst_content"=>$this->input->post("testi"),
						   "tst_by"=>$this->input->post("name"),
						   "tst_designation"=>$this->input->post("designation"),
						   "tst_status"=>$this->input->post("status")
						  );
			$opr = 1; //Insert mode
			$check = $this->Testimonials->insertTable($opr, $array);
			if(!$check){
				$this->session->set_flashdata("success", "Testimonial added successfully");
			}else{
				$this->session->set_flashdata("success", "Testimonial not added, Please try again");
			}
			redirect(base_url("Testimonial"));
		}
	}

	/*
	* Update Testimonials
	*/
	function edit($args){
		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
		$this->form_validation->set_rules("testi", "testimonial", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("name", "user name", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("designation", "designation", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("status", "status", "required", array("required"=>"Please enter %s"));
		if($this->form_validation->run() == FALSE){
			$data = array();
			$con['conditions'] = array("tst_id"=>$args);
			$data['details'] = $this->Testimonials->getRows($con);
			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Content Management');
		    $this->template->load('admin_layout', 'contents' , 'edit', $data);
		}
		else{
			$array = array("tst_content"=>$this->input->post("testi"),
						   "tst_by"=>$this->input->post("name"),
						   "tst_designation"=>$this->input->post("designation"),
						   "tst_status"=>$this->input->post("status")
						  );
			$opr = 2; //Update mode
			$check = $this->Testimonials->insertTable($opr, $array, array("tst_id"=>$args));
			if(!$check){
				$this->session->set_flashdata("success", "Testimonial updated successfully");
			}else{
				$this->session->set_flashdata("success", "Testimonial not updated, Please try again");
			}
			redirect(base_url("Testimonial"));
		}
	}

	/*
	* Trash Listing
	*/
	function trashListing(){
		$data = array();
		$con['conditions'] = array("tst_status"=>3);
		$cont['sorting'] = array("tst_date"=>"DESC");
		$data['details'] = $this->Testimonials->getRows($con);
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Content Management');
	    $this->template->load('admin_layout', 'contents' , 'trashList', $data);
	}

	/*
	* Move testimonail trash
	*/
	function trashTestimonial(){
		$id = $this->input->post("id");
		if(!empty($id)){
			$con['conditions'] = array("tst_id"=>$id);
			$check = $this->Testimonials->getRows($con);
			if(count($check) > 0){
				$array = array("tst_status"=>$this->input->post("mode"));
				$opr = 2; //Update mode
				$check = $this->Testimonials->insertTable($opr, $array, array("tst_id"=>$id));
				if($check)
					echo json_encode(array("status"=>200));
			}
			else{
				echo json_encode(array("status"=>1));
			}
		}
		else{
			echo json_encode(array("status"=>1));
		}
	}
}
?>