<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Testimonial</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($details)){
                      foreach($details as $testi){
                        $id = $testi['tst_id'];
                      ?>
                      <form id="addtestimonial" enctype="multipart/form-data" method="post" action='<?php echo base_url("Testimonial/edit/$id"); ?>' data-parsley-validate class="form-horizontal form-label-left">                                            
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Testimonial:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea class="form-control summernote" name="testi" id="testi"><?php echo $testi['tst_content']; ?></textarea>
                            <?php echo form_error('testi'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>By:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="name" class="form-control" placeholder="User Name" value="<?php echo $testi['tst_by']; ?>"/>
                            <?php echo form_error('name'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Designation:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="designation" min="1" value="<?php echo $testi['tst_designation']; ?>" class="form-control" placeholder="Designation"/>
                            <?php echo form_error('designation'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Status:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <select name="status" class="form-control">
                              <option value="">Select Status</option>
                              <option value="1" <?php if($testi['tst_status'] == 1) echo "selected"; ?>>Publish</option>
                              <option value="0" <?php if($testi['tst_status'] == 0) echo "selected"; ?>>Unpublish</option>
                            </select>
                            <?php echo form_error('status'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <button type="submit" class="btn btn-success">Update</button>                          
                          </div>
                        </div>
                      </form>
                      <?php
                      }
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->