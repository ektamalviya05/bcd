<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Testimonial"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Testimonial</p>
                        </div>
                    </a>
                </div>
            </div>                                       
          </div>
      </div>


    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Page List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th align="center">No.</th>
                  <th align="center">By User</th>                                
                  <th align="center">Content</th>                                                  
                  <th align="center">Date Created</th>
                  <th align="center">Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($details)){
                  foreach($details as $testi){
                  ?>
                  <tr>
                    <td align="center"><?php echo $count; ?></td>
                    <td align="center"><?php echo $testi['tst_by'] ?></td>
                    <td align="center"><?php echo $testi['tst_content']; ?></td>                                       
                    <td align="center"><?php echo date("d F, Y H:i", strtotime($testi['tst_date'])); ?></td>
                    <td align="center">
                      <div class="link-del-view">
                          <?php $tst_id = $testi['tst_id']; ?>                                      
                          <div class="tooltip-2"><a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="trashback-testi" modal-aria="<?php echo $tst_id; ?>"><i class="fa fa-reply" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Restore</span>
                          </div>
                          <div class="tooltip-2"><a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="trashback-publishtesti" modal-aria="<?php echo $tst_id; ?>"><i class="fa fa-window-restore" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Restore & Publish</span>
                          </div>                                     
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>