<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Pages extends MX_Controller {

    public function __construct() {
        parent:: __construct();
        $this->load->model("Page");
        $this->load->library("pagination");
        $this->load->library('email');
        $this->load->helper("common_helper");
         $this->load->library('facebook');
        //$data['top_content']        = $this->Page->getTopContent();
    }
    public function index($page = FALSE) {

        header("Access-Control-Allow-Origin: *");
        $data = array();


         $ip       =$_SERVER['REMOTE_ADDR'];
         $geo      = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
         $current_country  = $geo["geoplugin_countryName"];
         $data['authUrl'] =  $this->facebook->login_url();
         //echo $current_country;die;
         $data['current_country'] = $current_country;
         $this->session->set_userdata('current_country', $current_country);

         //echo "My Contry".$current_country;


        $user_id = $this->session->userdata('user_id');
        $this->db->select('userid');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $que2 = $this->db->get();
        $data['current_plan'] = $que2->result();

        $data['title'] = 'Home';
        $lang = $this->session->userdata("lang");
        if (empty($lang))
            $lang = "English";

        $con['conditions'] = array("features_ads" => 1,"status"=>1);
        //$con['start'] = 0;
        //$con['limit'] = 8;
        $con['sorting'] = array("business_id" => "ASC");
        $sql = "SELECT add_img_path FROM otd_advertisement where add_status=1 ORDER BY rand() limit 2";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['advertisements'] = $result;
        $data['top_content']        = $this->Page->getTopContent();
        $data['how_it_works']        = $this->Page->getHowItWork();
        $data['business_categories'] = $this->Page->getBusinessCategories();

        $data['featureCategoryListings'] = $this->Page->getHomeCatgoryListingRows($con);

        //print_r("<pre/>");
        //print_r($data['featureCategoryListings']);
        //die;

         if($this->session->userdata('lang_id')){
              if($this->session->userdata('lang_id')==20){
                  $next = "Next";
                  $prev = "Previous";
                  //$desc = $how_it_work['desc_english'];
              }else{
                    $next = "التالى";
                                $prev = "سابق";
                                //$desc = $how_it_work['desc_arabic'];
                              }
                         } else {
                              $next = "Next";
                              $prev = "Previous";
                              //$desc = $how_it_work['desc_english'];
                         }



         $this->db->select('*');
         $this->db->from( 'bdc_business');
         $this->db->where('features_ads',1);
         $this->db->where('status',1);
         $this->db->order_by('business_id','DESC');

         
         $config = array();
         $config['total_rows']  = count($data['featureCategoryListings']);
         $config['base_url']    = 'Pages/index/';
         $config['uri_segment'] = 3;
         $config['cur_tag_open'] = '<span class="current">';
         $config['cur_tag_close'] = '</span>';
         $config['next_link'] = '<b>'.$next.'</b>';
         $config['prev_link'] = '<b> '.$prev.' </b>';
         $config['per_page']    = 4;
         $config['num_links']   = 10;

            $this->load->library('pagination');
            $this->pagination->initialize($config);
            $this->pager = $this->pagination->create_links();

            $this->db->limit( $config['per_page'], $page );

            $query = $this->db->get();
            $result = $query->result_array();

            $data['featureCategoryListings'] = $result;
            $data['links1'] = $this->pager;

        //$total_rows  = count($data['featureCategoryListings']);
        //echo $total_rows;die;

    
        //echo "<pre>"; print_r($data['business_categories']); die;
        // $con['conditions'] = array("tst_status"=>1);
         $data['testimonials'] = $this->Page->getTestimonials();
         $data['section2'] = $this->Page->getArticles(24);
         $data['section31'] = $this->Page->getArticles(21);
         $data['section32'] = $this->Page->getArticles(22);
         $data['section33'] = $this->Page->getArticles(23);
         $data['section5'] = $this->Page->getArticles(14);
         $data['section61'] = $this->Page->getArticles(15);
         $data['section62'] = $this->Page->getArticles(16);
         $data['section63'] = $this->Page->getArticles(17);
         $data['section64'] = $this->Page->getArticles(18);
         $data['section65'] = $this->Page->getArticles(19);
         $data['section66'] = $this->Page->getArticles(20);
         $data['section7'] = $this->Page->getArticles(12);
         $data['section9'] = $this->Page->getArticles(13);
         $data['section6Bk'] = $this->Page->getArticles(25);
         $data['homecategorytab'] = $this->Page->getHomeCatgoryTabRows();
         $data['headings'] = $this->Page->getTextContent(9);
         $ban['conditions'] = array("bn_status" => 1);
         $ban['sorting'] = array("bn_id" => "RANDOM");
         $ban['limit'] = 3;
         $data['bn_images'] = $this->Page->getBannerImages($ban);
         $data['publishonhome'] = $this->Page->getPublishOnHomeCompanies();
        // echo "<pre>";
        // print_r($data['publishonhome']);
        // exit;
        $data['toprated'] = $this->Page->getTopRatedCompanies();
        $data['featuredCategories'] = $this->Page->getCategoryRows(array("conditions" => array("cat_status" => 1, "cat_homedisplay" => 1)));

        $data['active_tab_home'] = "active";

        $this->template->load('home_layout_listing', 'contents', 'home', $data);
    }
    public function news_subscription() {
        if ($this->input->post()) {
            $news_email = $this->input->post('news_email');
            $responce = $this->Page->checkSubscriptionEmail($news_email);
            if(!empty($responce)){
                echo "<span class='error removess'>Email Already Exist.</span>";
            }else{
                
            $query = array('user_email' => $this->input->post('news_email'));
            $this->db->insert('subscribers', $query);
            echo "<span class='btn-success removess'>Thank you for subscribe to our news";
            }
        }
    }

 public function paymentsuccess() {

         $amt = $_GET['amt'];
         $cc = $_GET['cc'];
         $cm = $_GET['cm'];
         $var = explode("-",$cm);
         $userid =  $var[0];
         $packageid =  $var[1];
         $duration =  $var[2];
         $item_name = $_GET['item_name'];
         $st = $_GET['st'];
         $tx = $_GET['tx'];

         $p = '+ '.$duration.' days';
         $Timestamp = strtotime(date('Y-m-d'));
         $TotalTimeStamp = strtotime($p, $Timestamp);
         $free_end = date('Y-m-d', $TotalTimeStamp);
         if($userid) {
            
            $this->db->select("*");
            $this->db->from("transaction_table");
            $this->db->where('userid',$userid);
            $this->db->where('plan_valid',1);
            $this->db->where('package_type!=',3);
            $this->db->where('package_type!=',4);
            $query = $this->db->get();
            $query->num_rows();
            if($query->num_rows()>0)
            {

                $result = $query->result_array();
                foreach ($result as $row)
                {
                     $pkg_end_date = $row['free_end'];
                     $today_date = date('Y-m-d');
                    $daysLeft = abs(strtotime($today_date) - strtotime($pkg_end_date));
                    $days = $daysLeft/(60 * 60 * 24);
                    $days = $days + $duration;
                    
                    if($days>0)
                    {

                        $adday = '+ '.$days.' days';
                        
                         $Timestamp1 = strtotime($today_date);
                        
                        $free_end_cal = strtotime($adday, $Timestamp1);
                            $free_end = date('Y-m-d', $free_end_cal);
                        
                        
                        
                    }

                    $updatedata = array('plan_valid' => 0);
                    $this->db->where('userid',$userid);
                    $this->db->where('package_type!= ',3);
                    $this->db->where('package_type!= ',4);
                    $isupdate = $this->db->update('transaction_table',$updatedata);
                    
                     if($isupdate)
                    {
                        $data = array(
                             'amt' => $amt,
                             'cc' =>  $cc,
                             'userid' => $userid,
                             'packageid' => $packageid,
                             'item_name' => $item_name,
                             'st' => $st,
                             'transaction_id' => $tx,
                             'free_start' => date('Y-m-d'),
                             'free_end' => $free_end,
                             'plan_valid' => '1',
                             'package_type' => '2',
                     );
                     //print_r($data);
                     $sql = $this->db->insert('transaction_table', $data);

                     //echo $this->db->last_query();
                     //die();
                    }

                }

            }

        }
        else
        {
             $data = array(
                                 'amt' => $amt,
                                 'cc' =>  $cc,
                                 'userid' => $userid,
                                 'packageid' => $packageid,
                                 'item_name' => $item_name,
                                 'st' => $st,
                                 'transaction_id' => $tx,
                                 'free_start' => date('Y-m-d'),
                                 'free_end' => $free_end,
                                 'plan_valid' => '1',
                                 'package_type' => '2',
                         );
                     //print_r($data);
            $sql = $this->db->insert('transaction_table', $data);
        }

         
         if($sql)
         {
            $this->session->set_flashdata('success', 'Your Payment is done');
            redirect(base_url("addlisting"));
         }


    }
     public function paysuccess()
    {
      
        $amt = $_GET['amt'];
        $cc = $_GET['cc'];
        $cm = $_GET['cm'];
        $var = explode("-",$cm);
        $userid =  $var[0];
        $packageid =  $var[1];
        $businessid =  $var[2];
        $duration =  $var[3];
        $item_name = $_GET['item_name'];
        $st = $_GET['st'];
        $tx = $_GET['tx'];

        $p = '+ '.$duration.' days';
        $Timestamp = strtotime(date('Y-m-d'));
        $TotalTimeStamp = strtotime($p, $Timestamp);
        $free_end = date('Y-m-d', $TotalTimeStamp);


        //$userid = 112;
        //echo $userid;die;
        if($userid) {

            $this->db->select("*");
            $this->db->from("transaction_table");
            $this->db->where('userid',$userid);
            $this->db->where('plan_valid',1);
            $this->db->where('package_type!=',1);
            $this->db->where('package_type!=',2);
            $query = $this->db->get();
            $query->num_rows();
            if($query->num_rows()>0)
            {

                $result = $query->result_array();
                  //print_r("<pre/>");
                  //print_r($result);die;

                foreach ($result as $row)
                {
                    $pkg_end_date = $row['free_end'];
                    
                    $today_date = date('Y-m-d');
                    $daysLeft = abs(strtotime($today_date) - strtotime($pkg_end_date));
                    //echo $daysLeft;die;
                    $days = $daysLeft/(60 * 60 * 24);
                    //echo $days;die;
                    $days = $days + $duration;

                    //echo $days;die;


                    if($days>0){

                        $adday = '+ '.$days.' days';
                        $Timestamp1 = strtotime($today_date);
                        $free_end_cal = strtotime($adday, $Timestamp1);
                        //echo $free_end_cal;die;
                         $free_end = date('Y-m-d', $free_end_cal);

                         //echo $free_end;die;
                    }
                    $updatedata = array('plan_valid' => 0);
                    $this->db->where('userid',$userid);
                    $this->db->where('package_type!= ',1);
                    $this->db->where('package_type!= ',2);
                    $isupdate = $this->db->update('transaction_table',$updatedata);
                    if($isupdate)
                    {
                            $data = array(
                                     'amt' => $amt,
                                     'cc' =>  $cc,
                                     'userid' => $userid,
                                     'packageid' => $packageid,
                                     'item_name' => $item_name,
                                     'st' => $st,
                                     'transaction_id' => $tx,
                                     'business_id' => $businessid,
                                     'free_start' => date('Y-m-d'),
                                     'free_end' => $free_end,
                                     'package_type' => '3',
                                     'plan_valid' => '1',
                             );
                            $fdata['features_ads'] = 1;
                            $this->db->update('bdc_business', $fdata, array("business_id"=>$businessid));
                            $sql = $this->db->insert('transaction_table', $data);


                    }
                }
            }else{

                 $data = array(
                                     'amt' => $amt,
                                     'cc' =>  $cc,
                                     'userid' => $userid,
                                     'packageid' => $packageid,
                                     'item_name' => $item_name,
                                     'st' => $st,
                                     'transaction_id' => $tx,
                                     'business_id' => $businessid,
                                     'free_start' => date('Y-m-d'),
                                     'free_end' => $free_end,
                                     'package_type' => '3',
                                     'plan_valid' => '1',
                             );

                 $fdata['features_ads'] = 1;
                 $this->db->update('bdc_business', $fdata, array("business_id"=>$businessid));
                $sql = $this->db->insert('transaction_table', $data);

            }
        }

        /*$data = array(
                 'amt' => $amt,
                 'cc' =>  $cc,
                 'userid' => $userid,
                 'packageid' => $packageid,
                 'item_name' => $item_name,
                 'st' => $st,
                 'transaction_id' => $tx,
                 'business_id' => $businessid,
                 'free_start' => date('Y-m-d'),
                 'free_end' => $free_end,
                 'package_type' => '3',
         );*/

         //$fdata['features_ads'] = 1;
         //$this->db->update('bdc_business', $fdata, array("business_id"=>$businessid));

         //$sql = $this->db->insert('transaction_table', $data);
         if($sql)
         {
            $this->session->set_flashdata('success', 'Your Payment is done');
            redirect(base_url("dashboard"));
         }

    }

    public function success()
    {


        $data['top_content']        = $this->Page->getTopContent();
        $this->template->load('home_layout', 'contents', 'paymentsuccess', $data);
    }

    public function cancel()
    {
        $data['top_content']        = $this->Page->getTopContent();
        $this->template->load('home_layout', 'contents', 'paymentcancel', $data);
    }

    public function paymentcancel()
    {
       
      redirect(base_url("cancel"));

    }

    public function submit_review() {
        if ($this->input->post()) {
            $rating = $this->input->post('rating');
            $visitor_name = $this->input->post('visitor_name');
            $email = $this->input->post('email');
            $description = $this->input->post('description');
            $business_id = $this->input->post('business_id');
            $user_id = $this->session->userdata('user_id');
            /*$responce = $this->Page->checkSubscriptionEmail($news_email);
            if(!empty($responce)){
                echo "<span class='error removess'>Email Already Exist.</span>";
            }else{
            } 
            */
            $checkrating =  $this->Page->checkrating($user_id,$business_id);
            if($checkrating)
            {
                $query = array('business_id' => $business_id,'user_id' => $user_id,'rating' => 0,'name' => $visitor_name,'email' => $email,'description' => $description,'status' => 0,'create_date' => date('Y-m-d H:i:s'),'update_date' => date('Y-m-d H:i:s'));
                    $res = $this->db->insert('business_review', $query);
                    if($res)
                    {
                        echo "<span class='btn-success removess' style='padding: 5px;'>Your Review has been submitted successfull!";    
                    }
                    else
                    {
                        echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";    
                    }

            }
            else{

                $query = array('business_id' => $business_id,'user_id' => $user_id,'rating' => $rating,'name' => $visitor_name,'email' => $email,'description' => $description,'status' => 0,'create_date' => date('Y-m-d H:i:s'),'update_date' => date('Y-m-d H:i:s'));
                    $res = $this->db->insert('business_review', $query);
                    if($res)
                    {
                        echo "<span class='btn-success removess' style='padding: 5px;'>Your Review has been submitted successfull!";    
                    }
                    else
                    {
                        echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";    
                    }

            }

           
        } 
        else {
        echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";
        }
            
    }

    public function send_contactInfo() {
        if ($this->input->post()) {
            $email = $this->input->post('email');
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $comment = $this->input->post('comment');
            $user_id = $this->session->userdata('user_id');
            
            $query = array('business_id' => $business_id,'user_id' => $user_id,'rating' => $rating,'name' => $visitor_name,'email' => $email,'description' => $description,'status' => 0,'create_date' => date('Y-m-d H:i:s'),'update_date' => date('Y-m-d H:i:s'));
            $res = $this->db->insert('business_review', $query);
            if($res)
            {
                echo "<span class='btn-success removess' style='padding: 5px;'>Your Review has been submitted successfull!";    
            }
            else
            {
                echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";    
            }
        } else {
            echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";
        }
    }

    public function contactformsubmitbyus()
    {
      $to      = 'votivephp.ekta@gmail.com'; 
      $subject = 'the subject'; 
      $message = 'hello'; 
      $headers = 'From: webmaster@votivephp.in' . "\r\n" . 
      'Reply-To: webmaster@example.com' . "\r\n" . 
      'X-Mailer: PHP/' . phpversion(); 

      $send = mail($to, $subject, $message, $headers); 
       print_r($send);
       
        // Sending email
        if($send){
            echo 'Your mail has been sent successfully.';
        } else{
            print_r(error_get_last());
            echo 'Unable to send email. Please try again.';
        }
    }

    public function contactformsubmitbyus_backup()
    {
        if ($this->input->post()) 
        {
            $email = $this->input->post('email');
            $name = $this->input->post('name');
            $phone = $this->input->post('phone');
            $comment = $this->input->post('comment');

            $sql = array(
                    'email' => $email,
                    'name'  => $name,
                    'mobilenumber' => $phone,
                    'message' => $comment,
                );
            $result = $this->db->insert('contactus', $sql);
            echo $result;
            if($result)
            {
                    // $from    = 'info@ilbait.com';
                    // $subject = "Contact Us Request";
                    // $email   =   'votivephp.ekta@gmail.com';
                    // $message = "<p>Dear System Admin ,</p>";
                    // $message .= "<p>User Name    : <b>".$name."</b> </p>";
                    // $message .= "<p>User Email   : <b>".$email."</b> </p>";
                    // $message .= "<p>User Phone   : <b>".$phone."</b> </p>";
                    // $message .= "<p>User Message : <b>".$comment."</b> </p>";
                    // $message .= "<p>Above information Contact Request by ".$name.".</p>";
                    // $message .= "<p><br></p>";
                    // $message .= "<p>Thanks,</p>";
                    // $message .= "<p>Ilbait Admin</p>";
                    // $headers .= "From: ilbait < ilbaitinfo@gmail.com >\n";
                    // $headers .= 'X-Mailer: PHP/' . phpversion();
                    // $headers .= "X-Priority: 1\n"; // Urgent message!
                    // $headers .= "Return-Path: mail@domain.com\n"; // Return path for errors
                    // $headers .= "MIME-Version: 1.0\r\n";
                    // $headers .= "Content-Type: text/html; charset=iso-8859-1\n";
                    // if (mail($email, $subject, $message, $headers)) 
                    // {
                    //   echo "Message accepted";
                    //   die();
                    // } 
                    // else 
                    // {
                    //     print_r(error_get_last());
                    //     die();
                    // }

                    $subject = "Test mail";
                    $to_email = "votivephp.akash@gmail.com";
                    $to_fullname = "John Doe";
                    $from_email = "votivephp.ekta@gmail.com";
                    $from_fullname = "Jane Doe";
                    $headers  = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html; charset=utf-8\r\n";
                    // Additional headers
                    // This might look redundant but some services REALLY favor it being there.
                    $headers .= "To: $to_fullname <$to_email>\r\n";
                    $headers .= "From: $from_fullname <$from_email>\r\n";
                    /*$message = "<html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"en\" xml:lang=\"en\">\r\n
                      <head>\r\n
                        <title>Hello Test</title>\r\n
                      </head>\r\n
                      <body>\r\n
                        <p></p>\r\n
                        <p style=\"color: #00CC66; font-weight:600; font-style: italic; font-size:14px; float:left; margin-left:7px;\">You have received an inquiry from your website.  Please review the contact information below.</p>\r\n
                      </body>\r\n
                      </html>";*/
                      $message = "Welcome";
                      if (!mail($to_email, $subject, $message, $headers)) { 
                        print_r(error_get_last());
                      }
                      else
                      {
                        echo "sent mail";
                      }
                    echo "<span class='btn-success removess' style='padding: 5px;'>We Will Contact Soon !! ";
            }
            else
            {
                echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";    
            }
        }
        else
        {
            echo "<span class='error removess' style='padding: 5px;'>Some internal issue occured. Please try again.</span>";

        }

    }

    function euaformsubmitbyus()
    {
        if ($this->input->post()) 
        {
             $name   = $_POST['inputName'];
             $email  = $_POST['inputEmail'];
             $message= $_POST['inputMessage'];
             $bid = $_POST['bid'];
             $email_from= $_POST['email_from'];
            $sql = array(
                    'email' => $email,
                    'name'  => $name,
                    'message' => $message,
                    'business_id' => $bid,
                    'email_from' => $email_from,
                );
            $result = $this->db->insert('bdc_enquiry', $sql);
            if($result){
                $status = 'ok';
                echo $status;
            }
            else{
                $status = 'err';
                echo $status;
            }
            

        }

    }

    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public function businessListing($args = NULL) {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '2048M');
        $data = array();
        $dist = $this->Page->getDistanceLimit();
        $dis_range = $dist['option_value'] > 0 ? $dist['option_value'] : 5;
        //My Code
        if (!empty($this->session->userdata("search_location"))) {
            $location = $this->session->userdata("search_location");
            $searchlatlong = $this->session->userdata("search_latlong");
            if (!empty($searchlatlong)) {
                $con['cur_latlng'] = explode(",", $searchlatlong);
            } else {
                $con['cur_latlng'] = explode(",", $this->get_lat_long($location));
            }
        } else {
            $location = "France";
            $con['cur_latlng'] = explode(",", $this->get_lat_long($location));
        }
        if (!empty($this->session->userdata("search_category"))) {
            $category = $this->session->userdata("search_category");
            $con['search_category'] = $category;
        }
        if (!empty($this->session->userdata("search_keyword"))) {
            $keyword = $this->session->userdata("search_keyword");
            $con['search_keyword'] = $keyword;
        }

        if (!empty($this->session->userdata("search_extrafilter"))) {
            $extrafilter = $this->session->userdata("search_extrafilter");
            $con['search_extrafilter'] = $extrafilter;
        }
        //My Code End

        $data["searchedcity"] = "";
        $data['keyword'] = "";
        $data['extraFilter'] = $this->session->userdata("search_extrafilter");

        $con['keyword'] = "";

        if (isset($_GET['category'])) {
            $cat = urldecode($_GET['category']);
            $con['conditions']['categories'] = $cat;
            $data['category'] = $cat;
        }
        if ($location != "") {
            $con['user_company_address'] = $location;
        } else {
            $con['user_company_address'] = "France";
        }

        $con['returnType'] = "";
        $con['distance'] = $dis_range;
        $data['ALaUNE'] = $this->Page->getALaUneProfiles1($con);
        $data['headprofile'] = "";

        $con['limit'] = 50;
        if (!empty($args)) {
            $con['start'] = $args;
        } else {
            $con['start'] = 0;
        }
        $result = $this->Page->getSearchRows1($con);
        $data['search_results'] = $result['result'];
        $data['businessMarker'] = $result['result'];
        $data['totalrecord'] = $result['count'];

        // Pagination Code Starts
        $config['base_url'] = base_url() . "Pages/businessListing/";
        $config['total_rows'] = $result['count'];
        $config['per_page'] = 50;
        $config['uri_segment'] = 3;
        $config['num_links'] = 2;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '<b> Next </b>';
        $config['prev_link'] = '<b> Previous </b>';
        $this->pagination->initialize($config);
        $data['links'] = $this->pagination->create_links();
        // Pagination Code End

        $this->load->helper("cookie");
        $data['business_categories'] = $this->Page->getBusinessCategories();
        $data['busienss_filters'] = $this->Page->getExtraFilters();
        $data['service_filter'] = $this->Page->getExtraFiltersByType(2);
        $data['product_filter'] = $this->Page->getExtraFiltersByType(3);
        $data['other_filter'] = $this->Page->getExtraFiltersByType(4);
        $data['googleadd'] = $this->Page->getGoogleAdd();
        $data['manualadd'] = $this->Page->getManualAddRows(array("sorting" => array("add_display" => "ASC"), "conditions" => array("add_status" => 1)));
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Business Listing');
        $this->template->load('home_layout', 'contents', 'searchResult', $data);
    }

    public function setSearchDelimiters() {
        $category = $this->input->get_post("category");
        $address = $this->input->get_post("address");
        if (!empty($category)) {
            $this->session->set_userdata("search_category", $category);
        }
        if (!empty($address)) {
            $this->session->set_userdata("search_location", $address);
            $searchlatlong = $this->get_lat_long($address);
            $this->session->set_userdata("search_latlong", $searchlatlong);
        }
        if (!empty($this->input->post())) {
            $location = $this->input->post("user_address");
            $category = $this->input->post("business_category1");
            $keyword = $this->input->post("keyword");
            $extfilt = $this->input->post("extra_fltr");
            $this->session->sess_expiration = '14400'; //Expres in 4 hours
            $this->session->set_userdata("search_location", $location);
            if (!empty($location)) {
                $searchlatlong = $this->get_lat_long($location);
                $this->session->set_userdata("search_latlong", $searchlatlong);
            }
            $this->session->set_userdata("search_category", $category);
            $this->session->set_userdata("search_keyword", $keyword);
            if (!empty($extfilt)) {
                $arr = array();
                foreach ($extfilt as $filt) {
                    $arr[] = $filt;
                }
                $this->session->set_userdata("search_extrafilter", $arr);
            } else {
                $this->session->unset_userdata("search_extrafilter");
            }
        }
        redirect(base_url("Pages/businessListing"));
    }

    // public function businessListing()
    // {
    //     $data = array();
    //     $dist = $this->Page->getDistanceLimit();
    //     $dis_range = $dist['option_value'] > 0 ?$dist['option_value']:5;
    //     if($this->input->post("searchSubmit"))
    //     {
    //         $location = $this->input->post("user_address");
    //         $business = $this->input->post("business_category1");
    //         $keyword = $this->input->post("keyword");
    //         $extra_fltr = $this->input->post("extra_fltr");
    //         $searchedcity = $this->input->post("searchedcity");
    //         $cur_latlng = explode(",", $this->get_lat_long($location));
    //         if(sizeof($cur_latlng) > 0 and $cur_latlng[0] !='' and $cur_latlng[1] !='')
    //             {
    //                 $con['cur_latlng'] = $cur_latlng;
    //             }
    //         $data['location'] = $location;
    //         $data['category'] = $business;
    //         $data['keyword'] = $keyword;
    //         $data['extraFilter'] = $extra_fltr;
    //         $data["searchedcity"] = $searchedcity;
    //         if($searchedcity != "")
    //         {
    //             $con['searchedcity'] = $searchedcity;
    //         }
    //         if($location != "")
    //         {
    //             $con['user_company_address'] = $location;
    //         }
    //         if(sizeof($extra_fltr) > 0)
    //         {
    //             $con['extraFilter'] = $extra_fltr;
    //         }
    //         if($business != "")
    //         {
    //             $con['conditions']['categories'] = $business;
    //         }
    //         $con['distance'] = $dis_range;
    //         $con['keyword'] = $keyword;
    //     }
    //     else
    //     {
    //         $locationlatlng = $this->session->userdata("currentlatlng");
    //         $cur_latlng = $locationlatlng;
    //         if($locationlatlng == "")
    //         {
    //             $location = "France";
    //             $cur_latlng = explode(",", $this->get_lat_long($location));
    //         }
    //         if(sizeof($cur_latlng) > 0 and $cur_latlng[0] !='' and $cur_latlng[1] !='')
    //         {
    //             $con['cur_latlng'] = $cur_latlng;
    //         }
    //         $data["searchedcity"] = "";
    //         $data['location'] = "";
    //         $data['category'] = "";
    //         $data['keyword'] = "";
    //         $data['extraFilter'] = "";
    //         $con['keyword'] = "";
    //         if(isset($_GET['category']))
    //         {
    //             $cat = urldecode($_GET['category']);   
    //             $con['conditions']['categories'] = $cat;
    //             $data['category'] = $cat;
    //         }
    //         if($location != "")
    //         {
    //             $con['user_company_address'] = $location;
    //         }
    //         else {
    //             $con['user_company_address'] = "France";
    //         }
    //         }
    //       /*Pagination code*/
    //         $con['returnType']="count"; 
    //         $con['distance'] = $dis_range;       
    //         $totalrecord = $result = $this->Page->getSearchRows1($con);
    //          //echo $this->db->last_query();die;		
    //         $data['record_found'] = $totalrecord;
    //         $totalrows = $totalrecord;
    //         $config['base_url'] = base_url()."Pages/businessListing/";
    //         $config['total_rows'] = $totalrows;
    //         $config['per_page'] = 20;
    //         $config['uri_segment'] = 3;
    //         $config['num_links'] = $totalrows / 1;
    //         $config['cur_tag_open'] = '&nbsp;<a class="current">';
    //         $config['cur_tag_close'] = '</a>';
    //         $config['next_link'] = '<b> Next </b>';
    //         $config['prev_link'] = '<b> Previous </b>';
    //         $choice = $config["total_rows"] / $config["per_page"];
    //         $config["num_links"] = round($choice);
    //         $this->pagination->initialize($config);
    //         $page = ($this->uri->segment(3));
    //         $con['limit'] = $config['per_page'];
    //         $start =($page-1) * $config['per_page'];
    //         if($start == 0)
    //         {
    //           $start = 0;
    //         }
    //         else
    //         {
    //           $start = ($start/$config['per_page'] )+1;
    //         }
    //         $con['start'] = $start;
    //         $data['userlist'] = array();
    //     if($totalrows > 0){
    //         $con['returnType']="";
    //        $data['ALaUNE']  = $this->Page->getALaUNEProfiles($con);
    //         $data['headprofile']  = "";
    //         $con['distance'] = $dis_range;
    //         $result = $this->Page->getSearchRows1($con);
    //         $data['search_results'] = $result;
    //         $data['businessMarker'] =  $result;
    //         $data["record_found"] = $totalrows; 
    //     }
    //     else
    //     {
    //       $data["record_found"] = 0; 
    //       $data['search_results'] = null;
    //       $data['businessMarker'] =  null;
    //     }
    //     $data["links"] = $this->pagination->create_links();
    //     $data['page'] = $page;
    //         /*End Pagination Code*/
    //         $this->load->helper("cookie");
    //         $data['business_categories'] = $this->Page->getBusinessCategories();
    //         $data['busienss_filters'] = $this->Page->getExtraFilters();
    //         $data['googleadd'] = $this->Page->getGoogleAdd();
    //         $data['manualadd'] = $this->Page->getManualAddRows(array("sorting"=>array("add_display"=>"ASC"), "conditions"=>array("add_status"=>1)));
    //         header("Access-Control-Allow-Origin: *");
    //         $this->template->set('title', 'Find NearBy Attraction');
    //         $this->template->load('home_layout', 'contents' , 'searchResult', $data);
    // }
    // function to get  the address
    function get_lat_long1($address) {
        $region = "fr";
        $latlong = "";
        $address = str_replace(" ", "+", $address);
        if ($address == "") {
            $address = "France";
        }
        if ($address != "") {
            $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=AIzaSyCFV0MgMCdDw4wuDxgS7_ymejhyA7d2h7A&address=$address&sensor=false&region=$region");
            $json = json_decode($json);
            if (!empty($json->results)) {
                $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
                $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
                $latlong = $lat . ',' . $long;
                $this->session->set_userdata("emptyaddress", 0);
            } else {
                $emptyaddress = $this->session->userdata("emptyaddress");
                $emptyaddress++;
                if ($emptyaddress < 3) {
                    $this->session->set_userdata("emptyaddress", $emptyaddress);
                    $this->get_lat_long($address);
                } else {
                    $lat = "48.8566140";
                    $long = "2.3522220";
                    $latlong = $lat . ',' . $long;
                }
            }
            //echo $address."<br><br>".$latlong;
            //die();
        } else {
            $lat = "48.8566140";
            $long = "2.3522220";
            $latlong = $lat . ',' . $long;
        }
        return $latlong;
    }

    public function searchResult() {
        $location = $this->input->post("user_address");
        $business = $this->input->post("user_categories");
        $keyword = $this->input->post("keyword");
        $this->load->helper("cookie");
        if (!empty($location)) {
            set_cookie(array('name' => 'location',
                'value' => $location,
                'expire' => 3600
            ));
        }
        if (!empty($business)) {
            set_cookie(array('name' => 'business',
                'value' => $business,
                'expire' => 3600
            ));
        }
        if (!empty($keyword)) {
            set_cookie(array('name' => 'kayword',
                'value' => $keyword,
                'expire' => 3600
            ));
        }
        redirect(base_url("Pages/businessListing"));
    }

    public function setMapSize() {
        $this->load->helper("cookie");
        $mapsize = get_cookie("mapsize");
        if (empty($mapsize) && $mapsize == 0) {
            set_cookie(array('name' => 'mapsize',
                'value' => 1,
                'expire' => 14400
            ));
        } else {
            set_cookie(array('name' => 'mapsize',
                'value' => 0,
                'expire' => 14400
            ));
        }
    }

    public function content($args = NULL) {
        header("Access-Control-Allow-Origin: *");
        $data = array();
        $con['conditions'] = array("page_url" => $args);
        $data['details'] = $this->Page->getPageRows($con);
        $con['conditions'] = array("pg_cat" => $data['details'][0]['pg_cat'], "pg_status" => 1);
        $con["sorting"] = array("pg_display_number" => "ASC");
        $data['similar_pages'] = $this->Page->getPageRows($con);
        $this->template->set('title', $data['details'][0]['pg_title']);
        $this->template->load('home_layout', 'contents', 'pages', $data);
    }

    function readMore($args = NULL) {
        $data = array();
        if ($args == "personal") {
            $query = $this->db->get_where("site_contents", array("content_id" => 4));
            foreach ($query->result() as $cnt) {
                
            }
            $data['details'] = $cnt->content;
            $this->template->set('title', 'Personal Account');
            $data['title'] = "Personal Account";
        } else if ($args == "professional") {
            $query = $this->db->get_where("site_contents", array("content_id" => 1));
            foreach ($query->result() as $cnt) {
                
            }
            $data['details'] = $cnt->content;
            $this->template->set('title', 'Professional Account');
            $data['title'] = "Professional Account";
        }
        header("Access-Control-Allow-Origin: *");
        $this->template->load('home_layout', 'contents', 'articals', $data);
    }

    public function getCategories($catType) {
        
    }

    function getAddress($latitude, $longitude) {
        if (!empty($latitude) && !empty($longitude)) {
            //Send request and receive json data by address
            $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng=' . trim($latitude) . ',' . trim($longitude) . '&sensor=false');
            $output = json_decode($geocodeFromLatLong);
            $status = $output->status;
            //Get address from json data
            $address = ($status == "OK") ? $output->results[1]->formatted_address : '';
            //Return address of the given latitude and longitude
            if (!empty($address)) {
                return $address;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function goodPlace() {
        if (!empty($this->input->post())) {
            $locat = $this->input->post("location");
            $keyword = $this->input->post("keyword");
            $eventtype = $this->input->post("brand_name");
            $this->session->set_userdata("eventtype", $eventtype);
            $this->session->set_userdata("locat", $locat);
            $this->session->set_userdata("keyword", $keyword);
            redirect(base_url("Pages/goodPlace"));
        }
        $data = array();
        $data['listing'] = $this->Page->getEventsRows(array("conditions" => array("event_status" => 1, "event_goodplace" => 1, "event_publish_start <=" => date("Y-m-d"), "event_publish_end >=" => date("Y-m-d"), "opt_option_status" => 1), "sorting" => array("event_publish_start" => "DESC")));
        $data['evttype'] = $this->Page->getEventTypeList();
        // $data['eventmarkers'] = $data['evttype'];
        // echo "<pre>";
        // print_r($data['evttype']);
        // exit;
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Find NearBy Attraction');
        $this->template->load('home_layout', 'contents', 'goodPlace', $data);
    }

    function clearGoodPlaceFilter() {
        $this->session->unset_userdata("eventtype");
        $this->session->unset_userdata("locat");
        $this->session->unset_userdata("keyword");
        redirect(base_url("Pages/goodPlace"));
    }

    /*
     * external function for user module
     */

    public function option_package() {
        $data = array();
        $this->template->load('home_layout', 'contents', 'option_package', $data);
    }

    function paymentIpn() {
        $this->load->library("email");
        $paypalInfo = $this->input->post();
        //insert the transaction data into the database

        $request_id = $paypalInfo['custom'];
        $arr = array("pyt_txn_id" => $paypalInfo['txn_id'],
            "pyt_amount" => $paypalInfo['mc_gross'],
            "pyt_fee" => $paypalInfo['mc_fee'],
            "pyt_email" => $paypalInfo['payer_email'],
            "pyt_date" => date("Y-m-d H:i:s", strtotime($paypalInfo['payment_date'])),
            "pyt_txn_status" => $paypalInfo['payment_status']
        );

        // $request_id = 1;
        // $arr = array("pyt_txn_id" => "fsadfsdfsadfsadfsfadf",
        //              "pyt_amount" => 456465,
        //              "pyt_fee" => 2.03,
        //              "pyt_email" =>"tesst@test.omc",
        //              "pyt_date" =>"2017-10-23 10:24",
        //              "pyt_txn_status" =>"Completed"
        //             );
        $check = $this->Page->updateTransaction($arr, array("pyt_id" => $request_id));
        $userdetails = $this->Page->getUserRowsFromOption($request_id);
        $email = $userdetails['user_email'];
        $optiondetails = $this->Page->getOptionDetails($request_id);
        if (!empty($optiondetails)) {
            $purchasedoptions = "";
            foreach ($optiondetails as $txnopt) {
                $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                $opt_option_price = $txnopt['opt_option_price'];
                $opt_option_id = $txnopt['opt_option_id'];
                $checkduration = $txnopt['otp_option_duration'];
                $opt_type = $txnopt['opt_option_type'];
                $mstopt = $this->Page->getMasterOption($txnopt['opt_option_id']);
                if ($opt_type == 4) {
                    $purchasedoptions .= "<p><b>" . $mstopt['opt_name'] . ":</b> " . $txnopt['otp_option_duration'] . "</p>";
                } else {
                    if ($txnopt['otp_option_duration'] == "1 Day") {
                        if ($opt_option_id != 7 && $opt_option_id != 8) {
                            $this->Page->updateUserOption(array("opt_option_status" => 1), array("opt_user_option_id" => $txnopt['opt_user_option_id']));
                        }

                        $date1 = date_create($txnopt['opt_option_active_date']);
                        $date2 = date_create($txnopt['opt_option_end_date']);
                        $difference = date_diff($date1, $date2);
                        $diff = $difference->format('%a');
                        if ($diff > 0)
                            $purchasedoptions .= "<p<b>" . $mstopt['opt_name'] . ": </b> " . $diff . " Days (" . $txnopt['opt_option_active_date'] . " to " . $txnopt['opt_option_end_date'] . ")</p>";
                        else
                            $purchasedoptions .= "<p<b>" . $mstopt['opt_name'] . ": </b> 1 Day (" . $txnopt['opt_option_active_date'] . ")";
                    }
                    if ($txnopt['opt_option_id'] == 6) {
                        $purchasedoptions .= "<p<b>" . $mstopt['opt_name'] . ": </b>" . $txnopt['otp_option_qnty'] . " Pictures For " . $txnopt['otp_option_duration'];
                    } else {
                        $purchasedoptions .= "<p><b>" . $mstopt['opt_name'] . ":</b> " . $txnopt['otp_option_duration'] . "</p>";
                    }
                }
            }
            //Fetch Email Templates
            $templateheader = $this->Page->getEmailTemplateRows(array("tmplate_id" => 26)); //Email Header
            $templatecenter = $this->Page->getEmailTemplateRows(array("tmplate_id" => 19));
            $templatefooter = $this->Page->getEmailTemplateRows(array("tmplate_id" => 25)); // Email Footer

            $subject = $templatecenter['tmplate_subject'];
            $dummy = array("%%Firstname%%",
                "%%LastName%%",
                "%%Email%%",
                "%%BusinessName%%",
                "%%CompanyNumber%%",
                "%%Address%%",
                "%%Phone%%",
                "%%Gender%%",
                "%%PurchaseId%%",
                "%%PurchaseDate%%",
                "%%PurchaseDetails%%");
            $real = array($userdetails['user_firstname'],
                $userdetails['user_lastname'],
                $userdetails["user_email"],
                $userdetails["user_company_name"],
                $userdetails['user_company_number'],
                $userdetails["user_company_address"],
                $userdetails['user_phone'],
                $userdetails['user_gender'],
                $request_id,
                $userdetails['opt_option_purchase_date'],
                $purchasedoptions
            );
            $msg = str_replace($dummy, $real, $templatecenter['tmplate_content']);
            $msg = $templateheader['tmplate_content'] . " " . $msg . " " . $templatefooter['tmplate_content'];
            $x = $this->send_mail($userdetails["user_email"], $subject, $msg);
            $y = $this->send_mail(ADMIN_EMAIL, $subject, $msg);
        }
    }

    public function sendMailFormat($subject, $msginner, $template = 21) {
        $templateheader = $this->Page->getEmailTemplateRows(array("tmplate_id" => 26)); //Email Header
        $templatefooter = $this->Page->getEmailTemplateRows(array("tmplate_id" => 25)); // Email Footer
        $templatedata = $this->Page->getEmailTemplateRows(array("tmplate_id" => $template));

        $format = '<html><head><meta charset="utf-8"><title>' . $subject . '</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"></head>
                    <body style="font-family: open Sans;font-size: 13px; line-height:20px;">
                    <div style="padding: 0 10px;">
                        <div style="max-width:550px;width:85%;padding:30px;margin:30px auto;border:1px solid #e2e2e2; overflow:hidden; background-position:center;background-size:cover;text-align: center;color: #fff;">
                            <div style="padding:30px;border:2px solid #fff;background:rgba(0, 0, 0, 0.63);">
                    ';

        $format .= $templateheader['tmplate_content'] . '
                    <div style="border-top: 1px dotted #fff; display: block; margin-top:5px; margin-bottom:5px;"></div>
                        <div class="mailbody" style="min-height: 250px;">
                            <div>' . $templatedata['tmplate_content'] . ' <br>' . $msginner . '</div>
                        </div>';

        $format .= $templatefooter['tmplate_content'] . '
                </div>
            </div>
        </div>
        </body></html>';

        return $format;
    }

    function send_mail($email, $subject = "Test Mail", $msg = "Testing mail") {
        $from = SENDER_EMAIL;
        $this->load->library('email');
        $config['protocol'] = 'ssl';
        $config['smtp_host'] = 'ssl://mail.gandi.net';
        $config['smtp_port'] = 587;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = $from;
        $config['smtp_pass'] = 'puR1-7,XifsArlyV!5';
        $config['charset'] = 'utf-8';
        $config['newline'] = "\r\n";
        $config['mailtype'] = 'html';
        $config['validation'] = TRUE;
        $this->email->initialize($config);
        $fromname = SENDER_NAME;
        $fromemail = SENDER_EMAIL;
        $to = $email;
        $subject = $subject;
        $message = $msg;
        $cc = false;
        $this->email->from($fromemail, $fromname);
        $this->email->to($to);
        $this->email->subject($subject);
        $this->email->message($msg);
        if ($cc) {
            $this->email->cc($cc);
        }

        if (!$this->email->send()) {
            return $this->email->print_debugger();
        } else {
            return true;
        }
    }

    public function sendcontactmail() {
        extract($_POST);
        $subject = "Contact";
        $msginner = "<table><tr><th>Name</th><td>'" . $name . "'</td></tr><tr><th>Email</th><td>'" . $email . "'</td></tr><tr><th>Email</th><td>'" . $contact . "'</td></tr><tr><th>Email</th><td>'" . $message . "'</td></tr></table>";
        $msg = $this->sendMailFormat($subject, $msginner, 13);

        $fromemail = SENDER_EMAIL;
        $fromname = SENDER_NAME;
        $x = $this->send_mail($fromemail, $subject, $msg);
        //  $x = $this->send_mail($email, $subject, $msg);
        $data['success_msg'] = 'Votre message à bien été envoyé.';
        $data['status'] = 200;
        echo json_encode($data);
    }

    /*
     * external function for user modules end
     */
    /*
     * User logout
     */

    public function logout() {
        $uid = $this->session->userdata('user_id');
        $ci_session['status'] = 1;
        $this->db->where("user_id", $uid);
        $this->db->update("ci_sessions", $ci_session);
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('social_login');
        //$this->session->sess_destroy();
        redirect(base_url('home'));
    }

public function Listings($page_number=false) 
{
   
                //Pradeep Verma
                //$near_by=0;
                $user_id = $this->session->userdata('user_id');
        
                $this->db->select('userid');
                $this->db->from('transaction_table');
                $this->db->where('userid',$user_id);
                $que2 = $this->db->get();
                $data['current_plan'] = $que2->result();
                $data['top_content'] = $this->Page->getTopContent();
                // echo $this->input->post('address');
          
                if(isset($_POST["fiterdata"]))
                {
                    if($this->input->post('near_by'))
                    {
                        $near_by = $this->input->post('near_by');
                        echo "In Near By: ". $near_by;
                        if($near_by == 0)
                        {
                            if($this->session->userdata("near_by"))
                            {
                                $near_by = $this->session->userdata("near_by");
                        //        echo "Near By: ". $near_by;
                            }
                    
                        }
                        $this->session->set_userdata("near_by", $near_by);
                    }
                    else
                    {
                        $near_by = 0;
                    }
                    $data['near_by'] =$near_by;
                }
                else if(isset($_POST["btnListingData"]))
                {
                    $this->session->set_userdata("near_by", 0);
                    $near_by = 0;
                    $data['near_by'] =$near_by;
                }
                else{
                    $near_by = 0;
                    if($this->session->userdata("near_by"))
                    {
                        $near_by = $this->session->userdata("near_by");
                    }
                    $data['near_by'] =$near_by;
                }

                    
                    

                 
                    if($this->input->post('address') || $this->session->userdata("search_location") )
                    {
                    
                        $address = $this->input->post('address');
                    

                        if($address == "")
                        {
                            $address = $this->session->userdata("search_location");
                        }
                        $this->session->set_userdata("search_location", $address);
                        $autolocation = explode(', ', $address);
                        $count =count($autolocation);
                       // echo "count".$count;
                        $len =$count-3;
                        $cityId = $autolocation[$len];

                        $country = end($autolocation);
                        
                        $latlong    =   getLocationLatLng($address);
                        $lat   =  $latlong['lat'];
                        $long  =  $latlong['lng'];
                    }
                    else
                    {
                        $ip =$_SERVER['REMOTE_ADDR'];
                        $geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
                        $country  = $geo["geoplugin_countryName"];
                        $cityId     = $geo["geoplugin_city"];
                        $lat      = $geo["geoplugin_latitude"];
                        $long     = $geo["geoplugin_longitude"];
                        $address =  $cityId. ", ".$country;
                    }
            
                    // echo $country. $cityId. $address;


                    $this->session->set_userdata("search_lat", $lat );
                    $this->session->set_userdata("search_long", $long );
                    

                    $name  = $this->input->post('business_name');
                    $cat_id = $this->input->post('category_id');
                    $con['conditions'] = array("features_ads"=>0,'status'=>1);
                    $fcon['conditions'] = array("features_ads"=>1,'status'=>1);
                   
                    $cat_count = $this->Page->listingCount1($country,$cityId,$address,$cat_id,$name, $lat, $long, $near_by);

                    $config['base_url'] = base_url()."listings";
                    $config['total_rows'] = count($cat_count);;
                    $config['per_page'] = 10;
                    $config['uri_segment'] = 2;
                    $config['num_links'] = 3;
                    $config['cur_tag_open'] = '<a class="current">';
                    $config['cur_tag_close'] = '</a>';
                    $config['next_link'] = '<b>Next</b>';
                    $config['prev_link'] = '<b>Previous</b>';
                    $this->pagination->initialize($config);
                    $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : '0';
                   
                    $con['start'] = $page ;
                   
                    $con['limit'] = $config['per_page'];
        

                    $categoryListing = $this->Page->listingCategoryAddressCount1($country,$cityId,$address,$cat_id,$name, $lat, $long,$config['per_page'],$page, $near_by);
                   //  echo "<br>".$this->db->last_query();
                    // die();
                    $data['name']  =  $name;
                    $data['address']  =  $address;
                    $data['near_by']  =  $near_by;
                    
                    $data['category_id']  =  $cat_id;
                    $data["links"] = $this->pagination->create_links();
                    $data['page'] = $page;
                
                    $fc_listing = array();
                    
                    $catListing = array();
                    if(!empty($categoryListing)){
                        foreach ($categoryListing as $key => $arr) {
                            $resArr = getLocationLatLng($arr['business_address']);
                            if($resArr){
                                $arr['latitude'] = $resArr['lat'];
                                $arr['longitude'] = $resArr['lng'];
                            }
                            else{
                                $arr['latitude'] = '';
                                $arr['longitude'] = '';   
                            }
                            $sql1 = "SELECT * FROM business_review where business_id='".$arr['business_id']."' AND status='1' ORDER BY id DESC";
                            $query1 = $this->db->query($sql1);
                            $result1 = $query1->result_array();
                            $review_list = $result1; 
                            $rating = 0;
                            $count = 0;
                            if(!empty($review_list))
                            {
                                foreach ($review_list as $key => $val) 
                                {
                                    $rating = $rating+$val['rating'];
                                    $count++;
                                }
                            }
                            $avg_rating = $rating/$count;
                            $arr['avg_rating'] = $avg_rating;
                            $catListing[] = $arr;
                        }
                    }
                    $data['categoryListing'] = $catListing;
                    $sql = "SELECT add_img_path FROM otd_advertisement where add_status=1 ORDER BY rand() limit 2";
                    $query = $this->db->query($sql);
                    $result = $query->result_array();

                    $data['active_tab_listings'] = "active";
                    $data['advertisements'] = $result;
                    $data['getratingstatus'] = $this->Page->getratingstatus();
                    $data['business_categories'] = $this->Page->getBusinessCategories();
                    $this->template->load('home_layout_listing', 'contents', 'listing',$data);
}
        
        
        
    // public function Listingsdharmendra($cat_id=false) {

    //      $user_id = $this->session->userdata('user_id');
    //      $this->db->select('userid');
    //      $this->db->from('transaction_table');
    //      $this->db->where('userid',$user_id);
    //      $que2 = $this->db->get();
    //      $data['current_plan'] = $que2->result();

    //      $data['top_content'] = $this->Page->getTopContent();
    //      $ip       =$_SERVER['REMOTE_ADDR'];
    //      $geo      = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
    //      $current_country  = $geo["geoplugin_countryName"];

    //      $current_city     = $geo["geoplugin_city"];
    //      $current_lat      = $geo["geoplugin_latitude"];
    //      $current_long     = $geo["geoplugin_longitude"];

    //      //echo $current_city;die;

    //      $current_address  = $this->getAddress($current_lat,$current_long);
    //      $currentlocation = explode(', ', $current_address);
    //      $current_address = $current_address;
        
    //      if($this->input->post('address')|| $this->input->post('category_id'))
    //      {

    //         echo "I am here";
    //         $address = $this->input->post('address');
    //         $cat_id = $this->input->post('category_id');

    //         $autolocation = explode(', ', $address);
    //         $count =count($autolocation);


    //         $len =$count-3;
    //         $cityId = $autolocation[$len];
    //         $country = end($autolocation);
    //         $new_address = $address;
    //         $name  = $this->input->post('business_name');

    //         //echo $cityId;
    //         //echo "<br/>";
    //         //echo $country;
    //         //echo "<br/>";
    //         //echo $new_address;die;

    //         $latlong    =   getLocationLatLng($address);
    //         $lat   =  $latlong['lat'];
    //         $long  =  $latlong['lng'];
            
    //         if($cat_id){
               
    //             //$categoryListing = $this->Page->listingCategoryCount($lat,$long,$cat_id);
    //             //$featureCategoryListings = $this->Page->listingCategoryFeatureCount($lat,$long,$cat_id);

    //             if($name){
    //                 $categoryListing = $this->Page->listingCategoryAddressCount1($lat,$long,$cat_id,$name);
    //                 $featureCategoryListings = $this->Page->listingCategoryAddressFeatureCount1($lat,$long,$cat_id,$name);

    //             } else {
    //             $categoryListing = $this->Page->listingCategoryCount($lat,$long,$cat_id);
    //             $featureCategoryListings = $this->Page->listingCategoryFeatureCount($lat,$long,$cat_id);

    //             }

    //         }
    //         else if($name){
    //            $categoryListing = $this->Page->listingAddressCount1($lat,$long,$name);

    //            $featureCategoryListings = $this->Page->listingAddressFeatureCount1($lat,$long,$name);

    //            //print_r("<pre/>");
    //            //print_r($categoryListing);
    //            //die;
    //            //$featureCategoryListings = $this->Page->listingAddressFeatureCount1($country,$cityId,$new_address,$name);
    //            // echo "address";

    //         }
    //         else {
    //             //$categoryListing = $this->Page->listingCount($lat,$long);
    //             //$featureCategoryListings = $this->Page->feature_listings_details($lat,$long);


    //             //echo $country;die;

    //             $categoryListing = $this->Page->listingCount($lat,$long);

    //             //print_r("<pre/>");
    //             //print_r($categoryListing);
    //             //die;

                
    //             $featureCategoryListings = $this->Page->feature_listings_details($lat,$long);

    //             //print_r("<pre/>");
    //             //print_r($categoryListing);
    //             //die;

    //             //print_r("<pre/>");
    //             //print_r($featureCategoryListings);
    //             //die;      
    //         }
            
    //         $data['category_id']  =  $cat_id;
    //         $data['name']  =         $this->input->post('business_name');
    //         $data['address']  =         $this->input->post('address');
            
    //     }
    //     else {
    //         echo "No I am here";
    //          $con['conditions'] = array("features_ads"=>0,'status'=>1);
    //          $fcon['conditions'] = array("features_ads"=>1,'status'=>1);
    //          $cat_count = $this->Page->listingCount1($current_country,$current_city,$current_address);
    //          $config['base_url'] = base_url()."listings";
    //          $config['total_rows'] = count($cat_count);
    //          $config['per_page'] = 10;
    //          $config['uri_segment'] = 2;
    //          $config['num_links'] = 2;
    //          $config['cur_tag_open'] = '<a class="current">';
    //          $config['cur_tag_close'] = '</a>';
    //          $config['next_link'] = '<b>Next</b>';
    //          $config['prev_link'] = '<b>Previous</b>';
    //          $this->pagination->initialize($config);
    //          $page = ($this->uri->segment(2)) ? $this->uri->segment(2) : '0';
    //          $con['start'] = $page;
    //          $con['limit'] = $config['per_page'];
    //          $categoryListing = $this->Page->listings_details1($config['per_page'],$page,$current_country,$current_city,$current_address);
    //         $featureCategoryListings = $this->Page->feature_listings_details1($current_country,$current_city,$current_address);
    //          $data["links"] = $this->pagination->create_links();
    //          $data['page'] = $page;
    //     }
    //     $fc_listing = array();
    //     if(!empty($featureCategoryListings)) {


    function get_lat_long($address){

    $address = str_replace(" ", "+", $address);

    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    //echo $lat;die;
    return $lat.','.$long;
} 
public function listings2($cat_id=false){
             $user_id = $this->session->userdata('user_id');
             $this->db->select('userid');
             $this->db->from('transaction_table');
             $this->db->where('userid',$user_id);
             $que2 = $this->db->get();
             $data['current_plan'] = $que2->result();

             $ip       =$_SERVER['REMOTE_ADDR'];
             $geo      = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
             $current_country  = $geo["geoplugin_countryName"];
             $current_city     = $geo["geoplugin_city"];
             $current_lat      = $geo["geoplugin_latitude"];
             $current_long     = $geo["geoplugin_longitude"];
             $data['top_content'] = $this->Page->getTopContent();

             $current_address  = $this->getAddress($current_lat,$current_long);


             //echo $cat_id;die;

             $catchange  = base64_decode($cat_id);

             //echo $catchange;die;
             //$catchange1  = base64_decode($cat_id);

             //echo "testing";die;
             if($catchange){

                          $this->db->select('*');
                          $this->db->from('otd_business_category');
                          $this->db->where(array('cat_parent!='=>0,'cat_id'=>$catchange));
                          $query = $this->db->get();

                          if($query->num_rows()>0){


                            //echo "test";die;
                           
                            //$con['conditions'] = array("sub_cat_id" => $catchange1,"features_ads"=>0,'status'=>1);
                            //$fcon['conditions'] = array("sub_cat_id" => $catchange1,"features_ads"=>1,'status'=>1);

                            //echo "test";die;

                            //$categoryListing = $this->Page->listingSubCategoryCount($current_lat,$current_long,$catchange);


                            //$featureCategoryListings = $this->Page->listingSubCategoryFeatureCount($current_lat,$current_long,$catchange);


                 $categoryListing = $this->Page->listingSubCategoryCount1($current_country,$current_city,$current_address,$catchange);

                 $featureCategoryListings = $this->Page->listingSubCategoryFeatureCount1($current_country,$current_city,$current_address,$catchange);



                             
                               //print_r("<pre/>");
                            //print_r($featureCategoryListings);
                            //die;

                          } else {

                            //echo $catchange;die;

                            //echo "test1";die;

                           // $con['conditions'] = array("cat_id" => $catchange,"features_ads"=>0,'status'=>1);
                            //$fcon['conditions'] = array("cat_id" => $catchange,"features_ads"=>1,'status'=>1);

                           // $categoryListing = $this->Page->listingCategoryCount($current_lat,$current_long,$catchange);
                            //$featureCategoryListings = $this->Page->listingCategoryFeatureCount($current_lat,$current_long,$catchange);

                            $categoryListing = $this->Page->listingCategoryCount1($current_country,$current_city,$current_address,$catchange);

                            $featureCategoryListings = $this->Page->listingCategoryFeatureCount1($current_country,$current_city,$current_address,$catchange);

                    }

            } 

            $fc_listing = array();
        if(!empty($featureCategoryListings))
        {
            foreach ($featureCategoryListings as $key => $arr) 
            {
              
              
                $resArr = getLocationLatLng($arr['business_address']);
             

                if($resArr)
                {
                    $arr['latitude'] = $resArr['lat'];
                    $arr['longitude'] = $resArr['lng'];
                }
                else
                {
                    $arr['latitude'] = '';
                    $arr['longitude'] = '';   
                }
                $sql1 = "SELECT * FROM business_review where business_id='".$arr['business_id']."' AND status='1' ORDER BY id DESC";
                $query1 = $this->db->query($sql1);
                $result1 = $query1->result_array();
                $review_list = $result1; 

                $rating = 0;
                $count = 0;
                if(!empty($review_list))
                {
                    foreach ($review_list as $key => $val) 
                    {
                        $rating = $rating+$val['rating'];
                        $count++;
                    }
                }
                $avg_rating = $rating/$count;
                
                $arr['avg_rating'] = $avg_rating;
               
                $fc_listing[] = $arr;
            }
        }
        $data['featureCategoryListings'] = $fc_listing;

        $catListing = array();
        if(!empty($categoryListing))
        {
            foreach ($categoryListing as $key => $arr) 
            {
                $resArr = getLocationLatLng($arr['business_address']);
              
                if($resArr)
                {
                    $arr['latitude'] = $resArr['lat'];
                    $arr['longitude'] = $resArr['lng'];
                }
                else
                {
                    $arr['latitude'] = '';
                    $arr['longitude'] = '';   
                }

                $sql1 = "SELECT * FROM business_review where business_id='".$arr['business_id']."' AND status='1' ORDER BY id DESC";
                $query1 = $this->db->query($sql1);
                $result1 = $query1->result_array();
                $review_list = $result1; 

                $rating = 0;
                $count = 0;
                if(!empty($review_list))
                {
                    foreach ($review_list as $key => $val) 
                    {
                        $rating = $rating+$val['rating'];
                        $count++;
                    }
                }
                $avg_rating = $rating/$count;
                
                $arr['avg_rating'] = $avg_rating;
                $catListing[] = $arr;
            }
        }
        $data['categoryListing'] = $catListing;

         $sql = "SELECT add_img_path FROM otd_advertisement where add_status=1 ORDER BY rand() limit 2";
         $query = $this->db->query($sql);
         $result = $query->result_array();
         $data['advertisements'] = $result;
         $data['getratingstatus'] = $this->Page->getratingstatus();
      
         $data['business_categories'] = $this->Page->getBusinessCategories();

        $this->template->load('home_layout', 'contents', 'listing',$data);

} 


    /*
     * User Catetgory Listing
     */

    public function listing_data() {
        
        
        //echo "test";die;
      
        $address = $this->input->post('address');
        $cat_id = $this->input->post('category_id');

        if($cat_id){
            $con['conditions'] = array("cat_id" => $cat_id,"features_ads"=>0,'status'=>1);
            $fcon['conditions'] = array("cat_id" => $cat_id,"features_ads"=>1,'status'=>1);
        }else {
            $con['conditions'] = array("features_ads"=>0,'status'=>1);
            $fcon['conditions'] = array("features_ads"=>1,'status'=>1);
        }

        $categoryListing = $this->Page->getHomeCatgoryListingRows($con);
        $featureCategoryListings = $this->Page->getHomeCatgoryListingRows($fcon);

        $fc_listing = array();
        if(!empty($featureCategoryListings))
        {
            foreach ($featureCategoryListings as $key => $arr) 
            {
                $sql1 = "SELECT * FROM business_review where business_id='".$arr['business_id']."' AND status='1' ORDER BY id DESC";
                $query1 = $this->db->query($sql1);
                $result1 = $query1->result_array();
                $review_list = $result1; 

                $rating = 0;
                $count = 0;
                if(!empty($review_list))
                {
                    foreach ($review_list as $key => $val) 
                    {
                        $rating = $rating+$val['rating'];
                        $count++;
                    }
                }
                $avg_rating = $rating/$count;
                //$featureCategoryListings[$key]['avg_rating'] = $avg_rating;
                $arr['avg_rating'] = $avg_rating;
                //$featureCategoryListings[$key] = $arr;
                $fc_listing[] = $arr;
            }
        }
        $data['featureCategoryListings'] = $fc_listing;

        $catListing = array();
        if(!empty($categoryListing))
        {
            foreach ($categoryListing as $key => $arr) 
            {
                $sql1 = "SELECT * FROM business_review where business_id='".$arr['business_id']."' AND status='1' ORDER BY id DESC";
                $query1 = $this->db->query($sql1);
                $result1 = $query1->result_array();
                $review_list = $result1; 

                $rating = 0;
                $count = 0;
                if(!empty($review_list))
                {
                    foreach ($review_list as $key => $val) 
                    {
                        $rating = $rating+$val['rating'];
                        $count++;
                    }
                }
                $avg_rating = $rating/$count;
                //$categoryListing[$key]['avg_rating'] = $avg_rating;
                $arr['avg_rating'] = $avg_rating;
                $catListing[] = $arr;
            }
        }
        $data['categoryListing'] = $catListing;
        

        if ($fc_listing) {
            foreach ($fc_listing as $featureListing) {

                $list_id  = base64_encode($featureListing['business_id']);
                ?>   
                <div class="featured-box listing-sub grid-system">
                    <div class="image-lis-sub">                 
                        <span class="feat-ad">FEATURED</span>
                        <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img src="<?php echo base_url('uploads/business/' . $featureListing['business_image1']); ?>"></a>
                    </div>
                    <div class="image-txt-sub">
                        <div class="img-txt-subhead">
                            <div class="profile">
                                <span class="discount-tab"><?php echo $featureListing['business_price']?></span>
                            </div>
                            <h3><?php echo $featureListing['business_title']?></h3>
                            <span class="rating-main">
                                <ul>
                                    <?php
                                    for ($i=0; $i < (int)$featureListing['avg_rating']; $i++) { 
                                        ?><li><i class="fa fa-star"></i></li><?php
                                    }
                                    for ($i=0; $i < 5-(int)$featureListing['avg_rating']; $i++) { 
                                        ?><li><i class="fa fa-star-o"></i></li><?php
                                    }
                                    ?>
                                    <!--<li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>-->
                                </ul>
                            </span>
                        </div>
                        <p class="add-l"><?php 
                                            
                                                echo !empty(text_wrap($featureListing['business_desc'],15)) ? text_wrap($listing['business_desc'],15) : "";?></p>
                        <div class="dsply-num">
                            <span class="add-new"><i class="fa fa-map-marker"></i><?php echo $featureListing['business_address']?></span>
                            <button class="btn btn-default like"><i class="fa fa-heart-o"></i></button>
                        </div>
                    </div>
                </div>
                <?php 
            }
        }

        if ($catListing) {
            foreach ($catListing as $listing) {

                $list_id  = base64_encode($listing['business_id']);
                ?>
                <div class="listing-sub grid-system">
                    <div class="image-lis-sub">
                        <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img src="<?php echo base_url('uploads/business/' . $listing['business_image1']); ?>">
                    </div>
                    <div class="image-txt-sub">
                        <div class="img-txt-subhead">
                            <div class="profile">
                                <span class="pro-img"><img src="images/profile.jpg"></span>
                                <!-- <span class="discount-tab">Discount 30%</span> -->
                            </div>
                            <h3><?php echo $listing['business_title']?></h3>
                            <span class="rating-main">
                                <ul>
                                    <?php
                                    for ($i=0; $i < (int)$listing['avg_rating']; $i++) { 
                                        ?><li><i class="fa fa-star"></i></li><?php
                                    }
                                    for ($i=0; $i < 5-(int)$listing['avg_rating']; $i++) { 
                                        ?><li><i class="fa fa-star-o"></i></li><?php
                                    }
                                    ?>
                                    <!--<li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>-->
                                </ul>
                            </span>
                        </div>
                        <p class="add-l"><?php 
                                            
                                                echo !empty(text_wrap($listing['business_desc'],15)) ? text_wrap($listing['business_desc'],15) : "";?> </p>
                        <div class="dsply-num">
                            <span class="add-new"><i class="fa fa-map-marker"></i> <?php echo $listing['business_address']?></span>
                            <button class="btn btn-default"><i class="fa fa-heart-o"></i></button>
                        </div>
                    </div>
                </div>
                <?php
            } 
        }

    }

    public function Offer() {


        $user_id = $this->session->userdata('user_id');
        $this->db->select('userid');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $que2 = $this->db->get();
        $data['current_plan'] = $que2->result();

       // $data['business_offers'] = $this->Page->getBusinessOffers();


        //$sql = "SELECT u.* FROM `vv_users_list`u  where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 7";
        $data['top_content']        = $this->Page->getTopContent();
        $current_date= date('Y-m-d');

        //echo $current_date;die;
       //$sql = "SELECT offer_image,business_id FROM bdc_offers where ".$current_date."<= start_date";
        $sql = "SELECT offer_image,business_id FROM bdc_offers where offer_image!='' ORDER BY rand() limit 5";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['business_offers'] = $result;

        //print_r("<pre/>");
        //print_r($result);die;
     

        //print_r("<pre/>");
        //print_r($data['business_offers']);die;
        $data['active_tab_offer'] = "active";
        $this->template->load('home_layout', 'contents', 'offer',$data);
    }
    public function about_us() {
        $data['top_content']        = $this->Page->getTopContent();
        $data['similar_pages'] = $this->Page->getPageRows();
        $data['active_tab_about'] = "active";
        $this->template->load('home_layout', 'contents', 'about',$data);
    }
    public function about_us_mobile() {
        $data['top_content']        = $this->Page->getTopContent();
        $data['similar_pages'] = $this->Page->getPageRows();
        //$this->template->load('home_layout', 'contents', 'about_mobile',$data);
        $this->load->view('about_mobile',$data);
    }

    public function term_condition() {
        $data['top_content']        = $this->Page->getTopContent();
        $arr['conditions'] = array('pg_meta_tag' => 'Term-Condition');
        $data['similar_pages'] = $this->Page->getPageRows($arr);
        $data['active_tab_term_condition'] = "active";
        $this->template->load('home_layout', 'contents', 'term_condition',$data);
    }
    public function term_condition_mobile() {
        $data['top_content']        = $this->Page->getTopContent();
        $arr['conditions'] = array('pg_meta_tag' => 'Term-Condition');
        $data['similar_pages'] = $this->Page->getPageRows($arr);
        //$this->template->load('home_layout', 'contents', 'term_condition_mobile',$data);
        $this->load->view('term_condition_mobile',$data);
    }

    public function privacy_policy() {
        $data['top_content']        = $this->Page->getTopContent();
        $arr['conditions'] = array('pg_meta_tag' => 'Privacy-Policy');
        $data['similar_pages'] = $this->Page->getPageRows($arr);
        $data['active_tab_term_privacy_policy'] = "active";
        $this->template->load('home_layout', 'contents', 'privacy_policy',$data);

    }
    public function privacy_policy_mobile() {
        $data['top_content']        = $this->Page->getTopContent();
        $arr['conditions'] = array('pg_meta_tag' => 'Privacy-Policy');
        $data['similar_pages'] = $this->Page->getPageRows($arr);
        //$this->template->load('home_layout', 'contents', 'privacy_policy',$data);
        $this->load->view('privacy_policy_mobile',$data);
    }

    public function contact_us() {

        $sql = "SELECT * FROM contact_us_content";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['contact_us']        = $result;
        $data['top_content']        = $this->Page->getTopContent();
        $data['active_tab_contact'] = "active";
        $this->template->load('home_layout', 'contents', 'contact',$data);
    }

    public function listing_details($list_id=false) 
    {
        //$sql = "SELECT add_img_path FROM otd_advertisement where add_status=1 ORDER BY rand() limit 2";
        $list_id  = base64_decode($list_id);
        $user_id = $this->session->userdata('user_id');
        $sql = "SELECT offer_image FROM bdc_offers where business_id='".$list_id."'";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['business_offers'] = $result;

        $sql1 = "SELECT * FROM business_review where business_id='".$list_id."' AND status='1' ORDER BY id DESC";
        $query1 = $this->db->query($sql1);
        $result1 = $query1->result_array();
        $review_list = $result1;

        if(!empty($review_list))
        {
            foreach ($review_list as $key => $val) 
            {
                $sql2 = "SELECT * FROM users where user_id='".$val['user_id']."'";
                $query2 = $this->db->query($sql2);
                $result2 = $query2->row_array();
                $review_list[$key]['user_profile_pic'] = $result2['user_profile_pic'];
                $review_list[$key]['user_full_name'] = $result2['user_firstname'].' '.$result2['user_lastname'];
            }
        }
        $data['review_list'] = $review_list;

        //echo $list_id;die;
        $con['conditions'] = array("business_id"=>$list_id);
        $data['listingDetails'] = $this->Page->getHomeCatgoryListingRows($con);

        $cond['conditions'] = array("business_id !="=>$list_id,'cat_id' => $data['listingDetails'][0]['cat_id']);
        $cond['sorting'] = array("business_id"=>'desc');
        $cond['limit'] = 3;
        $cond['start'] = 0;
        $data['listing_list'] = $this->Page->getHomeCatgoryListingRows($cond);
        $data['getratingstatus'] = $this->Page->getratingstatus();
        $data['checkrating'] =  $this->Page->checkrating($user_id,$list_id);
        //print_r("<pre/>");
        //print_r($data['listingDetails']);
        //die;
        $data['top_content']        = $this->Page->getTopContent();
        $data['listing_content']        = $this->Page->getListingContent();
        $data['user_content']        = $this->Page->getuserinfo($user_id);
        $data['listing_fav']        = $this->Page->getfavContent($list_id,$user_id);

        //print_r("<pre/>");
        //print_r($data['listing_content']);
        //die;
        $this->template->load('home_layout', 'contents', 'listing_details',$data);
    }

    public function CategoryListing() {

        $user_id = $this->session->userdata('user_id');
        $this->db->select('userid');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $que2 = $this->db->get();
        $data['current_plan'] = $que2->result();
        $sql = "SELECT add_img_path FROM otd_advertisement where add_status=1 ORDER BY rand() limit 2";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['advertisements'] = $result;


        $data['top_content']        = $this->Page->getTopContent();
        $data['business_categories'] = $this->Page->getBusinessCategories();
        $data['active_tab_category'] = "active";
        $this->template->load('home_layout', 'contents', 'category_details',$data);
    }

    public function Cat($arg) {

        $cat_id  = base64_decode($arg);

        $sql = "SELECT add_img_path FROM otd_advertisement where add_status=1 ORDER BY rand() limit 2";
        $query = $this->db->query($sql);
        $result = $query->result_array();
        $data['advertisements'] = $result;

        $data['top_content']        = $this->Page->getTopContent();
        $data['business_categories1'] = $this->Page->getBusinessCategories();
        $data['business_categories'] = $this->Page->getsubCategories($cat_id);
        $this->template->load('home_layout', 'contents', 'subcategory_details',$data);
    }
    public function lang($en) {
       //$lang_id = $this->session->userdata(23);
        //echo $en;die;
        //echo $en;die;

        //echo $this->uri->segment(1);die;

      $en = $_POST['id'];
       if($en=='en'){ 
         $this->session->set_userdata('lang_id', 20);
       } else {
         $this->session->set_userdata('lang_id', 23);
       }  
       /*if($this->uri->segment(1) == "listings"){
        redirect('listings');
       }else if($this->uri->segment(1) == "categories") {
        redirect('categories');
       }else if($this->uri->segment(1) == "offers") {
        redirect('offers');
       }else if($this->uri->segment(1) == "signup") {
        redirect('signup');
       }else if($this->uri->segment(1) == "about-us") {
        redirect('about-us');
       }else if($this->uri->segment(1) == "term-condition") {
        redirect('term-condition');
       }else if($this->uri->segment(1) == "privacy-policy") {
        redirect('privacy-policy');
       }else if($this->uri->segment(1) == "home") {
        redirect('home');
       }else if($this->uri->segment(1) == "contact-us") {
        redirect('contact-us');
       }else if($this->uri->segment(1) == "dashboard") {
        redirect('dashboard');
       }else if($this->uri->segment(1) == "addlisting") {
        redirect('addlisting');
       }*/
       //else if($this->uri->segment(1) == "details") {
        //redirect('listings_details');
       //}
       
       //echo $this->uri->segment(2);die;
       //$lang_id = $this->session->userdata('lang_id');
       //echo $lang_id;die;

       //if($this->session->userdata('isUserLoggedIn')) {
        //redirect('dashboard');
       //} else {
        //redirect('home');
       //}

       
    }
    //public function lang1($ar1,$ar2){
        //echo "test";die;

    //}

    public function testing(){


        //echo "test";die;

         $ip       =$_SERVER['REMOTE_ADDR'];
         $geo      = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$ip"));
         $current_country  = $geo["geoplugin_countryName"];
         $current_city     = $geo["geoplugin_city"];
         $current_lat      = $geo["geoplugin_latitude"];
         $current_long     = $geo["geoplugin_longitude"];
         //$con['cur_lat'] = $current_lat;
         //$con['cur_lng'] = $current_long;

          $dist = "round(( 3959 * acos( cos( radians(".$current_lat. ") ) * cos( radians( lat ) ) 
                * cos( radians(lon) - radians(".$current_long. ")) + sin(radians(".$current_long.")) 
                * sin( radians(lat)))), 2) AS distance having distance>=3";


        $query = "select *, ".$dist."  from bdc_business";

                
                $query1 = $this->db->query($query);
                $result1 = $query1->result_array();
                $review_list = $result1;



         //print_r("<pre/>");
         //print_r($review_list);
         //die;



    }

    public function locations()
    {
        $current_country =$this->session->userdata('current_country');
        $query = "SELECT city FROM tbl_city where country like '%$current_country%' ";
  
        $data = array("city"=>"All Locations".$current_country );
        $query1 = $this->db->query($query);
        $result = $query1->result_array();
        array_unshift($result,  $data);
        header('Content-Type" => application/json');
        echo json_encode($result);
    }

    public function locationssearch()
    {
        $data = array();
        header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Listing Management');
	    $this->template->load('home_layout', 'contents' , 'searchme', $data);
       
    }
   

}

/*

SELECT 
    ( 3959 * acos( cos( radians(42.290763) ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(-71.35368)) + sin(radians(42.290763)) 
   * sin( radians(user_lat)))) AS distance 
FROM vv_users_list 

ORDER BY distance
*/

