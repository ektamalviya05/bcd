<?php 
 ?>
 <?php
foreach ($getratingstatus as  $value) {
$show = $value['show_front'];  
 } 

 //echo $category_id;
  ?>
<div class="main-reg-content">  
    <section class="main-map-sec">      
        <div class="main-listing-sec wow slideInLeft animated">
            <section class="filter-sec wow fadeInDown animated">
                <div class="">
                        <div class="main-srch-form-sec">
                            <div class="row lis-page-row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="main-srch-sub my-listing-search">
                                        <div class="row">
                                            <!-- <form id="searching_form" onsubmit="return getListingData();" method="post"> -->

                                                <form id="searching_form" action="<?php echo base_url(); ?>listings" method="post">
                                              
                                              <div class="col-sm-3 col-md-3">
                          <div class="form-control frm-brdr-no">
                            <input type="text" placeholder="Name" name="business_name" value="<?php if(isset($name)){echo $name;}?>">  
                          </div>
                        </div>
                                              
                                               <div class="col-sm-4 col-md-4">


                                                    <div class="form-group form-sub form_first">
                                                        <input type="address" name="address" id="autocomplete" class="form-control add-form" placeholder="What are you looking for?" placeholder="Enter your address" onFocus="geolocate()">
                                                        <label class="error" for="autocomplete"></label>


                                                    </div>
                                                </div>
                                                <div class="col-sm-3 col-md-3">
                                                    <div class="form-group form-sub">
                                                        <select class="form-control cat-form" name="category_id" id="category_id">
                                                            <option value="">All</option>
                                                            <?php
                                                            if ($business_categories) 
                                                            {
                                                                foreach ($business_categories as $categories) 
                                                                {


                                                                  if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $category_name  = $categories['cat_name'];
                                 
                                                                  } else{
                                                                    $category_name  = $categories['cat_namea'];

                                                                  }
                                                                    ?>
                                                                    <!-- <option value="<?php echo $categories['cat_id']; ?>"><?php echo $category_name; ?></option> -->


                                                                      <option value="<?php echo $categories['cat_id']; ?>" <?php if($categories['cat_id']==
                                                $category_id){ echo "selected"; } ?>><?php echo $category_name; ?></option>   

                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                        <label class="error" for="category_id"></label>
                                                    </div>
                                                </div>
                                                <div class="col-sm-2 col-md-2">
                                                    <button id="btnListingData" class="btn btn-default" type="Submit">
                                                        <i class="fa fa-search"></i>
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="lis-grid">
                                        <button type="button" class="btn main-list"><i class="fa fa-bars"></i></button>
                                        <button type="button" class="btn main-grid act"><i class="fa fa-th-large"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    <!--</form>-->
                </div>
            </section>

            
            <div class="main-add">

                <?php
                    if ($advertisements) {
                        foreach ($advertisements as $advertisement) {
                            ?>
                <div class="col-sm-6">
                    <div class="add-img">
                        <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>"> 
                    </div>
                </div>
                <?php } } ?>
                
            </div>  

            <div id="listing_block">
            <?php
            
                    if ($featureCategoryListings) {

                           foreach ($featureCategoryListings as $featureListing) {

                            $list_id  = base64_encode($featureListing['business_id']);

                            if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 
                                 $featured_name  = "FEATURED";
                               } else {
                             
                                 $featured_name  = "متميز";
                               }
                               $business_name  = $featureListing['business_title'];
                               $business_desc  = $featureListing['business_desc'];
                               $business_address  = $featureListing['business_address'];
                        ?>   
            <div class="featured-box listing-sub grid-system">
                <div class="image-lis-sub">                 
                    <span class="feat-ad"><?php echo $featured_name;?></span>
                    <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img src="<?php echo base_url('uploads/business/' . $featureListing['business_image1']); ?>"></a>
                </div>
                <div class="image-txt-sub">
                    <div class="img-txt-subhead">
                         <!--<div class="profile">
                            <span class="pro-img"><img src="images/profile.jpg"></span>
                           
                        </div>-->
                     
                        <h3><?php echo $business_name;?></h3>
                        <?php 
                        if($show)
                        {
                        ?>
                        <span class="rating-main">
                            <ul>
                                <?php
                                for ($i=0; $i < (int)$featureListing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star"></i></li><?php
                                }
                                for ($i=0; $i < 5-(int)$featureListing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star-o"></i></li><?php
                                }
                                ?>
                           
                            </ul>
                        </span>
                        <?php
                        }
                        ?>
                    </div>
                    <p class="add-l"><?php 
                                        
                                            echo !empty(text_wrap($business_desc,15)) ? text_wrap($listing['business_desc'],15) : "";?></p>
                    <div class="dsply-num">
                        <span class="add-new"><i class="fa fa-map-marker"></i><?php echo $business_address;?></span>
                      
                    </div>
                </div>
            </div>

            <?php }} ?>     

            <?php
                    if ($categoryListing) {

                           foreach ($categoryListing as $listing) {

                           
                            $list_id  = base64_encode($listing['business_id']);
                            if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 
                                 $featured_name1  = "FEATURED";
                               } else {
                           
                                 $featured_name1  = "متميز";
                               }
                               $business_name1  = $listing['business_title'];
                               $business_desc1  = $listing['business_desc'];
                               $business_address1  = $listing['business_address'];
                               $distance = $listing['distance'];
                               $is_featured = $listing['features_ads'];
                        ?>
            <div class="listing-sub grid-system">
                <div class="image-lis-sub">
                <?php
                if($is_featured == 1)
                {
                ?>
                <span class="feat-ad"><?php echo $featured_name1. $is_featured;?></span>
                <?php
                    }
                ?>
                    <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img src="<?php echo base_url('uploads/business/' . $listing['business_image1']); ?>"></a>
                </div>
                <div class="image-txt-sub">
                    <div class="img-txt-subhead">
                        <!--<div class="profile">
                            <span class="pro-img"><img src="images/profile.jpg"></span>
                           
                        </div>-->
                        <h3><?php echo $business_name1;?></h3>
                        <?php 
                        if($show)
                        {
                        ?>
                        <span class="rating-main">
                            <ul>
                                <?php
                                for ($i=0; $i < (int)$listing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star"></i></li><?php
                                }
                                for ($i=0; $i < 5-(int)$listing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star-o"></i></li><?php
                                }
                                ?>
                      
                            </ul>
                        </span>
                        <?php
                        }
                        ?>
                    </div>
                    <p class="add-l"><?php 
                                        
                                            echo !empty(text_wrap($business_desc1,15)) ? text_wrap($business_desc1,15) : "";?> </p>
                    <div class="dsply-num">
                        <span class="add-new"><i class="fa fa-map-marker"></i> <?php echo $business_address1?> :  <?php echo $distance?></span>
                      
                    </div>
                </div>
            </div>
            <?php

              } }
            ?>

            </a>

             <div class="read-more paginat-cus">
                <nav aria-label="Page navigation example" class="servic_pagi custom_pagination">
                    <?php echo $links ;  ?>
                </nav>
            </div>
        </div>

         

            <div class="main-add">
                <?php
                    if ($advertisements) {
                        foreach ($advertisements as $advertisement) {
                            ?>
                <div class="col-sm-6">
                    <div class="add-img">
                        <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>"> 
                    </div>
                </div>
                <?php } } ?>
                
            </div>
            </div>
        </div>
        <div class="map-main wow slideInRight animated">
          
            <div id="map" style="width: 100%; height: 100%;"></div>
        </div>
    </section>
</div>
