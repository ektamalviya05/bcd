
<style type="text/css">
  .about .bread_out {
    padding: 50px 0 20px
  }
  .about.inner_head {
    height: 200px;
  }
  #crumbs {
      text-align: left;
  }
  .map-contact iframe {
    width:  100%;
    height: 320px;
  }
  .about.inner_head .single-data {
    padding:  5px 0 15px;
  }
  .heading-section.cont h2 {
    margin-top:  0;
  }
  section.contact-sec {
    padding: 50px 0;
}
.input-field {
    width:  100%;
    margin: 0 0 20px
}
.input-field label {
    text-align: left;
    width: 100%;
    font-size: 13px;
    padding: 0 0 0 5px;
}
.input-field input {
    width: 100%;
    padding: 8px 15px;
    height: 42px;
    border-radius: 21px;
    border: 1px solid #c02d64;
}
.input-field textarea.text-com {
    width:  100%;
    height: 90px !important;
    width: 100% !important;
    border: 1px solid #c02d64;
    border-radius: 20px;
}
section.contact-information {
    padding: 40px 0;
    /* background:  #f1f1f1; */
}
.co-info span {
    padding: 10px;
    background: #fff;
    border: 2px solid #be2d64;
    width: 50px;
    height: 50px;
    text-align: center;
    display: inline-block;
    font-size: 25px;
    color: #be2d64;
    border-radius: 50%;
    margin: 0 auto 10px;
}
.co-info {
    text-align: center;
    border: 2px solid #e2e2e2;
    padding: 20px 10px;
    border-radius: 7px;
}
.co-info h2 {
    margin: 2px 0 15px;
    font-size: 22px;
    font-weight: 600;
}
.co-info p {
    margin:  0;
}
.co-info p a {
    color: #be2d64;
    font-weight:  600;
}
</style>
<div class="about inner_head cont_sec">
    <div class="bread_out">
      <div class="container">
        <div class="row">
          <!-- <div class="col-sm-12">
            <div class="contact-head">
              <div id="crumbs">
                <a href="#">Home</a> / <span class="current">Temple</span>
              </div>
            </div>
          </div> -->
          <div class="col-sm-12">
            <div class="single-data">
              <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
              <h2><?php echo $contact_us[0]['contact_us_en']; ?></h2>
              <p><?php echo $contact_us[0]['contact_us_top_en']; ?></p>
              <?php }else {?>
               <h2><?php echo $contact_us[0]['contact_us_ar']; ?></h2>
              <p><?php echo $contact_us[0]['contact_us_top_ar']; ?></p>
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>
<div class="main-wrap">
  <section>
    <div class="map-contact">
      <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d25578613.853690047!2d42.107302343451565!3d38.50172954007418!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1525254932973" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
  </section>
    <section class="contact-sec">
        <div class="container">
            <div id="success_msg" style="position: static;margin-bottom: 10px;"></div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="heading-section cont">
                      <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
                      <h2 class="wow fadeInDown animated"><?php echo $contact_us[0]['get_in_touch_en']; ?></h2>
                      <?php }else {?>
                      <h2 class="wow fadeInDown animated"><?php echo $contact_us[0]['get_in_touch_ar']; ?></h2>
                      <?php } ?>
                    </div>
                </div>
            </div>
            <div class="contact-form">
              <form id="contact" method="post">
                <div class="row">
                  <div class="col-sm-4">
                    <div class="input-field">
                      <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
                      <label><?php echo $contact_us[0]['name_field_en']; ?></label>
                      <?php }else {?>
                      <label><?php echo $contact_us[0]['name_field_ar']; ?></label>
                      <?php } ?>
                      <input type="text" id="name" name="name">
                    </div>
                  </div>

                  <div class="col-sm-4">
                    <div class="input-field">
                      <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
                      <label><?php echo $contact_us[0]['email_field_en']; ?></label>
                      <?php }else {?>
                      <label><?php echo $contact_us[0]['email_field_ar']; ?></label>
                      <?php } ?>
                      <input type="email" id="email" name="email">
                    </div>
                  </div>
                  <div class="col-sm-4">
                    <div class="input-field">
                      <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
                      <label><?php echo $contact_us[0]['phone_field_en']; ?></label>
                      <?php }else {?>
                      <label><?php echo $contact_us[0]['phone_field_ar']; ?></label>
                      <?php } ?>
                      <input type="number" id="phone" name="phone">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12">
                    <div class="input-field">
                      <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
                      <label><?php echo $contact_us[0]['Comment_field_en']; ?></label>
                      <?php }else {?>
                      <label><?php echo $contact_us[0]['Comment_field_ar']; ?></label>
                      <?php } ?>
                      <textarea class="text-com" id="comment" name="comment"></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 sub-btn">
                    <!-- <input type="submit" name="" class="btn-submit" value="Send Message"> -->
                     <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
                    <button class="btn btn-default form-btn" id="contactSubmit" name="btnSubmit" type="submit"><?php echo $contact_us[0]['save_button_en']; ?></button>
                     <?php }else {?>
                     <button class="btn btn-default form-btn" id="contactSubmit" name="btnSubmit" type="submit"><?php echo $contact_us[0]['save_button_ar']; ?></button>
                     <?php } ?>

                  </div>
                </div>
              </form>
            </div>
        </div>
    </section>

  <section class="contact-information">
    <div class="container">
      <div class="row">
        <div class="col-sm-4">
          <div class="co-info">
            <span><i class="fa fa-map-marker"></i></span>
            <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
            <h2><?php echo $contact_us[0]['address_en']; ?></h2>
            <?php echo $contact_us[0]['address_value_en']; ?>
            <?php }else {?>
            <h2><?php echo $contact_us[0]['address_ar']; ?></h2>
            <?php echo $contact_us[0]['address_value_ar']; ?>
            <?php } ?>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="co-info">
            <span><i class="fa fa-phone"></i></span>
            <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
            <h2><?php echo $contact_us[0]['phone_field_en']; ?></h2>
            <?php }else {?>
            <h2><?php echo $contact_us[0]['phone_field_ar']; ?></h2>
            <?php } ?>
            <p><a href="#">3462 5145 5454</a></p>
            <p><a href="#">3462 5145 5454</a></p>
          </div>
        </div>
        <div class="col-sm-4">
          <div class="co-info">
            <span><i class="fa fa-envelope-o"></i></span>
            <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
            <h2><?php echo $contact_us[0]['email_field_en']; ?> & <?php echo $contact_us[0]['link_en']; ?></h2>
            <?php }else {?>
            <h2><?php echo $contact_us[0]['email_field_ar']; ?> & <?php echo $contact_us[0]['link_ar']; ?></h2>
             <?php } ?>
            <p><a href="#">ilbaitinfo@gmail.com</a></p>
            <p><a href="#">www.ilbait.com.in</a></p>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<div id="myModal" class="modal fade contactusmodle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-body">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
      <center><h2>Thank you !</h2></center>
      <center><h3>Contact form successfully submitted. Thank you, We will get back to you soon!</h3></center>
  </div>
  
</div>
</div>
</div>

<style>
.contactusmodle
{
  z-index: 9999999;

}
  
</style>

