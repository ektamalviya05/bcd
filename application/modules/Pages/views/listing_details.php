<style type="text/css">
    #infobar {
      width: 500px;
      height: 200px;
      border: 1px solid #333;
      overflow-y: scroll;
    }
    .content {
      width: 100%;
    }
    .content div {
      color: blue;
      opacity: 0.5;
    }
    .content div span {
      color: #666;
    }
    .info {
      margin-top: 10px;
    }
    .example {
      background-color: #F8F8F8;
    }
    .submit {
      margin-top: 20px;
    }
    @media only screen and (max-width: 720px) {
      #mobile {
        padding: 20px 0;
      }
    }
</style>

<?php

//print_r("<pre/>");
//print_r($listing_content);die;
if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {

     $send_enquiry  = $listing_content[0]['send_enquiry_en'];
     $descripiton   = $listing_content[0]['descripiton_field_en'];
     $review   = $listing_content[0]['review_en'];
     $related_business_field   = $listing_content[0]['related_business_en'];
     $submit_review_button_field   = $listing_content[0]['submit_review_button'];
     $email  = $listing_content[0]['email_field_english'];
     $reviews   = $listing_content[0]['reviews_en'];
     $name_field      = $listing_content[0]['name_en'];
     $message_field      = $listing_content[0]['message_en'];
     $enquiry_form_field      = $listing_content[0]['enquiry_form_en'];
     $close_field      = $listing_content[0]['close_button_en'];
     $enter_your_name      = $listing_content[0]['enter_your_name'];
     $enter_your_email      = $listing_content[0]['enter_your_email'];
     $enter_your_message      = $listing_content[0]['enter_your_message'];

     $rate_field      = $listing_content[0]['rate'];
     $write_review      = $listing_content[0]['write_reviews'];
     $your_name      = $listing_content[0]['your_name'];
     $email_address      = $listing_content[0]['email_address'];
     $tell_you_exp      = $listing_content[0]['tell_you_exp'];
     $save_review      = $listing_content[0]['save_review'];

     $your_rating  = "Your Rating";

} else {
     $send_enquiry  = $listing_content[0]['send_enquiry_ar'];
     $descripiton   = $listing_content[0]['descripiton_field_ar'];
     $review   = $listing_content[0]['review_ar'];
     $related_business_field   = $listing_content[0]['related_business_ar'];
     $submit_review_button_field   = $listing_content[0]['submit_review_button_ar'];
     $email  = $listing_content[0]['email_field_ar'];
     $reviews   = $listing_content[0]['reviews_ar'];
     $name_field      = $listing_content[0]['name_ar'];
     $message_field      = $listing_content[0]['message_ar'];
     $enquiry_form_field      = $listing_content[0]['enquiry_form_ar'];
     $close_field      = $listing_content[0]['close_button_ar'];
     $enter_your_name      = $listing_content[0]['enter_your_name_ar'];
     $enter_your_email      = $listing_content[0]['enter_your_email_ar'];
     $enter_your_message      = $listing_content[0]['enter_your_message_ar'];


     $rate_field      = $listing_content[0]['rate_ar'];
     $write_review      = $listing_content[0]['write_reviews_ar'];
     $your_name      = $listing_content[0]['your_name_ar'];
     $email_address      = $listing_content[0]['email_address_ar'];
     $tell_you_exp      = $listing_content[0]['tell_you_exp_ar'];
     $save_review      = $listing_content[0]['save_review_ar'];

     $your_rating  = "تقييمك";
}

$this->db->select('*');
$this->db->where('cat_id', $listingDetails[0]['cat_id']);
$query = $this->db->get('otd_business_category');
$result   = $query->result_array();

$image  = $result['cat_img_path'];

//print_r("<pre/>");
//print_r($result);
//die;

?>


<div class="inner_head">
        <div class="bread_out">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                      <!--   <div class="contact-head hidden">
                            <div id="crumbs">
                                <a href="#">Home</a> / <span class="current">Temple</span>
                            </div>
                        </div> -->
                         <?php
                         foreach ($getratingstatus as  $value) {
                            $show = $value['show_front'];  
                        } 
                         ?>
                    </div>
                    <div class="col-sm-6">
                        <div class="single-data">
                            <!--<div class="dis-ac"><span>Discount 10%</span></div>-->
                            <div class="dis-ac hidden">
                                <span>
                                Price <?php echo $listingDetails[0]['business_price']; ?>$
                                </span>
                            </div>
                            <h2><?php echo $listingDetails[0]['business_title']; ?></h2>
                            <div class="img-txt-subhead">
                                <span class="rating-main">
                                    <?php
                                    $rating = 0;
                                    $count = 0;
                                    if(!empty($review_list))
                                    {
                                        foreach ($review_list as $key => $val) 
                                        {
                                            $rating = $rating+$val['rating'];
                                            $count++;
                                        }
                                    }
                                    $avg_rating = $rating/$count;
                                    ?>
                                    <ul>
                                        <?php 
                                        if($show)
                                        {
                                        ?>
                                        <?php
                                        for ($i=0; $i < (int)$avg_rating; $i++) { 
                                            ?><li><i class="fa fa-star"></i></li><?php
                                        }
                                        for ($i=0; $i < 5-(int)$avg_rating; $i++) { 
                                            ?><li><i class="fa fa-star-o"></i></li><?php
                                        }
                                        ?>
                                        <!--<li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>-->
                                        
                                        <li>( <?php echo (!empty($review_list) ? count($review_list) : 0); ?> <?php echo $review;?> )</li>
                                        <?php
                                        }
                                        ?>
                                    </ul>
                                </span>
                            </div>
                            <div class="con-details">
                                <ul>
                                    <li><i class="fa fa-map-marker"></i> <?php echo $listingDetails[0]['business_address'].', '.$listingDetails[0]['business_country']; ?></li>
                                    <li><i class="fa fa-phone"></i> <?php echo $listingDetails[0]['business_mobile']; ?></li>
                                    <li><i class="fa fa-envelope-o"></i> <?php echo $listingDetails[0]['business_email']; ?></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-sm-2">
                      <?php if($listingDetails[0]['business_logo']) {?>
                    <div class="list_det_logo"><a class="navbar-brand" href="home"><img src="assets_new/images/logo.png"></a> 
                    </div>
                    <?php } else { ?>
                    <div class="list_det_logo"><a class="navbar-brand" href="home"><img src="<?php echo base_url('assets/img/category/' . $result[0]['cat_img_path']); ?>"></a> 
                    </div>
                    <?php } ?>
                  </div>
                    
                    <div class="col-sm-4">
                        <!--<div class="right-side-angle">
                            <ul>
                                <li><i class="fa fa-heart-o"></i> Save</li>
                                <li><button class="review-btn">Submit Review <i class="fa fa-angle-right"></i></button></li>
                            </ul>
                        </div>-->
                        <div class="pulir pull-right">
                            <button class="btn enquiry btn-lg" data-toggle="modal" data-target="#modalForm">
                                <i class="fa fa-envelope"></i> <?php echo $send_enquiry;?>
                            </button>
                            <?php
                            if(!empty($this->session->userdata('user_id')))
                            {
                            ?>
                                <?php
                                //echo $listing_fav;
                                if($listing_fav==1)
                                { 
                                ?>
                                
                                <button class="btn enquiry btn-lg" id="<?php echo $listingDetails[0]['business_id']; ?>">
                                  <i class="fa fa-heart "></i>
                                </button>
                                
                                <?php 
                                }
                                else
                                {
                                ?>
                                
                                 <button class="btn fav enquiryone enquiry btn-lg" id="<?php echo $listingDetails[0]['business_id']; ?>">
                                  <i class="fa fa-heart-o favlist"></i>
                                </button>
                                
                               <?php
                                }
                            }
                            else 
                            {
                            ?>
                            <a href="<?php echo base_url(); ?>login">
                            <button class="btn fav enquiryone enquiry btn-lg">
                            <i class="fa fa-heart-o favlist"></i>
                            </button>
                            </a>

                            <?php
                            }
                           ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

<div style="padding:0px 0 50px;background: #fff;">
    <section class="list_details_page">
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="single-details">
                        <div class="deteils-img">
                            <div class="demo">
                                <div class="item">            
                                    <div class="clearfix">
                                        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                            <?php
                                            if(!empty($listingDetails[0]['business_image1']))
                                            {
                                                ?>
                                                <li data-thumb="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image1']); ?>">
                                                    <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image1']); ?>" />
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            

                                            <?php
                                            if(!empty($listingDetails[0]['business_image2']))
                                            {
                                                ?>
                                                <li data-thumb="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image2']); ?>">
                                                    <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image2']); ?>" />
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            

                                            <?php
                                            if(!empty($listingDetails[0]['business_image3']))
                                            {
                                                ?>
                                                <li data-thumb="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image3']); ?>">
                                                    <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image3']); ?>" />
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            

                                            <?php
                                            if(!empty($listingDetails[0]['business_image3']))
                                            {
                                                ?>
                                                <li data-thumb="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image3']); ?>">
                                                    <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image3']); ?>" />
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            

                                            <?php
                                            if(!empty($listingDetails[0]['business_image4']))
                                            {
                                                ?>
                                                <li data-thumb="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image4']); ?>">
                                                    <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image4']); ?>" />
                                                </li>
                                                <?php
                                            }
                                            ?>
                                            
                                            <!-- <li data-thumb="images/inner-banner.jpg">
                                                <img src="images/inner-banner.jpg" />
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>                      

                        <div class="temp-details">
                            <div class="dmv-heading">
                                <h3><?php echo $descripiton;?></h3>
                            </div>
                            <p><?php echo $listingDetails[0]['business_desc']; ?></p>

                                                    
                        </div>

                        <div class="offer-img">
                            <div class="offer_ntabs">

                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#offer" aria-controls="offer" role="tab" data-toggle="tab">Offer</a></li>
                                    <li role="presentation"><a href="#services" aria-controls="services" role="tab" data-toggle="tab">Services</a></li>
                                </ul>

                                  <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="offer">
                                        <div id="demo">
                                          <div id="add-slid" class="owl-carousel">
                                            <?php
                                            if(!empty($business_offers))
                                            {
                                                foreach ($business_offers as $key => $arr) 
                                                {
                                                    ?>
                                                    <?php if($arr['offer_image']){ ?>
                                                    <div class="item"><img src="<?php echo base_url().'uploads/business/'.$arr['offer_image']; ?>"></div>
                                                    <?php } else { ?>
                                                    <div class="item">Offers Not Available</div>
                                                    <?php }  ?>


                                                    <?php
                                                }
                                            }else{
                                              echo "Result not found";
                                            }
                                            ?>
                                         
                                          </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="services">
                                        <ul class="services_list">

                                             <?php
                                            if(!empty($business_services))
                                            {
                                                foreach ($business_services as $key => $arr) 
                                                {
                                                    ?><li><a href="#"><?php echo $arr['services_name']?></a></li><?php
                                                }
                                            }
                                            else{
                                              echo "Services Not Available";
                                            }
                                            ?>
                                            
                                            
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            
                        </div>
                        <?php
                        foreach ($getratingstatus as  $value) {
                            $show = $value['show_front'];  
                        }
                        if($show)
                        {
                        ?>
                        <div class="temp-details review-box">
                            <div class="review-div">
                                <div class="main-review">
                                    <h2><?php echo (!empty($review_list) ? count($review_list) : 0); ?> <?php echo $reviews;?></h2>
                                </div>
                                <?php
                                if(!empty($review_list))
                                {
                                    foreach ($review_list as $key => $val) 
                                    {
                                        
                                        ?>
                                        <div class="row">
                                            <div class="col-sm-2">
                                                <div class="pro-img-re">
                                                    <?php
                                                    if(!empty($val['user_profile_pic']))
                                                    {
                                                        ?><img src="uploads/profile/<?php echo $val['user_profile_pic']; ?>"><?php
                                                    }
                                                    else
                                                    {
                                                        ?><img src="images/photo-1.png"><?php    
                                                    }
                                                    ?>
                                                    
                                                </div>
                                            </div>
                                            <div class="col-sm-10">
                                                <div class="review-containt">
                                                    <h2> 
                                                        <?php
                                                        if(!empty($val['user_full_name']))
                                                        {
                                                            echo $val['user_full_name'];
                                                        } 
                                                        else
                                                        {
                                                            echo $val['name'];     
                                                        }
                                                        
                                                        ?> 
                                                        <span><?php echo (!empty($val['create_date']) ? date('d-m-Y H:i:s a',strtotime($val['create_date'])) : ''); 
                                                        ?>
                                                            <!--6 months ago--></span></h2>
                                                    <span class="rating-main">
                                                        <ul>
                                                            <?php
                                                            if(!empty($val['rating']))
                                                            {
                                                                for ($i=0; $i < $val['rating']; $i++) 
                                                                { 
                                                                    ?><li><i class="fa fa-star"></i></li><?php
                                                                }

                                                                for ($i=0; $i < 5-$val['rating']; $i++) 
                                                                { 
                                                                    ?><li><i class="fa fa-star-o"></i></li><?php
                                                                }
                                                            }
                                                            else
                                                            {
                                                                ?>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <?php
                                                            }
                                                            ?>
                                                            <!--<li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>
                                                            <li><i class="fa fa-star"></i></li>-->                                                     
                                                        </ul>
                                                    </span>

                                                    <p>“<?php echo $val['description']; ?>”</p>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                                <!--<div class="row">
                                    <div class="col-sm-2">
                                        <div class="pro-img-re">
                                            <img src="images/profile.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="review-containt">
                                            <h2> Cole Harris <span>6 months ago</span></h2>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>                                                     
                                                </ul>
                                            </span>

                                            <p>“There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, ”</p>
                                        </div>
                                    </div>
                                </div>-->
                            </div>
                        </div>

                        <?php
                        if(!empty($this->session->userdata('user_id')))
                        {
                            ?>
                            <div class="temp-details review-box">
                                <div class="review-div">
                                    <div id="success_msg" style="position: static;margin-bottom: 10px;"></div>
                                    <div class="main-review">
                                        <h2><?php echo $rate_field;?> & <?php echo $write_review;?></h2>
                                    </div>
                                    <!-- onsubmit="return submitReview();" -->
                                    <form class="rating" id="ratingForm">
                                        <div class="row">                                   
                                            <div class="col-sm-12">
                                                <div class="review-containt">
                                                    <span class="rating-main">
                                                        <ul>
                                                            <!--
                                                                <li>Your Rating </li>
                                                                <li> <i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                                <li><i class="fa fa-star-o"></i></li>
                                                            -->
                                                            <?php if(empty($checkrating)){ ?>                                                
                                                            <li><?php echo  $your_rating;?> </li>
                                                            <li> 
                                                                <input type="number" class="rating" id="test" name="rating" data-min="1" data-max="5" value="0">
                                                            </li> 
                                                            <?php } ?>       
                                                        </ul>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        foreach ($user_content as  $valuearr) {
                                          $visitor_name  = $valuearr['user_firstname']." ".$valuearr['user_lastname'];
                                          $visitor_email = $valuearr['user_email'];
                                        }
                                        ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="revire-input">
                                                    <input type="hidden" name="business_id" value="<?php echo $listingDetails[0]['business_id']; ?>">
                                                    <input type="hidden" required id="visitor_name" name="visitor_name" value="<?php echo $visitor_name;?>">
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="revire-input">
                                                    <input type="hidden" id="visitor_email" required name="email" value="<?php echo $visitor_email ;?>">
                                                </div>
                                            </div>

                                            <div class="col-sm-12">
                                                <div class="revire-input">
                                                    <textarea name="description" id="description" required placeholder="<?php echo $tell_you_exp;?>"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12 sub-btn">
                                                <input type="submit" id="btnReview" name="btnSubmit" value="<?php echo $save_review;?>">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?php
                        }
                        else
                        {
                        ?>
                        <button class="btn btn-success review-theam" data-toggle="modal" data-target="#popUpWindow" ><?php echo $submit_review_button_field;?></button>
                        <?php 
                        }
                        ?>
                        <?php
                        }
                        ?>


                    </div>
                </div>

                <div class="col-sm-4">
                
 					<div class="side_map_se">	               
                    <div class="side-map">
                        <!--<iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14722.96212982255!2d75.92889325!3d22.700701!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1496980678600" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                        <div id="map" style="height:350px;  margin:20px auto 0;"></div>
                        
                    </div>
                     <?php
                          $p =$listingDetails[0]['lat'].','.$listingDetails[0]['lon'];

                          
                         
                    ?>
                    <div class="map_icon_sec"><i class="fa fa-map-o" aria-hidden="true"></i> <a href="https://www.google.com/maps?daddr=<?php echo $p;?>">Get Directions</a></div>
                    </div>
                    
                    <!--<div class="conta-details">
                        <div class="data-div">
                            <p><i class="fa fa-map-marker"></i>
                            400 E 34th St</p>
                            <a href="#">Get Direction</a>

                        </div>
                        <div class="data-div">
                            <p><i class="fa fa-phone"></i>
                            545-985-8727</p>
                        </div>
                        <div class="data-div">
                            <p><i class="fa fa-envelope-o"></i>
                            listing@site-example.com</p>
                        </div>
                        <div class="data-div">
                            <p><i class="fa fa-globe"></i>
                            https://yoursite.com/</p>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>-->
        
                    <!--<div class="right-search">
                        <div class="dmv-heading">
                            <h3>SEARCH PROPERTY</h3>
                        </div>
                        <div class="rigt-search">
                            <form>
                                <div class="input-serch">
                                    <input type="text" name="" placeholder="Type loaction...">
                                </div>
                                <div class="input-serch">
                                    <select>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                    </select>
                                </div>

                                <div class="input-serch">
                                    <select>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                    </select>
                                </div>

                                <div class="rooms">
                                    <div class="col-sm-6">
                                        <div class="input-serch">
                                            <select>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-serch">
                                            <select>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="range_slide">
                                    <div class="range-example-2"></div>
                                </div>

                                <div class="search-btn">
                                    <button class="search">
                                        Search Now
                                        <span class="icon-s">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </button>
                                </div>
                            </form>  
                        </div>
                    </div>-->

                    <div class="featur-pro">
                        <div class="dmv-heading">
                            <h3><?php echo $related_business_field;?></h3>
                        </div>
                        <ul>
                            <?php
                            if(!empty($listing_list))
                            {
                                foreach ($listing_list as $key => $arr) 
                                {

                                    $list_id  = base64_encode($arr['business_id']);
                                    ?>
                                    <li>
                                        <?php
                                        if(!empty($arr['business_image1']))
                                        {
                                            ?><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img style="width: 80px;height: 80px;" src="<?php echo base_url('uploads/business/' . $arr['business_image1']); ?>"></a><?php
                                        } elseif(!empty($arr['business_image2'])) {
                                            ?><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img style="width: 80px;height: 80px;" src="<?php echo base_url('uploads/business/' . $arr['business_image2']); ?>"></a><?php
                                        } elseif(!empty($arr['business_image3'])) {
                                            ?><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img style="width: 80px;height: 80px;" src="<?php echo base_url('uploads/business/' . $arr['business_image3']); ?>"></a><?php
                                        } elseif(!empty($arr['business_image4'])) {
                                            ?><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img style="width: 80px;height: 80px;" src="<?php echo base_url('uploads/business/' . $arr['business_image4']); ?>"></a><?php
                                        } else {
                                            ?><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img style="width: 80px;height: 80px;" src="images/list-1.jpg"></a><?php
                                        }
                                        ?>
                                        
                                        <div class="fre-details">
                                            <span><?php echo $arr['business_title']; ?></span>
                                            <h4><?php echo (!empty($arr['business_address']) ? $arr['business_address'].', ' : '').$arr['business_country']; ?></h4>
                                            <p class="hidden"><?php echo '$'.(!empty($arr['business_price']) ? $arr['business_price'] : '00'); ?></p>
                                        </div>
                                    </li>
                                    <?php
                                }
                            }
                            ?>
                            <!--<li>
                                <img src="images/list-1.jpg">
                                <div class="fre-details">
                                    <span>Appartement</span>
                                    <h4>Court Bellflower, CA 90706</h4>
                                    <p>$3510.000</p>
                                </div>
                            </li>

                            <li>
                                <img src="images/list-2.jpg">
                                <div class="fre-details">
                                    <span>Appartement</span>
                                    <h4>Court Bellflower, CA 90706</h4>
                                    <p>$3510.000</p>
                                </div>
                            </li>                       

                            <li>
                                <img src="images/list-3.jpg">
                                <div class="fre-details">
                                    <span>Appartement</span>
                                    <h4>Court Bellflower, CA 90706</h4>
                                    <p>$3510.000</p>
                                </div>
                            </li>-->
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>  
</div>

<script>
    jQuery(document).ready(function() {
        jQuery("#content-slider").lightSlider({
                   loop:true,
                   keyPress:true
                });
                jQuery('#image-gallery').lightSlider({
                    gallery:true,
                    item:1,
                    thumbItem:4,
                    slideMargin: 0,
                    speed:500,
                    auto:true,
                    loop:true,
                    responsive : [
                    {
                        breakpoint: 800,
                        settings: { // settings for width 480px to 800px
                        thumbItem: 4
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {  // settings for width 0px to 480px
                        thumbItem: 2
                    }
                }
            ],
            onSliderLoad: function() {
                jQuery('#image-gallery').removeClass('cS-hidden');
            }  
            });
        });
</script>
<script>
    $(document).ready(function() {
      $("#add-slid").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true
      });
    });
</script>
<script>
    $(document).ready(function() {
      var one = $(".range-example-2").asRange({
        range: true,
        limit: true,
        tip: {
          active: 'onMove'
        }
      });
      console.log(one.asRange('set', [30, 60]));
    });
    $(document).ready(function(){
        $(".log-op").click(function(){
            $(".login-drop").fadeToggle();
        });
    });
</script>


<!-- Map code start -->
<?php
$address    = trim(urlencode(@$listingDetails[0]['business_address'].', '.@$listingDetails[0]['business_country']));
/*$city         = trim(urlencode($_POST['city']));
$state      = trim(urlencode($_POST['state']));
$country    = trim(urlencode($_POST['country']));
$zip        = trim($_POST['zip']);*/

//$geocode  =   file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$add.',+'.$city.',+'.$state.',+'.$country.'&sensor=false');
$geocode    =   file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$address.'&sensor=false');

$output     = json_decode($geocode); //Store values in variable
//print_r($output);

if($output->status == 'OK'){ // Check if address is available or not
    $latitude = $output->results[0]->geometry->location->lat; //Returns Latitude
    
    $longitude = $output->results[0]->geometry->location->lng; // Returns Longitude

}

?>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script type="text/javascript">
    $(document).ready(function () {
        // Define the latitude and longitude positions
        var latitude = parseFloat("<?php echo $latitude; ?>"); // Latitude get from above variable
        var longitude = parseFloat("<?php echo $longitude; ?>"); // Longitude from same
        var latlngPos = new google.maps.LatLng(latitude, longitude);
        
        // Set up options for the Google map
        var myOptions = {
            zoom: 14,
            center: latlngPos,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControlOptions: true,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE
            }
        };
        
        // Define the map
        map = new google.maps.Map(document.getElementById("map"), myOptions);
        
        // Add the marker
        var marker = new google.maps.Marker({
            position: latlngPos,
            map: map,
            title: "Your Location"
        });
        
    });
</script>
<!-- Map code close -->

<!-- Rating code start -->
<script type="text/javascript">
    (function ($) {

      $.fn.rating = function () {

        var element;

        // A private function to highlight a star corresponding to a given value
        function _paintValue(ratingInput, value) {
          var selectedStar = $(ratingInput).find('[data-value=' + value + ']');
          selectedStar.removeClass('glyphicon-star-empty').addClass('glyphicon-star');
          selectedStar.prevAll('[data-value]').removeClass('glyphicon-star-empty').addClass('glyphicon-star');
          selectedStar.nextAll('[data-value]').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
        }

        // A private function to remove the selected rating
        function _clearValue(ratingInput) {
          var self = $(ratingInput);
          self.find('[data-value]').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
          self.find('.rating-clear').hide();
          self.find('input').val('').trigger('change');
        }

        // Iterate and transform all selected inputs
        for (element = this.length - 1; element >= 0; element--) {

          var el, i, ratingInputs,
            originalInput = $(this[element]),
            max = originalInput.data('max') || 5,
            min = originalInput.data('min') || 0,
            clearable = originalInput.data('clearable') || null,
            stars = '';

          // HTML element construction
          for (i = min; i <= max; i++) {
            // Create <max> empty stars
            stars += ['<span class="glyphicon glyphicon-star-empty" data-value="', i, '"></span>'].join('');
          }
          // Add a clear link if clearable option is set
          if (clearable) {
            stars += [
              ' <a class="rating-clear" style="display:none;" href="javascript:void">',
              '<span class="glyphicon glyphicon-remove"></span> ',
              clearable,
              '</a>'].join('');
          }

          el = [
            // Rating widget is wrapped inside a div
            '<div class="rating-input">',
            stars,
            // Value will be hold in a hidden input with same name and id than original input so the form will still work
            '<input type="hidden" name="',
            originalInput.attr('name'),
            '" value="',
            originalInput.val(),
            '" id="',
            originalInput.attr('id'),
            '" />',
            '</div>'].join('');

          // Replace original inputs HTML with the new one
          originalInput.replaceWith(el);

        }

        // Give live to the newly generated widgets
        $('.rating-input')
          // Highlight stars on hovering
          .on('mouseenter', '[data-value]', function () {
            var self = $(this);
            _paintValue(self.closest('.rating-input'), self.data('value'));
          })
          // View current value while mouse is out
          .on('mouseleave', '[data-value]', function () {
            var self = $(this);
            var val = self.siblings('input').val();
            if (val) {
              _paintValue(self.closest('.rating-input'), val);
            } else {
              _clearValue(self.closest('.rating-input'));
            }
          })
          // Set the selected value to the hidden field
          .on('click', '[data-value]', function (e) {
            var self = $(this);
            var val = self.data('value');
            self.siblings('input').val(val).trigger('change');
            self.siblings('.rating-clear').show();
            e.preventDefault();
            false
          })
          // Remove value on clear
          .on('click', '.rating-clear', function (e) {
            _clearValue($(this).closest('.rating-input'));
            e.preventDefault();
            false
          })
          // Initialize view with default value
          .each(function () {
            var val = $(this).find('input').val();
            if (val) {
              _paintValue(this, val);
              $(this).find('.rating-clear').show();
            }
          });

      };

      // Auto apply conversion of number fields with class 'rating' into rating-fields
      $(function () {
        if ($('input.rating[type=number]').length > 0) {
          $('input.rating[type=number]').rating();
        }
      });

    }(jQuery));


</script>
<!-- Rating code close -->

<div class="login-new-modal modal fade" id="popUpWindow">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="main-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="log-in-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-box log-in-form">
                                    <h2 class="text-center">Login</h2>
                                    <?php if ($this->session->flashdata('success')) { ?>
                                    <div class="alert alert-success message">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                    <?php echo $this->session->flashdata('success'); ?></div>
                                    <?php } ?>
                                    <?php if ($this->session->flashdata('error')) { ?>
                                    <div class="alert alert-danger message">
                                        <button type="button" class="close" data-dismiss="alert">x</button>
                                    <?php echo $this->session->flashdata('error'); ?></div>
                                    <?php } ?>
                                    <form name="loginforms" id="loginforms" method="post" action="<?php echo base_url(); ?>Users/do_login">
                                        <div class="f-login">
                                            <input type="eamil" name="email" id="email" placeholder="Email">
                                        </div>
                                        <div class="f-login">
                                            <input type="Password" name="password" id="password" placeholder="Password">
                                        </div>
                                        <input type="hidden" name="redirecturl" value="<?php echo 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>">
                                        <div class="submit-login">
                                            <input type="submit" name="" value="Login">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="theame-modal modal fade" id="modalForm" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">&times;</span>
                <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo $enquiry_form_field;?></h4>
            </div>
            
            <!-- Modal Body -->
            <div class="modal-body">
                <p class="statusMsg"></p>
                <form  id="enquiryForm" name="enquiryForm" method="post">
                    <div class="form-group">
                        <label for="inputName"><?php echo $name_field;?></label>
                        <input type="text" class="form-control" name="inputName" id="inputName" placeholder="<?php echo $enter_your_name;?>"/>
                        <span id="nameerror" style="color: red;font-size: 13px;font-weight: 600;"></span>
                        <input type="hidden" name="bid" id="bid" value="<?php echo $listingDetails[0]['business_id']; ?>">
                        <input type="hidden" name="email_from" id="email_from" 
                        value="<?php echo $listingDetails[0]['business_email']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="inputEmail"><?php echo $email;?></label>
                        <input type="email" class="form-control" name="inputEmail" id="inputEmail" placeholder="<?php echo $enter_your_email;?>"/>
                        <span id="emailerror" style="color: red;font-size: 13px;font-weight: 600;"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputMessage"><?php echo $message_field;?></label>
                        <textarea class="form-control" id="inputMessage" name="inputMessage" placeholder="<?php echo $enter_your_message;?>"></textarea>
                        <span id="messageerror" style="color: red;font-size: 13px;font-weight: 600;"></span>
                    </div>
                
            </div>
            
            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default close-btn" data-dismiss="modal"><?php echo $close_field;?></button>
                <!-- <button type="button" class="btn btn-primary submitBtn" onclick="submitContactForm()">SUBMIT</button> -->
                <button type="submit" class="btn btn-primary submitBtn" ><?php echo $send_enquiry;?></button>
            </div>
            </form>
        </div>
    </div>
</div>

<style>
    #popUpWindow{
      z-index: 999999999;
    }
    #modalForm{
         z-index: 999999999;
    }
    .enquiry {
        background: #be2d64;
        color: #fff;
    }
</style>
<script></script>