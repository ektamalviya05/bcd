<?php 
 ?>
 <style>
 .btnFilterData {
    display: inline-block;
    padding: 0px 10px;
    margin-bottom: 0;
    font-size: 14px;
    font-weight: 400;
    line-height: 1.42857143;
    text-align: center;
    border: 1px solid transparent;
    border-radius: 4px;
}
.lis-grid {
     padding-top: 14px!important;
}
.range-slider {
 margin: 0px 0 0 0%;
}

.range-slider {
 width: 100%;

}
input[type=range] {
    display: block;
    width: 75%!important;
    float: left;
    padding-top:0px;
}

.range-slider__range {
 -webkit-appearance: none;
 width: calc(100% - (73px));
 height: 12px;
 border-radius: 5px;
 background: #d7dcdf;
 outline: none;
 padding: 0;
 margin: 0;
}
.range-slider__range::-webkit-slider-thumb {
 -webkit-appearance: none;
         appearance: none;
 width: 10px;
 height: 20px;
 /* border-radius: 50%; */
 background: #c85480;
 cursor: pointer;
 transition: background .15s ease-in-out;
}
.range-slider__range::-webkit-slider-thumb:hover {
 background: #c85480;
}
.range-slider__range:active::-webkit-slider-thumb {
 background: #c85480;
}
.range-slider__range::-moz-range-thumb {
 width: 12px;
 height: 12px;
 border: 0;
 /* border-radius: 50%; */
 background: #c85480;
 cursor: pointer;
 transition: background .15s ease-in-out;
}
.range-slider__range::-moz-range-thumb:hover {
 background: #c85480;
}
.range-slider__range:active::-moz-range-thumb {
 background: #c85480;
}
.range-slider__range:focus::-webkit-slider-thumb {
 box-shadow: 0 0 0 3px #fff, 0 0 0 6px #c85480;
}

.range-slider__value {
 display: inline-block;
 position: relative;
 width: 60px;
 color: #fff;
 line-height: 20px;
 text-align: center;
 border-radius: 3px;
 background: #c85480;
 padding: 0px 10px;
 margin-left: 8px;
 margin-top:-5px;
} 
 .range-slider__value:after {
 /* position: absolute;
 top: 8px;
 left: -7px;
 width: 0;
 height: 0;
 border-top: 7px solid transparent;
 border-right: 7px solid #2c3e50;
 border-bottom: 7px solid transparent;
 content: ''; */
} 

::-moz-range-track {
 background: #d7dcdf;
 border: 0;
}

input::-moz-focus-inner,
input::-moz-focus-outer {
 border: 0;
}

.nearby {
width: 80%!important;
float: left!important;
padding-right: 10px!important;
padding-left: 10px!important;
padding-top: 22px!important;
}

     </style>
 <?php
foreach ($getratingstatus as  $value) {
$show = $value['show_front'];  
 } 
 //echo $category_id;
  ?>
<div class="main-reg-content">  
    <section class="main-map-sec">      
        <div class="main-listing-sec wow slideInLeft animated">
            <section class="filter-sec wow fadeInDown animated">
                <div class="">
                        <div class="main-srch-form-sec">
                            <div class="row lis-page-row">
                                <form id="searching_form" name="searching_form" action="<?php echo base_url(); ?>listings" method="post">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="main-srch-sub my-listing-search">
                                            <div class="row">
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class="form-control frm-brdr-no">
                                                            <input type="text" placeholder="Name" name="business_name" value="<?php if(isset($name)){echo $name;}?>">  
                                                        </div>
                                                    </div>
                                                
                                                    <div class="col-sm-4 col-md-4">
                                                        <div class="form-group form-sub form_first">
                                                            <input type="address" name="address" id="autocomplete" class="form-control add-form" placeholder="What are you looking for?" placeholder="Enter your address" onFocus="geolocate()">
                                                            <label class="error" for="autocomplete"></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3 col-md-3">
                                                        <div class="form-group form-sub">
                                                            <select class="form-control cat-form" name="category_id" id="category_id">
                                                                <option value="">All Categories</option>
                                                                <?php
                                                                if ($business_categories) 
                                                                {
                                                                    foreach ($business_categories as $categories) 
                                                                    {
                                                                        if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                            $category_name  = $categories['cat_name'];
                                                                        } else{
                                                                            $category_name  = $categories['cat_name'];
                                                                        }
                                                                        ?>
                                                                            <!-- <option value="<?php echo $categories['cat_id']; ?>"><?php echo $category_name; ?></option> -->
                                                                        <option value="<?php echo $categories['cat_id']; ?>" 
                                                                        <?php if($categories['cat_id']==$category_id)
                                                                        { echo "selected"; } ?>>
                                                                        <?php 
                                                                        echo $category_name; ?>
                                                                        </option>   
                                                                    <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </select>
                                                            <label class="error" for="category_id"></label>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 col-md-2">
                                                        <button id="btnListingData" class="btn btn-default" name="btnListingData" type="Submit">
                                                            <i class="fa fa-search"></i> Search
                                                        </button>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12" style="float:right">
                                       
                                        <div class="lis-grid">
                                            <button type="button" class="btn main-list"><i class="fa fa-bars"></i></button>
                                            <button type="button" class="btn main-grid act"><i class="fa fa-th-large"></i></button>
                                        </div>

                                        <div class="nearby">
                                        <div class="range-slider" >
 <input class="range-slider__range" name="near_by" id="near_by" type="range" value="<?php if(isset($near_by)){echo $near_by;}?>" min="0" step="5" max="1000">
 <span class="range-slider__value"><?php if(isset($near_by)){echo $near_by;}?></span>
 <!-- <span class="range_values" id="range_values"><?php if(isset($near_by)){echo $near_by;}?></span> -->
 <button id="btnFilterData" name="fiterdata" class="btnFilterData" type="Submit">
                                                           Filter
                                                        </button>
</div>


                                            <!-- <select class="form-control cat-form" name="near_by" id="near_by">
                                                <option value="250"  <?php if($near_by==250){ echo "selected"; } ?>>Select Near By <?php echo $near_by;  ?></option>
                                                <option value="5" <?php if($near_by==5){ echo "selected"; } ?>>5 KM</option>
                                                <option value="20" <?php if($near_by==20){ echo "selected"; } ?>>20 KM</option>
                                                <option value="50" <?php if($near_by==50){ echo "selected"; } ?>>50 KM</option>
                                                <option value="100" <?php if($near_by==100){ echo "selected"; } ?>>100 KM</option>
                                                <option value="150" <?php if($near_by==150){ echo "selected"; } ?>>150 KM</option>
                                                <option value="200" <?php if($near_by==200){ echo "selected"; } ?>>200 KM</option>
                                            </select> -->
                                        </div>
                                    </div>
                                
                                    <!-- <input type="range" name="near_by" id="near_by" max="300" min="5" step="5" value="100"> -->
                      

                                  
                                </form>
                            </div>
                        </div>                      
                    
                </div>
            </section>

            
            <div class="main-add">

                <?php
                    if ($advertisements) {
                        foreach ($advertisements as $advertisement) {
                            ?>
                <div class="col-sm-6">
                    <div class="add-img">
                        <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>"> 
                    </div>
                </div>
                <?php } } ?>
                
            </div>  

            <div id="listing_block">
            <?php
            
                    if ($featureCategoryListings) {

                           foreach ($featureCategoryListings as $featureListing) {

                            $list_id  = base64_encode($featureListing['business_id']);

                            if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 
                                 $featured_name  = "FEATURED";
                               } else {
                             
                                 $featured_name  = "متميز";
                               }
                               $business_name  = $featureListing['business_title'];
                               $business_desc  = $featureListing['business_desc'];
                               $business_address  = $featureListing['business_address'];
                        ?>   
            <div class="featured-box listing-sub grid-system">
                <div class="image-lis-sub">                 
                    <span class="feat-ad"><?php echo $featured_name;?></span>
                    <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img src="<?php echo base_url('uploads/business/' . $featureListing['business_image1']); ?>"></a>
                </div>
                <div class="image-txt-sub">
                    <div class="img-txt-subhead">
                         <!--<div class="profile">
                            <span class="pro-img"><img src="images/profile.jpg"></span>
                           
                        </div>-->
                     
                        <h3><?php echo $business_name;?></h3>
                        <?php 
                        if($show)
                        {
                        ?>
                        <span class="rating-main">
                            <ul>
                                <?php
                                for ($i=0; $i < (int)$featureListing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star"></i></li><?php
                                }
                                for ($i=0; $i < 5-(int)$featureListing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star-o"></i></li><?php
                                }
                                ?>
                           
                            </ul>
                        </span>
                        <?php
                        }
                        ?>
                    </div>
                    <p class="add-l"><?php 
                                        
                                            echo !empty(text_wrap($business_desc,15)) ? text_wrap($listing['business_desc'],15) : "";?></p>
                    <div class="dsply-num">
                        <span class="add-new"><i class="fa fa-map-marker"></i><?php echo $business_address;?></span>
                      
                    </div>
                </div>
            </div>

            <?php }} ?>     

            <?php
                    if ($categoryListing) {

                           foreach ($categoryListing as $listing) {

                           
                            $list_id  = base64_encode($listing['business_id']);
                            if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 
                                 $featured_name1  = "FEATURED";
                               } else {
                           
                                 $featured_name1  = "متميز";
                               }
                               $business_name1  = $listing['business_title'];
                               $business_desc1  = $listing['business_desc'];
                               $business_address1  = $listing['business_address'];
                               $distance = $listing['distance'];
                               $is_featured = $listing['features_ads'];
                        ?>
            <div class="listing-sub grid-system">
                <div class="image-lis-sub">
                <?php
                if($is_featured == 1)
                {
                    //. $is_featured
                ?>
                <span class="feat-ad"><?php echo $featured_name1;?></span>
                <?php
                    }
                ?>
                    <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><img src="<?php echo base_url('uploads/business/' . $listing['business_image1']); ?>"></a>
                </div>
                <div class="image-txt-sub">
                    <div class="img-txt-subhead">
                        <!--<div class="profile">
                            <span class="pro-img"><img src="images/profile.jpg"></span>
                           
                        </div>-->
                        <h3><?php echo $business_name1;?></h3>
                        <?php 
                        if($show)
                        {
                        ?>
                        <span class="rating-main">
                            <ul>
                                <?php
                                for ($i=0; $i < (int)$listing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star"></i></li><?php
                                }
                                for ($i=0; $i < 5-(int)$listing['avg_rating']; $i++) { 
                                    ?><li><i class="fa fa-star-o"></i></li><?php
                                }
                                ?>
                      
                            </ul>
                        </span>
                        <?php
                        }
                        ?>
                    </div>
                    <p class="add-l"><?php 
                                        
                                            echo !empty(text_wrap($business_desc1,15)) ? text_wrap($business_desc1,15) : "";?> </p>
                    <div class="dsply-num">
                        <span class="add-new"><i class="fa fa-map-marker"></i> <?php echo $business_address1?></span>
                        <span class="add-new-distance"><i class="fa fa-map-marker"></i> <?php echo " Distance: ". $distance. " km." ?></span>
                      
                    </div>
                </div>
            </div>
            <?php

              } }
            ?>

            </a>

             <div class="read-more paginat-cus">
                <nav aria-label="Page navigation example" class="servic_pagi custom_pagination">
                    <?php echo $links ;  ?>
                </nav>
            </div>
        </div>

         

            <div class="main-add">
                <?php
                    if ($advertisements) {
                        foreach ($advertisements as $advertisement) {
                            ?>
                <div class="col-sm-6">
                    <div class="add-img">
                        <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>"> 
                    </div>
                </div>
                <?php } } ?>
                
            </div>
            </div>
        </div>
        <div class="map-main wow slideInRight animated">
            <div id="map" style="width: 100%; height: 100%;"></div>
        </div>
    </section>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>


<script>
     $(function(){   

        // document.getElementById("near_by").value = <?php echo $near_by; ?>;
     
        

     });


    var rangeSlider = function(){
 var slider = $('.range-slider'),
     range = $('.range-slider__range'),
     value = $('.range-slider__value');
  
 slider.each(function(){

   value.each(function(){
     var value = $(this).prev().attr('value');
     $(this).html(value);
   });

   range.on('input', function(){
     $(this).next(value).html(this.value);
  
   });
 });

};

rangeSlider();
</script>