<style type="text/css">
     #infobar {
      width: 500px;
      height: 200px;
      border: 1px solid #333;
      overflow-y: scroll;
    }
    .content {
      width: 100%;
    }
    .content div {
      color: blue;
      opacity: 0.5;
    }
    .content div span {
      color: #666;
    }
    .info {
      margin-top: 10px;
    }
    .example {
      background-color: #F8F8F8;
    }
    .submit {
      margin-top: 20px;
    }
    @media only screen and (max-width: 720px) {
      #mobile {
        padding: 20px 0;
      }
    }
</style>
<link rel="stylesheet" type="text/css" href="css/lightslider.css"/>
<link rel="stylesheet" href="css/asRange.css">

<div class="inner_head">
        <div class="bread_out">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="contact-head">
                            <div id="crumbs">
                                <a href="#">Home</a> / <span class="current">Temple</span>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-7">
                        <div class="single-data">
                            <div class="dis-ac"><span>Discount 10%</span></div>
                            <h2>Property Name</h2>
                            <div class="img-txt-subhead">
                                <span class="rating-main">
                                    <ul>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li><i class="fa fa-star"></i></li>
                                        <li>( 24 Review )</li>
                                    </ul>
                                </span>
                            </div>
                            <div class="con-details">
                                <ul>
                                    <li><i class="fa fa-map-marker"></i> 305 Scholes St</li>
                                    <li><i class="fa fa-phone"></i> 545-985-8727</li>
                                    <li><i class="fa fa-envelope-o"></i> listing@site-example.com</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="right-side-angle">
                            <ul>
                                <li><i class="fa fa-heart-o"></i> Save</li>
                                <li><button class="review-btn">Submit Review <i class="fa fa-angle-right"></i></button></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</article>

<div style="padding:50px 0;background: #fff;">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="single-details">
                        <div class="deteils-img">
                            <div class="demo">
                                <div class="item">            
                                    <div class="clearfix">
                                        <ul id="image-gallery" class="gallery list-unstyled cS-hidden">
                                            <li data-thumb="images/inner-banner.jpg">
                                                <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image1']); ?>" />
                                            </li>
                                            <li data-thumb="images/inner-banner.jpg">
                                                <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image2']); ?>" />
                                            </li>
                                            <li data-thumb="images/inner-banner.jpg">
                                                <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image3']); ?>" />
                                            </li>
                                            <li data-thumb="images/inner-banner.jpg">
                                                <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image3']); ?>" />
                                            </li>
                                            <li data-thumb="images/inner-banner.jpg">
                                                <img src="<?php echo base_url('uploads/business/' . $listingDetails[0]['business_image4']); ?>" />
                                            </li>
                                            <!-- <li data-thumb="images/inner-banner.jpg">
                                                <img src="images/inner-banner.jpg" />
                                            </li> -->
                                        </ul>
                                    </div>
                                </div>
                            </div>  
                        </div>                      

                        <div class="temp-details">
                            <div class="dmv-heading">
                                <h3>Description</h3>
                            </div>
                            <p><?php echo $listingDetails[0]['business_desc']; ?></p>

                                                    
                        </div>

                        <div class="offer-img">
                            <div id="demo">
                              <div id="add-slid" class="owl-carousel">
                                <div class="item"><img src="images/offer.png"></div>
                                <div class="item"><img src="images/offer.png"></div>
                                <div class="item"><img src="images/offer.png"></div>
                              </div>
                            </div>
                        </div>

                        <div class="temp-details review-box">
                            <div class="review-div">
                                <div class="main-review">
                                    <h2>24 Reviews</h2>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="pro-img-re">
                                            <img src="images/profile.jpg">
                                        </div>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="review-containt">
                                            <h2> Cole Harris <span>6 months ago</span></h2>
                                            <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>                                                     
                                                </ul>
                                            </span>

                                            <p>“There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, ”</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="temp-details review-box">
                            <div class="review-div">
                                <div class="main-review">
                                    <h2>Rate & Write Reviews</h2>
                                </div>
                                <form>
                                    <div class="row">                                   
                                        <div class="col-sm-12">
                                            <div class="review-containt">
                                                <span class="rating-main">
                                                    <ul>
                                                        <li>Your Rating </li>
                                                        <li> <i class="fa fa-star-o"></i></li>
                                                        <li><i class="fa fa-star-o"></i></li>
                                                        <li><i class="fa fa-star-o"></i></li>
                                                        <li><i class="fa fa-star-o"></i></li>
                                                        <li><i class="fa fa-star-o"></i></li>                                                       
                                                    </ul>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="revire-input">
                                                <input type="text" name="" placeholder="Your Name *">
                                            </div>
                                        </div>

                                        <div class="col-sm-6">
                                            <div class="revire-input">
                                                <input type="email" name="" placeholder="Email Address *">
                                            </div>
                                        </div>

                                        <div class="col-sm-12">
                                            <div class="revire-input">
                                                <textarea placeholder="Tell us your experience..."></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12 sub-btn">
                                            <input type="submit" name="" value="Submit Your Review">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="side-map">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14722.96212982255!2d75.92889325!3d22.700701!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1496980678600" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                    <div class="conta-details">
                        <div class="data-div">
                            <p><i class="fa fa-map-marker"></i>
                            400 E 34th St</p>
                            <a href="#">Get Direction</a>

                        </div>
                        <div class="data-div">
                            <p><i class="fa fa-phone"></i>
                            545-985-8727</p>
                        </div>
                        <div class="data-div">
                            <p><i class="fa fa-envelope-o"></i>
                            listing@site-example.com</p>
                        </div>
                        <div class="data-div">
                            <p><i class="fa fa-globe"></i>
                            https://yoursite.com/</p>
                        </div>
                        <div class="social">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                    </div>
        
                    <div class="right-search">
                        <div class="dmv-heading">
                            <h3>SEARCH PROPERTY</h3>
                        </div>
                        <div class="rigt-search">
                            <form>
                                <div class="input-serch">
                                    <input type="text" name="" placeholder="Type loaction...">
                                </div>
                                <div class="input-serch">
                                    <select>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                        <option>Select City</option>
                                    </select>
                                </div>

                                <div class="input-serch">
                                    <select>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                        <option>Property Type</option>
                                    </select>
                                </div>

                                <div class="rooms">
                                    <div class="col-sm-6">
                                        <div class="input-serch">
                                            <select>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                                <option>Bedrooms</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-serch">
                                            <select>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                                <option>Bathrooms</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="range_slide">
                                    <div class="range-example-2"></div>
                                </div>

                                <div class="search-btn">
                                    <button class="search">
                                        Search Now
                                        <span class="icon-s">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </button>
                                </div>
                            </form>  
                        </div>
                    </div>

                    <div class="featur-pro">
                        <div class="dmv-heading">
                            <h3>Related Advertising</h3>
                        </div>
                        <ul>
                            <li>
                                <img src="images/list-1.jpg">
                                <div class="fre-details">
                                    <span>Appartement</span>
                                    <h4>Court Bellflower, CA 90706</h4>
                                    <p>$3510.000</p>
                                </div>
                            </li>

                            <li>
                                <img src="images/list-2.jpg">
                                <div class="fre-details">
                                    <span>Appartement</span>
                                    <h4>Court Bellflower, CA 90706</h4>
                                    <p>$3510.000</p>
                                </div>
                            </li>                       

                            <li>
                                <img src="images/list-3.jpg">
                                <div class="fre-details">
                                    <span>Appartement</span>
                                    <h4>Court Bellflower, CA 90706</h4>
                                    <p>$3510.000</p>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </section>  
</div>

<script>
    jQuery(document).ready(function() {
        jQuery("#content-slider").lightSlider({
                   loop:true,
                   keyPress:true
                });
                jQuery('#image-gallery').lightSlider({
                    gallery:true,
                    item:1,
                    thumbItem:4,
                    slideMargin: 0,
                    speed:500,
                    auto:true,
                    loop:true,
                    responsive : [
                    {
                        breakpoint: 800,
                        settings: { // settings for width 480px to 800px
                        thumbItem: 4
                        }
                    },
                    {
                        breakpoint: 480,
                        settings: {  // settings for width 0px to 480px
                        thumbItem: 2
                    }
                }
            ],
            onSliderLoad: function() {
                jQuery('#image-gallery').removeClass('cS-hidden');
            }  
            });
        });
</script>
<script>
    $(document).ready(function() {
      $("#add-slid").owlCarousel({
        navigation : true,
        slideSpeed : 300,
        paginationSpeed : 400,
        singleItem : true
      });
    });
</script>
<script>
    $(document).ready(function() {
      var one = $(".range-example-2").asRange({
        range: true,
        limit: true,
        tip: {
          active: 'onMove'
        }
      });
      console.log(one.asRange('set', [30, 60]));
    });
    $(document).ready(function(){
        $(".log-op").click(function(){
            $(".login-drop").fadeToggle();
        });
    });
</script>
