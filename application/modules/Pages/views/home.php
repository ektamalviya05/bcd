<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
    $all = "All";
    $name = "Name";
    $what_are_you_looking_for = "What are you looking for?";

    $lets = "Let's";
    $explore = "Explore";

    $all_top_location = "All the top locations ?";
   } else {
    $all = "الكل";
    $name = "اسم";
    $what_are_you_looking_for = "عما تبحث؟";
    $lets = "دعونا";
    $explore = "يكتشف";
    $all_top_location = "جميع المواقع العليا؟";

   }
?>
<div class="main-content">
    <div class="box_explore wow fadeInDown animated">
        <div class="container">
            <div class="header-srch">
                <div class="main-srch-heading">
                   <?php //echo $top_content[0]['top_content']; ?>

                   <h1><?php echo $lets;?> <span><?php echo $explore;?></span></h1>
                    <p><?php echo $all_top_location;?></p>
                </div>
            </div>
            <div class="main-srch-sub">
                <div class="row">
                    <form id="searching_form" action="<?php echo base_url(); ?>listings" method="post">
                        <div class="col-sm-3 col-md-3">
                          <div class="form-control frm-brdr-no">
                            <input type="text" placeholder="<?php echo $name;?>" name="business_name">  
                          </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group form-sub form_first">
                                <!-- <input type="address" class="form-control add-form" placeholder="<?php echo $top_content[0]['what_are_loking_for_label']; ?>"> -->
                                 <input type="address" name="address" id="autocomplete" class="form-control add-form" placeholder="<?php echo $what_are_you_looking_for;?>" placeholder="Enter your address" onFocus="geolocate()">
                                 <label class="error" for="autocomplete"></label>
                            </div>
                        </div>
                        <div class="col-sm-3 col-md-3">
                            <div class="form-group form-sub">
                               <!--  <input type="category" class="form-control cat-form" placeholder="<?php echo $top_content[0]['all_regions']; ?>"> -->
                                <select class="form-control cat-form" name="category_id" id="category_id">
                                  <option value=""><?php echo $all;?></option>
                                                            <?php
                                                            if ($business_categories) 
                                                            {
                                                                foreach ($business_categories as $categories) 
                                                                {

                                                                  if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $category_name  = $categories['cat_name'];
                                 
                                                                  } else{
                                                                    $category_name  = $categories['cat_namea'];

                                                                  }

                                                                    ?>
                                                                    <option value="<?php echo $categories['cat_id']; ?>"><?php echo $category_name; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <button id="btnListingData" name="btnListingData" class="btn btn-default" type="Submit"><i class="fa fa-search"></i> <?php echo $top_content[0]['search_button']; ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <section class="our-work">
        <div class="details">
            <div class="container">
                <div class="row">

                     <?php
                    if ($advertisements) {
                        foreach ($advertisements as $advertisement) {
                            ?>
                    <div class="col-sm-6">
                        <div class="add-img">
                            <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>">	
                        </div>
                    </div>
                    <?php } } ?>
                   
                </div>
                <div class="row">
                    <?php
                    if ($business_categories) {
                        foreach ($business_categories as $categories) {
                               $this->db->select('*');
                               $this->db->where('cat_id', $categories['cat_id']);
                               $this->db->where('status',1);


                               //$this->db->where('cat_id', $categories['cat_id']);
                               $query = $this->db->get('bdc_business');
                               $listingCount = $query->num_rows(); 
                               $category_id  = base64_encode($categories['cat_id']);
                               $checksubcat = $categories['cat_id'];
                               if($checksubcat)
                               {
                                $this->db->select('*');
                                $this->db->from('otd_business_category');
                                $this->db->where(array("cat_status"=>1, "type"=>1,'cat_parent'=>$checksubcat));
                                $query = $this->db->get();
                                if($query->num_rows()>0)
                                {

                                  $havesubcate = 1;
                                      
                                }
                                else
                                {
                                    $havesubcate = 0;  
                                }

                               }
                               if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 $category_name  = $categories['cat_name'];
                                 $category_desc  = $categories['cat_description'];
                               } else {
                                 $category_name  = $categories['cat_namea'];
                                 $category_desc  = $categories['cat_descriptiona'];
                               }

                            if ($this->session->userdata('isUserLoggedIn')){
                                $url='addlisting';
                            }else{
                                $url='login';
                            }
                            ?>


                            <div class="col-sm-6 col-md-3">
                                <div class="main_cat wow slideInLeft animated"> 
                                  <?php 
                                  if($havesubcate==1)
                                  {
                                  ?>
                                  <a href="<?php echo base_url();?>subcategory/<?php echo $category_id; ?>">

                                  <?php
                                  }
                                  if($havesubcate==0)
                                  {
                                  ?>
                                  <a href="<?php echo base_url();?>catListings/<?php echo $category_id; ?>">

                                  <?php
                                  }
                                  ?>

                                   <!-- <span><?php echo $listingCount;?></span> --> 
                                   <img src="<?php echo base_url('assets/img/category/' . $categories['cat_img_path']); ?>">
                                        <h4><?php 
                                        
                                            echo $category_name;?>
                                     </h4>
                                        <p><?php
                                           echo  $category_desc;?>
                                                
                                            </p>
                                    </a> </div>
                            </div>
                            <?php
                        }
                    } else {
                        ?>
                        <div class="col-sm-6 col-md-3">
                            There have no category found in table.
                        </div>
<?php }
?>

                </div>


                <!--                <div class="more-guru">
                                    <button class="more-list">View more category</button>
                                </div>-->
            </div>
        </div>
    </section>
    <section class="box_howItWork">
        <div class="container">
            <div class="heading-section">
                <?php echo $top_content[0]['how_it_works']; ?>

            </div>
            <div class="row">
               <?php $i = 0; ?>
                <?php foreach($how_it_works as $how_it_work) { 

                         if($this->session->userdata('lang_id')){
                              if($this->session->userdata('lang_id')==20){
                                $title = $how_it_work['title_english'];
                                $desc = $how_it_work['desc_english'];
                              }else{
                                $title = $how_it_work['title_arabic'];
                                $desc = $how_it_work['desc_arabic'];
                              }
                         } else {
                              $title = $how_it_work['title_english'];
                              $desc = $how_it_work['desc_english'];
                         }

                        
                        //$title = $how_it_work['title_english'];
                    ?>

                <div class="col-sm-6 col-md-4">
                    <div class="main_cat"> <a href="javascript:;"> <span> 
                        <?php if($i==0){ ?>   
                     <img src="assets_new/images/icon_find.png">
                 <?php }else if($i==1){ ?>
                 <img src="assets_new/images/icon_choose.png">
                 <?php } else if($i==2){ ?>
                 <img src="assets_new/images/img_loc.png">
                 <?php } ?>

                    </span>
                            <h4><?php echo $title;?></h4>
                            <p><?php echo $desc;?> </p>
                        </a> </div>
                </div>
                <?php $i++; } ?>
               
                
            </div>
        </div>
    </section>
    <section class="box_ads">
        <div class="container">
            <div class="heading-section">
                <?php echo $top_content[0]['featured_add_heading']; ?>
            </div>
            <p class="sub_hed"><?php echo $top_content[0]['featured_add_sub_heading']; ?></p>
            <div class="row">
                <?php
                    if ($featureCategoryListings) {
                        foreach ($featureCategoryListings as $featureListing) {

                              $business_name  = $featureListing['business_title'];
                              $business_desc  = $featureListing['business_desc'];
                              $business_address  = $featureListing['business_address'];
                             if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 //$business_name  = $featureListing['business_title'];
                                 //$business_desc  = $featureListing['business_desc'];
                                 //$business_address  = $featureListing['business_address'];
                                 $featured_name  = "FEATURED";
                               } else {
                                 //$business_name  = $featureListing['business_title_arabic'];
                                 //$business_desc  = $featureListing['business_desc_arabic'];
                                 //$business_address  = $featureListing['business_address_arabic'];
                                 $featured_name  = "متميز";
                               }

                            $list_id  = base64_encode($featureListing['business_id']);
                ?>            
                <div class="rtin-main-cols col-md-3 col-sm-6 col-xs-12 wow slideInLeft animated">
                    <div class="rt-course-box">
                        <div class="rtin-thumbnail hvr-bounce-to-right"> <img src="<?php echo base_url('uploads/business/' . $featureListing['business_image1']); ?>" class="" alt=""> <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>" title=""><i class="fa fa-link" aria-hidden="true"></i></a>
                            <div class="rtin-price">
                                <div class="course-price"> <span class="price"><?php echo $featured_name;?></span></div>
                            </div>
                        </div>
                        <div class="rtin-content-wrap">
                            <div class="rtin-content">
                                <!--<div class="profile">
                            <span class="pro-img"><img src="images/profile.jpg"></span>
                           
                        </div> -->
                                <div class="rtin-author"><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>"><?php echo $business_name;?></a></div>
                                <h3 class="rtin-title"><a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>" title=""><?php 
                                        
                                            echo $business_desc;
                                            ?></a></h3>
                                <!-- <div class="btn_price"> <span class="btn_pr">$<?php echo $featureListing['business_price']?></span></div> -->
                                <div class="rtin-map"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $business_address?></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php }} ?>
               
            </div>
            <div class="row">
                <div class="col-sm-12 paginat-cus">
                    <nav aria-label="Page navigation example" class="servic_pagi custom_pagination">
                        <?php //print_r($search_results);
                        echo $links1;
                      ?>
                    </nav>
                </div>
            </div>
            <div class="row">
                
                 <?php
                    if ($advertisements) {
                        foreach ($advertisements as $advertisement) {
                            ?>
                    <div class="col-sm-6">
                        <div class="add-img">
                            <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>">   
                        </div>
                    </div>
                    <?php } } ?>
            </div>
        </div>
    </section>
</div>