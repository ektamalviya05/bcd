<style type="text/css">
  .category-section {
      padding:  40px 0;
  }
 /* .category-section input {
      height:  40px !important;
  }*/
  /*.category-section button.btn.btn-default {
      height: 40px;
  }*/
  .category-section input.form-control.cat-form {
      background-position: top 19px right 5px !important;
  } 
  .cat-head h2 {
    text-align:  center;
    font-size:  30px;
    margin: 0 0 20px;
    font-weight: 600;
  }
  .cate-new .main_cat a img {
    max-width: 80px;
  }
</style>
<div class="category-section">
  <div class="container">
    <div class="row">
        <?php
        if ($advertisements) {
            foreach ($advertisements as $advertisement) {
                ?>
        <div class="col-sm-6">
            <div class="add-img">
                <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>"> 
            </div>
        </div>
        <?php } } ?>
      <div class="col-sm-12 cat-head">
       <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 $category  = "Category";
                                 $search_by_location = "Search By Location";
                                 $search_by_category = "Serach By Category";
                                 //$category_desc  = $categories['cat_description'];
                               } else {
                                 $category  = "الفئة";
                                 $search_by_location = "البحث حسب الموقع";
                                 $search_by_category = "البحث حسب الفئة";
                                 //$category_desc  = $categories['cat_descriptiona'];
                               }
                               ?>
        <h2><?php echo $category;?></h2>
      </div>
    </div>
  </div>
    <!-- <div class="main-srch-sub">
                <div class="row">
                    <form id="searching_form" action="<?php echo base_url(); ?>listings" method="post">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group form-sub form_first">
                               
                                 <input type="address" name="address" id="autocomplete" class="form-control add-form" placeholder="What are you looking for?" placeholder="Enter your address" onFocus="geolocate()">
                                 <label class="error" for="autocomplete"></label>
                            </div>
                        </div>
                        <div class="col-sm-4 col-md-4">
                            <div class="form-group form-sub">
                               
                                <select class="form-control cat-form" name="category_id" id="category_id">
                                                            <?php
                                                            if ($business_categories) 
                                                            {
                                                                foreach ($business_categories as $categories) 
                                                                {

                                                                  if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $category_name  = $categories['cat_name'];
                                 
                                                                  } else{
                                                                    $category_name  = $categories['cat_namea'];

                                                                  }

                                                                    ?>
                                                                    <option value="<?php echo $categories['cat_id']; ?>"><?php echo $category_name; ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                            </div>
                        </div>
                        <div class="col-sm-2 col-md-2">
                            <button id="btnListingData" class="btn btn-default" type="Submit"><i class="fa fa-search"></i> <?php echo $top_content[0]['search_button']; ?></button>
                        </div>
                    </form>
                </div>
            </div> -->
</div>

<div class="main-content">
  <section class="our-work">
    <div class="details">
      <div class="container">    
        <div class="row">


            <?php
                    if ($business_categories) {
                        foreach ($business_categories as $categories) {
                               $this->db->select('*');
                               $this->db->where('cat_id', $categories['cat_id']);
                               $query = $this->db->get('bdc_business');
                               $listingCount = $query->num_rows(); 
                               $category_id  = base64_encode($categories['cat_id']);

                               $checksubcat = $categories['cat_id'];
                               if($checksubcat)
                               {
                                $this->db->select('*');
                                $this->db->from('otd_business_category');
                                $this->db->where(array("cat_status"=>1, "type"=>1,'cat_parent'=>$checksubcat));
                                $query = $this->db->get();
                                if($query->num_rows()>0)
                                {

                                  $havesubcate = 1;
                                      
                                }
                                else
                                {
                                    $havesubcate = 0;  
                                }
                              }

                               if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 $category_name  = $categories['cat_name'];
                                 //$category_desc  = $categories['cat_description'];
                               } else {
                                 $category_name  = $categories['cat_namea'];
                                 //$category_desc  = $categories['cat_descriptiona'];
                               }
                              
                            ?>
          <div class="col-sm-6 col-md-3">
            <div class="main_cat wow slideInLeft animated"> 
              <?php 
              if($havesubcate==1)
              {
              ?>
              <a href="<?php echo base_url();?>subcategory/<?php echo $category_id; ?>">

              <?php
              }
              if($havesubcate==0)
              {
              ?>
              <a href="<?php echo base_url();?>listings/<?php echo $category_id; ?>">

              <?php
              }
              ?>
              <!-- <span><?php echo $listingCount;?></span> -->
               <img src="<?php echo base_url('assets/img/category/' . $categories['cat_img_path']); ?>">
              <h4><?php 
                                        
                                            echo $category_name;?></h4>
              </a>
            </div>
          </div>
          <?php
           }}
          ?>
         
         <?php
        if ($advertisements) {
            foreach ($advertisements as $advertisement) {
                ?>
        <div class="col-sm-6">
            <div class="add-img">
                <img src="<?php echo base_url('assets/img/advertise/' . $advertisement['add_img_path']); ?>"> 
            </div>
        </div>
        <?php } } ?>
         
        
         
          
        
         
       
         
         
         
        
        
          
         
        
        </div>       
      </div>
    </div>
  </section>
</div>