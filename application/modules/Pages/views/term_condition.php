 
<style type="text/css">
  .about.inner_head {
    height: 200px;
  }
  .about .single-data {
    padding: 0 0 20px;
  }
  #crumbs {
    text-align: left;
  }
  .about .bread_out {
    padding: 50px 0 20px
  }
  .single-data p {
    letter-spacing:  0.6px;
    max-width:  560px;
    white-space:  nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 8px 0 0;
}
section.who_we {
    padding: 40px 0;
    background: #fff;
}
.about-who h3 {
    margin: 0 0 15px 0;
    padding-bottom: 15px;
    position: relative;
}
.about-who p {
    font-size: 13px;
    line-height: 24px;
    color: #777777;
}
.about-who ul {
    padding: 0;
}
.about-who ul li {
    list-style: none;
    position: relative;
    padding: 9px 0 9px 25px;
    margin: 0;
    font-size: 13px;
    color: #777777;
}
.about-who ul li:after {
    position: absolute;
    height: 12px;
    width: 12px;
    content: "\f00c";
    font-family: FontAwesome;
    left: 0;
    top: 13px;
    border-radius: 3px;
    color: #000;
    font-size: 10px;
    line-height: 8px;
}
.who_we .col-sm-6 {
    display: table-cell;
    float: none;
    vertical-align: middle;
}
.who-we-img img {
    width: 100%;
}
.about-who .heading-section {
    text-align:  left;
    margin: 0;
    padding:  0;
}
.about-who .heading-section h2 {
    margin: 0 0 10px;
}
.who-we-img {
    padding:  10px;
    background: #fff;
    box-shadow: 0 0 20px rgba(0, 0, 0, 0.28);
}
.about-who .heading-section:after, .about-who .heading-section:before {
    display:  none;
}
.c-logo {
    border: 1px solid rgba(0, 0, 0, 0.1);
    box-shadow: 0 1px 5px rgba(0, 0, 0, 0.05);
}
.patner-sec .item {
    padding:  10px;
}
.patner-sec .owl-controls {
    margin:  0;
    position:  absolute;
    width:  100%;
    top: 50%;
    height: 40px;
    margin-top: -20px;
}
.c-logo img {
    width: 100%;
}
.owl-carousel .owl-wrapper-outer {
    z-index: 888;
}
.patner-sec .owl-prev, .patner-sec .owl-next {
    top: 0;
    margin-top: 0 !important;
    font-size: 30px !important;
    z-index: 999;
}
.owl-buttons .owl-prev, .owl-buttons .owl-next {
    z-index: 999;
}
section.patner-sec {
    padding:  40px 0;
    background: rgba(241, 241, 241, 0.6);
}
section.testmoni-sec {
    padding:  40px 0;
}
.test-img img {
    width: 100%;
}
.test-contain {
    text-align:  center;
}
.review ul {
    padding:  0;
    margin:  0;
}
.review ul li {
    display:  inline-block;
    color: #be2d64;
}
.review {
    border-top: 1px solid #ccc;
    padding: 10px 0 0;
}
.test-moni {
    padding:  15px;
    box-shadow: 0 2px 14px rgba(0, 0, 0, 0.2);
    background: #fff;
}
.test-img {
    border-radius:  50%;
    overflow: hidden;
    height: 150px;
    width: 150px;
    margin:  0 auto;
}
.test-contain p {
    margin: 0 0 10px;
    font-size: 13px;
    font-style: italic;
    font-weight: 600;
    color: #696969;
    letter-spacing: 0.8px;
    line-height: 20px;
    overflow: hidden;
    text-overflow: ellipsis;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 4;
    min-height: 82px;
    max-height: 82px;
}
.team-sec .item {
    padding: 0 30px;
}
.p-img {
    padding: 10px;
    background:  #fff;
    border: 1px solid rgba(0, 0, 0, 0.1);
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.25);
    margin: 0 0 10px 0;
}
.p-img img {
    width:  100%;
}
.about-main h2,
section.team-sec h2,
.patner-sec h2,
.testmoni-sec h2 {
    margin-top: 0;
}
.overlay-team {
    background: rgba(0, 0, 0, 0.47);
    position:  absolute;
    width: 100%;
    bottom: 0;
    left: 0;
    right: 0;
    padding:  10px;
}
.overlay-team h3 {
    color: #fff;
    text-align: center;
    margin: 0 0 0;
    font-size: 22px;
}
.overlay-team h4 {
    text-align: center;
    color: #fff;
    margin: 5px 0 0;
    font-size: 13px;
}
.dev-data {
    position:  relative;
}
section.team-sec {
    padding: 40px 0;
    background: rgba(241, 241, 241, 0.6);
}
.about_contant p {
    font-size: 13px;
    line-height: 25px;
    letter-spacing: 1px;
    text-align: center;
}
section.about-main {
    padding: 50px 0;
    background: rgba(241, 241, 241, 0.6);
}
.about_contant {
    max-width: 800px;
    margin: 0 auto;
    text-align: center;
}
</style>

 <div class="about inner_head priv_sec">
    <div class="bread_out">
      <div class="container">
        <div class="row">
         <!--  <div class="col-sm-12">
            <div class="contact-head">
              <div id="crumbs">
                <a href="#">Home</a> / <span class="current">Temple</span>
              </div>
            </div>
          </div> -->
          <div class="col-sm-12">
            <div class="single-data">
              <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>  
              <h2><?php echo $similar_pages[0]['pg_title'];?></h2>
              <p><?php echo $similar_pages[0]['description'];?></p>
              <?php }else {?>
              <h2><?php echo $similar_pages[0]['pg_title_ar'];?></h2>
              <p><?php echo $similar_pages[0]['description_ar'];?></p>
               <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</article>

<div class="main-wrap">  
  <section class="who_we priv">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="about-who">
            <?php if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) { ?>
            <?php echo $similar_pages[0]['pg_content'];?>
            <?php }else {?>
            <?php echo $similar_pages[0]['pg_content_ar'];?>
             <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </section>


</div>