<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit City <a href="javascript:window.history.back()" class="btn btn-default btn-back">  <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>              
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($details)){                      
                      ?>
                      <form id="editcity" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                                            
                       
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>City Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="name" class="form-control" placeholder="City Name" value="<?php echo $details[0]['name']; ?>"/>
                            <?php echo form_error('name'); ?>
                          </div>
                        </div>                      
                       
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Country:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <select name="country" class="form-control">
                                <option value="">Select Country</option>
                                <?php
                                foreach ($country as $value) {
                                ?>
                                <option value="<?php echo $value['id']; ?>" <?php if($details[0]['country_id']== $value['id']){ echo "selected";} ?>><?php echo $value['name']; ?></option>
                                <?php
                                }
                                ?>
                                
                              </select>
                              <?php //echo form_error('name'); ?>
                            </div>
                      </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <input type="hidden" name="id" value="<?php echo $details[0]['id']; ?>">
                            <button type="submit" class="btn btn-success" id="addsubmit">Save</button>
                            <div id="addmsg"></div>
                          </div>
                        </div>
                      </form>
                      <?php
                    }
                    ?>                    
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->        