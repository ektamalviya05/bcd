
  <h2>Add Content</h2>
  <form class="form-horizontal" action="<?php echo base_url()?>Content/addcontent" method="post" enctype="multipart/form-data"> 

  

    <div class="form-group">
  <label for="sel1">Select Section:</label>
  <select class="form-control" id="section" name="section" required="">
  <option value="">Select Section</option>
    <!--<option>Popup</option>-->
    <!--<option>Home</option> -->
    <option>Business User Registration</option>
    <option>Personal User Registration</option>
 </select>


</div>
 
  <div class="form-group">
  <label for="sel1">Select Sub Section:</label>
  <select class="form-control" id="sub-section" name="sub-section" required="">
    <option value="">Select Sub Section</option>
    <option value="Read More">Read More</option>
    <option value="Terms and Condition">Terms and Condition</option>
    <option value="Newsletters">Image and Content</option>
   <!-- <option value="Newsletters">Newsletters Two</option> -->
  </select>

<input type="hidden" name="content_type" id="content_type" value="text">
</div>
  




<div class="form-group">
  <label for="sel1">Enter Content Text:</label>
  <textarea class="form-control" rows="5" id="content" name="content"></textarea>
</div>


<div class="form-group" id="fileloader" style="float:left;">
  <label for="sel1">Select Image:</label>

 <input type='file' id="userfile" name="userfile"/>
 

</div>

<!--<div style="float:left;">
<img id="blah" src="#" alt="your image" height="70" width="70" />
</div>
-->

<br><br>
<div class="form-group" style="float: right">
 <input type="submit" name="contentSubmit" value="Save Content" class="btn btn-primary form-control" />
</div>


  </form>



<script  type="text/javascript">

$("#sub-section").change(function() {
   
    if(this.value == "Read More")
    {
      $("#fileloader").hide();
       $("#content_type").val("text");
    }
    else
{

  if(this.value == "Terms and Condition")
  {
      $("#content_type").val("pdf"); 
  }
  else
  {
   $("#content_type").val("img"); 
  }

  $("#fileloader").show();
}

});



function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#userfile").change(function(){
    readURL(this);
});

</script>