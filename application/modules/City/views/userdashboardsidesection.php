<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>User Dashboard Mes Option Side Section</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($listing)){                      
                    ?>
                    <div class="x_title">
                      <h2> Left Side Paragraph <small></small></h2>                   
                      <div class="clearfix"></div>
                    </div>
                    <form id="updatedashboardarticle" autocomplete="off" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/contentUpdate/$page"); ?>' data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group">                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control" name="label3" id="label3"><?php echo $listing["label3"]; ?></textarea>
                          <?php echo form_error('label3'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" id="catsub" class="btn btn-success">Update Paragraph</button>                          
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>                          
                            <div id="catupmsg">
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php
                    }
                    ?>
                  </div>
                  <div class="x_title">
                    <h2> Card Image Listing <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div>
                  <div class="import-create dash-counter">                        
                    <div class="row">                        
                      <div class="col-md-12">
                        <div class="dc-sub">
                          <form method="post" enctype="multipart/form-data" action="" id="uploaddebit">
                            <div class="form-group">                                                                                  
                              <div class="col-md-3 col-sm-3 col-xs-3">
                                <input type="file" name="ban_file" class="form-control" />
                                <div id="uplmsg"></div>
                              </div>
                              <div class="col-md-3 col-sm-3 col-xs-3">                                
                                <input type="text" name="imglink" class="form-control" placeholder="image redirect url" />                                
                              </div>
                              <div class="col-md-3 col-sm-3 col-xs-3">                                
                                <input type="number" name="order" class="form-control" min="1" placeholder="Display Order" />                                
                              </div>
                              <div class="col-md-2 col-sm-2 col-xs-2">
                                <button type="submit" id="bansub" class="btn btn-success">Upload</button>
                                <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span> 
                              </div>                        
                            </div>
                            <br>
                          </form>
                        </div>
                      </div>                      
                    </div>
                    <div class="clearfix"></div>
                    <br>
                    <div class="row">
                      <div class="user-table table-responsive">
                        <table class="table table-striped table-bordered">
                          <tbody>                              
                            <tr >
                              <th>No.</th>
                              <th>Image</th>                                
                              <th>Status</th>                                
                              <th>Display Order</th>
                              <th>Action</th>
                            </tr>
                            <?php
                            $count = 1;
                            if(!empty($debitimages)){
                              foreach($debitimages as $testi){
                              ?>
                              <tr>
                                <td><?php echo $count; ?></td>
                                <td>
                                  <?php $path = $testi['dbt_image_path']; ?>
                                  <img src="<?php echo base_url("assets/img/debitcardimg/$path"); ?>" height="100px" width="150px" />  
                                </td>  
                                <td>
                                  <input type="checkbox" class="debitcard_status" modal-aria="<?php echo $testi['dbt_id']; ?>" <?php if($testi['dbt_status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">                                                          
                                </td>                                
                                <td><?php echo $testi['dbt_display_order']; ?></td>
                                <td>
                                  <div class="link-del-view">
                                      <?php $tstid = $testi['dbt_id']; ?>
                                      <div class="tooltip-2"><a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="delet-debitcard" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <span class="tooltiptext">Delete</span>
                                      </div>                                      
                                  </div>
                                </td>
                              </tr>
                              <?php
                                $count++;
                              }
                            }
                            ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->
        <script type="text/javascript">
          CKEDITOR.replace( 'label3', {
            language: 'en',
            uiColor: '#9AB8F3',
            allowedContent:true,
          });
        </script>