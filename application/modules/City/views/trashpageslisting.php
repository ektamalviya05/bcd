<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
                    <div class="import-create dash-counter">
                        <!-- <h2>Create</h2> -->
                        <div class="row">
                          <div class="col-md-3">
                              <div class="dc-sub">
                                  <a href="<?php echo base_url("Content/pagesList"); ?>">
                                      <div class="dc-sub-ico">
                                          <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                      </div>
                                      <div class="dc-sub-txt pu-txt">
                                          <h3>View</h3>
                                          <p>Page Listing</p>
                                      </div>
                                  </a>
                              </div>
                          </div>                            
                        </div>
                    </div>


                  <!--  Main Containt  -->
                    <div class="dash-counter users-main">
                        <h2>Trash Page List</h2>
                        <div class="user-table table-responsive">
                          <table class="table table-striped table-bordered">
                            <tbody>
                              <tr></tr>
                              <tr>
                                <th>No.</th>
                                <th>Title</th>
                                <th>Meta Tag</th>                                
                                <th>Category</th>                                
                                <th>Date Created</th>
                                <th>Action</th>
                              </tr>
                              <?php
                              $count = 1;
                              foreach($details as $pages){
                              ?>
                              <tr>
                                <td><?php echo $count; ?></td>
                                <td><?php echo $pages['pg_title'] ?></td>
                                <td><?php echo $pages['pg_meta_tag']; ?></td>                                
                                <td><?php 
                                if($pages['pg_cat'] == 1)
                                  echo "Information";
                                else if($pages['pg_cat'] == 2)
                                  echo "Discover";
                                else if($pages['pg_cat'] == 3)
                                  echo "Professional Accounts";
                                else
                                  echo "None";                                 
                                ?></td>                                
                                <td><?php echo $pages['pg_date_created']; ?></td>
                                <td>
                                  <div class="link-del-view">
                                      <?php $pg_id = $pages['pg_id']; ?>                                      
                                      <div class="tooltip-2"><a href="javascript:;" class="trashback-page" modal-aria="<?php echo $pg_id; ?>"><i class="fa fa-reply" aria-hidden="true"></i></a>
                                        <span class="tooltiptext">Restore</span>
                                      </div>
                                      <div class="tooltip-2"><a href="javascript:;" class="trashback-publishpage" modal-aria="<?php echo $pg_id; ?>"><i class="fa fa-window-restore" aria-hidden="true"></i></a>
                                        <span class="tooltiptext">Restore & Publish</span>
                                      </div>
                                      <!-- <div class="tooltip-2"><a href="javascript:;" class="delete-page" modal-aria="<?php echo $pg_id; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                        <span class="tooltiptext">Delete Permanently</span>
                                      </div> -->                                      
                                  </div>
                                </td>
                              </tr>
                              <?php
                                $count++;
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>
                        <!-- <div class="read-more">
                          <a href="<?php echo base_url().'Admin/user_list'; ?>">
                              View more
                          </a>
                        </div> -->
                    </div>                    
                </div>




















<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Deleted");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDel').modal();
}  
</script>

  <?php
}
?>

<?php
if(isset($activated) && trim($activated) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Activated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaAct').modal();
}  
</script>

  <?php
}
else if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
 //alert( "Deactivated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>

  <?php
}
?>

