<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add New Page</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    foreach($details as $page){}
                      $pg_id = $page['pg_id'];
                    ?>
                    <form id="addpage" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/editPage/$pg_id"); ?>' data-parsley-validate class="form-horizontal form-label-left">                                            
                      
                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Title in English:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" name="title" class="form-control" placeholder="Page Title" value="<?php echo $page['pg_title']; ?>"/>
                          <?php echo form_error('title'); ?>
                        </div>
                      </div>
                       <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Title in Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" name="title_ar" class="form-control" placeholder="Page Title" value="<?php echo $page['pg_title_ar']; ?>"/>
                          <?php echo form_error('title_ar'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Description in English:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                         <textarea class="form-control summernote" name="description" id="description"><?php echo $page['description']; ?></textarea>
                          <?php echo form_error('description'); ?>
                        </div>
                      </div>
                       <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Description in Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                         <textarea class="form-control summernote" name="description_ar" id="description_ar"><?php echo $page['description_ar']; ?></textarea>
                          <?php echo form_error('description_ar'); ?>
                        </div>
                      </div>

                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Page URL:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" name="page_url" class="form-control" placeholder="Page Url" value="<?php echo $page['page_url']; ?>"/>
                          <?php echo form_error('page_url'); ?>
                        </div>
                      </div> -->
                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Meta Title:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" name="meta_title" class="form-control" placeholder="Meta Title" value="<?php echo $page['meta_title']; ?>"/>
                          <?php echo form_error('meta_title'); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Meta Description:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <!-- <input type="text" name="meta_description" class="form-control" placeholder="Meta Description" value="<?php echo $page['meta_description']; ?>"/> -->
                          <textarea class="form-control summernote" name="meta_description" id="meta_description"><?php echo $page['meta_description']; ?></textarea>
                          <?php echo form_error('meta_description'); ?>
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Meta Keywords:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" name="meta_keyword" class="form-control" placeholder="Meta Keywords" value="<?php echo $page['meta_keyword']; ?>"/>
                          <?php echo form_error('meta_keyword'); ?>
                        </div>
                      </div>






                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Content in english:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template"><?php echo $page['pg_content']; ?></textarea>
                          <?php echo form_error('template'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Content in arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template1" id="template1"><?php echo $page['pg_content_ar']; ?></textarea>
                          <?php echo form_error('template1'); ?>
                        </div>
                      </div>
                      <!-- <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Footer Section:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select class="form-control" name="category">
                            <option value="">Footer Section</option>
                            <option value="1" <?php if($page['pg_cat'] == 1) echo "selected"; ?>>Information</option>
                            <option value="2" <?php if($page['pg_cat'] == 2) echo "selected"; ?>>Discover</option>
                            <option value="3" <?php if($page['pg_cat'] == 3) echo "selected"; ?>>Professional Accounts</option>
                            <option value="0" <?php if($page['pg_cat'] == 0) echo "selected"; ?>>None</option>
                          </select>
                          <?php echo form_error('category'); ?>
                        </div>
                      </div> -->
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Display Order:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="number" name="display_order" min="1" value="<?php echo $page['pg_display_number'] ?>" class="form-control" placeholder="Display Order"/>
                          <?php echo form_error('status'); ?>
                        </div>
                      </div> -->
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Status:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select class="form-control" name="status">
                            <option value="">Page Status</option>
                            <option value="1" <?php if($page['pg_status'] == 1) echo "selected"; ?>>Publish</option>
                            <option value="0" <?php if($page['pg_status'] == 0) echo "selected"; ?>>Unpublish</option>
                          </select>
                          <?php echo form_error('status'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" class="btn btn-success">Save</button>                          
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->
        <script type="text/javascript">
        	CKEDITOR.replace( 'template', {
			    language: 'en',
			    uiColor: '#9AB8F3',
			    allowedContent:true,
			});
        </script>
        <script type="text/javascript">
          CKEDITOR.replace( 'template1', {
          language: 'en',
          uiColor: '#9AB8F3',
          allowedContent:true,
      });
        </script>