<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content_model extends CI_Model{
    function __construct() {
        $this->contTbl = 'site_contents';
        $this->contTbl1 = 'otd_page_contents';
        $this->socialTbl = "otd_social_links";
        $this->addTbl = "cities";
        $this->ftTbl = "otd_footer_content";
        $this->dbtTbl = "otd_debitcard_images";
    }

    
    /*
     * get rows from the Site Content table
     */
    function getRows($params = array()){

      $this->db->select('*');
        $this->db->from($this->contTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        //echo "<br>".$this->db->last_query()."<br>";
        return $result;
    }

    /*
     * Insert user information
     */
    public function insert($data = array()) {

        //insert user data to users table
        $insert = $this->db->insert($this->contTbl, $data);
        
        //return the status

        if($insert){
          //return $this->db->insert_id();
            return true;
        }else{
            return false;
        }
    }

    /*
    * Update Articals
    */

    function editArticles($id, $content){
        if(!empty($id)){
            $this->db->update($this->contTbl, $content, $id);
            if($this->db->affected_rows() > 0){
                $this->session->set_flashdata("success", "<label class='text-success'>Content updated successfully.</label>");
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    /*
    * get rows from the page table
    */
    function getPageRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->contTbl1);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert page information
    */
    public function insertPage($data = array()) {

        //insert user data to users table
        $insert = $this->db->insert($this->contTbl1, $data);
        
        //return the status

        if($insert){
          //return $this->db->insert_id();
            return true;
        }else{
            return false;
        }
    }

    /*
    * Update page content
    */

    function editPage($id, $content){        

        if(!empty($id)){
            $this->db->update($this->contTbl1, $content, $id);
            $query = $this->db->get_where($this->contTbl1, $id);            
            if($this->db->affected_rows() > 0){                
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    /*
     * get rows from the Social links table
     */
    function getSocialRows($params = array()){

      $this->db->select('*');
        $this->db->from($this->socialTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("sc_id",$params)){
            $this->db->where('sc_id',$params['sc_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        //echo "<br>".$this->db->last_query()."<br>";
        return $result;
    }

    function updateSocialLinks($params = array(), $where = array()){
        $this->db->update($this->socialTbl, $params, $where);
        if($this->db->affected_rows() > 0 ){
            return true;
        }
        else{
            return false;
        }
    }

    function getDistanceLimit()
    {
$this->db->select("*");
$this->db->from("otd_options");

       $this->db->where("option_name","distance");
        $query = $this->db->get();
            
        $result = $query->row_array();

        return $result;
    }

    /*
    * get rows from the advertisement table
    */

    public function record_count() {
        $this->db->select('*');
        $this->db->from($this->addTbl);
        $this->db->where('add_status!=','3');
        $query = $this->db->get();
        $result  = $query->num_rows();
        return $result;
    
    }

    function getAddRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->addTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("add_id",$params)){
            $this->db->where('add_id',$params['add_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * fetch country
    */

     function countryfetch(){
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('add_status!=',3); 
        $query = $this->db->get();
        $result= $query->result_array();
        //echo $this->db->last_query();
        //die();
        return $result;
    }


    /*
    * Insert / Update into advertisement table
    */

    function insertAddTable($opr = 1, $para = array(), $where = array()){
        if($opr == 1){ // insert testimonial
            $this->db->insert($this->addTbl, $para);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ // update existing testimonial
            $this->db->update($this->addTbl, $para, $where);
            return true;
        }
    }

/*
* Get footer content and update content start
*/
    function getContentRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->ftTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("ft_id",$params)){
            $this->db->where('ft_id',$params['ft_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function contentUpdate($para = array(), $where = array()){
        $this->db->update($this->ftTbl, $para, $where);
        $this->session->set_flashdata("success", "<label class='text-success'>Footer content updated successfully.</label>");
        return true;
    }
/*
* Get footer content and update content start Ends
*/
    /*
     * get rows from the Site Content table
     */
    function getDebitCartRows($params = array()){

      $this->db->select('*');
        $this->db->from($this->dbtTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("dbt_id",$params)){
            $this->db->where('dbt_id',$params['dbt_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        //echo "<br>".$this->db->last_query()."<br>";
        return $result;
    }


    /*
    * Insert / Update into debitcard table
    */

    function insertDebitCardTable($opr = 1, $para = array(), $where = array()){
        if($opr == 1){ // insert testimonial
            $this->db->insert($this->dbtTbl, $para);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ // update existing testimonial
            $this->db->update($this->dbtTbl, $para, $where);
            return true;
        }
    }

    /*
    * Delete Debit Card Rows
    */
    function removeDebitCard($where = array()){
        $this->db->delete($this->dbtTbl, $where);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

}