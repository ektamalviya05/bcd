<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class CartModel extends CI_Model{
    function __construct() {
        $this->userTbl = 'users';
        $this->userTblSubs = 'subscribers';
        $this->businessTbl = 'otd_business_details';
        $this->priceTbl = "otd_business_pricetable";
        $this->bsopenTbl = "otd_business_openingtime";
        $this->photogal = "otd_user_photogallery";
        $this->boxIdea = "otd_idea_box";
        $this->followTbl = "otd_business_follower";
        $this->langTbl = "otd_languages";
        $this->userbscatTbl = "otd_user_business_category";
        $this->msgTbl = "otd_user_messaging";
        $this->revewTbl = "profile_review";
        $this->notiTbl = "otd_notifications";
        $this->eventTbl = "otd_events";
        $this->reportedRevewTbl = "reported_profile_review";
        $this->phTbl = "otd_user_phone";
        $this->labelTbl = "otd_label_suggestion_list";
        $this->ftTbl = "otd_footer_content";
        $this->pytTbl = "otd_user_option_payment";
        $this->slmrefTbl = "otd_user_slimpayreference";
        $this->usoptMaster = "otd_option_master";
        $this->usoptprice = "otd_option_price";
        $this->usOptTbl = "otd_user_option";
        $this->eventTypeTbl = "otd_event_type";
        $this->emailTpTbl = "otd_email_templates";
        $this->pkgTbl = "otd_package_options";
    }

    public function getUserRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->userTbl);
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    public function updateTable($table="", $data = array(), $where = array()) {        
        $this->db->where($where);                        
        $x = $this->db->update($table, $data);
        //echo  $this->db->last_query();
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;        
    }

    function getUserMandate($where = array()){
        return $query = $this->db->get_where($this->slmrefTbl, $where)->row_array();
    }

    function get_single_options_pricebytype($where = array())
    {
        return $this->db->get_where("otd_option_price", $where)->row_array();
    }

    function get_MasterOptionDetails($args = 1){
        return $this->db->get_where($this->usoptMaster, array("opt_id"=>$args))->row_array();
    }

    function get_single_options_price($opt_id = 0){
        if($opt_id == "")
           { $opt_id = 0;}

            $sql = "SELECT * from otd_option_price WHERE opt_price_id = ". $opt_id;

           
            $query = $this->db->query($sql);
            $REC = $query->num_rows();

            if($query->num_rows()>0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
    }

    public function insertEventwithougNoti($params = array()){
        $this->db->insert($this->eventTbl, $params);
        if($this->db->affected_rows() > 0){            
            return $this->db->insert_id();

        }else{
            return false;
        }
    }

    /*
    * Insert Business events 
    */
    public function insertEvents($params = array()){
        $this->db->insert($this->eventTbl, $params);
        if($this->db->affected_rows() > 0){            
            $id = $this->db->insert_id();
            $ses = $this->session->userdata("user_id");
            $query = $this->db->select("user_id")
                              ->join($this->userTbl, "$this->userTbl.user_id = $this->followTbl.follower_user_id")
                              ->get_where($this->followTbl,array('followed_user_id'=>$ses));
            foreach($query->result() as $userid){
                $this->db->insert($this->notiTbl, array("nt_by"=>$ses, "nt_to"=>$userid->user_id, "nt_type"=>2, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s"), "nt_rlt_id"=>$id));
            }
            return $id;
        }else{
            return false;
        }
    }

    function insertUserOption($params = array())
    {
        $this->db->insert("otd_user_option", $params);
     
        $insertid =  $this->db->insert_id();
        if( $insertid > 0){
            return $insertid;
        }
        else{
           
            return false;
        }
    }

    function insertTransaction($params = array()){
        $this->db->insert($this->pytTbl, $params);
        return $this->db->insert_id();
    }

    /*
    * Get user own Events
    */
    public function getMyEvents(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            return $this->db->order_by("created_on", "DESC")->get_where($this->eventTbl, array("created_by_user"=>$ses))->result_array();
        }else{
            return false;
        }
    }

    /*
    * Get Free Event
    */
    public function getMyFreeEvents(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            return $this->db->order_by("created_on", "DESC")->get_where($this->eventTbl, array("created_by_user"=>$ses, "event_is_free"=>1, "event_status !="=>3))->result_array();
        }else{
            return false;
        }
    }

    /*
    * get rows for business opening time table
    */
    function getEmailTemplateRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->emailTpTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("tmplate_id",$params)){
            $this->db->where('tmplate_id',$params['tmplate_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get packages option list
    */
    function getPackageOptionRows($params = array()){
        $this->db->select("*");        
        $this->db->from($this->pkgTbl);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = pkg_inc_optid");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get rows from text content table
    */
    function getTextContent($args){
        if(!empty($args)){
            return $this->db->get_where($this->ftTbl, array("ft_id"=>$args))->row_array();
        }else{
            return false;
        }
    }

    /*
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
        $address = str_replace(" ", "+", $address);
        $region = "fr";
        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=AIzaSyCFV0MgMCdDw4wuDxgS7_ymejhyA7d2h7A&address=$address&sensor=false&region=$region");
        $json = json_decode($json);              
        if(!empty($json->results)){
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};            
            $this->session->set_userdata("emptyaddress", 0);
        }else{
            $emptyaddress = $this->session->userdata("emptyaddress");
            $emptyaddress++;
            if($emptyaddress < 3){
                $this->session->set_userdata("emptyaddress",$emptyaddress);
                $this->get_lat_long($address);
            }else{
                $lat="48.8566140";
                $long = "2.3522220";                
            }
        }
        $response = array('lat'=>$lat,'lng'=>$long);
        return $response;
    }

    //functions for slimpay coding start
        /*
        * retrive user and order detials by 34 digits slimpay mandate creation id reference
        */
        function getSlimpayTransactionDetails($reference){
            $this->db->select("*");
            $this->db->from($this->pytTbl);
            $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_tran_id = $this->pytTbl.pyt_id");
            $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
            $this->db->where("py_slmref_id", $reference);
            return $this->db->get()->row_array();
        }

        function getSlimpayUserDetails($reference){
            $this->db->select("*");
            $this->db->from($this->pytTbl);
            $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_tran_id = $this->pytTbl.pyt_id");
            $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
            $this->db->where("pyt_id", $reference);
            return $this->db->get()->row_array();
        }

        function getOptionDetails($args){
            return $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usOptTbl.opt_option_id")
                            ->get_where($this->usOptTbl, array("opt_tran_id"=>$args))->result_array();
        }

        function getMasterOption($args = 1){
            return $this->db->get_where($this->usoptMaster, array("opt_id"=>$args))->row_array();
        }

        function updateUserOption($params = array(), $where=array()){
            $this->db->update($this->usOptTbl, $params, $where);
            return true;
        }
    //Functions form slimpay coding end
}