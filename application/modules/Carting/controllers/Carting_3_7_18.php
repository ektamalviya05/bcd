<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Carting extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){	 

	 	parent:: __construct();	 	
		/* Load the libraries and helpers */

		$this->load->library('form_validation','session');		
		$this->load->library("cart");
        $this->load->model('CartModel');
        if(empty($this->session->userdata("user_id")))
        	redirect(base_url());
    }

    public function index(){    	
    	// $this->cart->destroy();
    	$data = array();    	
    	header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'User Purchase Details');
        $this->template->load('home_layout', 'contents' , 'carting', $data);
    }

    function addOptionToCart(){
        if(!empty($this->session->userdata("user_id"))){
            $opt_user_id = $this->session->userdata('user_id');
            $opt_option_id = $this->input->post('opt_id');            
            $startfrom = $this->input->post("perdaysdate");
            $endfrom = $this->input->post("perdayedate");
            $opt_option_purchase_date = date("Y-m-d", time());            
            $optid = $this->input->post('price_type');
            $package = $this->CartModel->get_single_options_price($optid);
            $masterOption = $this->CartModel->get_MasterOptionDetails($opt_option_id);
            $pagetext = $this->CartModel->getTextContent(30);
            $cartText = $this->CartModel->getTextContent(15);
            $opt_option_status = 0;
            $otp_option_duration = $package['opt_price_type'];
            $opt_option_price = $package['opt_price'];
            $opt_option_pricefor7 = $package['opt_price'];
            $otp_option_qnty = $package['opt_qnty'];
            $option_mastername = $masterOption['opt_name'];
            $option_type = $masterOption['opt_type'];
            $opt_option_active_date = null;
            $opt_option_end_date = null;
            $opt_option_inactive_validity=null;
            $otp_search_city = "";
            $opt_city_lat = "";
            $opt_city_long = "";
            if($otp_option_duration == "1 Day"){
                if($opt_option_id == 2){
                    $otp_search_city = $this->input->post("city");
                    $latlong = $this->CartModel->get_lat_long($this->input->post("city"));
                    $opt_city_lat = $latlong['lat'];
                    $opt_city_long = $latlong['lng'];
                }
                $opt_option_active_date = $startfrom;
                $opt_option_end_date = $endfrom;
                $date1 = date_create($opt_option_active_date);
                $date2 = date_create($opt_option_end_date);        
                $difference = date_diff($date1,$date2);
                $diff = $difference->format('%a');
                if($diff > 0 ){
                    $opt_option_price = $diff*$opt_option_price;
                    $cart_duration = $diff." jours";
                    $otp_option_durationtext = "<p><strong>Début de publication: </strong>".$opt_option_active_date."</p>
                    							<p><strong>Fin de publication: </strong>".$opt_option_end_date."</p>
                    							<p><strong>Nombre de jours: </strong>".$diff." jours</p>
                    							<p><strong>Prix: </strong>".$pagetext['label6'].$this->cart->format_number($package['opt_price'])." Par jour";
                }else{
                	$opt_option_price = $opt_option_price;
                	$cart_duration = "1 jour";
                	$otp_option_durationtext = "<p><strong>Début de publication: </strong>".$opt_option_active_date."</p>
                    							<p><strong>Fin de publication: </strong>".$opt_option_end_date."</p>
                    							<p><strong>Nombre de jours: </strong>1 jour</p>
                    							<p><strong>Prix: </strong>".$pagetext['label6'].$this->cart->format_number($package['opt_price'])." Par jour";
                }

            }else{
            	$cart_duration = $otp_option_duration;
            	$otp_option_durationtext = $otp_option_duration." à ".$pagetext['label6'].$this->cart->format_number($package['opt_price']);
            }

            $optionbuy = array("opt_user_id" => $opt_user_id,
                               "opt_option_id" => $opt_option_id,
                               "opt_option_type" => $option_type,
                               "opt_option_purchase_date" => $opt_option_purchase_date,
                               "otp_option_duration" => $otp_option_duration,
                               "otp_option_qnty"=>$otp_option_qnty,
                               "opt_option_price" => $opt_option_price,
                               "otp_search_city"=>$otp_search_city,
                               "opt_city_lat"=>$opt_city_lat,
                               "opt_city_long"=>$opt_city_long,
                               "opt_option_active_date" => $opt_option_active_date,
                               "opt_option_end_date"=>$opt_option_end_date,
                               "opt_option_inactive_validity"=> $opt_option_inactive_validity,
                               "opt_option_status"=>$opt_option_status);
            
            if($opt_option_id == 7 || $opt_option_id == 8){
                $eventDetails = array("event_status"=>0,
		                              "event_is_free"=>0,
		                              "created_by_user"=>$opt_user_id
		                             );
                if($otp_option_duration == "1 Day"){
                	$eventDetails['event_publish_start'] = $opt_option_active_date;
                    $eventDetails['event_publish_end'] = $opt_option_end_date;
                }
                if($opt_option_id == 8)
                    $eventDetails['event_goodplace'] = 1;
            }
            if($opt_option_id == 10){
            	$option_10text = $package['opt_text'];
            }
            if($cartcnt = $this->cart->total_items() > 0){
				foreach($this->cart->contents() as $items){
					if($items['id'] == "sku_".$opt_option_id){
						$valid = 1;
						break;
					}else{
						$valid = 0;
					}
				}
			}else{
				$valid = 0;
			}
			if($valid == 0){
	            $data = array(
	            			'id'=> "sku_".$opt_option_id,
					        'qty'=> 1,
					        'price'=>$opt_option_price,
					        'details'=> $otp_option_durationtext,
					        'duration'=> $cart_duration,
					        'name'=>$option_mastername,					        
					        'options'=> $optionbuy,					        
							);
	            if($opt_option_id == 7 || $opt_option_id == 8){
                	$data['eventDetails'] = $eventDetails;
                }
                if($opt_option_id == 10){
                	$data['purchased_option'] = $option_10text;
                }
	          
	            if($this->cart->insert($data)){
		            $totalproducts = $this->cart->total_items();
		            echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Ajouté au panier.</label>", "btntext"=>$cartText['label79'], "count"=>$totalproducts, "type"=>1));
		        }else{
		        	echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! Veuillez réessayer</label>", "type"=>0));
		        }
	        }else{
	        	echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Cette option fqit déjà parti de votre panier.</label>", "btntext"=>$cartText['label79'], "type"=>2));
	        }
        }
        else
        {
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé", "type"=>0));
        }
    }

    /*
    * Select payment method page
    */
    function completePaymentAction(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            if($this->cart->total_items() > 0){
                $data = array();
                $data['userdetails'] = $this->CartModel->getUserRows(array("user_id"=>$ses));
                $data["texts"] = $this->CartModel->getTextContent(31);
                $data["userinfoconfirm"] = $this->CartModel->getTextContent(32);
                $data["usermandateexists"] = $this->CartModel->getTextContent(33);
                $data["transfailed"] = $this->CartModel->getTextContent(35);
                header("Access-Control-Allow-Origin: *");
                $this->template->set('title', 'Select Payment Method');
                $this->template->load('home_layout', 'contents' , 'selectpaymentmethod', $data);
            }else{
                $this->session->set_flashdata("error", "<label class='text-danger'>panier vide</label>");
                //die("HEreeee");
                redirect(base_url("Users/account/messoptions"));
            }
        }else{
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    /*
    * Select paypal payment action
    */
    function paypalInteraction(){
        if(!empty($this->session->userdata("user_id"))){
            if($this->cart->total_items() > 0){             
                $opt_user_id = $this->session->userdata('user_id');
                if($this->cart->total() == 0 && $this->cart->total_items() == 1){                   
                    $arr1 = array("pyt_txn_status"=>"Completed",
                                  "pyt_amount"=>0,
                                  "pyt_fee"=>0,
                                  "pyt_date"=>date("Y-m-d H:i:s"));
                    $check = $this->CartModel->insertTransaction($arr1);
                    foreach($this->cart->contents() as $items){
                        $arr =  $items['options'];
                        $arr['opt_tran_id'] = $check;
                        if($items['options']['opt_option_id'] == 7){                            
                            $arr['opt_event_id'] = $this->CartModel->insertEvents($items['eventDetails']);
                        }
                    }
                    $y = $this->CartModel->insertUserOption($arr);

                    $this->cart->destroy();
                    $this->session->set_flashdata("error", "<label class='text-success'>Event is created successfully.</label>");
                    redirect(base_url("Users/account/messoptions"));
                }else{
                    $arr1 = array("pyt_txn_status"=>"Pending");
                    $check = $this->CartModel->insertTransaction($arr1);
                    foreach($this->cart->contents() as $items){
                        $arr =  $items['options'];
                        $arr['opt_tran_id'] = $check;                        
                        if($items['options']['opt_option_id'] == 7 || $items['options']['opt_option_id'] == 8){
                            if($items['options']['opt_option_price'] == 0 && $items['options']['opt_option_id'] == 7){
                                $arr['opt_event_id'] = $this->CartModel->insertEvents($items['eventDetails']);
                            }else{
                                $arr['opt_event_id'] = $this->CartModel->insertEventwithougNoti($items['eventDetails']);
                            }
                        }
                        $y = $this->CartModel->insertUserOption($arr);                        
                    }
                    if($check)
                    {
                        $totalprice = $this->cart->total();
                        $this->cart->destroy();                 
                        //Paypal Code Integration start
                            $this->load->library("paypal_lib");
                            //Set variables for paypal form
                            $returnURL = base_url().'Users/completePayment'; //payment success url
                            $cancelURL = base_url().'Users/cancelPayment'; //payment cancel url
                            $notifyURL = base_url().'Pages/paymentIpn'; //ipn url
                            //get particular product data                    
                            $this->paypal_lib->add_field('return', $returnURL);
                            $this->paypal_lib->add_field('cancel_return', $cancelURL);
                            $this->paypal_lib->add_field('notify_url', $notifyURL);
                            $this->paypal_lib->add_field('item_name', "YellowPages");
                            $this->paypal_lib->add_field('custom', $check);
                            $this->paypal_lib->add_field('item_number',  "1");
                            $this->paypal_lib->add_field('amount', $totalprice);
                            $this->paypal_lib->paypal_auto_form();
                              
                    }
                    else
                    {
                        $this->session->set_flashdata("error", "<label class='text-danger'>Option not Purchased, Please try again.</label>");
                    
                        redirect(base_url("Users/account/messoptions"));
                    }
                }
            }else{
              
                redirect(base_url("Users/account/messoptions"));
            }
        }
        else
        {
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    /*
    * Select slimpay payment action
    */
    function slimpayInteraction(){
        $ses = $this->session->userdata("user_id");
        if(!empty($this->session->userdata("user_id"))){
              if(empty($this->input->post())){
         
                redirect(base_url("Carting/completePaymentAction"));
            }
            if($this->cart->total_items() > 0){
              
                $opt_user_id = $this->session->userdata('user_id');
                $userdetails = $this->CartModel->getUserRows(array("user_id"=>$ses));
                              
                if($this->cart->total() == 0 && $this->cart->total_items() == 1){
                   
                    $arr1 = array("pyt_txn_status"=>"Completed",
                                  "pyt_amount"=>0,
                                  "pyt_fee"=>0,
                                  "pyt_date"=>date("Y-m-d H:i:s"));
                    $check = $this->CartModel->insertTransaction($arr1);
                    foreach($this->cart->contents() as $items){
                        $arr = $items['options'];
                        $arr['opt_tran_id'] = $check;
                        if($items['options']['opt_option_id'] == 7){
                            $arr['opt_event_id'] = $this->CartModel->insertEvents($items['eventDetails']);
                        }
                    }
                    $y = $this->CartModel->insertUserOption($arr);
                    $this->cart->destroy();
                    $this->session->set_flashdata("error", "<label class='text-success'>Event is created successfully.</label>");
                    redirect(base_url("Users/account/messoptions"));
                }else{                  
                    $arr1 = array("pyt_txn_status"=>"Pending");
                    $check = $this->CartModel->insertTransaction($arr1);
                    $transaction_id = $check;
                    foreach($this->cart->contents() as $items){
                        $arr =  $items['options'];
                        $arr['opt_tran_id'] = $check;
                        if($items['options']['opt_option_type'] == 4){
                            $pkgoptions = $this->CartModel->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$items['options']['opt_option_id'])));                    
                            $pkgid = $this->CartModel->insertUserOption($arr);
                            if(!empty($pkgoptions)){
                                foreach($pkgoptions as $pkopt){                        
                                    $optionbuy1 = array("opt_user_id" => $opt_user_id,
                                                        "opt_option_id" => $pkopt['pkg_inc_optid'],
                                                        "opt_option_type" => 5,
                                                        "opt_pkg_id"=>$pkgid,
                                                        "opt_option_purchase_date" => $items['options']['opt_option_purchase_date'],
                                                        "otp_option_duration" => $pkopt['pkg_opt_duration'],
                                                        "otp_option_qnty"=>$pkopt['pkg_opt_qty'],
                                                        "opt_option_price" => 0,
                                                        "otp_search_city"=>"",
                                                        "opt_option_active_date" => NULL,
                                                        "opt_option_end_date"=>NULL,
                                                        "opt_option_inactive_validity"=> NULL,
                                                        "opt_tran_id"=>0
                                                    );
                                    if($pkopt['opt_id'] == 7 || $pkopt['opt_id'] == 8){
                                        $arr = array("event_status"=>0,
                                                     "event_is_free"=>0,
                                                     "created_by_user"=>$opt_user_id
                                                    );                            
                                        if($pkopt['opt_id'] == 8)
                                            $arr['event_goodplace'] = 1;
                                        $optionbuy1['opt_event_id'] = $this->CartModel->insertEventwithougNoti($arr);
                                    }                                    
                                    $y = $this->CartModel->insertUserOption($optionbuy1);
                                }                    
                            }
                        }else{
                            if($items['options']['opt_option_id'] == 7 || $items['options']['opt_option_id'] == 8){
                                if($items['options']['opt_option_price'] == 0 && $items['options']['opt_option_id'] == 7){
                                    $arr['opt_event_id'] = $this->CartModel->insertEvents($items['eventDetails']);
                                }else{
                                    $arr['opt_event_id'] = $this->CartModel->insertEventwithougNoti($items['eventDetails']);
                                }
                            }
                            $y = $this->CartModel->insertUserOption($arr);
                        }
                    }

                    if($check){
                        $udetails = array("user_phone"=> $this->input->post("phone"),
                                          "user_firstname"=>$this->input->post("fname"),
                                          "user_lastname"=>$this->input->post("lname"),
                                          "user_company_name"=>$this->input->post("companyname")
                                         );
                        $bsdetails = array("bs_phone"=> $this->input->post("phone"),
                                           "bs_name"=>$this->input->post("companyname"),
                                           "bs_city"=>$this->input->post("usercity"),
                                           "bs_zipcode"=>$this->input->post("postalcode"),
                                           "bs_honorific"=>$this->input->post("honorificPrefix")
                                          );
                        $this->CartModel->updateTable("users", $udetails, array("user_id"=>$ses));
                        $this->CartModel->updateTable("otd_business_details", $bsdetails, array("bs_user"=>$ses));
                        $totalprice = $this->cart->total();                        

                        //Slimpay Integration start
                            $this->load->library("Slimpay_lib");
                            $jsonobj = array("reference"=>"BUSINESSUSER_".$ses,
                                             "address1"=>$this->input->post("company_address"),
                                             "address2"=>$this->input->post("company_address"),
                                             "city"=>$bsdetails['bs_city'],
                                             "postalCode"=>$bsdetails['bs_zipcode'],
                                             "country"=>'FR',
                                             "honorificPrefix"=>$bsdetails['bs_honorific'],
                                             "familyName"=>$udetails['user_lastname'],
                                             "givenName"=>$udetails['user_firstname'],
                                             "email"=>$userdetails['user_email'],
                                             "telephone"=> $udetails['user_phone'],
                                             "amount"=>$totalprice,
                                             "orderid"=>$transaction_id,
                                             "label"=>"First payment for your subscription",
                                             "executionDate"=>date("Y-m-d")
                                            );
                                          
                            $mandateexists = $this->CartModel->getUserMandate(array("slmref_userid"=>$opt_user_id));
                            if(!empty($mandateexists))
                            {                                
                                $mandateref = $mandateexists['slmref_mandateref'];

                                $mandatestate = $this->slimpay_lib->getMandateState(array("reference"=>$mandateref));

                              

                                if($mandatestate['state'] == "active"){  

                                  
                                    $jsonobj['mdreference'] = $mandateref;                                    
                                 
                                    $payments = $this->slimpay_lib->makeDirectPayment($jsonobj);
                                   
                                    if($payments){
                                      
                                        $this->session->unset_userdata("purchase_option");
                                        $this->cart->destroy();                                        
                                        $optiondetails = $this->CartModel->getOptionDetails($check);
                                   

                                        if(!empty($optiondetails)){
                                            $purchasedoptions = "";
                                            foreach($optiondetails as $txnopt){
                                                $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                                                $opt_option_price = $txnopt['opt_option_price'];
                                                $opt_option_id = $txnopt['opt_option_id'];
                                                $checkduration = $txnopt['otp_option_duration'];
                                                $opt_type = $txnopt['opt_option_type'];                
                                                $mstopt = $this->CartModel->getMasterOption($txnopt['opt_option_id']);                
                                            
                                                if($opt_type == 4){
                                                  
                                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                }else{
                                                    
                                                    if($txnopt['otp_option_duration'] == "1 Day" )
                                                    {
                                                       
                                                        if($opt_option_id != 7 && $opt_option_id != 8){
                                                      
                                                            $this->CartModel->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                                                        
                                                        }
                                                        $date1 = date_create($txnopt['opt_option_active_date']);
                                                        $date2 = date_create($txnopt['opt_option_end_date']);        
                                                        $difference = date_diff($date1,$date2);
                                                        $diff = $difference->format('%a');
                                                        if($diff > 0)
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                                        else
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                                    }
                                                    if($txnopt['opt_option_id'] == 6){
                                                    
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                                    }else{
                                                      
                                                        $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                    }
                                                }
                                            }
                                        }


                                        $transdetails = $this->CartModel->getSlimpayUserDetails($check);
                                      
                                        $templateheader = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                                        $templatecenter = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>34));
                                        $templatefooter = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                                        $subject = $templatecenter['tmplate_subject'];
                                        
                                        $dummy = array("%%Firstname%%",
                                                       "%%LastName%%", 
                                                       "%%Email%%", 
                                                       "%%BusinessName%%", 
                                                       "%%CompanyNumber%%", 
                                                       "%%Address%%", 
                                                       "%%Phone%%", 
                                                       "%%Gender%%",
                                                       "%%PurchaseId%%",
                                                       "%%PurchaseDate%%", 
                                                       "%%PurchaseDetails%%",
                                                       "%%MandateReference%%",
                                                       "%%TotalAmount%%",
                                                       "%%SignDate%%" );

                                        $real = array($transdetails['user_firstname'], 
                                                      $transdetails['user_lastname'],
                                                      $transdetails["user_email"],
                                                      $transdetails["user_company_name"],
                                                      $transdetails['user_company_number'],
                                                      $transdetails["user_company_address"],
                                                      $transdetails['user_phone'],
                                                      $transdetails['user_gender'],
                                                      $check,
                                                      $transdetails['opt_option_purchase_date'],
                                                      $purchasedoptions,
                                                      $mandateref,
                                                      $transdetails['pyt_amount'],
                                                      date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                                    );
                                    
                                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];  
                                                                       
                                        $x= $this->send_mail($transdetails["user_email"], $subject, $msg);
                                      
                                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                                      
                                      
                                        $this->session->set_userdata("slimpay_success", "Successfull");
                                    
                                       
                                        redirect(base_url("Users/account/messoptions"));
                                     
                                    }else{
                                        $optiondetails = $this->CartModel->getOptionDetails($check);
                                        if(!empty($optiondetails)){
                                            $purchasedoptions = "";
                                            foreach($optiondetails as $txnopt){
                                                $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                                                $opt_option_price = $txnopt['opt_option_price'];
                                                $opt_option_id = $txnopt['opt_option_id'];
                                                $checkduration = $txnopt['otp_option_duration'];
                                                $opt_type = $txnopt['opt_option_type'];                
                                                $mstopt = $this->CartModel->getMasterOption($txnopt['opt_option_id']);                
                                                if($opt_type == 4){
                                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                }else{
                                                    if($txnopt['otp_option_duration'] == "1 Day" ){                                                        
                                                        $date1 = date_create($txnopt['opt_option_active_date']);
                                                        $date2 = date_create($txnopt['opt_option_end_date']);        
                                                        $difference = date_diff($date1,$date2);
                                                        $diff = $difference->format('%a');
                                                        if($diff > 0)
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                                        else
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                                    }
                                                    if($txnopt['opt_option_id'] == 6){
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                                    }else{
                                                        $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                    }
                                                }
                                            }
                                        }
                                        $transdetails = $this->CartModel->getSlimpayUserDetails($check);
                                        $templateheader = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                                        $templatecenter = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>36));
                                        $templatefooter = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                                        $subject = $templatecenter['tmplate_subject'];
                                        $dummy = array("%%Firstname%%",
                                                       "%%LastName%%", 
                                                       "%%Email%%", 
                                                       "%%BusinessName%%", 
                                                       "%%CompanyNumber%%", 
                                                       "%%Address%%", 
                                                       "%%Phone%%", 
                                                       "%%Gender%%",
                                                       "%%PurchaseId%%",
                                                       "%%PurchaseDate%%", 
                                                       "%%PurchaseDetails%%",
                                                       "%%MandateReference%%",
                                                       "%%TotalAmount%%",
                                                       "%%SignDate%%" );
                                        $real = array($transdetails['user_firstname'], 
                                                      $transdetails['user_lastname'],
                                                      $transdetails["user_email"],
                                                      $transdetails["user_company_name"],
                                                      $transdetails['user_company_number'],
                                                      $transdetails["user_company_address"],
                                                      $transdetails['user_phone'],
                                                      $transdetails['user_gender'],
                                                      $check,
                                                      $transdetails['opt_option_purchase_date'],
                                                      $purchasedoptions,
                                                      $mandateref,
                                                      $transdetails['pyt_amount'],
                                                      date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                                    );
                                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];                                    
                                        $x= $this->send_mail($transdetails["user_email"], $subject, $msg);
                                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                                        
                                        $this->session->set_userdata("slimpay_error", "No");
                                        redirect(base_url("Carting/completePaymentAction"));
                                    }
                                }else{
                                    $this->session->unset_userdata("purchase_option");
                                    //$this->cart->destroy();
                                    $this->slimpay_lib->signSlimpayMandate($jsonobj);
                                }
                            }else{
                                $this->session->unset_userdata("purchase_option");
                                //$this->cart->destroy();                                                           
                               $this->slimpay_lib->signSlimpayMandate($jsonobj);
                             
                               
                            }
                        //Slimpay Integration End                 
                    }
                    else
                    {
                        $this->session->set_flashdata("error", "<label class='text-danger'>Option not Purchased, Please try again.</label>");
                        redirect(base_url("Users/account/messoptions"));
                    }
                }
            }else{
               
                redirect(base_url("Users/account/messoptions"));
            }            
        }
        else
        {
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    function completepayment(){
    	if(!empty($this->session->userdata("user_id"))){
    	
            if($this->cart->total_items() > 0){            	
            	$opt_user_id = $this->session->userdata('user_id');
            	if($this->cart->total() == 0 && $this->cart->total_items() == 1){            		
            		$arr1 = array("pyt_txn_status"=>"Completed",
                                  "pyt_amount"=>0,
                                  "pyt_fee"=>0,
                                  "pyt_date"=>date("Y-m-d H:i:s"));
            		$check = $this->CartModel->insertTransaction($arr1);
            		foreach($this->cart->contents() as $items){
            			$arr =  $items['options'];
            			$arr['opt_tran_id'] = $check;
            			if($items['options']['opt_option_id'] == 7){            				
            				$arr['opt_event_id'] = $this->CartModel->insertEvents($items['eventDetails']);
            			}
            		}
            		$y = $this->CartModel->insertUserOption($arr);
            		$this->cart->destroy();
            		$this->session->set_flashdata("error", "<label class='text-success'>Event is created successfully.</label>");
                
                    redirect(base_url("Users/account/messoptions"));
            	}else{            		
            		$arr1 = array("pyt_txn_status"=>"Pending");
            		$check = $this->CartModel->insertTransaction($arr1);
	            	foreach($this->cart->contents() as $items){
	            		$arr =  $items['options'];
	            		$arr['opt_tran_id'] = $check;
	            		if($items['options']['opt_option_type'] == 4){
	            			$pkgoptions = $this->CartModel->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$items['options']['opt_option_id'])));                    
			                $pkgid = $this->CartModel->insertUserOption($arr);
			                if(!empty($pkgoptions)){
			                    foreach($pkgoptions as $pkopt){                        
			                        $optionbuy1 = array("opt_user_id" => $opt_user_id,
			                                            "opt_option_id" => $pkopt['pkg_inc_optid'],
			                                            "opt_option_type" => 5,
			                                            "opt_pkg_id"=>$pkgid,
			                                            "opt_option_purchase_date" => $items['options']['opt_option_purchase_date'],
			                                            "otp_option_duration" => $pkopt['pkg_opt_duration'],
			                                            "otp_option_qnty"=>$pkopt['pkg_opt_qty'],
			                                            "opt_option_price" => 0,
			                                            "otp_search_city"=>"",
			                                            "opt_option_active_date" => NULL,
			                                            "opt_option_end_date"=>NULL,
			                                            "opt_option_inactive_validity"=> NULL,
			                                            "opt_tran_id"=>0
			                                       	);
			                        if($pkopt['opt_id'] == 7 || $pkopt['opt_id'] == 8){                        

			                            $arr = array("event_status"=>0,
			                                         "event_is_free"=>0,
			                                         "created_by_user"=>$opt_user_id
			                                        );                            
			                            if($pkopt['opt_id'] == 8)
			                                $arr['event_goodplace'] = 1;
			                            $optionbuy1['opt_event_id'] = $this->CartModel->insertEventwithougNoti($arr);
			                        }
			                      
			                        $y = $this->CartModel->insertUserOption($optionbuy1);
			                    }                    
			                }
	            		}else{
		            		if($items['options']['opt_option_id'] == 7 || $items['options']['opt_option_id'] == 8){
		            			if($items['options']['opt_option_price'] == 0 && $items['options']['opt_option_id'] == 7){
		            				$arr['opt_event_id'] = $this->CartModel->insertEvents($items['eventDetails']);
		            			}else{
					                $arr['opt_event_id'] = $this->CartModel->insertEventwithougNoti($items['eventDetails']);
					            }
				            }
				            $y = $this->CartModel->insertUserOption($arr);
				        }
		            }

		            if($check)
		            {
		            	$totalprice = $this->cart->total();
		            	$this->cart->destroy();	            	
		               
		                    $this->load->library("paypal_lib");
		                    //Set variables for paypal form
		                    $returnURL = base_url().'Users/completePayment'; //payment success url
		                    $cancelURL = base_url().'Users/cancelPayment'; //payment cancel url
		                    $notifyURL = base_url().'Pages/paymentIpn'; //ipn url
		                    //get particular product data                    
		                    $this->paypal_lib->add_field('return', $returnURL);
		                    $this->paypal_lib->add_field('cancel_return', $cancelURL);
		                    $this->paypal_lib->add_field('notify_url', $notifyURL);
		                    $this->paypal_lib->add_field('item_name', "YellowPages");
		                    $this->paypal_lib->add_field('custom', $check);
		                    $this->paypal_lib->add_field('item_number',  "1");
		                    $this->paypal_lib->add_field('amount', $totalprice);
		                    $this->paypal_lib->paypal_auto_form();
		                //Paypal Code Integration End                
		            }
		            else
		            {
		                $this->session->set_flashdata("error", "<label class='text-danger'>Option not Purchased, Please try again.</label>");
		                redirect(base_url("Users/account/messoptions"));
		            }
		        }
            }else{
            
            	redirect(base_url("Users/account/messoptions"));
            }            
        }
        else
        {

            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    function removeCartOption(){
    	$ses = $this->session->userdata("user_id");
    	if(!empty($ses)){
    		$row = $this->input->post("row");
    		$pagetext = $this->CartModel->getTextContent(30);
    		if(!empty($row)){    			
		  //   	$data = array('rowid' => $row,
				// 		      'qty'   => 0
				// 			 );		    	
				// $this->cart->update($data);
				$this->cart->remove($row);
				echo json_encode(array("status"=>200, "totalproduct"=>$this->cart->total_items(), "totalprice"=>$pagetext['label6'].$this->cart->format_number($this->cart->total())));
			}else{
				echo json_encode(array("status"=>1, "msg"=>"Required parameter is missing"));
			}
		}else{
			echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
		}

    }

    public function generateRandomString($length = 12) {
	        // $length = 12;
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function sendMailFormat($subject, $msginner, $template = 21)
    {
     
        $templateheader = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
        $templatefooter = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
        $templatedata = $this->CartModel->getEmailTemplateRows(array("tmplate_id"=>$template));

        $format = '<html><head><meta charset="utf-8"><title>'.$subject.'</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"></head>
                    <body style="font-family: open Sans;font-size: 13px; line-height:20px;">
                    <div style="padding: 0 10px;">
                        <div style="max-width:550px;width:85%;padding:30px;margin:30px auto;border:1px solid #e2e2e2; overflow:hidden; background-position:center;background-size:cover;text-align: center;color: #fff;">
                            <div style="padding:30px;border:2px solid #fff;background:rgba(0, 0, 0, 0.63);">';
        
        $format .=  $templateheader['tmplate_content'].'
                    <div style="border-top: 1px dotted #fff; display: block; margin-top:5px; margin-bottom:5px;"></div>
                        <div class="mailbody" style="min-height: 250px;">
                            <div>'.$templatedata['tmplate_content'].' <br>'.$msginner.'</div>
                        </div>';

        $format .= $templatefooter['tmplate_content'].'</div></div></div></body></html>';

        return $format;
    }



    function send_mail($email, $subject="Test Mail", $msg="Testing mail"){
	    $from = SENDER_EMAIL;
        $this->load->library('email');
        $config['protocol']    = 'ssl';
        $config['smtp_host']    = 'ssl://mail.gandi.net';
        $config['smtp_port']    = 587;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $from;
        $config['smtp_pass']    = 'puR1-7,XifsArlyV!5';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; 
        $config['validation'] = TRUE;      
        $this->email->initialize($config);
        $fromname = SENDER_NAME;
        $fromemail =SENDER_EMAIL;
        $to = $email;        
        $subject=$subject;
        $message=$msg;
        $cc=false;
        $this->email->from($fromemail, $fromname);
        $this->email->to($to);
        $this->email->subject($subject);       
        $this->email->message($msg);  
        if($cc){
            $this->email->cc($cc);
        }

        if(!$this->email->send()){
            return $this->email->print_debugger();
        }else{
            return true;
        }
	} 
}