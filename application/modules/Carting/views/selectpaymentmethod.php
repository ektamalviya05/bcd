<section class="paymnt-method">	
<div class="container">	
	<div class="row">
		<div class="paymentCont">
			<?php
				// echo "<pre>";
	            // print_r($this->cart->contents());
	            // echo "</pre>";
	            $purchase_option = $this->cart->contents();
	            $onlyslimpay = false;
	            foreach($purchase_option as $items){
	            	if($items['options']['opt_option_type'] == 4){
	            		$onlyslimpay = true;
	            		break;
	            	}
	            }
            ?>
			<div class="headingWrap">
					<h3 class="headingTop text-center"><?php echo $texts['label1']; ?></h3>	
					<p class="text-center"><?php echo $texts['label2']; ?></p>
			</div>
			<div class="paymentWrap">
				<div class="btn-group paymentBtnGroup btn-group-justified" >
					<a href="javascript:;<?php //echo base_url("Carting/slimpayInteraction"); ?>" class="btn paymentMethod" data-toggle="modal" data-target="#userDetailsModal">
		            	<div class="method visa"></div>		               
		            </a>
					<?php
					if($onlyslimpay){
						?>
						<label class="btn paymentMethod">
			            	<div class="method master-card disablemode"></div>		                
			            </label>
			            <label class="btn paymentMethod">
		            		<div class="method amex disablemode"></div>		                
			            </label>	
			            <?php
					}else{
						?>		            
			            <a href="javascript:;"  class="btn paymentMethod">
			            	<div class="method master-card"></div>		                
			            </a>
			            <a href="<?php echo base_url("Carting/paypalInteraction"); ?>" class="btn paymentMethod">
		            		<div class="method amex"></div>
			            </a>
			            <?php
		        	}
		            ?>
		        </div>        
			</div>
            
            <div class="abt-paymnt">
            	<?php echo $texts['label3']; ?>
            </div>
			<div class="footerNavWrap clearfix">
				<a href="<?php echo base_url("Users/account/messoptions"); ?>" class="btn btn-success pull-left btn-fyi-black"> <i class="fa fa-angle-left"></i><?php echo $texts['label4']; ?></a>
				<!-- <div class="btn btn-success pull-right btn-fyi-yellow"><i class="fa fa-money"></i>Pay Now</div> -->
			</div>
		</div>
	</div>
</div>
</section>

<!-- Business user details model-->  
	<div id="userDetailsModal" class="modal chat-modal fade" role="dialog">
	  	<div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      	<form id="checkslimpayuserdetails" method="POST" action="<?php echo base_url("Carting/slimpayInteraction"); ?>" autocomplete="off">
			        <div class="modal-header">
			          	<div class="prof-msg-details">
				            <div class="row">
				              	<div class="col-xs-12">
					                <div class="msg-pro-pic-sec">
						                <div class="msh-prof-details">
						                  <h3>Your Information</h3>
						                </div>
						                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>	                  	
					                </div>
				              	</div>
				            </div>
			          	</div>
			        </div>
			        <div class="modal-body review-body" data-mcs-theme="dark">
			        	<div class="col-sm-12">
			        		<div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label">Honorifics
					                  		<span class="tooltip">?
						                      	<span class="tooltiptext">Honorifics</span>
						                    </span>
						                </label>
					                    <div class="btn-group" data-toggle="buttons">					                      
											<label class="btn btn-default active">
					                        	<input type="radio" name="honorificPrefix" <?php if($userdetails['bs_honorific'] == "Mr") echo "checked"; ?> value="Mr"> Mr
					                      	</label>
					                      	<label class="btn btn-default">
					                        	<input type="radio" name="honorificPrefix" <?php if($userdetails['bs_honorific'] == "Miss") echo "checked"; ?> value="Miss"> Miss
					                      	</label>
					                      	<label class="btn btn-default">
					                        	<input type="radio" name="honorificPrefix" <?php if($userdetails['bs_honorific'] == "Mrs") echo "checked"; ?> value="Mrs"> Mrs
					                      	</label>
					                    </div>					                  
					                </div>
					            </div>
					            <div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label">TELEPHONE:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext">TELEPHONE</span>
						                    </span>
						                </label>
					                  	<input type="text" name="phone" value="<?php echo $userdetails['user_phone']; ?>" class="form-control" placeholder="Your First Name">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label">Prenom:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext">Prenom</span>
						                    </span>
						                </label>
					                  	<input type="text" name="fname" value="<?php echo $userdetails['user_firstname']; ?>" class="form-control" placeholder="Your First Name">
					                </div>
				              	</div>
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label">Nom:
						                    <span class="tooltip">?
						                      <span class="tooltiptext">Nom</span>
						                    </span>
					                  	</label>
					                  	<input type="text" name="lname" value="<?php echo $userdetails['user_lastname']; ?>" class="form-control" placeholder="Your Last Name">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label">NOM DE L'ENTREPRISE:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext">NOM DE L'ENTREPRISE</span>
						                    </span>
						                </label>
					                  	<input type="text" name="companyname" value="<?php echo $userdetails['user_company_name']; ?>" class="form-control" placeholder="NOM DE L'ENTREPRISE">
					                </div>
				              	</div>
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label">NUMÉRO DE SIRET:
						                    <span class="tooltip">?
						                      <span class="tooltiptext">NUMÉRO DE SIRET</span>
						                    </span>
					                  	</label>
					                  	<input type="text" readonly="readonly" value="<?php echo $userdetails['user_company_number']; ?>" class="form-control" placeholder="Your Last Name">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-12">
					                <div class="form-group">
						                <label class="form-label">Email:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext">Email</span>
						                    </span>
						                </label>
					                  	<input placeholder="*Email" readonly="readonly" class="form-control " type="text" value="<?php echo $userdetails['user_email']; ?>">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-12">
					                <div class="form-group">
						                <label class="form-label">VEUILLEZ SAISIR VOTRE ADRESSE:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext">VEUILLEZ SAISIR VOTRE ADRESSE</span>
						                    </span>
						                </label>
					                  	<input id="autocomplete" placeholder="*VEUILLEZ SAISIR VOTRE ADRESSE" name="company_address" class="form-control " onfocus="geolocate()" type="text" value="<?php echo $userdetails['user_company_address']; ?>">
					                </div>
				              	</div>				              	
				            </div>
				            <div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label">City:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext">City</span>
						                    </span>
						                </label>
					                  	<input type="text" name="usercity" value="<?php echo $userdetails['bs_city']; ?>" class="form-control" placeholder="City">
					                </div>
				              	</div>
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label">Postal Code:
						                    <span class="tooltip">?
						                      <span class="tooltiptext">Postal Code</span>
						                    </span>
					                  	</label>
					                  	<input type="text" name="postalcode" value="<?php echo $userdetails['bs_zipcode']; ?>" class="form-control" placeholder="Postal Code">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				            	<div class="col-md-12">
					            	<div class="form-group termlink">
					            		<label><input type="checkbox" checked name="acceptterms" id="proconfidential" value='1'/> By Clicking this you are agree with Otourdemoi terms & condtions.</label>
					            	</div>
					            </div>
				            </div>
			          	</div>
			        </div>
			        <div class="modal-footer">
			          	<div class="form-group">
				            <button type="button" id="evtsubmit" class="btn" name="confirmuserdetails"> Confirm</button>
				            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				            <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
				            <div class="userdetailsermsg"></div>
			          	</div>
			        </div>
		      	</form>
		    </div>
	  	</div>
	</div>
<!-- Business user details model end -->
<!-- Confirm for slimpay -->
	<div class="modal fade" id="usemandateconfirmbox" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
			  	<div class="modal-header"> 
				    <h4 class="modal-title"></h4>
				    <div class="title-imge">
				      	<img src="<?php echo base_url();?>assets/img/logo.png">
				    </div>
			  	</div>
			  	<div class="modal-body">
				    <div class="modal-login-txt">
				      <h3>You already has a mandate, do you want to use it?</h3>
				    </div>
			  	</div>
			  	<div class="modal-footer">
			  		<button type="button" id="evtsubmit" class="btn" name="confirmuserpermission"> Confirm</button>
			     	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			     	<span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
			 	</div>
			</div>
		</div>
	</div>
<!-- Confirm for slimpay -->

<!-- Slimpay transation thank you message -->  
  <?php
    $slimpaythankyou = $this->CartModel->getTextContent(26);
  ?>
    <div class="modal fade" id="slimpaytransactionfailed" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header"> 
            <h4 class="modal-title">Sorry<?php //echo $slimpaythankyou['label1'] ?></h4>
            <div class="title-imge">
              <img src="<?php echo base_url();?>assets/img/logo.png">
            </div>
          </div>
          <div class="modal-body">
            <div class="modal-login-txt">
              <h3>Your transaction is not proceed, Please try again.<?php //echo $slimpaythankyou['label2'] ?></h3>         
            </div>
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $slimpaythankyou['label3'] ?></button>
          </div>
        </div>
      </div>
    </div>      
<!-- Slimpay transation thank you message end-->  