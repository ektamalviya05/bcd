<?php
$pagetext = $this->CartModel->getTextContent(30);
?>
<section class="cart">
	<div class="container">
		<div class="row">			
			
            <div class="cart-table details-fill">
	            <?php
	            if($this->cart->total_items() > 0){
	            ?>
		            <table>
		            	<tr>
		                	<th><?php echo $pagetext['label1'] ?></th>
		                    <th><?php echo $pagetext['label2'] ?></th>
		                    <th><?php echo $pagetext['label12'] ?></th>
		                    <!-- <th><?php //echo $pagetext['label4'] ?></th> -->
		                    <th><?php echo $pagetext['label5'] ?></th>
		                </tr>
		                <?php	                
						foreach($this->cart->contents() as $items){
						?>
		                    <tr>                        
		                    	<td><?php echo $items['name']; ?></td>
		                        <td><?php echo $items['details']; ?></td>
		                        <td><?php echo $pagetext['label6'].$this->cart->format_number($items['subtotal']); ?></td>
		                        <!-- <td><?php //echo $items['duration']; ?></td> -->
		                        <td><button class="dele-btn removecart" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" atr-desc="<?php echo $items['rowid']; ?>"><i class="fa fa-trash-o"></i> <?php echo $pagetext['label7']; ?></button></td>
		                    </tr>
		                    <?php
						}
		                ?>
		            
		            	<tr>
		                	<th colspan="3"><?php echo $pagetext['label8'] ?></th>
		                    <th colspan="2"><?php echo $pagetext['label9'] ?></th>
		                </tr>
		                <tr>
		                	<!-- <td><p id="totalprod"><?php echo $this->cart->total_items(); ?></p></td> -->
		                    <td colspan="3">
		                    	<p style="margin: 0;">
		                    		<a class="btn btn-back" href="<?php echo base_url("Users/account/messoptions"); ?>"> &lt; <?php echo $pagetext['label10'] ?></a>
		                    		<b id="totalpayble" style="float:right;padding-top: 7px"><?php echo $pagetext['label6'].$this->cart->format_number($this->cart->total()); ?></b></p>
		                    </td>
		                    <td colspan="2">
		                    	<p style="margin: 0">
									<!-- <button class="dele-btn pay-btn cmplete-payment" data-title='Procéder au paiment ?' data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top"><i class="fa fa-money"></i> <?php //echo $pagetext['label11'] ?></button> -->
									<button class="dele-btn pay-btn cmplete-payment" data-title='Procéder au paiment ?' data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top"><i class="fa fa-money"></i> <?php echo $pagetext['label11'] ?></button>
								</p>
							</td>
		                </tr>
		            </table>
	            <?php
	        	}else{
	        		echo $pagetext['label3'];
	        	}
	            ?>
	            
                
					<!--<div class="cart-table details-fill cart-footer">
					
						<div class="col-sm-4">
							<div class="product-name">
								<h3>Total Options</h3>
							</div>

							<div class="product-name">
								<p id="totalprod"><?php echo $this->cart->total_items(); ?></p>
							</div>
						</div>

						<div class="col-sm-4 text-center">
							<div class="product-name">
								<h3>Total price</h3>
							</div>

							<div class="product-name">
								<p><b id="totalpayble">$<?php echo $this->cart->format_number($this->cart->total()); ?></b></p>
							</div>
						</div>

						<div class="col-sm-4 text-right">
							<div class="product-name">
								<h3>Pay Option</h3>
							</div>

							<div class="product-name">
								<p>
									<a class="btn btn-back" href="<?php echo base_url("Users/account/messoptions"); ?>"> &lt; Back to Options</a>
									<button class="dele-btn pay-btn cmplete-payment" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top"><i class="fa fa-money"></i> Pay Now</button>
								</p>
							</div>
						</div>
					</div>-->
					<!-- <?php
				// }else{
					?>
					<div class="cart-table details-fill">
						<div class="col-sm-12 div-br">
							<div class="product-name">
	          					<h3><strong>Sorry</strong>, Cart is empty. Please go to the mes options menu for buying options.</h3>
							</div>
						</div>
					</div>
					<?php
				// }
				?> -->			
			</div>
		</div>
	</div>
</section>