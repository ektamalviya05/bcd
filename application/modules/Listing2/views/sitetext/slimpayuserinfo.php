<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>User Information Details</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($listing)){                      
                    ?>
                    <form id="updatecontent" autocomplete="off" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/contentUpdate/$page"); ?>' data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group">                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Heading:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label1" id="label1" value="<?php echo $listing["label1"]; ?>"/>
                          <?php echo form_error('label1'); ?>
                        </div>                        
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Honorifics:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label17" id="label17" value="<?php echo $listing["label17"]; ?>"/>
                            <?php echo form_error('label17'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Honorifics?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label19" id="label19" value="<?php echo $listing["label19"]; ?>"/>
                            <?php echo form_error('label19'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">                        
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class='col-md-3 col-sm-3 col-xs-3'>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Mr :</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label2" id="label2" value="<?php echo $listing["label2"]; ?>"/>
                            </div>
                          </div>
                          <div class='col-md-3 col-sm-3 col-xs-3'>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Miss :</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label3" id="label3" value="<?php echo $listing["label3"]; ?>"/>
                            </div>
                          </div>
                          <div class='col-md-3 col-sm-3 col-xs-3'>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Mrs :</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label4" id="label4" value="<?php echo $listing["label4"]; ?>"/>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Telephone:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label5" id="label5" value="<?php echo $listing["label5"]; ?>"/>
                            <?php echo form_error('label5'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Telephone?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label20" id="label20" value="<?php echo $listing["label20"]; ?>"/>
                            <?php echo form_error('label20'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>First Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label6" id="label6" value="<?php echo $listing["label6"]; ?>" />
                            <?php echo form_error('label6'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>First Name?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label21" id="label21" value="<?php echo $listing["label21"]; ?>" />
                            <?php echo form_error('label21'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Last Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label7" id="label7" value="<?php echo $listing["label7"]; ?>" />
                            <?php echo form_error('label7'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Last Name?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label22" id="label22" value="<?php echo $listing["label22"]; ?>" />
                            <?php echo form_error('label22'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label8" id="label8" value="<?php echo $listing["label8"]; ?>" />
                            <?php echo form_error('label8'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Name?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label23" id="label23" value="<?php echo $listing["label23"]; ?>" />
                            <?php echo form_error('label23'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Number:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label9" id="label9" value="<?php echo $listing["label9"]; ?>" />
                            <?php echo form_error('label9'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Number?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label24" id="label24" value="<?php echo $listing["label24"]; ?>" />
                            <?php echo form_error('label24'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Email:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label10" id="label10" value="<?php echo $listing["label10"]; ?>" />
                            <?php echo form_error('label10'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Email?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label25" id="label25" value="<?php echo $listing["label25"]; ?>" />
                            <?php echo form_error('label25'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Address:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label11" id="label11" value="<?php echo $listing["label11"]; ?>" />
                            <?php echo form_error('label11'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Address?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label26" id="label26" value="<?php echo $listing["label26"]; ?>" />
                            <?php echo form_error('label26'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>City:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label12" id="label12" value="<?php echo $listing["label12"]; ?>" />
                            <?php echo form_error('label12'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>City?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label27" id="label27" value="<?php echo $listing["label27"]; ?>" />
                            <?php echo form_error('label27'); ?>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Postal Code:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label13" id="label13" value="<?php echo $listing["label13"]; ?>" />
                            <?php echo form_error('label13'); ?>
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Postal Code?:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label28" id="label28" value="<?php echo $listing["label28"]; ?>" />
                            <?php echo form_error('label28'); ?>
                          </div>
                        </div>
                      </div>
                      <hr/>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Accept Agreement:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label14" id="label14" value="<?php echo $listing["label14"]; ?>" />
                          <?php echo form_error('label14'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Confirm:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label15" id="label15" value="<?php echo $listing["label15"]; ?>" />
                          <?php echo form_error('label15'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Close:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label16" id="label16" value="<?php echo $listing["label16"]; ?>" />
                          <?php echo form_error('label16'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" id="catsub" class="btn btn-success">Update</button>                          
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>                          
                            <div id="catupmsg">
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content