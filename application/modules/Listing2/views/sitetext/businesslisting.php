<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Business Listing Form</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($listing)){                      
                    ?>
                    <form id="updatecontent" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/contentUpdate/$page"); ?>' data-parsley-validate class="form-horizontal form-label-left">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Location:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label1" id="label1" value="<?php echo $listing["label1"]; ?>"/>
                          <?php echo form_error('label1'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Category:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label2" id="label2" value="<?php echo $listing["label2"]; ?>"/>
                          <?php echo form_error('label2'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Key Word:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label3" id="label3" value="<?php echo $listing["label3"]; ?>" />
                          <?php echo form_error('label3'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Extra Filter:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label4" id="label4" value="<?php echo $listing["label4"]; ?>" />
                          <?php echo form_error('label4'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Filter Heading in Drop Down:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label5" id="label5" value="<?php echo $listing["label5"]; ?>" />
                          <?php echo form_error('label5'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Search:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label6" id="label6" value="<?php echo $listing["label6"]; ?>" />
                          <?php echo form_error('label6'); ?>
                        </div>
                      </div>
                      <hr/>
                      <div class="x_title">
                        <h2> Page Content<small></small></h2>                   
                        <div class="clearfix"></div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Top Profiles:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label7" id="label7" value="<?php echo $listing["label7"]; ?>" />
                          <?php echo form_error('label7'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Total Records:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label8" id="label8" value="<?php echo $listing["label8"]; ?>" />
                          <?php echo form_error('label8'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>No record:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label9" id="label9" value="<?php echo $listing["label9"]; ?>" />
                          <?php echo form_error('label9'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Full Map:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label10" id="label10" value="<?php echo $listing["label10"]; ?>" />
                          <?php echo form_error('label10'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Short Map:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label11" id="label11" value="<?php echo $listing["label11"]; ?>" />
                          <?php echo form_error('label11'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" id="catsub" class="btn btn-success">Update</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>
                            <div id="catupmsg">
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content