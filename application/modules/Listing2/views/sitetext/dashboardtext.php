<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>User Dashboard Lebel Text </h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($listing)){                      
                      ?>
                      <form id="updatecontent" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/contentUpdate/$page"); ?>' data-parsley-validate class="form-horizontal form-label-left">
                        <div class="x_title">
                          <h2>Mes Informations <small>Informations personnelles</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>First Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label1" id="label1" value="<?php echo $listing["label1"]; ?>"/>
                            <?php echo form_error('label1'); ?>
                          </div>
                        </div>                      
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Last Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label2" id="label2" value="<?php echo $listing["label2"]; ?>"/>
                            <?php echo form_error('label2'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>language:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label3" id="label3" value="<?php echo $listing["label3"]; ?>"/>
                            <?php echo form_error('label3'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Gender:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label4" id="label4" value="<?php echo $listing["label4"]; ?>"/>
                            <?php echo form_error('label4'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Date Of Birth:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label5" id="label5" value="<?php echo $listing["label5"]; ?>"/>
                            <?php echo form_error('label5'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Submit Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label45" id="label45" value="<?php echo $listing["label45"]; ?>"/>
                            <?php echo form_error('label45'); ?>
                          </div>
                        </div>
                        <hr>
                        <div class="x_title">
                          <h2>Mes Informations <small>Account parameter</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <h4 class="sub-heading">Change Email</h4>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>New Email:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label6" id="label6" value="<?php echo $listing["label6"]; ?>"/>
                            <?php echo form_error('label6'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label7" id="label7" value="<?php echo $listing["label7"]; ?>"/>
                            <?php echo form_error('label7'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Change Mail Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label46" id="label46" value="<?php echo $listing["label46"]; ?>"/>
                            <?php echo form_error('label46'); ?>
                          </div>
                        </div>
                        <hr/>
                        <h4 class="sub-heading">Change Password</h4>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Old Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label8" id="label8" value="<?php echo $listing["label8"]; ?>"/>
                            <?php echo form_error('label8'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>New Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label9" id="label9" value="<?php echo $listing["label9"]; ?>"/>
                            <?php echo form_error('label9'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Confirm New Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label10" id="label10" value="<?php echo $listing["label10"]; ?>"/>
                            <?php echo form_error('label10'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Change Password Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label47" id="label47" value="<?php echo $listing["label47"]; ?>"/>
                            <?php echo form_error('label47'); ?>
                          </div>
                        </div>
                        <hr/>
                        <h4 class="sub-heading">Delete My Account</h4>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Password:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label11" id="label11" value="<?php echo $listing["label11"]; ?>"/>
                            <?php echo form_error('label11'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Delete My Account Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label48" id="label48" value="<?php echo $listing["label48"]; ?>"/>
                            <?php echo form_error('label48'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Informations <small>Mon entreprise</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label12" id="label12" value="<?php echo $listing["label12"]; ?>"/>
                            <?php echo form_error('label12'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Number:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label13" id="label13" value="<?php echo $listing["label13"]; ?>"/>
                            <?php echo form_error('label13'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Website Link:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label14" id="label14" value="<?php echo $listing["label14"]; ?>"/>
                            <?php echo form_error('label14'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Creation Date:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label15" id="label15" value="<?php echo $listing["label15"]; ?>"/>
                            <?php echo form_error('label15'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Telephone:</label>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Heading:</label>
                            </div>
                            <input class="form-control" name="label16" id="label16" value="<?php echo $listing["label16"]; ?>"/>                            
                            <?php echo form_error('label16'); ?>                            
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Add Button:</label>
                            </div>
                            <input class="form-control" name="label50" id="label50" placeholder="Add New Button" value="<?php echo $listing["label50"]; ?>"/>
                            <?php echo form_error('label50'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Twitter User ID:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label17" id="label17" value="<?php echo $listing["label17"]; ?>"/>
                            <?php echo form_error('label17'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Email Address:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label18" id="label18" value="<?php echo $listing["label18"]; ?>"/>
                            <?php echo form_error('label18'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Categories:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label19" id="label19" value="<?php echo $listing["label19"]; ?>"/>
                            <?php echo form_error('label19'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Extra Filters:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label20" id="label20" value="<?php echo $listing["label20"]; ?>"/>
                            <?php echo form_error('label20'); ?>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Services:</label>
                            </div>
                            <input class="form-control" name="label51" id="label51" value="<?php echo $listing["label51"]; ?>"/>
                            <?php echo form_error('label51'); ?>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Product:</label>
                            </div>
                            <input class="form-control" name="label52" id="label52" value="<?php echo $listing["label52"]; ?>"/>
                            <?php echo form_error('label52'); ?>
                          </div>
                          <div class="col-md-4 col-sm-4 col-xs-4">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Others:</label>
                            </div>
                            <input class="form-control" name="label53" id="label53" value="<?php echo $listing["label53"]; ?>"/>
                            <?php echo form_error('label53'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Location:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label21" id="label21" value="<?php echo $listing["label21"]; ?>"/>
                            <?php echo form_error('label21'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Company Description:</label>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Heading:</label>
                            </div>
                            <input class="form-control" name="label22" id="label22" value="<?php echo $listing["label22"]; ?>"/>
                            <?php echo form_error('label22'); ?>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Character Limit Text:</label>
                            </div>
                            <input class="form-control" name="label62" id="label62" value="<?php echo $listing["label62"]; ?>"/>
                            <?php echo form_error('label62'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Submit Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label49" id="label49" value="<?php echo $listing["label49"]; ?>"/>
                            <?php echo form_error('label49'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Prices:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label23" id="label23" value="<?php echo $listing["label23"]; ?>"/>
                            <?php echo form_error('label23'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Upload Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label54" id="label54" value="<?php echo $listing["label54"]; ?>"/>
                            <?php echo form_error('label54'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Business Open Time:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label24" id="label24" value="<?php echo $listing["label24"]; ?>"/>
                            <?php echo form_error('label24'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Monday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label25" id="label25" value="<?php echo $listing["label25"]; ?>"/>
                            <?php echo form_error('label25'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Tuesday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label26" id="label26" value="<?php echo $listing["label26"]; ?>"/>
                            <?php echo form_error('label26'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Wednesday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label27" id="label27" value="<?php echo $listing["label27"]; ?>"/>
                            <?php echo form_error('label27'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Thursday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label28" id="label28" value="<?php echo $listing["label28"]; ?>"/>
                            <?php echo form_error('label28'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Friday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label29" id="label29" value="<?php echo $listing["label29"]; ?>"/>
                            <?php echo form_error('label29'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Saturday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label30" id="label30" value="<?php echo $listing["label30"]; ?>"/>
                            <?php echo form_error('label30'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Sunday:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label31" id="label31" value="<?php echo $listing["label31"]; ?>"/>
                            <?php echo form_error('label31'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Seperator Text:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label55" id="label55" value="<?php echo $listing["label55"]; ?>"/>
                            <?php echo form_error('label55'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Submit Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label56" id="label56" value="<?php echo $listing["label56"]; ?>"/>
                            <?php echo form_error('label56'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Gallery Photos:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label32" id="label32" value="<?php echo $listing["label32"]; ?>"/>
                            <?php echo form_error('label32'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Upload Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label57" id="label57" value="<?php echo $listing["label57"]; ?>"/>
                            <?php echo form_error('label57'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Informations <small>Boite à idée</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Title:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label33" id="label33" value="<?php echo $listing["label33"]; ?>"/>
                            <?php echo form_error('label33'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Message:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label34" id="label34" value="<?php echo $listing["label34"]; ?>"/>
                            <?php echo form_error('label34'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Send Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label58" id="label58" value="<?php echo $listing["label58"]; ?>"/>
                            <?php echo form_error('label58'); ?>
                          </div>
                        </div>

                        <hr/>
                        <div class="x_title">
                          <h2>Mes commerçants <small>Mes favoris</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>View Profile:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label65" id="label65" value="<?php echo $listing["label65"]; ?>"/>
                            <?php echo form_error('label65'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>UnFollow:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label66" id="label66" value="<?php echo $listing["label66"]; ?>"/>
                            <?php echo form_error('label66'); ?>
                          </div>
                        </div>                        
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Message:</label>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Send Message:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label67" id="label67" value="<?php echo $listing["label67"]; ?>"/>
                              <?php echo form_error('label67'); ?>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Live Chat:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label68" id="label68" value="<?php echo $listing["label68"]; ?>"/>
                              <?php echo form_error('label68'); ?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>No Records:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label69" id="label69" value="<?php echo $listing["label69"]; ?>"/>
                            <?php echo form_error('label69'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes commerçants <small>Bon plan de mes favoris</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>No Records:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label70" id="label70" value="<?php echo $listing["label70"]; ?>"/>
                            <?php echo form_error('label70'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Messages <small>Message</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>View Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label71" id="label71" value="<?php echo $listing["label71"]; ?>"/>
                            <?php echo form_error('label71'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>No Records:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label72" id="label72" value="<?php echo $listing["label72"]; ?>"/>
                            <?php echo form_error('label72'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Messages <small>Notification</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Following:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label73" id="label73" value="<?php echo $listing["label73"]; ?>"/>
                            <?php echo form_error('label73'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Creation:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label74" id="label74" value="<?php echo $listing["label74"]; ?>"/>
                            <?php echo form_error('label74'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Review Submission:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label75" id="label75" value="<?php echo $listing["label75"]; ?>"/>
                            <?php echo form_error('label75'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Link Option By Admin:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label95" id="label95" value="<?php echo $listing["label95"]; ?>"/>
                            <?php echo form_error('label95'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>No Records:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label76" id="label76" value="<?php echo $listing["label76"]; ?>"/>
                            <?php echo form_error('label76'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Options <small>Acheter une options</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Buy This Option:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label77" id="label77" value="<?php echo $listing["label77"]; ?>"/>
                            <?php echo form_error('label77'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Add to Cart:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label78" id="label78" value="<?php echo $listing["label78"]; ?>"/>
                            <?php echo form_error('label78'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Added to Cart:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label79" id="label79" value="<?php echo $listing["label79"]; ?>"/>
                            <?php echo form_error('label79'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Free Event Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label80" id="label80" value="<?php echo $listing["label80"]; ?>"/>
                            <?php echo form_error('label80'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Premium Event Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label81" id="label81" value="<?php echo $listing["label81"]; ?>"/>
                            <?php echo form_error('label81'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Options <small>Gestions des options</small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <!-- <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Headings:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Name:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label82" id="label82" value="<?php echo $listing["label82"]; ?>"/>
                              <?php echo form_error('label82'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Date of Purchase:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label83" id="label83" value="<?php echo $listing["label83"]; ?>"/>
                              <?php echo form_error('label83'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Activate:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label84" id="label84" value="<?php echo $listing["label84"]; ?>"/>
                              <?php echo form_error('label84'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Duration Bought:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label85" id="label85" value="<?php echo $listing["label85"]; ?>"/>
                              <?php echo form_error('label85'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Action:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label86" id="label86" value="<?php echo $listing["label86"]; ?>"/>
                              <?php echo form_error('label86'); ?>
                            </div>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Suggestion Text of Buttons:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Activate:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label94" id="label94" value="<?php echo $listing["label94"]; ?>"/>
                              <?php echo form_error('label94'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Activated:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label87" id="label87" value="<?php echo $listing["label87"]; ?>"/>
                              <?php echo form_error('label87'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>View:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label97" id="label97" value="<?php echo $listing["label97"]; ?>"/>
                              <?php echo form_error('label97'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Edit:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label88" id="label88" value="<?php echo $listing["label88"]; ?>"/>
                              <?php echo form_error('label88'); ?>
                            </div>
                          </div>                          
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Delete:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label90" id="label90" value="<?php echo $listing["label90"]; ?>"/>
                              <?php echo form_error('label90'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Receipt:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label96" id="label96" value="<?php echo $listing["label96"]; ?>"/>
                              <?php echo form_error('label96'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Download Receipt:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label91" id="label91" value="<?php echo $listing["label91"]; ?>"/>
                              <?php echo form_error('label91'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Expired:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label92" id="label92" value="<?php echo $listing["label92"]; ?>"/>
                              <?php echo form_error('label92'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Create Event:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label93" id="label93" value="<?php echo $listing["label93"]; ?>"/>
                              <?php echo form_error('label93'); ?>
                            </div>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>View Event:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label89" id="label89" value="<?php echo $listing["label89"]; ?>"/>
                              <?php echo form_error('label89'); ?>
                            </div>
                          </div>                          
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Mes Documents <small></small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Mandate Table Heading:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label98" id="label98" value="<?php echo $listing["label98"]; ?>"/>
                            <?php echo form_error('label98'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Mandate Reference:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label99" id="label99" value="<?php echo $listing["label99"]; ?>"/>
                            <?php echo form_error('label99'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Mandate State:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label100" id="label100" value="<?php echo $listing["label100"]; ?>"/>
                            <?php echo form_error('label100'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Date Signed:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label101" id="label101" value="<?php echo $listing["label101"]; ?>"/>
                            <?php echo form_error('label101'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Action:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label102" id="label102" value="<?php echo $listing["label102"]; ?>"/>
                            <?php echo form_error('label102'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>View:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label103" id="label103" value="<?php echo $listing["label103"]; ?>"/>
                            <?php echo form_error('label103'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Download Reference:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label104" id="label104" value="<?php echo $listing["label104"]; ?>"/>
                            <?php echo form_error('label104'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Update Back Account:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label105" id="label105" value="<?php echo $listing["label105"]; ?>"/>
                            <?php echo form_error('label105'); ?>
                          </div>
                        </div>                        
                        <hr/>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Documents table Heading:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label106" id="label106" value="<?php echo $listing["label106"]; ?>"/>
                            <?php echo form_error('label106'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Document Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label107" id="label107" value="<?php echo $listing["label107"]; ?>"/>
                            <?php echo form_error('label107'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Upload Date:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label108" id="label108" value="<?php echo $listing["label108"]; ?>"/>
                            <?php echo form_error('label108'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Action:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label109" id="label109" value="<?php echo $listing["label109"]; ?>"/>
                            <?php echo form_error('label109'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Download Document:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label110" id="label110" value="<?php echo $listing["label110"]; ?>"/>
                            <?php echo form_error('label110'); ?>
                          </div>
                        </div>
                        <hr/>
                        <div class="x_title">
                          <h2>Create Event <small></small></h2>                   
                          <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label35" id="label35" value="<?php echo $listing["label35"]; ?>"/>
                            <?php echo form_error('label35'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Type:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label36" id="label36" value="<?php echo $listing["label36"]; ?>"/>
                            <?php echo form_error('label36'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Start Publishing :</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label37" id="label37" value="<?php echo $listing["label37"]; ?>"/>
                            <?php echo form_error('label37'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event End Publishing:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label38" id="label38" value="<?php echo $listing["label38"]; ?>"/>
                            <?php echo form_error('label38'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Start Date:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label39" id="label39" value="<?php echo $listing["label39"]; ?>"/>
                            <?php echo form_error('label39'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event End Date:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label40" id="label40" value="<?php echo $listing["label40"]; ?>"/>
                            <?php echo form_error('label40'); ?>
                          </div>
                        </div>                        
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Number Of Persons:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label41" id="label41" value="<?php echo $listing["label41"]; ?>"/>
                            <?php echo form_error('label41'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Price:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label42" id="label42" value="<?php echo $listing["label42"]; ?>"/>
                            <?php echo form_error('label42'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Address:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label43" id="label43" value="<?php echo $listing["label43"]; ?>"/>
                            <?php echo form_error('label43'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event Images:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label59" id="label59" value="<?php echo $listing["label59"]; ?>"/>
                            <?php echo form_error('label59'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Event details:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label44" id="label44" value="<?php echo $listing["label44"]; ?>"/>
                            <?php echo form_error('label44'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Submit Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label60" id="label60" value="<?php echo $listing["label60"]; ?>"/>
                            <?php echo form_error('label60'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Close Button:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input class="form-control" name="label61" id="label61" value="<?php echo $listing["label61"]; ?>"/>
                            <?php echo form_error('label61'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Edit Event:</label>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Update Event:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label63" id="label63" value="<?php echo $listing["label63"]; ?>"/>
                              <?php echo form_error('label63'); ?>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-6">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Back:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="label64" id="label64" value="<?php echo $listing["label64"]; ?>"/>
                              <?php echo form_error('label64'); ?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <button type="submit" id="catsub" class="btn btn-success">Update</button>
                            <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>                          
                            <div id="catupmsg">
                            </div>
                          </div>
                        </div>
                      </form>
                      <?php                      
                    }
                    ?>
                    
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content