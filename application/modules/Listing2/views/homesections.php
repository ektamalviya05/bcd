<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $heading; ?>            
                    <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a>
                </h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($details)){                      
                      if(in_array($which, array(5,7,9,2))){
                        foreach($details as $art){ }
                        $content_id = $art['content_id'];
                      ?>                    
                      <form id="updateArticals" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/updatehomeSection/$which"); ?>' data-parsley-validate class="form-horizontal form-label-left">                      
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" value="<?php echo $art['section']; ?>" class="form-control" name="title" placeholder="Title" />
                            <?php echo form_error('title'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <textarea class="form-control summernote" name="template" id="template"><?php echo $art['content']; ?></textarea>
                            <?php echo form_error('template'); ?>
                          </div>
                        </div>
                        <?php
                        if($which == 5){
                          ?>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Video Type:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="radio" class="" name="vdtype" value="1" <?php if($art['video_type'] == 1){ echo "checked";} ?>/> Youtube video
                              <input type="radio" class="" name="vdtype" value="2" <?php if($art['video_type'] == 2){ echo "checked";} ?>/> Upload video
                            </div>
                          </div>
                          <div class="form-group uploadvideo" style="<?php if($art['video_type'] == 1) echo "display:none;"; ?>">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Upload Video:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="about_video" />                              
                            </div>
                          </div>
                          <div class="form-group youtube" style="<?php if($art['video_type'] == 2) echo "display:none;"; ?>">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Youtube URL:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" class="form-control" name="youtubeurl" value="<?php echo $art['background_img_path']; ?>" placeholder="Enter youtube url" />                              
                            </div>
                          </div>
                          <?php
                        }
                        ?>
                        <?php
                        if($which == 7 || $which == 9){
                          ?>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Background Image:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="backgroundimage" />                              
                            </div>
                          </div>
                          <?php
                        }
                        ?>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <button type="submit" class="btn btn-success">Update</button>
                            <div><?php echo $this->session->flashdata("error"); echo $this->session->flashdata("success"); ?></div>                          
                          </div>
                        </div>
                      </form>
                      <?php
                      }
                      else if($which == 6){                        
                        ?>
                        <div class="x_title">
                          <h4> Change Background Image<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-7" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>                          
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub7" class="btn btn-success">Update</button>
                              <div id="sec7msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Section 1<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-1" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub1" class="btn btn-success">Update</button>
                              <div id="sec1msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Section 2<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-2" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details1['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details1['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub2" class="btn btn-success">Update</button>
                              <div id="sec2msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Section 3<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-3" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details2['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details2['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub3" class="btn btn-success">Update</button>
                              <div id="sec3msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Section 4<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-4" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details3['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details3['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub4" class="btn btn-success">Update</button>
                              <div id="sec4msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Section 5<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-5" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details4['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details4['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub5" class="btn btn-success">Update</button>
                              <div id="sec5msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Section 6<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section6-6" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details5['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details5['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub6" class="btn btn-success">Update</button>
                              <div id="sec6msg"></div>
                            </div>
                          </div>
                        </form>
                        <?php
                      }
                      else if($which == 3){
                        ?>
                        <div class="x_title">
                          <h4> Promotional Section 1<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section3-1" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub31" class="btn btn-success">Update</button>
                              <div id="sec31msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Promotional Section 2<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section3-2" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details1['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details1['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub32" class="btn btn-success">Update</button>
                              <div id="sec32msg"></div>
                            </div>
                          </div>
                        </form>
                        <hr>
                        <div class="x_title">
                          <h4> Promotional Section 3<small></small></h4>                   
                          <div class="clearfix"></div>
                        </div>
                        <form id="section3-3" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="file" class="form-control" name="secimg" placeholder="Icon" />
                              <?php echo form_error('secimg'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input type="text" value="<?php echo $details2['section']; ?>" class="form-control" name="title" placeholder="Title" />
                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea name="template1" class="form-control" placeholder="Description"><?php echo $details2['content']; ?></textarea>                              <?php echo form_error('title'); ?>
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="sub33" class="btn btn-success">Update</button>
                              <div id="sec33msg"></div>
                            </div>
                          </div>
                        </form>
                        <?php
                      }
                      else if($which == "SocialLinks"){
                        ?>                        
                        <form id="updateSociallinks" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Facebook:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="facebook" placeholder="Facebook Link" value="<?php echo $details['sc_links']; ?>" type="text">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Twitter:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="twitter" placeholder="Twitter Link" value="<?php echo $details1['sc_links']; ?>" type="text">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Instagram:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <input class="form-control" name="instagram" placeholder="Instagram Link" value="<?php echo $details2['sc_links']; ?>" type="text">
                            </div>
                          </div>
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="socialsub" class="btn btn-success">Update</button>
                              <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span> 
                              <div id="facebookmsg"></div>
                            </div>
                          </div>
                        </form>
                        <?php
                      }else if($which == "purchaseReceipt"){
                        ?>
                        <script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
                        <form id="updatePurchaseReceipt" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                          <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <label>Receipt Format:</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                              <textarea class="form-control" name="receipt" placeholder="Receipt Page"><?php echo $details['content']; ?></textarea>
                            </div>
                          </div>                          
                          <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                              <button type="submit" id="socialsub" class="btn btn-success">Update</button>
                              <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span> 
                              <div id="facebookmsg"></div>
                            </div>
                          </div>
						  <div id="successmsg"></div>
							<p>%%order_id%%</p>

							<p>%%purchase_date%%</p>

							<p>%%amount%%</p>

							<p>%%description%%</p>

							<p>%%option_name%%</p>

							<p>%%option_duration%%</p>
						  
                        </form>
                        <script type="text/javascript">
                            CKEDITOR.replace( 'receipt', {
                                  language: 'en',
                                  uiColor: '#9AB8F3',
                                  allowedContent:true,
                              });
                        </script>
                        <?php
                      }
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->
        <script type="text/javascript">
        	CKEDITOR.replace( 'template', {
			    language: 'en',
			    uiColor: '#9AB8F3',
			    allowedContent:true,
			});
        </script>