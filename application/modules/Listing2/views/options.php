<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h2>Distance Limit Setting</h2>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
</div>
<div class="right_col" role="main">
  <div class="">
    <div class="page-title">
      <div class="">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_content">
              <span id="shownumberexits" data-dismiss="modal" data-toggle="modal" data-target="#myModalDistance"></span>
              <?php
              if( trim($status) == "Ok")
              {
              ?>
              <script type="text/javascript">
              $(document).ready(function() {
              $("#shownumberexits").trigger("click");
              });
              </script>
              <?php
              }
              ?>
              <form class="form-horizontal" action="<?php echo base_url()?>Content/setSearchDistanceLimit" method="post" enctype="multipart/form-data">
                <div class="form-group">
                  <label for="distancelimit">Select Limit:</label>
                  <div class="th-sele">
                    <select class="form-control" id="distancelimit" name="distancelimit" required="">
                      <option value="">Select Search Limit (KM)</option>
                      <option value="5" <?php echo $distance ==5? 'selected="selected"':'' ?> >5</option>
                      <option value="10" <?php echo $distance ==10? 'selected="selected"':'' ?>>10</option>
                      <option value="15" <?php echo $distance ==15? 'selected="selected"':'' ?>>15</option>
                      <option value="20" <?php echo $distance ==20? 'selected="selected"':'' ?>>20</option>
                      <option value="50" <?php echo $distance ==50? 'selected="selected"':'' ?>>50</option>
                      <option value="100" <?php echo $distance ==100? 'selected="selected"':'' ?>>100</option>
                      <option value="150" <?php echo $distance ==150? 'selected="selected"':'' ?>>150</option>
                      <option value="200" <?php echo $distance ==200? 'selected="selected"':'' ?>>200</option>                    
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <input class="btn btn-success" type="submit" name="limitSubmit" value="Save" />
                </div>  
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>