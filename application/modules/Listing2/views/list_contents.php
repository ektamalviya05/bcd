<p>&nbsp;</p>
<h1>Content Management System</h1>
<hr>
<script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script>
 <a class="btn btn-sx btn-success" href="<?php echo base_url();?>Content/addcontent">Add Content</a>
 <a href="<?php echo base_url("Content/editReadMore/personal") ?>" class="btn btn-sx btn-primary"><i class="fa fa-pencil"></i> Personal Account Article</a>
 <a href="<?php echo base_url("Content/editReadMore/professional") ?>" class="btn btn-sx btn-primary"><i class="fa fa-pencil"></i> Professional Account Article</a>
 <div class="clearfix"></div>
 <br>
<?php


echo "<table class='table table-striped table-bordered'><tr>";
$bur = base_url();
echo "<tr>";
echo "<th>Section</th>";
echo "<th>Sub Section</th>";
echo "<th>Content</th>";
echo "<th>File Path</th>";
echo "<th>Action</th>";
echo "</tr>";

//if($record_found>0)
//{
foreach ($contlist as $rows) 
{

echo "<tr>";
echo "<td>".$rows['section']."</td>";
echo "<td>".$rows['sub_section']."</td>";
echo "<td>".$rows['content']."</td>";
echo "<td>".$rows['uploaded_file_path']."</td>";
echo "<td><a href='". $bur."Content/editcont/".$rows['content_id']."'>Edit</a>";
/*echo ' | <a  data-toggle="confirmation" data-title="Are you sure to delete?" href="'. $bur.'content/deletecont/'.$rows['content_id'] .'" target="_self">Delete</a></td> ';*/
}
//}
//else
//{
  //echo "<tr><td colspan='7'>No Record Found</td></tr>";
//}
echo "</table>";
if(isset($links) && !empty($links))
{
  echo $links;
}

//print_r($contlist);
?>




<script  type="text/javascript">
$(document).ready(function() { });

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });

function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#userfile").change(function(){
    readURL(this);
});

</script>