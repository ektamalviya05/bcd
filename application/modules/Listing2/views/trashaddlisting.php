<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
                    <div class="import-create dash-counter">
                        <!-- <h2>Create</h2> -->
                        <div class="row">
                          <div class="col-md-3">
                              <div class="dc-sub">
                                  <a href="<?php echo base_url("Content/manualAdvertisement"); ?>">
                                      <div class="dc-sub-ico">
                                          <i class="fa fa-file-text-o" aria-hidden="true"></i>
                                      </div>
                                      <div class="dc-sub-txt pu-txt">
                                          <h3>View</h3>
                                          <p>Advertisement Listing</p>
                                      </div>
                                  </a>
                              </div>
                          </div>                            
                        </div>
                    </div>


                  <!--  Main Containt  -->
                    <div class="dash-counter users-main">
                        <h2>Trash Page List</h2>
                        <div class="user-table table-responsive">
                          <table class="table table-striped table-bordered">
                            <tbody>
                              <tr></tr>
                              <tr>
                                <th>No.</th>
                                <th>Image</th>
                                <th>Redirect Link</th>                                
                                <th>Date Created</th>
                                <th>Action</th>
                              </tr>
                              <?php
                              $count = 1;
                              foreach($details as $pages){
                              ?>
                              <tr>
                                <td><?php echo $count; ?></td>
                                <td><img src="<?php echo base_url('assets/img/advertise/').$pages['add_img_path']; ?>" height="100px" width="100px"/></td>
                                <td><?php echo $pages['add_url']; ?></td>                                                                                             
                                <td><?php echo $pages['add_date']; ?></td>
                                <td>
                                  <div class="link-del-view">
                                      <?php $pg_id = $pages['add_id']; ?>                                      
                                      <div class="tooltip-2"><a href="javascript:;" class="trashback-advertisement" modal-aria="<?php echo $pg_id; ?>"><i class="fa fa-reply" aria-hidden="true"></i></a>
                                        <span class="tooltiptext">Restore</span>
                                      </div>
                                      <div class="tooltip-2"><a href="javascript:;" class="trashback-publishadvertisement" modal-aria="<?php echo $pg_id; ?>"><i class="fa fa-window-restore" aria-hidden="true"></i></a>
                                        <span class="tooltiptext">Restore & Publish</span>
                                      </div>                                                                          
                                  </div>
                                </td>
                              </tr>
                              <?php
                                $count++;
                              }
                              ?>
                            </tbody>
                          </table>
                        </div>                        
                    </div>                    
                </div>


