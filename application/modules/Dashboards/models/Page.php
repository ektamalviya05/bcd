<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{

   public function __construct() {
        $this->userTbl = 'users';
        $this->userTbl1 = 'otd_page_contents';
        $this->userTblSubs = 'subscribers';
        $this->testimonial = "otd_testimonials";
        $this->contentTbl = "site_contents";
        $this->catTbl = "otd_business_category";
        $this->bannerTbl = "otd_home_banner";
        $this->userCatTbl = "otd_user_business_category";
        $this->userHomeCatTbl = "otd_home_cat_tab";
        $this->addTbl = "otd_advertisement";
        $this->pageHeadTbl = "otd_page_diff_lang_content";
        $this->ftTbl = "otd_footer_content";
        $this->pytTbl = "otd_user_option_payment";
        $this->usOptTbl = "otd_user_option";
        $this->usOptMstr = "otd_option_master";
        $this->eventTbl = "otd_events";
        $this->followTbl = "otd_business_follower";
        $this->notiTbl = "otd_notifications";
        $this->businessTbl = 'otd_business_details';
        $this->eventTypeTbl = "otd_event_type";
        $this->emailTpTbl = "otd_email_templates";
        $this->userHomeCatListTbl = "bdc_business";
    }


    public function getSearchRows1($params = array())
    {   
        $lat="48.8566140";
        $lng = "2.3522220";        
        if(array_key_exists("cur_latlng",$params))
        {            
            if(empty($params['cur_latlng'][0])){
                $lat="48.8566140";
                $lng = "2.3522220";
            }else{
                $lat= $params['cur_latlng'][0];
                $lng = $params['cur_latlng'][1];
            }
        }
        $now = date("Y-m-d");
        $mnthday = date("d");
        $this->db->select("SQL_CALC_FOUND_ROWS null as rows,
                          users.user_id AS user_id,
                          users.user_firstname AS user_firstname,
                          users.user_lastname AS user_lastname,
                          users.user_phone AS user_phone,
                          users.user_company_name AS user_company_name,
                          users.user_company_number AS user_company_number,
                          users.user_password AS user_password,                          
                          users.user_type AS user_type,
                          users.user_email AS user_email,
                          users.user_profile_pic AS user_profile_pic,
                          otd_business_details.bs_address AS user_company_address,
                          catview.mycategory_name as categories,
                          users.user_lat AS user_lat,
                          users.user_long AS user_long,
                          (select sum(blog_vote) from profile_review where voted_user_id = users.user_id AND vote_status=1 AND status=1) AS totalvotesum,
                          (select count(*) from profile_review where voted_user_id = users.user_id AND vote_status=1 AND status=1) AS totalvoter,
                          group_concat(if(otd_user_business_category.type= 1, category_name, null) separator ', ') AS mycategories,
                          group_concat(category_id separator ',') AS cate_id,
                          (CASE 
                            WHEN (SELECT COUNT(*) FROM $this->usOptTbl where opt_user_id = $this->userTbl.user_id AND opt_option_id = 1 AND opt_option_status=1 AND ((opt_option_active_date <= '$now' AND opt_option_end_date >= '$now') OR ( opt_mnth_strt_day <= $mnthday AND opt_mnth_end_day >= $mnthday ) ) )  > 0 
                            THEN 1
                            ELSE 0 END) as atTheTop,
                          round(( 6373 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) )
                          * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
                          * sin( radians(user_lat)))), 2) AS distance", FALSE);
        // round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) )
        //                   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
        //                   * sin( radians(user_lat)))), 2) AS distance
        $this->db->from("$this->userTbl");
        $this->db->join("$this->businessTbl", "$this->businessTbl.bs_user = $this->userTbl.user_id");
        $this->db->join("$this->userCatTbl", "$this->userCatTbl.user_id = $this->userTbl.user_id", "left");  
        $this->db->join("catview", "catview.user_id = users.user_id");    

        $distance = $params['distance'];
        if(array_key_exists("search_keyword", $params)){            
            $keyword = $params['search_keyword'];
            $keyword =  $this->escape_str($keyword);
           
            // $array = array("user_firstname"=>$keyword,
            //                "user_lastname"=>$keyword,
            //                "user_company_name"=>$keyword,
            //                "user_company_address"=> $keyword
            //              );
            // if(!array_key_exists("search_category", $params)){
            //     $array["$this->userCatTbl.category_name"] = $keyword;
            // }
            // $this->db->or_like($array);
            $str = "`user_firstname` LIKE '%$keyword%' ESCAPE '!' OR `user_lastname` LIKE '%$keyword%' ESCAPE '!' OR `user_company_name` LIKE '%$keyword%' ESCAPE '!' OR `user_company_address` LIKE '%$keyword%' ESCAPE '!'";
            if(!array_key_exists("search_category", $params)){
              $str .= "OR $this->userCatTbl.category_name LIKE '%$keyword%' ESCAPE '!'";
            }
            $this->db->where("($str)");
        }
        if(array_key_exists("search_extrafilter", $params)){
          // $flt = array();
          // foreach($params['search_extrafilter'] as $extflt){
          //     $flt["$this->userCatTbl.category_name"] = $extflt;
          // }
          // $this->db->or_like($flt);            
          $flt = "";
          $cnt = 1;
          foreach($params['search_extrafilter'] as $extflt){
            if($cnt == 1){
              $flt = "$this->userCatTbl.category_name LIKE '%$extflt%' ESCAPE '!' ";
            }else{
              $flt .= "OR $this->userCatTbl.category_name LIKE '%$extflt%' ESCAPE '!' ";
            }
            $cnt++;
          }
          if(array_key_exists("search_category",$params)){
            $cat = $params['search_category'];
            $flt .= "OR $this->userCatTbl.category_name LIKE '%$cat%' ESCAPE '!' ";
          }
          $this->db->where("($flt)");
         
        }
        if(!array_key_exists("search_extrafilter", $params) && array_key_exists("search_category",$params)){
            $cat = $params['search_category'];
            $this->db->like("$this->userCatTbl.category_name", $cat);            
        }
       // $this->db->where("`otd_user_business_category`.`type` = 1");        
        $this->db->group_by("users.user_id");
        $this->db->having("distance <=", $params['distance']);
        
        // if(array_key_exists("user_company_address",$params))        
        //     $this->db->or_like("$this->userTbl.user_company_address", $params['user_company_address']);
        
        
        if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit'],$params['start']);
            
        }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
            $this->db->limit($params['limit']);
        }
        $this->db->order_by("atTheTop", "DESC");
        $this->db->order_by("distance", "ASC");
        $query = $this->db->get();

        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $query->num_rows();
        }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
            $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        $return['result'] = $result;
        //  echo $this->db->last_query();
        //  echo "<pre>";
        //  print_r($result);
        //  echo "</pre>";
        //   die();
        $return ['count'] = $this->db->query('SELECT FOUND_ROWS() count;')->row()->count;
        return $return;
    }

    public function getALaUneProfiles1($params = array()){
        $lat="48.8566140";
        $lng = "2.3522220";        
        if(array_key_exists("cur_latlng",$params))
        {
            if(empty($params['cur_latlng'][0])){
            }else{
                $lat= $params['cur_latlng'][0]; 
                $lng = $params['cur_latlng'][1];
            }
        }        
        $this->db->select("users.user_id AS user_id,
                          users.user_firstname AS user_firstname,
                          users.user_lastname AS user_lastname,
                          users.user_company_name AS user_company_name,
                          users.user_profile_pic AS user_profile_pic,
                          otd_business_details.bs_address AS user_company_address,
                          users.user_lat AS user_lat,
                          users.user_long AS user_long,
                          (select COUNT(*) FROM otd_recommend_merchant where recommended_user_id= users.user_id) AS total_recommand,
                          (select COUNT(*) FROM profile_review where voted_user_id = users.user_id AND vote_status=1 AND status=1) AS total_review,                          
                          round(( 6373 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) )
                          * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
                          * sin( radians(user_lat)))), 2) AS distance,
                          round(( 6373 * acos( cos( radians(otd_user_option.opt_city_lat) ) * cos( radians( ".$lat." ) )
                          * cos( radians(".$lng.") - radians(otd_user_option.opt_city_long)) + sin(radians(otd_user_option.opt_city_lat)) 
                          * sin( radians(".$lat.")))), 2) AS citydistance,
                          group_concat(category_id separator ',') AS CategoryId");
        $this->db->from($this->usOptTbl);
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id");
        $this->db->join("$this->userCatTbl", "$this->userCatTbl.user_id = $this->userTbl.user_id", "left");
        $this->db->where(array("opt_option_active_date <="=>date("Y-m-d"), "opt_option_end_date >="=>date("Y-m-d"), "opt_option_status"=>1, "opt_option_id"=>2));
        $this->db->group_by("user_id");
        $this->db->limit(10);
        $distance = $params['distance'];
        if(array_key_exists("search_keyword", $params)){            
            $keyword = $params['search_keyword'];
            $keyword =  $this->escape_str($keyword);
            // $array = array("user_firstname"=>$keyword,
            //                "user_lastname"=>$keyword,
            //                "user_company_name"=>$keyword,
            //                "user_company_address"=> $keyword
            //              );
            // if(!array_key_exists("search_category", $params)){
            //     $array["$this->userCatTbl.category_name"] = $keyword;
            // }
            // $this->db->or_like($array);
            $str = "`user_firstname` LIKE '%$keyword%' ESCAPE '!' OR `user_lastname` LIKE '%$keyword%' ESCAPE '!' OR `user_company_name` LIKE '%$keyword%' ESCAPE '!' OR `user_company_address` LIKE '%$keyword%' ESCAPE '!'";
            if(!array_key_exists("search_category", $params)){
              $str .= "OR $this->userCatTbl.category_name LIKE '%$keyword%' ESCAPE '!'";
            }
            $this->db->where("($str)");
        }
        if(array_key_exists("search_extrafilter", $params)){
          // $flt = array();
          // foreach($params['search_extrafilter'] as $extflt){
          //     $flt["$this->userCatTbl.category_name"] = $extflt;
          // }
          // $this->db->or_like($flt);
          $flt = "";
          $cnt = 1;
          foreach($params['search_extrafilter'] as $extflt){
            if($cnt == 1){
              $flt = "$this->userCatTbl.category_name LIKE '%$extflt%' ESCAPE '!' ";
            }else{
              $flt .= "OR $this->userCatTbl.category_name LIKE '%$extflt%' ESCAPE '!' ";
            }
            $cnt++;
          }
          if(array_key_exists("search_category",$params)){
            $cat = $params['search_category'];
            $flt .= "OR $this->userCatTbl.category_name LIKE '%$cat%' ESCAPE '!' ";
          }
          $this->db->where("($flt)");
        }
        if(!array_key_exists("search_extrafilter", $params) && array_key_exists("search_category",$params)){
            $cat = $params['search_category'];
            $this->db->like("$this->userCatTbl.category_name", $cat);            
        }

        $this->db->order_by("users.user_id", "RANDOM");
        $this->db->having("distance <= $distance OR citydistance <= $distance");
        $result = $this->db->get()->result_array();
        return $result;
    }


    function escape_str($str) 
    {
        
    if (function_exists('mysqli_real_escape_string') AND is_resource($this->db->conn_id))
    {
    return mysqli_real_escape_string($this->db->conn_id, $str);
    }
    elseif (function_exists('mysql_escape_string'))
    {
    return mysql_escape_string($str);
    }
    else
    {
    return addslashes($str);
    }
    }


    public function getALaUNEProfiles($params = array())
    {

        $lat="48.8566140";                                                                                                                                                                  
        $lng = "2.3522220";
        $dist_range =  -1;
        $searchedcity = "";

        if(array_key_exists("cur_latlng",$params))
            {
                $lat= $params['cur_latlng'][0]; 
                $lng = $params['cur_latlng'][1];
        }
     


        $dist = "round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
                   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
                   * sin( radians(user_lat)))), 2) AS distance";
  
        $dist2 = "(round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
                   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
                   * sin( radians(user_lat)))), 2)) ";
   

        $sql_in = "SELECT opt_user_id FROM `otd_user_option` uo left join `otd_option_master` om on uo.opt_option_id = om.opt_id  where (uo.opt_option_status =1 and DATE(opt_option_end_date) > DATE(NOW())) and om.opt_id in (2, 3)";

        $sql_main = "select *, ".$dist."  from vv_users_list where user_type = 'Professional' and user_id in (".$sql_in.") ";
        
        if(array_key_exists("searchedcity", $params))
        {
            $searchedcity = $params['searchedcity'];
        }



        if(array_key_exists("user_company_address", $params))
        {
			   
              $sql_main = "select *, ".$dist." from vv_users_list where user_type = 'Professional' and (user_company_address like '%".$this->db->escape_str($params['user_company_address']) ."%' or user_company_address like '%".$searchedcity ."%' )"  ;

        }
        $sql_main .= " and user_id in (".$sql_in.") ";

        $sql="";
        if(array_key_exists("conditions",$params))
        {
            $sql =" and (";
            foreach ($params['conditions'] as $key => $value) 
            {
                
                if($key == 'Categories' || $key == 'categories')
                {
                $sql .= $key. " like '%". $value."%' Or " ;    
                }
                else
                {
                    $sql .= $key. " like '%". $value."%' Or " ;
                }
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
        }

        if(array_key_exists("extraFilter",$params))
        {
             if($sql != "")
               {
           
             $sql .=" Or ( ";
           
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
            }
            else
            {
            $sql =" and (";
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";


            }


        }
      

         if(array_key_exists("keyword",$params))
         {
            if($params['keyword'] != "")
            {
               if($sql != "")
               {
                 $sql .=" Or ( ";
                 $fields = $this->db->list_fields('vv_users_list');
                    foreach ($fields as $field)
                    {
                        $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                    }
                    $sql = rtrim($sql,"Or ");
                    $sql .= ") ";

                }
                else
                {
                $sql =" And (";
                $fields = $this->db->list_fields('vv_users_list');
                foreach ($fields as $field)
                    {
                      $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                     }
                $sql = rtrim($sql,"Or ");
                $sql .= ") ";
                }
            }
        }

      

        $ordby = " order by  rand() limit 10";
     
  
        
            $fullQuery = $sql_main. $sql. $ordby;

            $query = $this->db->query($fullQuery);
            
            //print_r($params);

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        //print_r($result);
        //echo $this->db->last_query();
        //echo "<br><br>SQL: ".$fullQuery;
        //die();
        return $result;
    }



     
    public function getSearchRows($params = array())
    {

        $lat="48.8566140";
        $lng = "2.3522220";
        //$start = 0;
        //$limit = 1;
   
        $dist_range =  is_null($params['distance'])?50: $params['distance'];  
  
 
        if(array_key_exists("cur_latlng",$params))
        {
                    
                   $lat= $params['cur_latlng'][0]; 
                  $lng = $params['cur_latlng'][1];
        }
     
 

        $dist = "round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
           * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
           * sin( radians(user_lat)))), 2) AS distance";
          
                $dist2 = "(round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
           * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
           * sin( radians(user_lat)))), 2)) ";
   

        $sql_main = "select *, ".$dist."  from distinct_allusers where user_type = 'Professional' and status !=3 ";
        
        if(array_key_exists("user_company_address", $params))
        {
			     
                 $user_company_address = str_replace("'","",$params['user_company_address']);
				 
				  
                 $var =  explode(" ",$user_company_address);
 			     if(!empty($var)){
					
			      for($i = 0 ; $i < count($var) ; $i++) {
			       $totalor = count($var);
				   
				  $value = $var[$i];
				  $value1  = $value;
				  if($i==0){
					   if($totalor>1){
						    $like =" ( (".$dist2. " < ".$dist_range."  or head = 1) and ( user_company_address  like '%". $value1."%' ";
					   }else{
						   
						    $like =" ( user_company_address  like '%". $value1 ."%' and  ( ".$dist2. " < ".$dist_range."  or head = 1 ) ) ";
						   
						  
					   }
				     
					  
				  }else{
						$like .="OR user_company_address like '%". $value1."%' ";			  
				  }
				  
				  }
					   if($totalor>1){
						    $like .=" )) "; 
					   } 			  
				  
                  
			     }else{
					  
			          $like =" ( user_company_address  like '%". $user_company_address."%' and   and  ".$dist2. " < ".$dist_range."  or head = 1) ";
			     }
 
                   $sql_main = "select *, ".$dist." from distinct_allusers where user_type = 'Professional' and status !=3 and $like " ;
 
 
        }

        $sql="";
        if(array_key_exists("conditions",$params))
        {
            $sql =" and (";
            foreach ($params['conditions'] as $key => $value) 
            {
                
                if($key == 'Categories' || $key == 'categories')
                {
                $sql .= $key. " like '%". $value."%' Or " ;    
                }
                else
                {
                    $sql .= $key. " like '%". $value."%' Or " ;
                }
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
        }

        if(array_key_exists("extraFilter",$params))
        {
             if($sql != "")
               {
           
             $sql .=" Or ( ";
           
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
            }
            else
            {
            $sql =" and (";
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";


            }


        }
      

         if(array_key_exists("keyword",$params))
         {
            if($params['keyword'] != "")
            {
               if($sql != "")
               {
                 $sql .=" Or ( ";
                 $fields = $this->db->list_fields('vv_users_list');
                    foreach ($fields as $field)
                    {
                        $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                    }
                    $sql = rtrim($sql,"Or ");
                    $sql .= ") ";

                }
                else
                {
                $sql =" And (";
                $fields = $this->db->list_fields('vv_users_list');
                foreach ($fields as $field)
                    {
                      $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                     }
                $sql = rtrim($sql,"Or ");
                $sql .= ") ";
                }
            }
        }

        if(array_key_exists("start",$params) && array_key_exists("limit",$params))
        {
            $start = $params['start'];
            $limit = $params['limit'];
            $ordby = " OR (".$dist2. " < ".$dist_range."  or head = 1) order by head DESC, distance ASC limit ". $limit. " offset ". $start;
        }
        else
        {
            $ordby = " OR (".$dist2. " < ".$dist_range." or head = 1) order by head DESC, distance ASC ";
        }
  
        
        $fullQuery = $sql_main. $sql. $ordby;

        $query = $this->db->query($fullQuery);
        
        //print_r($params);

        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
            $result = $query->num_rows();
        }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
            $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        //print_r($result);
        //echo $this->db->last_query();
        // echo "<br><br>SQL: ".$fullQuery;
        //die();
        return $result;
    }

    /*
    * get rows from the page table
    */
    function getPageRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userTbl1);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                if($key=='page_url')
                {
                    $this->db->where('page_url', $value);
                    $this->db->or_where('pg_meta_tag', $value);
                }
                else 
                {
                    $this->db->where($key,$value);
                }
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }


    function getExtraFilters(){
      $this->db->select('*');
      $this->db->from('otd_business_category');
      $this->db->where("cat_status", 1);
      $this->db->where("type !=", 1);
      $this->db->order_by("cat_name", "ASC");
      $query = $this->db->get();
      return $query;
    }

    function getExtraFiltersByType($type = 2){
      $this->db->select('*');
      $this->db->from('otd_business_category');
      $this->db->where("cat_status", 1);
      $this->db->where("type", $type);
      $this->db->order_by("cat_name", "ASC");
      $query = $this->db->get()->result_array();
      return $query;
    }


    /*
    * Get testimonials
    */
    function getTestimonials(){
        $query = $this->db->order_by("tst_date", "DESC")->get_where($this->testimonial, array("tst_status"=>1));
        return $query->result_array();
    }

    /**
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
       $address = str_replace(" ", "+", $address);
       $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
       $json = json_decode($json);
       if((isset($json)) && ($json->{'status'} == "OK")){
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
       }else{
         $lat = -25.274398; 
         $long = 133.775136; 
       }       
       $response = array('lat'=>$lat,'lng'=>$long);
       return $response;
    }

function getFollowersCount($user_id)
{
    $sql = "select followed_user_id, count(*) as total_followers from otd_business_follower where followed_user_id = ".$user_id. " group by followed_user_id";
           $query = $this->db->query($sql);

           return $query->num_rows();
}
    function getBusinessCategories(){

        $this->db->select('*');
        $this->db->from('otd_business_category');
        $this->db->where(array("cat_status"=>1, "type"=>1));
        $this->db->group_by("cat_name");
        $this->db->order_by("cat_name", "ASC");
        $query = $this->db->get();
        
        //        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
        
    }

     function getTopRatedCompanies()
    { 
      $sql = "SELECT u.* FROM `vv_users_list`u  where u.toprated = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 3";
      $query = $this->db->query($sql);
      return $query->result_array();
    }

	  function getTopRatedCompanies1(){
 
	   $this->db->select("`u`.`user_id` AS `user_id`,
	        `u`.`user_firstname` AS `user_firstname`,
			`u`.`user_lastname` AS `user_lastname`,
			`u`.`user_phone` AS `user_phone`,
			`u`.`user_gender` AS `user_gender`,
			`u`.`user_dob` AS `user_dob`,
			`u`.`user_address` AS `user_address`,
			`u`.`user_language` AS `user_language`,
			`bs`.`bs_name` AS `user_company_name`,
			`bs`.`bs_comp_number` AS `user_company_number`,
			`bs`.`bs_address` AS `user_company_address`,
			`bs`.`bs_email` AS `user_company_email`,
			`u`.`user_lat` AS `user_lat`,
			`u`.`user_long` AS `user_long`,
			`u`.`user_password` AS `user_password`,
			`u`.`user_type` AS `user_type`,
			`u`.`status` AS `status`,
			`u`.`user_profile_pic` AS `user_profile_pic`,
			`u`.`auth_check` AS `auth_check`,
			`u`.`sms_check` AS `sms_check`,
			`u`.`user_signup_method` AS `user_signup_method`,
			`c`.`category_id` AS `categories`,
			`c`.`type` AS `type`,
			`u`.`publishonhome` AS `publishonhome`,
			`u`.`toprated` AS `toprated`
			 ");
			 $this->db->from("$this->userTbl u");
             $this->db->join("$this->userCatTbl `c`", "`u`.`user_id` = `c`.`user_id`");
             $this->db->join("$this->followTbl fc", "`u`.`user_id` = `fc`.`followed_user_id`","left");
             $this->db->join("$this->businessTbl bs", "`u`.`user_id` = `bs`.`bs_user`","left");
             $this->db->where("u.user_type",'Professional');
             $this->db->where("u.toprated",1);
             $this->db->where("u.status",1);
             $query = $this->db->get();
			 $result = $query->result(); 
			 print_r($result);
 

    }

     function getPublishOnHomeCompanies()
    {

      // $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id");
      // $this->db->order_by("user_id", "RANDOM");
      // $this->db->limit(7);
      // $query = $this->db->get_where($this->userTbl, array("status"=>1, "user_type" => 'Professional', "publishonhome"=>1));
      // return $query->result_array();


      $sql = "SELECT u.* FROM `vv_users_list`u  where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 7";
      $query = $this->db->query($sql);
      return $query->result_array();

    }


    function getArticles($args){
        $query = $this->db->get_where($this->contentTbl, array("content_id"=>$args));
        return $query->row_array();
    }

    /*
    * get categories lists
    */   
    function getCategoryRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->catTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get Banner Images
    */
    function getBannerImages($params = array()){
        $this->db->select('*');
        $this->db->from($this->bannerTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("bn_id",$params)){
            $this->db->where('bn_id',$params['content_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get categories lists
    */   
    function getCategoryUsersRelationRows($params = array()){

        $this->db->select("*, $this->userTbl.user_id AS mainuserid");
        $this->db->join($this->userCatTbl, "$this->userCatTbl.user_id = $this->userTbl.user_id");
        $this->db->from($this->userTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("groupby",$params)){
            $this->db->group_by($params['groupby']);           
        }
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

     /*
    * get rows for home category tabs list
    */
    function getHomeCatgoryTabRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userHomeCatTbl);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get categories lists with corressponding tabs
    */   
    function getCategoryFromTab($params = array()){

        $this->db->select('*');
        $this->db->from($this->catTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        
        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }



    function getDistanceLimit()
    {
$this->db->select("*");
$this->db->from("otd_options");

       $this->db->where("option_name","distance");
        $query = $this->db->get();
            
        $result = $query->row_array();

        return $result;
    }


    /*
    * get rows for google advertisement script
    */
    function getGoogleAdd(){
        $query = $this->db->get_where($this->contentTbl, array("content_id"=>26));
        return $query->row_array();
    }

    /*
    * Get rows from manual advertisement table
    */
    function getManualAddRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->addTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        
        if(array_key_exists("add_id",$params)){
            $this->db->where('add_id',$params['add_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get rows from the page heading content table
    */
    function getHeadContentRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->pageHeadTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
              
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("ct_id",$params)){
            $this->db->where('ct_id',$params['ct_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get heading for content
    */
    public function getHeadings($params= array(), $section="", $get =""){
        foreach($params as $data){
            if(in_array($section, $data))
                return $data[$get];
        }
    }

    /*
    * get rows from text content table
    */
    function getTextContent($args){
        if(!empty($args)){
            return $this->db->get_where($this->ftTbl, array("ft_id"=>$args))->row_array();
        }else{
            return false;
        }
    }

/*
* external function for user purchase
*/
    function updateTransaction($params = array(), $where =array() ){
        $this->db->update($this->pytTbl, $params, $where);
        return true;
    }

    function updateUserOption($params = array(), $where=array()){
        $this->db->update($this->usOptTbl, $params, $where);
        return true;
    }

    function getUserRowsFromOption($args = ""){
        // return $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id")
        //                 ->join($this->usOptMstr, "$this->usOptMstr.opt_id = $this->usOptTbl.opt_option_id")
        //                 ->group_by("opt_user_id")
        //                 ->get_where($this->usOptTbl, array("opt_tran_id"=>$args))->row_array();
        return $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_user_id = $this->userTbl.user_id")
                        ->group_by("opt_user_id")
                        ->get_where($this->userTbl, array("opt_tran_id"=>$args))->row_array();
    }

    function getMasterOption($args = 1){
        return $this->db->get_where($this->usOptMstr, array("opt_id"=>$args))->row_array();
    }

    function getOptionDetails($args){
        return $this->db->join($this->usOptMstr, "$this->usOptMstr.opt_id = $this->usOptTbl.opt_option_id")
                        ->get_where($this->usOptTbl, array("opt_tran_id"=>$args))->result_array();
    }

    /*
    * Insert Business events 
    */
    public function updateEvents($params = array(),$user = Null, $where = 1, $mailcontent){
        if(!empty($user)){
            $this->db->update($this->eventTbl, $params, array("event_id"=>$where));
            if($this->db->affected_rows() > 0){            
                $id = $where;                
                $query = $this->db->select("user_id, user_email")
                                  ->join($this->userTbl, "$this->userTbl.user_id = $this->followTbl.follower_user_id")
                                  ->get_where($this->followTbl,array('followed_user_id'=>$user));
                foreach($query->result() as $userid){
                    $this->db->insert($this->notiTbl, array("nt_by"=>$user, "nt_to"=>$userid->user_id, "nt_type"=>2, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s"), "nt_rlt_id"=>$id));
                    $emails[] = $userid->user_email;
                }
                $fromemail = MASTER_EMAIL;
                $fromname = "YellowPages";                
                $this->email->from($fromemail, $fromname);                
                $this->email->subject("Event Alert");
                $this->email->message($mailcontent);
                if(!empty($emails)){
                    $this->email->bcc($emails);
                }
                $x = $this->email->send();                
                return $id;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }

    /*
    * Get row of event with Full Details
    */
    function getEventsRows($params = array()){
        if(!empty($this->session->userdata("locat"))){
            $latlng = $this->get_lat_long($this->session->userdata("locat"));
            $lat=$latlng['lat'];
            $lng = $latlng['lng'];
            $dist = "( 3959 * acos( cos( radians($lat) ) * cos( radians( event_lat ) ) 
                    * cos( radians(event_long) - radians($lng)) + sin(radians($lat)) 
                    * sin( radians(event_lat)))) AS distance";
            $this->db->select("*, $dist");
        }else{
           $this->db->select('*');
        }
       $this->db->from($this->eventTbl);
       $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->eventTbl.created_by_user");
       $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
       $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_event_id = $this->eventTbl.event_id");
       $this->db->join($this->eventTypeTbl, "$this->eventTypeTbl.evt_type_id = $this->eventTbl.event_type_id");
       //fetch data by conditions        
        if(!empty($this->session->userdata("keyword"))){
            $keyword  = $this->session->userdata("keyword");
            $this->db->where("(event_name LIKE '%".$keyword."%' ESCAPE '!' OR event_description LIKE '%".$keyword."%' ESCAPE '!' OR `event_address` LIKE '%".$keyword."%' ESCAPE '!' OR `bs_name` LIKE '%".$keyword."%' ESCAPE '!' OR `user_firstname` LIKE '%".$keyword."%' ESCAPE '!' OR `user_lastname` LIKE '%".$keyword."%' ESCAPE '!' OR `user_email` LIKE '%".$keyword."%' ESCAPE '!' OR `evt_type` LIKE '%".$keyword."%' ESCAPE '!')");
        }
        if(!empty($this->session->userdata("eventtype"))){
            $this->db->where("event_type_id", $this->session->userdata("eventtype"));
        }
        if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(!empty($this->session->userdata("locat"))){
            $this->db->having(array("distance <"=> 1));
        }
        if(array_key_exists("event_id",$params)){
           $this->db->where('event_id',$params['event_id']);
           $query = $this->db->get();
           $result = $query->row_array();
        }else{            
           //set start and limit
           if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
           }
           $query = $this->db->get();


           if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
           }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
           }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           }
        }
        return $result;
    }

   /*
    * Get events type list
    */
    function getEventTypeList(){
        return $this->db->order_by("evt_type", "ASC")->get_where($this->eventTypeTbl, array("evt_status"=>1))->result_array();        
    }

    /*
    * get rows for business opening time table
    */
    function getEmailTemplateRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->emailTpTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("tmplate_id",$params)){
            $this->db->where('tmplate_id',$params['tmplate_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }
	
	 function get_img($images_id){
		  $img = '';
		  
		 for($i=0;$i<count($images_id);$i++){
			 $array = array
			 (
			 "conditions"=>
			    array(
			         "cat_id"=>$images_id[$i]
					 ),
			         "returnType"=>'single'
			 );
			 $result = $this->getCategoryRows($array);
			  
			 if($result['cat_img_path'] != 'default.png'){
				 $img = $result['cat_img_path'];
				   
				 break;
				 
			 }else{
				 $j = $i+1;
				 if($j == count($images_id)){
					     
						$img = $result['cat_img_path'];
						break;
					 
				 }
			 }
		 }
		// echo $img;
		  return $img;

		 
	 } 

    /*
    * get rows for home category tabs list
    */
    function getHomeCatgoryListingRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userHomeCatListTbl);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }
function checkSubscriptionEmail($email){
        $this->db->select('user_email')->from('subscribers');
        $this->db->where('user_email',$email);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }else{
            return FALSE;
        }
    }
 function getPackage(){

        $this->db->select('*');
        $this->db->from('otd_option_master');
        $this->db->join( 'otd_option_price', 'otd_option_master.opt_id = otd_option_price.opt_type_id', 'left' );
        $this->db->where(array("opt_type"=>4, "opt_status"=>1));
        $query = $this->db->get();
        $results =$query->result_array();

        //return $results;

        //print_r("<pre/>");
        //print_r($results);
        //die;
        //$result = array();

        foreach($results as $result) {

          //print_r("<pre/>");
          //print_r($result['opt_id']);die;

          $package_option = $this->getOption($result['opt_type_id']);

          $result1[]  = array(
                            'opt_id'=>$result['opt_id'],
                            'opt_name'=>$result['opt_name'],
                            'opt_price'=>$result['opt_price'],
                            'opt_text'=>$result['opt_text'],
                            'package_option'=>$package_option
                          );

        }

        return $result1;

        //print_r("<pre/>");
        //print_r($result1);
        //die;


        //if($query->num_rows()>0){
           // return $query->result_array();
        //}else{
            //return FALSE;
        //}
    } 
    public function getOption($opt_type_id){

          //echo $opt_type_id;die;

        $this->db->select('*');
        $this->db->from('otd_package_options');
        $this->db->join( 'otd_option_master', 'otd_package_options.pkg_inc_optid = otd_option_master.opt_id', 'left' );
        //$this->db->where(array("opt_type"=>4, "pkg_package_id"=>$opt_type_id));

        $this->db->where(array("pkg_package_id"=>$opt_type_id));
        $query = $this->db->get();
        $results =$query->result_array();

        return $results;

        // print_r("<pre/>");
        //print_r($results);
        //die;

    }
    function getTopContent(){

        if(!$this->session->userdata('lang_id')) {
          $lang_id = 20;
        } else {
          $lang_id = $this->session->userdata('lang_id');
        }

        $this->db->select('*');
        $this->db->from('home_content');
        //$this->db->where(array("cat_status"=>1, "type"=>1));
        $this->db->where(array("language_id"=>$lang_id));
        //$this->db->group_by("cat_name");
        //$this->db->order_by("cat_name", "ASC");
        //$this->db->limit(4);
        $query = $this->db->get();
        
        //        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
        
    }
    function getDashboardContent(){
      $this->db->select('*');
      $this->db->from('dashboard_content');
      $query = $this->db->get();
      if($query->num_rows()>0){
        return $query->result_array();
      }else{
        return FALSE;
      }

    }  
     public function getPackageDetails()
    {

        
        $sql = "SELECT optm.opt_id, optm.opt_name,optm.price,optm.duration, optm.opt_description,  optm.opt_type,  optm.opt_status FROM `otd_option_master` optm where optm.opt_status = 1 ";
      
       
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

     public function getPackageDetails1()
    {

        
        $sql = "SELECT optm.opt_id, optm.opt_name,optm.price,optm.duration, optm.opt_description,  optm.opt_type,  optm.opt_status FROM `otd_option_master` optm where optm.opt_status = 1 AND optm.opt_id != 6";
      
       
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }
    function showcountry(){
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('add_status!=',3);

        $this->db->order_by("name", "ASC");

        $que = $this->db->get();
        $result = $que->result_array();
        return $result;
      }
     

     
/*
* External function for user purchase end
*/
}
?>