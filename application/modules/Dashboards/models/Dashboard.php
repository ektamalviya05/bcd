<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboard extends CI_Model {

    function __construct() {
        $this->userTbl = 'users';
        
    }
    function getProfileDetail($id){
        $this->db->select('*')->from($this->userTbl);
        $this->db->where('user_id',$id);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }
    }

    function getenquirylisting($user_id=0)
    {
      $this->db->select("bdc_enquiry.created,
                           bdc_enquiry.name,
                           bdc_enquiry.email,
                           bdc_enquiry.id,
                           bdc_enquiry.message,
                           bdc_business.business_title,
                           bdc_business.business_email,
                           bdc_business.business_mobile,
                           bdc_business.business_address,
            ");
        $this->db->from('bdc_enquiry');
        $this->db->join('bdc_business' ,'bdc_business.business_id = bdc_enquiry.business_id');
        $this->db->where('bdc_business.user_id =' ,$user_id);
        $this->db->where('bdc_business.status =' ,1);
        $query = $this->db->get();
        $result= $query->result_array();
        //echo $this->db->last_query();
        //die();
        return $result;
    }

    function getenquirydetail($user_id=0,$equiryId=0)
    {
      $this->db->select("bdc_enquiry.created,
                           bdc_enquiry.name,
                           bdc_enquiry.email,
                           bdc_enquiry.id,
                           bdc_enquiry.message,
                           bdc_business.business_title,
                           bdc_business.business_email,
                           bdc_business.business_mobile,
                           bdc_business.business_address,
            ");
        $this->db->from('bdc_enquiry');
        $this->db->join('bdc_business' ,'bdc_business.business_id = bdc_enquiry.business_id');
        $this->db->where('bdc_business.user_id =' ,$user_id);
        $this->db->where('bdc_enquiry.id =' ,$equiryId);
        $this->db->where('bdc_business.status =' ,1);
        $query = $this->db->get();

        $result= $query->result_array();
         $output .= '  
      <div class="table-responsive">  
           <table class="table table-bordered enquiry_table">';  
      foreach($result as $row) 
      {  
           $output .= '  
                <tr class="enquiry_name">  
                     <td width="30%"><label>Name</label></td>  
                     <td width="70%">'.$row["name"].'</td>  
                </tr>  
                <tr class="enquiry_email">  
                     <td width="30%"><label>Email</label></td>  
                     <td width="70%">'.$row["email"].'</td>  
                </tr>  
                <tr class="enquiry_msg">  
                     <td width="30%"><label>Message</label></td>  
                     <td width="70%">'.$row["message"].'</td>  
                </tr>  
                <tr class="enquiry_detail">  
                     <td width="30%"><label>Listing Detail</label></td>  
                     <td width="70%">'.$row["business_title"].'<br/> '.$row["business_address"].'</td>  
                </tr>  
                 
                ';  
      }  
      $output .= "</table></div>";  
      echo $output;
        
        
    }

    public function fetch_transactionRecord($user_id=0) {
 
       //$this->db->limit($limit, $start);
        $this->db->select("users.user_firstname,
                          users.user_lastname,
                         otd_option_master.opt_name,
                          transaction_table.transaction_id,
                         transaction_table.amt,
                         transaction_table.packageid,
                         transaction_table.create_date,
                         transaction_table.st,
                        ");
        $this->db->from('transaction_table');
        $this->db->join('users','users.user_id = transaction_table.userid');
        $this->db->join('otd_option_master','otd_option_master.opt_id = transaction_table.packageid');
        $this->db->where('transaction_table.userid',$user_id);
       //  $this->db->order_by('transaction_table.create_date', "DESC");
       $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
           foreach ($query->result() as $row) {
 
               $data[] = $row;
 
           }
 
           return $data;
 
       }
 
       return false;
 
   }
    public function fetch_transactionRecord1($user_id=0) {
 
       //$this->db->limit($limit, $start);
        $this->db->select("*");
        $this->db->from('transaction_table');
        //$this->db->join('users','users.user_id = transaction_table.userid');
        //$this->db->join('otd_option_master','otd_option_master.opt_id = transaction_table.packageid');
        $this->db->where('transaction_table.userid',$user_id);
         //$this->db->where('transaction_table.plan_valid',1);
         $this->db->where('transaction_table.package_type!=',3);
         $this->db->where('transaction_table.package_type!=',4);

       //  $this->db->order_by('transaction_table.create_date', "DESC");
       $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
           foreach ($query->result() as $row) {
 
               $data[] = $row;
 
           }
 
           return $data;
 
       }
 
       return false;
 
   }

    public function fetch_transactionRecord2($user_id=0) {
 
       //$this->db->limit($limit, $start);
        $this->db->select("*");
        $this->db->from('transaction_table');
        //$this->db->join('users','users.user_id = transaction_table.userid');
        //$this->db->join('otd_option_master','otd_option_master.opt_id = transaction_table.packageid');
        $this->db->where('transaction_table.userid',$user_id);
        
       //  $this->db->order_by('transaction_table.create_date', "DESC");
       $query = $this->db->get();
 
       //echo $this->db->last_query();
       //die();

 
       if ($query->num_rows() > 0) {
 
           foreach ($query->result() as $row) {
 
               $data[] = $row;
 
           }
 
           return $data;
 
       }
 
       return false;
 
   }


   // today code

    function showcategory(){
    
    $this->db->select('*');
      $this->db->from('otd_business_category');
      $this->db->where('cat_parent',0);
    $this->db->where('cat_status !=',3);
    $que = $this->db->get();
    $result = $que->result();
    return $result;
    //$num_rows = $que->num_rows();
  }

   function showcity()
    {
        $this->db->select("*");
        $this->db->from("cities");
        $this->db->where('add_status!=',3);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        
        return  $result ;

    }

  function subcat()
    {
        $this->db->select("*");
        $this->db->from("otd_business_category");
        $this->db->where('cat_parent!=',0);
        $this->db->where('cat_status',1);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        
        return  $result ;

    }

   function showcountry()
    {
        $this->db->select('*');
        $this->db->from('countries');
         $this->db->where('add_status=',0);
        $que = $this->db->get();
        $result = $que->result_array();
        //print_r($result);
        //die();
        return $result;

    }

    function getListingContent(){
      $this->db->select('*');
      $this->db->from('listing_content');
      $query = $this->db->get();
      if($query->num_rows()>0){
        return $query->result_array();
      }else{
        return FALSE;
      }

    }
     function getTopContent(){

        if(!$this->session->userdata('lang_id')) {
          $lang_id = 20;
        } else {
          $lang_id = $this->session->userdata('lang_id');
        }

        $this->db->select('*');
        $this->db->from('home_content');
        //$this->db->where(array("cat_status"=>1, "type"=>1));
        $this->db->where(array("language_id"=>$lang_id));
        //$this->db->group_by("cat_name");
        //$this->db->order_by("cat_name", "ASC");
        //$this->db->limit(4);
        $query = $this->db->get();
        
        //        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        if($query->num_rows()>0){
            return $query->result_array();
        }else{
            return FALSE;
        }
        
    }

    function getRow($params = array())
    {
         $this->db->select('*');
        $this->db->from('bdc_business'); 
         if(array_key_exists("business_id",$params))
         {
            $this->db->where('business_id',$params['business_id']);
           
        }
         $query = $this->db->get();
            $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        //print_r($result);
         return $result;
    }

    function getbus($params = 0)
    {
         $this->db->select('*');
        $this->db->from('bdc_business');
        $this->db->where('user_id',$params);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;

    } 

    function getRow1($params = array())
    {
         $this->db->select('*');
        $this->db->from('bdc_offers'); 
         if(array_key_exists("business_id",$params))
         {
            $this->db->where('business_id',$params['business_id']);
           
        }
         $query = $this->db->get();
            $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        //print_r($result);
         return $result;
    }


    function add_listing($uploadfile, $uploadfile2, $uploadfile3, $uploadfile4,$offer){
      if(isset($uploadfile)){
        $uploadfile;
      }else{
        $uploadfile=$this->input->post('business_image1');;
      }
      if(isset($uploadfile2)){
        $uploadfile2;
      }else{
        $uploadfile2=$this->input->post('business_image2');;
      }
      if(isset($uploadfile3)){
        $uploadfile3;
      }else{
        $uploadfile3=$this->input->post('business_image3');;
      }
      if(isset($uploadfile4)){
        $uploadfile4;
      }else{
        $uploadfile4=$this->input->post('business_image4');;
      }
      if($this->input->post('business_country'))
          {
              $business_country = $this->input->post('business_country');
          }
          else
          {
              $business_country = "";
          }
          if($this->input->post('business_city'))
          {
              $business_city = $this->input->post('business_city');
          }
          else
          {
              $business_city = " ";
          }
          if($this->input->post('location'))
          {
              $location = $this->input->post('location');
          }
          else
          {
              $location = $this->input->post('business_address');
          }
          $business_id = $this->input->post('business_id');
      $userData = array(
        'business_title '     => strip_tags($this->input->post('title')),
        'business_email '     => strip_tags($this->input->post('email')),
        'business_mobile'   => strip_tags($this->input->post('mobile')),
        'business_price'    => strip_tags($this->input->post('price')),
        'business_desc'     => strip_tags($this->input->post('description')),
        'sub_cat_id'            => strip_tags($this->input->post('sub_cat_id')),
        'business_address'    => $location,
        'cat_id '         => strip_tags($this->input->post('category')),
        'business_image1'     => $uploadfile,
        'business_image2'     => $uploadfile2,
        'business_image3'     => $uploadfile3,
        'business_image4'     => $uploadfile4,
        'features_ads'      => strip_tags($this->input->post('features')),
        'user_id'       => $this->session->userdata('user_id'),
              'business_country'      => $business_country,
        'business_city'         => $business_city,
        'status'   => 0,
      );
      $this->db->where('business_id', $business_id);
      $result = $this->db->update('bdc_business', $userData);
          //$lstId = $this->db->insert_id();
          $offerData = array('offer_image'=>$offer);
          
          $this->db->where('business_id', $business_id);
          $this->db->update('bdc_offers', $offerData);
      return  $result;
      //die();
  }
  function add_listing1($uploadfile, $uploadfile2, $uploadfile3, $uploadfile4,$uploadfile5, $uploadfile6, $uploadfile7, $uploadfile8,$uploadfile9, $uploadfile10,$imageIcon,$address_typ,$cityId,$countyId,$full_address){
      if(isset($uploadfile)){
        $uploadfile;
      }else{
        $uploadfile=$this->input->post('business_image1');
      }
      if(isset($uploadfile2)){
        $uploadfile2;
      }else{
        $uploadfile2=$this->input->post('business_image2');
      }
      if(isset($uploadfile3)){
        $uploadfile3;
      }else{
        $uploadfile3=$this->input->post('business_image3');
      }
      if(isset($uploadfile4)){
        $uploadfile4;
      }else{
        $uploadfile4=$this->input->post('business_image4');
      }

      if(isset($uploadfile5)){
        $uploadfile5;
      }else{
        $uploadfile5=$this->input->post('business_image5');
      }

      if(isset($uploadfile6)){
        $uploadfile6;
      }else{
        $uploadfile6=$this->input->post('business_image6');
      }

      if(isset($uploadfile7)){
        $uploadfile7;
      }else{
        $uploadfile7=$this->input->post('business_image7');
      }

      if(isset($uploadfile8)){
        $uploadfile8;
      }else{
        $uploadfile8=$this->input->post('business_image8');
      }

      if(isset($uploadfile9)){
        $uploadfile9;
      }else{
        $uploadfile9=$this->input->post('business_image9');
      }

      if(isset($uploadfile10)){
        $uploadfile10;
      }else{
        $uploadfile10=$this->input->post('business_image10');
      }

      if(isset($imageIcon)){
        $imageIcon;
      }else{
        $imageIcon=$this->input->post('business_logo');
      }
      // if(isset($offer)){
      //       $offer;
      // }else{
      //       $offer=$this->input->post('offerimg');
      // }

      if($full_address)
        {
            //$latlong    =   $this->get_lat_long1($full_address);
            //$latlong  = explode(',',$latlong);
            //$lat   =  $latlong[0];
           // $long  =  $latlong[1];

          $latlong    =   getLocationLatLng($full_address);
           
           $lat   =  $latlong['lat'];
           $long  =  $latlong['lng'];
        }
      // if($this->input->post('business_country'))
      //     {
      //         $business_country = $this->input->post('business_country');
      //     }
      //     else
      //     {
      //         $business_country = "";
      //     }
      //     if($this->input->post('business_city'))
      //     {
      //         $business_city = $this->input->post('business_city');
      //     }
      //     else
      //     {
      //         $business_city = " ";
      //     }
      //     if($this->input->post('location'))
      //     {
      //         $location = $this->input->post('location');
      //     }
      //     else
      //     {
      //         $location = $this->input->post('business_address');
      //     }
          $business_id = $this->input->post('business_id');
      $userData = array(
        'business_title '     => strip_tags($this->input->post('title')),
        'business_email '     => strip_tags($this->input->post('email')),
        'business_mobile'   => strip_tags($this->input->post('mobile')),
        'business_price'    => strip_tags($this->input->post('price')),
        'business_desc'     => strip_tags($this->input->post('description')),
        'sub_cat_id'            => strip_tags($this->input->post('sub_cat_id')),
        'business_address'    => $full_address,
        'cat_id '         => strip_tags($this->input->post('category')),
        'business_image1'     => $uploadfile,
        'business_image2'     => $uploadfile2,
        'business_image3'     => $uploadfile3,
        'business_image4'     => $uploadfile4,
        'business_image5'     => $uploadfile5,
        'business_image6'     => $uploadfile6,
        'business_image7'     => $uploadfile7,
        'business_image8'     => $uploadfile8,
        'business_image9'     => $uploadfile9,
        'business_image10'     => $uploadfile10,
        //'features_ads'      => strip_tags($this->input->post('features')),
        'user_id'       => $this->session->userdata('user_id'),
        'business_country'      => $countyId,
        'business_city'         => $cityId,
        "address_type"          => $address_typ,
        "lat"                   => $lat,
        "lon"                   => $long,
        "business_logo"         => $imageIcon,
        'status'                => 0,
      );
      $this->db->where('business_id', $business_id);
      $result = $this->db->update('bdc_business', $userData);
          //$lstId = $this->db->insert_id();
          //$offerData = array('offer_image'=>$offer);
          
          //$this->db->where('business_id', $business_id);
          //$this->db->update('bdc_offers', $offerData);
      return  $business_id;
      //die();
  }

  function viewcity()
    {
         $this->db->select('*');
        $this->db->from('cities');
        //$this->db->where('country_id',$hps_level);
        $que = $this->db->get();
        $result = $que->result_array();
        return $result;
        

    }

    function getcat($cat_id=NULL)
    {
       $this->db->select('*');
        $this->db->from('otd_business_category');
        $this->db->where('cat_parent',$cat_id);
        $this->db->where('cat_status','1');
        $query1 = $this->db->get();
        $result1 = $query1->result_array();
        //echo $this->db->last_query();
        //die();
        if($result1)
        {
            foreach ($result1 as $row) {
            ?>
            <option value="<?php echo $row['cat_id']; ?>"> <?php echo $row['cat_name']; ?></option>
            <?php
            }
        }
        else
        {
          ?>
          <option value="">There is no sub Category</option>
          <?php 
        }

    }
    public function mypackage($user_id=0) {

      $this->db->from('transaction_table');
      $this->db->where('transaction_table.userid',$user_id);
      $this->db->where('transaction_table.business_id',' ');
      $this->db->where('transaction_table.plan_valid',1);
      $this->db->where('transaction_table.package_type!=',3);
      $this->db->where('transaction_table.package_type!=',4);

      $query = $this->db->get();
      if ($query->num_rows() > 0) {
           foreach ($query->result() as $row) {
 
               $data[] = $row;
           }
           return $data;
       }
       return false;
   }
  public function featurepackage($user_id=0) {

      $this->db->from('transaction_table');
      $this->db->where('transaction_table.userid',$user_id);
      $this->db->where('transaction_table.business_id!=',' ');
      $this->db->where('transaction_table.plan_valid',1);
      $this->db->where('transaction_table.package_type!=',1);
      $this->db->where('transaction_table.package_type!=',2);
      $query = $this->db->get();
      if ($query->num_rows() > 0) {
           foreach ($query->result() as $row) {
 
               $data[] = $row;
           }
           return $data;
       }
       return false;
   }

   function get_lat_long1($address){

    $address = str_replace(" ", "+", $address);

    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    //echo $lat;die;
    return $lat.','.$long;
}

    public function gefavads($user_id)
    {
      $this->db->select('*');
      $this->db->from('bdc_favourite_business');
      $this->db->join('bdc_business','bdc_business.business_id=bdc_favourite_business.business_id');
      $this->db->where('bdc_favourite_business.user_id=',$user_id);
      $this->db->where('bdc_business.status=','1');
      $query1 = $this->db->get();
      $result1 = $query1->result_array();
      return $result1;
    }

     public function myoffer($user_id)
    {
      $this->db->select('*');
      $this->db->from('bdc_offers');
      $this->db->join('bdc_business','bdc_business.business_id=bdc_offers.business_id');
      $this->db->where('bdc_offers.user_id=',$user_id);
      $this->db->where('bdc_business.status=','1');
      $query1 = $this->db->get();
      $result1 = $query1->result_array();
      return $result1;
    }

     public function myserviecs($user_id)
    {
      $this->db->select('*');
      $this->db->from('bdc_services');
      $this->db->join('bdc_business','bdc_business.business_id=bdc_services.business_id');
      $this->db->where('bdc_services.user_id=',$user_id);
      $this->db->where('bdc_business.status=','1');
      $query1 = $this->db->get();
      $result1 = $query1->result_array();
      return $result1;
    }


    
}
