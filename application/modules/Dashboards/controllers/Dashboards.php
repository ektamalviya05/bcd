<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboards extends MX_Controller {

    public function __construct() {
        parent:: __construct();

        // error_reporting(E_ALL);
//ini_set('display_errors', 1);
        
        $this->load->library('form_validation', 'session');
        $this->load->model('Dashboard');
        $this->load->model('/./Page');
        //$this->load->model('Page');
         $this->userTbl = 'users';
        //is_user_login();

         $this->load->helper('common_helper');
       
    }

    public function index() {

        $data = array();
        $user_id = $this->session->userdata('user_id');


        $this->db->select('userid');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $que2 = $this->db->get();
        //$result2 = $que2->result();

        $data['current_plan'] = $que2->result();

        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Dashboard');

        $fCon['conditions'] = array("features_ads" => 1,"user_id"=>$user_id,'status'=>1);
        //$fCon['limit'] = 3;
        $activeCon['conditions'] = array("user_id"=>$user_id,'status'=>1);
        $inactiveCon['conditions'] = array("user_id"=>$user_id,'status!='=>1);
        $data['top_content']             = $this->Page->getTopContent();

        //print_r("<pre/>");
        //print_r($data['top_content']);
        //die;

        $data['dashboard_content']        = $this->Page->getDashboardContent();


        $this->db->select('*');
        $this->db->from('transaction_table');
        $this->db->where('userid',$user_id);
        $this->db->where('transaction_table.package_type=',1);
        //$this->db->where('transaction_table.package_type!=',4);
        $checkFreePlanQuery = $this->db->get();
        $checkFreePlanResult =  $checkFreePlanQuery->result();

        $data['checkFreePlan'] = $checkFreePlanResult;

        //print_r("<pre/>");
        //print_r($checkFreePlanResult);
        //die;




        //print_r("<pre/>");
        //print_r($data['dashboard_content']);
        //die;
        
        $data['featureCategoryListings']     = $this->Page->getHomeCatgoryListingRows($fCon);
        $data['activeCategoryListings']      = $this->Page->getHomeCatgoryListingRows($activeCon);
        $data['inactiveCategoryListings']    = $this->Page->getHomeCatgoryListingRows($inactiveCon);
        $data['packageListings']             = $this->Page->getPackageDetails();

        //getPackageDetails
        $data['getenquirylisting'] = $this->Dashboard->getenquirylisting($user_id);
        $data['getranstion'] = $this->Dashboard->fetch_transactionRecord1($user_id);

        $data['transtionHistory'] = $this->Dashboard->fetch_transactionRecord2($user_id);
        


         $data['mypackage'] = $this->Dashboard->mypackage($user_id);
         $data['featurepackage'] = $this->Dashboard->featurepackage($user_id);

         $data['gefavads']                      = $this->Dashboard->gefavads($user_id);
         $data['myoffer']                      = $this->Dashboard->myoffer($user_id);
         $data['myserviecs']                      = $this->Dashboard->myserviecs($user_id);


         //print_r("<pre/>");
          //print_r($data['getranstion']);
          //die;


        
        $data['business_categories'] = $this->Page->getBusinessCategories();
        $data['user_detail'] = $this->Dashboard->getProfileDetail($user_id);
        $data['country'] = $this->Page->showcountry();


        //print_r("<pre/>");
       // print_r($data['country']);die;

       

        //print_r("<pre/>");
       // print_r($data['user_details']);
        //die;
        $this->template->load('home_layout', 'contents', 'dashboard', $data);
    }

   

    public function editprofile() {
        $data = array();
        if ($this->input->post()) {
            $user_id = $this->session->userdata('user_id');
            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),

                'user_city' => strip_tags($this->input->post('user_city')),
                'user_country' => strip_tags($this->input->post('user_country')),
                //'user_password' => md5($this->input->post('user_password')),
                //'website' => $this->input->post('website'),
                //'about' => $this->input->post('about'),
                //'user_zipcode' => $this->input->post('user_zipcode'),
            );
            if (!empty($_FILES['picture']['name'])) {
                $file_element_name = 'picture';
                $config['upload_path'] = './uploads/profile/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['max_size'] = 1024 * 20;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload($file_element_name)) {
                    $this->upload->display_errors('', '');
                } else {
                    $data = $this->upload->data();
                    $userData['user_profile_pic'] = $data['file_name'];
                }
                @unlink($_FILES[$file_element_name]);
            }
            if($this->input->post('password')){
                $userData['user_password']= md5($this->input->post('password'));
            }
            $this->db->where('user_id', $user_id);
            $this->db->update($this->userTbl, $userData);
            
        }
    }

    public function buy_package() {

       

        $user_id = $this->session->userdata('user_id');
        if ($this->input->post()) {

            $opt_id = $this->input->post('opt_id');
            $creditCardType = $this->input->post('creditCardType');
            $creditCardNumber = $this->input->post('creditCardNumber');
            $expDateMonth = $this->input->post('expDateMonth');
            $expDateYear = $this->input->post('expDateYear');
            $cvv2Number = $this->input->post('cvv2Number');
            $first_name = $this->input->post('first_name');
            $last_name = $this->input->post('last_name');
            $city = $this->input->post('city');
            $state = $this->input->post('state');
            $street = $this->input->post('street');
            $zip = $this->input->post('zip');


            $package_info = $this->Page->getPackageInfo($opt_id);
            
            $requestParams = array(
                'IPADDRESS' => $_SERVER['REMOTE_ADDR'],          // Get our IP Address
                'PAYMENTACTION' => 'Sale'
            );

            $creditCardDetails = array(
                'CREDITCARDTYPE' => "'".$creditCardType."'",
                'ACCT' => "'".$creditCardNumber."'",
                'EXPDATE' => "'".$expDateMonth.$expDateYear."'",
                //'EXPDATE' => '062023',          // Make sure this is without slashes (NOT in the format 07/2017 or 07-2017)
                'CVV2' => "'".$cvv2Number."'"
            );

            $payerDetails = array(
                'FIRSTNAME' => "'".$first_name."'",
                'LASTNAME' => "'".$last_name."'",
                'COUNTRYCODE' => 'USD',
                'STATE' => "'".$state."'",
                'CITY' => "'".$city."'",
                'STREET' => "'".$street."'",
                'ZIP' => "'".$zip."'"
            );

            $orderParams = array(
                'AMT' => "'".$package_info['opt_price']."'", // This should be equal to ITEMAMT + SHIPPINGAMT
                'ITEMAMT' => "'".$package_info['opt_price']."'",
                'SHIPPINGAMT' => '0',
                'CURRENCYCODE' => 'USD'       // USD for US Dollars
            );

            $item = array(
                'L_NAME0' => "'".$package_info['opt_name']."'",
                'L_DESC0' => "'".$package_info['opt_description']."'",
                'L_AMT0' => "'".$package_info['opt_price']."'",
                'L_QTY0' => '1'
            );

            $paypal = new Paypal();
            $response = $paypal -> request('DoDirectPayment',
                $requestParams + $creditCardDetails + $payerDetails + $orderParams + $item
            );

            if( is_array($response) && $response['ACK'] == 'Success') { // Payment successful
                // We'll fetch the transaction ID for internal bookkeeping
                $transactionId = $response['TRANSACTIONID'];

                $para = array(
                            "user_id" => $user_id,
                            "package_id" => $package_info['opt_id'],
                            "pkg_opt_ids" => $package_info['optids'],
                            "option_price" => $package_info['opt_price'],
                            "purchase_amt" => $response['AMT'],
                            "currency_code" => $response['CURRENCYCODE'],
                            "transaction_id" => $response['TRANSACTIONID'],
                            "json_response" => json_encode($response),
                            "status" => 1,
                            "create_date" => date('Y-m-d H:i:s'),
                            "update_date" => date('Y-m-d H:i:s')
                            );
                $txn_id = $this->Page->submitTransactionInfo($para);
                if(!empty($txn_id))
                {
                    echo "txn success";
                }
                else
                {
                    echo "error";
                }
            }
            else
            {
                $response['L_SHORTMESSAGE0'];
                $response['L_LONGMESSAGE0'];
                $response['L_SEVERITYCODE0'];
                echo $response['L_LONGMESSAGE0'];
            }


        }
    }

    public function enquirydetail()
    {
        $data = array();
        $user_id = $this->session->userdata('user_id');
        $equiryId = $this->input->post('id');
        $data['getenquirydetail'] = $this->Dashboard->getenquirydetail($user_id,$equiryId);
        

    }

    /// Today code 

    public function listingdetail($args)
    {
       $data = array(); 
        $catchange  = base64_decode($args);
        //$args = $this->input->post('id');
        $data['category'] = $this->Dashboard->showcategory();
        $data['country'] = $this->Dashboard->showcountry();
        $data['subcategory'] =  $this->Dashboard->subcat();
        $data['city'] = $this->Dashboard->viewcity();
        $data['top_content']        = $this->Dashboard->getTopContent();
        $data['city'] = $this->Dashboard->showcity();
        $data['listing_content']        = $this->Dashboard->getListingContent();
        $data['business'] = $this->Dashboard->getRow(array("business_id"=>$catchange));
        $data['offerImg'] = $this->Dashboard->getRow1(array("business_id"=>$catchange));
        $this->template->load('home_layout', 'contents', 'editlisting', $data);
        
    }

     public function addservices()
    {
       $data = array(); 
        $catchange  = base64_decode($args);
        //$args = $this->input->post('id');
        $user_id = $this->session->userdata('user_id');
        $data['category'] = $this->Dashboard->showcategory();
        $data['country'] = $this->Dashboard->showcountry();
        $data['subcategory'] =  $this->Dashboard->subcat();
        $data['city'] = $this->Dashboard->viewcity();
        $data['top_content']        = $this->Dashboard->getTopContent();
        $data['listing_content']        = $this->Dashboard->getListingContent();
        $data['business'] = $this->Dashboard->getbus($user_id);
        $data['offerImg'] = $this->Dashboard->getRow1(array("business_id"=>$catchange));
        $this->template->load('home_layout', 'contents', 'addservices', $data);
        
    }

    public function insert_services()
    {

        $business_id = $this->input->post('business_id');
        $user_id = $this->session->userdata('user_id');
        $number = count($_POST["userfile"]); 
        if($number > 0)  
        {
            for($i=0; $i<$number; $i++)  
            {
                if(trim($_POST["userfile"][$i] != ''))  
                {   
                     $servicesData = 
                        array('services_name'=>$_POST["userfile"][$i],
                           'business_id'=>$business_id,
                           'user_id' => $user_id
                          );
                    $this->db->insert('bdc_services', $servicesData);

                }
            } 

        }

        redirect(base_url('dashboard'));

    }

    public function addoffer()
    {
       $data = array(); 
        $catchange  = base64_decode($args);
        //$args = $this->input->post('id');
        $user_id = $this->session->userdata('user_id');
        $data['category'] = $this->Dashboard->showcategory();
        $data['country'] = $this->Dashboard->showcountry();
        $data['subcategory'] =  $this->Dashboard->subcat();
        $data['city'] = $this->Dashboard->viewcity();
        $data['top_content']        = $this->Dashboard->getTopContent();
        $data['listing_content']        = $this->Dashboard->getListingContent();
        $data['business'] = $this->Dashboard->getbus($user_id);
        $data['offerImg'] = $this->Dashboard->getRow1(array("business_id"=>$catchange));
        $this->template->load('home_layout', 'contents', 'addoffer', $data);
        
    }

    public function insert_offer()
    {
        $uploaded_images = array();
        $business_id = $this->input->post('business_id');
        $user_id = $this->session->userdata('user_id');
        $start_date = $this->input->post('start_date');
        $end_date = $this->input->post('end_date');
        foreach($_FILES['userfile']['name'] as $key=>$val){
            $random = $this->generateRandomString(10);

            //$data['upload_path'] = 'uploads/business/'; 
            //$data['allowed_types'] = 'jpg|jpeg|png|gif';
            //$data['encrypt_name'] = true;
            $ext = pathinfo($_FILES['userfile']['name'][$key], PATHINFO_EXTENSION); 
            $file_name = $random.".".$ext;
            //$this->load->library('upload',$data);
            $target_dir = "uploads/business/";
            $target_file = $target_dir .$file_name;
            $size =$_FILES['userfile']['size'][$key];

            if($size  <   512000 )
            {
                if (move_uploaded_file($_FILES["userfile"]["tmp_name"][$key], $target_file)) 
                {
                   $offerFile = $file_name;

                }
                $offerData = array('offer_image'=>$offerFile,
                               'business_id'=>$business_id,
                               'user_id' => $user_id,
                               'start_date' => $start_date,
                               'end_date' => $end_date,
                              );
                $this->db->insert('bdc_offers', $offerData);
            }

            else
            {
                 $this->session->set_flashdata("error", "Image Size too large");
                 redirect(base_url('add_offer'));

            }


           
        }

        redirect(base_url('dashboard'));

    }

    public function generateRandomString($length = 12) {
        // $length = 12;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function addfav()
    {
        $b_id = $this->input->post('b_id');
        $user_id = $this->session->userdata('user_id');
        $favData = array('business_id'=>$b_id,
                           'user_id'=>$user_id,
                           'favourite' => 1
                          );
        $insert = $this->db->insert('bdc_favourite_business', $favData);
        if($insert)
        {
            return $insert;
        }
    }

     public function activelisting($args)
    {
       $data = array(); 
        $catchange  = base64_decode($args);
        $datatble = array('status'=> 2);
        $this->db->where('business_id',$catchange);
        $result = $this->db->update('bdc_business', $datatble);
        //echo $this->db->last_query();
        //die();
        //echo $result;
        if($result)
        {
            redirect(base_url('dashboard'));
        }


   }

    public function active($args)
    {
       $data = array(); 
        $catchange  = base64_decode($args);
        $datatble = array('status'=> 1);
        $this->db->where('business_id',$catchange);
        $result = $this->db->update('bdc_business', $datatble);
        //echo $this->db->last_query();
        //die();
        //echo $result;
        if($result)
        {
            redirect(base_url('dashboard'));
        }


   }



  public function update_listing(){
     //echo "string";
     //die();
        $address_type = $this->input->post('cars');
        if($address_type == 3)
        {
            $address_typ = 1;
            $autoloc = $this->input->post("location");
            if($autoloc)
            {
                $autolocation = explode(', ', $autoloc);
                $count =count($autolocation);
                $len =$count-3;
                $full_address = $this->input->post("location");
                $cityId = $autolocation[$len];
                $country = end($autolocation); 
                    $this->db->select('*');
                    $this->db->from('countries');
                    $this->db->where('name', $country);
                    $query = $this->db->get();
                    $listingCount = $query->num_rows();
                    if($listingCount)
                   {
                        $country_arry = $query->result_array();

                        foreach ($country_arry as  $valuecountry) {
                            $countyId = $valuecountry['id'];
                        }
                  }
                    else
                  {
                        $this->session->set_flashdata('error', 'This Country Not in Country Management!!');
                        $bid = $this->input->post('business_id');
                        $codebid = base64_encode($bid);
                        redirect(base_url('updatelisting/'.$codebid));
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Please Enter Address!!');
                $bid = $this->input->post('business_id');
                $codebid = base64_encode($bid);
                redirect(base_url('updatelisting/'.$codebid));

            }

        }
        else 
        
        {
            $address_typ = 0;
        
            if($this->input->post("business_city") && $this->input->post("business_country") && $this->input->post("business_address") )
            {
                $cityId  = $this->input->post("business_city");
                $country = $this->input->post("business_country");
                $full_address = $this->input->post("business_address");
            }
            else
            {
                $this->session->set_flashdata('error', 'Please Enter Address , Country , City !!');
                  $bid = $this->input->post('business_id');
                $codebid = base64_encode($bid);
                redirect(base_url('updatelisting/'.$codebid));

            }

        }
         
        // $data['upload_path'] = 'uploads/business/'; 
        // $data['allowed_types'] = 'jpg|jpeg|png|gif';
        // $data['encrypt_name'] = true;
        // $this->load->library('upload',$data);
        // $target_dir = "uploads/business/";
        // $target_file = $target_dir . basename($_FILES["offer"]["name"]);
        //  $size =$_FILES['offer']['size'] ;
        // //if($size  <   1000 ){
        //     if (move_uploaded_file($_FILES["offer"]["tmp_name"], $target_file)) {
        //        $offerFile = $_FILES["offer"]["name"];
               

        //     }
        //}

        // else{
        //         $this->session->set_flashdata('error', 'offer image too large!!');
        //         $bid = $this->input->post('business_id');
        //         $codebid = base64_encode($bid);
        //         redirect(base_url('updatelisting/'.$codebid));
        //   }

        
        $data['upload_path'] = 'uploads/business/'; 
        $data['allowed_types'] = 'jpg|jpeg|png|gif';
        $data['encrypt_name'] = true;

        $this->load->library('upload',$data);

        //$uploadfile ="";
        //$uploadfile2 ="";
        //$uploadfile3 ="";
        //$uploadfile4 ="";
        if($this->upload->do_upload('userfile'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile = $attachment_data['upload_data']['file_name'];
                 
        }       
        if($this->upload->do_upload('userfile2'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile2 = $attachment_data['upload_data']['file_name'];
                 
        }
        if($this->upload->do_upload('userfile3'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile3 = $attachment_data['upload_data']['file_name'];
                 
        }
        if($this->upload->do_upload('userfile4'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile4 = $attachment_data['upload_data']['file_name'];
                 
        }

        if($this->upload->do_upload('userfile5'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile5 = $attachment_data['upload_data']['file_name'];
                 
        }

        if($this->upload->do_upload('userfile6'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile6 = $attachment_data['upload_data']['file_name'];
                 
        }

        if($this->upload->do_upload('userfile7'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile7 = $attachment_data['upload_data']['file_name'];
                 
        }

        if($this->upload->do_upload('userfile8'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile8 = $attachment_data['upload_data']['file_name'];
                 
        }

         if($this->upload->do_upload('userfile9'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile9 = $attachment_data['upload_data']['file_name'];
                 
        }

         if($this->upload->do_upload('userfile10'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $uploadfile10 = $attachment_data['upload_data']['file_name'];
                 
        }

         if($this->upload->do_upload('imageIcon'))
        { 
            $attachment_data = array('upload_data' => $this->upload->data());
            $imageIcon = $attachment_data['upload_data']['file_name'];
                 
        }
        
        $data = $this->Dashboard->add_listing1($uploadfile, $uploadfile2, $uploadfile3, $uploadfile4,$uploadfile5, $uploadfile6, $uploadfile7, $uploadfile8,$uploadfile9, $uploadfile10,$imageIcon,$address_typ,$cityId,$country,$full_address);
        if($data){   

            $business_id  = base64_encode($data);
             //if($this->input->post('features')==1){
                //redirect(base_url('Users/featured_plan/'.$business_id));
            //}else{
                $this->session->set_flashdata('success', 'Listing has been added successfully');
                 redirect(base_url('dashboard'));
            //} 

            $this->session->set_flashdata('success', 'Listing has been added successfully');
            redirect(base_url('dashboard'));
        }else{
            $this->session->set_flashdata('error', 'Something went wrong');
            redirect(base_url('dashboard'));
        }
        
    }

    public function deletelisting()
    {
        $id = $this->input->post('id');
        // //$mode = $this->input->post('mode');
        

        // //$datatble = array('status'=>$mode);
        // $this->db->where('business_id',$id);
        // $result = $this->db->delete('bdc_business');
        // //echo $this->db->last_query();
        // //die();
        // echo $result;
        $this->db->where('business_id',$id);
        $result = $this->db->delete('bdc_business');
        if($result)
        {
           // echo $this->db->last_query();
                $this->db->where('business_id',$id);
                $sql = $this->db->delete('bdc_offers');
                 if($sql)
                {
                    $this->db->where('business_id',$id);
                    $query = $this->db->delete('bdc_services');
                     if($query){
                        //echo json_encode(array("status"=>200));
                        echo $query;
                     }
                }

        }

    }

     public function deleteservices()
    {
        $id = $this->input->post('id');
        $this->db->where('services_id',$id);
        $result = $this->db->delete('bdc_services');
        if($result)
        {
            echo json_encode(array("status"=>200));
        }

    }

    public function deleteoffer()
    {
        $id = $this->input->post('id');
        $this->db->where('offer_id',$id);
        $result = $this->db->delete('bdc_offers');
        if($result)
        {
            echo json_encode(array("status"=>200));
        }
    }


    public function category()
    {

        $cat_id = $this->input->post('cat_id');
        //$cat_id = '1';
        $getcat = $this->Dashboard->getcat($cat_id);


    }
     public function editpassword()
    {

        $old_password = $this->input->post('old_password');
        $old_password = md5($old_password);
        $user_password = $this->input->post('user_password');
        $user_password = md5($user_password);
        $user_id = $this->session->userdata('user_id');
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('user_id',$user_id);
        $this->db->where('user_password',$old_password);
        $query = $this->db->get();
         if ($query->num_rows() > 0) {
             $passData = array('user_password'=>$user_password,
                              );
              $this->db->where('user_id', $user_id);
            $this->db->update('users', $passData);
            echo json_encode(array("status"=>200, "typefive"=>1, "msg"=>"<label class='text-danger'>Password Update Successfully!!</label>"));
                         exit;


        }
         else
        {
            //echo "fsdfsfsd";
        echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>Please Enter Correct Old Password</label>"));
        exit;
         }





    }


      public function paymentsuccess1()
    {

        //$q = 5;
        //$p = '+ '.$q.' days';
        //$Timestamp = strtotime(date('Y-m-d'));
        //$TotalTimeStamp = strtotime($p, $Timestamp);
        //echo date('d-m-Y', $TotalTimeStamp);die;


        $amt = $_GET['amt'];
        $cc = $_GET['cc'];
        $cm = $_GET['cm'];
        $var = explode("-",$cm);
         $userid =  $var[0];
         $packageid =  $var[1];
         $duration =  $var[2];

        $item_name = $_GET['item_name'];
        $st = $_GET['st'];
        $tx = $_GET['tx'];

        //$duration = 10;


        //cho "test";die;

        $p = '+ '.$duration.' days';
        $Timestamp = strtotime(date('Y-m-d'));
        $TotalTimeStamp = strtotime($p, $Timestamp);
        $free_end = date('Y-m-d', $TotalTimeStamp);

       //echo $free_end;die;

        $data = array(
                 'amt' => $amt,
                 'cc' =>  $cc,
                 'userid' => $userid,
                 'packageid' => $packageid,
                 'item_name' => $item_name,
                 'st' => $st,
                 'transaction_id' => $tx,
                 'free_start' => date('Y-m-d'),
                 'free_end' => $free_end,
                 'plan_valid' => 1,
         );
         $sql = $this->db->insert('transaction_table', $data);
         if($sql)
         {
            $this->session->set_flashdata('success', 'Your Payment is done');
            redirect(base_url("dashboard"));
         }

    }

    public function upgradePlan()
    {
        $data = array();

        $user_id = $this->session->userdata('user_id');
        $data['top_content']             = $this->Page->getTopContent();
        $data['dashboard_content']        = $this->Page->getDashboardContent();
        $data['packageListings'] = $this->Page->getPackageDetails1();
         $data['user_detail'] = $this->Dashboard->getProfileDetail($user_id);
        $data['getranstion'] = $this->Dashboard->fetch_transactionRecord2($user_id);
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Upgrade Plan');
        $this->template->load('home_layout', 'contents', 'upgradePlan', $data);

    }

   
   

}
