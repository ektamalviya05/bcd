<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
      $full_name  = $dashboard_content[0]['full_name'];
      $email  = $dashboard_content[0]['email_field'];
      $phone  = $dashboard_content[0]['phone_field'];
      $website_url  = $dashboard_content[0]['website_url'];
      $zip_code  = $dashboard_content[0]['zip_code_field'];
      $address  = $dashboard_content[0]['address_field'];
      $profile_information  = $dashboard_content[0]['profile_information'];
      $first_name  = $dashboard_content[0]['first_name'];
      $last_name  = $dashboard_content[0]['last_name_field'];

    

   } else {
      $full_name  = $dashboard_content[0]['full_name_ar'];  
      $email  = $dashboard_content[0]['email_field_ar'];
      $phone  = $dashboard_content[0]['phone_field_ar'];
      $website_url  = $dashboard_content[0]['website_url_ar'];
      $zip_code  = $dashboard_content[0]['zip_code_field_ar'];
      $address  = $dashboard_content[0]['address_field_ar'];
      $profile_information  = $dashboard_content[0]['profile_information_ar'];
      $first_name  = $dashboard_content[0]['first_name_field_ar'];
      $last_name  = $dashboard_content[0]['last_name_field_ar'];
   }

   //print_r("<pre/>");
  // print_r($user_detail);
   //die;
?>
<div class="tab-pane  active" id="myprofile">
    <div class="row">
        <div class="col-md-3">
            <div class="faq-left">
                <div class="social-profile"> 
                    <?php if(empty($user_detail['user_profile_pic'])){?>
                    <img class="img-fluid" src="assets_new/images/profile.jpg" alt=""> 
                    <?php } else{ ?>
                        <img class="img-fluid" src="uploads/profile/<?php echo $user_detail['user_profile_pic'];?>" alt=""> 
                   <?php }?>
                </div>
                <div class="card-block">
                    <h4><?php echo !empty($user_detail['user_firstname'])?$user_detail['user_firstname'].' '.$user_detail['user_lastname']:"";?></h4>
<!--                    <h5>Plumber</h5>-->
                    <!--<div class="faq-profile-btn">
                        <button type="button" class="faq-profBtn">Follows</button>
                        <button type="button" class="faq-profBtn">Message</button>
                    </div>-->
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="general-info">
                <div class="card-header">
                    <h5 class="card-header-text"><?php echo $profile_information;?></h5>
                </div>
                <div class="rrow">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="f_name"><?php echo $first_name;?> :</div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_firstname'])?$user_detail['user_firstname']:"";?></div>
                            </div>
                        </div>
                    </div>
                     <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="f_name"><?php echo $last_name;?> :</div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_firstname'])?$user_detail['user_lastname']:"";?></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="f_name"><?php echo $email;?> :</div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_email'])?$user_detail['user_email']:"";?></div>
                            </div>
                        </div>
                    </div>													
                </div>

                <div class="rrow">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="f_name"><?php echo $phone;?>:</div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_phone'])?$user_detail['user_phone']:"";?></div>
                            </div>
                        </div>
                    </div>

                   <!--  <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="f_name"><?php echo $website_url;?> :</div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="f_nameOne"><a href="javascript:;"><?php echo !empty($user_detail['website'])?$user_detail['website']:"";?></a></div>
                            </div>
                        </div>
                    </div>	 -->												
                </div>

               <!--  <div class="rrow">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="f_name"><?php echo $zip_code;?>:</div>
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div class="f_nameOne"><?php echo !empty($user_detail['user_zipcode'])?$user_detail['user_zipcode']:"";?></div>
                            </div>
                        </div>
                    </div>
                   
                </div>	 -->
                <div class="rrow">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="f_name"><?php echo $address;?> :</div>
                                </div>
                                <div class="col-md-12 col-sm-12">
                                    <div class="f_nameOne"><?php echo !empty($user_detail['user_address'])?$user_detail['user_address']:"";?></div>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            <!--<div class="row">
                <div class="col-md-12">
                    <div class="general-info">
                        <div class="card-header">
                            <h5 class="card-header-text">About</h5>
                        </div>
                        <p>
                           <php echo !empty($user_detail['about'])?$user_detail['about']:"";?>
                        </p>														
                    </div>
                </div>
            </div>-->
        </div>
    </div>
</div>
