<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
      $featured_ads_label  = $dashboard_content[0]['featured_ads_label'];
      $active_ads_label  = $dashboard_content[0]['active_ads_label'];
      $inactive_ads_label  = $dashboard_content[0]['inactive_ads_label'];

      $active_add_not_found  = "<h2>Active Ads Not Found</h2>
                                    <p>Please check Other Tab</p>";
      $inactive_add_not_found  = "<h2>Inactive Ads Not Found</h2>
                                    <p>Please check Other Tab</p>"; 

      $favourite_add_not_found  = "<h2>Favourite Ads Not Found</h2>
                                    <p>Please check Other Tab</p>";  
      $favourite_ads  = "Favourite Ads";  

      $sponser_this_add  = "Sponsour this Add"; 
      $active_button  = "Active";
      $not_approve_by_admin  = "Not Approve by Admin";

      

                                
                                                            
   } else {
      $featured_ads_label  = $dashboard_content[0]['featured_ads_ar_label'];  
      $active_ads_label  = $dashboard_content[0]['active_ads_ar_label'];
      $inactive_ads_label  = $dashboard_content[0]['inactive_ads_ar_label'];

      $active_add_not_found  = "<h2>الاعلان النشط غير موجود</h2>
                                    <p>يرجى التحقق من علامة تبويب أخرى</p>";
      $inactive_add_not_found  = "<h2>الإعلانات غير النشطة غير موجودة</h2>
                                    <p>يرجى التحقق من علامة تبويب أخرى</p>";  

      $favourite_add_not_found  = "<h2>الاعلانات المفضلة غير موجودة</h2>
                                    <p>يرجى التحقق من علامة تبويب أخرى</p>";
                                    
      $favourite_ads  = "الإعلانات المفضلة";    

       $sponser_this_add  = "رعاية هذا إضافة";  
       $active_button  = "نشيط";  
       $not_approve_by_admin  = "غير موافق من قبل المسؤول";                      
      
      //$inactive_add_not_found = "Favourite Ads";

  }
?>
<div class="tab-pane" id="myad">
    <div class="my-add-tab">
        <div class="dash-counter">
            <div class="panel">
                <div class="panel-heading inner-tab-bar">
                    <ul class="nav panel-tabs">
                        
                        <li class="active"><a href="#tab2" data-toggle="tab"><?php echo $active_ads_label?></a></li>
                        <li ><a href="#tab1" data-toggle="tab"><?php echo $featured_ads_label?></a></li>
                        <!-- <li><a href="#tab3" data-toggle="tab">Favourite Ads</a></li> -->
                        <li><a href="#tab4" data-toggle="tab"><?php echo $inactive_ads_label?></a></li>
                        <li><a href="#tab5" data-toggle="tab"><?php echo $favourite_ads;?></a></li>
                    </ul>								                    
                </div>
                <div class="panel-body">
                    <div class="tab-content">
                        <div class="tab-pane" id="tab1">
                            <div class="row">
                                <?php
                    if ($featureCategoryListings) {
                        foreach ($featureCategoryListings as $featureCategoryListing) {
                            $list_id  = base64_encode($featureCategoryListing['business_id']);
                            ?>
                                <div class="rtin-main-cols col-md-4 col-sm-6 col-xs-12">
                                    <div class="rt-course-box">
                                        <div class="rtin-thumbnail hvr-bounce-to-right"> <img src="<?php echo base_url('uploads/business/' . $featureCategoryListing['business_image1']); ?>" class="" alt=""> <a href="<?php echo base_url();?>listings_details/<?php echo $list_id; ?>" title=""><i class="fa fa-link" aria-hidden="true"></i></a>
                                            <div class="rtin-price">
                                                <div class="course-price"> <span class="price">FEATURED</span></div>
                                            </div>
                                        </div>
                                        <div class="rtin-content-wrap">
                                            <div class="rtin-content">
                                                <h3 class="rtin-author"><a href="javascript:;" title=""><?php echo $featureCategoryListing['business_title']?></a></h3>
                                                <div class="rtin-title"><a href="javascript:;">
                                                    <?php 
                                        
                                            echo !empty(text_wrap($featureCategoryListing['business_desc'],15)) ? text_wrap($featureCategoryListing['business_desc'],20) : "";?>
                                                </a></div>
                                                
                                                <div class="btn_price"> <span class="btn_pr hidden">$<?php echo $featureCategoryListing['business_price']?></span></div>
                                                <div class="rtin-map"> <span><i class="fa fa-map-marker" aria-hidden="true"></i> <?php echo $featureCategoryListing['business_address']?></span> </div>
                                                 <?php $list_id  = base64_encode($featureCategoryListing['business_id']); ?>
                                                <div class="rtin-map-hover">
                                                    <a href="<?php echo base_url(); ?>updatelisting/<?php echo $list_id; ?>"> <button class="pull-left edituserlisting_bkup"><i  class="fa fa-edit"></i></button></a> 
                                               
                                                     <a href="<?php echo base_url(); ?>deactivelisting/<?php echo $list_id; ?>"> <button class="pull-left edituserlisting_bkup"><i  class="fa fa-eye-slash"></i></button></a>

                                                 <!--  <button class="pull-right deleteuserlisting" class="<?php echo  $featureCategoryListing['business_id']; ?>" id="<?php echo $featureCategoryListing['business_id']; ?>"><i class="fa fa-trash"></i></button> -->  
                                              </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } } ?>
                            </div>
                        </div>

                        <div class="tab-pane active" id="tab2">
                            <div class="row">
                                <?php
                    if ($activeCategoryListings) {
                        foreach ($activeCategoryListings as $activeCategoryListing) {


                            ?>
                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="<?php echo base_url('uploads/business/' . $activeCategoryListing['business_image1']); ?>">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile1">
                                               <!--  <span class="pro-img"><img src="assets_new/images/profile.jpg"></span> -->
                                                 <button class="btn btn-default like"><?php echo $active_button;?></button>
                                            </div>
                                            <h3><?php echo $featureCategoryListing['business_title']?></h3>
                                            <span class="rating-main hidden">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span>
                                        </div>
                                        <p class="add-l"><?php 
                                        
                                            echo !empty(text_wrap($activeCategoryListing['business_desc'],15)) ? text_wrap($activeCategoryListing['business_desc'],15) : "";?></p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i><?php echo $featureCategoryListing['business_address']?></span>
                                        </div>

                                    </div>
                                    <?php if($activeCategoryListing['features_ads']==0){ ?>
                                    <div class="prodct-spnsor">
                                        
                                        <a href="<?php echo base_url();?>Users/featured_plan/<?php echo base64_encode($activeCategoryListing['business_id']); ?>"><i class="fa fa-hand-o-right"></i> <?php echo $sponser_this_add;?></a>
                                    </div>

                                     <?php } else {?>
                                    
                                    <div class="prodct-spnsor">
                                    	
                                    	<!-- <a alt="already featured" style="pointer-events: none; background-color: #ddd"  href="<?php echo base_url();?>Users/featured_plan/<?php echo base64_encode($activeCategoryListing['business_id']); ?>"><i class="fa fa-hand-o-right"></i> Sponsour this Add</a> -->

                                        <!-- <a><i class="fa fa-hand-o-right"></i> <?php echo $sponser_this_add;?></a> -->

                                           <a href="<?php echo base_url();?>Users/featured_plan/<?php echo base64_encode($activeCategoryListing['business_id']); ?>"><i class="fa fa-hand-o-right"></i> <?php echo $sponser_this_add;?></a>


                                    </div>
                                    <?php } ?>
                                    <?php $list_id  = base64_encode($activeCategoryListing['business_id']); ?>
                                          <div class="rtin-map-hover">
                                              <a href="<?php echo base_url(); ?>updatelisting/<?php echo $list_id; ?>"> <button class="pull-left edituserlisting_bkup"><i  class="fa fa-edit"></i></button></a> 
                                              <a href="<?php echo base_url(); ?>deactivelisting/<?php echo $list_id; ?>"> <button class="pull-left edituserlisting_bkup"><i  class="fa fa-eye-slash"></i></button></a> 
                                              <!-- <button class="pull-right deleteuserlisting" class="<?php echo  $activeCategoryListing['business_id']; ?>" id="<?php echo $activeCategoryListing['business_id']; ?>"><i class="fa fa-trash"></i></button> --> 
                                            <!--  <a href="<?php //echo base_url(); ?>activelisting/<?php //echo $list_id; ?>"> 
                                                        <button class="pull-left edituserlisting_bkup">
                                                        <i  class="fa fa-check-circle-o"></i>
                                                        </button></a>  -->
                                           </div>
                                </div>
                                <?php }} else {?>
                                <div class="page-error">
                                    <?php echo $active_add_not_found; ?>
                                </div>

                                <?php } ?>
                            </div>
                        </div>

                        

                        <div class="tab-pane" id="tab4">
                            <div class="row">
                                
                                <?php
                    if ($inactiveCategoryListings) {
                        foreach ($inactiveCategoryListings as $inactiveCategoryListing) {
                            ?>
                                <div class="listing-sub grid-system">
                                    <div class="image-lis-sub">
                                        <img src="<?php echo base_url('uploads/business/' . $inactiveCategoryListing['business_image1']); ?>">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile1">
                                                <!-- <span class="pro-img"><img src="assets_new/images/profile.jpg"></span> -->
                                                <?php
                                                $status = $inactiveCategoryListing['status'];
                                                //echo $status;
                                                $list_id  = base64_encode($inactiveCategoryListing['business_id']);
                                                if($status==2)
                                                {
                                                ?>
                                                <!-- <a href="<?php //echo base_url(); ?>active/<?php //echo $list_id; ?>"> 
                                                <button class="btn btn-default inactive">Active</button>
                                                </a> -->

                                                <div class="rtin-map-hover" style="position: absolute;top: -225px;right: 18px;z-index: 9;">
                                                  <a href="<?php echo base_url(); ?>updatelisting/<?php echo $list_id; ?>"> <button class="pull-left edituserlisting_bkup"><i  class="fa fa-edit"></i></button></a> 
                                                  <a href="<?php echo base_url(); ?>active/<?php echo $list_id; ?>"> <button class="pull-left edituserlisting_bkup"><i  class="fa fa-eye"></i></button></a> 
                                                  <!-- <button class="pull-right deleteuserlisting" class="<?php echo  $activeCategoryListing['business_id']; ?>" id="<?php echo $activeCategoryListing['business_id']; ?>"><i class="fa fa-trash"></i></button> --> 
                                                <!--  <a href="<?php //echo base_url(); ?>activelisting/<?php //echo $list_id; ?>"> 
                                                            <button class="pull-left edituserlisting_bkup">
                                                            <i  class="fa fa-check-circle-o"></i>
                                                            </button></a>  -->
                                               </div>
                                                <?php
                                                }
                                                else
                                                {
                                                ?>
                                                <button class="btn btn-default"><?php echo $not_approve_by_admin;?></button>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                            <h3><?php echo $inactiveCategoryListing['business_title']?></h3>
                                            <!-- <span class="rating-main">
                                                <ul>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </span> -->
                                        </div>
                                        <p class="add-l"><?php 
                                        
                                            echo !empty(text_wrap($inactiveCategoryListing['business_desc'],15)) ? text_wrap($inactiveCategoryListing['business_desc'],15) : "";?></p>
                                        <div class="dsply-num">
                                            <span class="add-new"><i class="fa fa-map-marker"></i><?php echo $inactiveCategoryListing['business_address']?></span>
                                        </div>
                                    </div>
                                </div>
                                <?php }} else { ?>
                                    <div class="page-error">
                                     <?php echo $inactive_add_not_found;?>
                                </div>
                                <?php } ?>


                            </div>
                        </div>

                        <div class="tab-pane" id="tab5">
                            <div class="row">
                                <?php if($gefavads){ ?>
                                <?php 
                                foreach ($gefavads as  $value) {
                                    $business_image1 = $value['business_image1'];
                                    $business_title =  $value['business_title'];
                                    $business_desc =   $value['business_desc'];
                                
                                ?>
                                <div class="featured-box favourt-tabs listing-sub grid-system">
                                    <div class="image-lis-sub">                 
                                        <span class="feat-ad1"><i class="fa fa-heart" style=""></i></span> 
                                        <img src="<?php echo base_url(); ?>uploads/business/<?php echo $business_image1; ?> ">
                                    </div>
                                    <div class="image-txt-sub">
                                        <div class="img-txt-subhead">
                                            <div class="profile">
                                                <!-- <span class="discount-tab">$900 with Discount 10%</span> -->
                                            </div>
                                            <h3><?php echo $business_title ; ?></h3>
                                           
                                        </div>
                                        <!-- <p class="add-l"><?php //echo $business_desc;  ?></p> -->
                                        <div class="dsply-num">
                                            <span class="add-l"><?php echo $business_desc;  ?></span> 
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }

                                }else{
                                ?>
                                <div class="page-error">
                                   <?php echo $favourite_add_not_found;?>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class=" listing_show modal fade" id="popUplisting">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="main-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="log-in-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-box listindata">
                                    
   
                                    <div class="modal-body" id="listingdetail_show"> 
                                   
                                               
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
    

    $("#updatelisting").validate({
        rules: {
            title: {
                required: true
            },
            email: {
                required: true,
                email: true  
            },
            mobile:{
                required: true,
                number: true,
                minlength:10,
                maxlength:12
            },
            price:{
                required: true,
                number: true                
            },
            location:{
                required: true
                
            },
            category:{
                required: true
            },
            userfile:{
                required: true
            },
            description:{
                required: true
            },
            offer:{
                required: true
            }
        },
        messages: {
            title: {
                required: "This field is required"
            },
            email: {
                required: "This field is required",
                email: "Please Enter Valid Email Id" // <-- removed underscore
            },
            mobile:{
                required: "This field is required",
                number: "This field accepts only numbers",
                minlength: "Please insert valid phone number",
                maxlength:"Please insert valid phone number"
            },
            price:{
                required: "This field is required",
                number: "This field accepts only numbers"
                
            },
            location:{
                required: "This field is required"
            },
            category:{
                required: "This field is required"
            },
            userfile:{
                required: "This field is required"
            },
            description:{
                required: "This field is required"
            },
            offer:{
                required: "This field is required"
            }
        },
        submitHandler: function (form) { // for demo
            //alert('valid form');  // for demo
            return true;  // for demo
        }
    });
    
    
    
    var max_fields      = 4; //maximum input boxes allowed
    var imagefield      = $(".upload-sec"); //Fields wrapper
        
    var x = 1; //initlal text box count
    $(".image").click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(imagefield).append('<label><input type="file" name="userfile'+x+'" class="multiple_input" multiple=""></label>'); //add input box
        }else{
            $(".image").hide();
        }
    });
    
    
    
    var max_fields_service      = 4;
    var servicefield    = $("#service"); //Fields wrapper
    
    var i = 1; //initlal text box count
    $(".service").click(function(e){ //on add input button click
        e.preventDefault();
        if(i < max_fields_service){ //max input box allowed
            i++; //text box increment
            $(servicefield).append('<label><input type="text" name="service'+i+'" ></label>'); //add input box
        }else{
            $(".service").hide();
        }
    });
    
});
    </script>
</div>

<style >
    #popUplisting{
        z-index: 9999999;
    }
    .edituserlisting{
        background: #be2d64;
    color: #fff;
    padding: 10px 15px;
    border-radius: 20px;
    text-shadow: none;
    border: #be2d64;
    }
    span.add-l {
    color: #504e4f;
    transition: all .3s ease 0s;
    overflow: hidden;
    text-overflow: ellipsis;
    display: block;
    line-height: 19px;
    font-size: 13px;
    height: 55px;
    display: -webkit-box;
    display: -moz-box;
    display: -ms-box;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
    -moz-line-clamp: 3;
    -moz-box-orient: vertical;
    -ms-line-clamp: 3;
    -ms-box-orient: vertical;
    -o-line-clamp: 3;
    -o-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
}
</style>