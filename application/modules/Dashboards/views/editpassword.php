
<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
      $first_name  = $dashboard_content[0]['first_name'];
      $last_name  = $dashboard_content[0]['last_name_field'];
      $email  = $dashboard_content[0]['email_field'];
      $password  = $dashboard_content[0]['password_field'];
      $website_url  = $dashboard_content[0]['website_url'];
      $address  = $dashboard_content[0]['address_field'];
      $phone  = $dashboard_content[0]['phone_field'];
      $zip_code  = $dashboard_content[0]['zip_code_field'];
      $about  = $dashboard_content[0]['about_field'];
      $profile_picture  = $dashboard_content[0]['profile_picture'];
      $save_button  = $dashboard_content[0]['save_button'];
      $edit_profile  = $dashboard_content[0]['edit_profile'];
      $change_password = "Change Password";
      $new_password = "New Password";
      $old_password = "Old Password";
      $confirm_password = "Confirm Password";

   } else {
      $first_name   = $dashboard_content[0]['first_name_field_ar']; 
      $last_name  = $dashboard_content[0]['last_name_field_ar'];  
      $email  = $dashboard_content[0]['email_field_ar'];
      $password  = $dashboard_content[0]['password_field_ar'];
      $website_url  = $dashboard_content[0]['website_url_ar'];
      $address  = $dashboard_content[0]['address_field_ar'];
      $phone  = $dashboard_content[0]['phone_field_ar'];
      $zip_code  = $dashboard_content[0]['zip_code_field_ar'];
      $about  = $dashboard_content[0]['about_field_ar'];
      $profile_picture  = $dashboard_content[0]['profile_picture_ar'];
      $save_button  = $dashboard_content[0]['save_button_ar'];
      $edit_profile  = $dashboard_content[0]['edit_profile_ar'];
      $change_password = "تغيير كلمة السر";
      $new_password = "كلمة السر الجديدة";
      $confirm_password = "تأكيد كلمة المرور";
   }
?>
<div class="tab-pane" id="editpassword">
    <div class="dash-counter">
        <div class="Schedule_main_one">
            <div class="users-main">
                <h2><?php echo $change_password;?></h2>
                
                <form class="form-horizontal"  method="post" name="editpassword" id="editpasswords"  enctype="multipart/form-data">
                  

                    <div class="row">
                    
                    	<div class="chang_pass">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $old_password;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="password" class="form-control" id="old_password" placeholder="<?php echo $old_password;?>" name="old_password" value="">
                                    <span id="erorrpass"></span>
                                    
                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $new_password;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="password" class="form-control" id="user_password" placeholder="<?php echo $new_password;?>" name="user_password" value="">
                                </div>
                            </div>
                        </div>
                        </div>
                        
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $confirm_password;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="password" class="form-control" id="confirm_password" placeholder="<?php echo $confirm_password;?>" name="confirm_password" value="">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                     
                    <div class="form-group">
                        <div class="col-sm-12 sbt-btn">                                                         
                            <input type="submit" class="btn btn-default" id="editpasswordbutton" name="submit" value="<?php echo $save_button;?>">
                        </div>
                    </div>
                </form>
            </div>
        </div>                                      
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closebtn close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="center">Your Profile is Update Successfully</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="closebtn btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
