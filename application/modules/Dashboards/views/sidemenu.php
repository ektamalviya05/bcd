
<?php

//print_r("<pre/>");
//print_r($top_content)die;
?>

<div class="col-md-3 col-sm-4">
    <div class="dash-link-sub">
        <ul class="nav panel-tabs">
            <li class="active"><a href="#myprofile" data-toggle="tab"><i class="fa fa-user"></i> <?php echo $top_content[0]['my_profile']; ?></a></li>
            <li><a href="#package" data-toggle="tab"><i class="fa fa-address-card-o"></i><?php echo $top_content[0]['my_plan']; ?></a></li>
            <li><a href="#editprofile" data-toggle="tab"><i class="fa fa-pencil"></i> <?php echo $top_content[0]['edit_profile']; ?></a></li>
            <li><a href="#myad" data-toggle="tab"><i class="fa fa-address-card-o"></i> <?php echo $top_content[0]['my_add']; ?></a></li>
            <li><a href="#myoffer" data-toggle="tab"><i class="fa fa-address-card-o"></i> <?php echo $top_content[0]['my_offer_service']; ?></a></li>
            <li><a href="add_offer" ><i class="fa fa-paper-plane-o"></i> <?php echo $top_content[0]['add_offers']; ?></a></li>
            <li><a href="addservices"><i class="fa fa-star-o"></i> <?php echo $top_content[0]['add_services']; ?></a></li>								
            <li><a href="#pricelisting" data-toggle="tab"><i class="fa fa-th-list"></i><?php echo $top_content[0]['price_listing']; ?></a></li>
            <li><a href="#enquiry" data-toggle="tab"><i class="fa fa-hourglass-end"></i> <?php echo $top_content[0]['enquiry']; ?></a></li>
            <li><a href="#transaction" data-toggle="tab"><i class="fa fa-history"></i> <?php echo $top_content[0]['transaction_history']; ?> </a></li>
            <li><a href="#editpassword" data-toggle="tab"><i class="fa fa-unlock-alt" aria-hidden="true"></i> <?php echo $top_content[0]['change_password']; ?> </a></li>
           <!--  <li><a href="#favorites" data-toggle="tab"><i class="fa fa-hourglass-end"></i> <?php echo $top_content[0]['my_fav']; ?></a> -->
            <li><a href="logout"><i class="fa fa-sign-out"></i> <?php echo $top_content[0]['logout']; ?></a></li>
        </ul>
    </div>
</div>