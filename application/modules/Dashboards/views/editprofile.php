
<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
      $first_name  = $dashboard_content[0]['first_name'];
      $last_name  = $dashboard_content[0]['last_name_field'];
      $email  = $dashboard_content[0]['email_field'];
      $password  = $dashboard_content[0]['password_field'];
      $website_url  = $dashboard_content[0]['website_url'];
      $address  = $dashboard_content[0]['address_field'];
      $phone  = $dashboard_content[0]['phone_field'];
      $zip_code  = $dashboard_content[0]['zip_code_field'];
      $about  = $dashboard_content[0]['about_field'];
      $profile_picture  = $dashboard_content[0]['profile_picture'];
      $save_button  = $dashboard_content[0]['save_button'];
      $edit_profile  = $dashboard_content[0]['edit_profile'];

      $country_name  = "Country";
      $city_name     = "City";
      $select_country = "Select Country";
   } else {
      $first_name   = $dashboard_content[0]['first_name_field_ar']; 
      $last_name  = $dashboard_content[0]['last_name_field_ar'];  
      $email  = $dashboard_content[0]['email_field_ar'];
      $password  = $dashboard_content[0]['password_field_ar'];
      $website_url  = $dashboard_content[0]['website_url_ar'];
      $address  = $dashboard_content[0]['address_field_ar'];
      $phone  = $dashboard_content[0]['phone_field_ar'];
      $zip_code  = $dashboard_content[0]['zip_code_field_ar'];
      $about  = $dashboard_content[0]['about_field_ar'];
      $profile_picture  = $dashboard_content[0]['profile_picture_ar'];
      $save_button  = $dashboard_content[0]['save_button_ar'];
      $edit_profile  = $dashboard_content[0]['edit_profile_ar'];

      $country_name  = "بلد";
      $city_name     = "مدينة";
      $select_country = "حدد الدولة";
   }
?>
<div class="tab-pane" id="editprofile">
    <div class="dash-counter">
        <div class="Schedule_main_one">
            <div class="users-main">
                <h2><?php echo $edit_profile;?></h2>
                <form class="form-horizontal"  method="post" name="editprofile" id="editprofiles"  enctype="multipart/form-data">
                  

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $first_name;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_firstname" placeholder="First Name" name="user_firstname" value="<?php echo !empty($user_detail['user_firstname'])?$user_detail['user_firstname']:"";?>">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $last_name; ?><span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_lastname" placeholder="Last Name" name="user_lastname" value="<?php echo !empty($user_detail['user_lastname'])?$user_detail['user_lastname']:"";?>">
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $email;?> :</label>
                                    <input type="text" class="form-control" id="user_email" placeholder="Email" name="user_email" value="<?php echo !empty($user_detail['user_email'])?$user_detail['user_email']:"";?>" readonly>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $country_name;?><span style="color: #be2d63">*</span> :</label>
                                   <select name="user_country" id="user_country">
                                                <option value=""><?php echo $select_country;?></option>
                                            
                                                 <?php 
                                                //print_r($country);
                                                 foreach($country as $value1) {

                                                    if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $country_name  = $value1['name'];
                                 
                                                                  } else{
                                                                    $country_name  = $value1['arabic'];

                                                                  }
                                                ?>
                                                <option value="<?php echo $value1['name']; ?>" <?php if($value1['name']==
                                                $user_detail['user_country']){ echo "selected"; } ?>><?php  echo $country_name; ?></option>
                                                <?php
                                                }
                                                ?> 
                                            </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $city_name;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_city" placeholder="<?php echo $city_name;?>" name="user_city" value="<?php echo !empty($user_detail['user_city'])?$user_detail['user_city']:"";?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $address;?> <span style="color: #be2d63">*</span> :</label>
                                    <!-- //id="autocomplete" -->
                                    <input type="text" class="form-control"  placeholder="Address" name="user_address" value="<?php echo !empty($user_detail['user_address'])?$user_detail['user_address']:"";?>"
                                    id="<?php echo !empty($user_detail['user_address'])?"autocomplete1":"autocomplete";?>">
                                </div>
                            </div>
                        </div>
                       
                    </div>
                    <div class="row">
                        <!--  <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $password;?> :</label>
                                    <input type="password" class="form-control" id="password" placeholder="" name="password" autocomplete="off">
                                </div>
                            </div>
                        </div> -->
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $website_url;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="website" placeholder="" name="website" value="<?php echo !empty($user_detail['website'])?$user_detail['website']:"";?>">
                                </div>
                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $phone;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_phone" placeholder="Phone Number" name="user_phone" value="<?php echo !empty($user_detail['user_phone'])?$user_detail['user_phone']:"";?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-ms-10 col-xs-9">
                                    <div class="uploader-first">
                                        <div class="upload-sec">
                                            <span><?php echo $profile_picture;?> :</span>
                                            <label>
                                                <input type="file" id="pictureprofiles" name="picture" class="multiple_input" >
                                                <span id="lblError" style="color: red;"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 col-xs-3 uploaded_img">
                                    
                                       <?php if(empty($user_detail['user_profile_pic'])){?>
                                    <img  width="50" height="50" src="assets_new/images/avatar.png" alt=""> 
                    <?php } else{ ?>
                        <img width="50" height="50"  src="uploads/profile/<?php echo $user_detail['user_profile_pic'];?>" alt=""> 
                   <?php }?>
                                
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        
                        
                       
                    </div>
                    
                    <div class="row">
                        <!--  <div class="col-md-6">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for=""><?php echo $zip_code;?><span style="color: #be2d63">*</span> :</label>
                                    <input type="text" class="form-control" id="user_zipcode" placeholder="Zip code" name="user_zipcode" value="<?php echo !empty($user_detail['user_zipcode'])?$user_detail['user_zipcode']:"";?>">
                                </div>
                            </div>
                        </div> -->
                       <!--  <div class="col-md-12">
                            <div class="form-group box_txtAre">
                                <div class="col-md-12">
                                    <label for=""><?php echo $about;?><span style="color: #be2d63">*</span> :</label>
                                    <div class="te-area">
                                        <textarea maxlength="500" name="about" id="about"><?php echo !empty($user_detail['about'])?$user_detail['about']:"";?></textarea>
                                    </div>
                                </div>
                            </div>
                        </div> -->
                        
                    </div>
             
                    <div class="form-group">
                        <div class="col-sm-12 sbt-btn">															
                            <input type="submit" class="btn btn-default up_but" id="editprofilebutton" name="submit" value="<?php echo $save_button;?>">
                        </div>
                    </div>
                </form>
            </div>
        </div>										
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="closebtn close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
            </div>
            <div class="modal-body">
                <p class="center">Your Profile is Update Successfully</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="closebtn btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
