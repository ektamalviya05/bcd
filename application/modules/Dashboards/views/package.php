<?php

//print_r("<pre/>");
//print_r($listing_content);die;
if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {

      //$transaction_details_field  = $dashboard_content[0]['transaction_field_en'];
      //$serial_field  = $dashboard_content[0]['serail_field_en'];
      //$transcation_date_field  = $dashboard_content[0]['tranction_date_en'];
      //$user_name_field  = $dashboard_content[0]['user_name_field_en'];
      //$package_name_field  = $dashboard_content[0]['package_name_field_en'];
      //$transaction_no  = $dashboard_content[0]['traction_no_en'];

      //$buy_field  = $dashboard_content[0]['buy_field_en'];
      //$current_plan_field  = $dashboard_content[0]['current_plan_field_en'];

       $business_package  = "Business Packages";
       $featured_package  = "Featured Package";
       $serial_no  = 'Serial No';
       $package_name  = 'Package Name';
       $Price  = 'Price';
       $start_date  = 'Start Date';
       $end_date  = 'End Date';
       $feature_package_not_available = "Sorry Featured Packages are not available !!";
       $business_package_not_available = "Sorry Business Packages are not available !!";

       $upgrade_plan = "Upgrade Plan";
       $action = "Action";

} else {
      //$buy_field  = $dashboard_content[0]['buy_field_ar'];
     // $current_plan_field  = $dashboard_content[0]['current_plan_field_ar'];


      //$transaction_details_field  = $dashboard_content[0]['transaction_field_ar'];
      //$serial_field  = $dashboard_content[0]['serail_field_ar'];
     // $transcation_date_field  = $dashboard_content[0]['tranction_date_ar'];
      //$user_name_field  = $dashboard_content[0]['user_name_field_ar'];
      //$package_name_field  = $dashboard_content[0]['package_name_field_ar'];
      //$transaction_no  = $dashboard_content[0]['traction_no_ar'];
       $business_package  = "الحزم التجارية";
       $featured_package   = "حزمة مميزة";
       $serial_no  = 'الرقم التسلسلي';
       $package_name  = 'اسم الحزمة';
       $Price  = 'السعر';
       $start_date  = 'تاريخ البدء';
       $end_date  = 'تاريخ الانتهاء';
       $feature_package_not_available = "عذرا المميز الحزم غير متوفرة !!";
       $business_package_not_available = "عذرا حزم الأعمال غير متوفرة!";   
       $upgrade_plan = "خطة الترقية";
       $action = "عمل";
}
?>
<div class="tab-pane" id="package">
    <div class="user-plan">
        <h4 class="package_had"><?php echo $business_package;?></h4>
        <div class="user-table table-responsive">
          <?php if($mypackage) { ?>
          <table class="table table-striped table-bordered">
            <tbody>                              
              <tr >
                <th><?php echo $serial_no;?></th>
                <th><?php echo $package_name;?></th>
                <th><?php echo $Price;?></th>
               <th><?php echo $start_date;?></th>
                <th><?php echo $end_date;?></th>
                  <th><?php echo $action;?></th>
              </tr>
               <?php $i=1;?>
              <?php foreach($mypackage as $value) {?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $value->item_name;?></td>
                <td><?php echo "$".$value->amt;?></td>
                <td><?php echo date("d F, Y", strtotime($value->free_start)); ?></td>
                <td><?php echo date("d F, Y", strtotime($value->free_end)); ?></td>
                  <td><a href="<?php echo base_url(); ?>Dashboards/upgradePlan"><button class="curent-lagega_sec"><?php echo $upgrade_plan;?></button></a></td>
              </tr>
              <?php $i++; } ?>
                
               
            </tbody>
          </table>
          <?php } else { ?>
          <div class="pagke_msg">
            <?php echo $business_package_not_available; ?>
        
         </div>
            
          <?php } ?>
        </div>
    </div>
    <div class="user-plan">
        <h4 class="package_had"><?php echo $featured_package;?></h4>
        <div class="user-table table-responsive">

          <?php if($featurepackage) { ?>
          <table class="table table-striped table-bordered">
            <tbody>  
                                        
              <tr >
                <th><?php echo $serial_no;?></th>
                <th><?php echo $package_name;?></th>
                <th><?php echo $Price;?></th>
                <th><?php echo $start_date;?></th>
                <th><?php echo $end_date;?></th>  
                <th><?php echo $action;?></th>                              
              </tr>
             
              <?php $i=1;?>
              <?php foreach($featurepackage as $value1) {?>
              <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $value1->item_name;?></td>
                <td><?php echo "$".$value1->amt;?></td>
                <td><?php echo date("d F, Y", strtotime($value1->free_start)); ?></td>
                <td><?php echo date("d F, Y", strtotime($value1->free_end)); ?></td>

                <td><a href="<?php echo base_url(); ?>Dashboards/upgradePlan"><button class="curent-lagega_sec">Upgrade Plan</button></a></td>
                <!-- <td>10 June 2018</td>
                <td>20 June 2018</td> -->
              </tr>
                <?php $i++;  } ?>     
            </tbody>
          </table>
          <?php } else { ?>
          <div class="pagke_msg">
            <?php echo $feature_package_not_available; ?>
        
         </div>
            
          <?php } ?>

        </div>
    </div>
</div>
