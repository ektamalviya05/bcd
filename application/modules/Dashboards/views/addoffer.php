<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>

<?php

   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {

         $add_more_image       = "Add More Images";
         $update_offer         = "Update Offers";
         $select_business      = "Select Business";
         $select_related_business      = "Selected Related Business";

         $upload_size_tabel = "Upload Related images (Note : Offer Image size should be 500kb)";

         $back  = "Back";

         $offer_validity_start_date = "Offer validity Start Date";

         $offer_validity_end_date = "Offer validity End Date";

         

         
         //$sponsour             = "Sponsour this Add";

    }else {

         $add_more_image  = "إضافة المزيد من الصور";
         $update_offer    = "تحديث العروض";
         $select_business    = "حدد الأعمال";
         $select_related_business      = "الأعمال ذات الصلة المختارة";

         $upload_size_tabel = "تحميل صور ذات صلة (ملاحظة: يجب أن يكون حجم الصورة 500 كيلو بايت)";
         $back  = "الى الخلف";

         $offer_validity_start_date = "صلاحية العرض تاريخ البدء";

         $offer_validity_end_date = "صلاحية العرض تاريخ الانتهاء";


         //$sponsour             = "Sponsour this Add";
    }
?>

<div style="padding:50px 0;background: #fff;">
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="listing-add">
                        <form action="<?php echo base_url();?>Dashboards/insert_offer" enctype= "multipart/form-data" method="post" name="addoffer" id="addoffer">
                          <?php
                          if($this->session->flashdata('error')){
                            echo "<span class='error'>";
                            echo $this->session->flashdata('error');
                            echo "</span>" ;
                          }
                          else{
                            echo "";
                          }
                          
                         
                          ?>
                            <?php 
                             // foreach ($business as  $valuebusiness) {
                             //  $business_image1 = $valuebusiness['business_image1'];
                             //  $business_image2 = $valuebusiness['business_image2'];
                             //  $business_image3 = $valuebusiness['business_image3'];
                             //  $business_image4 = $valuebusiness['business_image4'];
                             //  $address_type = $valuebusiness['address_type'];
                             //  $business_city = $valuebusiness['business_city'];
                            ?>

                            <div class="row">
                              <div class="col-sm-12">
                                <span><?php echo $select_business;?></span>
                                  <label class="noclass add_offr_sec">
                                     <select name="business_id" id="business_id">
                                      <option value=""><?php echo $select_related_business?></option>
                                       <?php
                                       foreach ($business as  $valuebusiness) {
                                       ?>
                                       <option value="<?php echo $valuebusiness['business_id'] ;?>"><?php echo $valuebusiness['business_title'];  ?></option>
                                       <?php
                                       }
                                       ?>
                                     </select>
                                  </label>
                              </div>
                           </div>


                          <div class="row">
                              <div class="col-sm-12">
                                <span><?php echo $offer_validity_start_date;?></span>
                                  <label class="noclass add_offr_sec">
                                     <input type="Date" name="start_date" id="start_date" placeholder="Enter Date">
                                  </label>
                              </div>
                           </div>

                          <div class="row">
                              <div class="col-sm-12">
                                <span><?php echo $offer_validity_end_date;?></span>
                                  <label class="noclass add_offr_sec">
                                     <input type="Date" name="end_date" id="end_date" placeholder="Enter Date">
                                  </label>
                              </div>
                          </div>
                          

                            <div class="row">
                              <div class="col-sm-12">
                                <div class="uploader-first ading-afer">
                                  <div class="upload-sec1">
                                  <span><?php echo $upload_size_tabel;?></span>
                                    <label class="uplodfirstimg ">
                                      <div class="img-prw-boxtwo">
                                        <img id="output" class="imgclas">
                                      </div>
                                      <input type="file" name="userfile[]" class="multiple_input"  onchange="loadFile(event)" >
                                                                          </label>
                                  </div>
                                  <button class="add-more-img imageoffer"><?php echo $add_more_image;?></button>
                                </div>
                              </div>
                            </div>

                              

                          <div class="row">
                            <div class="col-sm-12 sbt-btn">

                              <!-- <input type="submit" name="submit" class="back_sec" value="back" -->
                            <a href="javascript:window.history.back()" class="back_sec"><i class="fa fa-arrow-left"></i> <?php echo $back; ?></a>
                              <input type="submit" name="submit" value="<?php echo $update_offer ;?>">
                             
                            </div>
                          </div>
                        </form>
                       <?php 
                        // }
                        ?>
                    </div>
                
                </div>
                <div class="col-sm-3">
                    <div class="right-add">
                        <!-- Google map auto fill code start -->
                        <div id="map_canvas"></div>
                        <!-- Google map auto fill code close -->

                        <img src="<?php echo base_url();?>assets/img/Property-Ad.jpg">
                    </div>
                </div>
            </div>
            
        </div>      
    </section>

    <!-- Google map auto fill code start -->
    <style>
        #map_canvas {         
            height: 400px;         
            width: 400px;         
            margin: 0.6em;       
        }
    </style>
    <!-- Google map auto fill code close -->

</div>
<script>
    $(function() {
            var e;
            e = $('.search-form input[name="location"]')[0];
            return new google.maps.places.Autocomplete(e, {
                types: ["geocode"]
            })

    });

    function loadFile (event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
  function loadFil (val) {
    var output = val;
    //alert(output);
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  // $(".previw").change(function(e){
  //    var output = $(this).data("id");
  //   alert(output);
  //   output.src = URL.createObjectURL(event.target.files[0]);
  // }




  // function loadFile1 (event) {
  //   var output1 = document.getElementById('output1');
  //   output1.src = URL.createObjectURL(event.target.files[0]);
  // };

  // function loadFile2 (event) {
  //   var output2 = document.getElementById('output2');
  //   output2.src = URL.createObjectURL(event.target.files[0]);
  // };

  // function loadFile3 (event) {
  //   var output3 = document.getElementById('output3');
  //   output3.src = URL.createObjectURL(event.target.files[0]);
  // };

  // function loadFile4 (event) {
  //   var output4 = document.getElementById('output4');
  //   output4.src = URL.createObjectURL(event.target.files[0]);
  // };


    </script>