
<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {

       $no_enquiry = "No Enquiry";
   }else {
       $no_enquiry = "لا يوجد استفسار";
   }
?>   
<div class="tab-pane" id="enquiry">
    <div class="mes-messages">
        <ul>
            <?php 
            if($getenquirylisting)
            {
            foreach ($getenquirylisting as $value) {
               
               $created = date("d F, Y H:i", strtotime($value['created']));
               $name = $value['name'] ;
               $message = $value['message'] ;
               $id = $value['id'];
            ?>

            <li>
                <a class="detail_enquiry" data-toggle="modal" data-target="" id="<?php echo $id;  ?>" >
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name"><?php echo $name ; ?><span class="date"><?php echo $created ?></span></h4>
                            <p class="msg-show"><?php echo $message ; ?></p>

                        </div>
                    </div>
                </a>
            </li>
            <?php 
            }
            }
            else
            {
            ?>
             <li>
                <a class="detail_enquirynew" data-toggle="modal" data-target=""  >
                    <div class="msg-sub-list">
                        <div class="msg-pro-pic">
                            <i class="fa fa-envelope-o"></i>
                        </div>
                        <div class="msg-text">
                            <h4 class="usr-name"><span class="date"></span></h4>
                            <p class="msg-show"><?php echo $no_enquiry;?></p>

                        </div>
                    </div>
                </a>
            </li>
            <?php 
            
            }
            ?>    
        </ul>
    </div>
</div>



<div class="login-new-modal enquiry_show modal fade" id="popUpshow">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="main-content">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="log-in-1">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-box log-in-form">
                                    <div class="modal-body" id="enquiry_show">  
               

                                   
                                   </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<style>
    #popUpshow{
      z-index: 999999999;
    }
</style>

 
 
