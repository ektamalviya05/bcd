<script src="https://maps.googleapis.com/maps/api/js?sensor=false&libraries=places"></script>
<?php
   if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
      $title  = $listing_content[0]['title_field_english'];
      $email  = $listing_content[0]['email_field_english'];
      $mobile  = $listing_content[0]['mobile_field_english'];
      $feature  = $listing_content[0]['feature_field_en'];
      $category_text  = $listing_content[0]['category_field_en'];
      $related_image_text  = $listing_content[0]['related_image_en'];
      $description  = $listing_content[0]['descripiton_field_en'];
      $offer_image  = $listing_content[0]['offer_image_en'];
      $yes_field  = $listing_content[0]['yes_field_en'];
      $no_field  = $listing_content[0]['no_field_en'];
      $location_field  = $listing_content[0]['location_field_en'];
      $by_location_field  = $listing_content[0]['by_location_field_en'];
      $by_location_google_field  = $listing_content[0]['by_google_field_en'];
      $business_address  = $listing_content[0]['business_address_en'];
      $country_field  = $listing_content[0]['country_en'];
      $city_field  = $listing_content[0]['city_en'];
      $add_more_field  = $listing_content[0]['add_more_image_button_en'];
      $add_button_field  = $listing_content[0]['add_listing_button_en'];

      $business_sub_category_text  = "Business Sub Category";
      $select_business_sub_category  = "Select Business Sub Category";
      $main_image = "Main image";
      $business_image_one = "Business Image One";
      $business_image_two = "Business Image Two";
      $business_image_three = "Business Image Three";
      $business_image_four = "Business Image Four";

      $optional_image = "Optional Images";

      $update_listing = "Update Listing";

   } else {
      $title  = $listing_content[0]['title_field_ar'];  
      $email  = $listing_content[0]['email_field_ar'];
      $mobile  = $listing_content[0]['mobile_field_ar'];
      $feature  = $listing_content[0]['feature_field_ar'];
      $category_text  = $listing_content[0]['category_field_ar'];
      $related_image_text  = $listing_content[0]['related_image_ar'];
      $description  = $listing_content[0]['descripiton_field_ar'];
      $offer_image  = $listing_content[0]['offer_image_ar'];
      $yes_field  = $listing_content[0]['yes_field_ar'];
      $no_field  = $listing_content[0]['no_field_ar'];
      $location_field  = $listing_content[0]['location_field_ar'];
      $by_location_field  = $listing_content[0]['by_location_field_ar'];
      $by_location_google_field  = $listing_content[0]['by_google_field_ar'];
      $business_address  = $listing_content[0]['business_address_ar'];
      $country_field  = $listing_content[0]['country_ar'];
      $city_field  = $listing_content[0]['city_ar'];
      $add_more_field  = $listing_content[0]['add_more_image_button_ar'];
      $add_button_field  = $listing_content[0]['add_listing_button_ar'];

      $business_sub_category_text  = "فئة فرعية الأعمال";
      $select_business_sub_category  = "حدد فئة الأعمال الفرعية";
      $main_image = "الصورة الرئيسية";
      $business_image_one = "صورة أعمال واحدة";
      $business_image_two = "صورة العمل الثانية";
      $business_image_three = "صورة العمل ثلاثة";
      $business_image_four = "صورة العمل الرابعة";

      $optional_image = "صور اختيارية";
      $update_listing = "تحديث القائمة";
  }
?>
<div style="padding:50px 0;background: #fff;">
    <section class="upadate_listing_page">
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="listing-add">
                        <form action="<?php echo base_url();?>Dashboards/update_listing" enctype= "multipart/form-data" method="post" name="editlisting" id="editlisting">

                              <?php 
                                /* if(isset($this->session->flashdata('success'))){
                                  echo $this->session->flashdata('success');                
                                }else if(isset($this->session->flashdata('error'))){
                                  echo $this->session->flashdata('error');
                                }else{
                                  echo "";
                                } */

                                if($this->session->flashdata('error')){
                                  echo "<span class='error'>";
                                  echo $this->session->flashdata('error');
                                  echo "</span>" ;
                                }
                                else{
                                  echo "";
                                }
                                
                                ?>
                            <?php 
                             foreach ($business as  $valuebusiness) {
                              $business_image1 = $valuebusiness['business_image1'];
                              $business_image2 = $valuebusiness['business_image2'];
                              $business_image3 = $valuebusiness['business_image3'];
                              $business_image4 = $valuebusiness['business_image4'];
                              $business_image5 = $valuebusiness['business_image5'];
                              $business_image6 = $valuebusiness['business_image6'];
                              $business_image7 = $valuebusiness['business_image7'];
                              $business_image8 = $valuebusiness['business_image8'];
                              $business_image9 = $valuebusiness['business_image9'];
                              $business_image10 = $valuebusiness['business_image10'];
                              $address_type = $valuebusiness['address_type'];
                              $business_city = $valuebusiness['business_city'];
                              $business_logo = $valuebusiness['business_logo'];
                            ?>
                            <div class="row">
                                <div class="col-sm-6">
                                    <label><?php echo $title;?><span style="color: #be2d63">*</span></label>
                                    <input style="cursor: not-allowed;" type="text" name="title" id="title" value="<?php echo $valuebusiness['business_title'] ?>" readonly >
                                     <input type="hidden" name="business_id" id="business_id" value="<?php echo $valuebusiness['business_id'] ?>" >
                                      <input type="hidden" name="business_image1" id="business_image1" value="<?php echo $valuebusiness['business_image1'] ?>" >
                                      <input type="hidden" name="business_image2" id="business_image2" value="<?php echo $valuebusiness['business_image2'] ?>" >
                                       <input type="hidden" name="business_image3" id="business_image3" value="<?php echo $valuebusiness['business_image3'] ?>" >
                                      <input type="hidden" name="business_image4" id="business_image4" value="<?php echo $valuebusiness['business_image4'] ?>" >
                                      <input type="hidden" name="business_image5" id="business_image5" value="<?php echo $valuebusiness['business_image5'] ?>" >
                                      <input type="hidden" name="business_image6" id="business_image6" value="<?php echo $valuebusiness['business_image6'] ?>" >
                                      <input type="hidden" name="business_image7" id="business_image7" value="<?php echo $valuebusiness['business_image7'] ?>" >
                                      <input type="hidden" name="business_image8" id="business_image8" value="<?php echo $valuebusiness['business_image8'] ?>" >
                                      <input type="hidden" name="business_image9" id="business_image9" value="<?php echo $valuebusiness['business_image9'] ?>" >
                                      <input type="hidden" name="business_image10" id="business_image10" value="<?php echo $valuebusiness['business_image10'] ?>" >
                                      <input type="hidden" name="business_logo" id="business_logo" value="<?php echo $valuebusiness['business_logo'] ?>" >
                                </div>
                                
                                <div class="col-sm-6">
                                    <label><?php echo $email;?><span style="color: #be2d63">*</span></label>
                                    <input type="email" name="email" id="email" value="<?php echo $valuebusiness['business_email']; ?>">
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <label><?php echo $mobile;?><span style="color: #be2d63">*</span></label>
                                    <input type="text" name="mobile" id="mobile" value="<?php echo $valuebusiness['business_mobile'];?>">
                                </div>
                               
                               <div class="f-login col-sm-6">

                                    <label><?php echo $category_text;?><span style="color: #be2d63">*</span></label>
                                    
                                    <div class="cat-sel">
                                        <select name="category" id="cat_id" style="cursor: not-allowed;" disabled>
                                            <?php 
                                                $count= count($category);
                                                for($i=0; $i<$count; $i++){

                                                  if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $category_name  = $category[$i]->cat_name;
                                 
                                                                  } else{
                                                                    $category_name  = $category[$i]->cat_namea;

                                                             }

                                                    
                                            ?>
                                            <option value="<?php 
                                                            if(isset($category[$i]->cat_id))
                                                            {
                                                                echo $category[$i]->cat_id;
                                                            }else{
                                                                echo "";
                                                            }
                                                            ?>" <?php if($category[$i]->cat_id== $valuebusiness['cat_id']){ echo "selected" ;} ?>>
                                                            <?php 
                                                            if(isset($category[$i]->cat_name))
                                                            {
                                                                echo $category_name;
                                                            }else{
                                                                echo "No category";
                                                            }
                                                            ?>
                                            </option>
                                                <?php } ?>                                          
                                        </select>
                                    </div>                                  
                                </div>
                               
                            </div>

                            <div class="row">
                                <!--div class="col-sm-6">
                                    <label>Location</label>
                                    <input type="text" name="location" id="location" value="">
                                </div-->
                                

                                <div class="f-login col-sm-6">
                                  <label><?php echo $business_sub_category_text;?>:</label>
                                  <div class="cat-sel">
                                    <select class="" id="sub_cat_id" name="sub_cat_id" style="cursor: not-allowed;" disabled>
                                       <option value=''><?php echo $select_business_sub_category;?> </option>
                                       <?php foreach ($subcategory as $getcategory1) {
                                        if($getcategory1['cat_parent']==$valuebusiness['cat_id'])
                                        {

                                          if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $sub_category_name  = $getcategory1['cat_name'];
                                 
                                                                  } else{
                                                                    $sub_category_name  = $getcategory1['cat_namea'];

                                                             }


                                        ?>
                                       <option value="<?php echo $getcategory1['cat_id']; ?>" <?php if($getcategory1['cat_id']==
                                       $valuebusiness['sub_cat_id']){ echo "selected";} ?>><?php echo $sub_category_name; ?></option>
                                       <?php
                                       }
                                       } ?>
                                    </select>
                                   </div>
                              </div>
                                
                                   
                                    
                                </div>

                                <div class="row">
                               <div class="form-group ad-list-imgb edit_list_add_one">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="upload-reltd-img"><?php echo "Image Icon";?></label>
                                    <!-- <label><?php //echo "Image Icon";?>:</label><span></span>  -->
                                  </div>

                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness['business_logo'])
                                    {
                                    ?>
                                    <div class="business-img-sec">
                                    <img id="outpute" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_logo"]; ?>' > </div>
                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec">
                                    <img id="outpute" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>      

                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="imageIcon" class="form-control" onchange="loadFilee(event)" />

                                    <div id="flupmsgone"></div>
                                  </div>
                                </div>
                              </div>

                                <div class="row">
                                <div class="f-login col-sm-12">
                                        <label><?php echo $location_field;?><span style="color: #be2d63">*</span></label>
                                        <div id="myRadioGroup" class="text-left">
                                          <span>
                                            <input type="radio" name="cars" checked="checked" value="2" <?php if($address_type == 0){echo "checked"; } ?> />
                                            <label for="yes"><?php echo $by_location_field;?></label>
                                          </span>
                                          <span>
                                            <input type="radio" name="cars" value="3" <?php if($address_type == 1){echo "checked"; } ?> />
                                            <label for="yes"><?php echo $by_location_google_field;?></label>
                                          </span>
                                        </div>
                                    </div>
                                <div class="col-md-12">
                                <div class="myRadioGroup-box">  
                                <div id="Cars2" class="desc" <?php if($address_type == 0){echo 'style="display: block;"'; } else{echo 'style="display:none;"'; } ?> >
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label><?php echo $business_address;?> </label>
                                            <input name="business_address" id="business_address" type="text" value="<?php echo $valuebusiness['business_address']; ?>" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="f-login col-sm-6">
                                            <label><?php echo $country_field; ?></label>
                                        <div class="cat-sel">
                                            <select name="business_country" id="business_country">
                                                <option>Select Country</option>
                                            
                                                 <?php 
                                                //print_r($country);
                                                 foreach($country as $value1) {


                                                  if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                                                    $country_name  = $value1['name'];
                                 
                                                                  } else{
                                                                    $country_name  = $value1['arabic'];

                                                                  }
                                                ?>
                                                <option value="<?php echo $value1['name']; ?>" <?php if($value1['name']==
                                                $valuebusiness['business_country']){ echo "selected"; } ?>><?php  echo $country_name; ?></option>
                                                <?php
                                                }
                                                ?> 
                                            </select>
                                        </div>
                                        </div>

                                        <div class="f-login col-sm-6">
                                            <label><?php echo $city_field;?></label>
                                        <div class="cat-sel">
                                          <input type="text" name="business_city" id="business_city" value="<?php echo $business_city ; ?>">
                                           
                                             
                                        </div>
                                        </div>
                                    </div>                                  
                                </div>

                                <div id="Cars3" class="desc" <?php if($address_type == 1){echo 'style="display: block;"'; } else{echo 'style="display:none;"'; } ?>>
                                    <input name="location" id="location" type="text" value="<?php echo $valuebusiness['business_address']; ?>" />
                                </div>
                              </div>
                            </div>

                              </div>

                              <div class="row">
                               <div class="form-group ad-list-imgb edit_list_add_one">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="upload-reltd-img"><?php echo $main_image;?></label>
                                    <label><?php echo $business_image_one;?>:</label><span></span> 
                                  </div>

                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness['business_image1'])
                                    {
                                    ?>
                                    <div class="business-img-sec">
                                    <img id="output" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image1"]; ?>' > </div>
                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec">
                                    <img id="output" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>      

                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile" class="form-control" onchange="loadFile(event)" />

                                    <div id="flupmsgone"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="prvw-hed"><h2><?php echo $optional_image;?></h2></div>
                              <div class="imgp-prwv-box">
                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label><?php echo $business_image_two;?>:</label>
                                    <span></span>
                                  </div>

                                   <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image2"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                     <img  id="output1" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image2"]; ?>' >
                                   </div>
                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output1" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                  </div>

                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile2" class="form-control" onchange="loadFile1(event)"/>
                                    <div id="flupmsgtwo"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label><?php echo $business_image_three;?>:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image3"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output2" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image3"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output2" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile3" class="form-control" onchange="loadFile2(event)"/>
                                    <div id="flupmsgthree"></div>
                                  </div>
                                </div>
                              </div>
                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label><?php echo $business_image_four;?>:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image4"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output3" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image4"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output3" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile4" class="form-control" onchange="loadFile3(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>

                                 <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Business Image Five:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image5"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output5" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image5"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output5" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile5" class="form-control" onchange="loadFile5(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Business Image Six:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image6"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output6" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image6"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output6" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile6" class="form-control" onchange="loadFile6(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Business Image Seven:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image7"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output7" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image7"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output7" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile7" class="form-control" onchange="loadFile7(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Business Image Eight:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image8"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output8" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image8"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output8" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile8" class="form-control" onchange="loadFile8(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Business Image Nine:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image9"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output9" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image9"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output9" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile9" class="form-control" onchange="loadFile9(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>

                              <div class="row">
                                <div class="form-group ad-list-imgb edit_list_add">
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label>Business Image Ten:</label>
                                    <span></span>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <?php 
                                    if($valuebusiness["business_image10"])
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                    <img id="output10" src='<?php echo base_url(); ?>uploads/business/<?php echo $valuebusiness["business_image10"]; ?>' >
                                    </div>

                                    <?php 
                                    }
                                    else
                                    {
                                    ?>
                                    <div class="business-img-sec2">
                                      <img id="output10" src='<?php echo base_url(); ?>uploads/business/avatar-gray.png' >
                                    </div>
                                    <?php
                                    }
                                    ?>
                                  </div>
                                  <div class="col-md-12 col-sm-12 col-xs-12">
                                    <input type="file" name="userfile10" class="form-control" onchange="loadFile10(event)"/>
                                    <div id="flupmsgfour"></div>
                                  </div>
                                </div>
                              </div>


                              </div>
                            <div class="row hidden">
                                <div class="col-sm-12">
                                    <div class="uploader-first">
                                      <div class="upload-sec">
                                        <span></span>
                                          <label>
                                              <input type="file" name="userfile_bkup" class="multiple_input" multiple="">
                                          </label>
                                      </div>
                                      <button class="add-more-img image"><?php echo $add_more_field;?></button>
                                    </div>
                                </div>
                            </div>
                            
                            
                                                 

                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="text-box">
                                        <label><?php echo $description;?> <span style="color: #be2d63">*</span></label>
                                        <textarea class="te-box" name="description" id="description"><?php echo $valuebusiness['business_desc']; ?></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="prvw-hed hidden"><h2>Offer Image</h2></div>
                            <div class="imgp-prwv-box1">
                              <div class="row">
                             <?php
                            foreach ($offerImg as $valueImg) {
                               $offer =  $valueImg['offer_image'];
                             } 
                            ?>

                              <div class="col-md-12 col-sm-12 col-xs-12 hidden">
                                <?php 
                                if($offer)
                                {
                                ?>
                                <div class="business-img-sec2">
                                <img id="output4" src='<?php echo base_url(); ?>uploads/business/<?php echo $offer; ?>' >
                                <input type="hidden" name="offerimg" value="<?php echo $offer;  ?>">
                                </div>
                                <?php 
                                }
                                else
                                {
                                ?>
                                <div class="business-img-sec2">
                                <img id="output4" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                                <input type="hidden" name="offerimg" value="no_offer.png">
                                </div>
                                <?php
                                }
                                ?>
                              </div>
                           
                                <div class="col-sm-12 hidden">
                                    <div class="uploader-first-new">
                                      <div class="upload-sec-one">
                                        <!-- <span> <?php echo $offer_image;?></span> -->
                                          <label>
                                              <input type="file" name="offer" class="multiple_input" onchange="loadFile4(event)">
                                          </label>
                                      </div>
            
                                    </div>
                                </div>
                            </div>
                           </div>

                            

                          <div class="row">
                            <div class="col-sm-12 sbt-btn">
                              <input type="submit" name="submit" value="<?php echo $update_listing ;?>">
                             
                            </div>
                          </div>
                        </form>
                       <?php 
                        }
                        ?>
                    </div>
                
                </div>
                <div class="col-sm-3">
                    <div class="right-add">
                        <!-- Google map auto fill code start -->
                        <div id="map_canvas"></div>
                        <!-- Google map auto fill code close -->

                        <img src="<?php echo base_url();?>assets/img/Property-Ad.jpg">
                    </div>
                </div>
            </div>
            
        </div>      
    </section>

    <!-- Google map auto fill code start -->
    <style>
        #map_canvas {         
            height: 400px;         
            width: 400px;         
            margin: 0.6em;       
        }
    </style>
    <!-- Google map auto fill code close -->

</div>
<script>
    $(function() {
            var e;
            e = $('.search-form input[name="location"]')[0];
            return new google.maps.places.Autocomplete(e, {
                types: ["geocode"]
            })

    });

     function loadFile (event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile1 (event) {
    var output1 = document.getElementById('output1');
    output1.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile2 (event) {
    var output2 = document.getElementById('output2');
    output2.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile3 (event) {
    var output3 = document.getElementById('output3');
    output3.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile4 (event) {
    var output4 = document.getElementById('output4');
    output4.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile5 (event) {
    var output5 = document.getElementById('output5');
    output5.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile6 (event) {
    var output6 = document.getElementById('output6');
    output6.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile7 (event) {
    var output7 = document.getElementById('output7');
    output7.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile8 (event) {
    var output8 = document.getElementById('output8');
    output8.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile9 (event) {
    var output9 = document.getElementById('output9');
    output9.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFile10 (event) {
    var output10 = document.getElementById('output10');
    output10.src = URL.createObjectURL(event.target.files[0]);
  };

  function loadFilee (event) {
    var outpute = document.getElementById('outpute');
    outpute.src = URL.createObjectURL(event.target.files[0]);
  };
    </script>