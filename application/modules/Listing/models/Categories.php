<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Categories extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->tbName = "bdc_business";
        $this->homeCatTbl = "otd_home_cat_tab";
        $this->catTble ="otd_business_category";
	}

	/*
	* get rows from the testimonail table
	*/
    function getRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->tbName);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get relational rows
    */
    public function getRelationalRows($params = array()){


    	$this->db->select("*");
        $this->db->from($this->tbName);
        //$this->db->join($this->tbName." AS t1", "t1.cat_id = $this->tbName.cat_parent", "left");
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['bn_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert / Update into testimonals
    */

    function insertTable($opr = 1, $para = array(), $where = array()){
    	if($opr == 1){ // insert testimonial
    		$this->db->insert($this->tbName, $para);
    		$insert_id = $this->db->insert_id();
    		if($this->db->affected_rows() > 0)
    			return $insert_id;
    		else
    			return false;
    	}
    	else{ // update existing testimonial
    		$this->db->update($this->tbName, $para, $where);
            //echo $this->db->last_query();
            //die();
    		return true;
    	}
    }

    function insertTable1($opr = 1, $para = array(), $where = array()){
        if($opr == 1){ 
            $this->db->insert('bdc_offers', $para);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ 
            $this->db->update('bdc_offers', $para, $where);
          
            return true;
        }
    }

    function cat()
    {
        $this->db->select("*");
        $this->db->from("otd_business_category");
        $this->db->where('cat_parent',0);
        $this->db->where('cat_status',1);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        
        return  $result ;

    }

    function subcat()
    {
        $this->db->select("*");
        $this->db->from("otd_business_category");
        $this->db->where('cat_parent!=',0);
        $this->db->where('cat_status',1);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        
        return  $result ;

    }

     function showcity()
    {
        $this->db->select("*");
        $this->db->from("cities");
        $this->db->where('add_status!=',3);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        
        return  $result ;

    }


     function allcat()
    {
        $this->db->select("*");
        $this->db->from("otd_business_category");
        
        $this->db->where('cat_status',1);
        $query = $this->db->get();
        $result = $query->result_array();
        //echo $this->db->last_query();
        //die();
        
        return  $result ;

    }

    function getcat($cat_id=NULL)
    {
       $this->db->select('*');
        $this->db->from('otd_business_category');
        $this->db->where('cat_parent=',$cat_id);
        $this->db->where('cat_status','1');
        $query1 = $this->db->get();
        $result1 = $query1->result_array();
        //echo $this->db->last_query();
        //die();
        if($result1)
        {
            foreach ($result1 as $row) {
            ?>
            <option value="<?php echo $row['cat_id']; ?>"> <?php echo $row['cat_name']; ?></option>
            <?php
            }
        }
        else
        {
          ?>
          
          <?php 
          echo "";
        }

    }

    function getRow($params = array())
    {
         $this->db->select('*');
        $this->db->from('bdc_business'); 
         if(array_key_exists("business_id",$params))
         {
            $this->db->where('business_id',$params['business_id']);
            $query = $this->db->get();
            $result = $query->result_array();
        }
        //echo $this->db->last_query();
        //print_r($result);
         return $result;
    }

     function getRow1($params = array())
    {
         $this->db->select('*');
        $this->db->from('bdc_offers'); 
         if(array_key_exists("business_id",$params))
         {
            $this->db->where('business_id',$params['business_id']);
            $query = $this->db->get();
            $result = $query->result_array();
        }
        //echo $this->db->last_query();
        //print_r($result);
         return $result;
    }



    /*
     * get rows from the users table
     */
    function getCatRows($params = array()){
      $this->db->select('*');
        $this->db->from($this->tbName);
         //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
          $order_mode = "DESC";
          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

              $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }
        $this->db->order_by('business_id', $order_mode);

        //$this->db->where("status !=", "3");
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{  
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

   //echo $this->db->last_query();
   //die();
        return $result;
    }
    
    /*
    * get rows from the testimonail table
    */
    function getHomeTabRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->homeCatTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    public function updateHomeTab($params = array(), $where = array()){
        $this->db->update($this->homeCatTbl, $params, $where);
        // if($this->db->affected_rows() > 0){
            return true;
        // }else{
        //     return false;
        // }
    }
      function showcountry()
    {
        $this->db->select('*');
        $this->db->from('countries');
        $this->db->where('add_status!=',3);
        $que = $this->db->get();
        $result = $que->result_array();
        return $result;
    }

}
?>