<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends MX_Controller {

	public function __construct(){
	 	parent:: __construct();
	 	$userid = $this->session->userdata("user_id");
		if(empty($userid)) {redirect("Admin");}
    	$this->load->model("Categories");
		$this->load->helper('url');
    	$this->load->helper('csv');
    	$this->load->library('pagination');
		$this->load->library("form_validation");
		$this->load->library('session');
		ini_set('max_execution_time', 0); 
		ini_set('memory_limit','2048M');
  	}

  	public function index(){
	
		$data = array();
		//$con['conditions'] = array("otd_business_category.cat_status !="=>3 , "type"=>1);
		//$con['sorting'] = array("otd_business_category.cat_id"=>"DESC");

		$con['returnType'] = 'count';
		if(!empty($this->session->userdata("filtercategory1"))){

			//echo $this->session->userdata('filtercategory1');die;
			$con['filter'] = array("business_title"=>$this->session->userdata("filtercategory1"));
			///print_r("<pre/>");
		    //print_r($con);
		    //die;
		}
		$con['conditions'] = array("status !="=>3 );
		$cat_count  = $this->Categories->getCatRows($con);	


		//print_r("<pre/>");
		//print_r($cat_count);
		//die;	

		if($cat_count > 0){
			$con = null;
			$con = array();
			$data["cat_count"] = $cat_count;
			$con['returnType'] = '';
			$con['conditions'] = array("status !="=>3 );
			if(!empty($this->session->userdata("filtercategory1"))){
				$con['filter'] = array("business_title"=>$this->session->userdata("filtercategory1"));

				 //print_r("<pre/>");
		         //print_r($con);
		         //die;	
			}
			
			$data['details'] = $this->Categories->getCatRows($con);
			//print_r("<pre/>");
			//print_r($data['details']);
			//die;
		}
		else
		{
			$data['details'] = array();
		}



		$config['base_url'] = base_url()."Listing/index";
		$config['total_rows'] = $cat_count;
		$config['per_page'] = 10;
		$config['uri_segment'] = 3;
		//$config['num_links'] = $cat_count / 50;
		$config['num_links'] = 2;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '<b> Next </b>';
		$config['prev_link'] = '<b> Previous </b>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

		$con['limit'] = $config['per_page'];
		$start =($page-1) * $config['per_page'];

		if($start == 0)
		{
		  $start = 0;
		}
		else
		{
		  $start = ($start/$config['per_page'] )+1;
		}

		$con['start'] = $start;
		if(!empty($this->session->userdata("filtercategory1"))){
			$con['filter'] = array("business_title"=>$this->session->userdata("filtercategory1"));
		}
		if($cat_count > 0){
		    $data['details'] = $this->Categories->getCatRows($con);		
		    $data["record_found"] = $cat_count; 
	    }
	    else
	    {
	      	$data["record_found"] = 0; 
	    }

		$data["links"] = $this->pagination->create_links();
		$data['page'] = $page;
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Listing Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
  	}

  	function setFilterDelimiter(){

  		//print_r("<pre/>");
  		//print_r($_POST);die;
  		$category = $this->input->post("category");
  		if(!empty($category))
  			$this->session->set_userdata("filtercategory1", $category);

  		//echo $category;die;
  		redirect(base_url("Listing"));
  	}

  	function clearFilterDelimiter(){
  		$this->session->unset_userdata("filtercategory1");
  		redirect(base_url("Listing"));
  	}

  	public function add(){
  		$data = array();
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter Business Name"));
  		//$this->form_validation->set_rules("name", "namea", "trim|required", array("required"=>"Please enter Business Name"));
  		//$this->form_validation->set_rules("type", "type", "trim|required", array("required"=>"Please enter Short Description"));
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter Status"));
  		//$this->form_validation->set_rules("cat_id", "cat_id", "trim|required", array("required"=>"Please Select Category"));
  		//$this->form_validation->set_rules("sub_cat_id", "sub_cat_id", "trim|required", array("required"=>"Please Select Sub Category"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));


  		if($this->form_validation->run() == FALSE){
  			$data['category'] =  $this->Categories->cat();
  			$data['country'] = $this->Categories->showcountry();
	  		header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Listing Management');
	  		$this->template->load('admin_layout', 'contents' , 'addListing', $data);
  		}
  		else {
             $address_type = $this->input->post('cars');
  			 if($address_type == 3) {

  				$address_typ = 1;
  			 	$autoloc = $this->input->post("business_address1");
  			 	if($autoloc)
  			 	{	$autolocation = explode(', ', $autoloc);
  			 		//$latlong    =   $this->get_lat_long($autoloc);
  					//$latlong  = explode(',',$latlong);
  					//$lat   =  $latlong[0];
            		//$long  =  $latlong[1];
  			        $latlong    =   getLocationLatLng($autoloc);
           			$lat   =  $latlong['lat'];
           			$long  =  $latlong['lng'];
	  			 	$count =count($autolocation);
	  			 	$len =$count-3;
	  				$full_address = $this->input->post("business_address1");
	  				$cityId = $autolocation[$len];
	  				$country = end($autolocation); 



	  					$this->db->select('*');
	  					$this->db->from('countries');
	     	            $this->db->where('name', $country);
	                    $query = $this->db->get();
	                    $listingCount = $query->num_rows();
	                    if($listingCount)
	                     {
		                 	$country_arry = $query->result_array();

		                 	foreach ($country_arry as  $valuecountry) {
		                		$countyId = $valuecountry['id'];
		                 	}
		                }
	                 	else
	                  {
	                 	echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>This Country Not in Country Management</label>"));
						  exit;
					}



  			 	}
  			 	else
  			 	{
  			 		echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>Please Enter Address</label>"));
						  exit;
  			 	}
  			 	

  			}
  			else 
  			    
  			{
		    	$address_typ = 0;
		    	if($this->input->post("business_city") && $this->input->post("business_country") && $this->input->post("business_address") )
		    	{
		    		$cityId  = $this->input->post("business_city");
		    		$country = $this->input->post("business_country");
		    		$full_address = $this->input->post("business_address");
		    		//$latlong    =   $this->get_lat_long($full_address);
					//$latlong  = explode(',',$latlong);
					//$lat   =  $latlong[0];
				    //$long  =  $latlong[1];
				    $latlong    =   getLocationLatLng($full_address);
           			$lat   =  $latlong['lat'];
           			$long  =  $latlong['lng'];
		    	}
		    	else{
		    		echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>Please Enter Address , Country , City,  </label>"));
				  exit;

		    	}
  			        
  			}


  			
  			$data = array
  			(
  				"business_title"=>$this->input->post('name'),
  				//"business_title_arabic"=>$this->input->post('namea'),
				"business_desc"=>$this->input->post('description'),
				//"business_desc_arabic"=>$this->input->post('descriptiona'),
				"status"       =>$this->input->post('status'),
				"user_id"      =>$this->session->userdata("user_id"),
				"business_price" => $this->input->post("business_price"),
				"business_email" => $this->input->post("business_email"),
				"business_mobile" => $this->input->post("business_mobile"),
				"business_address" => $full_address,
				//"business_address_arabic" => $this->input->post("business_addressa"),		  
				"business_country" => $country,
				"business_about" => $this->input->post("business_about"),
				//"business_email" => $this->input->post("business_email"),
				"cat_id" => $this->input->post("cat_id"),
				"sub_cat_id" => $this->input->post("sub_cat_id"),
				"features_ads"   => $this->input->post("features_ads"),
				"business_city"   => $cityId,
				"address_type" => $address_typ,
				"lat"   => $lat,
				"lon"   => $long,
				'created' => date('Y-m-d H:i:s'),
				//"created"   => $long,


				);

  			if(!empty($_FILES['business_image1']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image1']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF" ){
					if(move_uploaded_file($_FILES['business_image1']['tmp_name'], $dirpath)){
						$data['business_image1'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typeone"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}

			if(!empty($_FILES['business_image2']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image2']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF"){
					if(move_uploaded_file($_FILES['business_image2']['tmp_name'], $dirpath)){
						$data['business_image2'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typetwo"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}

			if(!empty($_FILES['business_image3']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image3']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF" ){
					if(move_uploaded_file($_FILES['business_image3']['tmp_name'], $dirpath)){
						$data['business_image3'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typethree"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}


			if(!empty($_FILES['business_image4']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image4']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF"  ){
					if(move_uploaded_file($_FILES['business_image4']['tmp_name'], $dirpath)){
						$data['business_image4'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typefour"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}

			if(!empty($_FILES['offer']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['offer']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$size =$_FILES['offer']['size'] ; 
				$dirpath = './././uploads/business/'.$file_name;
				if($size  <   512000 ){
					if(move_uploaded_file($_FILES['offer']['tmp_name'], $dirpath)){
						$data1['offer_image'] = $file_name;
					}
				}
				else{
					 echo json_encode(array("status"=>1, "typefour"=>1, "msg"=>"<label class='text-danger'>Image is too Large</label>"));
				 exit;
				}
			}




			$mode = 1;
			$check = $this->Categories->insertTable($mode, $data);

			$data1['business_id'] = $check;
			$data1['user_id'] = $this->session->userdata("user_id");

			$check1 = $this->Categories->insertTable1($mode, $data1);
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Business Listing added successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Business Listing not added, Please try again.</label>"));
			}



  		}

  		

  	}
  	function get_lat_long($address){

    $address = str_replace(" ", "+", $address);

    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);

    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    //echo $lat;die;
    return $lat.','.$long;
}  

  	public function category()
	{
		$cat_id = $this->input->post('cat_id');
  		$getcat = $this->Categories->getcat($cat_id);


	}

	public function trashCategory(){
  		$id = $this->input->post("id");
		if(!empty($id)){
			$con['conditions'] = array("business_id"=>$id);
			//$check = $this->Categories->getRow($con);
			if(1){
				$array = array("status"=>$this->input->post("mode"));
				$opr = 2; //Update mode
				$check = $this->Categories->insertTable($opr, $array, array("business_id"=>$id));
				if($check)
					echo json_encode(array("status"=>200));
			}
			else{
				echo json_encode(array("status"=>1));
			}
		}
		else{
			echo json_encode(array("status"=>1));
		}
  	}

  	public function deltrash(){
  	// 	$id = $this->input->post("id");
  	// 	$this->db->where('business_id',$id);
   //      $result = $this->db->delete('bdc_business');
   //      if($result){
			// 		echo json_encode(array("status"=>200));
			// }
   //      //echo $this->db->last_query();
   //      //die();

  	$id = $this->input->post("id");
	$this->db->where('business_id',$id);
    $result = $this->db->delete('bdc_business');
    if($result)
    {
    	    $this->db->where('business_id',$id);
            $sql = $this->db->delete('bdc_offers');
             if($sql)
            {
             	$this->db->where('business_id',$id);
                $query = $this->db->delete('bdc_services');
                 if($query){
                 	echo json_encode(array("status"=>200));
                 }
            }

    }
      
		
  	}


  	public function edit($args = NULL){
  		$data = array();
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter Business Name"));
  		//$this->form_validation->set_rules("type", "type", "trim|required", array("required"=>"Please enter Short Description"));
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter Status"));
  		//$this->form_validation->set_rules("cat_id", "cat_id", "trim|required", array("required"=>"Please Select Category"));
  		//$this->form_validation->set_rules("sub_cat_id", "sub_cat_id", "trim|required", array("required"=>"Please Select Sub Category"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
  			$data['category'] =  $this->Categories->cat();
  			$data['subcategory'] =  $this->Categories->subcat();
  			$data['country'] = $this->Categories->showcountry();
  			$data['city'] = $this->Categories->showcity();
  			$data['business'] = $this->Categories->getRow(array("business_id"=>$args));
  			$data['offerImg'] = $this->Categories->getRow1(array("business_id"=>$args));
	  		header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Listing Management');
	  		$this->template->load('admin_layout', 'contents' , 'edit', $data);
  		}
  		else
  		{
  			// city country code by webteam

  			$address_type = $this->input->post('cars');
  			if($address_type == 3)
  			{
  				$address_typ = 1;
  			 	$autoloc = $this->input->post("business_address1");
  			 	if($autoloc)
  			 	{	//$autolocation = explode(', ', $autoloc);

  			        $autolocation = explode(', ', $autoloc);

  			 		//$latlong    =   $this->get_lat_long($autoloc);
  					//$latlong  = explode(',',$latlong);
  					//$lat   =  $latlong[0];
            		//$long  =  $latlong[1];
            		$latlong    =   getLocationLatLng($autoloc);
           			$lat   =  $latlong['lat'];
           			$long  =  $latlong['lng'];

	  			 	$count =count($autolocation);
	  			 	$len =$count-3;
	  				$full_address = $this->input->post("business_address1");
	  				$cityId = $autolocation[$len];
	  				$country = end($autolocation); 
	  					$this->db->select('*');
	  					$this->db->from('countries');
	     	            $this->db->where('name', $country);
	                    $query = $this->db->get();
	                    $listingCount = $query->num_rows();
	                    if($listingCount)
	                   {
		                 	$country_arry = $query->result_array();

		                 	foreach ($country_arry as  $valuecountry) {
		                		$countyId = $valuecountry['id'];
		                 	}
		              }
	                 	else
	                  {
	                 	echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>This Country Not in Country Management</label>"));
						  exit;
					}



  			 	}
  			 	else
  			 	{
  			 		echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>Please Enter Address</label>"));
						  exit;
  			 	}
  			 	

  			}
  			else 
  			    
  			{
		    	$address_typ = 0;
		    	if($this->input->post("business_city") && $this->input->post("business_country") && $this->input->post("business_address") )
		    	{
		    		$cityId  = $this->input->post("business_city");
		    		$country = $this->input->post("business_country");
		    		$full_address = $this->input->post("business_address");
		    		//$latlong    =   $this->get_lat_long($full_address);
					//$latlong  = explode(',',$latlong);
					//$lat   =  $latlong[0];
				    //$long  =  $latlong[1];
				    $latlong    =   getLocationLatLng($full_address);
           			$lat   =  $latlong['lat'];
           			$long  =  $latlong['lng'];
		    	}
		    	else{
		    		echo json_encode(array("status"=>1, "typefive"=>1, "msg"=>"<label class='text-danger'>Please Enter Address , Country , City,  </label>"));
				  exit;

		    	}
  			        
  			}

  			

  			$data = array
  			(
  				"business_title"=>$this->input->post('name'),
  				//"business_title_arabic"=>$this->input->post('namea'),
				"business_desc"=>$this->input->post('description'),
				//"business_desc_arabic"=>$this->input->post('descriptiona'),
				"status"       =>$this->input->post('status'),
				//"user_id"      =>$this->session->userdata("user_id"),
				"business_price" => $this->input->post("business_price"),
				"business_email" => $this->input->post("business_email"),
				"business_mobile" => $this->input->post("business_mobile"),
				"business_address" => $full_address,
				//"business_address_arabic" => $this->input->post("business_addressa"),		  
				"business_country" =>  $country,
				"business_about" => $this->input->post("business_about"),
				"business_email" => $this->input->post("business_email"),
				"cat_id" => $this->input->post("cat_id"),
				"sub_cat_id" => $this->input->post("sub_cat_id"),
				"features_ads"   => $this->input->post("features_ads"),
				"business_city"   => $cityId,
				"address_type" => $address_typ,
				"lat"   => $lat,
				"lon"   => $long,
				'created' => date('Y-m-d H:i:s'),
				);

  			if(!empty($_FILES['business_image1']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image1']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF"  ){
					if(move_uploaded_file($_FILES['business_image1']['tmp_name'], $dirpath)){
						$data['business_image1'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typeone"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}

			if(!empty($_FILES['business_image2']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image2']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF" ){
					if(move_uploaded_file($_FILES['business_image2']['tmp_name'], $dirpath)){
						$data['business_image2'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typetwo"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}

			if(!empty($_FILES['business_image3']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image3']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF"  ){
					if(move_uploaded_file($_FILES['business_image3']['tmp_name'], $dirpath)){
						$data['business_image3'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typethree"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}


			if(!empty($_FILES['business_image4']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['business_image4']['name'], PATHINFO_EXTENSION);        
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
				if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "JPEG"|| $ext == "gif" || $ext == "GIF"  ){
					if(move_uploaded_file($_FILES['business_image4']['tmp_name'], $dirpath)){
						$data['business_image4'] = $file_name;
					}
				}
				else{
					//echo json_encode(array("status"=>1, "typefour"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					//exit;
				}
			}

			if(!empty($_FILES['offer']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['offer']['name'], PATHINFO_EXTENSION);
				$size =$_FILES['offer']['size'] ;       
				$file_name = $random.".".$ext;
				$dirpath = './././uploads/business/'.$file_name;
			    if($size  <   512000 ){
					if(move_uploaded_file($_FILES['offer']['tmp_name'], $dirpath)){
						$data1['offer_image'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "typefour"=>1, "msg"=>"<label class='text-danger'>Image is too large</label>"));
					 exit;
				}
			}

			$mode = 2;
			$check = $this->Categories->insertTable($mode, $data,array("business_id"=>$args));

			$check1 = $this->Categories->insertTable1($mode, $data1,array("business_id"=>$args));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Business Listing edit successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Business Listing not edit, Please try again.</label>"));
			}



  		}
  	}



  	public function view($args = NULL){
  		$data = array();
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</lable>");
  		$this->form_validation->set_rules("name", "name", "trim|required", array("required"=>"Please enter Business Name"));
  		//$this->form_validation->set_rules("type", "type", "trim|required", array("required"=>"Please enter Short Description"));
  		$this->form_validation->set_rules("status", "status", "trim|required", array("required"=>"Please enter Status"));
  		//$this->form_validation->set_rules("cat_id", "cat_id", "trim|required", array("required"=>"Please Select Category"));
  		//$this->form_validation->set_rules("sub_cat_id", "sub_cat_id", "trim|required", array("required"=>"Please Select Sub Category"));
  		$this->form_validation->set_rules("description", "description", "trim|required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
  			$data['category'] =  $this->Categories->cat();
  			$data['allcategory'] =  $this->Categories->allcat();
  			$data['business'] = $this->Categories->getRow(array("business_id"=>$args));
	  		header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Listing Management');
	  		$this->template->load('admin_layout', 'contents' , 'view', $data);
  		}
  		
  		
  	}



  	public function trashListing(){
  		$data = array();
		$con['conditions'] = array("status"=>3);
		$con['sorting'] = array("business_id"=>"DESC");
		$data['details'] = $this->Categories->getRelationalRows($con);		
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Listing Management');
	    $this->template->load('admin_layout', 'contents' , 'trashlist', $data);
  	}

  	

  	public function homeTab(){
  		$data = array();
		$data['details'] = $this->Categories->getHomeTabRows();
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Category Home Tab Management');
	    $this->template->load('admin_layout', 'contents' , 'cathometablist', $data);
  	}

  	public function editCatTab($args = NULL){
  		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
  		$this->form_validation->set_rules("name", "name", "required", array("required"=>"Please enter %s"));
  		$this->form_validation->set_rules("editid", "tab id", "required", array("required"=>"Please enter %s"));
  		if($this->form_validation->run() == FALSE){
	  		$data = array();
			$data['cat'] = $this->Categories->getHomeTabRows(array("tab_id"=>$args));		
			header("Access-Control-Allow-Origin: *");
			$this->template->set('title', 'Category Home Tab Management');
		    $this->template->load('admin_layout', 'contents' , 'editcattab', $data);
		}else{
			$data = array("tab_name"=>$this->input->post('name'));
			if(!empty($_FILES['cat_file']['name'])){
				$random = $this->generateRandomString(10);
				$ext = pathinfo($_FILES['cat_file']['name'], PATHINFO_EXTENSION);
				$file_name = $random.".".$ext;
				$dirpath = './././assets/img/category/'.$file_name;
				if($ext == "png" || $ext == "PNG" ){
					if(move_uploaded_file($_FILES['cat_file']['tmp_name'], $dirpath)){
						$data['tab_image'] = $file_name;
					}
				}
				else{
					echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
					exit;
				}
			}
			$query = $this->Categories->getHomeTabRows(array("tab_id"=>$args));
          	if(!empty($data['tab_image'])){
	            if(!empty($query['tab_image']) && $query['tab_image'] != "default.png"){
	              $dirpath = './././assets/img/category/'.$query['tab_image'];
	              unlink($dirpath);
	            }
	        }			
			$check = $this->Categories->updateHomeTab($data, array("tab_id"=>$this->input->post("editid")));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Category updated successfully.</label>"));
			}
			else{
				echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Category not updated, Please try again.</label>"));
			}
		}
  	}

  	public function generateRandomString($length = 12) {
        // $length = 12;
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
    }


    public function import_csv()
	{
		$data = array();
		$uploaded_file = "";
		$csvs = array();
		$result= "";
		$cat_count = 0;

		if($this->input->post('ImportSubmit'))
		{
		  $config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
		  $config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
		  $config['max_size']    = '20480'; //The max size of the image in kb's
		  $config['overwrite'] = TRUE; 

		  $this->load->library('upload', $config); //Load the upload CI library
		
	
		  	if (!$this->upload->do_upload('userfile'))
			{
				$error = $this->upload->display_errors();
				echo $error;
			}
			else
			{
				$file_info = $this->upload->data();
				$file_name = $file_info['file_name']; 
			}
					
			// echo base_url()."assets/content_csv/". $file_name;
				$result =  $this->uploadCSVData(base_url()."assets/content_csv/". $file_name);
				
				$data["success"] = $result["success"];
				//print_r($result);
				 //die("here");   
				}else
		            {
		            $data["error"]="File not found";
		            }

		if($result == 1)
		{
		  $data['import_done'] = "Categories are uploaded";
		}
		else
		{
		  $data['import_done'] = "Categories are NOT uploaded";
		}

		$con = null;
		$con = array();
		$con['returnType'] = 'count';
		$cat_count  = $this->Categories->getCatRows($con);

		if($cat_count > 0){
		$con = null;
		$con = array();
		$data["cat_count"] = $cat_count;
		$con['returnType'] = '';
		$data['catlist'] = $this->Categories->getCatRows($con);
		}
		else
		{
		 $data['catlist'] = array();
		}


		$config['base_url'] = base_url()."Category/import_csv/";
		$config['total_rows'] = $cat_count;
		$config['per_page'] = 50;
		$config['uri_segment'] = 3;
		//$config['num_links'] = $cat_count / 50;
		$config['num_links'] = 2;
		$config['cur_tag_open'] = '&nbsp;<a class="current">';
		$config['cur_tag_close'] = '</a>';
		$config['next_link'] = '<b> Next </b>';
		$config['prev_link'] = '<b> Previous </b>';
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = round($choice);
		$this->pagination->initialize($config);

		$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

		$con['limit'] = $config['per_page'];
		$start =($page-1) * $config['per_page'];

		if($start == 0)
		{
		  $start = 0;
		}
		else
		{
		  $start = ($start/$config['per_page'] )+1;
		}

		$con['start'] = $start;
		 if($cat_count > 0){
		      $data['catlist'] = $this->Categories->getCatRows($con);
		      $data["record_found"] = $cat_count; 
		    }
		    else
		    {
		      $data["record_found"] = 0; 
		    }

		$data["links"] = $this->pagination->create_links();
		$data['page'] = $page;


		        $data['import_type']="Categories"; 
		        header("Access-Control-Allow-Origin: *");
		        $this->template->set('title', 'Import CSV File'); 
		        $this->template->load('admin_layout', 'contents' , 'import_csv', $data);
		 
	}

	function uploadCSVData($fp)
    {
        $count=0;
        $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");
        while($csv_line = fgetcsv($fp,1024))
        {
			// echo "<pre>";
			// print_r($csv_line);
			// echo "</pre>";
            $count++;
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
            for($i = 0, $j = count($csv_line); $i < $j; $i++)
            {

				// echo "<br>".$csv_line[0]. "<br>";
                $insert_csv = array();
             
				$insert_csv['cat_name'] = $csv_line[0];
                $insert_csv['cat_description'] = $csv_line[0];
				$insert_csv['cat_parent'] = 0;
				$insert_csv['cat_img_path'] = 'default.png';
				$insert_csv['cat_homedisplay'] = 0;
				$insert_csv['cat_status'] = 1;
				$insert_csv['cat_by_user'] = 0;
				$insert_csv['type'] = 1;
				$insert_csv['cat_hometabdisplay'] = 0;
				$insert_csv['cat_hometab_id'] = 0;
            }
			$i++;


            $data = array(
                'cat_name' => $insert_csv['cat_name'],
				'cat_description' => $insert_csv['cat_description'],
				'cat_parent' => $insert_csv['cat_parent'],
				'cat_img_path' => $insert_csv['cat_img_path'],
				'cat_homedisplay' => $insert_csv['cat_homedisplay'],
				'cat_status' => $insert_csv['cat_status'],
				'cat_by_user' => $insert_csv['cat_by_user'],
				'type' => $insert_csv['type'],
				'cat_hometabdisplay' => $insert_csv['cat_hometabdisplay'],
				'cat_hometab_id' => $insert_csv['cat_hometab_id']
			
			);

			$x = $this->check_cat($insert_csv['cat_name']);

			if($x >= 1)
			{

			}
			else {
				$data['inserted_csv']=$this->db->insert('otd_business_category', $data);
			}
		
			
		}
	
        fclose($fp) or die("can't close file");
        $data['success']="success";
        return $data;
    }


	function check_cat($cat_name)
	{
		$check = $this->Categories->getRows(array("conditions"=>array("cat_name"=>$cat_name), "returnType"=>"count"));
		return $check;
	}

	function uploadBusinessCSVData($fp)
    {
        $count=0;
      
        $handle = fopen($fp,'r') or die("can't open file");        
        $usertest = array();
        while (($csv_line = fgetcsv($handle, 0, ",")) !== FALSE) {
  
			// echo "<pre>";
			// print_r($csv_line);
			// echo "</pre>";
			// echo "<br>".$csv_line[0]. " - ".$csv_line[1]. " - ".$csv_line[2]. " - ".$csv_line[3]. " - ".$csv_line[4]. " - ".$csv_line[5]. " - ".$csv_line[6]. " - ".$csv_line[7];			
            $count++;


            // continue;            
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
           // if(empty($csv_line[1]) && empty($csv_line[2])){
            	// 	continue;
            	// }

            
             
			  	$insert_csv = array();             
				$insert_csv['businessID'] = $csv_line[0];
				$insert_csv['Company_Name'] = $csv_line[1];
				$insert_csv['Company_Number'] = $csv_line[2];
				$insert_csv['Address'] = $csv_line[3];
				$insert_csv['Category'] = $csv_line[4];
				$insert_csv['Phone_Number'] = $csv_line[5];
				$insert_csv['status'] = $csv_line[6];
				$insert_csv['lat'] = $csv_line[7];
				$insert_csv['long'] = $csv_line[8];
          
  
            $data = array(
				'user_firstname' => '',
				'user_lastname' => '',
				'user_email' => '',
				'user_phone' => '',
				'user_address' =>'',
				'user_password' => '',
				'user_gender' => '',
				'user_type' => 'Professional',
				'auth_check' => '',
				'sms_check' => '',
				'user_language' => '',  
                'businessID' => $insert_csv['businessID'],
				'user_company_name' => $insert_csv['Company_Name'],
				'user_company_number' => $insert_csv['Company_Number'],
				'user_company_address' => $insert_csv['Address'],
				'status' => $insert_csv['status'],
				'user_lat' => $insert_csv['lat'],
				'user_long' => $insert_csv['long'],
			);
			  
 
			//'Phone_Number' => $insert_csv['Phone_Number'],
			//'Category' => $insert_csv['Category'],

			$business_phone = explode("::", $insert_csv['Phone_Number']);
			$business_Category = explode("::", $insert_csv['Category']);			
			$insert = $this->Categories->insert($data);
		
		 	$dataprofile = array(
				'bs_user' => $insert,
				'bs_name' => $insert_csv['Company_Name'],
				'bs_comp_number' => $insert_csv['Company_Number'],
				'bs_email' => '',
				'bs_phone' => '',
				'bs_twitter' => '',
				'bs_website' => '',
				'bs_address' => $insert_csv['Address'],
				'bs_desc' => ''			
			);
			
			if($insert > 0){
				// if(!empty(strip_tags($this->input->post('user_categories'))))
				//     $business_category = explode(",", strip_tags($this->input->post('user_categories')));
				// if(count($business_category) > 0){
				//     $this->Categories->insertUserCategories(1, $business_category, $insert);
				// }
			
				$this->Categories->insert_profile($dataprofile);
				if(!empty($business_Category)){
					$this->insertCategories($insert, $business_Category);
				} 
				if(!empty($business_phone)){
					$this->insertPhonenumber($insert, $business_phone);
				}
				$data1['success']="success";
			}else{
				$data1['error_msg'] = 'Some problems occured, please try again.';			   
			}  			
          unset($data);
		}	
		 	
        fclose($handle) or die("can't close file");
        return $data1;
    }

    function CheckBusinessCSVBeforeUpload(){
    	$result = 0;
    	if(!empty($_FILES) && !empty($_FILES['userfile']['name'])){    		
		  	$config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
		  	$config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
		  	$config['max_size']    = '20480'; //The max size of the image in kb's
		  	$config['overwrite'] = TRUE; 
		  	$new_name = time().$_FILES["userfile"]['name'];
			$config['file_name'] = $new_name;
		  	$this->load->library('upload', $config); //Load the upload CI library
		  	if (!$this->upload->do_upload('userfile'))
			{							
				$error = $this->upload->display_errors();
				echo $error;	
			}else{
				$file_info = $this->upload->data();
				$file_name = $file_info['file_name']; 
			}			
			$result =  $this->uploadBusinessCSVData("./././assets/content_csv/$new_name");			
			$data["success"] = $result["success"];
			if(file_exists("./././assets/content_csv/$new_name")){
                unlink("./././assets/content_csv/$new_name");
            }            
    	}else{
            
        }
        redirect(base_url("Category/import_businesscsv"));
    }

    public function import_businesscsv(){
    	$data = array();
    	header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Import CSV File'); 
        $this->template->load('admin_layout', 'contents' , 'import_businesscsv', $data);
    }

	// public function import_businesscsv()
	// {
	// 	$data = array();
	// 	$uploaded_file = "";
	// 	$csvs = array();
	// 	$result= "";
	// 	$cat_count = 0;

	// 	if($this->input->post('ImportSubmit'))
	// 	{
	// 	  	$config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save
	// 	  	$config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted
	// 	  	$config['max_size']    = '20480'; //The max size of the image in kb's
	// 	  	$config['overwrite'] = TRUE; 
	// 	  	$new_name = time().$_FILES["userfile"]['name'];
	// 		$config['file_name'] = $new_name;
	// 	  	$this->load->library('upload', $config); //Load the upload CI library
	// 	  	if (!$this->upload->do_upload('userfile'))
	// 		{							
	// 			$error = $this->upload->display_errors();
	// 			echo $error;	
	// 		}else{
	// 			$file_info = $this->upload->data();
	// 			$file_name = $file_info['file_name']; 
	// 		}           
	// 		$result =  $this->uploadBusinessCSVData("./././assets/content_csv/$new_name");
	// 		$data["success"] = $result["success"];
	// 		if(file_exists("./././assets/content_csv/$new_name")){
 //                unlink("./././assets/content_csv/$new_name");
 //            }            
	// 	}else{
 //            $data["error"]="File not found";
 //        }

	// 	if($result == 1)
	// 	{
	// 	  $data['import_done'] = "Busienss profiles are uploaded successfully";
	// 	}
	// 	else
	// 	{
	// 	  $data['import_done'] = "Busienss profiles are not uploaded, Please try again";
	// 	}
 //        $data['import_type']="Categories"; 
 //        header("Access-Control-Allow-Origin: *");
 //        $this->template->set('title', 'Import CSV File'); 
 //        $this->template->load('admin_layout', 'contents' , 'import_businesscsv', $data);		 
	// }

	function insertPhonenumber($user= NULL, $params = array()){
		if(!empty($params)){

			foreach($params as $pt){
				$business_phone2 = explode("==", $pt);
				$arr = array("phn_user"=>$user,
							 "phn_number"=> $business_phone2[1],
							 "phn_label"=> $business_phone2[0],
			);
			//print_r($arr);
	
				$this->db->insert("otd_user_phone", $arr);
			}
		
		}        
	}

	function insertCategories($user= NULL, $params = array()){
		if(!empty($params)){
			foreach($params as $pt){

				$checkcat = $this->check_cat($pt);

				if($checkcat > 0){
					$catdetails = $this->Categories->getRows(array("conditions"=>array("cat_name"=>$pt), "returnType"=>"single"));
					$catid = $catdetails['cat_id'];
					$usrarr = array("user_id"=>$user,
									"category_name"=> $pt,
									"category_id"=> $catid,
								    "type" =>1,
								);

		  			$this->Categories->insertUsercategories($usrarr);
				}else{
					$data = array(  'cat_name' => $pt,
									'cat_description' => $pt,
									'cat_parent' => 0,
									'cat_img_path' => 'default.png',
									'cat_homedisplay' => 0,
									'cat_status' => 1,
									'cat_by_user' => 0,
									'type' => 1,
									'cat_hometabdisplay' => 0,
									'cat_hometab_id' => 0								
								);

					$catid = $this->Categories->insertTable(1, $data);
					$usrarr = array("user_id"=>$user,
									"category_name"=> $pt,
									"category_id"=> $catid,
								    "type" =>1,
								);
		  			$chk = $this->Categories->insertUsercategories($usrarr);
				}

				// $business_cat = explode(",", $pt);
				// $arr = array("cat_by_user"=>$user,
				// 			 "cat_name"=> $business_cat[0],
				// 			 "cat_description"=> $business_cat[1],
				// 			 "type"=>1
				// 			);
				// if(empty($business_cat[1]))
				// {
				// 	if($business_cat[1] == 'produits')
				// 	{
				// 		$arr['type'] = 1;
				// 	}
				// 	else {
				// 		$arr['type'] = 2;
				// 	}
				// }else
				// {
				// 	$arr['type'] = 3;
				// }

				// $cat_id=$this->db->isCat($business_cat[0], $params);
				// $usrarr = array("user_id"=>$user,
				// 				"cat_name"=> $business_cat[0],
				// 				"category_id"=> $cat_id,
				// 			    "Type" =>$arr['type'],
				// );
			 //  	$this->db->insert("otd_user_business_category", $usrarr);
			}		
		}        
	}
	public function csvpro_registration()
	{
            $data = array();
            $data['error_msg']=""; 
           // $userData = array();
           /* $userData = array(
                    'user_firstname' => '',
                    'user_lastname' => '',
                    'user_email' => '',
                    'user_phone' => '',
                    'user_address' =>'',
                    'user_password' => '',
                    'user_gender' => '',
                    'user_type' => 'Professional',
                    'auth_check' => '',
                    'sms_check' => '',
                    'user_language' => '',                    
                    'user_company_name' => '',
                    'user_company_address' => '',
					'user_company_number' => '',
					'user_lat' => '',
                    'user_long' => '',
                );
  */
             //   $latlong = $this->User->get_lat_long($userData['user_company_address']);
              //  $userData['user_lat'] = $latlong['lat'];
               // $userData['user_long'] = $latlong['lng'];

               
                $insert = $this->Categories->insert($userData);

                if($insert > 0){
                    // if(!empty(strip_tags($this->input->post('user_categories'))))
                    //     $business_category = explode(",", strip_tags($this->input->post('user_categories')));
                    // if(count($business_category) > 0){
                    //     $this->Categories->insertUserCategories(1, $business_category, $insert);
                    // }
                   
                }else{
                    $data['error_msg'] = 'Some problems occured, please try again.';
                   
                }
                        
                   
      
	 }
}

?>