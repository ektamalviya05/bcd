<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>View Listing <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 <?php 
                foreach ($business as $value) {
                  $business_id= $value['business_id'];
                  $sub_cat_id= $value['sub_cat_id'];
                  $cat_id= $value['cat_id'];
                  $business_title= $value['business_title'];
                  $business_desc = $value['business_desc'];
                  $business_price = $value['business_price'];
                  $business_email = $value['business_email'];
                  $business_mobile= $value['business_mobile'];
                  $business_address  = $value['business_address'];
                  $business_country = $value['business_country'];
                  $business_about = $value['business_about'];
                  $business_image1 = $value['business_image1'];
                  $business_image2 = $value['business_image2'];
                  $business_image3 = $value['business_image3'];
                  $business_image4 = $value['business_image4'];
                  $status = $value['status'];
                  $features_ads = $value['features_ads'];
               
                }
                 ?>
                  <div class="x_content">
                    <br />
                    <?php //echo base_url("Listing/add"); ?>
                    <form id="editbusiness" enctype="multipart/form-data" method="post" action='#' data-parsley-validate class="form-horizontal form-label-left">                                            
                    
                    
                  <div class="list_vie_one">
                      <div class="col-md-6"> <div class="form-group">
                       <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Name:</label>
                      </div>
                      
                      <div class="col-md-8 col-sm-8 col-xs-12">
                          <?php echo $business_title;  ?>
                          <input type="hidden" class="form-control" name="business_id" id="business_id" value="<?php echo $business_id;  ?>"/>
                          <?php echo form_error('name'); ?>
                        </div>
                      </div>  </div>
                      
                      <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label >Business Category:</label>
                        </div>
                        <div class="col-lg-8">
                          
                             
                             <?php foreach ($category as $getcategory) {
                              ?>
                             <?php if($getcategory['cat_id']== $cat_id){ echo $getcategory['cat_name']; } ?>
                             <?php
                             } ?>
                          
                         </div>
                      </div>  </div>
                      </div>
                   <?php
                   if($sub_cat_id)
                   {
                   ?>   
                  <div class="list_vie_two">     
                      <div class="col-md-6"> <div class="form-group">
                        <label class="col-lg-4 pull-left">Business Sub Category:</label>
                        <div class="col-lg-8">
                          <?php foreach ($allcategory as $allgetcategory) {
                              ?>
                             <?php if($allgetcategory['cat_id']== $sub_cat_id){ echo $allgetcategory['cat_name']; } ?>
                             <?php
                             } ?>
                         </div>
                      </div>  
                  <?php 
                  }
                  ?>
                  </div>
 	                  <div class="col-md-6 hidden"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>About Business:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-12">
                           <?php echo $business_about; ?>  
          
                        </div>
                      </div>  </div>
                  </div>    
                      
                  <div class="list_vie_one">     
                 
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Short Description:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php echo $business_desc; ?>
                        </div>
                      </div>  </div>
                      
                    <div class="col-md-6 hidden"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Price:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php echo $business_price; ?>
                          
                        </div>
                      </div>  </div>
                 </div>     
                      
                  <div class="list_vie_two">     
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Email:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php echo  $business_email; ?>
                        </div>
                      </div>  </div>
                      
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Mobile:</label>
                        </div>
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <?php echo $business_mobile; ?>
                         
                        </div>
                      </div>  </div>
                  </div>    
                
				  <div class="list_vie_one">                      
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Address:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                         <?php echo  $business_address ; ?>
                        </div>
                      </div>  </div>
                      
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Image One:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php 
                          if($business_image1)
                          {
                          ?>
                          <img src="<?php echo base_url("uploads/business/$business_image1"); ?>" height="50px" width="50px"/>
                          <?php 
                          }
                          else
                          {
                          ?>
                          <img src="<?php echo base_url("uploads/business/previewimage.png"); ?>" height="50px" width="50px"/>
                          <?php 
                          } 
                          ?>
                        </div>
                      </div>  </div>
                      </div>
                      
                  <div class="list_vie_two">
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Image Two:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php 
                          if($business_image2)
                          {
                          ?>
                          <img src="<?php echo base_url("uploads/business/$business_image2"); ?>" height="50px" width="50px"/>
                          <?php 
                          }
                          else
                          {
                          ?>
                          <img src="<?php echo base_url("uploads/business/previewimage.png"); ?>" height="50px" width="50px"/>
                          <?php
                          }
                          ?>
                        </div>
                      </div>  </div>
                      
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Image Three:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php 
                          if($business_image3)
                          {
                          ?>
                           <img src="<?php echo base_url("uploads/business/$business_image3"); ?>" height="50px" width="50px"/>
                          <?php
                          }
                          else
                          {
                          ?>
                          <img src="<?php echo base_url("uploads/business/previewimage.png"); ?>" height="50px" width="50px"/>
                          <?php 
                          }
                          ?>
                        </div>
                      </div>  </div>
                      
                      </div>
                      
                  <div class="list_vie_one">
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Image Four:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php 
                          if($business_image4)
                          {
                          ?>
                           <img src="<?php echo base_url("uploads/business/$business_image4"); ?>" height="50px" width="50px"/>
                          <?php
                          }
                          else
                          {
                          ?>
                          <img src="<?php echo base_url("uploads/business/previewimage.png"); ?>" height="50px" width="50px"/>
                          <?php
                          }
                          ?>
                        </div>
                      </div>  </div>
                      
                    <div class="col-md-6"> <div class="form-group">
                        <div class="col-md-4 col-sm-6 col-xs-12">
                          <label>Business Country:</label>
                        </div>
                        <div class="col-md-8 col-sm-6 col-xs-12">
                          <?php echo $business_country; ?>
                        </div>
                      </div>  </div>
                    
                 </div>
                  
                  </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content