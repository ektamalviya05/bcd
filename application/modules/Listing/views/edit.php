<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Listing <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                 <?php 
                foreach ($business as $value) {
                  $business_id= $value['business_id'];
                  $sub_cat_id= $value['sub_cat_id'];
                  $cat_id= $value['cat_id'];
                  $business_title= $value['business_title'];
                  $business_title_arabic= $value['business_title_arabic'];
                  $business_desc = $value['business_desc'];
                  $business_desc_arabic = $value['business_desc_arabic'];
                  $business_price = $value['business_price'];
                  $business_email = $value['business_email'];
                  $business_mobile= $value['business_mobile'];
                  $business_address  = $value['business_address'];
                  $business_address_arabic  = $value['business_address_arabic'];
                  $business_country = $value['business_country'];
                  $business_about = $value['business_about'];
                  $business_image1 = $value['business_image1'];
                  $business_image2 = $value['business_image2'];
                  $business_image3 = $value['business_image3'];
                  $business_image4 = $value['business_image4'];
                  $status = $value['status'];
                  $features_ads = $value['features_ads'];
                  $business_city = $value['business_city'];
                  $address_type = $value['address_type'];
               
                }
                 ?>
                  <div class="x_content">
                    <br />
                    <?php //echo base_url("Listing/add"); ?>
                    <form id="editbusiness" enctype="multipart/form-data" method="post" action='#' data-parsley-validate class="form-horizontal form-label-left">                                            
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Name<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="name" id="name" value="<?php echo $business_title;  ?>"/>
                          <input type="hidden" class="form-control" name="business_id" id="business_id" value="<?php echo $business_id;  ?>"/>
                          <?php echo form_error('name'); ?>
                        </div>
                      </div>
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Name In Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="namea" id="name" value="<?php echo $business_title_arabic;  ?>"/>
                          <input type="hidden" class="form-control" name="business_id" id="business_id" value="<?php echo $business_id;  ?>"/>
                          <?php echo form_error('name'); ?>
                        </div>
                      </div> -->
                      <div class="row">

                      <div class="form-group col-lg-6 col-sm-6">
                        <label class="col-lg-12 pull-left">Business Category<span style="color: #be2d63">*</span>:</label>
                        <div class="col-lg-8">
                          <select class="form-control" id="cat_id" name="cat_id">
                             <option value="">Select Business Category First</option>
                             <?php foreach ($category as $getcategory) {
                              ?>
                             <option value="<?php echo $getcategory['cat_id']; ?>" <?php if($getcategory['cat_id']== $cat_id){ echo "selected";} ?>><?php echo $getcategory['cat_name']; ?></option>
                             <?php
                             } ?>
                          </select>
                         </div>
                      </div>


                      <div class="form-group col-lg-6 col-sm-6">
                        <label class="col-lg-12 pull-left">Business Sub Category:</label>
                        <div class="col-lg-8">
                          <select class="form-control" id="sub_cat_id" name="sub_cat_id">
                             <option value=''>Select Business Sub Category </option>
                              <?php foreach ($subcategory as $getcategory1) {
                                if($getcategory1['cat_parent']==$cat_id)
                                {
                              ?>
                             <option value="<?php echo $getcategory1['cat_id']; ?>" <?php if($getcategory1['cat_id']== $sub_cat_id){ echo "selected";} ?>><?php echo $getcategory1['cat_name']; ?></option>
                             <?php
                             }
                             } ?>
                          </select>
                         </div>
                      </div>
                    </div>


                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>About Business:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="business_about" class="form-control" placeholder="Write About Business"><?php echo $business_about; ?></textarea>
                          <?php echo form_error("business_about"); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Short Description<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="description" class="form-control" placeholder="Description"><?php echo $business_desc; ?></textarea>
                          <?php echo form_error("description"); ?>
                        </div>
                      </div>
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Short Description In Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="descriptiona" class="form-control" placeholder="Description"><?php echo $business_desc_arabic; ?></textarea>
                          <?php echo form_error("descriptiona"); ?>
                        </div>
                      </div> -->

                       <div class="form-group hidden">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Price:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input name="business_price" class="form-control" placeholder="Business Price" value="<?php echo $business_price; ?>">
                          <?php echo form_error("business_price"); ?>
                        </div>
                      </div>

                       <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Email<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="email" name="business_email" class="form-control" placeholder="Business Email" value="<?php echo  $business_email; ?>">
                          <?php echo form_error("business_email"); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Mobile<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input name="business_mobile" class="form-control" placeholder="Business Mobile" value="<?php echo 
                          $business_mobile; ?>">
                          <?php echo form_error("business_mobile"); ?>
                        </div>
                      </div>

                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Address</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="business_address" class="form-control" placeholder="Business Address"><?php echo  
                          $business_address ; ?></textarea>
                          <?php //echo form_error("business_address"); ?>
                        </div>
                      </div> -->
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Address In Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="business_addressa" class="form-control" placeholder="Business Address"><?php echo  
                          $business_address_arabic; ?></textarea>
                          <?php echo form_error("business_address"); ?>
                        </div>
                      </div> -->


                      <div class="add-mor-sub">
                        <h2>Main Images</h2>                        
                      </div>
                      <div class="imgp-prwv-admin">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image One<span style="color: #be2d63">*</span>:</label>
                        </div>
                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php 
                            if($business_image1)
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output" src='<?php echo base_url(); ?>uploads/business/<?php echo $business_image1; ?>' >
                          </div>
                            <?php 
                            }
                            else
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                          </div>
                            <?php
                            }
                            ?>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="business_image1" class="form-control" onchange="loadFile(event)"/>
                          <div id="flupmsgone"></div>
                        </div>
                      </div>
                    </div>

                    <div class="add-mor-sub">
                        <h2>Optional Images</h2>                        
                      </div>
                      <div class="imgp-prwv-box">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image Two:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php 
                            if($business_image2)
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output1" src='<?php echo base_url(); ?>uploads/business/<?php echo $business_image2; ?>' >
                          </div>
                            <?php 
                            }
                            else
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output1" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                          </div>

                            <?php
                            }
                            ?>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="business_image2" class="form-control" onchange="loadFile1(event)"/>
                          <div id="flupmsgtwo"></div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image Three:</label>
                        </div>
                         <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php 
                            if($business_image3)
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output2" src='<?php echo base_url(); ?>uploads/business/<?php echo $business_image3; ?>' >
                          </div>
                            <?php 
                            }
                            else
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output2" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                          </div>
                            <?php
                            }
                            ?>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="business_image3" class="form-control" onchange="loadFile2(event)"/>
                          <div id="flupmsgthree"></div>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image Four:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php 
                            if($business_image4)
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output3" src='<?php echo base_url(); ?>uploads/business/<?php echo $business_image4; ?>' >
                          </div>
                            <?php 
                            }
                            else
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output3" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                          </div> 
                            <?php
                            }
                            ?>

                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="business_image4" class="form-control" onchange="loadFile3(event)"/>
                          <div id="flupmsgfouro"></div>
                        </div>
                      </div>
                    </div>

                       <!-- <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Country:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="business_country" class="form-control">
                            <option value="">Select Country</option>
                            <option value="Country1" <?php //if($business_country == 'Country1' ){ echo "selected" ;} ?> >Country1</option>
                            <option value="Country2" <?php //if($business_country == 'Country2' ){ echo "selected" ;} ?> >Country2</option>
                          </select>
                          <?php //echo form_error('business_country'); ?>
                        </div>
                      </div> -->
                      <div class="add-mor-sub">
                        <h2>Business offer Image:(Note : Offer Image size should be 500kb)</h2>    
                                          
                      </div>
                      <div class="imgp-prwv-box">
                      <div class="form-group">
                       
                         <?php
                            foreach ($offerImg as $valueImg) {
                               $offer =  $valueImg['offer_image'];
                             } 
                          ?>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php 
                            if($offer)
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output4" src='<?php echo base_url(); ?>uploads/business/<?php echo $offer; ?>' >
                          </div>
                            <?php 
                            }
                            else
                            {
                            ?>
                            <div class="img-prw-box">
                            <img id="output4" src='<?php echo base_url(); ?>uploads/business/previewimage.png' >
                          </div>

                            <?php
                            }
                            ?>
                          </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="file" name="offer" class="form-control" onchange="loadFile4(event)"/>
                          <div id="flupmsgfour"></div>
                        </div>
                      </div>
                    </div>
                      

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Status<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="status" class="form-control">
                            <option value="" >Select Status</option>
                            <option value="1" <?php if($status == 1){echo "selected";} ?> >Publish</option>
                            <option value="0" <?php if($status == 0){echo "selected";} ?> >Unpublish</option>
                          </select>
                          <?php echo form_error('status'); ?>
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Add as Feature Ads<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="features_ads" class="form-control">
                            <option value="">Select Add Feature Ads</option>
                            <option value="1" <?php if($features_ads == "1"){echo "selected";} ?> >Yes</option>
                            <option value="0" <?php if($features_ads == "0"){echo "selected";} ?>>No</option>
                           
                          </select>
                          <?php echo form_error('features_ads'); ?>
                        </div>
                      </div>


                     <div class="f-login">
                    <label>Location</label>
                    <div id="myRadioGroup" class="text-left">

                     <span>
                        <input type="radio" name="cars" checked="checked" value="2" <?php if($address_type == 0){echo "checked"; } ?> />
                        <label for="yes">By Manual</label>
                      </span> 
                      <span>
                        <input type="radio" name="cars" value="3" <?php if($address_type == 1){echo "checked"; } ?>  />
                        <label for="yes">By Google</label>
                      </span>

                      </div>
                    </div>

                       <div id="Cars2" class="desc" <?php if($address_type == 0){echo 'style="display: block;"'; } else{echo 'style="display:none;"'; } ?>>
                          <div class="row">
                            <div class="col-sm-12">
                              <label class="adrs-label">Business Address<span style="color: #be2d63">*</span>:</label>
                              <input class="col-sm-4" name="business_address" id="business_address" type="text"  value="<?php echo  $business_address ; ?>" />
                            </div>
                          </div>
                          <div class="row">
                            <div class="f-login col-sm-6">
                              <label class="adrs-label">Country<span style="color: #be2d63">*</span>:</label>
                              <div class="cat-sel " >
                              <select name="business_country" id="business_country">
                              <option value="">Select Country</option>
                            
                               <?php 
                              //print_r($country);
                               foreach($country as $value1) {
                              ?>
                              <option value="<?php echo $value1['name']; ?>" <?php if($value1['name']== $business_country){ echo "selected" ;} ?> ><?php  echo $value1['name']; ?></option>
                              <?php
                              }
                              ?> 
                                </select>
                            </div>
                            </div>

                            
                          </div>  
                          <div class="row">
                            <div class="f-login col-sm-6">
                              <label class="adrs-label">City<span style="color: #be2d63">*</span>:</label>
                          <div class="cat-sel ">
                           <!--  <select name="business_city" id="business_city">
                              <?php 
                              
                              foreach($city as $value2) {
                               if($value2['country_id']== $business_country)
                               {
                              ?>
                              <option value="<?php echo $value2['id']; ?>" <?php if($value2['id']== $business_city){ echo "selected" ;} ?>><?php  echo $value2['name']; ?></option>
                              <?php
                              }
                              }
                              ?> 
                            
                            </select>  -->

                            <input type="text" name="business_city" id="business_city" value="<?php echo $business_city;?>">
                            </div>
                            </div>
                          </div>                    
                       </div>



                        <div id="Cars3" class="desc" <?php if($address_type == 1){echo 'style="display: block;"'; } else{echo 'style="display:none;"'; } ?>>
                           <label class="adrs-label">Location<span style="color: #be2d63">*</span>:</label>
                         <input name="business_address1" id="autocomplete" type="text" value="<?php echo  
                          $business_address ; ?>" /> 
                          
                      </div> 
                      <div id="flupmsgfive"></div> 

                     
                    
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <input type="hidden" value="1" name="type">
                          <button type="submit" id="businessadd" name="btnSave" class="btn btn-success">Update</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>                          
                          <div id="catupmsg" style="font-weight: 600;color: #478c2a;">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
         <!-- /page content -->

          <script>
           function loadFile (event) {
          var output = document.getElementById('output');
          output.src = URL.createObjectURL(event.target.files[0]);
        };

        function loadFile1 (event) {
          var output1 = document.getElementById('output1');
          output1.src = URL.createObjectURL(event.target.files[0]);
        };

        function loadFile2 (event) {
          var output2 = document.getElementById('output2');
          output2.src = URL.createObjectURL(event.target.files[0]);
        };

        function loadFile3 (event) {
          var output3 = document.getElementById('output3');
          output3.src = URL.createObjectURL(event.target.files[0]);
        };

        function loadFile4 (event) {
          var output4 = document.getElementById('output4');
          output4.src = URL.createObjectURL(event.target.files[0]);
        };
        </script>