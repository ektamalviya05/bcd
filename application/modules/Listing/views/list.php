<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 

<?php
if(!empty($success) && $success == "success")
{

    ?>
    <script type="text/javascript">
    $(document).ready(function() {
    openModal();
    });
    function openModal(){
     // $('#myModaCat').modal();
     alert("Category uploaded successfully");
    }  
    </script>
      <?php


}
 $segment  = $this->uri->segment(3);

      
?>
    <!--  Second Top Row   -->
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Listing/add"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Add</h3>
                            <p>New Listing</p>
                        </div>
                    </a>
                </div>
            </div>

<!--               <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category/import_csv"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-download" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Import</h3>
                            <p>Category from CSV</p>
                        </div>
                    </a>
                </div>
            </div>-->

            <!-- <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php //echo base_url("Listing/trashListing"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Trash Listings</p>
                        </div>
                    </a>
                </div>
            </div> -->
           <!--  <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category/homeTab"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-folder-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Category Tab</p>
                        </div>
                    </a>
                </div>
            </div> -->
          </div>
      </div>
      <div class="import-create dash-counter">        
        <form action="<?php echo base_url("Listing/setFilterDelimiter"); ?>" method="post" id="filterForm">
          <div class="row">
            <div class="form-group col-md-4" >
              <input type="text" autocomplete="off" class="form-control" name="category" id="category"  placeholder="Category Name" value="<?php echo $this->session->userdata('filtercategory1'); ?>">
            </div>
            <div class="form-group col-md-5">
              <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn btn-primary th-primary" value="Filter"/>               
                       
              <a href="<?php echo base_url("Listing/clearFilterDelimiter"); ?>"  class="btn btn-danger th-danger">Clear Filter</a>
            </div>
          </div>
        </form>        
      </div>
    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Listings</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th >No.</th>
                  <th >Name</th>
                  <th>Short Description</th>
                  <th align="center">Status</th>                                
                  <th >Date Created</th>
                  <th >Image</th>
                  <th >Action</th>
                </tr>
                <?php
                $count = 1;
                if($segment){
                  $count = $segment+1;
                }
                if(!empty($details)){
                  foreach($details as $testi){
                    $imgpath = $testi['business_image1'];
                  ?>
                  <tr>
                    <td ><?php echo $count; ?></td>
                    <td ><?php 
                    $keyword = $this->session->userdata("filtercategory");
                    // $t = preg_replace("/\p{L}*?".preg_quote($keyword)."\p{L}*/ui", "<b>$0</b>", $testi['cat_name']);
                    // echo $t."<br>";
                    if(!empty($keyword)){
                      echo $this->Categories->highlightkeyword($testi['business_title'], $keyword);
                    }else{
                      echo $testi['business_title'];
                    }
                    ?></td>
                    <td><?php echo substr($testi['business_desc'], 0, 100); ?></td>
                    <td align="center">
                      <input type="checkbox" class="business_status" modal-aria="<?php echo $testi['business_id']; ?>" <?php if($testi['status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">                                                          
                    </td>                               
                    <td><?php echo date("d F, Y H:i", strtotime($testi['created'])); ?></td>
                    <td><?php if(!empty($imgpath)) { ?><img src="<?php echo base_url("uploads/business/$imgpath"); ?>" height="50px" width="50px"/><?php } ?></td> 
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['business_id']; ?>
                          <div class="tooltip-2"><a href="<?php echo base_url("Listing/edit/$tstid"); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Edit</span>
                          </div>
                          <div class="tooltip-2"><a href="<?php echo base_url("Listing/view/$tstid"); ?>"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <span class="tooltiptext">View</span>
                          </div>
                          <div class="tooltip-2"><a href="javascript:;" data-toggle="" data-singleton="" data-popout="true" data-placement="top" class="trash_business" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Move to Trash</span>
                          </div>
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                else
                {
                  echo "<span class='nolist'>Sorry !! No listing found </span>  ";
                }
                ?>

              </tbody>
            </table>
             <div class="read-more">
                       <?php echo $links ;  ?>
                        </div>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>