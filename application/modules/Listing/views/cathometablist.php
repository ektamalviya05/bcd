    <!--  Second Top Row   -->
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Category List</p>
                        </div>
                    </a>
                </div>
            </div>                                      
          </div>
      </div>


    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Category Tab List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th>No.</th>
                  <th>Name</th>
                  <th>Image</th>                  
                  <th>Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($details)){
                  foreach($details as $testi){
                    $imgpath = $testi['tab_image'];
                  ?>
                  <tr>
                    <td ><?php echo $count; ?></td>
                    <td ><?php echo $testi['tab_name'] ?></td>
                    <td><?php if(!empty($imgpath)) { ?><img src="<?php echo base_url("assets/img/category/$imgpath"); ?>" height="50px" width="50px"/><?php } ?></td>
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['tab_id']; ?>
                          <div class="tooltip-2"><a href="<?php echo base_url("Category/editCatTab/$tstid"); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Edit</span>
                          </div>
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>