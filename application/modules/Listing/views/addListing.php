<!--page content -->
        <style type="text/css">
          .cat-sel input {
    padding: 7px 15px;
    height: 35px;
    border-radius: 0;
    outline: none;
    box-shadow: none;
    outline: none !important;
    box-shadow: none !important;
    border-radius: 30px;
    border: 1px solid #ccc;
    margin-bottom: 15px;
}
div#Cars2 label.error {
    position: absolute;
    left: 169px;
    bottom: -5px;
}
</style>
        <div class="right_col rig_new_cool" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Add Listing <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php //echo base_url("Listing/add"); ?>
                    <form id="addbusiness" enctype="multipart/form-data" method="post" action='#' data-parsley-validate class="form-horizontal form-label-left">                                            
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Title<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="name" id="name" value="<?php echo set_value("name"); ?>"/>
                          <?php echo form_error('name'); ?>
                        </div>
                      </div>
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Name in Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="namea" id="name" value="<?php echo set_value("namea"); ?>"/>
                          <?php echo form_error('namea'); ?>
                        </div>
                      </div> -->
                      <div class="row">  
                      <div class="form-group col-lg-6 col-sm-6">
                        <label class="col-lg-12 pull-left">Business Category<span style="color: #be2d63">*</span>:</label>
                        <div class="col-lg-8">
                          <select class="form-control" id="cat_id" name="cat_id">
                             <option value=''>Select Business Category First</option>
                             <?php foreach ($category as $getcategory) {
                              ?>
                             <option value="<?php echo $getcategory['cat_id']; ?>"><?php echo $getcategory['cat_name']; ?></option>
                             <?php
                             } ?>
                          </select>
                         </div>
                      </div>

                  <div class="form-group col-lg-6 col-sm-6">
                        <label class="col-lg-12 pull-left">Business Sub Category:</label>
                        <div class="col-lg-8">
                          <select class="form-control" id="sub_cat_id" name="sub_cat_id">
                             <option value=''>Select Business Sub  Category </option>
                          </select>
                         </div>
                      </div>
                    </div>


                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>About Business:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="business_about" class="form-control" placeholder="Write About Business"><?php echo set_value("business_about") ?></textarea>
                          <?php echo form_error("business_about"); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Short Description<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="description" class="form-control" placeholder="Description"><?php echo set_value("description") ?></textarea>
                          <?php echo form_error("description"); ?>
                        </div>
                      </div>
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Short Description In Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="descriptiona" class="form-control" placeholder="Description"><?php echo set_value("descriptiona") ?></textarea>
                          <?php echo form_error("descriptiona"); ?>
                        </div>
                      </div> -->

                       <div class="form-group hidden">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Price:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input name="business_price" class="form-control" placeholder="Business Price"><?php echo set_value("business_price") ?>
                          <?php echo form_error("business_price"); ?>
                        </div>
                      </div>

                       <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Email<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="email" name="business_email" class="form-control" placeholder="Business Email"><?php echo set_value(" business_email") ?>
                          <?php echo form_error("business_email"); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Mobile<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input name="business_mobile" class="form-control" placeholder="Business Mobile"><?php echo set_value(" business_mobile") ?>
                          <?php echo form_error("business_mobile"); ?>
                        </div>
                      </div>

                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Address:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="business_address" class="form-control" placeholder="Business Address"><?php echo set_value("business_address") ?></textarea>
                          <?php echo form_error("business_address"); ?>
                        </div>
                      </div> -->
                     <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Address In Arabic:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea name="business_addressa" class="form-control" placeholder="Business Address"><?php echo set_value("business_address") ?></textarea>
                          <?php echo form_error("business_address"); ?>
                        </div>
                      </div> -->

                      <div class="add-mor-sub">
                        <h2>Main Images</h2>                        
                      </div>
                      <div class="imgp-prwv-admin">
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Business Image One<span style="color: #be2d63">*</span>:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="img-prw-box">
                              <img id="output" class="imgclas" >
                            </div>
                            <input type="file" name="business_image1" class="form-control" onchange="loadFile(event)" />
                            <div id="flupmsgone"></div>
                          </div>
                        </div>
                      </div>
                      <div class="add-mor-sub">
                        <h2>Optional Images</h2>                        
                      </div>
                      <div class="imgp-prwv-box">
                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image Two:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="img-prw-box">
                          <img id="output1" class="imgclas" >
                        </div>
                          <input type="file" name="business_image2" class="form-control" onchange="loadFile1(event)" />
                          <div id="flupmsgtwo"></div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image Three:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="img-prw-box">
                          <img id="output2" class="imgclas" >
                        </div>
                          <input type="file" name="business_image3" class="form-control" onchange="loadFile2(event)" />
                          <div id="flupmsgthree"></div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Image Four:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="img-prw-box">
                          <img id="output3" class="imgclas" >
                        </div>
                          <input type="file" name="business_image4" class="form-control" onchange="loadFile3(event)"/>
                          <div id="flupmsgfouro"></div>
                        </div>
                      </div>
                    </div>
                      <!--  <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business Country:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="business_country" class="form-control">
                            <option value="">Select Country</option>
                            <option value="Country1" >Country1</option>
                            <option value="Country2" >Country2</option>
                          </select>
                          <?php echo form_error('business_country'); ?>
                        </div>
                      </div> -->
                      

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Status<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="status" class="form-control">
                            <option value="">Select Status</option>
                            <option value="1" >Publish</option>
                            <option value="0" >Unpublish</option>
                          </select>
                          <?php echo form_error("status"); ?>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Business offer:(Note : Offer Image size should be 500kb)</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <div class="img-prw-boxtwo">
                          <img id="output4" class="imgclas" >
                          </div>
                          <input type="file" name="offer" class="form-control" onchange="loadFile4(event)"/>
                          <div id="flupmsgfour"></div>
                        </div>
                      </div>
                     



                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Add as Feature Ads<span style="color: #be2d63">*</span>:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="features_ads" class="form-control">
                            <option value="">Select Add Feature Ads</option>
                            <option value="1" >Yes</option>
                            <option value="0" >No</option>
                            
                          </select>
                          <?php echo form_error('features_ads'); ?>
                        </div>
                      </div>

                    


                     <div class="f-login">
                    <label>Location</label>
                    <div id="myRadioGroup" class="text-left">

                      <span>
                        <input type="radio" name="cars" checked="checked" value="2"  />
                        <label for="yes">By Manual</label>
                      </span>
                      <span>
                        <input type="radio" name="cars" value="3" />
                        <label for="yes">By Google</label>
                      </span>

                      </div>
                    </div>

                <div id="Cars2" class="desc">
                    <div class="row">
                      <div class="col-sm-12 buss_add_sec">
                        <label class="adrs-label">Business Address<span style="color: #be2d63">*</span>: </label>
                        <input class="col-sm-4" name="business_address" id="business_address" type="text" />
                      </div>
                    </div>
                    <div class="row">
                      <div class="f-login col-sm-6">
                        <label class="adrs-label">Country<span style="color: #be2d63">*</span>:</label>
                        <div class="cat-sel " >
                        <select name="business_country" id="business_country">
                        <option value="">Select Country</option>
                      
                         <?php 
                        //print_r($country);
                         foreach($country as $value1) {
                        ?>
                        <option value="<?php echo $value1['name']; ?>"><?php  echo $value1['name']; ?></option>
                        <?php
                        }
                        ?> 
                          </select>
                      </div>
                      </div>

                      
                    </div>  
                    <div class="row">
                      <div class="f-login col-sm-6">
                        <label class="adrs-label">City<span style="color: #be2d63">*</span>:</label>
                    <div class="cat-sel ">
                   <!--    <select name="business_city" id="business_city">
                      
                      </select>  -->

                      <input type="text" name="business_city" id="business_city">
                      </div>
                      </div>
                    </div>                    
                    </div>


                    <div id="Cars3" class="desc add_loc_se" style="display: none;">
                       <label class="adrs-label">Location<span style="color: #be2d63">*</span>:</label>
                       <input name="business_address1" id="autocomplete" type="text"/> 
                  </div> 
                  <div id="flupmsgfive" class="add_con_se"></div>

                     
                    
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <input type="hidden" value="1" name="type">
                          <button type="submit" id="businessadd" name="btnSave" class="btn btn-success">Save</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>                          
                          <div id="catupmsg">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>

        <script type="text/javascript">
          $('#myRadioGroup input:checkbox').change(function(){
              if($(this).is(":checked")) {
                  $(this).closest("span").addClass("menuitemshow");
              } else {
                  $(this).closest("span").removeClass("menuitemshow");
              }
          });


                   function loadFile (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
          };
          function loadFile1 (event) {
            var output1 = document.getElementById('output1');
            output1.src = URL.createObjectURL(event.target.files[0]);
          };

          function loadFile2 (event) {
            var output2 = document.getElementById('output2');
            output2.src = URL.createObjectURL(event.target.files[0]);
          };

          function loadFile3 (event) {
            var output3 = document.getElementById('output3');
            output3.src = URL.createObjectURL(event.target.files[0]);
          };

          function loadFile4 (event) {
            var output4 = document.getElementById('output4');
            output4.src = URL.createObjectURL(event.target.files[0]);
          };

          function loadFile5 (event) {
            var output5 = document.getElementById('output5');
            output5.src = URL.createObjectURL(event.target.files[0]);
          };
        </script>

        <!-- /page content

