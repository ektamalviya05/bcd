<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<?php
if(!empty($success) && $success == "success")
{

    ?>
    <script type="text/javascript">
    $(document).ready(function() {
    openModal();
    });
    function openModal(){
     // $('#myModaCat').modal();
     alert("Category uploaded successfully");
    }  
    </script>
      <?php
}
?>
      
      <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category/add"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Add</h3>
                            <p>New Category</p>
                        </div>
                    </a>
                </div>
            </div>

               <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category/import_csv"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-download" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Import</h3>
                            <p>Category from CSV</p>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("Category/trashListing"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>View</h3>
                            <p>Trash Categories</p>
                        </div>
                    </a>
                </div>
            </div>                            
          </div>
      </div>

<div class="import-create dash-counter">
 <h2>Import <?php echo $import_type; ?></h2>  
 <form class="form-horizontal" action="" method="post" name="upload_csv" enctype="multipart/form-data">
<fieldset>
                 
        <div class="form-group">
            <label class="col-md-4 control-label" for="filebutton">Select File</label>
                <div class="col-md-3">
                    <input type="file" name="userfile" required="" id="userfile" class="input-large">
                </div>
                <div class="col-md-4">
                    <input type="submit" id="submit" name="ImportSubmit" id="ImportSubmit" class="btn btn-primary button-loading" data-loading-text="Loading..." value="Import">
                </div>
        </div>


</fieldset>
</form>
</div>

      <div class="dash-counter users-main">
          <h2>Category List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th >No.</th>
                  <th >Category</th>
                  <th>Parent Category</th>
                  <th align="center">Status</th>                                
                  <th >Date Created</th>
                  <th >Action</th>
                </tr>
                <?php
                $count = 1;
                if(!empty($catlist)){
                  foreach($catlist as $testi){
                  ?>
                  <tr>
                    <td ><?php echo $count; ?></td>
                    <td ><?php echo $testi['cat_name'] ?></td>
                    <td ><?php if(!empty($testi['parentName']))echo $testi['parentName']; else echo "Main Category"; ?></td>
                    <td align="center">
                      <input type="checkbox" class="cat_status" modal-aria="<?php echo $testi['cat_id']; ?>" <?php if($testi['cat_status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">                                                          
                    </td>                                
                    <td><?php echo date("d F, Y H:i", strtotime($testi['cat_date_created'])); ?></td>
                    <td>
                      <div class="link-del-view">
                          <?php $tstid = $testi['cat_id']; ?>
                          <div class="tooltip-2"><a href="<?php echo base_url("Category/edit/$tstid"); ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Edit</span>
                          </div>
                          <div class="tooltip-2"><a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="trash-cat" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <span class="tooltiptext">Move to Trash</span>
                          </div>
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
              <div class="read-more">
                       <?php echo $links ;  ?>
                        </div>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>