<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {



public function __construct(){   

  parent:: __construct();

  $this->load->library('session');

  $this->load->model('Admin_model');

  $this->load->helper('url');

  $this->load->helper('csv');

  $this->load->library('form_validation');  

  $this->load->library('pagination');

  ini_set('max_execution_time', 0); 

}

 /*

     * dashboard - index

  */

public function index() {

  $data = array();

  if ($this->session->userdata('isUserLoggedIn')) {

    $this->dashboard();

  } else {

    redirect(base_url().'Admin/login');

  }

}

/*

     * User login

     */

public function login(){      

  $data = array();

  $con['conditions'] ="";

  $data['error_msg']=""; 

  $con['returnType'] = 'single';

  if($this->input->post('loginSubmit')) {      

      $con['user_login'] = array("yes");

      $con['conditions'] = array(

          'user_email'=>$this->input->post('user_email'),

          'user_password' => md5($this->input->post('user_password')),

          'user_type'=>"Admin",

          'status' => '0'

      );

      $checkLogin = $this->Admin_model->getRows($con);          

      if($checkLogin) {

        $this->session->set_userdata('isUserLoggedIn',TRUE);

        $this->session->set_userdata('user_id',$checkLogin['user_id']);

        $this->session->set_userdata('user_type','Admin');

        $this->session->set_userdata('timeout',time());

        redirect(base_url().'Admin/dashboard');

      }else{

            $this->session->set_flashdata("error", "Wrong email or password, please try again.");

            redirect(base_url("Admin/login"));

      }          

  } else {

    if( !empty($this->session->userdata('isUserLoggedIn')) && $this->session->userdata('user_type') == "Admin"){

        redirect(base_url().'Admin/dashboard');

      }

  }

  header("Access-Control-Allow-Origin: *");

  $this->template->set('title', 'Admin Login');

  $this->template->load('admin_login_layout', 'contents' , 'login_page', $data);

}

/*

     * Dashboard

     */

public function dashboard(){

    $type ="";

    $data = array();

    $user_count = 0;

    $profile_count = 0;

    $subs_count= 0;

    $event_count=0;

    if($this->session->userdata('isUserLoggedIn')){

      $con['returnType'] = 'count';



       $con['conditions'] = array(

        //'user_type'=> 'Personal',

        'add_status !='=> '3'

            );



      $event_count = $this->Admin_model->record_count($con, "otd_advertisement");

      



      $con1['conditions'] = array(

        'cat_status !='=> '3'

            );

      $category_count = $this->Admin_model->record_count($con1,"otd_business_category");

      //For  Users

      $con['conditions'] = array(

        'user_type'=> 'Personal',

        'status !='=> '3'

            );

      $user_count = $this->Admin_model->getRows($con);

      //For Pro Users

      $con['returnType'] = 'count';

      $con['conditions'] = array(

        'user_type'=> 'Professional',

        'status !='=> '3'

            );

      $profile_count = $this->Admin_model->getRows($con);

      //For Subscribers

      $con['returnType'] = 'count';

      $con['conditions'] = array(

        'status !='=> '3'

            );

      $subs_count = $this->Admin_model->getSubsRows($con);





      $data['user_count']    = $user_count;

      $data['profile_count'] = $profile_count;

      $data['subs_count']    = $subs_count;

      $data['event_count']   = $event_count;

      $data['category_count']   = $category_count;

           

      if($event_count > 0) {

         $data['events'] =  $this->Admin_model->getEventsAll();

      }else{

         $data['events'] =  array();

      }



      if($user_count > 0){

      $con = null;

      //For All Users

      $con['returnType'] = '';

      $con['limit'] = '5';

      $con['conditions'] = array(

        'user_type !='=> 'Admin',

        'status !='=> '3'

            );

      $data['userlist'] = $this->Admin_model->getRows($con);

      }

      else

      {

       $data['userlist'] = array();

      }



      //list of profiles



      if($profile_count > 0){

      $con = null;

      $con['returnType'] = '';

      $con['limit'] = '5';

      $con['conditions'] = array(

        'user_type'=> 'Professional',

        'status !='=> '3'

            );

      $data['profilelist'] = $this->Admin_model->getRows($con);

      }

      else

      {

       $data['profilelist'] = array();

      }



      //list of subscribers



      if($subs_count > 0){

      $con = null;

      $con = array();



      $con['returnType'] = '';

      $con['limit'] = '5';

      $con['sorting'] = array("subscription_date");

      $data['subslist'] = $this->Admin_model->getSubsRows($con);

      }

      else

      {

       $data['subslist'] = array();

      }



      $data['review_count'] = $this->Admin_model->getReviewRows(array("conditions"=>array("vote_status"=>0), "returnType"=>"count"));

      $data['claimed_count'] = $this->Admin_model->getClaimedRows(array("conditions"=>array("claim_status"=>0), "returnType"=>"count"));

      $data['purchased_package_count'] = $this->Admin_model->getUserOptionRows(array("conditions"=>array("opt_option_type"=>4, "opt_option_status"=>0), "returnType"=>"count"));

      header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'Admin Dashboard');

      $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

    }else{

      redirect(base_url().'Admin/login');

    }

 }



function option_change($args)

{



  $ses = $this->session->userdata("user_id");

  if(!empty($ses)){

    $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

    $this->form_validation->set_rules("opt_price", "option price", "required", array("required"=>"Please enter %s"));

    $this->form_validation->set_rules("status", "options status", "required", array("required"=>"Please enter %s"));

    if($this->form_validation->run() == FALSE){

      $data = array();

      $option_data = $this->Admin_model->getOptionDetails($args);

      $data['option_list'] = $option_data;

      header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'Package List');

      $this->template->load('admin_layout', 'contents' , 'option_change', $data);

    }else{

      $where = array("opt_price_id"=>$args);

      $arr = array("opt_price"=>$this->input->post("opt_price"),

                   "opt_pr_status"=>$this->input->post("status"),

                   "opt_text"=>$this->input->post("opt_price_type_text")

                  );

      $check = $this->Admin_model->updatePriceOption($arr, $where);

      if($check){

        $this->session->set_flashdata("success", "<label class='text-success'>Option details updated successfully</label>");

        redirect(base_url("Admin/option_list"));

      }



    }

  }else{

    redirect(base_url("Admin"));

  }



}



  public function editMasterOption($args){

    $ses = $this->session->userdata("user_id");

    if(!empty($args) && !empty($ses)){

      $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

      $this->form_validation->set_rules("name", "name", "required",array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("template", "description", "required",array("required"=>"Please enter %s"));

      if($this->form_validation->run() == FALSE){

        $data = array();

        $data['msoptdetails'] = $this->Admin_model->getMasterOptionRows(array("opt_id"=>$args));

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'User List');

        $this->template->load('admin_layout', 'contents' , 'masteroption/editoption', $data);

      }else{

        $array = array("opt_name"=>$this->input->post("name"), "opt_description"=>$this->input->post("template"));

        $check = $this->Admin_model->updateOption($array, array("opt_id"=>$args));

        if($check){

          $this->session->set_flashdata("success", "Option details updated successfully.");

          redirect(base_url("Admin/option_list"));    

        }else{

          $this->session->set_flashdata("error", "Option details not updated, please try again.");

          redirect(base_url("Admin/option_list"));  

        }

      }

    }else{

      $this->session->set_flashdata("error", "Invalid Access");

      redirect(base_url("Admin/option_list"));

    }

  }





  public function viewMasterOption($args){

    $ses = $this->session->userdata("user_id");

    if(!empty($args) && !empty($ses)){

      $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

      $this->form_validation->set_rules("name", "name", "required",array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("template", "description", "required",array("required"=>"Please enter %s"));

      if($this->form_validation->run() == FALSE){

        $data = array();

        $data['msoptdetails'] = $this->Admin_model->getMasterOptionRows(array("opt_id"=>$args));

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'User List');

        $this->template->load('admin_layout', 'contents' , 'masteroption/viewoption', $data);

      }else{

        $array = array("opt_name"=>$this->input->post("name"), "opt_description"=>$this->input->post("template"));

        $check = $this->Admin_model->updateOption($array, array("opt_id"=>$args));

        if($check){

          $this->session->set_flashdata("success", "Option details updated successfully.");

          redirect(base_url("Admin/option_list"));    

        }else{

          $this->session->set_flashdata("error", "Option details not updated, please try again.");

          redirect(base_url("Admin/option_list"));  

        }

      }

    }else{

      $this->session->set_flashdata("error", "Invalid Access");

      redirect(base_url("Admin/option_list"));

    }

  }



  function option_list()

  {

    $data = array();

    $option_data_typ2_2 = $this->Admin_model->getOptionDetailsAdmin();    

    $data['option_list_2_3'] = $option_data_typ2_2;

    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'Package List');

    $this->template->load('admin_layout', 'contents' , 'option_price', $data);

  }





function purchase_list()

{

  $data = array();

  if(!empty($this->input->post())){

    $useremail = $this->input->post("user_email");

    $option = $this->input->post("option");

    $expiry = $this->input->post("expiry");

    $this->session->set_userdata("user_email", $useremail);

    $this->session->set_userdata("option", $option);

    $this->session->set_userdata("expiry", $expiry);

    redirect(base_url("Admin/purchase_list"));

  }

  $data['purchase_list'] = $this->Admin_model->getUserPerchase(array("conditions"=>array("opt_option_type"=>1)));

  $data['purchase_package_list'] = $this->Admin_model->getUserPerchase(array("conditions"=>array("opt_option_type"=>4)));

  $data['user_purchasd_adv_options'] = $this->Admin_model->getUserAdvOptions();

  $data['optionList'] = $this->Admin_model->masterOptionList();

  

  header("Access-Control-Allow-Origin: *");

  $this->template->set('title', 'User List');

  $this->template->load('admin_layout', 'contents' , 'option_purchase_list', $data);

}



function clearoptionFilter(){

  $this->session->unset_userdata("user_email");

  $this->session->unset_userdata("option");

  $this->session->unset_userdata("expiry");

  redirect(base_url("Admin/purchase_list"));

}



function optionView($args = NULL){

  $data['details'] = $this->Admin_model->getUserOptionRows(array("opt_user_option_id"=>$args));

  header("Access-Control-Allow-Origin: *");

  $this->template->set('title', 'User List');

  $this->template->load('admin_layout', 'contents' , 'option_view', $data);

}





function formOptionView($frm_opt_id = NULL){

  $data['details'] = $this->Admin_model->getFormOptionRows($frm_opt_id);

  // echo "<pre>";

  // print_r($data['details']);

  // exit;

  header("Access-Control-Allow-Origin: *");

  $this->template->set('title', 'User List');

  $this->template->load('admin_layout', 'contents' , 'form_option_view', $data);

}



function removeOption(){

  $ses = $this->session->userdata("user_id");

  if(!empty($ses)){

    $id = $this->input->post("option");

    if(!empty($id)){

      $optiondetails = $this->Admin_model->getUserOptionRows(array("opt_user_option_id"=>$id));      

      if($optiondetails['opt_option_type'] == 4){

        $pkopts = $this->Admin_model->getPurchasedPackageOptionRows(array("conditions"=>array("opt_pkg_id"=>$optiondetails['opt_user_option_id'])));                

        foreach($pkopts as $pkopt){          

          $this->Admin_model->removeOption($pkopt['opt_user_option_id']);

        }

      }

      $check = $this->Admin_model->removeOption($id);

      if($check)

        echo json_encode(array("status"=>200, "msg"=>"Option deleted successfully."));

      else

        echo json_encode(array("status"=>1, "msg"=>"Something went wrong, Please try after sometime."));

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Required parameter is missing"));

    }

  }else{

    echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

  }

}





  function user_list($args = 0)

  {

    //Filter Code End

    $con['conditions'] = array("user_type"=>"Personal", "status!="=>3);

    if(!empty($this->session->userdata("serch_peremail"))){

      $con['filter']['user_email'] = $this->session->userdata("serch_peremail");

    }



    if(!empty($this->session->userdata("serch_perfname"))){

      $con['filter']['user_firstname'] = $this->session->userdata("serch_perfname");

    }

    if(!empty($this->session->userdata("serch_peremail"))){

      $con['filter']['user_lastname'] = $this->session->userdata("serch_peremail");

    }



    if(!empty($this->session->userdata("serch_persignupmethod"))){

      if($this->session->userdata("serch_persignupmethod") == 1)

        // $con['filter']['user_signup_method'] = "Regular";

      if($this->session->userdata("serch_persignupmethod") == 2)

        $con['filter']['user_signup_method'] = "Facebook";

      if($this->session->userdata("serch_persignupmethod") == 3)

        $con['filter']['user_signup_method'] = "Google";

    }



    $totalrows = $this->Admin_model->record_count($con);

    $config['base_url'] = base_url()."Admin/user_list/";

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 5;

    $config['num_links'] = 3;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';    

    $this->pagination->initialize($config);

    $con['limit'] = $config['per_page'];

    

    $con['start'] = $args;

    $data['userlist'] = array();



    if($this->session->userdata('isUserLoggedIn')){   

      if($totalrows > 0){

        $data['userlist'] = $this->Admin_model->getRows($con);

        $data["record_found"] = $totalrows; 

      }

      else

      {

        $data["record_found"] = 0; 

      }

      $data["links"] = $this->pagination->create_links();

      $data['type'] = "Personal";

      $data['usertype'] = "Personal";

      // $data['order'] = $order;

      // $data['page'] = $page;

      header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'User List');

      $this->template->load('admin_layout', 'contents' , 'users', $data);

    }else{

      redirect(base_url().'Admin/login');

    }

  }



  public function downloadPerUserCSV(){

    $con['conditions'] = array("user_type"=>"Personal", "status!="=>3);

    if(!empty($this->session->userdata("serch_peremail"))){

      $con['filter']['user_email'] = $this->session->userdata("serch_peremail");

    }

    if(!empty($this->session->userdata("serch_perfname"))){

      $con['filter']['user_firstname'] = $this->session->userdata("serch_perfname");

    }

    if(!empty($this->session->userdata("serch_peremail"))){

      $con['filter']['user_lastname'] = $this->session->userdata("serch_peremail");

    }



    if(!empty($this->session->userdata("serch_persignupmethod"))){

      if($this->session->userdata("serch_persignupmethod") == 1)

       // $con['filter']['user_signup_method'] = "Regular";

       //$con['filter']['user_signup_method'] = "Regular";

       if($this->session->userdata("serch_persignupmethod") == 2)

        $con['filter']['user_signup_method'] = "Facebook";

      if($this->session->userdata("serch_persignupmethod") == 3)

        $con['filter']['user_signup_method'] = "Google";

    }



    $query = $this->Admin_model->downloadRows($con);

    query_to_csv($query,TRUE,'Users_'.date('dMy').'.csv');

  }



  public function personalUserSetDelimiter(){

    $this->session->set_userdata("serch_perfname", $this->input->post("userfirstname"));

    $this->session->set_userdata("serch_perlname", $this->input->post("userlastname"));

    $this->session->set_userdata("serch_peremail", $this->input->post("user_email"));

    $this->session->set_userdata("serch_persignupmethod", $this->input->post("signup_method"));

    redirect(base_url("Admin/user_list"));

  }



  public function clearPersonalUserSetDelimiter(){

    $this->session->unset_userdata("serch_peremail");

    $this->session->unset_userdata("serch_perfname");

    $this->session->unset_userdata("serch_perlname");

    $this->session->unset_userdata("serch_persignupmethod");

    redirect(base_url("Admin/user_list"));

  }



  function user_list_pro()

  {

    $segcount = $this->uri->total_segments();

    $order_by = "";

    $order="";

    $type ="";

    $page = 0;

    $data = array();

    //echo "I am here: 338 (".$this->session->userdata('profltr_signup_method').")";

    if($segcount == 3)

    {

    $this->session->unset_userdata('profltr_user_firstname');

    $this->session->unset_userdata('profltr_user_lastname');

    $this->session->unset_userdata('profltr_user_language');

    $this->session->unset_userdata('profltr_user_email');

    $this->session->unset_userdata('profltr_signup_method');

    $this->session->unset_userdata('profltr_user_company_name');

    $this->session->unset_userdata('profltr_user_company_number');

    $this->session->unset_userdata('profltr_user_company_address');



    $fltr_user_firstname = "";

    $fltr_user_lastname = "";

    $fltr_user_language = "";

    $fltr_user_company_name = "";

    $fltr_user_company_address = "";

    $fltr_user_company_number = "";

    $fltr_user_email = "";

    $fltr_signup_method = "";



   

    $data['fltr_user_firstname'] = "";

    $data['fltr_user_lastname'] = "";

    $data['fltr_user_language'] = "";

    $data['fltr_user_email'] = "";

    $data['fltr_signup_method'] = "";

    $data['fltr_user_company_name'] = "";

    $data['fltr_user_company_number'] = "";

    $data['fltr_user_company_address'] = "";

    }



    if($segcount == 3)

    {

      $type = $this->uri->segment(3);

    }

    else if ($segcount == 5)

    {

      $order = $this->uri->segment(4);

      $type = $this->uri->segment(3);

    }

    else if ($segcount == 4)

    {

      $order = "company";

      $type = $this->uri->segment(3);

    }

    else if($segcount < 3)

    {

      $order = "company";

      $type = "Professional";

    }



    if($order == 'firstname')

    {

      $order_by = "user_firstname";

    }

    else if($order == 'lastname')

    {

      $order_by = "user_lastname";

    }

    else if($order == 'email')

    {

      $order_by = "user_email";

    }

    else if($order == 'company')

    {

      $order_by = "user_company_name";

    }

    else if($order == 'number')

    {

      $order_by = "user_phone";

    }

    else if($order == 'address')

    {

      $order_by = "user_address";

    }

    else if($order == 'lastlogin')

    {

      $order_by = "last_login";

    }

    else

    {

      $order_by = "user_company_name";

    }



    if(is_numeric($type))

    {

     $type = "Professional";

    }



    // $con['sorting'] = array('order'=>$order_by);



    $data["type"] = $type;

    $con['conditions'] = array('user_type'=>'Personal');



    //Filter Code

   // echo "I am here: 432 (".$this->session->userdata('profltr_signup_method').")";

    if($this->input->post('filterSubmit'))

    {

      $fltr_user_firstname = $this->input->post('user_firstname');

      $fltr_user_lastname = $this->input->post('user_lastname');

      $fltr_user_language = $this->input->post('user_language');

      $fltr_user_email = $this->input->post('user_email');

      $fltr_signup_method = $this->input->post('signup_method');

      $fltr_user_company_name = $this->input->post('user_company_name');

      $fltr_user_company_number = $this->input->post('user_company_number');

      $fltr_user_company_address = $this->input->post('user_company_address');

    



      $this->session->set_userdata('profltr_user_firstname', $fltr_user_firstname);

      $this->session->set_userdata('profltr_user_lastname', $fltr_user_lastname);

      $this->session->set_userdata('profltr_user_language', $fltr_user_language);

      $this->session->set_userdata('profltr_user_email', $fltr_user_email);

      $this->session->set_userdata('profltr_signup_method', $fltr_signup_method);

      $this->session->set_userdata('profltr_user_company_name', $fltr_user_company_name);

      $this->session->set_userdata('profltr_user_company_number', $fltr_user_company_number);

      $this->session->set_userdata('profltr_user_company_address', $fltr_user_company_address);



      $data['fltr_user_firstname'] = $fltr_user_firstname;

      $data['fltr_user_lastname'] = $fltr_user_lastname;

      $data['fltr_user_language'] = $fltr_user_language;

      $data['fltr_user_email'] = $fltr_user_email;

      $data['fltr_signup_method'] = $fltr_user_email;

      $data['fltr_user_company_name'] = $fltr_user_company_name;

      $data['fltr_user_company_number'] = $fltr_user_company_number;

      $data['fltr_user_company_address'] = $fltr_user_company_address;



     // echo "I am here: 463 (".$this->session->userdata('profltr_signup_method').")";

    }

    else

    {

      //die("here");

      //echo "I am here: 468   (".$this->session->userdata('profltr_signup_method').")";

      $fltr_user_firstname = $this->session->userdata('profltr_user_firstname');

      $fltr_user_lastname = $this->session->userdata('profltr_user_lastname');

      $fltr_user_language = $this->session->userdata('profltr_user_language');

      $fltr_user_email = $this->session->userdata('profltr_user_email');

      $fltr_signup_method = $this->session->userdata('profltr_signup_method');

      $fltr_user_company_name = $this->session->userdata('profltr_user_company_name');

      $fltr_user_company_number = $this->session->userdata('profltr_user_company_number');

      $fltr_user_company_address = $this->session->userdata('profltr_user_company_address');



      

      $this->session->set_userdata('profltr_user_firstname', $fltr_user_firstname);

      $this->session->set_userdata('profltr_user_lastname', $fltr_user_lastname);

      $this->session->set_userdata('profltr_user_language', $fltr_user_language);

      $this->session->set_userdata('profltr_user_email', $fltr_user_email);

      $this->session->set_userdata('profltr_signup_method', $fltr_signup_method);

      $this->session->set_userdata('profltr_user_company_name', $fltr_user_company_name);

      $this->session->set_userdata('profltr_user_company_number', $fltr_user_company_number);

      $this->session->set_userdata('profltr_user_company_address', $fltr_user_company_address);

    //  echo "<br>I am here: 487 (".$this->session->userdata('profltr_signup_method').")";



      if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit'))

      {

        $this->session->unset_userdata('profltr_user_firstname');

        $this->session->unset_userdata('profltr_user_lastname');

        $this->session->unset_userdata('profltr_user_language');

        $this->session->unset_userdata('profltr_signup_method');

        $this->session->unset_userdata('profltr_user_email');

        $this->session->unset_userdata('profltr_user_company_name');

        $this->session->unset_userdata('profltr_user_company_number');

        $this->session->unset_userdata('profltr_user_company_address');



        $fltr_user_firstname = "";

        $fltr_user_lastname = "";

        $fltr_user_language = "";

        $fltr_user_company_name = "";

        $fltr_user_company_address = "";

        $fltr_user_company_number = "";

        $fltr_user_email = "";

        $fltr_signup_method = "";



        $data['fltr_user_firstname'] = "";

        $data['fltr_user_lastname'] = "";

        $data['fltr_user_language'] = "";

        $data['fltr_user_email'] = "";

        $data['fltr_signup_method'] = "";

        $data['fltr_user_company_name'] = "";

        $data['fltr_user_company_number'] = "";

        $data['fltr_user_company_address'] = "";

       // echo "<br>I am here: 517 (".$this->session->userdata('profltr_signup_method').")";

      }

    }





    if($fltr_user_firstname != "")

    {

      $con['filter']['user_firstname'] =$fltr_user_firstname;

    }

    if($fltr_user_lastname != "")

    {

      $con['filter']['user_lastname'] =$fltr_user_lastname;

    }



    if($fltr_user_language != "")

    {

      //echo "<br>I am here: 533 (".$this->session->userdata('profltr_signup_method').")";

      $con['filter']['user_language'] =$fltr_user_language;

      /*

      if($fltr_signup_method ==2){$con['filter']['user_language'] ="English";}  

      if($fltr_signup_method ==3){$con['filter']['user_language'] ="French";}

      if($fltr_signup_method ==4){$con['filter']['user_language'] ="Spanish";}

      if($fltr_signup_method ==5){$con['filter']['user_language'] ="Arab";}

      if($fltr_signup_method ==6){$con['filter']['user_language'] ="Italion";}

      if($fltr_signup_method ==7){$con['filter']['user_language'] ="German";}*/

    }



    if($fltr_user_email != "")

    {

      $con['filter']['user_email'] =$fltr_user_email;

    }

   // echo "<br>I am here: 548 (".$this->session->userdata('profltr_signup_method').")";

    if($fltr_signup_method != "" && $fltr_signup_method != "-1")

    {

      if($fltr_signup_method ==1){$con['filter']['user_signup_method'] = "Regular";}

      if($fltr_signup_method ==2){$con['filter']['user_signup_method'] = "Facebook";}  

      if($fltr_signup_method ==3){$con['filter']['user_signup_method'] = "Google";}

      if($fltr_signup_method ==4){$con['filter']['user_signup_method'] = "Imported";}

    }

    if($fltr_user_company_name != "")

    {

      $con['filter']['user_company_name'] =$fltr_user_company_name;

    }

    if($fltr_user_company_number != "")

    {

      $con['filter']['user_phone'] =$fltr_user_company_number;

    }

    if($fltr_user_company_address != "")

    {

      $con['filter']['user_address'] =$fltr_user_company_address;

    }



   // echo "<br>I am here: 569 (".$this->session->userdata('profltr_signup_method').")";



    $totalrows = $this->Admin_model->record_count($con);

   // echo $this->db->last_query();

    $config['base_url'] = base_url()."Admin/user_list_pro/company";

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 4;

    $config['num_links'] = 3;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    // $choice = $config["total_rows"] / $config["per_page"];

    // $config["num_links"] = round($choice);

    $this->pagination->initialize($config);



    $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

    if(is_numeric($this->uri->segment(4)))

    {

      $page = ($this->uri->segment(4));

    }

    else if(is_numeric($this->uri->segment(3)))

    {

      $page = ($this->uri->segment(3));

    }

    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];



    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }

    $con['start'] = $start;

    $data['userlist'] = array();



    if($this->session->userdata('isUserLoggedIn')){



      if($totalrows > 0){

   

        $data['userlist'] = $this->Admin_model->getRows($con);

        //echo $this->db->last_query(); die;

        $data["record_found"] = $totalrows; 

      }

      else

      {

        $data["record_found"] = 0; 

      }



      $data["links"] = $this->pagination->create_links();

      $data['usertype'] = $type;

      $data['order'] = $order;

      $data['page'] = $page;



      header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'User List');

      $this->template->load('admin_layout', 'contents' , 'pro_users', $data);

    }else{

     redirect(base_url().'Admin/login');

    }

  }



  function usersListWaitingForApprroval()

  {

    $segcount = $this->uri->total_segments();

    $order_by = "";

    $order="";

    $type ="";

    $page = 0;



    $this->session->unset_userdata('profltr_user_firstname');

    $this->session->unset_userdata('profltr_user_lastname');

    $this->session->unset_userdata('profltr_user_language');

    $this->session->unset_userdata('profltr_user_email');

    $this->session->unset_userdata('profltr_signup_method');

    $this->session->unset_userdata('profltr_user_company_name');

    $this->session->unset_userdata('profltr_user_company_number');

    $this->session->unset_userdata('profltr_user_company_address');



    $fltr_user_firstname = "";

    $fltr_user_lastname = "";

    $fltr_user_language = "";

    $fltr_user_company_name = "";

    $fltr_user_company_address = "";

    $fltr_user_company_number = "";

    $fltr_user_email = "";

    $fltr_signup_method = "";



    $data = array();

    $data['fltr_user_firstname'] = "";

    $data['fltr_user_lastname'] = "";

    $data['fltr_user_language'] = "";

    $data['fltr_user_email'] = "";

    $data['fltr_signup_method'] = "";

    $data['fltr_user_company_name'] = "";

    $data['fltr_user_company_number'] = "";

    $data['fltr_user_company_address'] = "";





    if($segcount == 3)

    {

    $type = $this->uri->segment(3);

    }

    else if ($segcount == 5)

    {

    $order = $this->uri->segment(4);

     $type = $this->uri->segment(3);

    }

    else if ($segcount == 4)

    {

    $order = "company";

    $type = $this->uri->segment(3);

    }

    else if($segcount < 3)

    {

     $order = "company";

     $type = "Professional";

    }





    if($order == 'firstname')

    {

    $order_by = "user_firstname";

    }

    else if($order == 'lastname')

    {

    $order_by = "user_lastname";

    }

    else if($order == 'email')

    {

    $order_by = "user_email";

    }

    else if($order == 'company')

    {

    $order_by = "user_company_name";

    }

    else if($order == 'number')

    {

    $order_by = "user_company_number";

    }

    else if($order == 'address')

    {

    $order_by = "user_company_address";

    }

    else if($order == 'lastlogin')

    {

    $order_by = "last_login";

    }

    else

    {

    $order_by = "user_company_name";

    }



    if(is_numeric($type))

    {

     $type = "Professional";

    }



    // $con['sorting'] = array('order'=>$order_by);



    $data["type"] = $type;

    $con['conditions'] = array('user_type'=> 'Professional');



    //Filter Code



    if($this->input->post('filterSubmit'))

    {

      $fltr_user_firstname = $this->input->post('user_firstname');

      $fltr_user_lastname = $this->input->post('user_lastname');

      $fltr_user_language = $this->input->post('user_language');

      $fltr_user_email = $this->input->post('user_email');

      $fltr_signup_method = $this->input->post('signup_method');

      $fltr_user_company_name = $this->input->post('user_company_name');

      $fltr_user_company_number = $this->input->post('user_company_number');

      $fltr_user_company_address = $this->input->post('user_company_address');



      $this->session->set_userdata('profltr_user_firstname', $fltr_user_firstname);

      $this->session->set_userdata('profltr_user_lastname', $fltr_user_lastname);

      $this->session->set_userdata('profltr_user_language', $fltr_user_language);

      $this->session->set_userdata('profltr_user_email', $fltr_user_email);

      $this->session->set_userdata('profltr_signup_method', $fltr_signup_method);

      $this->session->set_userdata('profltr_user_company_name', $fltr_user_company_name);

      $this->session->set_userdata('profltr_user_company_number', $fltr_user_company_number);

      $this->session->set_userdata('profltr_user_company_address', $fltr_user_company_address);



      $data['fltr_user_firstname'] = $fltr_user_firstname;

      $data['fltr_user_lastname'] = $fltr_user_lastname;

      $data['fltr_user_language'] = $fltr_user_language;

      $data['fltr_user_email'] = $fltr_user_email;

      $data['fltr_signup_method'] = $fltr_user_email;

      $data['fltr_user_company_name'] = $fltr_user_company_name;

      $data['fltr_user_company_number'] = $fltr_user_company_number;

      $data['fltr_user_company_address'] = $fltr_user_company_address;

    }

    else

    {

      $fltr_user_firstname = $this->session->userdata('profltr_user_firstname');

      $fltr_user_lastname = $this->session->userdata('profltr_user_lastname');

      $fltr_user_language = $this->session->userdata('profltr_user_language');

      $fltr_user_email = $this->session->userdata('profltr_user_email');

      $fltr_signup_method = $this->session->userdata('profltr_signup_method');

      $fltr_user_company_name = $this->session->userdata('profltr_user_company_name');

      $fltr_user_company_number = $this->session->userdata('profltr_user_company_number');

      $fltr_user_company_address = $this->session->userdata('profltr_signup_methoduser_company_address');



      if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit'))

      {

        $this->session->unset_userdata('profltr_user_firstname');

        $this->session->unset_userdata('profltr_user_lastname');

        $this->session->unset_userdata('profltr_user_language');

        $this->session->unset_userdata('profltr_signup_method');

        $this->session->unset_userdata('profltr_user_email');

        $this->session->unset_userdata('profltr_user_company_name');

        $this->session->unset_userdata('profltr_user_company_number');

        $this->session->unset_userdata('profltr_user_company_address');



        $fltr_user_firstname = "";

        $fltr_user_lastname = "";

        $fltr_user_language = "";

        $fltr_user_company_name = "";

        $fltr_user_company_address = "";

        $fltr_user_company_number = "";

        $fltr_user_email = "";

        $fltr_signup_method = "";



        $data['fltr_user_firstname'] = "";

        $data['fltr_user_lastname'] = "";

        $data['fltr_user_language'] = "";

        $data['fltr_user_email'] = "";

        $data['fltr_signup_method'] = "";

        $data['fltr_user_company_name'] = "";

        $data['fltr_user_company_number'] = "";

        $data['fltr_user_company_address'] = "";

      }

    }





    if($fltr_user_firstname != "")

    {

      $con['filter']['user_firstname'] =$fltr_user_firstname;

    }

    if($fltr_user_lastname != "")

    {

      $con['filter']['user_lastname'] =$fltr_user_lastname;

    }



    if($fltr_user_language != "")

    {

      $con['filter']['user_language'] =$fltr_user_language;

      /*

      if($fltr_signup_method ==2){$con['filter']['user_language'] ="English";}  

      if($fltr_signup_method ==3){$con['filter']['user_language'] ="French";}

      if($fltr_signup_method ==4){$con['filter']['user_language'] ="Spanish";}

      if($fltr_signup_method ==5){$con['filter']['user_language'] ="Arab";}

      if($fltr_signup_method ==6){$con['filter']['user_language'] ="Italion";}

      if($fltr_signup_method ==7){$con['filter']['user_language'] ="German";}*/

    }



    if($fltr_user_email != "")

    {

      $con['filter']['user_email'] =$fltr_user_email;

    }

    if($fltr_signup_method != "" && $fltr_signup_method != "-1")

    {

      if($fltr_signup_method ==1){

        // $con['filter']['user_signup_method'] = "Regular";

      }

      if($fltr_signup_method ==2){$con['filter']['user_signup_method'] = "Facebook";}  

      if($fltr_signup_method ==3){$con['filter']['user_signup_method'] = "Google";}

    }

    if($fltr_user_company_name != "")

    {

      $con['filter']['user_company_name'] =$fltr_user_company_name;

    }

    if($fltr_user_company_number != "")

    {

      $con['filter']['user_company_number'] =$fltr_user_company_number;

    }

    if($fltr_user_company_address != "")

    {

      $con['filter']['user_company_address'] =$fltr_user_company_address;

    }



    $con['filter']['status'] = 0;

    $totalrows = $this->Admin_model->record_count($con);

    $config['base_url'] = base_url()."Admin/usersListWaitingForApprroval/company/";

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 4;

    $config['num_links'] = 3;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    $this->pagination->initialize($config);



    $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

    if(is_numeric($this->uri->segment(4)))

    {

      $page = ($this->uri->segment(4));

    }

    else if(is_numeric($this->uri->segment(3)))

    {

      $page = ($this->uri->segment(3));

    }





    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];



    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }

    $con['start'] = $start;

    $data['userlist'] = array();



    if($this->session->userdata('isUserLoggedIn')){

      if($totalrows > 0){

        // echo "<pre>";

        // print_r($con);

        // exit;

        $data['userlist'] = $this->Admin_model->getRows($con);

        $data["record_found"] = $totalrows; 

      }

      else

      {

        $data["record_found"] = 0; 

      }



      $data["links"] = $this->pagination->create_links();

      $data['usertype'] = $type;

      $data['order'] = $order;

      $data['page'] = $page;



      header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'User List');

      $this->template->load('admin_layout', 'contents' , 'user_pro_waiting', $data);

    }else{

     redirect(base_url().'Admin/login');

    }

  }



  function deleteproUser(){

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      $id = $this->input->post("id");

      if(!empty($id)){

        $this->Admin_model->deleteUserFromSite(array($id));

        $this->session->set_flashdata("success", "Deleted User!! .");

        echo json_encode(array("status"=>200));

      }else{

        echo json_encode(array("status"=>1, "msg"=>"Required parameter missing."));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }



  function subs_list()

  {



    $this->session->unset_userdata('subsfltr_user_email');

    $this->session->unset_userdata('subfltr_signup_method');



    $fltr_user_email = "";

    $fltr_signup_method = "";



    $data = array();

    $data['fltr_user_email'] = "";

    $data['fltr_signup_method'] = "";

    $segcount = $this->uri->total_segments();

    $order_by = "";

    $order="";

    $type ="";

    $page = 0;



    if($segcount == 3)

    {

    $order = $this->uri->segment(3);

    }

    else if ($segcount == 4)

    {

    $order = $this->uri->segment(3);

    }

    else if($segcount < 3)

    {

     $order = "email";

    }



    if($order == 'postcode')

    {

    $order_by = "user_postcode";

    }

    else if($order == 'email')

    {

    $order_by = "user_email";

    }

    else

    {

      $order_by = "subs_id";

    }







    $con['sorting'] = array('order'=>$order_by);

    //$data = array();



    //Filter Code



    if($this->input->post('filterSubmit'))

     {



    $fltr_user_email = $this->input->post('user_email');

    $fltr_signup_method = $this->input->post('signup_method');



    $this->session->set_userdata('subfltr_user_email', $fltr_user_email);

    $this->session->set_userdata('subfltr_signup_method', $fltr_signup_method);

    $data['fltr_user_email'] = $fltr_user_email;

    $data['fltr_signup_method'] = $fltr_user_email;

    }

    else

    {

    $fltr_user_email = $this->session->userdata('subfltr_user_email');

    $fltr_signup_method = $this->session->userdata('subfltr_signup_method');



     if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit'))

     {

       $this->session->unset_userdata('subfltr_signup_method');

       $this->session->unset_userdata('subfltr_user_email');

       

       $this->session->unset_userdata('subprofltr_signup_method');

       $this->session->unset_userdata('subprofltr_user_email');



       $fltr_user_email = "";

       $fltr_signup_method = "";

      $data['fltr_user_email'] = "";

      $data['fltr_signup_method'] = "";



     }

    }



    if($fltr_user_email != "")

    {

    $con['filter']['user_email'] =$fltr_user_email;

    }



    if($fltr_signup_method != "" && $fltr_signup_method != "-1")

    {

          if($fltr_signup_method ==1){$con['filter']['promo_mail'] = "1";}

          if($fltr_signup_method ==2){$con['filter']['promo_sms'] = "1";}  

         

    }



    //Filter Code End



    $totalrows = $this->Admin_model->record_count($con, "subscribers");

    $config['base_url'] = base_url()."Admin/subs_list/".$order;

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 4;

    $config['num_links'] = $totalrows / 10;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    $choice = $config["total_rows"] / $config["per_page"];

    $config["num_links"] = round($choice);

    $this->pagination->initialize($config);

    $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

    if(is_numeric($this->uri->segment(4)))

    {

      $page = ($this->uri->segment(4));

    }

    else if(is_numeric($this->uri->segment(3)))

    {

      $page = ($this->uri->segment(3));

    }



    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];

    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }

    $con['start'] = $start;

    if($this->session->userdata('isUserLoggedIn'))

            {

    if($totalrows > 0){

    $data['subslist'] = $this->Admin_model->getSubsRows($con);

    $data["record_found"] = $totalrows; 

    }

    else

    {

     $data['subslist'] = array();

     $data["record_found"] = 0; 

    }





    $data["links"] = $this->pagination->create_links();

    $data['order'] = $order;

    $data['page'] = $page;



    header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'User List');

      $this->template->load('admin_layout', 'contents' , 'subscribers', $data);

    }else{

     redirect(base_url().'Admin/login');

    }



  }



  function deleteSubscriber(){

    $ids = $this->input->post("id");

    if( !empty($this->session->userdata('isUserLoggedIn')) && $this->session->userdata('user_type') == "Admin"){

      if(!empty($ids)){

        $check = $this->Admin_model->deleteSubscriber($ids);

        if($check){

          echo json_encode(array("status"=>200, "msg"=>"done"));

        }else{

          echo json_encode(array("status"=>1, "msg"=>"Something went wrong, Please try after sometime."));

        }

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }



  function deleteAllSubscriber(){

    if( !empty($this->session->userdata('isUserLoggedIn')) && $this->session->userdata('user_type') == "Admin"){      

      $check = $this->Admin_model->deleteAllSubscriberRows();

      if($check){

        echo json_encode(array("status"=>200, "msg"=>"done"));

      }else{

        echo json_encode(array("status"=>1, "msg"=>"Something went wrong, Please try after sometime."));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }





function subs_list_download()

{

  $order_by = "";

  $order="";

  $con['sorting'] = array('order'=>$order_by);

  $data = array();

  $query = $this->Admin_model->downloadRowsSubs($con);

  query_to_csv($query,TRUE,'Subscribers_'.date('dMy').'.csv');

}



function user_list_download()

{

  $order_by = "";

  $order="";

  $con['sorting'] = array('order'=>$order_by);

  $data = array();

  $type =  $this->uri->segment(3);

  if($type != "" && $type != "All" && is_numeric($type) != true)

  {

    $con['conditions'] = array(

    'user_type'=> $type,

    'status !='=> '3'

        );

  }

  else

  {

        $con['conditions'] = array(

             'status !='=> '3'

        );

  }



  //Filter Code



    if($this->input->post('filterSubmit')){

      $fltr_user_firstname = $this->input->post('user_firstname]');

      $fltr_user_lastname = $this->input->post('user_lastname');

      $fltr_user_language = $this->input->post('user_language');

      $fltr_user_email = $this->input->post('user_email');

      $fltr_signup_method = $this->input->post('signup_method');

      $fltr_user_company_name = $this->input->post('user_company_name');

      $fltr_user_company_number = $this->input->post('user_company_number');

      $fltr_user_company_address = $this->input->post('user_company_address');



      $this->session->set_userdata('profltr_user_firstname', $fltr_user_firstname);

      $this->session->set_userdata('profltr_user_lastname', $fltr_user_lastname);

      $this->session->set_userdata('profltr_user_language', $fltr_user_language);

      $this->session->set_userdata('profltr_user_email', $fltr_user_email);

      $this->session->set_userdata('profltr_signup_method', $fltr_signup_method);

      $this->session->set_userdata('profltr_user_company_name', $fltr_user_company_name);

      $this->session->set_userdata('profltr_user_company_number', $fltr_user_company_number);

      $this->session->set_userdata('profltr_user_company_address', $fltr_user_company_address);



      $data['fltr_user_firstname'] = $fltr_user_firstname;

      $data['fltr_user_lastname'] = $fltr_user_lastname;

      $data['fltr_user_language'] = $fltr_user_language;

      $data['fltr_user_email'] = $fltr_user_email;

      $data['fltr_signup_method'] = $fltr_user_email;

      $data['fltr_user_company_name'] = $fltr_user_company_name;

      $data['fltr_user_company_number'] = $fltr_user_company_number;

      $data['fltr_user_company_address'] = $fltr_user_company_address;

    }else{

      $fltr_user_firstname = $this->session->userdata('profltr_user_firstname');

      $fltr_user_lastname = $this->session->userdata('profltr_user_lastname');

      $fltr_user_language = $this->session->userdata('profltr_user_language');

      $fltr_user_email = $this->session->userdata('profltr_user_email');

      $fltr_signup_method = $this->session->userdata('profltr_signup_method');

      $fltr_user_company_name = $this->session->userdata('profltr_user_company_name');

      $fltr_user_company_number = $this->session->userdata('profltr_user_company_number');

      $fltr_user_company_address = $this->session->userdata('profltr_signup_methoduser_company_address');



      if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit')){

        $this->session->unset_userdata('profltr_user_firstname');

        $this->session->unset_userdata('profltr_user_lastname');

        $this->session->unset_userdata('profltr_user_language');

        $this->session->unset_userdata('profltr_signup_method');

        $this->session->unset_userdata('profltr_user_email');

        $this->session->unset_userdata('profltr_user_company_name');

        $this->session->unset_userdata('profltr_user_company_number');

        $this->session->unset_userdata('profltr_user_company_address');



        $fltr_user_firstname = "";

        $fltr_user_lastname = "";

        $fltr_user_language = "";

        $fltr_user_company_name = "";

        $fltr_user_company_address = "";

        $fltr_user_company_number = "";

        $fltr_user_email = "";

        $fltr_signup_method = "";



        $data['fltr_user_firstname'] = "";

        $data['fltr_user_lastname'] = "";

        $data['fltr_user_language'] = "";

        $data['fltr_user_email'] = "";

        $data['fltr_signup_method'] = "";

        $data['fltr_user_company_name'] = "";

        $data['fltr_user_company_number'] = "";

        $data['fltr_user_company_address'] = "";

      }

    }



    if($fltr_user_firstname != ""){

      $con['filter']['user_firstname'] =$fltr_user_firstname;

    }

    if($fltr_user_lastname != ""){

      $con['filter']['user_lastname'] =$fltr_user_lastname;

    }

    if($fltr_user_language != "")

    {

      $con['filter']['user_language'] =$fltr_user_language;



      if($fltr_signup_method ==2){$con['filter']['user_language'] ="English";}  

      if($fltr_signup_method ==3){$con['filter']['user_language'] ="French";}

      if($fltr_signup_method ==4){$con['filter']['user_language'] ="Spanish";}

      if($fltr_signup_method ==5){$con['filter']['user_language'] ="Arab";}

      if($fltr_signup_method ==6){$con['filter']['user_language'] ="Italion";}

      if($fltr_signup_method ==7){$con['filter']['user_language'] ="German";}

    }



    if($fltr_user_email != "")

    {

      $con['filter']['user_email'] =$fltr_user_email;

    }

    if($fltr_signup_method != "" && $fltr_signup_method != "-1")

    {

      if($fltr_signup_method ==1){

        // $con['filter']['user_signup_method'] = "Regular";

      }

      if($fltr_signup_method ==2){$con['filter']['user_signup_method'] = "Facebook";}  

      if($fltr_signup_method ==3){$con['filter']['user_signup_method'] = "Google";}

    }

    if($fltr_user_company_name != "")

    {

      $con['filter']['user_company_name'] =$fltr_user_company_name;

    }

    if($fltr_user_company_number != "")

    {

      $con['filter']['user_company_number'] =$fltr_user_company_number;

    }

    if($fltr_user_company_address != "")

    {

      $con['filter']['user_company_address'] =$fltr_user_company_address;

    }



  //Filter Code End



    $query = $this->Admin_model->downloadRows($con);

    query_to_csv($query,TRUE,'Users_'.date('dMy').'.csv');



}











    

    











    /*

     * User logout

     */

    public function logout(){

        $this->session->unset_userdata('isUserLoggedIn');

        $this->session->unset_userdata('user_Id');

        $this->session->unset_userdata('user_type');

        $this->session->sess_destroy();

        redirect(base_url().'Admin/');

    }

    

    /*

     * Existing email check during validation

     */

    public function email_check($str){

         $con['returnType'] = 'count';

        $con['conditions'] = array('user_email'=>$str);

        $checkEmail = $this->Admin_model->getRows($con);

        if($checkEmail > 0){

            $this->form_validation->set_message('email_check', 'The given email already exists.');

            return FALSE;

        } else {

            return TRUE;

        }

    }

  public function createbusiness()

  {

            $data = array();

            $data['error_msg']=""; 

            $userData = array();

            if($this->input->post('regisSubmit')){

                $this->form_validation->set_rules('user_company_name', 'Company Name', 'required');

                $this->form_validation->set_rules('user_company_address', 'Company Address', 'required');

                $this->form_validation->set_rules('user_company_number', 'Company Number', 'required|is_unique[users.user_company_number]',

                array(

                        'required'      => 'You have not provided %s.',

                        'is_unique'     => 'This %s already exists.'

                      

                ));



                $this->form_validation->set_rules('user_firstname', 'First Name', 'required');

                $this->form_validation->set_rules('user_lastname', 'Last Name', 'required');

                $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[users.user_email]',

                array(

                        'required'      => 'You have not provided %s.',

                        'is_unique'     => 'This %s already exists.',

                        'valid_email'     => 'This %s already exists.'

                ));

                $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',

                array(

                        'required'      => 'You have not provided %s.',

                        'numeric'     => 'This %s not numeric value.',

                        'is_natural'     => 'Only natural numbers can be accepted.'

                ));



                $this->form_validation->set_rules('user_password', 'password', 'required|min_length[6]',

                array(

                        'required'      => 'You have not provided %s.',

                        'min_length'     => 'This %s can not be less than 6 digit.'

                ));



                $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');

                $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[user_password]');

                $this->form_validation->set_rules("user_categories", "categories", "required", array("required"=>"Please select %s"));

                $this->form_validation->set_error_delimiters('', '');





                if(!null == $this->input->post('user_language'))

                {

                            $langs = implode(",", $this->input->post('user_language'));

                }

                else

                {

                    $langs="1";

                }



                $userData = array(

                    'user_firstname' => strip_tags($this->input->post('user_firstname')),

                    'user_lastname' => strip_tags($this->input->post('user_lastname')),

                    'user_email' => strip_tags($this->input->post('user_email')),

                    'user_phone' => strip_tags($this->input->post('user_phone')),

                   // 'user_address' => strip_tags($this->input->post('user_address')),

                    'user_password' => md5($this->input->post('user_password')),

                    'user_gender' => $this->input->post('user_gender'),

                    'user_phone' => strip_tags($this->input->post('user_phone')),

                    'user_type' => strip_tags($this->input->post('user_type')),

                    'auth_check' => strip_tags($this->input->post('auth_check')),

                    'sms_check' => strip_tags($this->input->post('sms_check')),

                    'user_language' => $langs,

                    'user_categories' => strip_tags($this->input->post('user_categories')),

                    'user_company_name' => strip_tags($this->input->post('user_company_name')),

                    'user_company_address' => strip_tags($this->input->post('user_company_address')),

                    'user_company_number' => strip_tags($this->input->post('user_company_number')),

                );



                $activelink="";



                $email = $userData['user_email'];

                $emailadmin="Lloyd@otourdemoi.fr, votive.pradeep01@gmail.com";

                $subject = "Registration of Professional Account";

                $subjectadmin = "Registration of Professional Account from website";

                $msginner = "Thank you, the administrator will review the application and validate the account<br><br><br>Thanks";

/*

                $msg = '

                            <html>

                            <head>

                            <title>Please Activate Your Account</title>

                            </head>

                            <body>

                            <h3>'.$subject.'</h3>

                            <p>'.$msginner.'</p><br>

                            <p>With Regards</p>

                            <p>Sales Team</p>

                            </body>

                            </html>

                        ';

*/

                     $msg = $this->sendMailFormat($subject, $msginner);  



                $strdata="";

                foreach($userData as $key=>$value)

                {

                   $strdata .= "<br>". $key . " : ". $value;

                }



                 /* $msgadmin = '

                            <html>

                            <head>

                            <title>Personal User Registered</title>

                            </head>

                            <body>

                            <h3>'.$subjectadmin.'</h3>

                            <p>A Personal User Get Registered from website</p><br>Details Are:<br>

                '.

                $strdata



                .'

                            <p>With Regards</p>

                            <p>Sales Team</p>

                            </body>

                            </html>

                            ';*/



                $msginner = "<p>A Personal User Get Registered from website</p><br>Details Are:<br>".$strdata;



                 $msgadmin = $this->sendMailFormat($subjectadmin, $msginner);               

                $fromemail = "votive.pradeep01@gmail.com";

                $fromname = "Sales Team";

               if($this->form_validation->run() == true){

                $recaptcha = $this->input->post('g-recaptcha-response');

                $insert = $this->Admin_model->insert($userData);

                if($insert > 0){

                    $this->session->set_userdata('success_msg', 'Your registration was successfully. Please login to your account.');

                    $this->session->set_userdata('reg_success', 'yes');

                     $this->send_mail($email, $subject, $msg);

                     $this->send_mail($emailadmin, $subjectadmin, $msgadmin);

                    redirect(base_url());

                }else{

                    $data['error_msg'] = 'Some problems occured, please try again.';

                }

           

                }

            }

                

            $data['user'] = $userData;

         /*   $data['business_categories'] = $this->User->getBusinessCategories();

            $data['newsletter1'] = $this->User->site_contents(9);

            $data['newsletter2'] = $this->User->site_contents(10);

            */



            

              $data['business_categories'] = $this->Admin_model->getBusinessCategories();        



            $data['newsletter1'] = $this->Admin_model->site_contents(9);

            $data['newsletter2'] = $this->Admin_model->site_contents(10);



            header("Access-Control-Allow-Origin: *");

            

            $this->template->set('title', 'Professional Account Creation');

            $this->template->load('admin_layout', 'contents' , 'create_business', $data);

    

  }



public function edituser()

  {



    $data = array();

    $data['error_msg']=""; 

    $userData = array();

$id = "";

 if($this->input->post('updateSubmit'))

 {



  $userData = null;

  $userlang = "";



            $userData = array(

                'user_firstname' => strip_tags($this->input->post('user_firstname')),

                'user_lastname' => strip_tags($this->input->post('user_lastname')),

                'user_phone' => strip_tags($this->input->post('user_phone')),

                'user_address' => strip_tags($this->input->post('user_address')),

                'user_gender' => $this->input->post('user_gender'),

                'user_phone' => strip_tags($this->input->post('user_phone')),

                  );



 

$id = $this->input->post('user_id');

       //if($this->form_validation->run() == true){

                $update = $this->Admin_model->update($userData, $id);

                if($update >=1){

                    $this->session->set_userdata('success_msg', 'Data Updated.');

                    $this->session->set_userdata('upd_success', 'yes');

                    redirect(base_url()."Admin/user_list");

            }

          //}

        }

      

      $uid = $id = "";

      if($this->uri->segment(3)){

$uid = ($this->uri->segment(3)) ;

}



 if($this->uri->segment(4)){

$action = ($this->uri->segment(4)) ;

$data['act'] = $action;

}

else

{

 $data['act'] = "view"; 

}



 $con['conditions'] = array(

            'user_id'=> $uid

      );





    $data['user'] = $this->Admin_model->getRows($con);

    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'Professional Account Creation'); 

    if($data['act'] == "view")

    {

$this->template->load('admin_layout', 'contents' , 'user_view', $data);

    }

     else{

      $this->template->load('admin_layout', 'contents' , 'user_edit', $data);

     }     

  



  }





  public function edituserpro(){

    $data = array();

    $data['error_msg']=""; 

    $userData = array();

    $id = "";     

    if($this->input->post('updateSubmit')){      

      $userData = null;

      $userlang = "";

      if($this->input->post('user_type')=="Personal")

      {

        $userData = array(

            'user_firstname' => strip_tags($this->input->post('user_firstname')),

            'user_lastname' => strip_tags($this->input->post('user_lastname')),

            'user_phone' => strip_tags($this->input->post('user_phone')),

            'user_address' => strip_tags($this->input->post('user_address')),

            'user_gender' => $this->input->post('user_gender'),

            'user_phone' => strip_tags($this->input->post('user_phone')),

             );

      }else{

        $userlang = implode(",", $this->input->post('user_language'));

        $userData = array(

                    'user_firstname' => strip_tags($this->input->post('user_firstname')),

                    'user_lastname' => strip_tags($this->input->post('user_lastname')),

                    'user_phone' => strip_tags($this->input->post('user_phone')),

                    'user_address' => strip_tags($this->input->post('user_address')),

                    'user_gender' => $this->input->post('user_gender'),

                    'user_company_name' => strip_tags($this->input->post('user_phone')),

                    'user_company_number' => strip_tags($this->input->post('user_company_number')),

                    'user_language' => $userlang

                      );

      }           

      $id = $this->input->post('user_id');

      $update = $this->Admin_model->update($userData, $id);

      if($update >=1){                  

        $this->session->set_userdata('success_msg', 'Data Updated.');

        $this->session->set_userdata('upd_success', 'yes');

        redirect(base_url()."Admin/user_list_pro");

      }

    }        

    $uid = $id = "";

    if($this->uri->segment(3)){

      $uid = ($this->uri->segment(3)) ;

    }

    if($this->uri->segment(4)){

      $action = ($this->uri->segment(4)) ;

      $data['act'] = $action;

    }else

    {

      $data['act'] = "view"; 

    }

    $con['conditions'] = array('user_id'=> $uid);

    $data['user'] = $this->Admin_model->getRows($con);

    header("Access-Control-Allow-Origin: *");    

    if($data['act'] == "view")

    {

      $data['availableDoc'] = $this->Admin_model->getDocumentsList();

      $data['uploaddocumentList'] = $this->Admin_model->getPurchaseDocumentRows(array("conditions"=>array("dc_userid"=>$uid)));

      $data['title'] = "Admin- View Profile";      

      $this->template->load('admin_layout', 'contents' , 'user_view', $data);

    }else{

      $data['title'] = "Admin- Edit Profile";

      $this->template->load('admin_layout', 'contents' , 'user_edit_pro', $data);     

    }

  }







  public function deleteuser(){  

    $data = array();

    $data['error_msg']=""; 

    $userData = array();

    $id = "";

    if($this->uri->segment(3)){

      $id = ($this->uri->segment(3)) ;

    }



    if($this->uri->segment(4)){

      $action = ($this->uri->segment(4)) ;

      $data['act'] = $action;

    }

    else

    {

      $data['act'] = "view"; 

    }



   



    $con['conditions'] = array(

            'user_id'=> $id

      );



    $userData = array(

                'status' => '3'

                );

    // $rec = $this->Admin_model->update($userData, $id);

    $rec = $this->Admin_model->deletePersonalUserFromSite($id);



    if($rec > 0)

    {

      $data['deleted'] = "yes";

    }





    $con['conditions'] = array( );

    $data['usertype'] = "";

    $data['page'] = "1";

  

    $data['userlist'] = $this->Admin_model->getRows($con);

    header("Access-Control-Allow-Origin: *");                    

    $this->template->set('title', 'Professional Account Creation');

    $this->template->load('admin_layout', 'contents' , 'users', $data);

    redirect(base_url()."Admin/user_list");

  }





public function settopratedonhome()

  {

  

  $data = array();

  $data['error_msg']=""; 

  $userData = array();

  $action="";

  $id = "";



if($this->uri->segment(3)){

$id = ($this->uri->segment(3)) ;

}

else

{

  $id = $this->input->post("user_id");

}



if($this->uri->segment(4)){

$action = ($this->uri->segment(4)) ;

}

else

{

   $action = $this->input->post("action"); 

}



  $con['conditions'] = array('user_id'=> $id);

  $userData = array('toprated' => $action);

  $rec = $this->Admin_model->update($userData, $id);

  if($rec > 0 && $action==1)

  {

    $data['toprated'] = "yes";

    $data['success_msg'] = 'Set Top Rated On Home';

  }

  else

  {

  $data['toprated'] = "no";

   $data['error_msg'] = 'Unset Top Rated From Home';

  }





}







public function setpublishonhome()

  {

  

  $data = array();

  $data['error_msg']=""; 

  $userData = array();

  $action="";

  $id = "";



if($this->uri->segment(3)){

$id = ($this->uri->segment(3)) ;

}

else

{

  $id = $this->input->post("user_id");

}



if($this->uri->segment(4)){

$action = ($this->uri->segment(4)) ;

}

else

{

   $action = $this->input->post("action"); 

}



  $con['conditions'] = array('user_id'=> $id);

  $userData = array('publishonhome' => $action);

  $rec = $this->Admin_model->update($userData, $id);

  if($rec > 0 && $action==1)

  {

    $data['published'] = "yes";

    $data['success_msg'] = 'Published On Home';

  }

  else

  {

  $data['published'] = "no";

   $data['error_msg'] = 'Unpublished From Home';

  }





}







  public function activateuser(){

  

      $data = array();

      $data['error_msg']=""; 

      $userData = array();

      $action="";

      $id = "";



      if($this->uri->segment(3)){

        $id = ($this->uri->segment(3)) ;

      }

      else

      {

        $id = $this->input->post("user_id");

      }



      if($this->uri->segment(4)){

        $action = ($this->uri->segment(4)) ;

      }

      else

      {

         $action = $this->input->post("action"); 

      }



      //echo $action;

      $con['conditions'] = array('user_id'=> $id);

      $userData = array('status' => $action);

      $rec = $this->Admin_model->update($userData, $id);



      if($rec > 0 && $action==1)

      {

        $data['activated'] = "yes";



        if($rec >=1){



          $urec = $this->Admin_model->getRows(array("user_id"=>$id));

          $email = $urec['user_email'];



          // //Fetch Email Templates

          // $templateheader = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header

          // $templatecenter = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>29));

          // $templatefooter = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer



          // $subject = $templatecenter['tmplate_subject'];

          // $dummy = array("%%Firstname%%",

          //                "%%LastName%%", 

          //                "%%Email%%", 

          //                "%%BusinessName%%", 

          //                "%%CompanyNumber%%", 

          //                "%%Address%%", 

          //                "%%Phone%%", 

          //                "%%Gender%%");

          // $real = array($urec['user_firstname'], 

          //               $urec['user_lastname'],

          //               $urec["user_email"],

          //               $urec["user_company_name"],

          //               $urec['user_company_number'],

          //               $urec["user_company_address"],

          //               $urec['user_phone'],

          //               $urec['user_gender']);

          // $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );

          // $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];

          // $x= $this->send_mail($urec["user_email"], $subject, $msg);        

          // $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);

          $data['success_msg'] = 'Business User Activated';                       

        }else{

          $data['error_msg'] = 'Wrong email, please try again.';

        }      

      }else if($rec > 0 && $action==0){

        $data['activated'] = "no";

      }

  }





  

// public function activateoption()

// {



//   $data = array();

//   $data['error_msg']=""; 

//   $userData = array();

//   $action="";

//   $id = "";



//   if($this->uri->segment(3)){

//   $id = ($this->uri->segment(3)) ;

//   }

//   else

//   {

//   $id = $this->input->post("opt_user_option_id");

//   }



//   if($this->uri->segment(4)){

//   $action = ($this->uri->segment(4)) ;

//   }

//   else

//   {

//    $action = $this->input->post("action"); 

//   }





//   $con['conditions'] = array(

//             'opt_user_option_id'=> $id

//       );



//   $userData = array('opt_option_status' => $action);

//   $rec = $this->Admin_model->update($userData, $id, "otd_user_option");



//   if($rec > 0 && $action==1)

//   {

//   $data['activated'] = "yes";



//   if($rec >=1)

//   {

//      $data['success_msg'] = 'User Option Activated';

                   

//   }else{

//                     $data['error_msg'] = 'Wrong email, please try again.';

//                 }



//   }





//   else if($rec > 0 && $action==0)

//   {

//   $data['activated'] = "no";

//   }







// }



  public function activateoption()

  {

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      $id = $this->input->post("option");

      $action = $this->input->post("action");

      $userData = array();

      $optiondetails = $this->Admin_model->getUserOptionRows(array("opt_user_option_id"=>$id));

      if(empty($optiondetails['opt_option_active_date'] && $optiondetails['opt_option_end_date'])){

        $activedate = date("Y-m-d");

        $enddate = date("Y-m-d", strtotime($activedate." + ".$optiondetails['otp_option_duration']));

        $userData['opt_option_active_date'] = $activedate;

        $userData['opt_option_end_date'] = $enddate;

      }

      $userData['opt_option_status'] = $action;

      $rec = $this->Admin_model->update($userData, $id, "otd_user_option");

      if($rec){        

        // if($optiondetails['opt_option_status'])

        if($action == 1){

          $emailMain = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>23));

        }else{

          $emailMain = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>24));

        }

        $emailHeader = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>26));

        $emailFooter = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>25));

        $subject = $emailMain['tmplate_subject'];

        $dummy = array("%%Firstname%%",

                       "%%LastName%%", 

                       "%%Email%%", 

                       "%%BusinessName%%", 

                       "%%CompanyNumber%%", 

                       "%%Address%%", 

                       "%%Phone%%", 

                       "%%Gender%%", 

                       "%%OptionName%%", 

                       "%%OptionsDescription%%", 

                       "%%PurchaseDate%%", 

                       "%%OptionDuration%%", 

                       "%%OptionPrice%%", 

                       "%%OptionsVisibleCity%%", 

                       "%%OptionActiveStartDate%%",

                       "%%OptionActiveEndDate%%" );

        $real = array($optiondetails['user_firstname'], 

                      $optiondetails['user_lastname'],

                      $optiondetails["user_email"],

                      $optiondetails["user_company_name"],

                      $optiondetails['user_company_number'],

                      $optiondetails["user_company_address"],

                      $optiondetails['user_phone'],

                      $optiondetails['user_gender'],

                      $optiondetails['opt_name'],

                      $optiondetails['opt_description'],

                      $optiondetails['opt_option_purchase_date'],

                      $optiondetails['otp_option_duration'],

                      $optiondetails['opt_option_price'],

                      $optiondetails['otp_search_city'],

                      $optiondetails['opt_option_active_date'],

                      $optiondetails['opt_option_end_date']

                    );

        $msg = str_replace($dummy, $real, $emailMain['tmplate_content'] );



        $msg = $emailHeader['tmplate_content']." ".$msg." ".$emailFooter['tmplate_content'];

        $x= $this->send_mail($optiondetails["user_email"], $subject, $msg);        

        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);

        echo json_encode(array("status"=>200, "msg"=>"Updated successfully."));



      }else{

        echo json_encode(array("status"=>1, "msg"=>"Something went wrong, Please try after sometime."));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }





  public function activateformoption()

  {

    $data = array();

    $data['error_msg']=""; 

    $optionData = array();

    $act ="";

    $id = "";

    if($this->uri->segment(3)){

    $id = ($this->uri->segment(3)) ;

    }

    

    if($this->uri->segment(4)){

      $act = ($this->uri->segment(4)) ;

      }

        



    $con['conditions'] = array(

              'opt_id'=> $id

        );

    

        if($act == 1)

        {

                $optionData = array(

                'opt_status' => '2'

                  );

        //$data["msg"] = "You Just Deactive Package" ;

        $this->session->set_flashdata("success", "You Just Deactive Package.");

      }

                else {

                  

                    $optionData = array(

                                  'opt_status' => '1'

                                  );



                   //$data["msg"] = "You Just Active Package" ;

                   $this->session->set_flashdata("success", "You Just Active Package.");

                                

                }



     $rec = $this->Admin_model->updateOption($optionData, $con['conditions']);

    

     

    if($rec > 0)

    {

    $data['activated'] = "yes";

    }

    

    redirect(base_url()."Admin/option_list");

    

    // $option_data = $this->Admin_model->getOptionDetails();

    // $data['option_list'] = $option_data;

    // header("Access-Control-Allow-Origin: *");

    // $this->template->set('title', 'User List');

    // $this->template->load('admin_layout', 'contents' , 'option_price', $data);

    

  }



  // public function deletesubscriber()

  // {

  

  //   $data = array();

  //   $data['error_msg']=""; 

  //   $userData = array();

  //   $id = "";



  //    /* if($this->uri->segment(3)){

  //   $id = ($this->uri->segment(3)) ;

  //   }

  //   */



  //   $id = $this->input->post('subs_id');



  //   $data['act'] = "delete";

 



  //    $con['conditions'] = array(

  //               'subs_id'=> $id

  //         );



  //   $userData = array(

  //                   'status' => '3'

  //                   );

  //   $rec = $this->Admin_model->update($userData, $id, "subscribers");



  //   if($rec > 0)

  //   {

  //   return $rec;  //$data['deleted'] = "yes";

  //   }

  //   else

  //   {

  //     return 0;

  //   }

  //    /*$con['conditions'] = array( );

  //    $data["page"] = 1;

  //    $data['subslist'] = $this->Admin_model->getSubsRows($con);

  //    header("Access-Control-Allow-Origin: *");

                        

  //            $this->template->set('title', 'Professional Account Creation'); 

  //            $this->template->load('admin_layout', 'contents' , 'subscribers', $data);



  //   */

  // }





public function import_csv()

{

$data = array();

$uploaded_file = "";

$csvs = array();

$result= "";

$cat_count = 0;



if($this->input->post('ImportSubmit'))

{

  $config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save

  $config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted

  $config['max_size']    = '20480'; //The max size of the image in kb's

  $config['overwrite'] = TRUE; 



  $this->load->library('upload', $config); //Load the upload CI library

      if (!$this->upload->do_upload('userfile'))

                {

                 $error = $this->upload->display_errors();

                      

                }

                else

                {

                 $file_info = $this->upload->data();

                 $file_name = $file_info['file_name']; 

                // $uploaded_file = "assets/content_csv/";                }

                 }

           $result =  $this->str_import_data(base_url()."assets/content_csv/". $file_name);

            }else

            {

            $data["error"]="File not found";

            }



if($result == 1)

{

  $data['import_done'] = "Categories are uploaded";

}

else

{

  $data['import_done'] = "Categories are NOT uploaded";

}



$con = null;

$con = array();

$con['returnType'] = 'count';

$cat_count  = $this->Admin_model->getCatRows($con);



if($cat_count > 0){

$con = null;

$con = array();

$data["cat_count"] = $cat_count;

$con['returnType'] = '';

$data['catlist'] = $this->Admin_model->getCatRows($con);

}

else

{

 $data['catlist'] = array();

}





$config['base_url'] = base_url()."Admin/import_csv/";

$config['total_rows'] = $cat_count;

$config['per_page'] = 10;

$config['uri_segment'] = 3;

$config['num_links'] = $cat_count / 10;

$config['cur_tag_open'] = '&nbsp;<a class="current">';

$config['cur_tag_close'] = '</a>';

$config['next_link'] = '<b> Next </b>';

$config['prev_link'] = '<b> Previous </b>';

$choice = $config["total_rows"] / $config["per_page"];

$config["num_links"] = round($choice);

$this->pagination->initialize($config);



$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;



$con['limit'] = $config['per_page'];

$start =($page-1) * $config['per_page'];



if($start == 0)

{

  $start = 0;

}

else

{

  $start = ($start/$config['per_page'] )+1;

}



$con['start'] = $start;

 if($cat_count > 0){

      $data['catlist'] = $this->Admin_model->getCatRows($con);

      $data["record_found"] = $cat_count; 

    }

    else

    {

      $data["record_found"] = 0; 

    }



$data["links"] = $this->pagination->create_links();

$data['page'] = $page;





        $data['import_type']="Categories"; 

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'Import CSV File'); 

        $this->template->load('admin_layout', 'contents' , 'import_csv', $data);

 

}





public function str_import_data($upl_file)

{

  

$filename = $upl_file;

$file = fopen($filename, "r");

$x=0;

          while (($getData = fgetcsv($file, 10000, ",")) !== FALSE)

           {



$uid =   $this->session->userdata('user_id');

$uid = $uid != ""?$uid:0;

      if( $getData[1] != "")

      {

      $userData = array('cat_name' => $getData[1],

                'cat_description' => $getData[2],

                'cat_status' => 1,

                'cat_by_user' => $uid,

               );



 $insert = $this->Admin_model->insertCat($userData, "otd_business_category");

                if($insert > 0){

                    $this->session->set_userdata('success_msg', 'Your File has been uploaded.');

                    $this->session->set_userdata('reg_success', 'yes');

                  $str = 1;

                  //  redirect(base_url(). "/Admin");

                }else{

                    $data['error_msg'] = 'Some problems occured, please try again.';

                     $str = -1;

                }

              }



           } 

          

      

           fclose($file); 

   //  }

  }  







public function resetpassmail()

  {

  

  $data = array();

  $data['error_msg']=""; 

  $userData = array();

  $action="";

  $id = "";



if($this->uri->segment(3)){

$id = ($this->uri->segment(3)) ;

}

else

{

$id = $this->input->post('user_id');  

}

$urec = $this->Admin_model->getUserEmail($id);

$email = $urec['user_email'].", Lloyd@otourdemoi.fr, votive.pradeep01@gmail.com";



            $subject = "Change Your Password from Admin". $id;

            $msginner = "Hello ". $urec['user_firstname']." Administrator has sent you the link to change your password. Please use below link to change your password. <br><br>

            <a href='".base_url()."'>Change Password</a><br><br>";



            /*$msg = '

            <html>

            <head>

            <title>Change Your Password</title>

            </head>

            <body>

            <h3>'.$subject.'</h3>

            <p>'.$msginner.'</p><br><br>

            <p>With Regards</p>

            <p>Support Team</p>

            </body>

            </html>

            ';*/



        $msg = $this->sendMailFormat($subject, $msginner);  



        $fromemail = "votive.pradeep01@gmail.com";

        $fromname = "Support Team";



     $x= $this->send_mail($email, $subject, $msg);

//echo " x =".$x;

if($x)

{

 echo 'Change Password Email Sent';

 }else{

      

  echo 'Wrong email, please try again.';

  }

 



  }







  function userEmails(){

     $q = $this->input->get("query");

     $query = $this->db->like("user_email", $q)->group_by("user_email")->get_where("users", array("status !="=>3, "user_type"=>"Professional"));

     $arr = array();

     foreach($query->result() as $em){

       $arr[] = array("name"=>$em->user_email);

     }    

     echo json_encode($arr);

  }



  function userPerEmails(){

    $q = $this->input->get("query");

    $query = $this->db->like("user_email", $q)->group_by("user_email")->get_where("users", array("status !="=>3, "user_type"=>"Personal"));

    $arr = array();

    foreach($query->result() as $em){

      $arr[] = array("name"=>$em->user_email);

    }    

    echo json_encode($arr);

  }



   function subscribeUserEmails(){

     $q = $this->input->get("query");

     $query = $this->db->like("user_email", $q)->group_by("user_email")->get_where("subscribers", array("status !="=>3));

     $arr = array();

     foreach($query->result() as $em){

       $arr[] = array("name"=>$em->user_email);

     }

     echo json_encode($arr);

   }



 

  function event_list()

  {

    if(!empty($this->input->post())){

      $useremail = $this->input->post("user_email");

      $eventtype = $this->input->post("eventtype");

      $publishstart = $this->input->post("stpublish");

      $eventstart = $this->input->post("stevent");

      $this->session->set_userdata("user_email", $useremail);

      $this->session->set_userdata("eventtype", $eventtype);

      $this->session->set_userdata("publishstart", $publishstart);

      $this->session->set_userdata("eventstart", $eventstart);

      redirect(base_url("Admin/event_list"));

    }



      $data = array();

      $order_by = "created_on";

      $order="";

      $type ="";

      $page = 0;



      $con['sorting'] = array('order'=>$order_by);



      $totalrows = $this->Admin_model->record_count($con, "otd_events");

      $config['base_url'] = base_url()."Admin/event_list/";

      $config['total_rows'] = $totalrows;

      $config['per_page'] = 10;

      $config['uri_segment'] = 3;

      $config['num_links'] = $totalrows / 10;

      $config['cur_tag_open'] = '&nbsp;<a class="current">';

      $config['cur_tag_close'] = '</a>';

      $config['next_link'] = '<b> Next </b>';

      $config['prev_link'] = '<b> Previous </b>';

      $choice = $config["total_rows"] / $config["per_page"];

      $config["num_links"] = round($choice);

      $this->pagination->initialize($config);

      $page = ($this->uri->segment(3));



      $con['limit'] = $config['per_page'];

      $start =($page-1) * $config['per_page'];



      if($start == 0)

      {

        $start = 0;

      }

      else

      {

        $start = ($start/$config['per_page'] )+1;

      }

      $con['start'] = $start;



      if($this->session->userdata('isUserLoggedIn'))

      {

        if($totalrows > 0){

          $data['eventlist'] = $this->Admin_model->getEventRows($con);

          $data["record_found"] = $totalrows; 

        }

        else

        {

          $data['eventlist'] = array();

          $data["record_found"] = 0; 

        }

          $data["links"] = $this->pagination->create_links();

          $data['order'] = $order;

          $data['page'] = $page;



        //print_r($data);

          $data['eventTypeList'] = $this->Admin_model->getEventTypeRows(array("conditions"=>array("evt_status"=>1)));

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'Events List');

          $this->template->load('admin_layout', 'contents' , 'event_listing', $data);

     }

     else

     {

          redirect(base_url().'Admin/login');

     }



  }



  function clearEventFilter(){

    $this->session->unset_userdata("user_email");

    $this->session->unset_userdata("eventtype");

    $this->session->unset_userdata("publishstart");

    $this->session->unset_userdata("eventstart");

    redirect(base_url("Admin/event_list"));

  }



  function ViewEvent($args){



    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      if(!empty($args)){

        $data = array();

        $data['details'] = $this->Admin_model->getEventRows(array("conditions"=>array("event_id"=>$args), "returnType"=>"single"));

        if(!empty($data['details'])){

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'Event Details');

          $this->template->load('admin_layout', 'contents' , 'viewevent', $data);

        }

      }

    }

  }



  function eventTypeList(){

    $data = array();

    $data['details'] = $this->Admin_model->getEventTypeRows(array("sorting"=>array("evt_type"=>"ASC")));

    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'User List');

    $this->template->load('admin_layout', 'contents' , 'eventTypelisting', $data);

  }



  function eventTypeview($args){

    $ses = $this->session->userdata("user_id");    

    if(!empty($ses)){

      $this->form_validation->set_rules("name", "Type Name", "required");

      $this->form_validation->set_rules("status", "Type Status", "required");

      if($this->form_validation->run() == FALSE){

        $data = array();

        $data['details'] = $this->Admin_model->getEventTypeRows(array("evt_type_id"=>$args));

        if($data['details']){

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'User List');

          $this->template->load('admin_layout', 'contents' , 'eventTypeedit', $data);

        }

      }else{

        $check = $this->Admin_model->update(array("evt_type"=>$this->input->post("name"), "evt_status"=>$this->input->post("status")), $args, "otd_event_type");

        if($check){

          echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Details updated successfully.</label>"));

        }else{

          echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Details not updated, Please try again.</label>"));

        }

      }

    }

  }



  function addEventType(){

    $ses = $this->session->userdata("user_id");    

    if(!empty($ses)){

      $this->form_validation->set_rules("name", "Type Name", "required");

      $this->form_validation->set_rules("status", "Type Status", "required");

      if($this->form_validation->run()  == FALSE){

        $data = array();

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'User List');

        $this->template->load('admin_layout', 'contents' , 'addeventType', $data);        

      }else{

        $check = $this->Admin_model->insertEventType(array("evt_type"=>$this->input->post("name"), "evt_status"=>$this->input->post("status")));

        if($check){

          echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Details added successfully.</label>"));

        }else{

          echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Details not added, Please try again.</label>"));

        }

      }

    }

  }



  function deleteEventType(){

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      $id = $this->input->post("id");

      if(!empty($id)){

        $this->Admin_model->deleteeventType($id);

        echo json_encode(array("status"=>200));

      }else{

        echo json_encode(array("status"=>1, "msg"=>"Required parameter missing"));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }    

  }





  public function reviewListing(){

    $con= array();

    $reviews =  $this->Admin_model->getReviewRows(array("conditions"=>array("vote_status!="=>3)));

    $data = array();

    $order_by = "";

    $order="";

    $page = 0;

    $con['sorting'] = array('review_date'=>'DESC');

    $con['returnType'] = "count";

    $con['conditions'] = array("vote_status!="=>3);

    $totalrows = $this->Admin_model->getReviewRows($con);;

    $config['base_url'] = base_url()."Admin/reviewListing";

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 3;

    $config['num_links'] = $totalrows / 10;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    $choice = $config["total_rows"] / $config["per_page"];

    $config["num_links"] = round($choice);

    $this->pagination->initialize($config);

    $page = ($this->uri->segment(3));

    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];

    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }

    $con['start'] = $start;

    $con['returnType'] = "";

    $con['conditions'] = array("vote_status!="=>3);

    if($this->session->userdata('isUserLoggedIn'))

    {

        if($totalrows > 0){



          $data['reviewlist'] = $this->Admin_model->getReviewRows($con);

          $data["record_found"] = $totalrows; 

        }

        else

        {

          $data['reviewlist'] = array();

          $data["record_found"] = 0; 

        }

          $data["links"] = $this->pagination->create_links();

          $data['order'] = $order;

          $data['page'] = $page;



          //print_r($data);



          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'Review List');

          $this->template->load('admin_layout', 'contents' , 'Review/review_listing', $data);

    }

    else

    {

      redirect(base_url().'Admin/login');

    }

  }



  function updateReviewStatus(){

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      $check = $this->Admin_model->getReviewRows(array("vote_id"=>$this->input->post("id")));

      if(!empty($check)){

        $this->Admin_model->update(array("vote_status"=>$this->input->post("mode"), "status"=>$this->input->post("mode")), $this->input->post("id"), "profile_review");

        echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Review updated successfully.</label>"));

      }else{

        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Review not updated, Please refresh page and try again.</label>"));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Invalid Access.</label>"));

    }

  }



  public function delreview()

  {

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      $check = $this->Admin_model->getReviewRows(array("vote_id"=>$this->input->post("review")));

      if(!empty($check)){

        $this->Admin_model->update(array("vote_status"=>3, "status"=>3), $this->input->post("review"), "profile_review");

        echo json_encode(array("status"=>200));

      }else{

        echo json_encode(array("status"=>1));

      }

    }else{

      echo json_encode(array("status"=>1));

    }

  }



  public function reportReviewListing()

  {

    $con= array();

    $reviews =  $this->Admin_model->getReportedReviewRows();

    $data = array();

    $order_by = "";

    $order="";

    $page = 0;

    $con['sorting'] = array('order'=>$order_by);

    $con['returnType'] = "count";

    $totalrows = $this->Admin_model->getReportedReviewRows($con);;

    $config['base_url'] = base_url()."Admin/review_listing";

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 3;

    $config['num_links'] = $totalrows / 10;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    $choice = $config["total_rows"] / $config["per_page"];

    $config["num_links"] = round($choice);

    $this->pagination->initialize($config);

    $page = ($this->uri->segment(3));

    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];

    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }

    $con['start'] = $start;

    $con['returnType'] = "";

    if($this->session->userdata('isUserLoggedIn'))

    {

        if($totalrows > 0){

          $data['reviewlist'] = $this->Admin_model->getReportedReviewRows($con);

          $data["record_found"] = $totalrows; 

        }

        else

        {

          $data['reviewlist'] = array();

          $data["record_found"] = 0; 

        }

          $data["links"] = $this->pagination->create_links();

          $data['order'] = $order;

          $data['page'] = $page;



          //print_r($data);



          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'Reported Review List');

          $this->template->load('admin_layout', 'contents' , 'Review/reportedreviewlisting', $data);

    }

    else

    {

      redirect(base_url().'Admin/login');

    }

  }



  public function viewreview($args){

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      if(!empty($args)){

        $details = $this->Admin_model->getReviewRows(array("vote_id"=>$args));

        if(!empty($details)){

          $data['details'] = $details;

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'User List');

          $this->template->load('admin_layout', 'contents' , 'Review/viewreview', $data);

        }else{

          redirect(base_url("Admin/reviewListing"));

        }

      }else{

        redirect(base_url("Admin"));

      }

    }else{

      redirect(base_url("Admin"));

    }

  }



  public function reviewListingDetail()

 {







  



    $data = array();





    if($this->session->userdata('isUserLoggedIn'))

    {

        $data['reviewsdetails'] =  $this->Admin_model->getReportedReviewDetails(4);



  //print_r($data);



          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'User List');

          $this->template->load('admin_layout', 'contents' , 'reported_review_listing', $data);

     }

     else

     {

          redirect(base_url().'Admin/login');

     }







  }







  public function sendMailFormat($subject, $msginner)

     {

 $format = '<html><head><meta charset="utf-8"><title>'.$subject.'</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"></head>

<body style="font-family: open Sans;font-size: 13px; line-height:20px;"><div style="padding: 0 10px;"><div style="max-width:550px;width:85%;padding:30px;margin:30px auto;border:1px solid #e2e2e2; overflow:hidden; background-position:center;background-size:cover;text-align: center;color: #fff;"><div style="padding:30px;border:2px solid #fff;background:rgba(0, 0, 0, 0.63);">

                <div style="text-align:center">';

        

    $format .='<a href="http://votivephp.in/VotiveYellowPages/" style="display:block;overflow:hidden;padding: 0 0 0 0;"><img style="max-width:270px;width: 100%;" src="http://votivephp.in/VotiveYellowPages/assets/img/logo.png"></a></div><div style="border-top: 1px dotted #fff; display: block; margin-top:5px; margin-bottom:5px;"></div><div class="mailbody" style="min-height: 250px;"><h3>'.$subject.'</h3><div>'.$msginner.'</div></div>';



   $format .= '<div><div style="border-top: 1px dotted #fff; display: block;margin-top:5px; margin-bottom:5px;"></div><p style="margin:0 0 8px 0">Regards,</p><p style="margin:0 0 8px 0">Sales Team</p><p style="margin:0 0 8px 0"><a href="http://votivephp.in/VotiveYellowPages/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">http://votivephp.in/VotiveYellowPages/</a></p></div></div></div></div></body></html>';



return $format;

     }



       //$msgadmin = sendMailFormat($subjectadmin, $msginner);  

        //$msg = sendMailFormat($subject, $msginner);



    /*

    * Create event from dashboard

    */

    public function createEvent(){

      $ses = $this->session->userdata("user_id");

      if(!empty($ses)){

        $arr = array("event_name"=>$this->input->post("name"),

                     "event_description"=> $this->input->post("evtdescription"),

                     "event_date"=> $this->input->post("eventdate"),

                     "event_start_time"=> $this->input->post("starttime"),

                     "event_end_time"=> $this->input->post("endtime"),

                     "created_by_user"=> $ses,

                     "event_status"=>$this->input->post("status")

                    );

        $check = $this->Admin_model->insertEvent(1, $arr);

        if($check){

          echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Event Created successfully.</label>"));

        }else{

          echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Event not created, Please try again.</label>"));

        }

      }else{

        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Invalid Access</label>"));

      }

    }





    public function editEvent($args)

    {

      $userid = $this->session->userdata("user_id");



      if(!empty($userid)){





          $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

          $this->form_validation->set_rules("ename", "event name", "required");

          $this->form_validation->set_rules("eventtype", "event type", "required");

          $this->form_validation->set_rules("pesdate", "Publishing Start Date", "required");

          $this->form_validation->set_rules("peedate", "Publishing End Date", "required");

          $this->form_validation->set_rules("esdate", "Start Date", "required");

          $this->form_validation->set_rules("eedate", "End Date", "required");

          $this->form_validation->set_rules("noperson", "Number Of Persons", "required");

          $this->form_validation->set_rules("eventprice", "Price Per Person", "required");

          $this->form_validation->set_rules("evtaddress", "Address", "required");

          $this->form_validation->set_rules("description", "Description", "required");

          $this->form_validation->set_rules("status", "status", "required");

          if($this->form_validation->run() == TRUE){

            $eventid = $this->input->post("event_id");

            

            //  $eventDetails = $this->User->getMyFreeEvents();

              $latlong = $this->Admin_model->get_lat_long($this->input->post("evtaddress"));

              $arr = array("event_name"=>$this->input->post("ename"),

                           "event_type_id"=>$this->input->post("eventtype"),

                           "event_publish_start"=>$this->input->post("pesdate"),

                           "event_publish_end"=>$this->input->post("peedate"),

                           "event_start_date"=>$this->input->post("esdate"),

                           "event_end_date"=>$this->input->post("eedate"),

                           "event_per_ltd"=>$this->input->post("noperson"),

                           "event_price"=>$this->input->post("eventprice"),

                           "event_address"=>$this->input->post("evtaddress"),

                           "event_lat"=>$latlong['lat'],

                           "event_long"=>$latlong['lng'],

                           "event_description"=>$this->input->post("description"),

                           "event_status"=>$this->input->post("status"),                             

                          );

			  	  $single_event = $this->db->get_where("otd_events",array("event_id"=>$eventid))->row();

                				  

                 if(!empty($_FILES['evtfile']['name'][0] !='')){

                  $cnt = 1;

				  $filecount = count($_FILES['evtfile']['name']);

 

			    

                  foreach($_FILES['evtfile']['name'] as $key=>$val){

	                

                      //upload and stored images

                      $random = $this->generateRandomString(30);

                      $ext = pathinfo($_FILES['evtfile']['name'][$key], PATHINFO_EXTENSION);

                      $file_name = $random.".".$ext;

                      $dirpath = './././assets/img/events/'.$file_name;

					   

                      if($filecount >=4){

						   $insid = 0;

						  if(move_uploaded_file($_FILES['evtfile']['tmp_name'][$key],$dirpath)){

							  $arr["event_img".$cnt] = $file_name;

						  }						  

					  }elseif($filecount==3){

						   

						  

						  if($cnt==1){

							  $insid = 0;

						      if($single_event->event_img4 != ''){

								  

								  $arr["event_img".$cnt] =  $single_event->event_img4;

								  $insid = 1;

							  }else{

								  $arr["event_img4"] =  $single_event->event_img1;

								  

							  }



						  }

						if(move_uploaded_file($_FILES['evtfile']['tmp_name'][$key],$dirpath)){

							  $main = $cnt+$insid;

						      $arr["event_img".$main] =  $file_name;

						 

						}

	 						

					  }elseif($filecount==2){

						   

						     if($cnt==1){

								 $insid = 0;

								  if($single_event->event_img4 != ''){

									  

									  $arr["event_img1"] =  $single_event->event_img4;

									  $insid = 1;

								  }else{

									  $arr["event_img4"] =  $single_event->event_img1;

									  

								  }

								  if($single_event->event_img3 != ''){

									  $main =$cnt+1;

									  $arr["event_img2"] =  $single_event->event_img3;

									  $insid = 2;

								  }else{

									  $arr["event_img3"] =  $single_event->event_img2;

									  

								  }								  



							 }

	 

							if(move_uploaded_file($_FILES['evtfile']['tmp_name'][$key],$dirpath)){

								  $main = $cnt+$insid;

								  $arr["event_img".$main] =  $file_name;

							 

							}							 

								 

 

					  }elseif($filecount==1){

						  $insid = 0;

						  if($single_event->event_img4 != ''){

							  

							  $arr["event_img1"] =  $single_event->event_img4;

							  $insid = 1;

						  }else{

							  $arr["event_img4"] =  $single_event->event_img1;

							  

						  }

						  if($single_event->event_img3 != ''){

							  

							  $arr["event_img2"] =  $single_event->event_img3;

							  $insid = 2;

						  }else{

							  

							  $arr["event_img3"] =  $single_event->event_img2;

						  }	

						  if($single_event->event_img2 != ''){

							  

							   $arr["event_img3"] =  $single_event->event_img2;

							  $insid = 3;

						  }else{

							  $arr["event_img2"] =  $single_event->event_img1;

						  }

							if(move_uploaded_file($_FILES['evtfile']['tmp_name'][$key],$dirpath)){

								  $main = $cnt+$insid;

								  $arr["event_img".$main] =  $file_name;

							 

							}						  		  

					 

					  }



                      if($cnt+$insid == 4)

                          break;

                      $cnt++;

                  }

				  

				  

              }

		 

			  

              $arr["event_publish_start"] = $this->input->post("pesdate");

              $arr["event_publish_end"] = $this->input->post("peedate");

 

              $opt_arr['opt_option_active_date'] = $this->input->post("pesdate");

              $opt_arr['opt_option_end_date'] = $this->input->post("peedate");

 

              $check = $this->Admin_model->updateUserEvent($arr, array("event_id"=> $eventid ));

             

               if($check){

               

                  // $this->Admin_model->updateUserOption($opt_arr, array("opt_event_id"=>$eventid ));

                 // echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Event updated successfully.</label>"));

                  $this->session->set_flashdata("success", "Event details updated successfully.");

                  redirect(base_url()."Admin/event_list");

                }else{

                 // echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, event is not updated, Please try again.</label>"));

              }

              redirect(base_url()."Admin/event_list");



          }else{

              $data = array();  

                     

              $data['suggestion'] = $this->Admin_model->getSuggestions();

              $data['eventTypeList'] = $this->Admin_model->getEventTypeList();

              $data['details'] = $this->Admin_model->getEventRows(array("conditions"=>array("event_id"=>$args), "returnType"=>"single"));

              // echo "<pre>";

              // print_r($data['details']);

              // exit;

              header("Access-Control-Allow-Origin: *");                     

              $this->template->load('admin_layout', 'contents' , 'edit_event', $data);

          }

      }else{

          redirect(base_url());

      }

    }

    public function delete_event_img(){

		 $event_id  =  $_POST['event_id'];

		 $event_col =  $_POST['event_col'];

		 $single_event = $this->db->get_where("otd_events",array("event_id"=>$event_id))->row();

		 $data = array();

		 if($event_col=='event_img1'){

			 $data['event_img1'] = $single_event->event_img2;

			 $data['event_img2'] = $single_event->event_img3;

			 $data['event_img3'] = $single_event->event_img4;

			 $data['event_img4'] = '';

			 unlink('./././assets/img/events/'. $single_event->event_img1);

			 

		 }

		 if($event_col=='event_img2'){

			 $data['event_img2'] = $single_event->event_img3;

			 $data['event_img3'] = $single_event->event_img4;

			 $data['event_img4'] = '';

			 unlink('./././assets/img/events/'. $single_event->event_img2);

		 }

		 if($event_col=='event_img3'){

			 $data['event_img3'] = $single_event->event_img4;

			 $data['event_img4'] = '';

			 unlink('./././assets/img/events/'. $single_event->event_img3);

			 

		 }

		 if($event_col=='event_img4'){

			 $data['event_img4'] = '';

			 unlink('./././assets/img/events/'. $single_event->event_img4);

			 

		 }

		 

		 $this->db->where("event_id",$event_id);

         $this->db->update("otd_events",$data); 

         $array['status'] = 200;

         echo json_encode($array);		 

		 //echo $event_col."</br>";

		 //echo $event_col."</br>";

		

	}

    public function RemoveEvent(){

      $id = $this->input->post("id");

      if(!empty($id)){

        $con['conditions'] = array("event_id"=>$id);

        $con['returnType'] = "single";

        $check = $this->Admin_model->getEventRows($con);

        if(count($check) > 0){

          $img1 = $check['event_img1']; 

          if(!empty($img1)){

              if(file_exists("./././assets/img/events/$img1")){

                  unlink("./././assets/img/events/$img1");

              }

          }

          $img2 = $check['event_img2'];

          if(!empty($img2)){

              if(file_exists("./././assets/img/events/$img2")){

                  unlink("./././assets/img/events/$img2");

              }

          }

          $img3 = $check['event_img3'];

          if(!empty($img3)){

              if(file_exists("./././assets/img/events/$img3")){

                  unlink("./././assets/img/events/$img3");

              }

          }

          $img4 = $check['event_img4'];

          if(!empty($img4)){

              if(file_exists("./././assets/img/events/$img4")){

                  unlink("./././assets/img/events/$img4");

              }

          }

          $check1 = $this->Admin_model->removeEvent($id);

          if($check1)

            echo json_encode(array("status"=>200));

        }

        else{

          echo json_encode(array("status"=>1));

        }

      }

      else{

        echo json_encode(array("status"=>1));

      }

    }





    public function createStaticOption($opt_type = 3)

    {

      $data = array();

      

        if($this->session->userdata('isUserLoggedIn'))

          {   

                if($this->input->post('btn_submit'))

                {

                    $data = null;

                    $opt_id = strip_tags($this->input->post('txt_option_id'));

                    $opt_type = strip_tags($this->input->post('txt_option_type'));

                    $data = array(

                                  'opt_name' => strip_tags($this->input->post('txt_option_name')),

                                  'opt_description' => $this->input->post('txt_option_description'),

                                  'opt_status' => 1,

                                  'opt_type' => $opt_type,

      

                                    );

                       

                          $insert = $this->Admin_model->createOption($data);

                          redirect(base_url()."Admin/option_list");

                   

                }

                else {

                                 

                   

                      $data['optiontype3'] =  $this->Admin_model->getOptionTypeDetails($opt_type);

                      $data['optiontype'] = 3;



                      header("Access-Control-Allow-Origin: *");                     

                      $this->template->load('admin_layout', 'contents' , 'createOption', $data);

                }

           }

          else

          {

            redirect(base_url().'Admin/login');

          }



    }



    

  public function editStaticOption()

  {

    $data = array();

    $opt_id = $this->uri->segment(3);

   

    if($this->session->userdata('isUserLoggedIn'))

      {   

            if($this->input->post('btn_submit'))

            {

                $data = null;

                $opt_id = strip_tags($this->input->post('txt_option_id'));

                $opt_type = strip_tags($this->input->post('txt_option_type'));

                $data = array(

                              'opt_name' => strip_tags($this->input->post('txt_option_name')),

                              'opt_description' => $this->input->post('txt_option_description'),

                              'opt_status' => 1,

                              'opt_type' => $opt_type,



                                );

                               

                    if(!empty($opt_id))

                    {  $whr = array("opt_id" => $opt_id);

                      $update = $this->Admin_model->updateOption($data, $whr);

                      redirect(base_url()."Admin/option_list");

                            

                    }

            }

            else {

                  $data['optiontype3'] =  $this->Admin_model->getOptionTypeEditDetails($opt_id);

                  $data["editOption"] = "yes";

                  $data['optiontype'] = 3;

  				$data['prices'] = $this->db->get_where(" otd_option_price",array("opt_type_id"=>$opt_id))->result();

                  header("Access-Control-Allow-Origin: *");                     

                  $this->template->load('admin_layout', 'contents' , 'createOption', $data);

            }

       }

      else

      {

        redirect(base_url().'Admin/login');

      }

    

  }



public function deleteStaticOption()

{

  

}







public function createOption($opt_type = 2)

{

  $data = array();



  if($this->session->userdata('isUserLoggedIn'))

    {   

          if($this->input->post('btn_submit'))

          {

            $data = null;

            $opt_id = strip_tags($this->input->post('txt_option_id'));

            $opt_type = strip_tags($this->input->post('txt_option_type'));

            $data = array(

                          'opt_name' => strip_tags($this->input->post('txt_option_name')),

                          'opt_description' => $this->input->post('txt_option_description'),

                          'opt_status' => 1,

                          'opt_type' => $opt_type,



                            );

            $insert = $this->Admin_model->createOption($data);

            redirect(base_url()."Admin/option_list");

         

          }

          else {                         

            $data['optiontype3'] =  $this->Admin_model->getOptionTypeDetails($opt_type);

            $data['optiontype'] = $opt_type;

            $data["editOption"] = "no";

            header("Access-Control-Allow-Origin: *");                     

            $this->template->load('admin_layout', 'contents' , 'createOption', $data);

          }

     }

    else

    {

      redirect(base_url().'Admin/login');

    }

  

}



public function editOption()

{

  $data = array();

  $opt_id = $this->uri->segment(3);

  $opt_type = 2;

  if($this->session->userdata('isUserLoggedIn'))

    {   

          if($this->input->post('btn_submit'))

          {

              $data = null;

              $opt_id = strip_tags($this->input->post('txt_option_id'));

              $opt_type = strip_tags($this->input->post('txt_option_type'));

              $data = array(

                            'opt_name' => strip_tags($this->input->post('txt_option_name')),

                            'opt_description' => $this->input->post('txt_option_description'),

                            'opt_status' => 1,

                            'opt_type' => $opt_type,



                              );

                             

                  if(!empty($opt_id))

                  {  $whr = array("opt_id" => $opt_id);

                    $update = $this->Admin_model->updateOption($data, $whr);

                    redirect(base_url()."Admin/option_list");

                          

                  }

                 



          }

          else {

                $data['optiontype3'] =  $this->Admin_model->getOptionTypeEditDetails($opt_id);

                // print_r( $data['optiontype3']);

                // die();

                $data["editOption"] = "yes";

                $data['optiontype'] = $opt_type;

                header("Access-Control-Allow-Origin: *");                     

                $this->template->load('admin_layout', 'contents' , 'createOption', $data);

          }

     }

    else

    {

      redirect(base_url().'Admin/login');

    }

  

}











public function deletoption()

{



$data = array();

$data['error_msg']=""; 

$optionData = array();

$id = "";

if($this->uri->segment(3)){

$id = ($this->uri->segment(3)) ;

}



// if($this->uri->segment(4)){

// $action = ($this->uri->segment(4)) ;

// $data['act'] = $action;

// }

// else

// {

// $data['act'] = "view"; 

// }







$con['conditions'] = array(

          'opt_id'=> $id

    );



$optionData = array(

              'opt_status' => '3'

              );

 $rec = $this->Admin_model->updateOption($optionData, $con['conditions']);



if($rec > 0)

{

$data['deleted'] = "yes";

}



redirect(base_url("Admin/option_list"));



// $option_data = $this->Admin_model->getOptionDetails();

// $data['option_list'] = $option_data;

// header("Access-Control-Allow-Origin: *");

// $this->template->set('title', 'User List');

// $this->template->load('admin_layout', 'contents' , 'option_price', $data);



}



public function get_static_option_price(){

	$static_id = $_POST['static_id'];

	$otd_price = $this->db->get_where('otd_option_price', array("opt_type_id"=>$static_id))->row();

 

	$data['name'] = $otd_price->opt_text;

	$data['opt_price'] = $otd_price->opt_price;

	$data['status'] = 200;

	echo json_encode($data);

	

	

}

public function save_static_option_price(){

	

	 extract($_POST);

	 $otd_price = $this->db->get_where('otd_option_price', array("opt_type_id"=>$id))->result();

	 

	 $data['opt_text'] = $op_name;

	 $data['opt_price'] = $op_price;

	

	 $data['opt_pr_status'] = 1;

	  

	 if(empty($otd_price) and $id ==''){

		  $data['opt_type_id'] = $op_price_id;

		  $this->db->insert("otd_option_price",$data);		 

	 }else{

		  

		  $this->db->where("opt_price_id",$id);

		  $this->db->update("otd_option_price",$data);		 

		 

	 }



	 $return['status'] = 200;

	 echo json_encode($return);

	

}

  

  function importDeleteUserCSV(){

    $data = array();

    $uploaded_file = "";

    $csvs = array();

    $result= "";

    $cat_count = 0;



    if($this->input->post('ImportSubmit'))

    {

      $config['upload_path'] = 'assets/content_csv/'; //The path where the image will be save

      $config['allowed_types'] = 'csv|CSV|xls|xlsx'; //Images extensions accepted

      $config['max_size']    = '20480'; //The max size of the image in kb's

      $config['overwrite'] = TRUE; 



      $this->load->library('upload', $config); //Load the upload CI library

    

  

      if (!$this->upload->do_upload('userfile'))

      {              

        $error = $this->upload->display_errors();

        echo $error;  

      }

      else

      {

        $file_info = $this->upload->data();

        $file_name = $file_info['file_name']; 

      }

          

      // echo base_url()."assets/content_csv/". $file_name;

      $result =  $this->deleteBusinessCSVData(base_url()."assets/content_csv/". $file_name);

        

      $data["success"] = $result["success"];

      //print_r($result);

      //die("here");   

    }else{

      $data["error"]="File not found";

    }



    if($result == 1)

    {

      $data['import_done'] = "Business profiles are deleted successfully";

    }

    else

    {

      $data['import_done'] = "Business profiles are not deleted, Please try again";

    }



    $con = null;

    $con = array();

    $con['returnType'] = 'count';

    //$cat_count  = $this->Categories->getCatRows($con);



    if($cat_count > 0){

    $con = null;

    $con = array();

    $data["cat_count"] = $cat_count;

    $con['returnType'] = '';

    $data['businesslist'] = $this->Categories->getBusinessRows($con);

    }

    else

    {

      $data['businesslist'] = array();

    }





    $config['base_url'] = base_url()."Category/import_businesscsv/";

    $config['total_rows'] = $cat_count;

    $config['per_page'] = 50;

    $config['uri_segment'] = 3;

    $config['num_links'] = 2;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    $choice = $config["total_rows"] / $config["per_page"];

    $config["num_links"] = round($choice);

    $this->pagination->initialize($config);



    $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;



    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];



    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }



    $con['start'] = $start;

     if($cat_count > 0){

          $data['catlist'] = $this->Categories->getCatRows($con);

          $data["record_found"] = $cat_count; 

        }

        else

        {

          $data["record_found"] = 0; 

        }



        $data["links"] = $this->pagination->create_links();

        $data['page'] = $page;



        $data['import_type']="Categories"; 

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'Import CSV File'); 

        $this->template->load('admin_layout', 'contents' , 'import_deleteusercsv', $data);

  }



  function deleteBusinessCSVData($fp){

    $count=0;

    $fp = fopen($_FILES['userfile']['tmp_name'],'r') or die("can't open file");

    while($csv_line = fgetcsv($fp,1024))

    {

      $count++;

      if($count == 1)

      {

          continue;

      }//keep this if condition if you want to remove the first row



      $businessID = $csv_line[0];

      $userids = array();

      if(!empty($businessID)){

        $bsdetails = $this->Admin_model->getRows(array("conditions"=>array("businessID"=>$businessID)));        

        if(!empty($bsdetails)){

          foreach($bsdetails as $bsdt){

            $userids[] = $bsdt['user_id'];

          }

          if(!empty($userids)){

            $this->Admin_model->deleteUserFromSite($userids);

          }

        }

      }

      // echo "<pre>";

      // print_r($userids);

      // echo "</pre>";

      // $i++;

      $data['success']="success";

    }    

    fclose($fp) or die("can't close file");

    //  $data['success']="success";

    return $data;

  }



  function purchasedPackageView($args){

    $ses = $this->session->userdata("user_id");    

    if(!empty($ses)){

      $data = array();

      header("Access-Control-Allow-Origin: *");

      $this->template->set('title', 'User List');      

      $data['details'] = $this->Admin_model->getUserOptionRows(array("opt_user_option_id"=>$args));

      $data['options'] = $this->Admin_model->getPurchasedPackageOptionRows(array("conditions"=>array("opt_pkg_id"=>$data['details']['opt_user_option_id'])));

      $this->template->load('admin_layout', 'contents' , 'Package/packageview', $data);

    }else{

      $this->session->set_flashdata("error", "Session Expired, Please login.");

      redirect(base_url()."Admin");

    }

  }



  function createPackage(){

    $ses = $this->session->userdata("user_id");    

    if(!empty($ses)){

      $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

      $this->form_validation->set_rules("txt_option_name", "package name", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("txt_option_description", "package description", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("text_opt_price", "package price", "required|numeric", array("required"=>"Please enter %s",'numeric'     => 'This %s not numeric value.'));

      //$this->form_validation->set_rules("text_opt_text", "price option text", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("status", "package status", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("text_opt_duration", "package duration", "required", array("required"=>"Please enter %s"));

      if($this->form_validation->run() == FALSE){

        $data = array();

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'Create Package');

        $this->template->load('admin_layout', 'contents' , 'Package/create', $data);

      }else{

        $data = array(

                      'opt_name' => strip_tags($this->input->post('txt_option_name')),

                      'opt_description' => $this->input->post('txt_option_description'),

                      'price' => $this->input->post('text_opt_price'),

                      'duration' => $this->input->post('text_opt_duration'),

                      'opt_status' => $this->input->post('status'),

                      'opt_type' => 4,

                    );      

        $insert = $this->Admin_model->createOption($data);

        $this->session->set_flashdata("success", "Package Added Successfully.");

        redirect(base_url()."Admin/option_list");

      }

    }else{

      $this->session->set_flashdata("error", "Session Expired, Please login.");

      redirect(base_url()."Admin");

    }

  }



  function editPackage($args = NULL){

    $ses = $this->session->userdata("user_id");    

    if(!empty($ses)){

      if(!empty($args)){

        $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

        $this->form_validation->set_rules("txt_option_name", "package name", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("txt_option_description", "package description", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("text_opt_price", "package price", "required|numeric", array("required"=>"Please enter %s",

         'numeric'     => 'This %s not numeric value.'));

        //$this->form_validation->set_rules("text_opt_text", "price option text", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("status", "package status", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("text_opt_duration", "package duration", "required", array("required"=>"Please enter %s"));

        if($this->form_validation->run() == FALSE){

          $data = array();

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'Edit Package');



          $data['packageDetails'] = $this->Admin_model->getPackageEditDetails1($args);

          if(empty($data['packageDetails'])){

            $this->session->set_flashdata("error", "No package available.");

            redirect(base_url()."Admin/option_list");

          }

          $this->template->load('admin_layout', 'contents' , 'Package/edit', $data);

        }else{

          $data = array(

                        'opt_name' => strip_tags($this->input->post('txt_option_name')),

                        'opt_description' => $this->input->post('txt_option_description'),

                        'opt_status' => $this->input->post('status'),

                        'duration' => $this->input->post('text_opt_duration'),

                        'price' => $this->input->post('text_opt_price'),

                        'opt_type' => 4,

                        

                      );      

          $insert = $this->Admin_model->updateOption($data, array("opt_id"=>$args));

          

          $this->session->set_flashdata("success", "Package Updated Successfully.");

          redirect(base_url()."Admin/option_list");

        }

      }else{

        $this->session->set_flashdata("error", "No package available.");

        redirect(base_url()."Admin/option_list");

      }

    }else{

      $this->session->set_flashdata("error", "Session Expired, Please login.");

      redirect(base_url()."Admin");

    }

  }







   function viewPackage($args = NULL){

    $ses = $this->session->userdata("user_id");    

    if(!empty($ses)){

      if(!empty($args)){

        $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

        $this->form_validation->set_rules("txt_option_name", "package name", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("txt_option_description", "package description", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("text_opt_price", "package price", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("text_opt_text", "price option text", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("status", "package status", "required", array("required"=>"Please enter %s"));

        if($this->form_validation->run() == FALSE){

          $data = array();

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'View Package');

          $data['masteroption'] = $this->Admin_model->masterOptionList(1);



          //print_r("<pre/>");

          //print_r($data['masteroption']);

          //die;

          $data['packageDetails'] = $this->Admin_model->getPackageRows(array("conditions"=>array("opt_id"=>$args), "returnType"=>"single"));

          if(empty($data['packageDetails'])){

            $this->session->set_flashdata("error", "No package available.");

            redirect(base_url()."Admin/option_list");

          }

          $this->template->load('admin_layout', 'contents' , 'Package/view', $data);

        }else{

          $data = array(

                        'opt_name' => strip_tags($this->input->post('txt_option_name')),

                        'opt_description' => $this->input->post('txt_option_description'),

                        'opt_status' => $this->input->post('status'),

                        'opt_type' => 4,

                      );      

          $insert = $this->Admin_model->updateOption($data, array("opt_id"=>$args));

          

          $optprice = array("opt_text"=>"1 Year",

                            "opt_price_type"=>"1 Year",

                            "opt_qnty"=>1,

                            "opt_price"=>$this->input->post("text_opt_price"),

                            "opt_text"=> $this->input->post("text_opt_text"),

                            "opt_is_package"=>1,                              

                            "opt_pr_status"=>$this->input->post("status"),

                           );

          $this->Admin_model->updatePriceOption($optprice, array("opt_type_id"=>$args));

          $mst = $this->input->post("mst");

          $this->Admin_model->deletePackageOption(array("pkg_package_id"=>$args));

          if(!empty($mst)){

            $pkg = array();

            foreach($mst as $idss){

              $pkg["pkg_package_id"]= $args;

              $pkg["pkg_inc_optid"] = $idss;

              $pkg["pkg_opt_duration"] = $this->input->post("prcid_".$idss);

              //if($idss == 6){

                //$pkg['pkg_opt_qty'] = $this->input->post("prcidph_".$idss);

              //}

              $this->Admin_model->insertPackageOption($pkg);

            }

          }          

          $this->session->set_flashdata("success", "Package Updated Successfully.");

          redirect(base_url()."Admin/option_list");

        }

      }else{

        $this->session->set_flashdata("error", "No package available.");

        redirect(base_url()."Admin/option_list");

      }

    }else{

      $this->session->set_flashdata("error", "Session Expired, Please login.");

      redirect(base_url()."Admin");

    }

  }



  function deletePackage($args){

    

          $data = array('opt_status' => 3);      

          $insert = $this->Admin_model->updateOption($data, array("opt_id"=>$args));

          //$this->Admin_model->deletePackageOption(array("pkg_package_id"=>$args));

          $this->session->set_flashdata("success", "Package Deleted Successfully.");

          redirect(base_url()."Admin/option_list");

  }



  function linkOptionUserSearch(){

    $segcount = $this->uri->total_segments();

    $order_by = "";

    $order="";

    $type ="";

    $page = 0;



    $this->session->unset_userdata('profltr_user_firstname');

    $this->session->unset_userdata('profltr_user_lastname');

    $this->session->unset_userdata('profltr_user_language');

    $this->session->unset_userdata('profltr_user_email');

    $this->session->unset_userdata('profltr_signup_method');

    $this->session->unset_userdata('profltr_user_company_name');

    $this->session->unset_userdata('profltr_user_company_number');

    $this->session->unset_userdata('profltr_user_company_address');



    $fltr_user_firstname = "";

    $fltr_user_lastname = "";

    $fltr_user_language = "";

    $fltr_user_company_name = "";

    $fltr_user_company_address = "";

    $fltr_user_company_number = "";

    $fltr_user_email = "";

    $fltr_signup_method = "";



    $data = array();

    $data['fltr_user_firstname'] = "";

    $data['fltr_user_lastname'] = "";

    $data['fltr_user_language'] = "";

    $data['fltr_user_email'] = "";

    $data['fltr_signup_method'] = "";

    $data['fltr_user_company_name'] = "";

    $data['fltr_user_company_number'] = "";

    $data['fltr_user_company_address'] = "";





    if($segcount == 3)

    {

    $type = $this->uri->segment(3);

    }

    else if ($segcount == 5)

    {

    $order = $this->uri->segment(4);

     $type = $this->uri->segment(3);

    }

    else if ($segcount == 4)

    {

    $order = "company";

    $type = $this->uri->segment(3);

    }

    else if($segcount < 3)

    {

     $order = "company";

     $type = "Professional";

    }





    if($order == 'firstname')

    {

    $order_by = "user_firstname";

    }

    else if($order == 'lastname')

    {

    $order_by = "user_lastname";

    }

    else if($order == 'email')

    {

    $order_by = "user_email";

    }

    else if($order == 'company')

    {

    $order_by = "user_company_name";

    }

    else if($order == 'number')

    {

    $order_by = "user_company_number";

    }

    else if($order == 'address')

    {

    $order_by = "user_company_address";

    }

    else if($order == 'lastlogin')

    {

    $order_by = "last_login";

    }

    else

    {

    $order_by = "user_company_name";

    }



    if(is_numeric($type))

    {

     $type = "Professional";

    }



    $con['sorting'] = array('order'=>$order_by);



    $data["type"] = $type;

    $con['conditions'] = array('user_type'=> 'Professional');



    //Filter Code



    if($this->input->post('filterSubmit'))

     {

    $fltr_user_firstname = $this->input->post('user_firstname');

    $fltr_user_lastname = $this->input->post('user_lastname');

    $fltr_user_language = $this->input->post('user_language');

    $fltr_user_email = $this->input->post('user_email');

    $fltr_signup_method = $this->input->post('signup_method');

    $fltr_user_company_name = $this->input->post('user_company_name');

    $fltr_user_company_number = $this->input->post('user_company_number');

    $fltr_user_company_address = $this->input->post('user_company_address');



    $this->session->set_userdata('profltr_user_firstname', $fltr_user_firstname);

    $this->session->set_userdata('profltr_user_lastname', $fltr_user_lastname);

    $this->session->set_userdata('profltr_user_language', $fltr_user_language);

    $this->session->set_userdata('profltr_user_email', $fltr_user_email);

    $this->session->set_userdata('profltr_signup_method', $fltr_signup_method);

    $this->session->set_userdata('profltr_user_company_name', $fltr_user_company_name);

    $this->session->set_userdata('profltr_user_company_number', $fltr_user_company_number);

    $this->session->set_userdata('profltr_user_company_address', $fltr_user_company_address);



    $data['fltr_user_firstname'] = $fltr_user_firstname;

    $data['fltr_user_lastname'] = $fltr_user_lastname;

    $data['fltr_user_language'] = $fltr_user_language;

    $data['fltr_user_email'] = $fltr_user_email;

    $data['fltr_signup_method'] = $fltr_user_email;

    $data['fltr_user_company_name'] = $fltr_user_company_name;

    $data['fltr_user_company_number'] = $fltr_user_company_number;

    $data['fltr_user_company_address'] = $fltr_user_company_address;

    }

    else

    {

    $fltr_user_firstname = $this->session->userdata('profltr_user_firstname');

    $fltr_user_lastname = $this->session->userdata('profltr_user_lastname');

    $fltr_user_language = $this->session->userdata('profltr_user_language');

    $fltr_user_email = $this->session->userdata('profltr_user_email');

    $fltr_signup_method = $this->session->userdata('profltr_signup_method');

    $fltr_user_company_name = $this->session->userdata('profltr_user_company_name');

    $fltr_user_company_number = $this->session->userdata('profltr_user_company_number');

    $fltr_user_company_address = $this->session->userdata('profltr_signup_methoduser_company_address');



    if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit'))

     {

      $this->session->unset_userdata('profltr_user_firstname');

      $this->session->unset_userdata('profltr_user_lastname');

      $this->session->unset_userdata('profltr_user_language');

      $this->session->unset_userdata('profltr_signup_method');

      $this->session->unset_userdata('profltr_user_email');

      $this->session->unset_userdata('profltr_user_company_name');

      $this->session->unset_userdata('profltr_user_company_number');

      $this->session->unset_userdata('profltr_user_company_address');



      $fltr_user_firstname = "";

      $fltr_user_lastname = "";

      $fltr_user_language = "";

      $fltr_user_company_name = "";

      $fltr_user_company_address = "";

      $fltr_user_company_number = "";

      $fltr_user_email = "";

      $fltr_signup_method = "";



      $data['fltr_user_firstname'] = "";

      $data['fltr_user_lastname'] = "";

      $data['fltr_user_language'] = "";

      $data['fltr_user_email'] = "";

      $data['fltr_signup_method'] = "";

      $data['fltr_user_company_name'] = "";

      $data['fltr_user_company_number'] = "";

      $data['fltr_user_company_address'] = "";

     }

    }





    if($fltr_user_firstname != "")

    {

    $con['filter']['user_firstname'] =$fltr_user_firstname;

    }

    if($fltr_user_lastname != "")

    {

    $con['filter']['user_lastname'] =$fltr_user_lastname;

    }



    if($fltr_user_language != "")

    {

      $con['filter']['user_language'] =$fltr_user_language;

    /*

      if($fltr_signup_method ==2){$con['filter']['user_language'] ="English";}  

      if($fltr_signup_method ==3){$con['filter']['user_language'] ="French";}

      if($fltr_signup_method ==4){$con['filter']['user_language'] ="Spanish";}

      if($fltr_signup_method ==5){$con['filter']['user_language'] ="Arab";}

      if($fltr_signup_method ==6){$con['filter']['user_language'] ="Italion";}

      if($fltr_signup_method ==7){$con['filter']['user_language'] ="German";}*/

    }



    if($fltr_user_email != "")

    {

    $con['filter']['user_email'] =$fltr_user_email;

    }

    if($fltr_signup_method != "" && $fltr_signup_method != "-1")

    {

          if($fltr_signup_method ==1){

            // $con['filter']['user_signup_method'] = "Regular";

          }

          if($fltr_signup_method ==2){$con['filter']['user_signup_method'] = "Facebook";}  

          if($fltr_signup_method ==3){$con['filter']['user_signup_method'] = "Google";}

    }

    if($fltr_user_company_name != "")

    {

    $con['filter']['user_company_name'] =$fltr_user_company_name;

    }

    if($fltr_user_company_number != "")

    {

    $con['filter']['user_company_number'] =$fltr_user_company_number;

    }

    if($fltr_user_company_address != "")

    {

    $con['filter']['user_company_address'] =$fltr_user_company_address;

    }

      



    $totalrows = $this->Admin_model->record_count($con);

    $config['base_url'] = base_url()."Admin/linkOptionUserSearch/".$order;

    $config['total_rows'] = $totalrows;

    $config['per_page'] = 10;

    $config['uri_segment'] = 4;

    $config['num_links'] = 2;

    $config['cur_tag_open'] = '&nbsp;<a class="current">';

    $config['cur_tag_close'] = '</a>';

    $config['next_link'] = '<b> Next </b>';

    $config['prev_link'] = '<b> Previous </b>';

    $choice = $config["total_rows"] / $config["per_page"];

    // $config["num_links"] = round($choice);

    $this->pagination->initialize($config);



    $page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;

    if(is_numeric($this->uri->segment(4)))

    {

      $page = ($this->uri->segment(4));

    }

    else if(is_numeric($this->uri->segment(3)))

    {

      $page = ($this->uri->segment(3));

    }





    $con['limit'] = $config['per_page'];

    $start =($page-1) * $config['per_page'];



    if($start == 0)

    {

      $start = 0;

    }

    else

    {

      $start = ($start/$config['per_page'] )+1;

    }

    $con['start'] = $start;

    $data['userlist'] = array();



    if($this->session->userdata('isUserLoggedIn'))

            {

       

        if($totalrows > 0){

         // print_r($con);

          $data['userlist'] = $this->Admin_model->getRows($con);

          $data["record_found"] = $totalrows; 

        }

        else

        {

          $data["record_found"] = 0; 

        }



    $data["links"] = $this->pagination->create_links();

    $data['usertype'] = $type;

    $data['order'] = $order;

    $data['page'] = $page;



    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'User List');

    $this->template->load('admin_layout', 'contents' , 'LinkUser/linkoptionuser', $data);

    }else{

     redirect(base_url().'Admin/login');

    }

  }



  function linkOptiontoUser($args){

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      if(!empty($args)){

        $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

        $this->form_validation->set_rules("mst[]", "option", "required", array("required"=>"Please select %s"));

        if($this->form_validation->run() == FALSE){

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'User List');

          $data['masteroption'] = $this->Admin_model->masterOptionList(1);

          $data['packagelist'] = $this->Admin_model->masterOptionList(4);

          $data['userlist'] = $this->Admin_model->getRows(array("user_id"=>$args));

          if(empty($data['userlist'])){

            $this->session->set_flashdata("error", "No Records Found");

            redirect(base_url("Admin/linkOptionUserSearch"));

          }

          $this->template->load('admin_layout', 'contents' , 'LinkUser/create', $data);

        }else{                    

          $trnsid = $this->Admin_model->insertTransaction(array("pyt_txn_status"=>"Added By Admin"));

          $linkoptions = $this->input->post("mst");

          $linkpackages = $this->input->post("pkg");

          $linkeduserid = $this->input->post("userid");

          $userlinkedoptions = array(); //linked options will be pushed in it to send bunch email          

          if(!empty($linkoptions)){

            $pusharray = array();

            foreach($linkoptions as $lnkopt){

                $arr = array("opt_user_id"=> $linkeduserid,

                             "opt_option_id"=>$lnkopt,

                             "opt_option_type"=>1,

                             "opt_option_purchase_date"=>date("Y-m-d"),

                             "otp_option_duration"=>$this->input->post("prcid_".$lnkopt),

                             "opt_tran_id"=>$trnsid

                            );

                if($lnkopt == 6){

                  $arr['otp_option_qnty'] = $this->input->post("prcidph_".$lnkopt);

                }

                $userlinkedoptions[] = $lnkopt;

                $pusharray[] = $arr;

            }

            $this->Admin_model->insertUserOptionInBulk($pusharray);

          }

          if(!empty($linkpackages)){            

            foreach($linkpackages as $lnkpkg){

              $arr = array("opt_user_id"=>$linkeduserid,

                           "opt_option_id"=>$lnkpkg,

                           "opt_option_type"=>4,

                           "opt_option_purchase_date"=>date("Y-m-d"),

                           "otp_option_duration"=>$this->input->post("pkgprcid_".$lnkpkg),

                           "opt_tran_id"=>$trnsid

                          );

              $inspkgid = $this->Admin_model->insertUserOption($arr);                

              if($inspkgid){

                $pkgopts = $this->Admin_model->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$lnkpkg)));

                if(!empty($pkgopts)){

                  $pusharray = array();

                  foreach($pkgopts as $opts){

                    $arr = array("opt_user_id"=>$linkeduserid,

                                 "opt_option_id"=>$opts['pkg_inc_optid'],

                                 "opt_option_type"=>5,

                                 "opt_pkg_id"=>$inspkgid,

                                 "opt_option_purchase_date"=>date("Y-m-d"),

                                 "otp_option_duration"=>$this->input->post("pkgprcid_".$lnkpkg."_".$opts['pkg_inc_optid']),

                                 "opt_tran_id"=>0);

                    if($opts['pkg_inc_optid'] == 6){

                      $arr['otp_option_qnty'] = $this->input->post("prcidph_".$lnkpkg."_".$opts['pkg_inc_optid']);

                    }

                    $pusharray[] = $arr;

                  }

                  $this->Admin_model->insertUserOptionInBulk($pusharray);

                }

              }

              $userlinkedoptions[] = $lnkpkg;                

            }

          }

          $this->Admin_model->addUserNotification($args, 4);

          $optlist = $this->Admin_model->getSelectedMasterOptions(array("select"=>"opt_name", "wherein"=>$userlinkedoptions));

          $oplist = array();

          foreach($optlist as $lst){

            $oplist[] = $lst['opt_name'];

          }

          $oplist = implode(", ", $oplist);



          //Fetch Email Templates

          $templateheader = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header

          $templatecenter = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>27));

          $templatefooter = $this->Admin_model->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer

          $subject = $templatecenter['tmplate_subject'];

          $userData = $this->Admin_model->getRows(array("user_id"=>$args));          

          $dummy = array("%%Firstname%%",

                         "%%LastName%%",

                         "%%Email%%",

                         "%%BusinessName%%",

                         "%%Address%%",

                         "%%Phone%%",

                         "%%Gender%%",

                         "%%OptionsName%%",

                         );

          $real = array($userData['user_firstname'],

                        $userData['user_lastname'], 

                        $userData["user_email"], 

                        $userData['user_company_name'], 

                        $userData['user_company_address'], 

                        $userData['user_phone'], 

                        $userData['user_gender'],

                        $oplist

                        );

          $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );

          $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];

          $this->send_mail($userData["user_email"], $subject, $msg); //send mail to user

          $this->send_mail(ADMIN_EMAIL, $subject, $msg); // send mail to admin          

          $this->session->set_flashdata("success", "Option linked to user successfully.");

          redirect(base_url("Admin/linkOptionUserSearch"));

        }

      }else{

        $this->session->set_flashdata("error", "Invalid Parameter Passed.");

        redirect(base_url()."Admin/linkOptionUserSearch");

      }

    }else{

      $this->session->set_flashdata("error", "Session Expired, Please login.");

      redirect(base_url()."Admin");

    }    

  }



  function displayOptionPriceListingViaAjax(){

    $ses = $this->session->userdata("user_id");

    if(!empty($ses)){

      $id = $this->input->post("id");

      if(!empty($id)){

        $mstprices = $this->Admin_model->getOptionPriceRows(array("conditions"=>array("opt_type_id"=>$id, "opt_pr_status"=>1)));

        $options = "";

        $cnt = 1;

        $check = "";

        foreach($mstprices as $pr){

          if($cnt == 1){

            $check = "checked";

          }else{

            $check = "";

          }

          if($pr['opt_price_type'] == "1 Day")

            $single = "single";

          else

            $single = "";

          $options .= "<div><input ".$check." atr-val='".$single."' type='radio' class='optionprices' name='opt_price' value='".$pr['opt_price_id']."' /> ".$pr['opt_text']." = ".$pr['opt_price']."</div>";

          $cnt++;

        }

        echo json_encode(array("status"=>200, "content"=>$options));

      }else{

        echo json_encode(array("status"=>1, "msg"=>"Required parameters missing."));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }



  function send_mail($email, $subject="Test Mail", $msg="Testing mail", $cc=NULL, $bcc=NULL){

    $from = SENDER_EMAIL;

    $this->load->library('email');

    $config['protocol']    = 'ssl';

    $config['smtp_host']    = 'ssl://mail.gandi.net';

    $config['smtp_port']    = 587;

    $config['smtp_timeout'] = '7';

    $config['smtp_user']    = $from;

    $config['smtp_pass']    = 'puR1-7,XifsArlyV!5';

    $config['charset']    = 'utf-8';

    $config['newline']    = "\r\n";

    $config['mailtype'] = 'html'; 

    $config['validation'] = TRUE;      

    $this->email->initialize($config);

    $fromname = SENDER_NAME;

    $fromemail =SENDER_EMAIL;

    $to = $email;        

    $subject=$subject;

    $message=$msg;

    $cc=false;

    $this->email->from($fromemail, $fromname);

    $this->email->to($to);

    $this->email->subject($subject);       

    $this->email->message($msg);  

    if($cc){

        $this->email->cc($cc);

    }



    if(!$this->email->send()){

        return $this->email->print_debugger();

    }else{

        return true;

    }

  } 



	public function generateRandomString($length = 12) {

		// $length = 12;

    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    $charactersLength = strlen($characters);

    $randomString = '';

    for ($i = 0; $i < $length; $i++) {

      $randomString .= $characters[rand(0, $charactersLength - 1)];

    }

    return $randomString;

	}  





  function Claimed_profileLIsting(){

    $data = array();

    $data['msoptions'] = $this->Admin_model->getClaimedRows();

    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'Claimed Business Profile Requeste Listing');

    $this->template->load('admin_layout', 'contents' , 'ClaimedProfile/list', $data);

  }



  function viewClaimedprofile($args){

    $data = array();

    $data['details'] = $this->Admin_model->getClaimedRows(array("claim_request_id"=>$args));

    if(empty($data['details']))

      redirect(base_url("Admin/Claimed_profileLIsting"));

    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'Claimed Business Profile Requeste Listing');

    $this->template->load('admin_layout', 'contents' , 'ClaimedProfile/view', $data);

  }



  function deleteClaimedprofile(){

    $ids = $this->input->post("ids");

    if( !empty($this->session->userdata('isUserLoggedIn')) && $this->session->userdata('user_type') == "Admin"){

      if(!empty($ids)){

        $check = $this->Admin_model->deleteClaimedProfileRow($ids);

        if($check){

          echo json_encode(array("status"=>200, "msg"=>"done"));

        }else{

          echo json_encode(array("status"=>1, "msg"=>"Something went wrong, Please try after sometime."));

        }

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }



  function deleteallClaimedprofile(){    

    if( !empty($this->session->userdata('isUserLoggedIn')) && $this->session->userdata('user_type') == "Admin"){      

      $check = $this->Admin_model->deleteAllClaimedProfileRow();

      if($check){

        echo json_encode(array("status"=>200, "msg"=>"done"));

      }else{

        echo json_encode(array("status"=>1, "msg"=>"Something went wrong, Please try after sometime."));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));

    }

  }





  /*

  * upload user purchase documents

  */

  public function uploadUserPurchaseDocument(){    

    if(!empty($this->session->userdata("user_id"))){      

      $random = $this->generateRandomString(30);

      $ext = pathinfo($_FILES['manddoc']['name'], PATHINFO_EXTENSION);        

      $file_name = $random.".".$ext;

      $dirpath = './././assets/img/UserPurchaseDocuments/'.$file_name;

      if($ext == "pdf" ){

        $where = array();

        if(move_uploaded_file($_FILES['manddoc']['tmp_name'], $dirpath)){                

            $params = array("dc_docid"=>$this->input->post("options"),

                            "dc_userid"=> $this->input->post("user"),

                            "dc_docpath"=>$file_name

                            );

            $mode = 1; //insert into table



            // $mandateref = $this->Slimpay_model->getMandate(array("slmref_mandateref"=>$this->input->post("mandate")));                

            // if(!empty($mandateref)){

            //     $filepath = $mandateref['slmref_document'];

            //     if(!empty($mandateref['slmref_document']))

            //         unlink("./././assets/img/UserPurchaseDocuments/$filepath");

            //     $mode = 2;

            //     $where["slmref_id"] = $mandateref['slmref_id'];

            // }else{

            //     $params["slmref_id"] = $mandateref['slmref_id'];

            // }

            // $check = $this->Slimpay_model->updateMandate($params, $where);



            $check = $this->Admin_model->insertTable($mode, "otd_user_purchasedoc", $params);

            if($check){

              echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Document uploaded successfully.</label>"));

            }

            else{

              echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Document not uploaded, Please try again.</label>"));

            }

        }

        else{

            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Document not uploaded, Please try again.</label>"));

        }

      }

      else{

          echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please upload only pdf file.</label>"));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Invalid access.</label>"));

    }

  }



  /*

  * download document

  */

  public function attachedDocument($args){

    if(!empty($this->session->userdata("user_id")) && !empty($args)){

      $details = $this->Admin_model->getPurchaseDocumentRows(array("dc_id"=>$args));

      $filename = $details['dc_docpath'];

      $path = "./././assets/img/UserPurchaseDocuments/$filename";

      $filename = $details['dc_userid']."-".$details['ct_name'].".pdf";

      header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly                

      header('Accept-Ranges: bytes');  // For download resume

      header('Content-Length: ' . filesize($path));  // File size

      header('Content-Encoding: none');

      header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF

      header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog                

      readfile($path);  //this is nec      

    }    

  }



  /*

  * remove purchased uploade document

  */

  public function removeAdminUploadDoc(){

    if(!empty($this->session->userdata("user_id"))){

      $id = $this->input->post("id");

      if(!empty($id)){

        $docdetails = $this->Admin_model->getPurchaseDocumentRows(array("dc_id"=>$id));

        $check = $this->Admin_model->removeTable("otd_user_purchasedoc", array("dc_id"=>$id));

        if($check){

          if(!empty($docdetails['dc_docpath'])){

            $filepath = $docdetails['dc_docpath'];

            unlink("./././assets/img/UserPurchaseDocuments/$filepath");

          }

          echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Document removed successfully.</label>"));

        }else{

          echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Document not removed, Please try again.</label>"));

        }

      }else{

        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Required parameters missing.</label>"));

      }

    }else{

      echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Invalid access.</label>"));

    }

  }

  function pacakge_list(){

    $data = array();

    $option_data_typ2_2 = $this->Admin_model->getPackageDetailsAdmin();   

    $data['option_list_2_3'] = $option_data_typ2_2;

    header("Access-Control-Allow-Origin: *");

    $this->template->set('title', 'Featured Package List');

    $this->template->load('admin_layout', 'contents' , 'package_price', $data);

  }

  function deleteFeaturePackage($args){

     $data = array('status' => 3);      

     $insert = $this->Admin_model->updatePackage($data, array("id"=>$args));

     $this->session->set_flashdata("success", "Package Deleted Successfully.");

     redirect(base_url()."Admin/pacakge_list");

  }

  public function activatePackage(){

    $data = array();

    $data['error_msg']=""; 

    $optionData = array();

    $act ="";

    $id = "";

    if($this->uri->segment(3)){

    $id = ($this->uri->segment(3)) ;

    }

    if($this->uri->segment(4)){

      $act = ($this->uri->segment(4)) ;

      }

    $con['conditions'] = array(

              'id'=> $id

        );

    

        if($act == 1)

        {

                $optionData = array(

                'status' => '2'

                  );

      }

                else {

                  

                    $optionData = array(

                                  'status' => '1'

                                  );

                                

                }



    $rec = $this->Admin_model->updatePackage($optionData, $con['conditions']);

    if($rec > 0)

    {

    $data['activated'] = "yes";

    }

    $this->session->set_flashdata("success", "Status Updated Successfully.");

    redirect(base_url()."Admin/pacakge_list");

    

  }

  function updatePackage($args = NULL){

    $ses = $this->session->userdata("user_id");    

     

        $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

        $this->form_validation->set_rules("package_name", "package name", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("package_desc", "package description", "required", array("required"=>"Please enter %s"));

        $this->form_validation->set_rules("package_price", "package price", "required", array("required"=>"Please enter %s"));

        

        $this->form_validation->set_rules("status", "package status", "required", array("required"=>"Please enter %s"));

        if($this->form_validation->run() == FALSE){

          $data = array();

          header("Access-Control-Allow-Origin: *");

          $this->template->set('title', 'Edit Package');

          $data['packageDetails'] = $this->Admin_model->getPackageEditDetails($args);

          if(empty($data['packageDetails'])){

            $this->session->set_flashdata("error", "No package available.");

            redirect(base_url()."Admin/option_list");

          }

          $this->template->load('admin_layout', 'contents' , 'Package/update', $data);

        }else{

          $data = array(

                        'package_title' => strip_tags($this->input->post('package_name')),

                        'package_price' => $this->input->post('package_price'),

                        'package_desc' => $this->input->post('package_desc'),

                        'package_validity' => $this->input->post('package_validity'),

                        'status' => $this->input->post('status'),

                      );      

          $insert = $this->Admin_model->updatePackage($data, array("id"=>$args));

          $this->session->set_flashdata("success", "Package Updated Successfully.");

          redirect(base_url()."Admin/pacakge_list");

        }

  }

  function addPackage(){



      $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");

      $this->form_validation->set_rules("package_name", "package name", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("package_desc", "package description", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("package_price", "package price", "required", array("required"=>"Please enter %s"));

      $this->form_validation->set_rules("package_validity", "package Duration", "required|numeric", array("required"=>"Please enter %s"));

     

      $this->form_validation->set_rules("status", "package status", "required", array("required"=>"Please enter %s"));

      if($this->form_validation->run() == FALSE){

        $data = array();

        header("Access-Control-Allow-Origin: *");

        $this->template->set('title', 'Create Package');

        $data['masteroption'] = $this->Admin_model->getFreeOptions();



        $this->template->load('admin_layout', 'contents' , 'Package/add', $data);

      }else{

        $data = array(

                      'package_title' => strip_tags($this->input->post('package_name')),

                      'package_price' => $this->input->post('package_price'),

                      'package_desc' => $this->input->post('package_desc'),

                      'package_validity' => $this->input->post('package_validity'),

                      'status' => $this->input->post('status'),

                    );      

        $insert = $this->Admin_model->createPackage($data);

       

        $this->session->set_flashdata("success", "Package Added Successfully.");

        redirect(base_url()."Admin/pacakge_list");

      }

   

  }

  

}

?>