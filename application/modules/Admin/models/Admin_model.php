<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model{
    function __construct() {
        $this->userTbl = 'users';
        $this->subsTbl = "subscribers";
        $this->catTbl = "otd_business_category";
        $this->eventTable = "otd_events";
        $this->revewTbl = "profile_review";
        $this->reportedRevewTbl = "reported_profile_review";
        $this->businessTbl = 'otd_business_details';
        $this->eventTbl = "otd_events";
        $this->usoptMaster = "otd_option_master";
        $this->usoptprice = "otd_option_price";
        $this->usOptTbl = "otd_user_option";
        $this->eventTypeTbl = "otd_event_type";
        $this->pytTbl = "otd_user_option_payment";        
        $this->labelTbl = "otd_label_suggestion_list";
        $this->langTbl = "otd_languages";
        $this->flTbl = "otd_business_follower";
        $this->bsopnTbl = "otd_business_openingtime";
        $this->bsPriceTbl = "otd_business_pricetable";
        $this->ntTbl = "otd_notifications";
        $this->rcUTble = "otd_recommend_merchant";
        $this->usBsCTTbl = "otd_user_business_category";
        $this->msgTbl = "otd_user_messaging";
        $this->usPhTbl = "otd_user_phone";
        $this->phtTbl = "otd_user_photogallery";
        $this->pkgTbl = "otd_package_options";
        $this->emailTpTbl = "otd_email_templates";
        $this->notiTbl = "otd_notifications";
        $this->bsclaimTbl = "otd_claimed_requests";
        $this->upldDocTbl = "otd_user_purchasedoc";
        $this->docTbl = "otd_contractdoc";
    }

    /*
    * get rows from user option table
    */
    function getUserOptionRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usOptTbl);
        $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id", "left");
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        $this->db->join($this->eventTbl, "$this->eventTbl.event_id = $this->usOptTbl.opt_event_id", "left");
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usOptTbl.opt_option_id");
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_user_option_id",$params)){
            $this->db->where('opt_user_option_id',$params['opt_user_option_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }


     /*
    * get rows from user option table
    */
    function getFormOptionRows($frm_opt_id){
        $sql = "SELECT bs.bs_name, bs.bs_comp_number, fo.*, om.* FROM `otd_formoption` `fo` LEFT JOIN `otd_option_master` `om` ON om.`opt_id` = fo.`opt_id` left join otd_business_details bs on bs.bs_user = fo.user_id where fo.frm_option_id =$frm_opt_id and om.opt_status != 2";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->row_array();
        }
        else
        {
            return false;
        }
    }


    /*
    * get master option listing
    */
    function masterOptionList($type = NULL){
        if(!empty($type))
            $this->db->where("opt_type",$type);
        $this->db->where("opt_status",1);
         $this->db->limit(4,0);
        


        return $this->db->get($this->usoptMaster)->result_array();
    }

    function masterOptionListfeture($type = NULL){
        if(!empty($type))
            $this->db->where("opt_type",$type);
        $this->db->where("opt_status",1);
         $this->db->limit(4,4);
        


        return $this->db->get($this->usoptMaster)->result_array();
    }

    function masterOptionListview($type = NULL){
        if(!empty($type))
            $this->db->where("opt_type",$type);
        $this->db->where("opt_status",1);
         
        


        return $this->db->get($this->usoptMaster)->result_array();
    }

    function getFeatureOptions() {
        $this->db->where("opt_type",2);
        $this->db->where("opt_status",1);
        return $this->db->get($this->usoptMaster)->result_array();
    }

    function getFreeOptions() {
        $this->db->where("opt_type",1);
        $this->db->where("opt_status",1);
        return $this->db->get($this->usoptMaster)->result_array();
    }

    /*
    * remove option from table
    */
    function removeOption($args = NULL){
        if(!empty($args)){
            $option = $this->getUserOptionRows(array("opt_user_option_id"=>$args));            
            if(!empty($option)){
                if($option['opt_option_id'] == 7 || $option['opt_option_id'] == 8){
                    $check = $this->updateEventStatus(array("event_status"=>3), array("event_id"=>$option['opt_event_id']));
                    if($check){
                        $userData = array("opt_option_status"=>2);
                        $rec = $this->update($userData, $args, "otd_user_option");
                        if($rec)
                            return true;
                        else
                            return false;
                    }else{
                        return false;
                    }
                }else{
                    $userData = array("opt_option_status"=>2);
                    $rec = $this->update($userData, $args, "otd_user_option");
                    if($rec)
                        return true;
                    else
                        return false;
                }
            }else{
                return false;
            }
        }
    }

    function updateEventStatus($params = array(), $where = array()){
        $this->db->update($this->eventTbl, $params, $where);
        if($this->db->affected_rows() > 0)
            return true;
        else 
            return false;
    }
    
    /*
     * get rows from the users table
     */
    function getRows($params = array()){
        $chk_email=0;
        $this->db->select('*');
        $this->db->from($this->userTbl);
        //fetch data by conditions
        $this->db->where("status !=", 3);
        $this->db->order_by("user_company_name", "ASC");
        if(array_key_exists("conditions",$params)){       
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);              
            }
        }
        
        if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
               
                if($key == "user_signup_method" && $value == "Regular")
                {
                    $chk_email = 1;
                    $this->db->or_where("user_email !=",  "");        
                }
                else if($key == "user_signup_method" && $value == "Imported")
                {
                    $chk_email = 4;
                    $this->db->like($key,$value);
                    
                    $this->db->where("user_signup_method",  "Imported"); 
                    $this->db->or_where("user_email",  "");         

                }
                else
                {
                    $this->db->like($key,$value);
                }
            }
            //$this->db->like($params['filter']);
        }

        // if($chk_email == 1)
        // {
           
        // }
        // if(array_key_exists("sorting",$params)){
        //     foreach($params['sorting'] as $key => $value) {
        //         $this->db->order_by($key, $value);
        //         // $this->session->set_userdata('order_by',$value);
        //     }
        // }

        //$this->db->where('user_type !=', 'Admin');
        // $order_mode = $this->session->userdata('order_mode');        
        // if($order_mode == 'ASC')
        // {
        //     $order_mode = "DESC";
        //     $this->session->set_userdata('order_mode',$order_mode);
        // }
        // else
        // {
        //   $order_mode = "ASC";   
        //     $this->session->set_userdata('order_mode',$order_mode);
        // }

        $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {
                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }
        else
        {
            $this->db->order_by('created', 'DESC');
        }

        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        return $result;
    }

public function record_count($params = array(), $tbl="users")
{

      $this->db->select('*');
        $this->db->from($tbl);

        if($tbl == "otd_events")
        {
                    $this->db->where("event_status !=", 3);
        }
        else if($tbl == "otd_advertisement")
        {
            $this->db->where("add_status !=", 3);
        }
        else if($tbl == "otd_business_category")
        {
            $this->db->where("cat_status !=", 3);
        }
        else if($tbl == "reported_profile_review")
        {
                    $this->db->where("status !=", "");
        }
        else
        {
                    $this->db->where("status !=", 3);
        
        }

        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
               // $this->db->like($key,$value);
                if($key == "user_signup_method" && $value == "Regular")
                {
                    $chk_email = 1;
                    $this->db->or_where("user_email !=",  "");        
                }
                else if($key == "user_signup_method" && $value == "Imported")
                {
                    $chk_email = 4;
                    $this->db->like($key,$value);
                    
                    $this->db->where("user_signup_method",  "Imported"); 
                    $this->db->or_where("user_email",  "");         

                }
                
                else
                {
                    $this->db->like($key,$value);
                }

            }
        }
       
            $query = $this->db->get();

            return $query->num_rows();
        }

 

/*
     * get rows from the users table
     */
    function getRowsPaged($params = array()){

      $this->db->select('*');
        $this->db->from($this->userTbl);
        $this->db->where("status !=", 3);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
       $order_mode = $this->session->userdata('order_mode');
        
                if($order_mode == 'ASC')
                {
                    $order_mode = "DESC";
                    $this->session->set_userdata('order_mode',$order_mode);
                }
                else
                {
                  $order_mode = "ASC";   
                    $this->session->set_userdata('order_mode',$order_mode);
                }

          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }


         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
               // $this->db->where($key,$value);
                if($key == "user_signup_method" && $value == "Regular")
                {
                    $chk_email = 1;
                    $this->db->or_where("user_email !=",  "");        
                }
                else if($key == "user_signup_method" && $value == "Imported")
                {
                    $chk_email = 4;
                    $this->db->like($key,$value);
                    
                    $this->db->where("user_signup_method",  "Imported"); 
                    $this->db->or_where("user_email",  "");         

                }
                
                else
                {
                    $this->db->like($key,$value);
                }
            }
        }



        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
           
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }

            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

  // echo $this->db->last_query();
        return $result;
    }

function getEventRows($params = array())
{
        $this->db->select('*');
        $this->db->from($this->eventTable);
        $this->db->join($this->eventTypeTbl, "$this->eventTypeTbl.evt_type_id =$this->eventTable.event_type_id");
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->eventTable.created_by_user");
        $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_event_id = $this->eventTable.event_id");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user =$this->userTbl.user_id", "left");
        if(!empty($this->session->userdata("user_email"))){
            $this->db->where("user_email", $this->session->userdata("user_email"));
        }
        if(!empty($this->session->userdata("eventtype"))){
            $this->db->where("event_type_id", $this->session->userdata("eventtype"));
        }
        if(!empty($this->session->userdata("publishstart"))){
            $this->db->where("event_publish_start", $this->session->userdata("publishstart"));
        }
        if(!empty($this->session->userdata("eventstart"))){
            $this->db->where('DATE(event_start_date)', $this->session->userdata("eventstart"));
        }
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        $order_mode = "DESC";
          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("event_id",$params)){
            $this->db->where('event_id',$params['event_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{  
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
       // echo $this->db->last_query();
       // die();
         return $result;

}

    /*
    * Get event type rows
    */
    function getEventTypeRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->eventTypeTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("evt_type_id",$params)){
            $this->db->where('evt_type_id',$params['evt_type_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{  
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert / Update into event table
    */
    function insertEventType($para = array()){        
        $this->db->insert($this->eventTypeTbl, $para);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows() > 0)
            return $insert_id;
        else
            return false;        
    }

    /*
    * delete event type and update event type null in EVents
    */
    function deleteeventType($id){
        $this->db->delete($this->eventTypeTbl, array("evt_type_id"=>$id));
        if($this->db->affected_rows() > 0){
            $this->db->update($this->eventTbl, array("event_type_id"=>0), array("event_type_id"=>$id));
        }
    }


 /*
     * get rows from the users table
     */
    function getSubsRows($params = array()){
      $this->db->select('*');
        $this->db->from($this->subsTbl);
         //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        $this->db->where('status !=', 3);

         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        
          $order_mode = "DESC";
          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        //$this->db->where("status !=", "3");
        if(array_key_exists("subs_id",$params)){
            $this->db->where('subs_id',$params['subs_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{  
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

         return $result;
    }



 /*
     * get rows from the users table
     */
    function getCatRows($params = array()){
      $this->db->select('*');
        $this->db->from($this->catTbl);
         //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
          $order_mode = "DESC";
          if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

              $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        //$this->db->where("status !=", "3");
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{  
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        echo $this->db->last_query();
        return $result;
    }


 function downloadRows($params = array()){
        
 $this->db->select('user_id, user_firstname, user_lastname, user_email, user_type, user_address, user_gender, created, last_login, user_phone');

        $this->db->from($this->userTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


         if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


        // $this->db->where('user_type !=', 'Admin');        
$order_mode = $this->session->userdata('order_mode');
$value = $this->session->userdata('order_by');
$this->db->order_by($value, $order_mode);
        


        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            
            

            $query = $this->db->get();

        }
        return $query;
    }


function downloadRowsSubs($params = array()){
        
        //$this->db->select('*');
        $this->db->select('subs_id as id,user_email as email,subscription_date');
        $this->db->from($this->subsTbl);
        $this->db->where("status !=", "3");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


        if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }


// $this->db->where('user_type !=', 'Admin');        
$order_mode = $this->session->userdata('order_mode');
$value = $this->session->userdata('order_by');
$this->db->order_by($value, $order_mode);
        
        if(array_key_exists("subs_id",$params)){
            $this->db->where('subs_id',$params['subs_id']);
            $query = $this->db->get();
        }else{
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
        }
        return $query;
    }


     function getPersonalUsers($params = array()){
        $this->db->select('user_id, user_firstname, user_lastname, user_email, user_type, user_address, user_gender, created, last_login');
        $this->db->from($this->userTbl);
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
         $this->db->where('user_type !=', 'Admin');   
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

   
        return $result;
    }
    
    /*
     * Insert user information
     */
    public function insert($data = array()) {
        if(!array_key_exists("created", $data)){
            $data['created'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified", $data)){
            $data['modified'] = date("Y-m-d H:i:s");
        }
        $insert = $this->db->insert($this->userTbl, $data);
        
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }

    public function insertCat($data = array(), $tbl) {
        
        $insert = $this->db->insert($tbl, $data);
        
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
    }

    public function update($data = array(), $id=0, $tbl = "users") {

if($tbl == "users")
{
       $this->db->where('user_id', $id);
}

else if($tbl == "profile_review")
{
       $this->db->where('vote_id', $id);
}
else if($tbl == "otd_user_option")
{
       $this->db->where('opt_user_option_id', $id);
}else if($tbl == "otd_event_type"){
    $this->db->where('evt_type_id', $id);
}
else
{
       $this->db->where('subs_id', $id);
}    
        $x = $this->db->update($tbl, $data);
        //echo  $this->db->last_query();
        return $x;


    }

     

 public function delete($data = array(), $id=0) {

       $this->db->where('user_id', $id);
        return $this->db->update($this->userTbl, $data);

    }

    public function getUserEmail($uid=0)
    {
         $this->db->select('user_firstname, user_lastname, user_email');    
        $this->db->from($this->userTbl);  
        $this->db->where('user_id', $uid);
          $query = $this->db->get();
         // echo $this->db->last_query();
          return $query->row_array();
    }

    function getBusinessCategories(){
        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        return $query;
    }

    function getCategoriesAndFilters($user, $type= 1){       
       $query = $this->db->select("GROUP_CONCAT(DISTINCT CONCAT(category_id)) as category")
                         ->group_by("user_id",$user)
                         ->get_where($this->usBsCTTbl, array("user_id"=>$user, "type"=>$type));
       if($query->num_rows() > 0 ){
           foreach($query->result() as $res){ }
           return $res->category;
       }
       else{
           return "";
       }
    }



    function site_contents($args){
        $query = $this->db->get_where("site_contents", array("content_id"=>$args));
        foreach($query->result() as $q){
            $data = $q->content;
        }
        return $data;
    }
    

 function getEventsAll($limit=5, $offset=0)
{
$lmtqry = " limit ". $limit. " offset ". $offset;
$sql = "SELECT e.*, u.user_company_name `created_by_user_name` from otd_events e left join users u on u.user_id = e.created_by_user WHERE e.event_status = 1 ". $lmtqry;

$query = $this->db->query($sql);
$REC = $query->num_rows();
if($query->num_rows()>0)
{
    return $query->result_array();
}
else
{
    return false;
}

}

/*
    * get rows from the review table
*/
   
    function getReportedReviewRows($params = array()){
        // $sql="select usr.user_firstname, usr.user_lastname, users.user_company_name, report.*, vv_ratings.vote_count from users join (select pr.*, rpr.total_reported from profile_review pr left join (SELECT voted_user_id, count(*)total_reported FROM `reported_profile_review` group by voted_user_id) as rpr on pr.voted_user_id = rpr.voted_user_id group by pr.voted_user_id ) as report on users.user_id = report.voted_user_id join users usr on usr.user_id = report.voter_user_id left join vv_ratings on vv_ratings.profile_id = report.voted_user_id where report.vote_status != 3 ORDER BY `user_company_name` ASC";
        // $query = $this->db->query($sql);
        $this->db->select("*, CONCAT(t2.user_firstname, ' ', t2.user_lastname) AS votername, t1.user_company_name AS votedcompany, COUNT(reported_id) as total_reported");
        $this->db->from($this->reportedRevewTbl);
        $this->db->join($this->revewTbl, "$this->revewTbl.vote_id = $this->reportedRevewTbl.vote_id");
        $this->db->join("$this->userTbl AS t1", "t1.user_id = $this->reportedRevewTbl.voted_user_id");
        $this->db->join("$this->userTbl AS t2", "t2.user_id = $this->reportedRevewTbl.voter_user_id");
        $this->db->group_by("$this->reportedRevewTbl.vote_id");
        $this->db->where(array("$this->revewTbl.vote_status !="=>3, "$this->revewTbl.status !="=>3));
        $query = $this->db->get();
        if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
           $result = $query->num_rows();                
        }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
           $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        }else{
           $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        }
        return $result;
   }


    function getReportedReviewDetails($id){
        $sql = "select CONCAT(usr.user_firstname, ' ', usr.user_lastname)reported_by, CONCAT(usr_rv.user_firstname, ' ', usr_rv.user_lastname)reviewed_by, report.* from (SELECT rpr.reported_by_user_id, rpr.vote_id, rpr.report_date, rpr.review_text, rpr.voter_user_id, rpr.voted_user_id FROM `reported_profile_review` rpr) report join users usr on usr.user_id = report.reported_by_user_id join users usr_rv on usr_rv.user_id = report.voter_user_id";
      

           $query = $this->db->query($sql);


         
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
         
       return $result;
   }

    /*
    * Insert / Update into event table
    */
    function insertEvent($opr = 1, $para = array(), $where = array()){
        if($opr == 1){ // insert testimonial
            $this->db->insert($this->eventTable, $para);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ // update existing testimonial
            $this->db->update($this->eventTable, $para, $where);
            return true;
        }
    }

    /*
    * Remove event via status
    */
    function removeEvent($id){
        $this->db->delete($this->usOptTbl, array("opt_event_id"=>$id));
        $check1 = $this->db->affected_rows();
        $this->db->delete($this->eventTable, array("event_id"=>$id));
        $check2 = $this->db->affected_rows();
        if($check1 && $check2)
            return true;
        else
            return false;
    }

    public function getOptionDetails($price_id = 0)
    {

        if($price_id == 0)
        {
        $sql = "SELECT optm.opt_id, optm.opt_name, optm.opt_description, optp.opt_pr_status, optp.opt_text, optp.opt_price_type, optp.opt_price, optp.opt_price_id, optm.opt_type,  optm.opt_status FROM `otd_option_master` optm left join `otd_option_price` optp on optm.opt_id = optp.opt_type_id where optm.opt_status != 3 ORDER BY optp.`opt_price_id` ASC";
        }
        else
        {
        $sql = "SELECT optm.opt_id, optm.opt_name, optm.opt_description, optp.opt_pr_status, optp.opt_text, optp.opt_price_type, optp.opt_price, optp.opt_price_id, optm.opt_type,  optm.opt_status FROM `otd_option_master` optm left join `otd_option_price` optp on optm.opt_id = optp.opt_type_id where optm.opt_status != 3 and optp.opt_price_id =".$price_id." ORDER BY optp.`opt_price_id` ASC";


        }

            
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

    public function getOptionDetailsAdmin()
    {

        
        $sql = "SELECT optm.opt_id, optm.opt_name, optm.opt_description,  optm.opt_type,  optm.opt_status , optm.duration FROM `otd_option_master` optm where optm.opt_status != 3";
      
       

            
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

    public function update_option($opt_price_id, $opt_price)
    {
        $sql= "update otd_option_price set opt_price = ". $opt_price . " where opt_price_id = ". $opt_price_id;
        $upd = $this->db->query($sql);
         if($this->db->affected_rows() > 0)
                return $upd;
            else
                return false;
    }

    public function updatePriceOption($params = array(), $where = array()){
        $this->db->update($this->usoptprice, $params, $where);
        return true;
    }

    public function insertPriceOption($params = array()){
        $this->db->insert($this->usoptprice, $params);
        return $this->db->insert_id();
    }


    public function getUserPerchase($params = array())
    {
        $user_email = $this->session->userdata("user_email");
        $option = $this->session->userdata("option");
        $expiry = $this->session->userdata("expiry");
        $where = array("opt_option_status!=" => 2);
        if(!empty($user_email))
            $where['user_email'] = $user_email;
        if(!empty($option))
            $where['opt_option_id'] = $option;
        if(!empty($expiry))
            $where['opt_option_end_date'] = $expiry;
        // $sql = "SELECT  optm.opt_name, ouo.*, vul.user_company_name FROM `otd_user_option` ouo left join vv_users_list vul on ouo.opt_user_id = vul.user_id left join `otd_option_master` optm on optm.opt_id = ouo.opt_option_id WHERE opt_option_status !=2";

        // $query = $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id")
        //                   ->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id")
        //                   ->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "Left")
        //                   ->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usOptTbl.opt_option_id")
        //                   ->where($where)
        //                   ->order_by("opt_user_option_id", "DESC")
        //                   ->get($this->usOptTbl);
        // // $query = $this->db->query($sql);
        // $result = ($query->num_rows() > 0)?$query->result_array():FALSE;


        $this->db->select("*");
        $this->db->from($this->usOptTbl);
        $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id");
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        $this->db->join($this->eventTbl, "$this->eventTbl.event_id = $this->usOptTbl.opt_event_id", "left");
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usOptTbl.opt_option_id");
        if(!empty($where))
            $this->db->where($where);
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_user_option_id",$params)){
            $this->db->where('opt_user_option_id',$params['opt_user_option_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function getUserAdvOptions()
    {
        $sql = "SELECT bs.bs_name, bs.bs_comp_number, fo.user_id, fo.frm_option_id, fo.opt_id, fo.Budget, fo.StartDate, fo.Description, om.opt_name, om.opt_description, fo.status FROM `otd_formoption` `fo` LEFT JOIN `otd_option_master` `om` ON om.`opt_id` = fo.`opt_id` left join otd_business_details bs on bs.bs_user = fo.user_id where om.opt_status != 2";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
    
    function getUserAdvOptionsDetails($frm_option_id)
    {
        $sql = "SELECT fo.*, om.opt_name, om.opt_description FROM `otd_formoption` `fo` LEFT JOIN `otd_option_master` `om` ON om.`opt_id` = fo.`opt_id` where  om.opt_status != 2 ";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
    

    /*
    * get rows from the testimonail table
    */
    function getReviewRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->revewTbl);
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->revewTbl.voter_user_id");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->revewTbl.voted_user_id");

        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("vote_id",$params)){
            $this->db->where('vote_id',$params['vote_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

     /*
    * update event
    */
    function updateUserEvent($param = array(), $where){
                  
            $this->db->update($this->eventTbl, $param, $where);
           
            return true;
       
    }
   
    function updateOption($params = array(), $where){
        $this->db->update("otd_option_master", $params, $where);
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;
    }

    function createOption($params = array()){
        if(!array_key_exists("opt_create_date", $params)){
            $params['opt_create_date'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("opt_last_update", $params)){
            $paramsdata['opt_last_update'] = date("Y-m-d H:i:s");
        }
        $insert =$this->db->insert("otd_option_master", $params);
        
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }

     
    }

    function getOptionTypeDetails($typeid)
    {
        $this->db->select('*');
        $this->db->from("otd_option_master");        
        $this->db->where('opt_type',$typeid);
        $query = $this->db->get();
         return $query->row_array();       

    }
    function getOptionTypeEditDetails($typeid)
    {
        $this->db->select('*');
        $this->db->from("otd_option_master");        
        $this->db->where('opt_id',$typeid);
        $query = $this->db->get();
      
        return $query->row_array();       

    }
     /*
    * Get all records from label suggestion tble
    */
    function getSuggestions(){
        $this->db->select('*');
        $this->db->from($this->labelTbl);        
        $this->db->where('label_id',1);
        $query = $this->db->get();
        return $query->row_array();        
    }

    /*
    * Get events type list
    */
    function getEventTypeList(){
       $ses = $this->session->userdata("user_id");
       if(!empty($ses)){
           return $this->db->order_by("evt_type", "ASC")->get_where($this->eventTypeTbl, array("evt_status"=>1))->result_array();
       }else{
           return false;
       }
   }


    /*
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
        
               $address = str_replace(" ", "+", $address);
        
               $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
               $json = json_decode($json);
        
               if((isset($json)) && ($json->{'status'} == "OK")){
                    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
                    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
               }else{
                    $lat = -25.274398; 
                    $long = 133.775136; 
               }
               
               $response = array('lat'=>$lat,'lng'=>$long);
               return $response;
        
            }

    function deleteUserFromSite($ids = array()){
        foreach($ids as $id){
            $this->db->delete($this->userTbl, array("user_id"=>$id));
            $this->db->delete($this->eventTable, array("created_by_user"=>$id));
            $this->db->delete($this->revewTbl, array("voted_user_id"=>$id));
            $this->db->delete($this->reportedRevewTbl, array("voted_user_id"=>$id));
            $this->db->delete($this->businessTbl, array("bs_user"=>$id));
            $query = $this->db->group_by("opt_tran_id")->get_where($this->usOptTbl, array("opt_user_id"=>$id))->result_array();
            if(!empty($query)){
                foreach($query as $pyt){
                    $this->db->delete($this->pytTbl, array("pyt_id"=>$pyt['opt_tran_id']));
                }
            }
            $this->db->delete($this->usOptTbl, array("opt_user_id"=>$id));
            $this->db->delete($this->flTbl, array("followed_user_id"=>$id));
            $this->db->delete($this->flTbl, array("follower_user_id"=>$id));
            $this->db->delete($this->bsopnTbl, array("op_user"=>$id));
            $this->db->delete($this->bsPriceTbl, array("pr_user"=>$id));
            $this->db->delete($this->ntTbl, array("nt_by"=>$id));
            $this->db->delete($this->ntTbl, array("nt_to"=>$id));
            // $this->db->delete($this->rcUTble, array(""));
            $this->db->delete($this->usBsCTTbl, array("user_id"=>$id));
            $this->db->delete($this->msgTbl, array("msg_sender"=>$id));
            $this->db->delete($this->msgTbl, array("msg_reciever"=>$id));
            $this->db->delete($this->usPhTbl, array("phn_user"=>$id));
            $this->db->delete($this->phtTbl, array("ph_user"=>$id));
        }
    }

    function deletePersonalUserFromSite($id){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses) && !empty($id)){
            $this->db->delete($this->userTbl, array("user_id"=>$id));
            $this->db->delete($this->revewTbl, array("voter_user_id"=>$id));
            $this->db->delete($this->reportedRevewTbl, array("voter_user_id"=>$id));
            $this->db->delete($this->flTbl, array("followed_user_id"=>$id));
            $this->db->delete($this->flTbl, array("follower_user_id"=>$id));
            $this->db->delete($this->ntTbl, array("nt_by"=>$id));
            $this->db->delete($this->ntTbl, array("nt_to"=>$id));
            $this->db->delete($this->msgTbl, array("msg_sender"=>$id));
            $this->db->delete($this->msgTbl, array("msg_reciever"=>$id));
            return true;
        }else{
            return false;
        }
        
    }

    function getOptionPriceRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usoptprice);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usoptprice.opt_type_id");
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_price_id",$params)){
            $this->db->where('opt_price_id',$params['opt_price_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function getPackageRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usoptMaster);
        $this->db->join($this->usoptprice, "$this->usoptprice.opt_type_id = $this->usoptMaster.opt_id", "left");
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function getPackageOptionRows($params = array()){
        $this->db->select("*");        
        $this->db->from($this->pkgTbl);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->pkgTbl.pkg_inc_optid", "left");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("pkg_id",$params)){
            $this->db->where('pkg_id',$params['pkg_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function getPurchasedPackageOptionRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usOptTbl);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = opt_option_id");        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function insertPackageOption($params = array()){
        $this->db->insert($this->pkgTbl, $params);
        return $this->db->insert_id();
    }

    function updatePackageOption($params = array(), $where =array()){        
        $this->db->update($this->pkgTbl, $params, $where);
        return true;
    }

    function deletePackageOption($where =array()){        
        $this->db->delete($this->pkgTbl, $where);
        return true;
    }

    function insertTransaction($params = array()){
        $this->db->insert($this->pytTbl, $params);
        return $this->db->insert_id();
    }

    function insertUserOption($params = array()){
        $this->db->insert($this->usOptTbl, $params);
        return $this->db->insert_id();
    }

    function getSelectedMasterOptions($params = array()){
        $this->db->select($params['select']);
        $this->db->from($this->usoptMaster);
        if(array_key_exists("wherenotin",$params)){
            $this->db->where_not_in($params['wherenotin']);
        }else{
            $this->db->where_in("opt_id", $params['wherein']);
        }
        return $this->db->get()->result_array();
    }

    function getMasterOptionRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->usoptMaster);
        //fetch data by conditions        
        if(array_key_exists("conditions",$params)){       
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);              
            }
        }
        
        if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->like($key,$value);
            }
            //$this->db->like($params['filter']);
        }
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {
                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }        

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        return $result;
    }

    function insertUserOptionInBulk($params = array()){
        foreach($params as $par){
            if($par['opt_option_id'] == 7 || $par['opt_option_id'] == 8 ){
                $arr1 = array("event_status"=>0,
                              "event_is_free"=>0,
                              "created_by_user"=>$par['opt_user_id'],
                            );
                if($par['opt_option_id'] == 8)
                    $arr1['event_goodplace'] = 1;
                $par['opt_event_id'] = $this->insertEvent(1, $arr1);
            }
            $this->db->insert($this->usOptTbl, $par);
        }
        // return $this->db->insert_id();
        return true;
    }

    /*
    * get rows for business opening time table
    */
    function getEmailTemplateRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->emailTpTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("tmplate_id",$params)){
            $this->db->where('tmplate_id',$params['tmplate_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function addUserNotification($to, $nt_type){
        $this->db->insert($this->notiTbl, array("nt_by"=>26, "nt_to"=>$to, "nt_type"=>$nt_type, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s")));
        return $this->db->insert_id();
    }


    function getClaimedRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->bsclaimTbl);
        $this->db->join($this->userTbl, "$this->userTbl.user_company_number = $this->bsclaimTbl.claim_user_company_number");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id");
        //fetch data by conditions        
        if(array_key_exists("conditions",$params)){       
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);              
            }
        }
        
        if(array_key_exists("filter",$params)){
            foreach ($params['filter'] as $key => $value) {
                $this->db->like($key,$value);
            }
            //$this->db->like($params['filter']);
        }
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {
                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }        

        if(array_key_exists("claim_request_id",$params)){
            $this->db->where('claim_request_id',$params['claim_request_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        return $result;
    }

    function deleteClaimedProfileRow($params = array()){
        foreach($params as $pr){
            $this->db->delete($this->bsclaimTbl, array("claim_request_id"=>$pr));
        }
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function deleteAllClaimedProfileRow(){
        $this->db->truncate($this->bsclaimTbl);
        return true;
    }

    function deleteSubscriber($params){
        //foreach($params as $pr){
            $this->db->delete($this->subsTbl, array("subs_id"=>$params));
        //}
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    function deleteAllSubscriberRows(){
        $this->db->truncate($this->subsTbl);
        return true;
    }


    /*
    * get rows from the langauges table
    */
    function getLanguageRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->langTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
        }

        if(array_key_exists("lang_id",$params)){
            $this->db->where('lang_id',$params['lang_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    public function insertTable($mode = 1, $table, $params=array(), $where = array()){
        if($mode == 1){
            $this->db->insert($table, $params);
            return $this->db->insert_id();
        }else{
            $this->db->update($table, $params, $where);
            if($this->db->affected_rows() > 0){
                return true;
            }else{
                return false;
            }
        }
    }

    public function getDocumentsList(){
        return $this->db->get_where($this->docTbl, array("ct_status"=>1))->result_array();
    }

    /*
    * Get purchase document rows
    */
    public function getPurchaseDocumentRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->upldDocTbl);        
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->upldDocTbl.dc_userid");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        $this->db->join($this->docTbl, "$this->docTbl.ct_id = $this->upldDocTbl.dc_docid");
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("dc_id",$params)){
            $this->db->where('dc_id',$params['dc_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * remove content from table
    */
    public function removeTable($table, $where = array()){
        $this->db->delete($table, $where);
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }
    public function getPackageDetailsAdmin(){
        $sql = "SELECT optm.id, optm.package_title, optm.package_validity,  optm.package_price,  optm.status FROM `bdc_package_listing` optm where optm.status != 3";
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }
    function updatePackage($params = array(), $where){
        $this->db->update("bdc_package_listing", $params, $where);
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;
    }
    function getPackageEditDetails($typeid)
    {
        $this->db->select('*');
        $this->db->from("bdc_package_listing");        
        $this->db->where('id',$typeid);
        $query = $this->db->get();
      
        return $query->row_array();       

    }
    function createPackage($params = array()){
        if(!array_key_exists("created", $params)){
            $params['created'] = date("Y-m-d H:i:s");
        }
        /*if(!array_key_exists("opt_last_update", $params)){
            $paramsdata['opt_last_update'] = date("Y-m-d H:i:s");
        }*/
        $insert =$this->db->insert("bdc_package_listing", $params);
        
        if($insert){
            return $this->db->insert_id();;
        }else{
            return false;
        }
     
    }
     function getPackageEditDetails1($typeid)
    {
        $this->db->select('*');
        $this->db->from("otd_option_master");        
        $this->db->where('opt_id',$typeid);
        $query = $this->db->get();
      
        return $query->row_array();       

    }

}