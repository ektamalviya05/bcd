 <span id="showreviewdel" data-dismiss="modal" data-toggle="modal" data-target="#myModalalReviewDel"></span>

<div class="dash-counter users-main">
                        <h2>Review List</h2>

                        <div class="user-table table-responsive">
                            <table class="table table-striped table-bordered">
                              <tbody>
                                <tr></tr>
                               


<?php
//print_r($reviewlist);

$bur = base_url();
echo "<tr>";
echo "<th>Reviewed By</th>";
echo "<th>Profile</th>";
echo "<th>Reviewed On</th>";
echo "<th>Rating</th>";
echo "<th>Review</th>";
echo "<th>Report Count</th>";
echo "<th>Action</th>";
echo "</tr>";

if($record_found>0)
{
foreach ($reviewlist as $rows) 
{
echo "<tr>";
?>

<td><?php echo $rows['user_firstname'].' '.$rows['user_lastname']; ?></td> 
<td><?php echo $rows['user_company_name']; ?> </td> 
<td><?php echo $rows['review_date']; ?></td>
<td><?php echo $rows['vote_count']; ?></td> 
<td><?php echo $rows['review_text']; ?></td>
<td><?php echo $rows['total_reported']; ?></td>
<td><a href='javascript:deletereview(<?php echo $rows['vote_id']; ?>);' data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="<?php echo $rows['vote_id']; ?>">Delete</a>
</p>


<?php

echo "</tr>";

}
}
else
{
  echo "<tr><td colspan='7'>No Record Found</td></tr>";
}
//echo "</table>";

?>


</tbody>
</table>
<?php


if(isset($links) && !empty($links))
{
  echo $links;
}

?>

</div>
</div>

<div class="modal fade" id="myModalalReviewDel" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Review Deleted</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          You have deleted the review.
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<script>


 function deletereview(review_id)
  {
     // $("#showreviewemail").trigger("click");
       	var data = "";
       	data = 'vote_id=' + review_id ;
     	$.ajax({

                type: "POST",
                data: data,
                url: "<?php echo base_url("Admin/delreview"); ?>", //CALLBACK FILE
                success: function (e) {
                	$("#showreviewdel").trigger("click");
                },
                error: function (e) {
                    alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
                    //$("#showreviewemail").trigger("click");
                }
            });

  }

</script>