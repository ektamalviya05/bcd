<div class="import-create dash-counter">
 <h2>Categories</h2>  
 <form class="form-horizontal" action="" method="post" name="upload_csv" enctype="multipart/form-data">
<fieldset>

 <span>(*) Required Fields</span>                   
    <legend>Import <?php echo $import_type; ?></legend>
        <div class="form-group">
            <label class="col-md-4 control-label" for="filebutton">Select File</label>
                <div class="col-md-4">
                    <input type="file" name="userfile" id="userfile" class="input-large">
                </div>
        </div>

        <!-- Button -->
        <div class="form-group">
            <label class="col-md-4 control-label" for="singlebutton">Import data</label>
                <div class="col-md-4">
                    <input type="submit" id="submit" name="ImportSubmit" id="ImportSubmit" class="btn btn-primary button-loading" data-loading-text="Loading..." value="Import">
                </div>
            </div>
</fieldset>
</form>
</div>

<div class="import-create dash-counter">
    <h2>Categories List</h2>
                        <div class="user-table table-responsive">
                           <table class="table table-striped table-bordered">
                             <tbody>
                               <tr></tr>
                                                              
<?php
echo "<tr>";
echo "<th>ID </a></th>";
echo "<th>Category Name</a></th>";
echo "<th>Description</a></th>";
echo "<th>Status</a></th>";
echo "</tr>";

if($cat_count>0)
{
foreach ($catlist as $rows) 
{
$stat= 1;
echo "<tr>";
echo "<td>".$rows['cat_id']."</td>";
echo "<td>".$rows['cat_name']."</td>";
echo "<td>".$rows['cat_description']."</td>";
echo "<td>".$stat."</td>";
echo "</td>";
echo "</tr>";
}
}
else
{
  echo "<tr><td colspan='7'>No Record Found</td></tr>";
}

?>


                             </tbody>
                           </table>


                        </div>
                        <div class="read-more">
                       <?php echo $links ;  ?>
                        </div>
                    </div>