<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<!--  First Top Row   -->
                    <div class="dash-counter">
                        <h2>Analytics</h2>
                        <div class="row">
                          <div class="col-md-3 col-sm-6">
                            <a href="<?php echo base_url().'Admin/user_list_pro'; ?>"> 
                              <div class="dc-sub">
                                  <div class="dc-sub-ico busi-ico">
                                      <i class="fa fa-user-o" aria-hidden="true"></i>
                                  </div>
                                  <div class="dc-sub-txt">
                                    <h3><?php echo $user_count; ?></h3>

                                      <p>Users</p>
                                  </div>
                              </div>
                            </a>
                          </div>
                          <div class="col-md-3 col-sm-6">
                            <a href="<?php echo base_url().'Category'; ?>"> 
                              <div class="dc-sub">
                                  <div class="dc-sub-ico busi-ico">
                                      <i class="fa fa-gavel" aria-hidden="true"></i>
                                  </div>
                                  <div class="dc-sub-txt">
                                       <h3><?php echo $category_count; ?></h3>
                                      <p> Category</p>
                                  </div>
                              </div>
                            </a>
                          </div>
                         <!--  <div class="col-md-3 col-sm-6">
                                      <a href="<?php echo base_url().'Content/manualAdvertisement'; ?>"> 
                                <div class="dc-sub">
                                    <div class="dc-sub-ico event-ico">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
<h3><?php echo $event_count; ?></h3>
                                        <p>Advertisements</p>
                                    </div>
                                </div>
                                </a>
                            </div> -->
                          <!-- <div class="col-md-3 col-sm-6">
                            <a href="<?php echo base_url().'Admin/option_list'; ?>"> 
                              <div class="dc-sub">
                                <div class="dc-sub-ico">
                                  <i class="fa fa-user-o" aria-hidden="true"></i>
                                </div>
                                <div class="dc-sub-txt">
                                  <h3><?php echo $user_count; ?></h3>

                                  <p>Packages</p>
                                </div>
                              </div>
                            </a>
                          </div> -->                            
                           <div class="col-md-3 col-sm-6">
                                  <a href="<?php echo base_url().'Admin/subs_list'; ?>"> 
                                <div class="dc-sub">
                                    <div class="dc-sub-ico new-let-ico">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
<h3><?php echo $subs_count; ?></h3>
                                        <p>Communication</p>
                                    </div>
                                </div>
                                </a>
                            </div>
                            
                            <?php
                            if($purchased_package_count > 0){
                            ?>
                              <div class="col-md-3 col-sm-6">
                                <a href="<?php echo base_url().'Admin/purchase_list'; ?>"> 
                                  <div class="dc-sub">
                                      <div class="dc-sub-ico event-ico">
                                          <i class="fa fa-dropbox" aria-hidden="true"></i>
                                      </div>
                                      <div class="dc-sub-txt">
<!--                                          <h3><?php echo $purchased_package_count; ?></h3>-->
                                          <p>Purchased Packages</p>
                                      </div>
                                  </div>
                                </a>
                              </div>
                            <?php
                            }
                            if($review_count > 0){
                            ?>
                            <div class="col-md-3 col-sm-6">
                                  <a href="<?php echo base_url().'Admin/reviewListing'; ?>"> 
                                <div class="dc-sub">
                                    <div class="dc-sub-ico new-let-ico">
                                        <i class="fa fa-star-half-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
<!--                                        <h3><?php echo $review_count; ?></h3>-->
                                        <p>Reviews</p>
                                    </div>
                                </div>
                                </a>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                    </div>

                    
                    
    <!--  Second Top Row   -->
<!--                    <div class="import-create dash-counter">
                        <h2>Import & Create</h2>
                        <div class="row">
                            <div class="col-md-3 col-sm-6">
                                <div class="dc-sub">
                                    <a href="<?php echo base_url('Category/import_businesscsv'); ?>">
                                        <div class="dc-sub-ico">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt pu-txt">
                                            <h3>Import</h3>
                                            <p>Business Users</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="dc-sub">
                                    <a href="<?php echo base_url('Category/import_csv'); ?>">
                                        <div class="dc-sub-ico busi-ico">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt bu-txt">
                                            <h3>Import</h3>
                                            <p>Categories</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="dc-sub">
                                    <a href="<?php echo base_url();?>Admin/createbusiness">
                                        <div class="dc-sub-ico new-let-ico">
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt nl-txt">
                                            <h3>Create</h3>
                                            <p>Business User</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-6">
                                <div class="dc-sub">
                                    <a href="javascript:;" data-toggle="modal" data-target="#creteevent">
                                        <div class="dc-sub-ico event-ico">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt et-txt">
                                            <h3>Create</h3>
                                            <p>Event</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>-->


                      <!--  Main Containt  -->
<!--                    <div class="dash-counter users-main">
                        <h2>Users</h2>
                        <div class="user-table table-responsive">
                            <table class="table table-striped table-bordered">
                              <tbody>
                               <tr></tr>-->
<?php
//echo "<tr>";
//echo "<th>First Name</a></th>";
//echo "<th>Last Name</a></th>";
//echo "<th>Email</a></th>";
//echo "<th>Type</a></th>";
//echo "<th>Signup Time</a></th>";
//echo "<th>Last Login</a></th>";
//echo "<th>Status</a></th>";
//echo "</tr>";
//
//if($user_count>0)
//{
//foreach ($userlist as $rows) 
//{
//$stat= $rows['is_online'] == 1? 'Online': 'Offline';
//echo "<tr>";
//echo "<td>".$rows['user_firstname']."</td>";
//echo "<td>".$rows['user_lastname']."</td>";
//echo "<td>".$rows['user_email']."</td>";
//echo "<td>".$rows['user_type']."</td>";
//echo "<td>".$rows['created']."</td>";
//echo "<td>".$rows['last_login']."</td>";
//echo "<td>".$stat."</td>";
//echo "</td>";
//echo "</tr>";
//}
//}
//else
//{
//  echo "<tr><td colspan='7'>No Record Found</td></tr>";
//}
//echo "</table>";
?>
<!--</tbody>
</table>

</div>-->
<!--                        <div class="read-more">
                        <a href="<?php echo base_url().'Admin/user_list'; ?>">
                            View more
                        </a>
                        </div>
                    </div>
                    <div class="dash-counter profile-main">
                        <h2>Profiles</h2>
                        <div class="user-table table-responsive">
                           <table class="table table-striped table-bordered">
                             <tbody>
                               <tr></tr>-->
                                                              
<?php
//echo "<tr>";
//echo "<th>First Name</a></th>";
//echo "<th>Last Name</a></th>";
//echo "<th>Email</a></th>";
//echo "<th>Type</a></th>";
//echo "<th>Signup Time</a></th>";
//echo "<th>Last Login</a></th>";
//echo "<th>Status</a></th>";
//echo "</tr>";
//
//if($profile_count>0)
//{
//foreach ($profilelist as $rows) 
//{
//$stat= $rows['is_online'] == 1? 'Online': 'Offline';
//echo "<tr>";
//echo "<td>".$rows['user_firstname']."</td>";
//echo "<td>".$rows['user_lastname']."</td>";
//echo "<td>".$rows['user_email']."</td>";
//echo "<td>".$rows['user_type']."</td>";
//echo "<td>".$rows['created']."</td>";
//echo "<td>".$rows['last_login']."</td>";
//echo "<td>".$stat."</td>";
//echo "</td>";
//echo "</tr>";
//}
//}
//else
//{
//  echo "<tr><td colspan='7'>No Record Found</td></tr>";
//}

?>


<!--                             </tbody>
                           </table>


                        </div>-->
<!--                        <div class="read-more">
                        <a href="<?php echo base_url().'Admin/user_list_pro'; ?>">
                            View more
                        </a>
                        </div>
                    </div>
                    <div class="dash-counter news-let-evnts">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="news-let-dash">
                                    <h2>News Letter Subscribers</h2>

                                    <div class="user-table table-responsive">
                                       <table id="subs" class="table table-striped table-bordered">
                                         <tbody>
                                           <tr></tr>-->
                                           

<?php
//echo "<table id='subs' class='table table-striped table-bordered'><tr>";

//$bur = base_url();
//
//echo "<tr>";
//echo "<th>ID</a></th>";
//echo "<th>Email</a></th>";
//echo "<th>Postal Code</a></th>";
//echo "</tr>";
//
//if($subs_count>0)
//{
//foreach ($subslist as $rows) 
//{
//echo "<tr>";
//echo "<td>".$rows['subs_id']."</td>";
//echo "<td>".$rows['user_email']."</td>";
//echo "<td>".$rows['user_postcode']."</td>";
//echo "</tr>";
//}
//}
//else
//{
//  echo "<tr><td colspan='7'>No Record Found</td></tr>";
//}
//echo "</table>";

?>

<!--                                         </tbody>
                                       </table>



                                    </div>
                                    <div class="read-more">
                                       <a href="<?php echo base_url().'Admin/subs_list'; ?>">
                                            View more
                                        </a>
                                    </div>
                                </div>
                            </div>-->
<!--                            <div class="col-md-6">
                                <div class="news-let-dash">
                                    <h2>Events</h2>

                                    <div class="user-table table-responsive">
                                       <table class="table-striped table-bordered table">
                                           <tbody>
                                               <tr>
                                                   <th>
                                                       Event Name
                                                   </th>
                                                   <th>
                                                       Date Posted
                                                   </th>
                                                   <th>
                                                       User
                                                   </th>
                                                 
                                               </tr>-->
                                               <?php

//                                               foreach ($events as $event) {
//                                                echo "<tr>";
//                                                echo "<td>".$event['event_name']."</td>";
//                                                echo "<td>".$event['created_on']."</td>";
//                                                echo "<td>".$event['created_by_user_name']."</td>";
//                                                echo "</tr>";
//                                                 # code...
//                                               }

                                               ?>
                                               <!-- <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   
                                               </tr> -->
<!--                                           </tbody>
                                       </table>



                                    </div>
                                    <div class="read-more">
                                        <a href="<?php echo base_url().'Admin/event_list'; ?>">
                                            View more
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>-->




















<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Deleted");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDel').modal();
}  
</script>

  <?php
}
?>

<?php
if(isset($activated) && trim($activated) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Activated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaAct').modal();
}  
</script>

  <?php
}
else if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
 //alert( "Deactivated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>

  <?php
}
?>

