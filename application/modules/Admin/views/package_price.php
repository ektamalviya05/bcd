<div class="dash-counter users-main">
	<?php
	if(!empty($this->session->flashdata("success")))
	{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata("success"); ?>
				</div>
			</div>
		</div>
		<?php
	}
	if(!empty($this->session->flashdata("error")))
	{
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> <?php echo $this->session->flashdata("error"); ?>
			</div>
		</div>
	</div>
	<?php
	}
	?>
</div>
<div class="dash-counter users-main">
	<h2>Packages Lists</h2>
	<div class="user-table table-responsive">
		<div class="sucsessmsg"></div>
		<table class="table table-striped table-bordered">
			<tbody>
				<tr></tr>
				<?php
				$bur = base_url();
				echo "<tr>";
				echo "<th>Package Name</th>";
				echo "<th>Package Price</th>";
				echo "<th>Action</th>";
				echo "</tr>";

				foreach ($option_list_2_3 as $rows) 
				{
						echo "<tr>";
						echo "<td>".$rows["package_title"]."</td>";
						echo "<td>".$rows["package_price"]."</td>";
						echo "<td><a href='". base_url()."Admin/updatePackage/".$rows["id"]."'<i class='fa fa-edit'></i></a> &nbsp;";
						echo "<a class='delete'  data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."Admin/deleteFeaturePackage/".$rows['id'] . "' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i> </a>";
						if($rows['status'] == 1){
						echo "<a class='delete'  href='". $bur."Admin/activatePackage/".$rows['id']. "/1" . "' target='_self'><i class='fa fa-toggle-on' aria-hidden='true'></i> </a>";
						}
						else{
						echo "<a class='delete'  href='". $bur."Admin/activatePackage/".$rows['id']. "/2" . "' target='_self'><i class='fa fa-toggle-off' aria-hidden='true'></i> </a>";
						}
						echo " </td></tr>";
				}

				?>

			</tbody>
		</table>
		<?php
		if(isset($links) && !empty($links))
		{
			echo $links;
		}
		?>

	</div>
</div>


