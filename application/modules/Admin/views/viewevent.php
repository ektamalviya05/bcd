
<div class="toppad">
  <div class="panel panel-custom user-panel">
    <div class="panel-heading">
      <h3 class="panel-title"><?php 
        if(!empty($details['bs_name']))
          $username = ucwords($details['bs_name']);
        else
          $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
          echo $username;
        ?>
      </h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3 col-md-3">
          <div class="profile_tag">
            <img alt="User Pic" src="<?php echo base_url("assets/profile_pics/").$details['user_profile_pic']; ?>" class="img-responsive main-pro_img">
          </div>
        </div>
        <div class="col-sm-9 col-md-9">
          <table class="table table-user-information">
            <tbody>
              <tr>
                <td>Company:</td>
                <td>
                  <?php 
                    if(!empty($details['bs_name']))
                      $username = ucwords($details['bs_name']);
                    else
                      $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
                    echo $username;
                  ?>
                </td>
              </tr>
              <tr>
                <td>Email:</td>
                <td>
                  <?php 
                    echo $details['user_email'];
                  ?>                        
                </td>
              </tr>
              <tr>
                <td>Phone Number: </td>
                <td><?php echo $details['user_phone']; ?></td>
              </tr>              
              <tr>
                <td>Event Name: </td>
                <td>
                  <?php echo $details['event_name']; ?>
                </td>
              </tr>
              <tr>
                <td>Event Type: </td>
                <td>
                  <?php echo $details['evt_type']; ?>
                </td>
              </tr>
              <tr>
                <td>Start Publishing: </td>
                <td>
                  <?php echo $details['event_publish_start']; ?>
                </td>
              </tr>
              <tr>
                <td>End Publishing: </td>
                <td>
                  <?php echo $details['event_publish_end']; ?>
                </td>
              </tr>
              <tr>
                <td>Duration of Publishing: </td>
                <td>
                  <?php 
                  $date1 = date_create(date("Y-m-d", strtotime($details['event_publish_start'])));
                  $date2 = date_create(date("Y-m-d", strtotime($details['event_publish_end'])));
                  $difference = date_diff($date1,$date2);
                  $diff = $difference->format('%a');
                  $days = $diff > 0?" Days":" Day";
                  echo $diff.$days;
                  ?>
                </td>
              </tr>
              <tr>
                <td>Publish On Good Place: </td>
                <td>
                  <?php echo $details['event_goodplace']?'Yes':'No'; ?>
                </td>
              </tr>
              <tr>
                <td>Event Start Date: </td>
                <td>
                  <?php echo date("Y-m-d H:i", strtotime($details['event_start_date'])); ?>
                </td>
              </tr>
              <tr>
                <td>Event End Date: </td>
                <td>
                  <?php echo date("Y-m-d H:i", strtotime($details['event_end_date'])); ?>
                </td>
              </tr>
              <tr>
                <td>Event Price Per Person: </td>
                <td>
                  $<?php echo $details['event_price']; ?>
                </td>
              </tr>
              <tr>
                <td>People Limit: </td>
                <td>
                  <?php echo $details['event_per_ltd']; ?>
                </td>
              </tr>
              <tr>
                <td>Address: </td>
                <td>
                  <?php echo $details['event_address']; ?>
                </td>
              </tr>
              <tr>
                <td>Images: </td>
                <td>
                  <?php
                  if(!empty($details['event_img1'])){
                  ?>
                  <img src="<?php echo base_url("assets/img/events/").$details['event_img1']; ?>" height="250px", width="250px" />
                  <?php
                  }
                  if(!empty($details['event_img2'])){
                  ?>
                  <img src="<?php echo base_url("assets/img/events/").$details['event_img2']; ?>" height="250px", width="250px" />
                  <?php
                  }
                  if(!empty($details['event_img3'])){
                  ?>
                  <img src="<?php echo base_url("assets/img/events/").$details['event_img3']; ?>" height="250px", width="250px" />
                  <?php
                  }
                  if(!empty($details['event_img4'])){
                  ?>
                  <img src="<?php echo base_url("assets/img/events/").$details['event_img4']; ?>" height="250px", width="250px" />
                  <?php
                  }
                  ?>
                </td>
              </tr>
              <tr>
                <td>Event Description: </td>
                <td>
                  <?php echo $details['event_description']; ?>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <span class="pull-right">
          <a href="<?php echo base_url('Admin/event_list'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
      </span>
      <span class="clearfix"></span>
    </div>
  </div>
</div>