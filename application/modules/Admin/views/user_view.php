<div class="import-create dash-counter">
  <h2>View <?php echo $user[0]['user_type']; ?> User Details</h2>
</div>

<div class="import-create dash-counter"> 
  <?php
  if($user[0]['user_type'] == "Personal")
  {
    ?>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="gender-main">*GENRE</label>
          <div class="for-radio">
            <div class="btn-group" data-toggle="buttons">                        
              <?php
                if(!empty($user[0]['user_gender']) && $user[0]['user_gender'] == 'Female'){
                    $fcheck = 'checked="checked"';
                    $mcheck = '';
                }else{
                    $mcheck = 'checked="checked"';
                    $fcheck = '';
                }
              ?>

              <label class="btn btn-default active">
                <input type="radio" name="user_gender" disabled="" <?php echo $mcheck; ?> value="Male">Homme
              </label>
              <label class="btn btn-default">
                <input type="radio" name="user_gender" disabled="" <?php echo $fcheck; ?> value="Female">Femme
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">      
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>First Name</label>
          <input type="text" class="form-control " readonly="" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_firstname']; ?>">
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
             <label>Last Name</label>
            <input type="text" readonly="" class="form-control " name="user_lastname" placeholder="NOM"  value="<?php echo $user[0]['user_lastname']; ?>">
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
             <label>Email</label>
            <input type="email" readonly="" class="form-control" name="user_email"  placeholder="*Email"  value="<?php echo $user[0]['user_email']; ?>">

        </div>
      </div>

      <div class="col-md-6">
        <div class="form-group">
            <label>Phone No.</label>
            <input readonly="" type="text" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo $user[0]['user_phone']; ?>">

        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">                        
          <label>Address</label>
          <input id="autocomplete" placeholder="*ADRESSE" readonly="" name="user_address" class="form-control" onFocus="geolocate()" type="text"  value="<?php echo $user[0]['user_address']; ?>"/>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-11"></div>               
      <div class="col-md-1">
        <div class="btn-div">
          <br><br><button onclick="history.go(-1);" class="btn btn-warning form-btn" id="backbutton" value="Update" name="backbutton">Back</button>
        </div>
      </div>
    </div>
    <?php
  }
  else{
    ?>
    <!-- Company User  -->

    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label class="gender-main">*GENRE</label>
          <div class="for-radio">
            <div class="btn-group" data-toggle="buttons">                        
              <?php
              if(!empty($user[0]['user_gender']) && $user[0]['user_gender'] == 'Female'){
                  $fcheck = 'checked="checked"';
                  $mcheck = '';
              }else{
                  $mcheck = 'checked="checked"';
                  $fcheck = '';
              }
              ?>
              <label class="btn btn-default active">
                <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male">Homme
              </label>
              <label class="btn btn-default">
                <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female">Femme
              </label>
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-6">      
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>First Name</label>
          <input type="text" class="form-control " name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_firstname']; ?>" />
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Last Name</label>
          <input type="text" class="form-control " name="user_lastname" placeholder="NOM"  value="<?php echo $user[0]['user_lastname']; ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <label>Email</label>
          <input type="email" readonly="" class="form-control" name="user_email"  placeholder="*Email"  value="<?php echo $user[0]['user_email']; ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
          <label>Phone No.</label>
          <input type="text" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo $user[0]['user_phone']; ?>">
          <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>
        </div>
      </div>
    </div>


    <div class="row">
      <div class="col-md-12">
        <div class="form-group">                        
          <label>Address</label>
          <input id="autocomplete" placeholder="*ADRESSE" name="user_address" class="form-control" onFocus="geolocate()" type="text"  value="<?php echo $user[0]['user_address']; ?>"/>   
          <?php echo form_error('user_address','<span class="help-block">','</span>'); ?>
        </div>
      </div>
    </div>


    
    <div class="row">
      <div class="col-md-6">
        <div class="form-group">
          <input type="text" class="form-control " readonly=""  name="user_company_name" placeholder="*NOM DE L'ENTREPRISE"  value="<?php echo $user[0]['user_company_name']; ?>">
          <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>
        </div>
      </div>
      <div class="col-md-6">
        <div class="form-group">
            <input type="text" readonly="" class="form-control" name="user_company_number" placeholder="*NUMÉRO DE SIRET" value="<?php echo $user[0]['user_company_number']; ?>">
             <?php echo form_error('user_company_number','<span class="help-block">','</span>'); ?>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <div>
            <label>LANGUE</label>
          </div>
          <?php
          $lang = $user[0]['user_language'];
          if(!empty($lang))
            $lang = explode(",", $lang);                                    
          else
            $lang = array();
          ?>
          
          <?php
          $languages = $this->Admin_model->getLanguageRows(array("conditions"=>array("lang_status"=>1)));
          if(!empty($languages)){
            foreach($languages as $lang1){
              ?>                
                <input type="checkbox" name="user_language[]" value="<?php echo $lang1['lang_id']; ?>" <?php if(in_array($lang1['lang_id'], $lang)) echo "checked"; ?>>
                <?php echo ucwords($lang1['lang_name']); ?>                
              <?php
            }
          }
          ?>            
        </div>
      </div>
    </div>
    


    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <!-- <input type="text" class="form-control" value="<?php //echo $user[0]['user_categories']; ?>" name="user_categories" placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)"> -->
          <label>User Categories</label>
          <input 
            class='form-control' 
            readonly 
            multiple='multiple' 
            list='categories' 
            name='user_categories' 
            id="business_category" 
            type='text' 
            placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)"
            value="<?php echo $this->Admin_model->getCategoriesAndFilters($user[0]['user_id'], 1); ?>">
          <datalist id="categories">
              <?php
              $business_categories = $this->Admin_model->getBusinessCategories(1);
              foreach($business_categories->result() as $cate){
                ?>
                <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                <?php
              }
              ?>
          </datalist>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <div>
            <label class="gender-main">User Documents:</label>
          </div>
          <button type="button" data-toggle="modal" data-target="#uploadpurchasedoc" class="btn btn-primary userpurchasedoc">Upload Document</button>
        </div>
        <div class="form-group">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>Document Name</th>
                <th>Upload Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php
              if(!empty($uploaddocumentList)){
                foreach($uploaddocumentList as $lst){
                  ?>
                    <tr>
                      <td><?php echo $lst['ct_name'] ?></td>
                      <td><?php echo date("d M, Y h:i A", strtotime($lst['dc_date_created'])); ?></td>
                      <td>
                        <div class="link-del-view">
                          <div class="tooltip-2">
                            <a href="<?php echo base_url("Admin/attachedDocument/").$lst['dc_id']; ?>"><i class="fa fa-download" aria-hidden="true"></i></a><span class="tooltiptext">Download Document</span>
                          </div>
                          <div class="tooltip-2">
                            <a href="javascript:;" class="deletedoc" refers="<?php echo $lst['dc_id']; ?>" data-toggle="confirmation" data-title="Are you sure to delete?"><i class="fa fa-trash-o" aria-hidden="true"></i></a><span class="tooltiptext">Remove</span>
                          </div>
                        </div>
                      </td>                          
                    </tr>
                  <?php
                }
              }else{
                ?>
                <tr>
                  <td colspan="3">No Record found</td>
                </tr>
                <?php
              }
              ?>                        
            </tbody>
          </table>
        </div>
      </div>
      <!-- <div class="col-md-6">      
      </div> -->
    </div>


    <div class="row">
      <div class="col-md-11"></div>               
      <div class="col-md-1">
        <div class="btn-div">
          <br><br><button onclick="history.go(-1);" class="btn btn-warning form-btn" id="backbutton" value="Update" name="backbutton">Back</button>
        </div>
      </div>
    </div>

    <?php
  }

  ?>
</div>

<!-- upload doc modal -->
  <div id="uploadpurchasedoc" class="modal chat-modal fade" role="dialog">
    <div class="modal-dialog">    
      <div class="modal-content">
          <form id="uploaduserpurchasedoc" method="POST" action="<?php echo base_url("Admin/uploadUserPurchaseDocument") ?>" autocomplete="off">
            <div class="modal-header">
                <div class="prof-msg-details">
                  <div class="row">
                      <div class="col-xs-12">
                        <div class="msg-pro-pic-sec">
                          <div class="msh-prof-details">
                            <h3>Upload User Documents</h3>
                          </div>
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>                     
                        </div>
                      </div>
                  </div>
                </div>
            </div>
            <div class="modal-body">              
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-label">Option</label>
                    <select name="options" class="form-control">
                      <option value="">Select Document</option>
                      <?php
                      if(!empty($availableDoc)){
                        foreach($availableDoc as $lst){
                          ?>
                          <option value="<?php echo $lst['ct_id'] ?>"><?php echo $lst['ct_name']; ?></option>
                          <?php
                        }
                      }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="form-label">Upload Document</label>
                    <input type="file" name="manddoc" accept="application/pdf" />
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer">
                <div class="form-group">
                  <input type="hidden" value="<?php echo $user[0]['user_id'] ?>" name="user" />
                  <button type="submit" class="btn btn-primary upload"> Upload</button>
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                  <div class="userdetailsermsg"></div>
                </div>
            </div>
          </form>
      </div>
    </div>
  </div>
<!-- upload doc modal end -- >