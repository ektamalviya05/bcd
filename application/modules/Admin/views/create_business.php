    <script type="text/javascript">
/*      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M'
        });
      };*/
    </script>

<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 

  <div class="row">
       <div class="col-md-12">
          <div class="reg-form-main">

    <form action="" method="post" name="myForm">
<span>(*) Required Fields</span>
<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="gender-main">*GENRE</label>
                    <div class="for-radio">
                      <div class="btn-group" data-toggle="buttons">
                        
 <?php
            if(!empty($user['user_gender']) && $user['user_gender'] == 'Female'){
                $fcheck = 'checked="checked"';
                $mcheck = '';
            }else{
                $mcheck = 'checked="checked"';
                $fcheck = '';
            }
            ?>

                        <label class="btn btn-default active">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male">Homme
                        </label>
                        <label class="btn btn-default">
                          <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female">Femme
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" class="form-control" required="" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>
   <input type="hidden" name="user_type" value="Professional">

   <?php
$str = form_error('user_company_number');
$newphrase = str_replace("<p>", "", $str);
$newphrase = str_replace("</p>", "", $newphrase );
if( trim($newphrase) == "This Company Number already exists.")
{
?>
<script type="text/javascript">
$(document).ready(function() {
openModal();
});
function openModal(){
  $('#myModaComp').modal();
}  
</script>
  <?php
}
?>
                  </div>
                </div>
              </div>






 <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" class="form-control " required="" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo set_value('user_firstname'); ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="text" class="form-control " required="" name="user_lastname" placeholder="NOM"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>





        <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" class="form-control " required="" name="user_company_name" placeholder="*NOM DE L'ENTREPRISE"  value="<?php echo set_value('user_company_name'); ?>">
          <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" required="" class="form-control" name="user_company_number" placeholder="*NUMÉRO DE SIRET" value="<?php echo set_value('user_company_number'); ?>">
                       <?php echo form_error('user_company_number','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>



<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="language-main">LANGUE</label>
                    <div class="check-box-inline">

                      <label class="checkbox-inline">
                          <input type="checkbox" value="2" name="user_language[]">English
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="3" name="user_language[]">French
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="4" name="user_language[]">Spanish
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" value="5" name="user_language[]">Arab
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="6" name="user_language[]">italion
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="7" name="user_language[]">Germen
                        </label>



                    </div>
                  </div>
                </div>
              </div>

 <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <!-- <input type="text" class="form-control" name="user_categories" placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)"> -->
                    <input class='form-control'                     
                     multiple='multiple'
                     list='categories'
                     name='user_categories'
                     id="business_category"
                     type='text' placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)" value="<?php echo set_value("user_categories"); ?>">

                    <datalist id="categories">
                        <?php
                        foreach($business_categories->result() as $cate){
                          ?>
                          <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                          <?php
                        }
                        ?>                        
                    </datalist>
                  </div>
                </div>
                <!-- <div class="col-md-2">
                  <div class="form-group">
                    <input type="checkbox" id="showallcategory"> Show all                    
                  </div>
                </div> -->
              </div>

  <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                        
           
  <input id="autocomplete" placeholder="*VEUILLEZ SAISIR VOTRE ADRESSE" required=""
  name="user_company_address" class="form-control " onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>"></input>
   
         <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?>
        


                  </div>
                </div>
              </div>



<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      
                      <input type="email" class="form-control" required="" name="user_email"  placeholder="*Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                <input type="password" class="form-control " required="" name="user_password"  id="user_password" placeholder="*MOT DE PASSE" >
          <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="password" class="form-control " name="conf_password" placeholder="CONFIRMER LE MOT DE PASSE" >
          <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>


       



<div><br><p>&nbsp;</p></div>
        <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="auth_check" id="auth_check" value="1" checked="checked"><span><?php echo $newsletter1; ?></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="sms_check" id="sms_check" value="1" checked="checked"><span><?php echo $newsletter2; ?></span>
                    </label>
                  </div>
                </div>
              </div>
            </div>

    <div class="row">
                <div class="col-md-12">
      <br>
                  <div class="btn-div">
                   <button class="btn btn-default form-btn" value="S'enregistrer" name="regisSubmit" type="submit">S'enregistrer</button>

                  </div>
                </div>
</div>

    </form>

    </div>
    </div>
    </div>
<script type="text/javascript">
    $(function() {
   $("form[name='myForm']").validate({
    // alert("dfd");
    // Specify validation rules
    rules: {
      user_firstname: "required",
      user_lastname: "required",
      user_email: {
        required: true,
        email: true
      },
      user_password: {
        required: true,
        minlength: 6
      },
      conf_password:{
        required: true,
        equalTo: "#user_password",
        minlength: 6
      }
      
    },
    // Specify validation error messages
    messages: {
      user_firstname: "Please enter your firstname",
      user_lastname: "Please enter your lastname",
      user_password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
      conf_password: {
        required: "Please confirm password",
        minlength: "Your password must be at least 6 characters long",
        equalTo: "Password must be same"
      },
      user_email: "Please enter a valid email address"
    }
    ,
    submitHandler: function(form) {
     form.submit();
    //return false;
    }
  });
});

</script>    