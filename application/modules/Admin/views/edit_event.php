<style>
a.del_evt_img {
    width: 100%;
    float: left;
}
</style>
<section class="single_event">
  <div class="container">    
    <div class="row">
      <?php
      $evnt=$details;
       // foreach($details as $evnt){
              ?>
      <form method="post" enctype="multipart/form-data" id="editeventww" action="<?php echo base_url("Admin/editEvent/").$details['event_id']; ?>" autocomplete="off" >
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label">Event name:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label35']; ?></span>
                </span>
              </label>
              <input type="hidden" class="event_id" name="event_id" value="<?php echo $evnt['event_id']; ?>">
              <input type="text" name="ename" class="form-control" placeholder="Event Name" value="<?php echo $evnt['event_name']; ?>">
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass">Event Type:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <select name="eventtype" class="form-control">
                <option value="">Select Type</option>
                <?php
                if(!empty($eventTypeList)){
                  foreach($eventTypeList as $evttype){
                    ?>
                    <option value="<?php echo $evttype['evt_type_id']; ?>" <?php if($evnt['event_type_id'] == $evttype['evt_type_id']) echo "selected"; ?>><?php echo $evttype['evt_type']; ?></option>
                    <?php
                  }
                }
                ?>
              </select>
              <?php echo form_error("eventtype"); ?>
            </div>
          </div>
        </div>
        <div class='row publicationdiv'>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label">Publication Start Date:<span class="tooltip">?<span class="tooltiptext"><?php echo $suggestion['label36']; ?></span></span></label>
              <input type="text" readonly name="pesdate" class="form-control edatepickerfuture" value="<?php echo $evnt['event_publish_start']; ?>" placeholder="Publishing Start Date">
              <?php echo form_error("pesdate"); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass">Publication End Date:<span class="tooltip">?<span class="tooltiptext"><?php echo $suggestion['label37']; ?></span></span></label>
              <input type="text" readonly name="peedate" class="form-control edatepickerfuture" value="<?php echo $evnt['event_publish_end']; ?>" placeholder="Publishing End Date">
              <?php echo form_error("peedate"); ?>
            </div>
          </div>
        </div>        
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label">Event Start Date:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label36']; ?></span>
                </span>
              </label>
              <input type="text" readonly name="esdate" class="form-control datetimepickerfuture" placeholder="Start Date" value="<?php echo date("Y-m-d H:i", strtotime($evnt['event_start_date'])); ?>">
              <?php echo form_error("esdate"); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass">Event End Date:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <input type="text" readonly name="eedate" class="form-control datetimepickerfuture" placeholder="End Date" value="<?php echo date("Y-m-d H:i", strtotime($evnt['event_end_date'])); ?>">
              <?php echo form_error("eedate"); ?>
            </div>
          </div>                                                                    
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass">Number Of Persons:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <input type="number" min="1" name="noperson" class="form-control" placeholder="Number Of Persons" value="<?php echo $evnt['event_per_ltd']; ?>">
              <?php echo form_error("noperson"); ?>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <label class="form-label confm-pass">Event Price:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <input type="text" name="eventprice" class="form-control" placeholder="Price Per Person" value="<?php echo $evnt['event_price']; ?>"/>                                                        
              <?php echo form_error("eventprice"); ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label confm-pass">Address:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <input id="autocomplete" type="text" name="evtaddress" class="form-control citycomplete" onfocus="geolocate()" placeholder="Event Address" value="<?php echo $evnt['event_address']; ?>" />                    
              <?php echo form_error("evtaddress"); ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label confm-pass">Event Images:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <input type="file" multiple="true" name="evtfile[]" id="evtfile" class="form-control" />
              <div id="evtimgmsg"></div>
              <div id="evtimgpreview">
                <ul>
                  <?php
                  if(!empty($evnt['event_img1']) && file_exists("./././assets/img/events/".$evnt['event_img1'])){
                  ?>
                  <li>
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img1']; ?>" height="100px" width="100px" />
					<a href="javascript:;" class="del_evt_img" data-column="event_img1" ><i class="fa fa-trash"></i></a>
                  </li>
                  <?php
                  }
                  ?>
                  <?php
                  if(!empty($evnt['event_img2']) && file_exists("./././assets/img/events/".$evnt['event_img2'])){
                  ?>
                  <li>                    
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img2']; ?>" height="100px" width="100px"/>   <a href="javascript:;" class="del_evt_img" data-column="event_img2" ><i class="fa fa-trash"></i></a>                   
                  </li>
                  <?php
                  }
                  ?>
                  <?php
                  if(!empty($evnt['event_img3']) && file_exists("./././assets/img/events/".$evnt['event_img3'])){
                  ?>
                  <li>
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img3']; ?>" height="100px" width="100px"/><a href="javascript:;" class="del_evt_img" data-column="event_img3" ><i class="fa fa-trash"></i></a>                      
                  </li>
                  <?php
                  }
                  ?>
                  <?php
                  if(!empty($evnt['event_img4']) && file_exists("./././assets/img/events/".$evnt['event_img4'])){
                  ?>
                  <li>
                    <img src="<?php echo base_url("assets/img/events/").$evnt['event_img4']; ?>" height="100px" width="100px"/> 
                     <a href="javascript:;" class="del_evt_img" data-column="event_img4" ><i class="fa fa-trash"></i></a>                    
                  </li>
                  <?php
                  }
                  ?>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label">Event details:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label39']; ?></span>
                </span>
              </label>
              <textarea class="textarea-main form-control" name="description" placeholder="Description"><?php echo $evnt['event_description']; ?></textarea>
            </div>
          </div>                                
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="form-group">
              <label class="form-label confm-pass">Status:
                <span class="tooltip">?
                  <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                </span>
              </label>
              <select class="form-control" name="status">
                <option value="">Select Status</option>
                <option value="1" <?php if($evnt['event_status']) echo "selected"; ?>>Activate</option>
                <option value="0" <?php if(!$evnt['event_status']) echo "selected"; ?>>Deactivate</option>
              </select>
              <?php echo form_error("status"); ?>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12"> 
            <button type="submit" class="btn btn-primary buyopt upps"> Update Event</button>
            <a href="<?php echo base_url("Admin/event_list"); ?>" class="btn btn-default"> Back</a>
            <!-- <a href="<?php //echo base_url("Users/myEvents/").$evnt['event_id']; ?>" class="btn btn-default cancelevent"> <i class="fa fa-arrow-left"></i> Back</a>
            <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
            <div class="eventmsg"></div> -->
          </div>
        </div>
      </form>
      <?php
      //}
      ?>
    </div>
  </div>
</section>