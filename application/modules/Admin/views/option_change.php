  <div class="dash-counter users-main">
    <h2>Options Lists</h2>
    <div class="user-table table-responsive">
      <br>
      <form id="myform" name="myform" action="" method="POST">
        <table class="table">
          <tr>
            <td  width="20%">Option Name</td>
            <td>
              <input class="form-control" type="text" readonly="" name="opt_name" value="<?php echo $option_list[0]['opt_name'] ?>">
              <?php echo form_error("opt_name"); ?>
            </td>
          </tr>

          <tr>
            <td>Duration</td>
            <td>
              <input class="form-control" type="text" readonly="" name="opt_price_type" value="<?php echo $option_list[0]['opt_price_type'] ?>">              
            </td>
          </tr>
          <tr>
            <td>Duration Display Text</td>
            <td>
              <input class="form-control" type="text" name="opt_price_type_text" value="<?php echo $option_list[0]['opt_text']; ?>">              
            </td>
          </tr>

          <tr>
            <td>Price</td>
            <td>
              <input class="form-control" type="text" required="" id="opt_price" name="opt_price" value="<?php echo $option_list[0]['opt_price'] ?>">
              <?php echo form_error("opt_price"); ?>
            </td>
          </tr>
          <tr>
            <td>Option Status</td>
            <td>
              <select class="form-control" name="status">
                <option value="">Select Status</option>
                <option value="1" <?php if( $option_list[0]['opt_pr_status'] == 1) echo "selected"; ?>> Enable</option>
                <option value="0" <?php if( $option_list[0]['opt_pr_status'] == 0) echo "selected"; ?>> Disable</option>
              </select>
              <?php echo form_error("status"); ?>
            </td>
          </tr>
          <tr>
            <td></td>
            <td>&nbsp;</td>
          </tr>

          <tr>
            <td></td>
            <td>
              <input type="submit" class="btn btn-primary" name="changeOptionChange" value="Save Changes">
              <a href="<?php echo base_url("Admin/option_list"); ?>" class="btn btn-default"> < Back</a>
            </td>
          </tr>
        </table>
      </form>
    </div>
  </div>
  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
  <script>
  $(document).ready(function () {
    $("#opt_price").keypress(function (e) {
       if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
          $("#errmsg").html("Digits Only").show().fadeOut("slow");
                 return false;
      }
     });
  });
  </script>