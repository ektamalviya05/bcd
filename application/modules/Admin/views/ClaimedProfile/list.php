<div class="dash-counter users-main">
  <h2>Claimed Business Profile list</h2>
  <div class="row">
    <div class="col-md-12">
      <button disabled="disabled" class="btn btn-danger btn-xs" id="deleteclaimeprofiles">Delete Selected</button>
    </div>
  </div>
  <br>
  <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th><input type="checkbox" class="checkall"/> All</th>            
            <th>Claimed Profile</th>
            <th>Claimed By</th>            
            <th>Email</th>
            <th>Number</th>            
            <th>Status</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody id="displayrecords">
          <?php
          if(!empty($msoptions)){
            $cnt = 1;
            foreach($msoptions as $msopt){
              ?>
              <tr>                
                <td><input type="checkbox" class="check_tupple" name="assign_tupple[]" value="<?php echo $msopt['claim_request_id']; ?>"/> <?php echo $cnt; ?></td>
                <td><?php echo $msopt['user_company_name']; ?></td>
                <td><?php echo $msopt['claim_user_firstname']." ".$msopt['claim_user_lastname']; ?></td>
                <td><?php echo $msopt['claim_user_email']; ?></td>
                <td><?php echo $msopt['claim_user_phone']; ?></td>
                <td><?php
                if($msopt['claim_status'])
                  echo "Approved";
                else
                  echo "Requested";
                ?></td>
                <td>
                  <div class='link-del-view'>
                    <div class='tooltip-2'>
                      <a target='_blank' href='<?php echo base_url("Admin/viewClaimedprofile/").$msopt['claim_request_id']; ?>'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span>
                    </div>                    
                  </div>
                </td>
              </tr>
              <?php
              $cnt++;
            }
          }else{
            ?>
            <tr><td colspan="7"><span>No records available.</span></td></tr>
            <?php
          }
          ?>
        </tbody>
      </table>
  </div>
</div>