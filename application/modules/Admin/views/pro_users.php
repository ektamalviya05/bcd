<script src="<?php echo base_url(); ?>assets/js/bootstrap-confirmation.min.js"></script>
<script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
<?php
if (isset($deleted) && trim($deleted) == "yes") {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            openModal();

        });
        function openModal() {
            $('#myModaDel').modal();
        }
    </script>
    <?php
}
?>

<?php
if (isset($activated) && trim($activated) == "yes") {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            openModal();
        });
        function openModal() {
            $('#myModaAct').modal();
        }
    </script>

    <?php
} else if (isset($activated) && trim($activated) == "no") {
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            openModal();
        });
        function openModal() {
            $('#myModaDAct').modal();
        }
    </script>
    <?php
}
?>

<div class="loader">
    <center>
        <img class="image-responsive" src="<?php echo base_url(); ?>assets/img/loader.svg" alt="loading..">
    </center>
</div>
<!--<div class="import-create dash-counter">
     <h2>Create</h2> 
    <div class="row">

        <div class="col-md-3">
            <div class="dc-sub">
                <a href="<?php echo base_url(); ?>Admin/createbusiness">
                    <div class="dc-sub-ico new-let-ico">
                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                    </div>
                    <div class="dc-sub-txt nl-txt">
                        <h3>Create</h3>
                        <p>User</p>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>-->
<div class="import-create dash-counter">
    <h2>Filters</h2>

    <form action="<?php echo base_url() . 'Admin/user_list_pro/'; ?>" method="post" id="filterForm">
        <div class="row">

            <div class="form-group col-md-4" >
                <input type="text" class="form-control" name="user_firstname" id="user_firstname"  placeholder="User First Name"  
                       value="<?php echo $this->session->userdata('profltr_user_firstname') ?>">

            </div>
            <div class="form-group col-md-4">
                <input type="text" class="form-control" name="user_lastname" id="user_lastname"  placeholder="User Last Name"  
                       value="<?php echo $this->session->userdata('profltr_user_lastname') ?>">
            </div>

            <div class="form-group col-md-4">
                <input type="email" autocomplete="off" class="form-control" name="user_email" id="user_email"  placeholder="Email"  
                       value="<?php echo $this->session->userdata('profltr_user_email') ?>">
                <input type="hidden" name="clearfilter" id="clearfilter"  value="<?php echo $this->session->userdata('profltr_user_email') ?>">

            </div>
            
            <!--
            <div class="form-group col-md-4">
            <input type="text" class="form-control" name="user_language" id="user_language"  placeholder="Language"  
             value="<?php echo $fltr_user_language != '' ? $fltr_user_language : ''; ?>">
            </div>
            -->

        </div>

        <div class="row">            


            <!-- <div class="form-group col-md-4" >
                <div class="th-sele">                
                    <select id="signup_method" class="form-control" id="signup_method"  name="signup_method">
                        <option value="-1">Select Method </option>
                        <option value="1"  <?php echo $this->session->userdata('profltr_signup_method') == 1 ? "selected" : ""; ?> >Regular Signup</option>
                        <option value="2"  <?php echo $this->session->userdata('profltr_signup_method') == 2 ? "selected" : ""; ?> >Facebook Signup</option>
                        <option value="3"  <?php echo $this->session->userdata('profltr_signup_method') == 3 ? "selected" : ""; ?> >Google Signup </option>
                        <option value="4"  <?php echo $this->session->userdata('profltr_signup_method') == 4 ? "selected" : ""; ?> >Imported </option>
                    </select>
                </div>
            </div> -->
        </div>

        <div class="row">

            <!-- <div class="form-group col-md-4" >
                <input type="text" autocomplete="off" class="form-control" name="user_company_name" id="user_company_name"  placeholder="Company Name"  
                       value="<?php echo $this->session->userdata('profltr_user_company_name') ?>">

            </div> -->
            <div class="form-group col-md-4">
                <input type="text" class="form-control" name="user_company_number" id="user_company_number"  placeholder="User Number"  
                       value="<?php echo $this->session->userdata('profltr_user_company_number') ?>">
            </div>

            <div class="form-group col-md-4">
                <input type="text" class="form-control" name="user_company_address" id="user_company_address"  placeholder="User Address"  
                       value="<?php echo $this->session->userdata('profltr_user_company_address') ?>">
            </div>
        </div>

        

        <div class="row">
            <div class="form-group col-md-12">
                <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn-primary btn th-primary" value="Filter"/>

                <input type="submit"  id="clearfilterSubmit" name="clearfilterSubmit" class="btn-danger btn th-danger" value="Clear Filter"/>

                <!-- <a href="<?php echo base_url("Admin/usersListWaitingForApprroval"); ?>" class="btn btn-info th-wating">Waiting Users</a> -->
           
                
            </div>
        </div>
    </form>

    <div class="dash-counter users-main">
    <?php
    if(!empty($this->session->flashdata("success")))
    {
        ?>
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> <?php echo $this->session->flashdata("success"); ?>
                </div>
            </div>
        </div>
        <?php
    }
    if(!empty($this->session->flashdata("error")))
    {
    ?>
    <div class="row">
        <div class="col-md-12">
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> <?php echo $this->session->flashdata("error"); ?>
            </div>
        </div>
    </div>
    <?php
    }
    ?>


<?php
//$downurl = base_url(). "Admin/user_list_download/".$usertype;
$downurl = base_url() . "Admin/user_list_download/" . $type;
?>
    <form action="<?php echo $downurl; ?>" method="post" id="export-form">
        <input type="hidden" value='<?php echo set_value('user_email'); ?>' id='hidden_user_email' name='hidden_user_email'/>
        <input type="hidden" value='<?php echo set_value('signup_method'); ?>' id='hidden_signup_method' name='hidden_signup_method'/>
        <input type="hidden" value='' id='hidden-type' name='ExportType'/>
    </form>


</div>

<div class="dash-counter users-main">
    <!-- <h2>Users <a href="javascript:void(0)" class="btn btn-warning th-warn" id="export-to-csv">Download Users</a></h2> -->

    <div class="user-table table-responsive">
        <table class="table table-striped table-bordered">
            <tbody>
                <tr></tr>


<?php
$bur = base_url();
//This is for Professional Users

echo "<tr>";
echo "<th>Serial No.</th>";
echo "<th><a href='" . $bur . "Admin/user_list_pro/firstname/" . $page . "'>First Name</a></th>";
echo "<th><a href='" . $bur . "Admin/user_list_pro/lastname/" . $page . "'>Last Name</a></th>";
echo "<th><a href='" . $bur . "Admin/user_list_pro/email/" . $page . "'>Email</a></th>";
/*echo "<th><a href='" . $bur . "Admin/user_list_pro/company/" . $page . "'>Company Name</a></th>";*/
echo "<th>Phone Number</th>";
echo "<th>Address</th>";
//echo "<th><a href='" . $bur . "Admin/user_list_pro/lastlogin/" . $page . "'>Status</a></th>";
echo "<th>Action</th>";
echo "</tr>";
// echo "<tr><td colspan='6'>";
// print_r($userlist);
// echo "</td></tr>";
$i =1;
$segment  = $this->uri->segment(4);
//echo $segment;die;
 if($segment){
                  $i = $segment+1;
                }



if ($record_found > 0) {
    foreach ($userlist as $rows) {
        $stat = $rows['is_online'] == 1 ? 'Online' : 'Offline';
        $add = $rows['user_address'] ;
        if($add)
        {
           $address =$add;

        }
        else
        {
            $address ='NA';
        }
        $ph = $rows['user_phone'];
        if($ph)
        {
            $phone = $ph;
        }
        else
        {
            $phone = 'NA';

        }

        //$address = (//$rows['user_address'])? $add : 'NA';
        echo "<tr>";
        echo "<td>" . $i . "</td>";
        echo "<td>" . $rows['user_firstname'] . "</td>";
        echo "<td>" . $rows['user_lastname'] . "</td>";
        echo "<td>" . $rows['user_email'] . "</td>";
        /*echo "<td>" . $rows['user_company_name'] . "</td>";*/
        echo "<td>" . $phone . "</td>";
        echo "<td>" . $address . "</td>";

        
//        if ($rows['is_online'] == 1) {
//            echo "<td style='color:green; font-weight:bold;'>Online</td>";
//        } else {
//            echo "<td style='color:red'>Offline</td>";
//        }


        echo "<td><div class='link-del-view'>";
        //echo "<div class='tooltip-2'><a target='_blank' href='" . $bur . "Users/user_profile/" . $rows['user_id'] . "'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span></div>";
       // echo "<div class='tooltip-2'><a href='" . $bur . "Admin/edituserpro/" . $rows['user_id'] . "/updt'><i class='fa fa-edit' aria-hidden='true'></i></a><span class='tooltiptext'>Edit</span></div>";

// echo "<div class='tooltip-2'><a class='dluser' data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."Admin/deleteuser/".$rows['user_id'] . "/". $rows['user_type'] ."' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div> ";
        //echo "<div class='tooltip-2'><a class='dluser' data-toggle='confirmation' modal-aria='" . $rows['user_id'] . "' data-title='Are you sure to delete?' href='javascript:;' ><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div> ";
        $tstid = $rows['user_id'];
        ?>
        <div class="tooltip-2"><a href="javascript:;" data-toggle="" data-singleton="" data-popout="true" data-placement="top" class="dluser" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
            <span class="tooltiptext">Move to Trash</span>
        </div>
        <?php


        if ($rows['status'] == 0) {
            echo "<div class='tooltip-2'><a id='actv" . $rows['user_id'] . "'  class='activate' ><i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></div>";
        } else {
            echo "<div class='tooltip-2'><a id='dact" . $rows['user_id'] . "'  class='activate'><i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></div>";
        }

        //echo "<div class='tooltip-2'><a id='rec" . $rows['user_id'] . "' class='resend'><i class='fa fa-refresh' aria-hidden='true'></i></a><span class='tooltiptext'>Send Reset Password Link</span></div> ";

//        if ($rows['publishonhome'] == 0) {
//            echo "<div class='tooltip-2'><a id='pubs" . $rows['user_id'] . "' class='publish'><i class='fa fa-play-circle' aria-hidden='true'></i></a><span class='tooltiptext'>Publish on Home</span></div> ";
//        } else {
//            echo "<div class='tooltip-2'><a id='upub" . $rows['user_id'] . "' class='publish'><i class='fa fa-play-circle-o' aria-hidden='true'></i></a><span class='tooltiptext'>Unpublish from Home</span></div> ";
//        }


//        if ($rows['toprated'] == 0) {
//            echo "<div class='tooltip-2'><a id='tops" . $rows['user_id'] . "' class='toprated'><i class='fa fa-arrow-up' aria-hidden='true'></i></a><span class='tooltiptext'>Publish on Home</span></div> ";
//        } else {
//            echo "<div class='tooltip-2'><a id='ntop" . $rows['user_id'] . "' class='toprated'><i class='fa fa-arrow-down' aria-hidden='true'></i></a><span class='tooltiptext'>Unpublish from Home</span></div> ";
//        }

        echo "</div></td>";
        echo "</tr>";
        $i++;
    }
} else {
    echo "<tr><td colspan='7'>No Record Found</td></tr>";
}
echo "</table>";
?>
            <div class="read-more">
                <?php
                if (isset($links) && !empty($links)) {
                    echo $links;
                }
                ?>
            </div>
                <?php
                ?>
    </div>
</div>

<script  type="text/javascript">
    $(document).ready(function () {
//$('.loader').hide();
        jQuery('#export-to-csv').bind("click", function () {
            var target = $(this).attr('id');
            switch (target) {
                case 'export-to-csv' :
                    $('#hidden-type').val(target);
                    // alert($('#signup_method').val());
                    $('#hidden_user_email').val($('#user_email').val());
                    $('#hidden_signup_method').val($('#signup_method').val());
                    $('#export-form').submit();
                    $('#hidden-type').val('');
                    break
            }
        });


        jQuery('.resend').bind("click", function () {
            var vals = 0;
            vals = $(this).attr('id').substr(3);
            $.ajax({
                url: '<?php echo base_url() . "Admin/resetpassmail/"; ?>' + vals,
                type: 'POST',
                data: {
                    user_id: vals
                },
                // your ajax code
                beforeSend: function () {
                    //$('.loader').show()
                    $('.loader').css("visibility", "visible");
                },
                complete: function () {
                    // $('.loader').hide();
                    $('.loader').css("visibility", "hidden");
                },
                //dataType: 'json',
                success: function (data) {
                    $('#myModaResentMail').modal();
                },
                error: function (data) {}

            });

        });

        jQuery('.activate').bind("click", function () {

            var vals = 0;
            var acts = "";
            vals = $(this).attr('id').substr(4);
            acts = $(this).attr('id').substr(0, 4);
//alert(vals + " -- " + acts);
            if (acts == 'actv')
            {
                acts = 1;
            } else
            {
                acts = 0;
            }


            $.ajax({
                url: '<?php echo base_url() . "Admin/activateuser/"; ?>' + vals,
                type: 'POST',
                data: {
                    user_id: vals,
                    action: acts
                },
                beforeSend: function () {
                    $('.loader').css("visibility", "visible");
                },
                complete: function () {
                    $('.loader').css("visibility", "hidden");
                },
                //dataType: 'json',
                success: function (data) {
                    if (acts == 1)
                    {
                        $('#myModaAct').modal();
                    } else
                    {
                        $('#myModaDAct').modal();
                    }

                },
                error: function (data) {}
            });

        });



        jQuery('.publish').bind("click", function () {

            var vals = 0;
            var acts = "";
            vals = $(this).attr('id').substr(4);
            acts = $(this).attr('id').substr(0, 4);
//alert(vals + " -- " + acts);
            if (acts == 'pubs')
            {
                acts = 1;
            } else
            {
                acts = 0;
            }

            $.ajax({
                url: '<?php echo base_url() . "Admin/setpublishonhome/"; ?>' + vals,
                type: 'POST',
                data: {
                    user_id: vals,
                    action: acts
                },
                beforeSend: function () {
                    $('.loader').css("visibility", "visible");
                },
                complete: function () {
                    $('.loader').css("visibility", "hidden");
                },
                //dataType: 'json',
                success: function (data) {
                    if (acts == 1)
                    {
                        $('#myModaPubs').modal();
                    } else
                    {
                        $('#myModaUPubs').modal();
                    }

                },
                error: function (data) {}
            });

        });




        jQuery('.toprated').bind("click", function () {

            var vals = 0;
            var acts = "";
            vals = $(this).attr('id').substr(4);
            acts = $(this).attr('id').substr(0, 4);
//alert(vals + " -- " + acts);
            if (acts == 'tops')
            {
                acts = 1;
            } else
            {
                acts = 0;
            }

            $.ajax({
                url: '<?php echo base_url() . "Admin/settopratedonhome/"; ?>' + vals,
                type: 'POST',
                data: {
                    user_id: vals,
                    action: acts
                },
                beforeSend: function () {
                    $('.loader').css("visibility", "visible");
                },
                complete: function () {
                    $('.loader').css("visibility", "hidden");
                },
                //dataType: 'json',
                success: function (data) {
                    if (acts == 1)
                    {
                        $('#myModaPubs').modal();
                    } else
                    {
                        $('#myModaUPubs').modal();
                    }

                },
                error: function (data) {}
            });

        });





    });



    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        container: 'body'
    });



    $("#clearfilterSubmit").click(function ()
    {
        $('#hidden_user_email').val("");
        $('#user_email').val("");
        $('#hidden_signup_method').val("");
        $('#signup_method').val("");
        $('#clearfilter').val("yes");
        $('#filterForm').submit();
    });

</script>
