<div class="dash-counter users-main">
  <h2>Master Option List</h2>
  <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <thead>
          <tr>
            <th>No.</th>
            <th>Master Option</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
          <?php
          if(!empty($msoptions)){
            $cnt = 1;
            foreach($msoptions as $msopt){
              ?>
              <tr>
                <td><?php echo $cnt; ?></td>
                <td><?php echo $msopt['opt_name']; ?></td>
                <td>
                  <a href="<?php echo base_url("Admin/editMasterOption/").$msopt['opt_id']; ?>" class="btn btn-info btn-xs">Edit</a>
                </td>
              </tr>
              <?php
              $cnt++;
            }
          }
          ?>
        </tbody>
      </table>
  </div>
</div>