<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 

<div class="import-create dash-counter">
 <h2><?php echo $user[0]['user_type']; ?> User Details</h2>
 <span>(*) Required Fields</span>                           
                      
 </div>
<!--   </div> -->
<div class="import-create dash-counter">
<!-- <form action="<?php echo base_url() ?>Admin/edituserpro" method="post"> -->

<form action="<?php echo base_url() ?>Admin/edituserpro" name="myForm" method="post">

 <input type="hidden" class="form-control" name="user_id" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_id']; ?>">
 <input type="hidden" class="form-control" name="user_type" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_type']; ?>">
 
    

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="gender-main">*Genre</label>
                    <div class="for-radio">
                      <div class="btn-group" data-toggle="buttons">                        
                        <?php
                        if(!empty($user[0]['user_gender']) && $user[0]['user_gender'] == 'Female'){
                            $fcheck = 'checked="checked"';
                            $mcheck = '';
                        }else{
                            $mcheck = 'checked="checked"';
                            $fcheck = '';
                        }
                        ?>
                        <label class="btn btn-default active">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male">Homme
                        </label>
                        <label class="btn btn-default">
                          <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female">Femme
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                
                </div>
              </div>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <label>*First Name</label>
                      <input type="text" class="form-control " required="" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo $user[0]['user_firstname']; ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                       <label>*Last Name</label>
                      <input type="text" class="form-control "  required="" name="user_lastname" placeholder="NOM"  value="<?php echo $user[0]['user_lastname']; ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                       <label>*Email</label>
                      <input type="email" readonly=""  required="" class="form-control" name="user_email"  placeholder="*Email"  value="<?php echo $user[0]['user_email']; ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                      <label>*Phone No.</label>
                      <input type="text"  required="" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo $user[0]['user_phone']; ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div>


<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                        
            <label>*Address</label>
<input id="autocomplete" placeholder="*ADRESSE" required=""
  name="user_address" class="form-control" onFocus="geolocate()" type="text"  value="<?php echo $user[0]['user_address']; ?>"></input>
   
         <?php echo form_error('user_address','<span class="help-block">','</span>'); ?>
</div>
                </div>
 </div>


    
     <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                       <label>*Company Name</label>
                     <input type="text" class="form-control"  required="" 
                     name="user_company_name" placeholder="*NOM DE L'ENTREPRISE"  value="<?php echo $user[0]['user_company_name']; ?>">
          <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label>*Company Number</label>
                      <input type="text"  required="" class="form-control" 
                      name="user_company_number" placeholder="*NUMÉRO DE SIRET" value="<?php echo $user[0]['user_company_number']; ?>">
                       <?php echo form_error('user_company_number','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <div>
                      <label>LANGUE</label>
                    </div>
                      <?php
                      $lang = $user[0]['user_language'];
                      if(!empty($lang))
                        $lang = explode(",", $lang);                                    
                      else
                        $lang = array();
                      ?>                    
                      <?php
                      $languages = $this->Admin_model->getLanguageRows(array("conditions"=>array("lang_status"=>1)));
                      if(!empty($languages)){
                        foreach($languages as $lang1){
                          ?>                          
                          <input type="checkbox" name="user_language[]" value="<?php echo $lang1['lang_id']; ?>" <?php if(in_array($lang1['lang_id'], $lang)) echo "checked"; ?>>
                          <?php echo ucwords($lang1['lang_name']); ?>                          
                          <?php
                        }
                      }
                      ?>                                
                  </div>
                </div>
              </div>

               <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label>User Categories</label>
                    <input 
                    class='form-control' 
                    readonly 
                    multiple='multiple' 
                    list='categories' 
                    name='user_categories' 
                    id="business_category" 
                    type='text' 
                    placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)"
                    value="<?php echo $this->Admin_model->getCategoriesAndFilters($user[0]['user_id'], 1); ?>">
                  <datalist id="categories">
                      <?php
                      $business_categories = $this->Admin_model->getBusinessCategories(1);
                      foreach($business_categories->result() as $cate){
                        ?>
                        <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                        <?php
                      }
                      ?>
                  </datalist>
                  </div>
                </div>
              </div>
<?php
if($act=="updt")
{
  ?>
    <div class="row">
                <div class="col-md-12">

               
      <br>
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" value="Update" name="updateSubmit" type="submit">Update</button>

                  </div>
                </div>
              </div>
<?php
}
?>
     
   </form>

    </div>
   </div>



<script type="text/javascript">
  $(function() {
  
  $("form[name='myForm']").validate({
  rules: {
      user_firstname: "required",
      user_lastname: "required",
      user_email: {
        required: true,
        email: true
      },
      user_cumpany_name: "required"
      ,
      user_company_number:  {required: true,
        maxlength: 14,
      maxlength: 14},
    },
    // Specify validation error messages
    messages: {
      user_firstname: "Please enter your first name",
      user_lastname: "Please enter your last name",
      user_email: "Please enter a valid email address",
      user_company_name: "Please enter your company name",
      user_company_number:{required: "Please enter your company number", maxlength:"Please enter 14 char of comany number",
    maxlength:"Please enter 14 char of comany number"}
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    