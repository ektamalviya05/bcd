        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Event Type</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <form id="editEventType" enctype="multipart/form-data" method="post" action='<?php echo base_url("Admin/eventTypeview/").$details['evt_type_id']; ?>' data-parsley-validate class="form-horizontal form-label-left">                                            
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Name:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="name" id="name" value="<?php echo $details['evt_type']; ?>">
                          <?php echo form_error('name'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Status:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <select name="status" class="form-control">
                            <option value="">Select Status</option>
                            <option value="1" <?php if($details['evt_status'] == 1) echo "selected"; ?>>Publish</option>
                            <option value="0" <?php if($details['evt_status'] == 0) echo "selected"; ?>>Unpublish</option>
                          </select>
                          <?php echo form_error('status'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">                          
                          <button type="submit" id="catsub" class="btn btn-success">Save</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>                          
                          <div id="catupmsg">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->