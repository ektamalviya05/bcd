<div class="toppad">
  <div class="panel panel-custom user-panel">
    <form name="linkuseroption" id="linkuseroption" autocomplete="off" method="post" action="<?php echo base_url("Admin/linkOptiontoUser/").$userlist['user_id']; ?>">
      <div class="panel-heading">
        <h3 class="panel-title">
        Link Option To User
        </h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-9 col-md-9">
            <table class="table table-user-information">
              <tbody>              
                <tr>
                  <td width="20%">Name:</td>
                  <td>
                    <input readonly class="option-name" type="text" placeholder="User Name" value="<?php echo $userlist['user_firstname'].' '.$userlist['user_lastname']; ?>" />
                    <input type="hidden" name="userid" value="<?php echo $userlist['user_id']; ?>" />
                  </td>
                </tr>
                <tr>
                  <td width="20%">Company:</td>
                  <td>
                    <input readonly class="option-name" type="text" placeholder="User Company" value="<?php echo $userlist['user_company_name']; ?>" />                                     
                  </td>
                </tr>
                <tr>
                  <td width="20%">Company Number:</td>
                  <td>
                    <input readonly class="option-name" type="text" placeholder="User Company Number" value="<?php echo $userlist['user_company_number']; ?>" />                                     
                  </td>
                </tr>
                <tr>
                  <td width="20%">Email:</td>
                  <td>
                    <input readonly class="option-name" type="text" placeholder="User Email" value="<?php echo $userlist['user_email']; ?>" />                                     
                  </td>
                </tr>
                <tr>
                  <td width="20%">Link Option:</td>
                  <td>
                    <!-- <select name="linkoption" id="linkoption" class="form-control">
                      <option value="">Select Option</option>
                      <?php
                      // foreach($masteroption as $opt){
                        ?>
                        <option value="<?php //echo $opt['opt_id']; ?>"><?php //echo $opt['opt_name']; ?></option>
                        <?php
                      // }
                      // if(!empty($packagelist)){
                      //   foreach($packagelist as $pkg){
                          ?>
                          <option value="<?php //echo $pkg['opt_id']; ?>"><?php //echo $pkg['opt_name']; ?></option>
                          <?php
                      //   }
                      // }
                      ?>
                    </select> -->
                    <div id="priceoption">
                    </div>
                    <div class="col-md-12">
                      <h4>General Options</h4>
                      <?php
                      foreach($masteroption as $opt){
                        ?>
                        <div class="mcheckbox" id="mcheckbox_<?php echo $opt['opt_id']; ?>">
                          <div class="col-md-12">
                            <input type="checkbox" name="mst[]" value="<?php echo $opt['opt_id']; ?>" class="opt_mstcheck"> <?php echo $opt['opt_name']; ?>
                            <div class="putpriceselection" style="display: none;">
                              <div>
                                <?php 
                                if($opt['opt_id'] == 6){
                                  ?>
                                  <label class="opt_heading1">Picture Upload Limit:</label>
                                  <input type="number" value="10" min="1" placeholder="Picture Upload Limit" name="prcidph_<?php echo $opt['opt_id'] ?>" class='optionprices form-control'>
                                  <?php
                                }
                                ?>
                                <label class="opt_heading2">Duration:</label>
                                <input type="text" value="7 Days" placeholder="Enter days ie. 7 Days" name="prcid_<?php echo $opt['opt_id'] ?>" class='optionprices form-control'>
                              </div>                                  
                            </div>
                          </div>                                                    
                        </div>
                        <?php
                      }
                      if(!empty($packagelist)){
                        ?>
                        <h4>Packages</h4>
                        <?php
                        foreach($packagelist as $pkg){
                          ?>
                          <div class="mcheckbox" id="mcheckbox_<?php echo $pkg['opt_id']; ?>">
                            <div class="col-md-12">
                              <input type="checkbox" name="pkg[]" value="<?php echo $pkg['opt_id']; ?>" class="opt_mstcheck"> <?php echo $pkg['opt_name']; ?>
                              <div class="putpriceselection" style="display: none;">
                                <div class="main-pkg-dur">
                                  <label>Package Duration:</label>
                                  <input type="text" value="11 Days" placeholder="Enter days ie. 7 Days" name="pkgprcid_<?php echo $pkg['opt_id'] ?>" class='optionprices form-control'>
                                </div>
                                <div class="main-pkg-dur1">
                                  <label class='opt_heading1'>Package Options:</label>
                                  <div class="pkg_details">
                                    <?php
                                    $pkgopts = $this->Admin_model->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$pkg['opt_id'])));
                                    if(!empty($pkgopts)){
                                      foreach($pkgopts as $pkgopt){
                                        ?>
                                        <div class="opt_heading2">
                                          <label class="opt-head-content"><?php echo $pkgopt['opt_name']; ?>:</label>                                        
                                          <div class="opt-heading2-details">
                                            <?php
                                            if($pkgopt['pkg_inc_optid'] == 6){
                                              ?>
                                              <p>Picture Upload Limit:</p>
                                              <input type="number" value="10" min="1" placeholder="Picture Upload Limit" name="prcidph_<?php echo $pkg['opt_id']."_".$pkgopt['pkg_inc_optid'] ?>" class='optionprices form-control'>
                                              <?php
                                            }
                                            ?>
                                            <p>Duration:</p>
                                            <input type="text" value="11 Days" placeholder="Enter days ie. 7 Days" name="pkgprcid_<?php echo $pkg['opt_id']."_".$pkgopt['pkg_inc_optid'] ?>" class='optionprices form-control'>
                                          </div>
                                        </div>                                      
                                        <?php
                                      }
                                    }
                                    ?>
                                  </div>
                                </div>
                              </div>
                            </div>                                                    
                          </div>
                          <?php
                        }
                      }
                      ?>

                    </div> 
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
			<!-- </form> -->
      <div class="panel-footer">
        <span class="pull-right">
          <input type="submit" name="btn_submit" class="btn btn-sm" id="btn_submit" value="Save Option" /> 
          <a href="<?php echo base_url('Admin/linkOptionUserSearch'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
        </span>
        <span class="clearfix"></span>
      </div>            
    </form>
  </div>
</div>