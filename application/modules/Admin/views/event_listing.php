<div class="import-create dash-counter">
  <h2>Filters</h2>
  <form method="post" action="" autocomplete="off">
    <div class="row">
      <div class="form-group col-md-3">
        <input type="email" autocomplete="off" class="form-control" name="user_email" id="user_email"  placeholder="Company Email" value="<?php echo $this->session->userdata("user_email"); ?>">
        <input type="hidden" name="clearfilter" id="clearfilter"  value="">        
      </div>
      <div class="form-group col-md-3">
        <select name="eventtype" class="form-control">
          <option value="">Event Type</option>
          <?php
          if(!empty($eventTypeList)){
            foreach($eventTypeList as $type){
              ?>
              <option value="<?php echo $type['evt_type_id']; ?>" <?php if($type['evt_type_id'] == $this->session->userdata("eventtype")) echo "selected"; ?>><?php echo $type['evt_type']; ?></option>
              <?php
            }
          }
          ?>          
        </select>        
      </div>
      <div class="form-group col-md-3">
        <input type="text" autocomplete="off" class="form-control onlydatepicker" name="stpublish" id="stpublish"  placeholder="Start Publishing"  
         value="<?php echo $this->session->userdata("publishstart"); ?>">        
      </div>
      <div class="form-group col-md-3">
        <input type="text" autocomplete="off" class="form-control onlydatepicker" name="stevent" id="stevent"  placeholder="Start Event"  
         value="<?php echo $this->session->userdata("eventstart"); ?>">        
      </div>      
      <div class="form-group col-md-3">
        <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn btn-primary" value="Filter"/>
        <a href="<?php echo base_url("Admin/clearEventFilter"); ?>" class="btn btn-default">Clear</a>
      </div>
    </div>
    <?php
    if(!empty($this->session->flashdata("success"))){
    ?>
    <div class="row">
      <div class="col-md-12">
        <div class="alert alert-success">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <strong>Success!</strong> <?php echo $this->session->flashdata("success"); ?>
        </div>
      </div>
    </div>
    <?php
    }
    ?>
  </form>
</div>
<div class="dash-counter users-main">
  <h2>Event Lists</h2>

  <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <tbody>
          <?php
          $bur = base_url();
          echo "<tr>";
          echo "<th>Name</th>";
          echo "<th>Company</th>";
          // echo "<th>Type</th>";
          echo "<th>Company Email</th>";
          echo "<th>Start Publish</th>";
          echo "<th>End Publish</th>";
          echo "<th>Start Event</th>";
          echo "<th>End Event</th>";
          // echo "<th>Created By</th>";
          echo "<th>Status</th>";
          echo "<th>Action</th>";
          /*echo "<th>Created On</th>";
          echo "<th>event_status</th>";*/
          echo "</tr>";

          if($record_found>0)
          {
            if(!empty($eventlist) && count($eventlist) > 0){
              foreach ($eventlist as $rows) 
              {
                echo "<tr>";
                echo "<td>".$rows['event_name']."</td>";
                // echo "<td>".$rows['evt_type']."</td>";
                if($rows['user_type'] == "Professional"){
                  if(!empty($rows['bs_name'])){
                    echo "<td>".substr(ucwords($rows['bs_name']), 0, 10)."</td>";
                }
                  else{
                    echo "<td>".ucwords($rows['user_firstname']." ".$rows['user_lastname'])."</td>";
                }
              }
              else
                echo "<td>".ucwords($rows['user_firstname']." ".$rows['user_lastname'])."</td>";
                echo "<td>".$rows['bs_email']."</td>";
                echo "<td>".$rows['event_publish_start']."</td>";
                echo "<td>".$rows['event_publish_end']."</td>";
                echo "<td>".date("Y-m-d H:i", strtotime($rows['event_start_date']))."</td>";
                echo "<td>".date("Y-m-d H:i", strtotime($rows['event_end_date']))."</td>";
               
                if($rows['event_status'] == 0){
                	echo "<td><button type='button' class='btn btn-default btn-xs'>Not Activated</button></td>";
                }else if($rows['event_status'] == 1){
                	echo "<td><button type='button' class='btn btn-primary btn-xs'>Activated</button></td>";
                }else{
                	echo "<td><button type='button' class='btn btn-danger btn-xs'>Removed By User</button></td>";
                }
                echo "<td>
                        <div class='link-del-view'>
                          <div class='tooltip-2'><a href='".base_url("Admin/ViewEvent/").$rows['event_id']."'><i class='fa fa-eye'></i></a><span class='tooltiptext'>View</span></div>
                          <div class='tooltip-2'><a href='".base_url("Admin/EditEvent/").$rows['event_id']."'><i class='fa fa-pencil'></i></a><span class='tooltiptext'>Edit</span></div>
                          <div class='tooltip-2'><a class='trashevent' modal-aria='".$rows['event_id']."' href='javascript:;' data-toggle='confirmation' data-title='Delete this event permanently? '><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div>
                        </div>
                      </td>
                    </tr>";

              }
            }else{
              echo "<tr><td colspan='7'>No Record Found</td></tr>";
            }
          }
          else
          {
            echo "<tr><td colspan='7'>No Record Found</td></tr>";
          }
          //echo "</table>";

          ?>


        </tbody>
      </table>
      <?php


      if(isset($links) && !empty($links))
      {
        echo $links;
      }

      ?>

  </div>
</div>
