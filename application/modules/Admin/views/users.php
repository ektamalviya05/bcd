 <script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script>
 <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
 
<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
$(document).ready(function() {
openModal();
});
function openModal(){
  $('#myModaDel').modal();
}  
</script>
<?php
}
if(isset($activated) && trim($activated) == "yes")
{
 ?>
<script type="text/javascript">
$(document).ready(function() {
openModal();
});
function openModal(){
  $('#myModaAct').modal();
}  
</script>
<?php
}
else if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
$(document).ready(function() {
openModal();
});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>
<?php
}
?>

<div class="loader">
   <center>
       <img class="image-responsive" src="<?php echo base_url(); ?>assets/img/loader.svg" alt="loading..">
   </center>
</div>
<div class="import-create dash-counter">
<h2>Filters</h2>
<div class="row">
  <form action="<?php echo base_url('Admin/personalUserSetDelimiter/'); ?>" method="post" id="filterForm">
    <input type="hidden" name="clearfilter" id="clearfilter"  value="">
    <div class="form-group col-md-4">
      <input type="text" autocomplete="off" class="form-control" name="userfirstname" id="userfirstname"  placeholder="First Name" value="<?php echo $this->session->userdata("serch_perfname"); ?>">         
    </div>
    <div class="form-group col-md-4">
      <input type="text" autocomplete="off" class="form-control" name="userlastname" id="userlastname"  placeholder="Last Name" value="<?php echo $this->session->userdata("serch_perlname"); ?>">         
    </div>
    <div class="form-group col-md-4">
      <input type="email" autocomplete="off" class="form-control" name="user_email" id="user_peremail"  placeholder="Email" value="<?php echo $this->session->userdata("serch_peremail"); ?>">         
    </div>
    <div class="form-group col-md-4" >
      <select id="signup_method" class="form-control" id="signup_method"  name="signup_method">
        <option value="-1">Select Method</option>
        <option value="1"  <?php echo $this->session->userdata("serch_persignupmethod") ==1? "selected":""; ?> >Regular Signup</option>
        <option value="2"  <?php echo $this->session->userdata("serch_persignupmethod") ==2? "selected":""; ?> >Facebook Signup</option>
        <option value="3"  <?php echo $this->session->userdata("serch_persignupmethod") ==3? "selected":""; ?> >Google Signup </option>
      </select>
    </div>
    <div class="form-group col-md-2">
      <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn-primary form-control" value="Filter"/>           
    </div>
     <div class="form-group col-md-2">            
      <a href="<?php echo base_url("Admin/clearPersonalUserSetDelimiter"); ?>" class="btn btn-primary form-control" >Clear Filter</a>
    </div>
  </form>
                           
                       
                    </div>
                    <div  class="form-group" style="float:right;  ">

                    <a href="javascript:void(0)" class="btn btn-primary form-control" id="export-to-csv">Download Users</a>
                    
                    </div>

<?php
//$downurl = base_url(). "Admin/user_list_download/".$usertype;
$downurl = base_url(). "Admin/downloadPerUserCSV";
?>
<form action="<?php echo $downurl; ?>" method="post" id="export-form">
  <input type="hidden" value='<?php echo $this->session->userdata("serch_peremail"); ?>' id='hidden_user_email' name='hidden_user_email'/>
  <input type="hidden" value='<?php echo $this->session->userdata("serch_persignupmethod"); ?>' id='hidden_signup_method' name='hidden_signup_method'/>
  <input type="hidden" value='' id='hidden-type' name='ExportType'/>
</form>

</div>
<div class="dash-counter users-main">
<h2><?php 

if($usertype !="")
{
    echo strtoupper($usertype). " USERS LIST"; 
}
else
{
   $usertype="All";
   echo "All USERS LIST"; 
}


?>
</h2>

                        <div class="user-table table-responsive">
                            <table class="table table-striped table-bordered">
                              <tbody>
                                <tr></tr>
                               

<?php
$bur = base_url();
if($usertype != "Professional")
{
  //This is for Professional Users
echo "<tr>";
echo "<th>First Name</th>";
echo "<th>Last Name</th>";
echo "<th>Email</th>";
echo "<th>Type</th>";
echo "<th>Signup Time</th>";
echo "<th>Last Login</th>";
echo "<th>Status</th>";
echo "<th>Action</th>";
echo "</tr>";

if($record_found>0)
{

foreach ($userlist as $rows) 
{
  $stat= $rows['is_online'] == 1? 'Online': 'Offline';
  
echo "<tr>";
echo "<td>".$rows['user_firstname']."</td>";
echo "<td>".$rows['user_lastname']."</td>";
echo "<td>".$rows['user_email']."</td>";
echo "<td>".$rows['user_type']."</td>";
?>
<td>
<?php
echo !empty($rows['created'])?date("d M, Y H:i", strtotime($rows['created'])):'N/A';
?>  
</td>
<td>
  <?php
    echo !empty($rows['last_login'])?date("d M, Y H:i", strtotime($rows['last_login'])):'N/A';
  ?>  
</td>
<?php
if( $rows['is_online'] == 1)
{
echo "<td style='color:green; font-weight:bold;'>Online</td>";

}
else
{
  echo "<td style='color:red'>Offline</td>";

}
echo "<td><div class='link-del-view'>";
if($rows['user_type'] == "Personal")
{
echo "<div class='tooltip-2'><a href='". $bur."Admin/edituser/".$rows['user_id']."'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span></div><div class='tooltip-2'><a href='". $bur."Admin/edituser/".$rows['user_id']."/updt'><i class='fa fa-edit' aria-hidden='true'></i></a><span class='tooltiptext'>Edit</span></div>";
}
else
{
  echo "<div class='tooltip-2'><a href='". $bur."Admin/edituserpro/".$rows['user_id']."'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span></div><div class='tooltip-2'><a href='". $bur."Admin/edituserpro/".$rows['user_id']."/updt'><i class='fa fa-edit' aria-hidden='true'></i></a><span class='tooltiptext'>Edit</span></div>";
}


                                            
//echo "| <a href='". $bur."Admin/deleteuser/".$rows['user_id']."'>Delete</a> | ";
echo "<div class='tooltip-2'><a  data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."Admin/deleteuser/".$rows['user_id'] . "/". $rows['user_type'] ."' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div> ";
/*
if($rows['status'] == 0)
{
echo "<div class='tooltip-2'><a href='". $bur."Admin/activateuser/".$rows['user_id']."/1'><i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></div>";
}
else
{
echo "<div class='tooltip-2'><a href='". $bur."Admin/activateuser/".$rows['user_id']."/0'><i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></div>"; 
}*/


if($rows['status'] == 0)
{
  echo "<div class='tooltip-2'><a id='actv". $rows['user_id']."'  class='activate' ><i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></div>";
}
else
{
  echo "<div class='tooltip-2'><a id='dact". $rows['user_id']."'  class='activate'><i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></div>"; 
}

echo "<div class='tooltip-2'><a id='rec". $rows['user_id']."' class='resend'><i class='fa fa-refresh' aria-hidden='true'></i></a><span class='tooltiptext'>Send Reset Password Link</span></div> ";

echo "</div></td>";
echo "</tr>";
}

}
else
{
  echo "<tr><td colspan='7'>No Record Found</td></tr>";
}

//End of PErsonal User Section
}
else
{
  //This is for Professional Users

echo "<tr>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/firstname/".$page."'>First Name</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/lastname/".$page."'>Last Name</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/email/".$page."'>Email</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/email/".$page."'>Company</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/email/".$page."'>Number</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/email/".$page."'>Address</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/lastlogin/".$page."'>Status</a></th>";
echo "<th>Action</th>";
echo "</tr>";

if($record_found>0)
{
foreach ($userlist as $rows) 
{
$stat= $rows['is_online'] == 1? 'Online': 'Offline';
echo "<tr>";
echo "<td>".$rows['user_firstname']."</td>";
echo "<td>".$rows['user_lastname']."</td>";
echo "<td>".$rows['user_email']."</td>";
echo "<td>".$rows['user_company_name']."</td>";
echo "<td>".$rows['user_company_number']."</td>";
echo "<td>".$rows['user_company_address']."</td>";
echo "<td>".$stat."</td>";

echo "<td><div class='link-del-view'>";
echo "<div class='tooltip-2'><a href='". $bur."Admin/edituserpro/".$rows['user_id']."'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span></div><div class='tooltip-2'><a href='". $bur."Admin/edituserpro/".$rows['user_id']."/updt'><i class='fa fa-edit' aria-hidden='true'></i></a><span class='tooltiptext'>Edit</span></div>";

echo "<div class='tooltip-2'><a class='delete'  data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."Admin/deleteuser/".$rows['user_id'] . "/". $rows['user_type'] ."' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div> ";


if($rows['status'] == 0)
{
//echo "<div class='tooltip-2'><a id='dact". $rows['user_id']."'  class='activate' href='". $bur."Admin/activateuser/".$rows['user_id']."/1'><i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></div>";
  echo "<div class='tooltip-2'><a id='actv". $rows['user_id']."'  class='activate' ><i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></div>";
}
else
{
  echo "<div class='tooltip-2'><a id='dact". $rows['user_id']."'  class='activate'><i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></div>"; 

//echo "<div class='tooltip-2'><a id='actv". $rows['user_id']."'  class='activate' href='". $bur."Admin/activateuser/".$rows['user_id']."/0'><i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></div>"; 
}


echo "<div class='tooltip-2'><a id='rec". $rows['user_id']."' class='resend'><i class='fa fa-refresh' aria-hidden='true'></i></a><span class='tooltiptext'>Send Reset Password Link</span></div> ";

echo "</div></td>";
echo "</tr>";
}

}
else
{
  echo "<tr><td colspan='7'>No Record Found</td></tr>";
}

  //End for Professional Users
}

echo "</table>";
?>
    <div class="read-more">
      <?php
        if(isset($links) && !empty($links))
        {
          echo $links;
        }
      ?>
    </div>
  </div>
</div>
<script  type="text/javascript">
$(document).ready(function() {
jQuery('#export-to-csv').bind("click", function() {
var target = $(this).attr('id');
switch(target) {
    case 'export-to-csv' :
    $('#hidden-type').val(target);
   // alert($('#signup_method').val());
    $('#hidden_user_email').val($('#user_email').val());
    $('#hidden_signup_method').val($('#signup_method').val());
    $('#export-form').submit();
    $('#hidden-type').val('');
    break
}
});


jQuery('.resend').bind("click", function() {
var vals = 0;
vals = $(this).attr('id').substr(3);
$.ajax({
        url: '<?php echo base_url()."Admin/resetpassmail/"; ?>'+vals,
        type: 'POST',
        data: {
            user_id: vals
        },
     
  beforeSend: function(){
       $('.loader').css("visibility", "visible");
   },
  complete: function(){
        $('.loader').css("visibility", "hidden");
  },
        //dataType: 'json',
        success: function(data) {
           $('#myModaResentMail').modal();
        },
        error: function(data) {
        }

    });
  
});



jQuery('.activate').bind("click", function() {

var vals = 0;
var acts = "";
vals = $(this).attr('id').substr(4);
acts =  $(this).attr('id').substr(0,4);
//alert(vals + " -- " + acts);
if(acts == 'actv')
{
  acts = 1;
}
else
{
  acts = 0;
}


$.ajax({
        url: '<?php echo base_url()."Admin/activateuser/"; ?>'+vals,
        type: 'POST',
        data: {
            user_id: vals,
             action: acts
        },
  beforeSend: function(){
       $('.loader').css("visibility", "visible");
   },
  complete: function(){
        $('.loader').css("visibility", "hidden");
  },
        //dataType: 'json',
        success: function(data) {
                if(acts == 1)
                {
                  $('#myModaAct').modal();
                }
                else
                {
                  $('#myModaDAct').modal();
                }

        },
        error: function(data) {}
    });
  
});




});

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });

$("#clearfilterSubmit").click(function()
  {
  $('#hidden_user_email').val("");
  $('#user_email').val("");
  $('#hidden_signup_method').val("");
  $('#signup_method').val("");
  $('#clearfilter').val("yes");
  $('#filterForm').submit();
  });





  

</script>
<?php
if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
$(document).ready(function() {
openModal();
});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>
  <?php
}
?>