<div class="dash-counter users-main">
  <h2>Review List</h2>

  <div class="user-table table-responsive">
    <table class="table table-striped table-bordered">
      <tbody>
        <?php
        //print_r($reviewlist);

        $bur = base_url();
        echo "<tr>";
        echo "<th>Reviewed By</th>";
        echo "<th>Profile</th>";
        echo "<th>Reviewed On</th>";
        echo "<th>Rating</th>";
        echo "<th>Status</th>";
        echo "<th>Action</th>";
        echo "</tr>";

        if($record_found>0)
        {
        foreach ($reviewlist as $rows) 
        {
        echo "<tr>";
        ?>

        <td><?php echo $rows['user_firstname'].' '.$rows['user_lastname']; ?></td> 
        <td><?php echo $rows['bs_name']; ?> </td> 
        <td><?php echo date("Y-m-d, H:i", strtotime($rows['review_date'])); ?></td>
        <td><?php echo $rows['blog_vote']; ?></td> 
        <!-- <td><?php //echo substr($rows['review_text'], 0, 20); ?></td> -->
        <td>
          <input type="checkbox" class="vot_status" modal-aria="<?php echo $rows['vote_id']; ?>" <?php if($rows['vote_status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">          
        </td>
        <td>  
          <div class="tooltip-2"><a href="<?php echo base_url('Admin/viewreview/').$rows['vote_id']; ?>" ><i class="fa fa-eye text-primary"></i> </a>
            <span class="tooltiptext">Edit or View</span>
          </div>
          <div class="tooltip-2"><a href='javascript:;' data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="delete-review" modal-aria="<?php echo $rows['vote_id']; ?>"><i class="fa fa-trash text-danger"></i> </a>
            <span class="tooltiptext">Delete</span>
          </div>
        </td>


        <?php

        echo "</tr>";

        }
        }
        else
        {
          echo "<tr><td colspan='7'>No Record Found</td></tr>";
        }
        //echo "</table>";

        ?>


      </tbody>
    </table>
    <?php


    if(isset($links) && !empty($links))
    {
      echo $links;
    }

    ?>

  </div>
</div>
