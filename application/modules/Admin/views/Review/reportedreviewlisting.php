 <span id="showreviewdel" data-dismiss="modal" data-toggle="modal" data-target="#myModalalReviewDel"></span>

  <div class="dash-counter users-main">
    <h2>Reported Review List</h2>

    <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <tbody>
          <?php
          //print_r($reviewlist);
          $bur = base_url();
          ?>
          <tr>
            <th>Reviewed By</th>
            <th>Profile</th>
            <th>Reviewed On</th>
            <th>Rating</th>
            <th>Review</th>
            <th>Report Count</th>
            <th>Action</th>
          </tr>
          <?php
          if($record_found>0)
          {
            foreach ($reviewlist as $rows) 
            {
              ?>
              <tr>
                <td><?php echo $rows['votername']; ?></td> 
                <td><?php echo $rows['votedcompany']; ?> </td> 
                <td><?php echo $rows['review_date']; ?></td>
                <td><?php echo $rows['blog_vote']; ?></td> 
                <td><?php echo $rows['review_text']; ?></td>
                <td><?php echo $rows['total_reported']; ?></td>
                <td>
                  <div class="tooltip-2"><a href='javascript:;' data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="delete-review" modal-aria="<?php echo $rows['vote_id']; ?>"><i class="fa fa-trash text-danger"></i> </a>
                    <span class="tooltiptext">Delete</span>
                  </div>
                </td>
              </tr>
              <?php
            }
          }
          else
          {
            echo "<tr><td colspan='7'>No Record Found</td></tr>";
          }
        ?>


        </tbody>
      </table>
      <?php
      if(isset($links) && !empty($links))
      {
        echo $links;
      }
      ?>
    </div>
  </div>

  <div class="modal fade" id="myModalalReviewDel" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header"> 
          <h4 class="modal-title">Review Deleted</h4>
          <div class="title-imge">
            <img src="<?php echo base_url();?>assets/img/logo.png">
          </div>
        </div>
        <div class="modal-body">
          <div class="modal-login-txt">
            You have deleted the review.
          </div>
        </div>
        <div class="modal-footer">
           <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>


<script>
  function deletereview(review_id)
  {
    // $("#showreviewemail").trigger("click");
   	var data = "";
   	data = 'vote_id=' + review_id ;
   	$.ajax({
        type: "POST",
        data: data,
        url: "<?php echo base_url("Admin/delreview"); ?>", //CALLBACK FILE
        success: function (e) {
        	$("#showreviewdel").trigger("click");
        },
        error: function (e) {
            alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
            //$("#showreviewemail").trigger("click");
        }
    });
  }
</script>