
<div class="toppad">
  <div class="panel panel-custom user-panel">
    <div class="panel-heading">
      <h3 class="panel-title"><?php 
        if(!empty($details['bs_name']))
          $username = ucwords($details['bs_name']);
        else
          $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
          echo $username;
        ?>
      </h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3 col-md-3">
          <div class="profile_tag">
            <img alt="User Pic" src="<?php echo base_url("assets/profile_pics/").$details['user_profile_pic']; ?>" class="img-responsive main-pro_img">
          </div>
        </div>
        <div class="col-sm-9 col-md-9">
          <table class="table table-user-information">
            <tbody>
              <tr>
                <td>Review To:</td>
                <td>
                  <?php 
                    if(!empty($details['bs_name']))
                      $username = ucwords($details['bs_name']);
                    else
                      $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
                      echo $username;
                  ?>
                </td>
              </tr><tr>
                <td>Review By:</td>
                <td>
                  <?php 
                    echo ucwords($details['user_firstname']." ".$details['user_lastname']);
                  ?>
                </td>
              </tr>
              <tr>
                <td>Email:</td>
                <td>
                  <?php 
                    echo $details['user_email'];
                  ?>                        
                </td>
              </tr>
              <tr>
                <td>Phone Number: </td>
                <td><?php echo $details['user_phone']; ?></td>
              </tr>              
              <tr>
                <td>Rating: </td>
                <td>
                  <?php echo $details['blog_vote']; ?>
                </td>
              </tr>
              <tr>
                <td>Review: </td>
                <td>
                  <?php echo $details['review_text']; ?>
                </td>
              </tr>
              <tr>
                <td>Review Date: </td>
                <td>
                  <?php echo date("Y-m-d, H:i", strtotime($details['review_date'])); ?>
                </td>
              </tr>
              <tr>
                <td>Publish on Profile: </td>
                <td>
                  <input type="checkbox" class="vot_status" modal-aria="<?php echo $details['vote_id']; ?>" <?php if($details['vote_status']) echo "checked"; ?> data-on="Publish" data-off="Unpublish" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">
                  <div id="rwmsg"></div>
                </td>
              </tr>              
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <span class="pull-right">
          <a href="<?php echo base_url('Admin/reviewListing'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
      </span>
      <span class="clearfix"></span>
    </div>
  </div>
</div>