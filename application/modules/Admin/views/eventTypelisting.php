				<div class="dash-counter users-main">
                    <h2>Event Lists</h2>

                    <div class="user-table table-responsive">
                        <table class="table table-striped table-bordered">
                            <tbody>
                            	<tr>
                            		<th>Name</th>
                            		<th>Action</th>
                            	</tr>                            	
                            		<?php
                            		foreach($details as $evt){
                            			?>
                            			<tr>
	                            			<td><?php echo $evt['evt_type']; ?></td>
	                            			<td>
	                            				<a class="btn btn-primary btn-xs" href="<?php echo base_url("Admin/eventTypeview/").$evt['evt_type_id']; ?>"><i class="fa fa-edit"></i> Edit</a>
	                            				<button class="btn btn-danger btn-xs dleventtype" modal-aria="<?php echo $evt['evt_type_id']; ?>" data-toggle='confirmation' data-title='Are you sure to delete?'><i class="fa fa-trash"></i> Delete</button>
	                            			</td>	
	                            		</tr>
                            			<?php
                            		}
                            		?>
							</tbody>
						</table>
							<?php


							// if(isset($links) && !empty($links))
							// {
							//   echo $links;
							// }

							?>

					</div>
				</div>
