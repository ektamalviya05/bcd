<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<div class="toppad">
  <div class="panel panel-custom user-panel">
    <form name="frmCreateOption" autocomplete="off" method="post" action="<?php echo base_url("Admin/editPackage/").$packageDetails['opt_id']; ?>">
      <div class="panel-heading">
        <h3 class="panel-title">
        Edit Package
        </h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <table class="table table-user-information">
              <tbody>              
                <tr>
                  <td>Package Name:</td>
                  <td>
                    <input class="option-name" type="text" name="txt_option_name" id="txt_option_name" placeholder="Package Name" value="<?php echo $packageDetails['opt_name']; ?>" />
                    <?php echo form_error("txt_option_name"); ?>                    
                  </td>
                </tr>
                                 
                <tr>
                  <td>Package Description:</td>
                  <td>
                    <textarea class="opetion-describe" name="txt_option_description" id="txt_option_description" cols="30" rows="5" placeholder="Package Description"><?php echo $packageDetails['opt_description']; ?></textarea>
                    <?php echo form_error("txt_option_description"); ?>                    
                  </td>
                </tr>
                <tr>
                  <td>Package Price:</td>
                  <td>
                    <input class="option-name" type="text" name="text_opt_price" id="text_opt_price" placeholder="Package Price" value="<?php echo $packageDetails['price']; ?>" />
                    <?php echo form_error("text_opt_price"); ?> 
                  </td>
                </tr>
                 <tr>
                  <td>Package Duration:</td>
                  <td>
                    <input class="option-name" type="text" name="text_opt_duration" id="text_opt_price" placeholder="Package Duration" value="<?php echo $packageDetails['duration']; ?>" />
                    <?php echo form_error("text_opt_duration"); ?> 
                  </td>
                </tr>
                
                <tr>
                  <td>Package Status:</td>
                  <td>
                    <select name="status" class="form-control">
                      <option value="">Select Status</option>
                      <option value="1" <?php if($packageDetails['opt_status']) echo "selected"; ?> >Publish</option>
                      <option value="0" <?php if(!$packageDetails['opt_status']) echo "selected"; ?> >Unpublish</option>
                    </select>
                    <?php echo form_error("status"); ?>                    
                  </td>
                </tr>                
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- </form> -->
      <div class="panel-footer">
        <span class="pull-right">
          <input type="submit" name="btn_submit" class="btn btn-sm" id="btn_submit" value="Save Option" /> 
          <a href="<?php echo base_url('Admin/option_list'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
        </span>
        <span class="clearfix"></span>
      </div>            
    </form>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace( 'txt_option_description', {
    language: 'en',
    uiColor: '#9AB8F3',
    allowedContent:true,
  });
</script>

<script>
  function displayContent(id){
     newclass = "div#priceselection_feature_"+id;
     //newclass = "div.putpriceselection"+id; 
    if($('.feature_ads:checked')){
        
        $(".other").css('display','none'); 
        $(newclass).css('display','block');   
      /*$(newclass).css('display','block');
      $(newclass).removeClass("other");
      $(".other").css('display','none');*/
    }else{
       $(".other").css('display','none');
    }
    
     
     $(".feature_ads:checkbox").not("#"+id+":checkbox").prop('checked',false);
     $(".feature_ads:checkbox").not("#"+id+":checkbox").prop('checked') = false;
  
  }


  function chkOperation(id){
     //$(".free_ads:checkbox").prop('checked',false);
     //$(".free_ads:checkbox").prop('checked') = false;
    //newclass = "div.putpriceselection"+id; 
    newclass = "div#putpriceselection"+id; 
    if($('.free_ads:checked')){
        
        $(".free_other").css('display','none'); 
        $(newclass).css('display','block');

      /*$(newclass).css('display','block');
      $(newclass).removeClass("other");
      $(".other").css('display','none');*/
    }else{
       $(".free_other").css('display','none');
    }
    

     $(".free_ads:checkbox").not("#"+id+":checkbox").prop('checked',false);
     $(".free_ads:checkbox").not("#"+id+":checkbox").prop('checked') = false;

    

    

     
  }
</script>