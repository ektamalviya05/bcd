
<div class="toppad">
  <div class="panel panel-custom user-panel">
    <div class="panel-heading">
      <h3 class="panel-title"><?php 
        if(!empty($details['bs_name']))
          $username = ucwords($details['bs_name']);
        else
          $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
          echo $username;
        ?>
      </h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-sm-3 col-md-3">
          <div class="profile_tag">
            <img alt="User Pic" src="<?php echo base_url("assets/profile_pics/").$details['user_profile_pic']; ?>" class="img-responsive main-pro_img">
          </div>
        </div>
        <div class="col-sm-9 col-md-9">
          <table class="table table-user-information">
            <tbody>
              <tr>
                <td>User:</td>
                <td>
                  <?php 
                    if(!empty($details['bs_name']))
                      $username = ucwords($details['bs_name']);
                    else
                      $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
                    echo $username;
                  ?>
                </td>
              </tr>
              <tr>
                <td>Email:</td>
                <td>
                  <?php 
                    echo $details['user_email'];
                  ?>                        
                </td>
              </tr>
              <tr>
                <td>Phone Number: </td>
                <td><?php echo $details['user_phone']; ?></td>
              </tr>
              <tr>
                <td>Purchase ID: </td>
                <td><?php echo $details['pyt_id']; ?></td>
              </tr>
              <tr>
                <td>Package Name:</td>
                <td><?php echo $details['opt_name']; ?></td>
              </tr>
              <tr>
                <td>Package Description:</td>
                <td>
                  <?php echo $details['opt_description'];
                  ?>
                  <h3>Options</h3>
                  <?php
                  foreach($options as $opts){
                    ?>
                    <div class="">
                      <label><?php echo $opts['opt_name']; ?>:</label>
                      <span>
                        <?php echo $opts['otp_option_duration']; 
                        if($opts['opt_option_status'] == 1)
                          echo " : Activated";
                        ?>
                        
                      </span>

                    </div>
                    <?php
                  }
                  ?>
                  
                </td>
              </tr>
              <tr>
                <td>Purchase Date:</td>
                <td><?php echo $details['opt_option_purchase_date']; ?></td>
              </tr>
              <tr>
                <td>Durations: </td>
                <td>
                  <?php 
                    if($details['opt_option_id'] == 7){
                      if($details['event_is_free'])
                        echo "Free Event";
                      else
                        echo $details['otp_option_duration']; 
                    }else{
                      echo $details['otp_option_duration'];
                    }
                  ?>
                      
                </td>
              </tr>                   
              <tr>                             
                <td>Total Price: </td>
                <td><?php echo empty($details['opt_option_price'])?'--':$details['opt_option_price']; ?></td>
              </tr>
              <tr>                             
                <td>Payment Status: </td>
                <td><?php echo $details['pyt_txn_status']; ?></td>
              </tr>
              <?php
                if($details['pyt_txn_status'] == "Completed"){
                ?>
                  <tr>                             
                    <td>Payment Date: </td>
                    <td><?php echo date("d M, Y H:i", strtotime($details['pyt_date'])); ?></td>
                  </tr>
                  <tr>                             
                    <td>Payer Email: </td>
                    <td><?php echo empty($details['pyt_email'])?'--':$details['pyt_email']; ?></td>
                  </tr>
                  <tr>                             
                    <td>Transaction Id: </td>
                    <td><?php echo empty($details['pyt_txn_id'])?'--':$details['pyt_txn_id']; ?></td>
                  </tr>
                <?php
                }
                ?>                  
              <tr>
                <td>Option Status: </td>
                <td><?php 
                  if($details['opt_option_status'] == 1)
                    echo "Activate";                      
                  else if($details['opt_option_status'] == 3)
                    echo "Expired";
                  else
                    echo "Not Activate";

                // echo $details['opt_option_status'] == 1?'Activate':$details['opt_option_status']==0?'Not Activate':"Expired"; ?></td>
              </tr>
              <?php
              if($details['opt_option_status'] == 1 || $details['opt_option_status'] == 3){
                ?>
                  <tr>
                    <td>Activate Date: </td>
                    <td><?php echo $details['opt_option_active_date']; ?></td>
                  </tr>
                  <tr>
                    <td>Expire Date: </td>
                    <td><?php echo $details['opt_option_end_date']; ?></td>
                  </tr>
                <?php
              }
              if(($details['opt_option_id'] == 7 || $details['opt_option_id'] == 7 ) && $details['opt_option_status'] != 0){
                if($details['event_status'] == 1){
                  ?>
                  <tr>
                    <td>Event Name</td>
                    <td><a href="<?php if($details['event_status'] == 1 ) echo base_url("Admin/ViewEvent/").$details['event_id']; else echo "javascript:;" ?>" target="_blank" ><?php echo $details['event_name']; ?></a></td>
                  </tr>
                  <tr>
                    <td>Event Publishing Start Date</td>
                    <td><?php echo $details['event_publish_start']; ?></td>
                  </tr>
                  <tr>
                    <td>Event Publishing End Date</td>
                    <td><?php echo $details['event_publish_start']; ?></td>
                  </tr>
                  <tr>
                    <td>Event Start Date</td>
                    <td><?php echo date("Y-m-d H:i", strtotime($details['event_start_date']));  ?></td>
                  </tr>
                  <tr>
                    <td>Event End Date</td>
                    <td><?php echo date("Y-m-d H:i", strtotime($details['event_end_date']));  ?></td>
                  </tr>
                  <?php
                }
              }
              ?>

            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <span class="pull-right">
          <a href="<?php echo base_url('Admin/purchase_list'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
      </span>
      <span class="clearfix"></span>
    </div>            
  </div>
</div>