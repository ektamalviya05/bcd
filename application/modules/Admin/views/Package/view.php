<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<div class="toppad">
  <div class="panel panel-custom user-panel">
    <a href="<?php echo base_url('Admin/option_list'); ?>" class="btn btn-sm btn-info pull-right"><i class="fa fa-arrow-left"></i> Back</a>
    <form name="frmCreateOption" autocomplete="off" method="post" action="<?php echo base_url("Admin/editPackage/").$packageDetails['opt_id']; ?>">
      <div class="panel-heading">
        <h3 class="panel-title">
         <?php echo $packageDetails['opt_name']; ?> Detail

        </h3>

      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <table class="table table-user-information">
              <tbody>              
                <tr>
                  <td>Package Name:</td>
                  <td>
                    <input class="option-name" type="text" name="txt_option_name" id="txt_option_name" placeholder="Package Name" value="<?php echo $packageDetails['opt_name']; ?>" readonly />
                    <?php echo form_error("txt_option_name"); ?>                    
                  </td>
                </tr>
                                 
                <tr>
                  <td>Package Description:</td>
                  <td>
                    <div class="opetion-describe" cols="30" rows="5" placeholder="Package Description"><?php echo $packageDetails['opt_description']; ?></div>
                    <?php echo form_error("txt_option_description"); ?>                    
                  </td>
                </tr>
                <tr>
                  <td>Include Options</td>
                  <td>
                    <div class="">
                      <?php
                      foreach($masteroption as $opt){
                        $check = $this->Admin_model->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$packageDetails['opt_id'], "pkg_inc_optid"=>$opt['opt_id']), "returnType"=>"single" ));
                       
                        ?>
                        
                        <div class="mcheckbox" id="mcheckbox_<?php echo $opt['opt_id']; ?>">
                          <div class="col-md-12">
                            <input type="checkbox" disabled="true" name="mst[]" <?php if(!empty($check)) echo "checked"; ?> value="<?php //echo $opt['opt_id']; ?>" class="opt_mstcheck" readonly> <?php echo $opt['opt_name']; ?>

                           
                            <div class="putpriceselection" style="<?php if(empty($check)) echo "display: none;"?>">
                              <div>
                                <!-- <?php 
                                if($opt['opt_id'] == 6){
                                  ?>
                                  <label class="opt_heading1">Picture Upload Limit:</label>
                                  <input type="number" value="<?php echo $check['pkg_opt_qty']; ?>" min="1" placeholder="Picture Upload Limit" name="prcidph_<?php echo $opt['opt_id'] ?>" class='optionprices form-control'>
                                  <?php
                                }
                                ?> -->
                                <label class="opt_heading2">Duration:</label>
                                <input type="text" value="<?php echo $check['pkg_opt_duration']; ?>"  readonly placeholder="Enter days ie. 7 Days" name="prcid_<?php echo $opt['opt_id'] ?>" class='optionprices form-control' readonly>
                              </div>                                  
                            </div>
                          </div>                                                    
                        </div>
                        <?php
                      }
                      ?>
                    </div>                    
                  </td>
                </tr>
                <tr>
                  <td>Package Price:</td>
                  <td>
                    <input class="option-name" type="text" name="text_opt_price" id="text_opt_price" readonly placeholder="Package Price" value="<?php echo $packageDetails['opt_price']; ?>" />
                    <?php echo form_error("text_opt_price"); ?> 
                  </td>
                </tr>
                <tr>
                  <td>Package Price Option Text:</td>
                  <td>
                    <input class="option-name" type="text" name="text_opt_text"  readonly id="text_opt_text" placeholder="Package Price Option Text" value="<?php echo $packageDetails['opt_text']; ?>" />
                    <?php echo form_error("text_opt_text"); ?> 
                  </td>
                </tr>
                <tr>
                  <td>Package Status:</td>
                  <td>
                    <select name="status" class="form-control" disabled="true">
                      <option value="">Select Status</option>
                      <option value="1" <?php if($packageDetails['opt_status']) echo "selected"; ?> >Publish</option>
                      <option value="0" <?php if(!$packageDetails['opt_status']) echo "selected"; ?> >Unpublish</option>
                    </select>
                    <?php echo form_error("txt_option_description"); ?>                    
                  </td>
                </tr>                
              </tbody>
            </table>
          </div>
        </div>
      </div>
			<!-- </form> -->
      <div class="panel-footer">
        <span class="pull-right">
          <input type="submit" name="btn_submit" class="btn btn-sm hidden" id="btn_submit" value="Save Option" /> 
         
        </span>
        <span class="clearfix"></span>
      </div>            
    </form>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace( 'txt_option_description', {
    language: 'en',
    uiColor: '#9AB8F3',
    allowedContent:true,
  });
</script>