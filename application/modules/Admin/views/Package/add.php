<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<div class="toppad">
  <a href="javascript:window.history.back()" class="btn btn-default btn-back pull-right"> <i class="fa fa-arrow-left"></i> Back </a>
  <div class="panel panel-custom user-panel">
    <form name="frmCreateOption" autocomplete="off" method="post" action="<?php echo base_url("Admin/addPackage"); ?>">
      <div class="panel-heading">
        <h3 class="panel-title">
        Create Featured Package
        </h3>
      </div>
      <div class="panel-body">
        <div class="row">
          <div class="col-sm-12 col-md-12">
            <table class="table table-user-information">
              <tbody>              
                <tr>
                  <td>Package Name:</td>
                  <td>
                    <input class="option-name" type="text" name="package_name" id="txt_option_name" placeholder="Package Name" value="<?php echo set_value("package_name"); ?>" />
                    <?php echo form_error("package_name"); ?>                    
                  </td>
                </tr>
                                 
                <tr>
                  <td>Package Description:</td>
                  <td>
                    <textarea class="opetion-describe" name="package_desc" id="txt_option_description" cols="30" rows="5" placeholder="Package Description"><?php echo set_value("package_desc"); ?></textarea>
                    <?php echo form_error("package_desc"); ?>                    
                  </td>
                </tr>     
                <tr>
                  <td>Package Price:</td>
                  <td>
                    <input class="option-name" type="text" name="package_price" id="text_opt_price" placeholder="Package Price" value="<?php echo set_value("package_price"); ?>" />
                    <?php echo form_error("package_price"); ?> 
                  </td>
                </tr>
                 <tr>
                  <td>Package Duration:</td>
                  <td>
                    <input class="option-name" type="text" name="package_validity" id="text_opt_price" placeholder="Package Duration" value="<?php echo set_value("package_validity"); ?>" />
                    <?php echo form_error("package_validity"); ?> 
                  </td>
                </tr>
                <tr>
                  <td>Package Status:</td>
                  <td>
                    <select name="status" class="form-control">
                      <option value="">Select Status</option>
                      <option value="1">Publish</option>
                      <option value="0">Unpublish</option>
                    </select>
                    <?php echo form_error("status"); ?>                    
                  </td>
                </tr>                
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <!-- </form> -->
      <div class="panel-footer">
        <span class="pull-right">
          <input type="submit" name="btn_submit" class="btn btn-sm" id="btn_submit" value="Save Package" /> 
          
        </span>
        <span class="clearfix"></span>
      </div>            
    </form>
  </div>
</div>
<script type="text/javascript">
  CKEDITOR.replace( 'txt_option_description', {
    language: 'en',
    uiColor: '#9AB8F3',
    allowedContent:true,
  });
</script>

