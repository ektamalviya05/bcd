<div class="toppad">
  <div class="panel panel-custom user-panel">
    <div class="panel-heading">
      <h3 class="panel-title"><?php 
          $username = ucwords($details['bs_name']);
          echo $username;
        ?>
      </h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <!-- <div class="col-sm-3 col-md-3">
          <div class="profile_tag">
            <img alt="User Pic" src="<?php echo base_url("assets/profile_pics/").$details['user_profile_pic']; ?>" class="img-responsive main-pro_img">
          </div>
        </div> -->
        <div class="col-sm-12 col-md-12">
          <table class="table table-user-information option_view">
            <tbody>
              <tr>
                <td >Business:</td>
                <td>
                  <?php 
                    if(!empty($details['bs_name']))
                      $username = ucwords($details['bs_name']);
                    else
                      $username = ucwords($details['user_firstname']." ".$details['user_lastname']);
                    echo $username;

                 //  print_r($details);
                  ?>
                </td>
              </tr>
              <tr>
                <td>Business Number:</td>
                <td><?php echo $details['bs_comp_number']; ?></td>
              </tr>
              <tr>
                <td>Email:</td>
                <td>
                  <?php 
                    echo $details['emailTo'];
                  ?>                        
                </td>
              </tr>
                         
              <tr>
                <td>Option Name:</td>
                <td><?php echo $details['opt_name']; ?></td>
              </tr>
              <tr>
                <td>Option Description:</td>
                <td><?php echo $details['Description']; ?></td>
              </tr>
            

              <tr>
                <td>Submit Date:</td>
                <td><?php echo $details['StartDate']; ?></td>
              </tr>

              <tr>                             
                <td>Budget: </td>
                <td><?php echo $details['Budget']; ?></td>
              </tr>
                        


              <tr>
                    <td>Purchase Date: </td>
                    <td><?php echo $details['opt_create_date']; ?></td>
              </tr>

                      
              <!-- <tr>
                <td>Option Status: </td>
                <td><?php 
                  if($details['opt_status'] == 1)
                    echo "Activate";                      
                  else if($details['opt_status'] == 3)
                    echo "Expired";
                  else
                    echo "Not Activate";

                // echo $details['opt_option_status'] == 1?'Activate':$details['opt_option_status']==0?'Not Activate':"Expired"; ?></td>
              </tr> -->
              <?php
             /*  if($details['opt_status'] == 1 || $details['opt_status'] == 3){
                ?>
                  <tr>
                    <td>Activate Date: </td>
                    <td><?php echo $details['opt_status']; ?></td>
                  </tr>
                  <tr>
                    <td>Expire Date: </td>
                    <td><?php echo $details['opt_status']; ?></td>
                  </tr>
                <?php
              }*/
              ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="panel-footer">
      <span class="pull-right">
          <a href="<?php echo base_url('Admin/purchase_list'); ?>" class="btn btn-sm btn-info"><i class="fa fa-arrow-left"></i> Back</a>
      </span>
      <span class="clearfix"></span>
    </div>            
  </div>
</div>