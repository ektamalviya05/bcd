<style>
.toggle input[type="checkbox"] {
    display: inline!important;
}
</style>
<div class="import-create dash-counter">
  <h2>Filters</h2>
  <form method="post" action="" autocomplete="off">
    <div class="row">
      <div class="form-group col-md-3">
        <input type="email" autocomplete="off" class="form-control" name="user_email" id="user_email"  placeholder="Email"  
         value="<?php echo $this->session->userdata("user_email"); ?>">
        <input type="hidden" name="clearfilter" id="clearfilter"  value="">        
      </div>
      <div class="form-group col-md-3">
        <select name="option" class="form-control">
          <option value="">Select Options</option>
          <?php
          if(!empty($optionList)){
            foreach($optionList as $opt){
              ?>
              <option value="<?php echo $opt['opt_id']; ?>" <?php if($opt['opt_id'] == $this->session->userdata("option")) echo "selected"; ?>><?php echo $opt['opt_name']; ?></option>
              <?php
            }
          }
          ?>          
        </select>
        <input type="hidden" name="clearfilter" id="clearfilter"  value="">        
      </div>
      <div class="form-group col-md-3">
        <input type="text" autocomplete="off" class="form-control onlydatepicker" name="expiry" id="expiry"  placeholder="End Date"  
         value="<?php echo $this->session->userdata("expiry"); ?>">        
      </div>
      <div class="form-group col-md-3">
        <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn btn-primary" value="Filter"/>
        <a href="<?php echo base_url("Admin/clearoptionFilter"); ?>" class="btn btn-default">Clear</a>
      </div>
    </div>
  </form>
</div>


<div class="dash-counter users-main">
  <h2>Purchased Options Lists</h2>
  <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <tbody>
          
          <?php
          $bur = base_url();
          ?>
          <tr>
          <th>Business</th>
          <th>Option</th>
          <th>Purchase</th>
          <th>Activation</th>
          <th>End Date</th>
          <th>Status</th>
          <th>Action</th>
          </tr>
          <?php
         
          if(!empty($purchase_list)){
            foreach ($purchase_list as $rows) 
            {
              //print_r($rows);

            	$activstate = $rows['opt_option_status']==1?'Yes':'No';
            	$activeaction = $rows['opt_option_status']==1?'Deactivate':'Activate';
              ?>
              <tr>
                <td><?php echo $rows["user_company_name"]; ?></td>
                <td><?php echo $rows['opt_name']; ?></td>
                <td><?php echo $rows['opt_option_purchase_date']; ?></td>
                <td><?php echo $rows['opt_option_active_date']; ?></td>
                <td><?php echo $rows['opt_option_end_date']; ?></td>
                <!-- <td><?php //echo $activstate; ?></td> -->            
                <td>
                  <?php
                  if($rows['opt_option_status'] == 1 || $rows['opt_option_status'] == 0){
                    ?>
                    <input type="checkbox" class="opt_status" modal-aria="<?php echo $rows['opt_user_option_id']; ?>" 
                    <?php if($rows['opt_option_status']) echo "checked"; ?> data-on="Activated" data-off="Deactivated" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">
                    <?php
                  }else{
                    ?>
                    <input type="checkbox" disabled data-off="Expired" data-toggle="toggle" data-offstyle="danger">
                    <?php
                  }
                  ?>
                </td>
                <td>
                  <?php $tstid = $rows['opt_user_option_id']; ?>
                  <div class="tooltip-2"><a class="btn btn-info btn-xs" href="<?php echo base_url("Admin/optionView/$tstid"); ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <span class="tooltiptext">View</span>
                  </div>
                  <div class="tooltip-2"><button class="btn btn-danger btn-xs dltopt" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    <span class="tooltiptext">Delete</span>
                  </div>
                </td>
                  <?php
                  // if($rows['opt_option_status'] == 0)
                  // {
                  //   echo "<td><a id='actv". $rows['opt_user_option_id']."'  class='activate' >
                  //   <i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></td>";
                  // }
                  // else
                  // {
                  //   echo "<td><a id='dact". $rows['opt_user_option_id']."'  class='activate'>
                  //   <i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></td>"; 
                  // }
                  ?>
              </tr>
              <?php
            }
          }else{
            
          }
          ?>
        </tbody>
      </table>
  </div>
</div>


<div class="dash-counter users-main">
  <h2>Form Options Lists</h2>
  <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <tbody>
          
          <?php
          $bur = base_url();
          ?>
          <tr>
          <th>Business</th>
          <th>Company Number</th>
          <th>Option</th>
          <th>Submit Date</th>
          <th>Budget</th>
          <!-- <th>Status</th> -->
          <th>Action</th>
          </tr>
          <?php
           //print_r($user_purchasd_adv_options);
          ?>
          <?php
         
          if(!empty($user_purchasd_adv_options)){
            foreach ($user_purchasd_adv_options as $rows) 
            {
            	$activstate = $rows['status']==1?'Yes':'No';
            	$activeaction = $rows['status']==1?'Deactivate':'Activate';
              ?>
              <tr>
                <td><?php echo $rows["bs_name"]; ?></td>
                <td><?php echo $rows['bs_comp_number']; ?></td>
                <td><?php echo $rows['opt_name']; ?></td>
                <td><?php echo $rows['StartDate']; ?></td>
                <td><?php echo $rows['Budget']; ?></td>
                <!-- <td><?php echo $activstate; ?></td>             -->
                <!-- <td>
                  <?php
                  if($rows['status'] != 3){
                    ?>
                    <input type="checkbox" class="opt_status" modal-aria="<?php echo $rows['frm_option_id']; ?>" <?php if($rows['status']) echo "checked"; ?> data-on="Activated" data-off="Deactivated" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">
                    <?php
                  }else{
                    ?>
                    <input type="checkbox" disabled data-off="Expired" data-toggle="toggle" data-offstyle="danger">
                    <?php
                  }
                  ?>
                </td> -->
                <td>
                  <?php $tstidval = $rows['frm_option_id']; ?>
                  <div class="tooltip-2"><a class="btn btn-info btn-xs" href="<?php echo base_url("Admin/formOptionView/$tstidval"); ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <span class="tooltiptext">View</span>
                  </div>
                  <!-- <div class="tooltip-2"><button class="btn btn-danger btn-xs dltopt" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" modal-aria="<?php echo $tstidval; ?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    <span class="tooltiptext">Delete</span>
                  </div> -->
                </td>
                  
              </tr>
              <?php
            }
          }else{
            
          }
          ?>
        </tbody>
      </table>
  </div>
</div>

<div class="dash-counter users-main">
  <h2>Purchased Packages Lists</h2>
  <div class="user-table table-responsive">
      <table class="table table-striped table-bordered">
        <tbody>
          
          <?php
          $bur = base_url();
          ?>
          <tr>
          <th>Business</th>
          <th>Option</th>
          <th>Purchase</th>
          <th>Activation</th>
          <th>End Date</th>
          <th>Status</th>
          <th>Action</th>
          </tr>
          <?php
         
          if(!empty($purchase_package_list)){
            foreach ($purchase_package_list as $rows) 
            {
              //print_r($rows);

              $activstate = $rows['opt_option_status']==1?'Yes':'No';
              $activeaction = $rows['opt_option_status']==1?'Deactivate':'Activate';
              ?>
              <tr>
                <td><?php echo $rows["user_company_name"]; ?></td>
                <td><?php echo $rows['opt_name']; ?></td>
                <td><?php echo $rows['opt_option_purchase_date']; ?></td>
                <td><?php echo $rows['opt_option_active_date']; ?></td>
                <td><?php echo $rows['opt_option_end_date']; ?></td>
                <!-- <td><?php //echo $activstate; ?></td> -->            
                <td>
                  <?php
                  if($rows['opt_option_status'] == 1 || $rows['opt_option_status'] == 0){
                    ?>
                    <input type="checkbox" class="opt_status" modal-aria="<?php echo $rows['opt_user_option_id']; ?>" 
                    <?php if($rows['opt_option_status']) echo "checked"; ?> data-on="Activated" data-off="Deactivated" data-toggle="toggle" data-onstyle="primary" data-offstyle="warning">
                    <?php
                  }else{
                    ?>
                    <input type="checkbox" disabled data-off="Expired" data-toggle="toggle" data-offstyle="danger">
                    <?php
                  }
                  ?>
                </td>
                <td>
                  <?php $tstid = $rows['opt_user_option_id']; ?>
                  <div class="tooltip-2"><a class="btn btn-info btn-xs" href="<?php echo base_url("Admin/purchasedPackageView/$tstid"); ?>" ><i class="fa fa-eye" aria-hidden="true"></i></a>
                    <span class="tooltiptext">View</span>
                  </div>
                  <div class="tooltip-2"><button class="btn btn-danger btn-xs dltopt" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" modal-aria="<?php echo $tstid; ?>"><i class="fa fa-trash" aria-hidden="true"></i></button>
                    <span class="tooltiptext">Delete</span>
                  </div>
                </td>
                  <?php
                  // if($rows['opt_option_status'] == 0)
                  // {
                  //   echo "<td><a id='actv". $rows['opt_user_option_id']."'  class='activate' >
                  //   <i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></td>";
                  // }
                  // else
                  // {
                  //   echo "<td><a id='dact". $rows['opt_user_option_id']."'  class='activate'>
                  //   <i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></td>"; 
                  // }
                  ?>
              </tr>
              <?php
            }
          }else{
            
          }
          ?>
        </tbody>
      </table>
  </div>
</div>
