<div class="dash-counter users-main">
	<?php
	if(!empty($this->session->flashdata("success")))
	{
		?>
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-success">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
					<strong>Success!</strong> <?php echo $this->session->flashdata("success"); ?>
				</div>
			</div>
		</div>
		<?php
	}
	if(!empty($this->session->flashdata("error")))
	{
	?>
	<div class="row">
		<div class="col-md-12">
			<div class="alert alert-danger">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>Error!</strong> <?php echo $this->session->flashdata("error"); ?>
			</div>
		</div>
	</div>
	<?php
	}
	?>
<div class="dash-counter users-main">
	<h2>Packages Lists</h2>
	<div class="user-table table-responsive pckeg-listng">
		<div class="sucsessmsg"></div>
		<table class="table table-striped table-bordered">
			<tbody>
				
				<?php
				$bur = base_url();
				echo "<tr>";
				echo "<th>Package Name</th>";
				echo "<th>Description</th>";
				echo "<th>Duration</th>";
				echo "<th>Action</th>";
				echo "</tr>";

				foreach ($option_list_2_3 as $rows) 
				{
					
						echo "<tr>";
						echo "<td>".$rows["opt_name"]."</td>";
						
						echo "<td>".strip_tags(substr($rows["opt_description"],0,50))."</td>";
						echo "<td>".$rows["duration"]."</td>";
						if($rows["opt_id"]==6)
						{
							echo "<td></td>";
						}
						else
						{
							echo "<td><a href='". base_url()."Admin/editPackage/".$rows["opt_id"]."'<i class='fa fa-edit'></i></a> &nbsp;";
							//echo "<a href='". base_url()."Admin/viewPackage/".$rows["opt_id"]."'><i class='fa fa-eye'></i></a> &nbsp;";
							echo "<a class='delete'  data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."Admin/deletePackage/".$rows['opt_id'] . "' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i> </a>";

							if($rows['opt_status'] == 1)
							{
							echo "<a class='delete'  href='". $bur."Admin/activateformoption/".$rows['opt_id']. "/1" . "' target='_self'><i class='fa fa-toggle-on' aria-hidden='true'></i> </a>";
							}
							else
							{
							echo "<a class='delete'  href='". $bur."Admin/activateformoption/".$rows['opt_id']. "/2" . "' target='_self'><i class='fa fa-toggle-off' aria-hidden='true'></i> </a>";
							}
							echo " </td> ";
					        }
						 echo "</tr>";
				}

				?>

			</tbody>
		</table>
		<?php
		if(isset($links) && !empty($links))
		{
			echo $links;
		}

		?>

	</div>
</div>


