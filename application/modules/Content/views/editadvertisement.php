<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Advertisement <a href="javascript:window.history.back()" class="btn btn-default btn-back">  <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>              
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($details)){                      
                      ?>
                      <form id="editadvertisement" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                                            
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Upload Image:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="file" name="ad_image" class="form-control"/>
                            <div id="addimgmsg"></div>                          
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Redirect Link:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="link" class="form-control" placeholder="Redirect Link" value="<?php echo $details['add_url']; ?>"/>
                            <?php echo form_error('link'); ?>
                          </div>
                        </div>                      
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Display Order:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="number" name="display_order" min="1" class="form-control" placeholder="Display Order" value="<?php echo $details['add_display']; ?>" />
                            <?php echo form_error('display_order'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Status:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="th-sele">
                              <select class="form-control" name="status">
                                <option value="">Advertisement Status</option>
                                <option value="1" <?php if($details["add_status"] == 1) echo "selected"; ?>>Publish</option>
                                <option value="0" <?php if($details["add_status"] == 0) echo "selected"; ?>>Unpublish</option>
                              </select>
                              <?php echo form_error('status'); ?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <input type="hidden" name="addid" value="<?php echo $details['add_id']; ?>">
                            <button type="submit" class="btn btn-success" id="addsubmit">Save</button>
                            <div id="addmsg"></div>
                          </div>
                        </div>
                      </form>
                      <?php
                    }
                    ?>                    
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->        