<script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
$(document).ready(function() {
openModal();
});
function openModal(){
  $('#myModaDelSubs').modal();
}  
</script>
<?php
}
?>

<div class="import-create dash-counter">
                        <h2>Filters</h2>
                        <div class="row">
                            
    <form action="<?php echo base_url(). 'Admin/subs_list'; ?>" method="post" id="filterForm">
       <input type="hidden" name="clearfilter" id="clearfilter"  value="">
        <div class="form-group col-md-4">
  <input type="email" class="form-control" name="user_email" id="user_email"  placeholder="Email"  value="">
         
        </div>
        

        <div class="form-group col-md-4" >

         <select id="newsletter_type" class="form-control" id="signup_method"  name="signup_method">
           <option value="-1">Select Newsletter</option>
           <option value="1" >Newsletter One</option>
           <option value="2" >Newsletter Two</option>
          
         </select>
        </div>
        
     
        <div class="form-group col-md-2">
            <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn-primary form-control" value="Filter"/>
           
        </div>
         <div class="form-group col-md-2">
            
            <input type="submit"  id="clearfilterSubmit" name="clearfilterSubmit" class="btn-primary form-control" value="Clear Filter"/>
        </div>
    </form>
                           
                       
                    </div>


                    <div  class="form-group" style="float:right; ">

                   <a href="javascript:void(0)" class="btn btn-primary" id="export-to-csv">Download List</a>
                    
                    </div>

<?php
//$downurl = base_url(). "Admin/user_list_download/".$usertype;
$downurl = base_url(). "Admin/subs_list_download/";
?>
<form action="<?php echo base_url()?>Admin/subs_list_download" method="post" id="export-form">
                    <input type="hidden" value='' id='hidden-type' name='ExportType'/>
                  </form>
                  
</div>





<div class="dash-counter users-main">
                        <h2>Newsletter Subscribers</h2>

                        <div class="user-table table-responsive">
                            <table class="table table-striped table-bordered">
                              <tbody>
                                <tr></tr>
                               

<?php
//echo "<table id='subs' class='table table-striped table-bordered'><tr>";

$bur = base_url();

echo "<tr>";
echo "<th><a href='". $bur."Admin/subs_list/subs_id/".$page."'>ID</a></th>";
echo "<th><a href='". $bur."Admin/subs_list/email/".$page."'>Email</a></th>";
echo "<th><a href='". $bur."Admin/subs_list/postcode/".$page."'>Postal Code</a></th>";
//echo "<th><a href='". $bur."Admin/subs_list/signup'>Signup Date</a></th>";
echo "<th>Action</th>";
echo "</tr>";

if($record_found>0)
{
foreach ($subslist as $rows) 
{
echo "<tr>";
echo "<td>".$rows['subs_id']."</td>";
echo "<td>".$rows['user_email']."</td>";
echo "<td>".$rows['user_postcode']."</td>";
echo "<td>";
echo "<div class='link-del-view'><div class='tooltip-2'><a class='btn btn-large btn-primary'  data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."'Admin/deletesubscriber/'".$rows['subs_id'] . "' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div></div>";

echo "</td></tr>";
}
}
else
{
  echo "<tr><td colspan='7'>No Record Found</td></tr>";
}
//echo "</table>";

?>


</tbody>
</table>
<?php


if(isset($links) && !empty($links))
{
  echo $links;
}

?>

</div>
</div>





<div class="modal fade" id="myModaDAct" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Delete Confirmation</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Do You want to delete the selected record? 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
         <a class="btn btn-default" href="<?php echo base_url();?>Admin/deletesubscriber/". >Yes</a>
        <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>


<script  type="text/javascript">
/*$(document).ready(function() {
jQuery('#export-to-csv').bind("click", function() {
var target = $(this).attr('id');
switch(target) {
    case 'export-to-csv' :
    $('#hidden-type').val(target);
    //alert($('#hidden-type').val());
    $('#export-form').submit();
    $('#hidden-type').val('');
    break
}
});
    });

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });

*/
</script>


<script  type="text/javascript">
$(document).ready(function() {
jQuery('#export-to-csv').bind("click", function() {
var target = $(this).attr('id');
switch(target) {
    case 'export-to-csv' :
    $('#hidden-type').val(target);
   // alert($('#signup_method').val());
    $('#hidden_user_email').val($('#user_email').val());
    $('#hidden_signup_method').val($('#signup_method').val());
    $('#export-form').submit();
    $('#hidden-type').val('');
    break
}
});
    });

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });



$("#clearfilterSubmit").click(function()
  {
  $('#hidden_user_email').val("");
  $('#user_email').val("");
  $('#hidden_signup_method').val("");
  $('#signup_method').val("");
  $('#clearfilter').val("yes");
  $('#filterForm').submit();
  });




</script>
