<h2>Admin Login</h2>

    <form action="login" method="post">
        
        <div class="form-group">
            <input type="email" class="form-control" name="user_email" required="" placeholder="Email"  value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
        </div>
        

        <div class="form-group">
          <input type="password" class="form-control" name="user_password" placeholder="Password" required="">
          <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
        </div>
        
     
        <div class="form-group">
            <input type="submit" name="loginSubmit" class="btn-primary" value="Submit"/>
        </div>
    </form>
 