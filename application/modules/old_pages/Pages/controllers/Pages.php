<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){	 

	 	parent:: __construct();
	 	$this->load->model("Page");
		/* Load the libraries and helpers */

		//$this->load->library('form_validation');
        //$this->load->model('user');

	 }
	 	
	public function index()
	{
		header("Access-Control-Allow-Origin: *");
        $data = array();
        $this->template->set('title', 'Home');
        $data['business_categories'] = $this->Page->getBusinessCategories();
        // $con['conditions'] = array("tst_status"=>1);
        $data['testimonials'] = $this->Page->getTestimonials();
        $data['section5'] = $this->Page->getArticles(14);
        $data['section61'] = $this->Page->getArticles(15);
        $data['section62'] = $this->Page->getArticles(16);
        $data['section63'] = $this->Page->getArticles(17);
        $data['section64'] = $this->Page->getArticles(18);
        $data['section65'] = $this->Page->getArticles(19);
        $data['section66'] = $this->Page->getArticles(20);
        $data['section7'] = $this->Page->getArticles(12);
        $data['section9'] = $this->Page->getArticles(13);

		$data['publishonhome'] = $this->Page->getPublishOnHomeCompanies();
		$data['toprated'] = $this->Page->getTopRatedCompanies();


        $this->template->load('home_layout', 'contents' , 'home', $data);
	}

	public function businessListing(){
		$this->load->helper("cookie");
		header("Access-Control-Allow-Origin: *");
        $data = array();
        $this->template->set('title', 'Find NearBy Attraction');
        $data['business_categories'] = $this->Page->getBusinessCategories();
        // if(!empty($location))
        // $data['latlong'] = $this->Page->get_lat_long($location);
        $this->template->load('home_layout', 'contents' , 'searchResult', $data);
	}

	public function searchResult(){
		$location = $this->input->post("user_address");
		$business = $this->input->post("user_categories");
		$keyword = $this->input->post("keyword");
		$this->load->helper("cookie");
		if(!empty($location)){
			set_cookie(array('name' => 'location',
					        'value' => $location,
					        'expire' => 3600
					        ));
		}
		if(!empty($business)){
			set_cookie(array('name' => 'business',
					        'value' => $business,
					        'expire' => 3600
					        ));
		}
		if(!empty($keyword)){
			set_cookie(array('name' => 'kayword',
					        'value' => $keyword,
					        'expire' => 3600
					        ));
		}
		redirect(base_url("Pages/businessListing"));		
	}

	public function content($args = NULL){
		header("Access-Control-Allow-Origin: *");
        $data = array();
        $con['conditions'] = array("pg_meta_tag"=>$args);
        $data['details'] = $this->Page->getPageRows($con);
        $con['conditions'] = array("pg_cat"=>$data['details'][0]['pg_cat'], "pg_status"=>1);
        $data['similar_pages'] = $this->Page->getPageRows($con);
        $this->template->set('title', $data['details'][0]['pg_title']);
        $this->template->load('home_layout', 'contents' , 'pages', $data);
	}
	
	function readMore($args = NULL){		
		$data = array();
        if($args == "personal"){        	    	
        	$query = $this->db->get_where("site_contents", array("content_id"=>4));                        
            foreach($query->result() as $cnt){}
            $data['details'] = $cnt->content;
        	$this->template->set('title', 'Personal Account');
        	$data['title'] = "Personal Account";
        }
        else if($args == "professional"){
        	$query = $this->db->get_where("site_contents", array("content_id"=>1));
            foreach($query->result() as $cnt){}
            $data['details'] = $cnt->content;
        	$this->template->set('title', 'Professional Account');
        	$data['title'] = "Professional Account";
        }
        header("Access-Control-Allow-Origin: *");
        $this->template->load('home_layout', 'contents' , 'articals', $data);
    }

}
