<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 function __construct() {
	      parent::__construct();
		  
		
	 
	 }	 
	public function index()
	{
		
	//	$this->load->view('welcome_message');
		header("Access-Control-Allow-Origin: *");
                     $data = array();
                     $this->template->set('title', 'Home');
                     $this->template->load('home_layout', 'contents' , 'welcome_message', $data);
	}
	
	public function test(){
		
	      
         /* $constants = $this->db->get_where('otd_footer_content', array("ft_id"=>22))->row();
	     echo "<pre>";
		  print_r($constants);*/
         
		  
		  //echo ADMIN_EMAIL;
	}
}
