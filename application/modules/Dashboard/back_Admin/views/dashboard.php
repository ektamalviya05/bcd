<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 


   <!--  First Top Row   -->
                    <div class="dash-counter">
                        <h2>Analytics</h2>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <div class="dc-sub-ico">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
                                        <h3>80</h3>
                                        <p>Personal Users</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <div class="dc-sub-ico busi-ico">
                                        <i class="fa fa-user-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
                                        <h3>125</h3>
                                        <p>Business Users</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <div class="dc-sub-ico new-let-ico">
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
                                        <h3>65</h3>
                                        <p>Subscribers</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <div class="dc-sub-ico event-ico">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                    <div class="dc-sub-txt">
                                        <h3>65</h3>
                                        <p>Events</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    
                    
    <!--  Second Top Row   -->
                    <div class="import-create dash-counter">
                        <h2>Import & Create</h2>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <a href="#">
                                        <div class="dc-sub-ico">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt pu-txt">
                                            <h3>Import</h3>
                                            <p>Business Users</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <a href="#">
                                        <div class="dc-sub-ico busi-ico">
                                            <i class="fa fa-user-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt bu-txt">
                                            <h3>Import</h3>
                                            <p>Categories</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <a href="#">
                                        <div class="dc-sub-ico new-let-ico">
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt nl-txt">
                                            <h3>Create</h3>
                                            <p>News Letter</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="dc-sub">
                                    <a href="#">
                                        <div class="dc-sub-ico event-ico">
                                            <i class="fa fa-calendar" aria-hidden="true"></i>
                                        </div>
                                        <div class="dc-sub-txt et-txt">
                                            <h3>Create</h3>
                                            <p>Event</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>


                      <!--  Main Containt  -->
                    <div class="dash-counter users-main">
                        <h2>Users</h2>
                        <div class="user-table table-responsive">
                            <table class="table table-striped table-bordered">
                              <tbody>
                                <tr></tr>
                                <tr>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/firstname/1">First Name</a></th>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/lastname/1">Last Name</a></th>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/email/1">Email</a></th>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/type/1">Type</a></th>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/signup/1">Signup Time</a></th>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/signup/1">Last Login</a></th>
                                  <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/All/lastlogin/1">Status</a></th>
                                  <th>Action</th>
                                </tr>
                                <tr>
                                  <td>Amit</td>
                                  <td>Verma</td>
                                  <td>pradeep.invite111@gmail.com</td>
                                  <td>Personal</td>
                                  <td>2017-06-09 11:50:53</td>
                                  <td>0000-00-00 00:00:00</td>
                                  <td>Offline</td>
                                  <td>
                                    <div class="link-del-view">
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">View</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Edit</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Delete</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Deactivate</span>
                                        </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Deepesyh</td>
                                  <td>Verma</td>
                                  <td>pradeep@gmail.com</td>
                                  <td>Personal</td>
                                  <td>2017-06-07 10:02:54</td>
                                  <td>0000-00-00 00:00:00</td>
                                  <td>Offline</td>
                                  <td>
                                    <div class="link-del-view">
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">View</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Edit</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Delete</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Deactivate</span>
                                        </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>John-Lloyd</td>
                                  <td>HAMLET</td>
                                  <td>john-lloyd@hamlet.pro</td>
                                  <td>Professional</td>
                                  <td>2017-06-21 07:54:52</td>
                                  <td>2017-06-21 07:54:52</td>
                                  <td>Offline</td>
                                  <td>
                                      <div class="link-del-view">
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">View</span>
                                          </div>
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">Edit</span>
                                          </div>
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">Delete</span>
                                          </div>
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">Activate</span>
                                          </div>
                                      </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Deepesyh</td>
                                  <td>Verma</td>
                                  <td>pradeep@gmail.com</td>
                                  <td>Personal</td>
                                  <td>2017-06-09 12:24:26</td>
                                  <td>0000-00-00 00:00:00</td>
                                  <td>Offline</td>
                                  <td>
                                    <div class="link-del-view">
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">View</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Edit</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Delete</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Deactivate</span>
                                        </div>
                                    </div>
                                  </td>
                                </tr>
                                <tr>
                                  <td>Deepesyh</td>
                                  <td>Verma</td>
                                  <td>pradeep@gmail.com</td>
                                  <td>Personal</td>
                                  <td>2017-06-09 12:32:38</td>
                                  <td>0000-00-00 00:00:00</td>
                                  <td>Offline</td>
                                  <td>
                                      <div class="link-del-view">
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">View</span>
                                          </div>
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">Edit</span>
                                          </div>
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">Delete</span>
                                          </div>
                                          <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-on" aria-hidden="true"></i></a>
                                            <span class="tooltiptext">Activate</span>
                                          </div>
                                      </div>
                                  </td>
                                </tr>
                              </tbody>
                            </table>

                        </div>
                        <div class="read-more">
                        <a href="<?php echo base_url().'Admin/user_list'; ?>">
                            View more
                        </a>
                        </div>
                    </div>
                    <div class="dash-counter profile-main">
                        <h2>Profiles</h2>
                        <div class="user-table table-responsive">
                           <table class="table table-striped table-bordered">
                             <tbody>
                               <tr></tr>
                               <tr>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/firstname/1">First Name</a></th>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/lastname/1">Last Name</a></th>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/email/1">Email</a></th>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/type/1">Type</a></th>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/signup/1">Signup Time</a></th>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/signup/1">Last Login</a></th>
                                 <th><a href="http://votivephp.in/Otourdemoi/Admin/user_list/Professional/lastlogin/1">Status</a></th>
                                 <th>Action</th>
                               </tr>
                               <tr>
                                 <td>John-Lloyd</td>
                                 <td>HAMLET</td>
                                 <td>john-lloyd@hamlet.pro</td>
                                 <td>Professional</td>
                                 <td>2017-06-21 07:54:52</td>
                                 <td>2017-06-21 07:54:52</td>
                                 <td>Offline</td>
                                 <td>
                                     <div class="link-del-view">
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">View</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Edit</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Delete</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Deactivate</span>
                                        </div>
                                    </div>
                                 </td>
                               </tr>
                               <tr>
                                 <td>Test</td>
                                 <td>tesitn</td>
                                 <td>votive.toshik@testing.com</td>
                                 <td>Professional</td>
                                 <td>2017-06-28 05:44:12</td>
                                 <td>2017-06-28 05:44:12</td>
                                 <td>Offline</td>
                                 <td>
                                     <div class="link-del-view">
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">View</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Edit</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Delete</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Deactivate</span>
                                        </div>
                                    </div>
                                 </td>
                               </tr>
                               <tr>
                                 <td>zubaer</td>
                                 <td>khan</td>
                                 <td>zubaer.votive@gmail.com</td>
                                 <td>Professional</td>
                                 <td>2017-06-09 05:14:15</td>
                                 <td>0000-00-00 00:00:00</td>
                                 <td>Offline</td>
                                 <td>
                                     <div class="link-del-view">
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">View</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Edit</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Delete</span>
                                        </div>
                                        <div class="tooltip-2"><a href="#"><i class="fa fa-toggle-off" aria-hidden="true"></i></a>
                                          <span class="tooltiptext">Deactivate</span>
                                        </div>
                                    </div>
                                 </td>
                               </tr>
                             </tbody>
                           </table>


                        </div>
                        <div class="read-more">
                        <a href="<?php echo base_url().'Admin/user_list/Professional'; ?>">
                            View more
                        </a>
                        </div>
                    </div>
                    <div class="dash-counter news-let-evnts">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="news-let-dash">
                                    <h2>News Letter Subscribers</h2>

                                    <div class="user-table table-responsive">
                                       <table id="subs" class="table table-striped table-bordered">
                                         <tbody>
                                           <tr></tr>
                                           <tr>
                                             <th><a href="http://votivephp.in/Otourdemoi/Admin/subs_list/subs_id/1">ID</a></th>
                                             <th><a href="http://votivephp.in/Otourdemoi/Admin/subs_list/email/1">Email</a></th>
                                             <th><a href="http://votivephp.in/Otourdemoi/Admin/subs_list/postcode/1">Postal Code</a></th>
                                             <th>Action</th>
                                           </tr>
                                           <tr>
                                             <td>7</td>
                                             <td>alert@alert.com</td>
                                             <td></td>
                                             <td>
                                                 <div class="link-del-view">
                                                     <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                       <span class="tooltiptext">View</span>
                                                     </div>
                                                 </div>
                                             </td>
                                           </tr>
                                           <tr>
                                             <td>13</td>
                                             <td>dfhsdkjh311133w1wd@gmail.com</td>
                                             <td></td>
                                             <td>
                                                <div class="link-del-view">
                                                     <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                       <span class="tooltiptext">View</span>
                                                     </div>
                                                 </div>
                                              </td>
                                           </tr>
                                           <tr>
                                             <td>12</td>
                                             <td>dfhsdkjh333w1wd@gmail.com</td>
                                             <td></td>
                                             <td>
                                                 <div class="link-del-view">
                                                     <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                       <span class="tooltiptext">View</span>
                                                     </div>
                                                 </div>
                                             </td>
                                           </tr>
                                           <tr>
                                             <td>11</td>
                                             <td>dfhsdkjh333wwd@gmail.com</td>
                                             <td></td>
                                             <td>
                                                 <div class="link-del-view">
                                                     <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                       <span class="tooltiptext">View</span>
                                                     </div>
                                                 </div>
                                             </td>
                                           </tr>
                                           <tr>
                                             <td>9</td>
                                             <td>dfhsdkjhjd@gmail.com</td>
                                             <td></td>
                                             <td>
                                                 <div class="link-del-view">
                                                     <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                       <span class="tooltiptext">View</span>
                                                     </div>
                                                 </div>
                                             </td>
                                           </tr>
                                         </tbody>
                                       </table>



                                    </div>
                                    <div class="read-more">
                                       <a href="<?php echo base_url().'Admin/subs_list'; ?>">
                                            View more
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="news-let-dash">
                                    <h2>Events</h2>

                                    <div class="user-table table-responsive">
                                       <table class="table-striped table-bordered table">
                                           <tbody>
                                               <tr>
                                                   <th>
                                                       Event Name
                                                   </th>
                                                   <th>
                                                       Date Posted
                                                   </th>
                                                   <th>
                                                       User
                                                   </th>
                                                   <th>
                                                       Action
                                                   </th>
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   <td>
                                                        <div class="link-del-view">
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">View</span>
                                                           </div>
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">Delete</span>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   <td>
                                                       <div class="link-del-view">
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">View</span>
                                                           </div>
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">Delete</span>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   <td>
                                                       <div class="link-del-view">
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">View</span>
                                                           </div>
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">Delete</span>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   <td>
                                                       <div class="link-del-view">
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">View</span>
                                                           </div>
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">Delete</span>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                               <tr>
                                                   <td>Jean-Louis Fournot</td>
                                                   <td>2017-06-21 07:54:52</td>
                                                   <td>John-Lloyd</td>
                                                   <td>
                                                       <div class="link-del-view">
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">View</span>
                                                           </div>
                                                           <div class="tooltip-2"><a href="#"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                             <span class="tooltiptext">Delete</span>
                                                           </div>
                                                       </div>
                                                   </td>
                                               </tr>
                                           </tbody>
                                       </table>



                                    </div>
                                    <div class="read-more">
                                        <a href="#">
                                            View more
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>




















<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Deleted");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDel').modal();
}  
</script>

  <?php
}
?>

<?php
if(isset($activated) && trim($activated) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Activated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaAct').modal();
}  
</script>

  <?php
}
else if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
 //alert( "Deactivated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>

  <?php
}
?>

