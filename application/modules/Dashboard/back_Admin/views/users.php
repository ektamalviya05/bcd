 <script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script>

    <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>


<?php
if(isset($deleted) && trim($deleted) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Deleted");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDel').modal();
}  
</script>

  <?php
}
?>

<?php
if(isset($activated) && trim($activated) == "yes")
{
 ?>
<script type="text/javascript">
 //alert( "Activated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaAct').modal();
}  
</script>

  <?php
}
else if(isset($activated) && trim($activated) == "no")
{
 ?>
<script type="text/javascript">
 //alert( "Deactivated");
 

$(document).ready(function() {
openModal();

});
function openModal(){
  $('#myModaDAct').modal();
}  
</script>

  <?php
}
?>


<div class="import-create dash-counter">
                        <h2>Filters</h2>
                        <div class="row">
                            
                       <form action="<?php echo base_url(). 'Admin/user_list/'.$type; ?>" method="post" id="filterForm">
       <input type="hidden" name="clearfilter" id="clearfilter"  value="">
        <div class="form-group col-md-4">
   <input type="email" class="form-control" name="user_email" id="user_email"  placeholder="Email"  
   value="<?php echo $fltr_user_email != ''? $fltr_user_email : ''; ?>">
         
        </div>
        

        <div class="form-group col-md-4" >

         <select id="signup_method" class="form-control" id="signup_method"  name="signup_method">
           <option value="-1">Select Method</option>
           <option value="1"  <?php echo set_value('signup_method') ==1? "selected":""; ?> >Regular Signup</option>
           <option value="2"  <?php echo set_value('signup_method') ==2? "selected":""; ?> >Facebook Signup</option>
           <option value="3"  <?php echo set_value('signup_method') ==3? "selected":""; ?> >Google Signup </option>
         </select>
        </div>
        
     
        <div class="form-group col-md-2">
            <input type="submit"  id="filterSubmit" name="filterSubmit" class="btn-primary form-control" value="Filter"/>
           
        </div>
         <div class="form-group col-md-2">
            
            <input type="submit"  id="clearfilterSubmit" name="clearfilterSubmit" class="btn-primary form-control" value="Clear Filter"/>
        </div>
    </form>
                           
                       
                    </div>
                    <div  class="form-group" style="float:right;  ">

                    <a href="javascript:void(0)" class="btn btn-primary form-control" id="export-to-csv">Download Users</a>
                    
                    </div>

<?php
//$downurl = base_url(). "Admin/user_list_download/".$usertype;
$downurl = base_url(). "Admin/user_list_download/".$type;
?>
<form action="<?php echo $downurl; ?>" method="post" id="export-form">
 <input type="hidden" value='<?php echo set_value('user_email'); ?>' id='hidden_user_email' name='hidden_user_email'/>
  <input type="hidden" value='<?php echo set_value('signup_method'); ?>' id='hidden_signup_method' name='hidden_signup_method'/>
 <input type="hidden" value='' id='hidden-type' name='ExportType'/>
                  </form>

                  
</div>







<div class="dash-counter users-main">
                        <h2><?php 

if($usertype !="")
{
    echo strtoupper($usertype). " USERS LIST"; 
}
else
{
   $usertype="All";
   echo "All USERS LIST"; 
}


?>
</h2>

                        <div class="user-table table-responsive">
                            <table class="table table-striped table-bordered">
                              <tbody>
                                <tr></tr>
                               

<?php
$fields = array('First Name', 'Last Name', 'Email', 'Type', 'Signup Time', 'Last Login', 'Action');
//echo "<table class='table table-striped table-bordered'><tr>";
$bur = base_url();

echo "<tr>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/firstname/".$page."'>First Name</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/lastname/".$page."'>Last Name</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/email/".$page."'>Email</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/type/".$page."'>Type</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/signup/".$page."'>Signup Time</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/signup/".$page."'>Last Login</a></th>";
echo "<th><a href='". $bur."Admin/user_list/".$usertype."/lastlogin/".$page."'>Status</a></th>";
echo "<th>Action</th>";
echo "</tr>";
?>

 


<?php
if($record_found>0)
{

foreach ($userlist as $rows) 
{
  $stat= $rows['is_online'] == 1? 'Online': 'Offline';
echo "<tr>";
echo "<td>".$rows['user_firstname']."</td>";
echo "<td>".$rows['user_lastname']."</td>";
echo "<td>".$rows['user_email']."</td>";
echo "<td>".$rows['user_type']."</td>";
echo "<td>".$rows['created']."</td>";
echo "<td>".$rows['last_login']."</td>";
echo "<td>".$stat."</td>";

?>



<?php

echo "<td><div class='link-del-view'>";
if($rows['user_type'] == "Personal")
{
echo "<div class='tooltip-2'><a href='". $bur."Admin/edituser/".$rows['user_id']."'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span></div><div class='tooltip-2'><a href='". $bur."Admin/edituser/".$rows['user_id']."/updt'><i class='fa fa-edit' aria-hidden='true'></i></a><span class='tooltiptext'>Edit</span></div>";
}
else
{
  echo "<div class='tooltip-2'><a href='". $bur."Admin/edituserpro/".$rows['user_id']."'><i class='fa fa-eye' aria-hidden='true'></i></a><span class='tooltiptext'>View</span></div><div class='tooltip-2'><a href='". $bur."Admin/edituserpro/".$rows['user_id']."/updt'><i class='fa fa-edit' aria-hidden='true'></i></a><span class='tooltiptext'>Edit</span></div>";
}


                                            
//echo "| <a href='". $bur."Admin/deleteuser/".$rows['user_id']."'>Delete</a> | ";
echo "<div class='tooltip-2'><a  data-toggle='confirmation' data-title='Are you sure to delete?' href='". $bur."Admin/deleteuser/".$rows['user_id'] . "/". $rows['user_type'] ."' target='_self'><i class='fa fa-trash-o' aria-hidden='true'></i></a><span class='tooltiptext'>Delete</span></div> ";


if($rows['status'] == 0)
{
echo "<div class='tooltip-2'><a href='". $bur."Admin/activateuser/".$rows['user_id']."/1'><i class='fa fa-toggle-on' aria-hidden='true'></i></a><span class='tooltiptext'>Activate</span></div>";
}
else
{
echo "<div class='tooltip-2'><a href='". $bur."Admin/activateuser/".$rows['user_id']."/0'><i class='fa fa-toggle-off' aria-hidden='true'></i></a><span class='tooltiptext'>Deactivate</span></div>"; 
}



?>


                                          
                                       
                                          


<?php
echo "</div></td>";
echo "</tr>";
}
}

else
{
  echo "<tr><td colspan='7'>No Record Found</td></tr>";
}
echo "</table>";
if(isset($links) && !empty($links))
{
  echo $links;
}


?>
                       </div>
                       
                    </div>








<script  type="text/javascript">
$(document).ready(function() {
jQuery('#export-to-csv').bind("click", function() {
var target = $(this).attr('id');
switch(target) {
    case 'export-to-csv' :
    $('#hidden-type').val(target);
   // alert($('#signup_method').val());
    $('#hidden_user_email').val($('#user_email').val());
    $('#hidden_signup_method').val($('#signup_method').val());
    $('#export-form').submit();
    $('#hidden-type').val('');
    break
}
});
    });

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });



$("#clearfilterSubmit").click(function()
  {
  $('#hidden_user_email').val("");
  $('#user_email').val("");
  $('#hidden_signup_method').val("");
  $('#signup_method').val("");
  $('#clearfilter').val("yes");
  $('#filterForm').submit();
  });




</script>
