<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MX_Controller {

	public function __construct(){	 

	 	parent:: __construct();
		$this->load->library('session');
    $this->load->model('Admin_model');
    $this->load->helper('url');
    $this->load->helper('csv');
    $this->load->library('form_validation');  
    $this->load->library('pagination');

	 }
	 	
function index()
{
  $data = array();
  
  if($this->session->userdata('isUserLoggedIn')){

     //$data['user'] = $this->Admin_model->getRows(array('user_id'=>$this->session->userdata('user_id')));
      // if($data['user']['user_type'] == 'Admin')
        if($this->session->userdata('user_type') == "Admin")
        {
             header("Access-Control-Allow-Origin: *");
                     
                     $this->template->set('title', 'Signup');
                     $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

        }
else
{
     redirect(base_url().'Users/account');
}
        }else{
            redirect(base_url().'Admin/login');
        }

}




function user_list()
{

$segcount = $this->uri->total_segments();
$order_by = "";
$order="";
$type ="";
$page = 0;
$fltr_user_email = "";
$fltr_signup_method = "";



if($segcount == 3)
{

$type = $this->uri->segment(3);

}

else if ($segcount == 5)
{
$order = $this->uri->segment(4);
 $type = $this->uri->segment(3);
}
else if ($segcount == 4)
{
$order = "firstname";
$type = $this->uri->segment(3);
}
else if($segcount < 3)
{
 $order = "firstname";
 $type = "All";
}




if($order == 'firstname')
{
$order_by = "user_firstname";
}
else if($order == 'lastname')
{
$order_by = "user_lastname";
}
else if($order == 'email')
{
$order_by = "user_email";
}
else if($order == 'type')
{
$order_by = "user_type";
}
else if($order == 'signup')
{
$order_by = "created";
}
else if($order == 'lastlogin')
{
$order_by = "last_login";
}
else
{
  $order_by = "user_firstname";
}

if(is_numeric($type))
      {
        $type = "All";
      }

  $con['sorting'] = array('order'=>$order_by);
  $data = array();
$data['fltr_user_email'] = "";
$data['fltr_signup_method'] = "";

  $data["type"] = $type;
  $con['conditions'] = array('user_type !='=> 'Admin');

 if($type != "" && $type != "All")
{
$con['conditions'] = array('user_type'=> $type);
}
else
{
   $con['conditions'] = array('user_type !='=> null);
}


//Filter Code

if($this->input->post('filterSubmit'))
 {

$fltr_user_email = $this->input->post('user_email');
$fltr_signup_method = $this->input->post('signup_method');
$this->session->set_userdata('fltr_user_email', $fltr_user_email);
$this->session->set_userdata('fltr_signup_method', $fltr_signup_method);
$data['fltr_user_email'] = $fltr_user_email;
$data['fltr_signup_method'] = $fltr_user_email;
}
else
{
$fltr_user_email = $this->session->userdata('fltr_user_email');
$fltr_signup_method = $this->session->userdata('fltr_signup_method');

 if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit'))
 {
   $this->session->unset_userdata('fltr_signup_method');
   $this->session->unset_userdata('fltr_user_email');
   $fltr_user_email = "";
   $fltr_signup_method = "";
  $data['fltr_user_email'] = "";
  $data['fltr_signup_method'] = "";

 }
}

if($fltr_user_email != "")
{
$con['filter']['user_email'] =$fltr_user_email;
}

if($fltr_signup_method != "" && $fltr_signup_method != "-1")
{
      if($fltr_signup_method ==1){$con['filter']['user_signup_method'] = "Regular";}
      if($fltr_signup_method ==2){$con['filter']['user_signup_method'] = "Facebook";}  
      if($fltr_signup_method ==3){$con['filter']['user_signup_method'] = "Google";}
}

//Filter Code End

$totalrows = $this->Admin_model->record_count($con);
$config['base_url'] = base_url()."Admin/user_list/".$type."/".$order;
$config['total_rows'] = $totalrows;
$config['per_page'] = 10;
$config['uri_segment'] = 5;
$config['num_links'] = $totalrows / 10;
$config['cur_tag_open'] = '&nbsp;<a class="current">';
$config['cur_tag_close'] = '</a>';
$config['next_link'] = '<b> Next </b>';
$config['prev_link'] = '<b> Previous </b>';
$choice = $config["total_rows"] / $config["per_page"];
$config["num_links"] = round($choice);
$this->pagination->initialize($config);

$page = ($this->uri->segment(5)) ? $this->uri->segment(5) : 1;
if(is_numeric($this->uri->segment(5)))
{
  $page = ($this->uri->segment(5));
}
else if(is_numeric($this->uri->segment(4)))
{
  $page = ($this->uri->segment(4));
}
else if(is_numeric($this->uri->segment(3)))
{
  $page = ($this->uri->segment(3));
}


$con['limit'] = $config['per_page'];
$start =($page-1) * $config['per_page'];

if($start == 0)
{
  $start = 0;
}
else
{
  $start = ($start/$config['per_page'] )+1;
}
$con['start'] = $start;
$data['userlist'] = array();

if($this->session->userdata('isUserLoggedIn'))
        {
   
    if($totalrows > 0){
      $data['userlist'] = $this->Admin_model->getRows($con);
      $data["record_found"] = $totalrows; 
    }
    else
    {
      $data["record_found"] = 0; 
    }

$data["links"] = $this->pagination->create_links();
$data['usertype'] = $type;
$data['order'] = $order;
$data['page'] = $page;


header("Access-Control-Allow-Origin: *");
$this->template->set('title', 'User List');
$this->template->load('admin_layout', 'contents' , 'users', $data);
}else{
     redirect(base_url().'Admin/login');
    }

}

function subs_list()
{

$segcount = $this->uri->total_segments();
$order_by = "";
$order="";
$type ="";
$page = 0;

if($segcount == 3)
{

$order = $this->uri->segment(3);

}
else if ($segcount == 4)
{

$order = $this->uri->segment(3);
}
else if($segcount < 3)
{
 $order = "email";
}


if($order == 'postcode')
{
$order_by = "user_postcode";
}
else if($order == 'email')
{
$order_by = "user_email";
}
else
{
  $order_by = "subs_id";
}


  $con['sorting'] = array('order'=>$order_by);
  $data = array();
    






$totalrows = $this->Admin_model->record_count($con, "subscribers");
$config['base_url'] = base_url()."Admin/subs_list/".$order;
$config['total_rows'] = $totalrows;
$config['per_page'] = 10;
$config['uri_segment'] = 4;
$config['num_links'] = $totalrows / 10;
$config['cur_tag_open'] = '&nbsp;<a class="current">';
$config['cur_tag_close'] = '</a>';
$config['next_link'] = '<b> Next </b>';
$config['prev_link'] = '<b> Previous </b>';

$choice = $config["total_rows"] / $config["per_page"];
$config["num_links"] = round($choice);
$this->pagination->initialize($config);

$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 1;
if(is_numeric($this->uri->segment(4)))
{
  $page = ($this->uri->segment(4));
}
else if(is_numeric($this->uri->segment(3)))
{
  $page = ($this->uri->segment(3));
}

$con['limit'] = $config['per_page'];
$start =($page-1) * $config['per_page'];
if($start == 0)
{
  $start = 0;
}
else
{
  $start = ($start/$config['per_page'] )+1;
}
$con['start'] = $start;
if($this->session->userdata('isUserLoggedIn'))
        {


 if($totalrows > 0){
$data['subslist'] = $this->Admin_model->getSubsRows($con);
$data["record_found"] = $totalrows; 
}
else
{
 $data['subslist'] = array();
 $data["record_found"] = 0; 
}


$data["links"] = $this->pagination->create_links();
$data['order'] = $order;
$data['page'] = $page;
header("Access-Control-Allow-Origin: *");
$this->template->set('title', 'User List');
$this->template->load('admin_layout', 'contents' , 'subscribers', $data);
}else{
     redirect(base_url().'Admin/login');
    }

}



function subs_list_download()
{
$order_by = "";
$order="";

 $con['sorting'] = array('order'=>$order_by);
 $data = array();
 $query = $this->Admin_model->downloadRowsSubs($con);
 query_to_csv($query,TRUE,'Subscribers_'.date('dMy').'.csv');


}

function user_list_download()
{

$order_by = "";
$order="";


 $con['sorting'] = array(
            'order'=>$order_by
                  
                );

     $data = array();
    // $data['usertype'] = "";


       $type =  $this->uri->segment(3);

     if($type != "" && $type != "All" && is_numeric($type) != true)
{
          $con['conditions'] = array(
            'user_type'=> $type,
            'status !='=> '3'
      );
}
else
{
      $con['conditions'] = array(
           'status !='=> '3'
      );
}


//Filter Code

if($this->input->post('filterSubmit'))
 {

$fltr_user_email = $this->input->post('user_email');
$fltr_signup_method = $this->input->post('signup_method');
$this->session->set_userdata('fltr_user_email', $fltr_user_email);
$this->session->set_userdata('fltr_signup_method', $fltr_signup_method);
$data['fltr_user_email'] = $fltr_user_email;
$data['fltr_signup_method'] = $fltr_user_email;
}
else
{
$fltr_user_email = $this->session->userdata('fltr_user_email');
$fltr_signup_method = $this->session->userdata('fltr_signup_method');

 if( $this->input->post('clearfilter') == 'yes' || $this->input->post('clearfilterSubmit'))
 {
   $this->session->unset_userdata('fltr_signup_method');
   $this->session->unset_userdata('fltr_user_email');
   $fltr_user_email = "";
   $fltr_signup_method = "";
  $data['fltr_user_email'] = "";
  $data['fltr_signup_method'] = "";

 }
}

if($fltr_user_email != "")
{
$con['filter']['user_email'] =$fltr_user_email;
}

if($fltr_signup_method != "" && $fltr_signup_method != "-1")
{
      if($fltr_signup_method ==1){$con['filter']['user_signup_method'] = "Regular";}
      if($fltr_signup_method ==2){$con['filter']['user_signup_method'] = "Facebook";}  
      if($fltr_signup_method ==3){$con['filter']['user_signup_method'] = "Google";}
}

//Filter Code End



 $query = $this->Admin_model->downloadRows($con);
 query_to_csv($query,TRUE,'Users_'.date('dMy').'.csv');

}



    /*
     * User login
     */
    public function login(){

        $data = array();
        $con['conditions'] ="";
        $data['error_msg']=""; 
        $con['returnType'] = 'single';
       
        if($this->session->userdata('success_msg')){
            $data['success_msg'] = $this->session->userdata('success_msg');
            $this->session->unset_userdata('success_msg');
        }
        if($this->session->userdata('error_msg')){
            $data['error_msg'] = $this->session->userdata('error_msg');
            $this->session->unset_userdata('error_msg');
        }

        if($this->input->post('loginSubmit'))
        {
      
          $con['user_login'] = array("yes");
                $con['conditions'] = array(
                    'user_email'=>$this->input->post('user_email'),
                    'user_password' => md5($this->input->post('user_password')),
                    //'status' => '1'
                );

                $checkLogin = $this->Admin_model->getRows($con);
                //echo $checkLogin;
                //die();
                if($checkLogin){

                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('user_id',$checkLogin['user_id']);
                    $this->session->set_userdata('user_type','Admin');
                    redirect(base_url().'Admin/dashboard');
                    //redirect(base_url().'Admin/user_list');
                    
                }else{
                    $data['error_msg'] = 'Wrong email or password, please try again.';
                }
            //}
               
        }
else
{
if( $this->session->userdata('isUserLoggedIn'))
{
  redirect(base_url().'Admin/dashboard');
}

}
        header("Access-Control-Allow-Origin: *");
                    // $data = array();
                     $this->template->set('title', 'Admin Login');
                     $this->template->load('admin_login_layout', 'contents' , 'login_page', $data);

    }
    

public function dashboard()
    {

if( $this->session->userdata('isUserLoggedIn'))
{
  $data = array();
    header("Access-Control-Allow-Origin: *");
                    // $data = array();
                     $this->template->set('title', 'Admin Login');
                     $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

    }
    else
    {
  redirect(base_url().'Admin/login');
    }

    }

    /*
     * User logout
     */
    public function logout(){
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_Id');
        $this->session->unset_userdata('user_type');
        $this->session->sess_destroy();
        redirect(base_url().'Admin/');
    }
    
    /*
     * Existing email check during validation
     */
    public function email_check($str){
         $con['returnType'] = 'count';
        $con['conditions'] = array('user_email'=>$str);
        $checkEmail = $this->Admin_model->getRows($con);
        if($checkEmail > 0){
            $this->form_validation->set_message('email_check', 'The given email already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }
	
public function edituser()
  {
    $data = array();
    $data['error_msg']=""; 
    $userData = array();
$id = "";
 if($this->input->post('updateSubmit'))
 {

  /*
  $this->form_validation->set_rules('user_address', 'User Address', 'required');
  $this->form_validation->set_rules('user_firstname', 'First Name', 'required');
  $this->form_validation->set_rules('user_lastname', 'Last Name', 'alpha');
  $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',
        array(
                'required'      => 'You have not provided %s.',
                'numeric'     => 'This %s not numeric value.',
                'is_natural'     => 'Only natural numbers can be accepted.'
        ));
*/
  $userData = null;
  $userlang = "";

            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_gender' => $this->input->post('user_gender'),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                  );

 
$id = $this->input->post('user_id');
       //if($this->form_validation->run() == true){
                $update = $this->Admin_model->update($userData, $id);
                if($update >=1){
                    $this->session->set_userdata('success_msg', 'Data Updated.');
                    $this->session->set_userdata('upd_success', 'yes');
                    redirect(base_url()."Admin/user_list");
            }
          //}
        }
      
      $uid = $id = "";
      if($this->uri->segment(3)){
$uid = ($this->uri->segment(3)) ;
}

 if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;
$data['act'] = $action;
}
else
{
 $data['act'] = "view"; 
}

 $con['conditions'] = array(
            'user_id'=> $uid
      );


    $data['user'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    
          $this->template->set('title', 'Professional Account Creation'); 
          $this->template->load('admin_layout', 'contents' , 'user_edit', $data);
  

  }


  public function edituserpro()
  {
     $data = array();
    $data['error_msg']=""; 
    $userData = array();
$id = "";
 if($this->input->post('updateSubmit'))
 {

  /*
  $this->form_validation->set_rules('user_address', 'User Address', 'required');
  $this->form_validation->set_rules('user_firstname', 'First Name', 'required');
  $this->form_validation->set_rules('user_lastname', 'Last Name', 'alpha');
  $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',
        array(
                'required'      => 'You have not provided %s.',
                'numeric'     => 'This %s not numeric value.',
                'is_natural'     => 'Only natural numbers can be accepted.'
        ));
*/
  $userData = null;
  $userlang = "";
if($this->input->post('user_type')=="Personal")
{
            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_gender' => $this->input->post('user_gender'),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                  );

 }
 else{
  $userlang = implode("|", $this->input->post('user_language'));

   $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_gender' => $this->input->post('user_gender'),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_language' => $userlang
                  );
 }           
$id = $this->input->post('user_id');
       //if($this->form_validation->run() == true){
                $update = $this->Admin_model->update($userData, $id);
                if($update >=1){
                  //echo $update. "hsjdhfjd";
                    $this->session->set_userdata('success_msg', 'Data Updated.');
                    $this->session->set_userdata('upd_success', 'yes');
                    redirect(base_url()."Admin/user_list");
            }
          //}
        }
      
      $uid = $id = "";
      if($this->uri->segment(3)){
$uid = ($this->uri->segment(3)) ;
}

 if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;
$data['act'] = $action;
}
else
{
 $data['act'] = "view"; 
}

 $con['conditions'] = array(
            'user_id'=> $uid
      );


    $data['user'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    

    $this->template->load('admin_layout', 'contents' , 'user_edit_pro', $data);
}



public function deleteuser()
  {
  
  $data = array();
  $data['error_msg']=""; 
  $userData = array();
  $id = "";
  if($this->uri->segment(3)){
$id = ($this->uri->segment(3)) ;
}

if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;
$data['act'] = $action;
}
else
{
 $data['act'] = "view"; 
}

 

 $con['conditions'] = array(
            'user_id'=> $id
      );

$userData = array(
                'status' => '3'
                );
   $rec = $this->Admin_model->update($userData, $id);

if($rec > 0)
{
  $data['deleted'] = "yes";
}


 $con['conditions'] = array( );
  $data['usertype'] = "";
  $data['page'] = "1";
  
    $data['userlist'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    
          $this->template->set('title', 'Professional Account Creation'); 
          $this->template->load('admin_layout', 'contents' , 'users', $data);

// redirect(base_url()."Admin/user_list");  

  }


public function activateuser()
  {
  
  $data = array();
  $data['error_msg']=""; 
  $userData = array();
  $action="";
  $id = "";
  if($this->uri->segment(3)){
$id = ($this->uri->segment(3)) ;
}

if($this->uri->segment(4)){
$action = ($this->uri->segment(4)) ;

}
//echo $action;
 $con['conditions'] = array(
            'user_id'=> $id
      );

$userData = array('status' => $action);
$rec = $this->Admin_model->update($userData, $id);

if($rec > 0 && $action==1)
{
  $data['activated'] = "yes";

  if($rec >=1)
{

 $urec = $this->Admin_model->getUserEmail($id);
$email = $urec['user_email'];

            $subject = "Congratulation Your Account is Activated";
            $msginner = "Thank You ". $urec['user_firstname']." for Registering with us. Your account is now active and you can login to your account. Please use below link to login to your dashboard. <br><br>
            <a href='".base_url()."'> Login Here</a><br><br>";

            $msg = '
            <html>
            <head>
            <title>Congratulation Your Account is Activated</title>
            </head>
            <body>
            <h3>'.$subject.'</h3>
            <p>'.$msginner.'</p><br><br>
            <p>With Regards</p>
            <p>Support Team</p>
            </body>
            </html>
            ';


        $fromemail = "votive.pradeep01@gmail.com";
        $fromname = "Support Team";

     $x= $this->send_mail($email, $subject, $msg);

     $data['success_msg'] = 'Business User Activated';
                   
}else{
                    $data['error_msg'] = 'Wrong email, please try again.';
                }
  
}


else if($rec > 0 && $action==0)
{
  $data['activated'] = "no";
}


 $con['conditions'] = array( );
  $data['usertype'] = "";
    $data['userlist'] = $this->Admin_model->getRows($con);
    header("Access-Control-Allow-Origin: *");
                    
          $this->template->set('title', 'Professional Account Creation'); 
          $this->template->load('admin_layout', 'contents' , 'dashboard', $data);

// redirect(base_url()."Admin/user_list");  

  }

public function deletesubscriber()
  {
  
  $data = array();
  $data['error_msg']=""; 
  $userData = array();
  $id = "";
  if($this->uri->segment(3)){
$id = ($this->uri->segment(3)) ;
}

 $data['act'] = "delete";
 

 $con['conditions'] = array(
            'subs_id'=> $id
      );

$userData = array(
                'status' => '3'
                );
   $rec = $this->Admin_model->update($userData, $id, "subscribers");

if($rec > 0)
{
  $data['deleted'] = "yes";
}


 $con['conditions'] = array( );
$data["page"] = 1;
    $data['subslist'] = $this->Admin_model->getSubsRows($con);
 header("Access-Control-Allow-Origin: *");
                    
         $this->template->set('title', 'Professional Account Creation'); 
         $this->template->load('admin_layout', 'contents' , 'subscribers', $data);

//redirect(base_url()."Admin/subs_list");  

  }


function send_mail($email, $subject="Test Mail", $msg="Testing mail"){

$from = "votive.pradeep01@gmail.com";
$this->load->library('email');
$config['protocol']    = 'smtp';
$config['smtp_host']    = 'ssl://smtp.gmail.com';
$config['smtp_port']    = '465';
$config['smtp_timeout'] = '7';
$config['smtp_user']    = $from;
$config['smtp_pass']    = 'votive123456';
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype'] = 'html'; 
$config['validation'] = TRUE;      
$this->email->initialize($config);



      $fromname = "Sales Team";
      $fromemail ="votive.pradeep01@gmail.com";
      $mpass = "votive123456";
      $to = "votive.pradeep01@gmail.com, ". $email;
      $from = "votive.pradeep01@gmail.com";
      $subject=$subject;
      $message=$msg;
      $cc=false;


      $this->email->from($fromemail, $fromname);
      $this->email->to($to);
      $this->email->subject($subject);       
      $this->email->message($msg);  
      if($cc){
            $this->email->cc($cc);
            }
            
      /*      if(!empty($bcc)){
                $this->email->bcc($bcc);
            }
              */

            if(!$this->email->send()){
              return $this->email->print_debugger();
            }else{
              return true;
            }

     

} 


}
