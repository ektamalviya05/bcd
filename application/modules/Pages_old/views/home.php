

<script src="http://jwpsrv.com/library/A7o4ns39EeS3agp+lcGdIw.js"></script>
<div class="main-content">
  <section class="main-search-sec">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php
          $cnt = 1;
          foreach($bn_images as $imgs){
            $imgpath = $imgs['bn_path'];
            ?>
              <div class="item <?php if($cnt == 1) echo "active"; ?>"> <img src="<?php echo base_url("assets/img/Banners/$imgpath"); ?>" alt="Los Angeles"> </div>
            <?php
            $cnt++;
          }

        ?>
        <!-- <div class="item active"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/slider1.jpg" alt="Los Angeles"> </div>
        <div class="item"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/slider1.jpg" alt="Chicago"> </div>
        <div class="item"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/slider1.jpg" alt="New York"> </div> -->
      </div>
      
      <!-- Left and right controls --> 
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> </div>
    <div class="container">
      <div class="header-srch">
        <div class="row">
          <div class="col-md-12">
            <div class="main-srch-heading">
              <h1><?php echo $headings['label1']; ?></h1>
              <p><?php echo $headings['label2']; ?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="main-srch-sub">
        <div class="row">
         <!--  <form method="post" action="<?php echo base_url("Pages/searchResult"); ?>"> -->
            
          <form method="post" name="searchForm" id="searchForm" action="<?php echo base_url("Pages/setSearchDelimiters"); ?>">
            <div class="col-md-4">
              <div class="form-group form-sub"> 
                
                <!-- <input type="address" class="form-control add-form" placeholder="Entrez votre adresse">-->
                
                <input id="autocomplete" placeholder="<?php echo $headings['label7']; ?>" required="" title="Address is required to search." name="user_address" class="form-control add-form" onFocus="geolocate()" type="text">
                  <input id="searchedcity" name="searchedcity" value="" type="hidden" />
                </input>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-sub">
                <input type="text" name="business_category1" id="business_category1" 
                  list='categories1'
                  name='user_categories'
                  id="business_category"
                  class="form-control cat-form" placeholder="<?php echo $headings['label4']; ?>">
                <datalist id="categories1">
                  <?php
                    foreach($business_categories->result() as $cate){
                      ?>
                  <option value="<?php echo $cate->cat_name; ?>"><?php echo $cate->cat_name; ?></option>
                  <?php
                    }
                    ?>
                </datalist>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group form-sub no-bord">
                <input class="form-control" name="keyword" placeholder="<?php echo $headings['label5']; ?>">
              </div>
            </div>
            <div class="col-md-2">
              <button class="btn btn-default" name="searchSubmit" value="<?php echo $headings['label6']; ?>" type="Submit"><i class="fa fa-search"></i> <?php echo $headings['label6']; ?></button>
            </div>
          </form>
        </div>
      </div>
      <!-- <div class="srch-links">
        <div class="row">
          <div class="col-md-12">
            <ul>
            <?php
            // if(!empty($featuredCategories)){
            //   foreach($featuredCategories as $fcategory){
            //     $fcat_img = $fcategory['cat_img_path'];
                ?>
                <li> 
                  <a class="srch-link" href="#">
                    <div class="img-srch"> <img class="catlisthomesearch" src="<?php //echo base_url("assets/img/category/$fcat_img"); ?>"> </div>
                    <div class="srchtxt">
                      <p><?php //echo $fcategory['cat_name']; ?></p>
                    </div>
                  </a> 
                </li>
                <?php
            //   }
            // }
            ?>
            </ul>
          </div>
        </div>
      </div> -->
      <div class="scrol-btm"> <a href="#scroll-here"><i class="fa fa-angle-down"></i></a> </div>
    </div>
  </section>
  <section class="interstng-sec" id="scroll-here">
    <div class="container">
      <div class="interstng-sec-head">
        <div class="col-md-12">
          <h2><?php echo $headings['label8']; ?></h2>
          <p><?php echo $headings['label3']; ?></p>
        </div>
      </div>
      <div class="intertng-links-sec">
        <div class="panel panel-primary">
          <div class="row">
            <div class="col-md-12">
              <ul class="nav panel-tabs">
                <?php 
                if(!empty($homecategorytab)){
                  $cnt = 1;
                  foreach($homecategorytab as $tab){
                    ?>
                    <li class="<?php if($cnt == 1) echo "active"; ?>">
                    <a href="#tab<?php echo $tab['tab_id']; ?>" data-toggle="tab"><!--<i class="fa fa-wrench" aria-hidden="true"></i>--> <img src="<?php echo base_url('assets/img/category/').$tab['tab_image']; ?>" alr="image not found"/> <?php echo $tab['tab_name']; ?></a></li>
                    <?php
                    $cnt++;
                  }
                }
                ?>
                <!-- <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-wrench" aria-hidden="true"></i> Travaux</a></li>
                <li><a href="#tab2" data-toggle="tab"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shopping</a></li>
                <li><a href="#tab3" data-toggle="tab"><i class="fa fa-handshake-o" aria-hidden="true"></i> Services</a></li>
                <li><a href="#tab4" data-toggle="tab"><i class="fa fa-heartbeat" aria-hidden="true"></i> Santé</a></li>
                <li><a href="#tab5" data-toggle="tab"><i class="fa fa-glass" aria-hidden="true"></i> Goûts et Saveurs</a></li>
                <li><a href="#tab6" data-toggle="tab"><i class="fa fa-cutlery" aria-hidden="true"></i> Sortis et Loisirs</a></li> -->
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="panel-body">
              <div class="tab-content">
                <?php
                $cnt = 1;
                if(!empty($homecategorytab)){
                  foreach($homecategorytab as $tbcat){
                    $userlist = $this->Page->getCategoryFromTab(array("conditions"=>array("cat_hometab_id"=>$tbcat['tab_id'], "cat_hometabdisplay"=>1, "cat_status"=>1)));
                    ?>
                    <div class="tab-pane <?php if($cnt == 1) echo "active"; ?>" id="tab<?php echo $cnt; ?>">
                      <?php
                      if(!empty($userlist)){
                        foreach($userlist as $uslist){
                          $pro_path = $uslist['cat_img_path'];
                          $path = urlencode($uslist['cat_name']);
                        ?>
                          <div class="col-md-2 col-sm-4">
                            <div class="intrst-link-sub-sec">     
                              <a class="categorylistingdev" href="<?php echo base_url("Pages/setSearchDelimiters?category=$path"); ?>">
                              <div class="ilss-img"> <img src="<?php echo base_url("assets/img/category/$pro_path"); ?>"> </div>
                              <div class="ilss-txt">
                                <h4><?php                                   
                                  if(strlen($uslist['cat_name']) > 40 ){
                                    ?>
                                    <?php echo substr(ucwords($uslist['cat_name']), 0, 30)."..."; ?>  
                                    <?php
                                  }else{
                                    ?>
                                    <?php echo ucwords($uslist['cat_name']); ?>  
                                    <?php
                                  }
                                ?>                                  
                                </h4>
                                <!-- <p><a href="<?php //echo base_url("Pages/businessListing?category=$path"); ?>"><?php echo $headings['label12']; ?></a></p> -->
                              </div>
							  </a>
                            </div>
                          </div>
                        <?php
                        }
                      }
                      else{
                        ?>
                        <div class="col-md-12">
                          <h3><?php echo $headings['label13']; ?></h3>
                        </div>
                        <?php
                      }
                      ?>
                      <div class="more-cat-link">
                        <div class="col-md-12"> <a href="#">View all categories</a> </div>
                      </div>
                    </div>
                    <?php
                    $cnt++;
                  }
                }
                ?>
                <!-- <div class="tab-pane active" id="tab1">
                  <div class="category-sec-link">
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-01.png"> </div>
                        <div class="ilss-txt">
                          <h4>Electricien</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-02.png"> </div>
                        <div class="ilss-txt">
                          <h4>Plombier</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-03.png"> </div>
                        <div class="ilss-txt">
                          <h4>Carreleur</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-04.png"> </div>
                        <div class="ilss-txt">
                          <h4>Peintre</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-05.png"> </div>
                        <div class="ilss-txt">
                          <h4>Maçon</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-06.png"> </div>
                        <div class="ilss-txt">
                          <h4>Menuisier</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="more-cat-link">
                    <div class="col-md-12"> <a href="#">View all categories</a> </div>
                  </div>
                </div>               
                <div class="tab-pane" id="tab2">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab3">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab4">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab5">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab6">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="popu-countries">
    <div class="container">
      <div class="interstng-sec-head">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <h2><?php echo $headings['label9']; ?></h2>
            </div>
          </div>
        </div>
      </div>
      <div class="popu-con-section">
      
      		<div class="customNavigation">
          
                <a class="btn prev">Previous</a>
                <a class="btn next">Next</a>
              </div>
              <div id="owl-demo-pro" class="owl-carousel">
			  <?php 
        foreach ($publishonhome as $rows) {
            ?>
                
                <div class="item">
                <div class="popu-con-sub">
              <div class="popu-img"> 
			  <a href="<?php echo base_url("Users/user_profile/").$rows['user_id']; ?>">
			  <img src="<?php echo base_url('assets/profile_pics/').$rows['user_profile_pic'];?>">
                <div class="popu-ovrly"> 
                  <h3><?php echo $rows['user_company_name']; ?></h3>
                   </div>
				   </a>
              </div>
              <div class="popu-txt">
                <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                <div class="like-comm">
                  <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['total_recommand']; ?></div>
                  <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i><?php echo $rows['total_review']; ?> comment</div>
                </div>
              </div>
            </div>
            </div>

          <?php
          }
          ?>
              </div>
              
      
        <div class="row">
         
        </div>
      </div>
    </div>
  </section>
  <section class="other-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="other-sec-sub">
            <?php
            $promoimg31 = $section31['uploaded_file_path'];
            $promoimg32 = $section32['uploaded_file_path'];
            $promoimg33 = $section33['uploaded_file_path'];
            ?>
            <div class="oss-img"> <img src="<?php echo base_url("assets/img/$promoimg31");?>"> </div>
            <div class="oss-txt">
              <h3><?php echo $section31['section']; ?></h3>
              <p><?php echo $section31['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="other-sec-sub">
            <div class="oss-img"> <img src="<?php echo base_url("assets/img/$promoimg32");?>"> </div>
            <div class="oss-txt">
              <h3><?php echo $section32['section']; ?></h3>
              <p><?php echo $section32['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="other-sec-sub">
            <div class="oss-img"> <img src="<?php echo base_url("assets/img/$promoimg33");?>"> </div>
            <div class="oss-txt">
              <h3><?php echo $section33['section']; ?></h3>
              <p><?php echo $section33['content']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="home-vdeo-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="vd--txt">
            <h3><?php echo $section5['section']; ?></h3>
            <?php
            echo $section5['content'];
            ?>
          </div>
        </div>
        <div class="col-md-7">
          <div class="vdo-sec-main"> 
          <?php
            $vidname = $section5['uploaded_file_path'];
            $youtube = $section5['background_img_path'];
            if($section5['video_type'] == 2){
          ?>
          <video width="100%" controls poster="<?php echo base_url("assets/img/poster.jpg"); ?>">          
            <source src="<?php echo base_url("assets/img/homevideo/$vidname"); ?>" type="video/mp4">
          </video>
          <!-- <img src="<?php echo base_url();?>assets/img/vdo-img.jpg">  -->

            <div class="playpause"></div>
          <?php
          }if($section5['video_type'] == 1){
          ?>
            <!-- <object width="420" height="315"
            data="<?php echo $youtube; ?>">
            </object> -->
<iframe width="620" height="420" src="https://www.youtube.com/embed/<?php echo $youtube; ?>?rel=0&amp;controls=0&amp;showinfo=0?ecver=1" frameborder="0" allowfullscreen></iframe>

            

          <?php
          }
          ?>
         </div>

        <!--  <div id='playerzpumcZwzBZKD'></div>
<script type='text/javascript'>
    jwplayer('playerzpumcZwzBZKD').setup({
        file: "<?php echo base_url().'assets/img/homevideo/'.$vidname; ?>",
        image: '//www.longtailvideo.com/content/images/jw-player/lWMJeVvV-876.jpg',
        width: '100%',
        aspectratio: '16:9'
    });
</script>-->

        </div>
      </div>
    </div>
  </section>

  <?php
$backimgsec=base_url("assets/img/locaton-img.jpg");
  if(isset($section6Bk['uploaded_file_path']) && $section6Bk['uploaded_file_path'] != "")
  {
    $backimgsec = base_url("assets/img/").$section6Bk['uploaded_file_path'];
  }

  ?>

<!-- 
?> -->
  <section class="contxt-location-sec" style="background: url(<?php echo $backimgsec; ?>); background-size:cover !important; background-attachment: fixed !important;     background-position: right 0 top 0 !important; background-repeat: no-repeat !important;">
  
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img61 = $section61['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img61"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section61['section']; ?></h4>
              <p><?php echo $section61['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img62 = $section62['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img62"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section62['section']; ?></h4>
              <p><?php echo $section62['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img63 = $section63['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img63"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section63['section']; ?></h4>
              <p><?php echo $section63['content']; ?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img64 = $section64['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img64"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section64['section']; ?></h4>
              <p><?php echo $section64['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img65 = $section65['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img65"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section65['section']; ?></h4>
              <p><?php echo $section65['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img66 = $section66['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img66"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section66['section']; ?></h4>
              <p><?php echo $section66['content']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <?php
$backimg=base_url("assets/img/parallax-01.jpg");
  if(isset($section7['background_img_path']) && $section7['background_img_path'] != "")
  {
$backimg = base_url("assets/img/").$section7['background_img_path'];
  }

  ?>
  <section class="info-parralax" style="background: url(<?php echo $backimg; ?>); background-size:100%!important;  background-attachment: fixed!important;">
    <div class="container">
      <div class="col-md-offset-7 col-md-5">
        <div class="info-parralax-txt">
          <h3><?php echo $section7['section']; ?></h3>
          <?php
          echo $section7['content'];
          ?>
        </div>
      </div>
    </div>
  </section>
  <section class="test-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="popu-cont-test">
            <div class="interstng-sec-head">
              <div class="col-md-12">
                <h2><?php echo $headings['label10']; ?></h2>
              </div>
            </div>
            <div class="popu-con-section">
              <div class="row">
              
              <div id="owl-demo-down" class="owl-carousel">
			  <?php foreach ($toprated as $rows) {
            ?>
                
                <div class="item">
                <div class="popu-con-sub">
				<a href="<?php echo base_url("Users/user_profile/").$rows['user_id']; ?>">
              <div class="popu-img"> 
			    <?php  profile_pic($rows['user_profile_pic'],$rows['cate_id']); ?>
			  
			  
                <div class="popu-ovrly"> 
                  <h3><?php echo $rows['user_company_name']; ?></h3>
                   </div>
              </div>
              <div class="popu-txt">
                <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                <div class="like-comm">
                  <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['total_recommand']; ?></div>
                  <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i><?php echo $rows['total_review']; ?> comment</div>
                </div>
              </div>
			  </a>
            </div>
            </div>

          <?php
          }
          ?>
              </div>
            
              </div>
              
              <!--  end loop here  --> 
              
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="test-slider">
            <div class="row">
              <div class="interstng-sec-head">
                <div class="col-md-12">
                  <h2><?php echo $headings['label11']; ?></h2>
                </div>
              </div>
            </div>
            <div id="owl-demo" class="owl-demo owl-carousel owl-theme">
              <?php
                  if(!empty($testimonials))
                    foreach($testimonials as $testi){
                  ?>
              <div class="item">
                <div class="test-sub">
                  <h4><?php echo strtoupper($testi['tst_by']); ?></h4>
                  <h5><?php echo $testi['tst_designation']; ?></h5>
                  <p><?php echo $testi['tst_content']; ?></p>
                </div>
              </div>
              <?php
                    }
                  ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

   <?php
$backimg2=base_url("assets/img/parallax-02.jpg");
  if(isset($section9['background_img_path']) && $section9['background_img_path'] != "")
  {
$backimg2 = base_url("assets/img/").$section9['background_img_path'];
  }

  ?>

  <section class="info-parralax-2" style="background: url(<?php echo $backimg2; ?>); background-size:cover !important; background-attachment: fixed !important;background-position: center;">
    <div class="container">
      <div class="col-md-5">
        <div class="info-parralax-txt">
          <h3><?php echo $section9['section']; ?></h3>
          <?php          
            echo $section9['content'];
          ?>
        </div>
      </div>
    </div>
  </section>
</div>





