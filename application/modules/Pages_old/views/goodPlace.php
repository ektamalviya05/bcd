<section class="search_event-list">
	<div class="container">
		<div class="row">		
			<div class="col-sm-12">
				<div class="search_event-filter">
                    <form autocomplete="off" action="<?php echo base_url("Pages/goodPlace") ?>" method="POST">
    					<div class="event-name">
                        	<div class="adinput extra-select extxt">
                            	<input type="text" placeholder="Adresse" class="citycomplete" name="location" value="<?php echo $this->session->userdata("locat"); ?>">
                            </div>
                            <div class="adinput extra-select extxt">
                            	<input type="text" placeholder="Mot-clés" name="keyword" value="<?php echo $this->session->userdata("keyword"); ?>">
                            </div>
    						<div class="adinput extra-select extxt">
                                <select name="brand_name">
                                  <option value="">Type</option>
                                  <?php 
                                  if(!empty($evttype)){
                                    foreach($evttype as $tp){
                                        ?>
                                        <option value="<?php echo $tp['evt_type_id']; ?>" <?php if($tp['evt_type_id'] == $this->session->userdata("eventtype")) echo "selected"; ?>><?php echo $tp['evt_type']; ?></option>
                                        <?php
                                    }
                                  }
                                  ?>
                                </select>
                            </div>
                            <div class="filter-button">
                            	<input class="seacrch" type="submit" value="Rechercher" />
                                <!-- <a href="<?php echo base_url("Pages/clearGoodPlaceFilter"); ?>" class="btn btn-primary clfilter">Clear Filter</a> -->
                            </div>
    					</div>
                    </form>
				</div>
			</div>
		</div>
	</div>
</section>


<section>
	<div class="search-map-sec for-map-full">
		<div class="container">
			<div class="row">
            <div class="search-main" id="eventmap">
        	   <!-- <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2483.878459718958!2d-0.10855068438845887!3d51.49709781927763!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487604bcd118927b%3A0x3b731da034bd8490!2sNotre+Dame+Roman+Catholic+Girls&#39;+School!5e0!3m2!1sen!2suk!4v1506583059239" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe> -->
            </div>
                <div class="search-listing-sec">
                    <?php
                    if(!empty($listing) && count($listing) > 0){
                        $markerid = 0;
                        foreach($listing as $evnt){
                            $picpath = $evnt['user_profile_pic'];

                        ?>
                            <div class="mes-commercant">
                                <div class="listing-bon-plan">
                                    <div class="evnt-listing">
                                        <a href="<?php echo base_url("Users/eventsView/").$evnt['event_id']; ?>" target="_blank" class="event-class" id="<?php echo 'evt-'.$markerid; ?>">
                                            <ul class="event-list">
                                                <li>
                                                    <div class="mang-envt-time">
                                                        <span class="day"><?php echo date("d", strtotime($evnt['event_start_date'])); ?></span>
                                                        <span class="month"><?php echo date("M", strtotime($evnt['event_start_date'])); ?></span>
                                                        <span class="year"><?php echo date("Y", strtotime($evnt['event_start_date'])); ?></span>           
                                                    </div>
                                                    <div class="mang-evnt-dicrp">
                                                        <h2 class="title"><?php echo substr(ucwords($evnt['event_name']), 0, 20); ?></h2>
                                                        <p class="desc"><?php echo substr($evnt['event_description'], 0, 40); ?></p>

                                                        <p class="ev-type"><span>Event Type : - <?php echo $evnt['evt_type']; ?></span></p>
                                                        <p class="ev-type"><span>Address : - <?php echo $evnt['event_address']; ?></span></p>
                                                    </div>
                                                    <div class="mang-evnt-usr-pro">
                                                        <div class="profile-image">
                                                            <img src="<?php echo base_url("assets/profile_pics/$picpath"); ?>">
                                                        </div>
                                                        <div class="mang-evnt-usr-profile ">
                                                            <strong>
                                                                <?php 
                                                                echo substr(ucwords($evnt['user_firstname']." ".$evnt['user_lastname']), 0 , 20);                                                
                                                                ?> 
                                                                <?php //echo ucwords($evnt['user_company_name']); ?>
                                                            </strong>
                                                            <article>
                                                                <?php
                                                                    if(!empty($evnt['bs_name'])) 
                                                                        echo substr(ucwords($evnt['bs_name']), 0 , 20);                                                                    
                                                                ?>
                                                            </article>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </a>
                                    </div>
                                </div>                                                    
                            </div>
                        <?php
                        $markerid++;
                        }
                    }
                    ?>
                </div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
  jQuery(window).scroll(function() {   
        if (jQuery(window).scrollTop() >=50) { 
          if(jQuery('.search_event-list').hasClass( "fs-top" ))
        {
      //jQuery('#header').removeClass("fixed-theme");
      }
      else
      {
      jQuery('.search_event-list').addClass("fs-top");
      }
        }
    else{
    jQuery('.search_event-list').removeClass("fs-top");
    }
   });
</script> 