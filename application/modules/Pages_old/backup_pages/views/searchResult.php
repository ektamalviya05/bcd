<div class="main-content">
  <div class="main-reg-content">
    <section class="filter-sec">
      <div class="container">
        <form name="searchForm" action="#" method="post">
          <div class="main-srch-form-sec">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
        <input type="text" name="user_address" class="form-control add-form" onFocus="geolocate()" class="form-control" placeholder="VILLE,CODE POSTAL">
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
        <input type="text" name="business_category1" id="business_category1" 
                  list='categories1'
                  name='user_categories'
                  id="business_category"
                  class="form-control cat-form" placeholder="Entrez une catégorie">
                  <datalist id="categories1">
                    <?php
                    foreach($business_categories->result() as $cate){
                      ?>
                      <option value="<?php echo $cate->cat_name; ?>"><?php echo $cate->cat_name; ?></option>
                      <?php
                    }
                    ?>                        
                  </datalist>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
        <input type="text" class="form-control" name="keyword" placeholder="KEYWORD">
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <a class="btn btn-default extra-filter" href="javascript:void()">Extra Filters</a>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <input class="btn btn-default main-sbmit" name="searchSubmit" value="Search" type="submit" />
              </div>
            </div>
          </div>
          <div class="extra-filter-sec">
            <div class="row">
              <div class="col-md-4">
                <div class="etra-fil-head">
                  <h3>Services</h3>
                </div>
                <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark">
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-01" name="check" />
                      <label class="chk-box-label" for="ser-01"></label><span>A domicile</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-02" name="check" />
                      <label class="chk-box-label" for="ser-02"></label><span>Animaux acceptés</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-03" name="check" />
                      <label class="chk-box-label" for="ser-03"></label><span>Conciergerie</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-04" name="check" />
                      <label class="chk-box-label" for="ser-04"></label><span>Conseil personnalisé</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-05" name="check" />
                      <label class="chk-box-label" for="ser-05"></label><span>Dégustation possible</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-24" name="check" />
                      <label class="chk-box-label" for="ser-24"></label><span>Dépannage / Réparation</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-06" name="check" />
                      <label class="chk-box-label" for="ser-06"></label><span>Devis gratuit</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-07" name="check" />
                      <label class="chk-box-label" for="ser-07"></label><span>Drive</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-08" name="check" />
                      <label class="chk-box-label" for="ser-08"></label><span>Echanges produits</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-09" name="check" />
                      <label class="chk-box-label" for="ser-09"></label><span>Espace bien-être (Spa, Sauna…)</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-10" name="check" />
                      <label class="chk-box-label" for="ser-10"></label><span>Espace détente</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-11" name="check" />
                      <label class="chk-box-label" for="ser-11"></label><span>Espace jeux pour enfants</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-12" name="check" />
                      <label class="chk-box-label" for="ser-12"></label><span>Essayage/test produit possible</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-13" name="check" />
                      <label class="chk-box-label" for="ser-13"></label><span>Point restauration</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-14" name="check" />
                      <label class="chk-box-label" for="ser-14"></label><span>Prêt de poussettes</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-15" name="check" />
                      <label class="chk-box-label" for="ser-15"></label><span>Prêt de véhicule</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-16" name="check" />
                      <label class="chk-box-label" for="ser-16"></label><span>Retouches</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-17" name="check" />
                      <label class="chk-box-label" for="ser-17"></label><span>Sans rendez-vous</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-18" name="check" />
                      <label class="chk-box-label" for="ser-18"></label><span>Service 24h/24 7j/7</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-19" name="check" />
                      <label class="chk-box-label" for="ser-19"></label><span>Tarif de groupe</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-20" name="check" />
                      <label class="chk-box-label" for="ser-20"></label><span>Terrasse</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-21" name="check" />
                      <label class="chk-box-label" for="ser-21"></label><span>Travaux</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-22" name="check" />
                      <label class="chk-box-label" for="ser-22"></label><span>Vente à domicile</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-23" name="check" />
                      <label class="chk-box-label" for="ser-23"></label><span>Visite de chantiers</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="etra-fil-head">
                  <h3>Produits/marques</h3>
                </div>
                <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark">
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-25" name="check" />
                      <label class="chk-box-label" for="ser-25"></label><span>Adidas</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-26" name="check" />
                      <label class="chk-box-label" for="ser-26"></label><span>Agatha</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-27" name="check" />
                      <label class="chk-box-label" for="ser-27"></label><span>Allibert</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-28" name="check" />
                      <label class="chk-box-label" for="ser-28"></label><span>André</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-29" name="check" />
                      <label class="chk-box-label" for="ser-29"></label><span>Black et Decker</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-30" name="check" />
                      <label class="chk-box-label" for="ser-30"></label><span>Bosch</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-31" name="check" />
                      <label class="chk-box-label" for="ser-31"></label><span>Célio</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-32" name="check" />
                      <label class="chk-box-label" for="ser-32"></label><span>Chanel</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-33" name="check" />
                      <label class="chk-box-label" for="ser-33"></label><span>Dior</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-34" name="check" />
                      <label class="chk-box-label" for="ser-34"></label><span>Eden Park</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-35" name="check" />
                      <label class="chk-box-label" for="ser-35"></label><span>Esprit</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-36" name="check" />
                      <label class="chk-box-label" for="ser-36"></label><span>Facom</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-37" name="check" />
                      <label class="chk-box-label" for="ser-37"></label><span>Filière courte direct producteur</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-38" name="check" />
                      <label class="chk-box-label" for="ser-38"></label><span>Fisher-Price</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-39" name="check" />
                      <label class="chk-box-label" for="ser-39"></label><span>Givenchy</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-40" name="check" />
                      <label class="chk-box-label" for="ser-40"></label><span>Guy Degrenne</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-41" name="check" />
                      <label class="chk-box-label" for="ser-41"></label><span>Jacadi</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-42" name="check" />
                      <label class="chk-box-label" for="ser-42"></label><span>Jacob Delafon</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-43" name="check" />
                      <label class="chk-box-label" for="ser-43"></label><span>Lacoste</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-44" name="check" />
                      <label class="chk-box-label" for="ser-44"></label><span>Lancel</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-45" name="check" />
                      <label class="chk-box-label" for="ser-45"></label><span>Longchamps</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-46" name="check" />
                      <label class="chk-box-label" for="ser-46"></label><span>Loréal</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-47" name="check" />
                      <label class="chk-box-label" for="ser-47"></label><span>Nike</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-48" name="check" />
                      <label class="chk-box-label" for="ser-48"></label><span>Petit Bateau</span>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-4">
                <div class="etra-fil-head">
                  <h3>Informations Pratiques</h3>
                </div>
                <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark">
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-49" name="check" />
                      <label class="chk-box-label" for="ser-49"></label><span>Adidas</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-50" name="check" />
                      <label class="chk-box-label" for="ser-50"></label><span>Agatha</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-51" name="check" />
                      <label class="chk-box-label" for="ser-51"></label><span>Allibert</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-52" name="check" />
                      <label class="chk-box-label" for="ser-52"></label><span>André</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-53" name="check" />
                      <label class="chk-box-label" for="ser-53"></label><span>Black et Decker</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-54" name="check" />
                      <label class="chk-box-label" for="ser-54"></label><span>Bosch</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-55" name="check" />
                      <label class="chk-box-label" for="ser-55"></label><span>Célio</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-56" name="check" />
                      <label class="chk-box-label" for="ser-56"></label><span>Chanel</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-57" name="check" />
                      <label class="chk-box-label" for="ser-57"></label><span>Dior</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-58" name="check" />
                      <label class="chk-box-label" for="ser-58"></label><span>Eden Park</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-59" name="check" />
                      <label class="chk-box-label" for="ser-59"></label><span>Esprit</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-60" name="check" />
                      <label class="chk-box-label" for="ser-60"></label><span>Facom</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-61" name="check" />
                      <label class="chk-box-label" for="ser-61"></label><span>Filière courte direct producteur</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-62" name="check" />
                      <label class="chk-box-label" for="ser-62"></label><span>Fisher-Price</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-63" name="check" />
                      <label class="chk-box-label" for="ser-63"></label><span>Givenchy</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-64" name="check" />
                      <label class="chk-box-label" for="ser-64"></label><span>Guy Degrenne</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-65" name="check" />
                      <label class="chk-box-label" for="ser-65"></label><span>Jacadi</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-66" name="check" />
                      <label class="chk-box-label" for="ser-66"></label><span>Jacob Delafon</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-67" name="check" />
                      <label class="chk-box-label" for="ser-67"></label><span>Lacoste</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-68" name="check" />
                      <label class="chk-box-label" for="ser-68"></label><span>Lancel</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-69" name="check" />
                      <label class="chk-box-label" for="ser-69"></label><span>Longchamps</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-70" name="check" />
                      <label class="chk-box-label" for="ser-70"></label><span>Loréal</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-71" name="check" />
                      <label class="chk-box-label" for="ser-71"></label><span>Nike</span>
                    </div>
                  </div>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                      <input type="checkbox" value="None" id="ser-72" name="check" />
                      <label class="chk-box-label" for="ser-72"></label><span>Petit Bateau</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </section>
    <section class="main-map-sec">
      <div class="map-main">
        <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14722.96212982255!2d75.92889325!3d22.700701!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1496980678600" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
      <div class="main-listing-sec">
        
<?php foreach ($search_results as $row) {
  ?>
        <div class="listing-sub">
          <div class="image-lis-sub">
            <img src="<?php echo base_url("assets/img/loginicon.png"); ?>">
          </div>
          <div class="image-txt-sub">
            <div class="img-txt-subhead">
              <h3><?php echo $row['user_company_name'] != ""? $row['user_company_name']: "Name not in DB"; ?></h3>
              <span class="rating-main">
                <ul>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                </ul>
              </span>
            </div>
            <p class="add-l"><i class="fa fa-map" aria-hidden="true"></i><?php echo $row['user_company_address'] != ""? $row['user_company_address']: "Address not in DB"; ?></p>
            <p class="catgry-l"><span class="f-cat"><i class="fa fa-bars" aria-hidden="true"></i> Categorie(s): </span><span><?php echo $row['categories'] != ""? $row['categories']: "Category not in DB"; ?></span></p>
            <p class="distance-l"><span class="f-cat"><i class="fa fa-map-marker"></i>Distance: </span><span><?php echo $row['user_lat']. ', '.  $row['user_long']; ?></span></p>
            <div class="dsply-num">
              <button class="btn btn-default"><i class="fa fa-phone"></i>Afficher le numéro</button>
            </div>
          </div>
        </div>
      <?php

      # code...
}
      ?>
        <?php //print_r($search_results); ?>       
      </div>    
    </section>
  </div>
</div>
