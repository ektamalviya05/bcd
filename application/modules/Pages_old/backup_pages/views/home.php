<div class="main-content">
  <section class="main-search-sec">
    <div id="myCarousel" class="carousel slide" data-ride="carousel"> 
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
      </ol>
      
      <!-- Wrapper for slides -->
      <div class="carousel-inner">
        <?php
          $cnt = 1;
          foreach($bn_images as $imgs){
            $imgpath = $imgs['bn_path'];
            ?>
              <div class="item <?php if($cnt == 1) echo "active"; ?>"> <img src="<?php echo base_url("assets/img/Banners/$imgpath"); ?>" alt="Los Angeles"> </div>
            <?php
            $cnt++;
          }

        ?>
        <!-- <div class="item active"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/slider1.jpg" alt="Los Angeles"> </div>
        <div class="item"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/slider1.jpg" alt="Chicago"> </div>
        <div class="item"> <img src="http://votivephp.in/VotiveYellowPages/assets/img/slider1.jpg" alt="New York"> </div> -->
      </div>
      
      <!-- Left and right controls --> 
      <a class="left carousel-control" href="#myCarousel" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> <span class="sr-only">Previous</span> </a> <a class="right carousel-control" href="#myCarousel" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> <span class="sr-only">Next</span> </a> </div>
    <div class="container">
      <div class="header-srch">
        <div class="row">
          <div class="col-md-12">
            <div class="main-srch-heading">
              <h1>Find Nearby Attractions</h1>
              <p>Expolore top-rated attractions, activities and more</p>
            </div>
          </div>
        </div>
      </div>
      <div class="main-srch-sub">
        <div class="row">
          <form method="post" action="<?php echo base_url("Pages/searchResult"); ?>">
            <div class="col-md-4">
              <div class="form-group form-sub"> 
                
                <!-- <input type="address" class="form-control add-form" placeholder="Entrez votre adresse">-->
                
                <input id="autocomplete" placeholder="Entrez votre adresse" name="user_address" class="form-control add-form" onFocus="geolocate()" type="text">
                </input>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-group form-sub">
                <input type="text" name="business_category1" id="business_category1" 
                  list='categories1'
                  name='user_categories'
                  id="business_category"
                  class="form-control cat-form" placeholder="Entrez une catégorie">
                <datalist id="categories1">
                  <?php
                    foreach($business_categories->result() as $cate){
                      ?>
                  <option value="<?php echo $cate->cat_name; ?>"><?php echo $cate->cat_name; ?></option>
                  <?php
                    }
                    ?>
                </datalist>
              </div>
            </div>
            <div class="col-md-2">
              <div class="form-group form-sub no-bord">
                <input class="form-control" placeholder="Mot-clés">
              </div>
            </div>
            <div class="col-md-2">
              <button class="btn btn-default" type="Submit"><i class="fa fa-search"></i> Search</button>
            </div>
          </form>
        </div>
      </div>
      <div class="srch-links">
        <div class="row">
          <div class="col-md-12">
            <ul>
              <li> <a class="srch-link" href="#">
                <div class="img-srch"> <img src="<?php echo base_url();?>assets/img/srch-01.png"> </div>
                <div class="srchtxt">
                  <p>Car</p>
                </div>
                </a> </li>
              <li> <a class="srch-link" href="#">
                <div class="img-srch"> <img src="<?php echo base_url();?>assets/img/srch-02.png"> </div>
                <div class="srchtxt">
                  <p>Food & Drinks</p>
                </div>
                </a> </li>
              <li> <a class="srch-link" href="#">
                <div class="img-srch"> <img src="<?php echo base_url();?>assets/img/srch-03.png"> </div>
                <div class="srchtxt">
                  <p>Travels</p>
                </div>
                </a> </li>
              <li> <a class="srch-link" href="#">
                <div class="img-srch"> <img src="<?php echo base_url();?>assets/img/srch-04.png"> </div>
                <div class="srchtxt">
                  <p>Business</p>
                </div>
                </a> </li>
              <li> <a class="srch-link" href="#">
                <div class="img-srch"> <img src="<?php echo base_url();?>assets/img/srch-05.png"> </div>
                <div class="srchtxt">
                  <p>Shopping</p>
                </div>
                </a> </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="scrol-btm"> <a href="#scroll-here"><i class="fa fa-angle-down"></i></a> </div>
    </div>
  </section>
  <section class="interstng-sec" id="scroll-here">
    <div class="container">
      <div class="interstng-sec-head">
        <div class="col-md-12">
          <h2><span>Explore</span> what's interesting</h2>
          <p>Ne his postulant posidonium adversarium. Ius tollit tamquam indoctum ea, cu quo equidem perfecto adipiscing. <br>
            Eu mel aliquid delenit. Recteque laboramus ea est, te qui eirmod similique.</p>
        </div>
      </div>
      <div class="intertng-links-sec">
        <div class="panel panel-primary">
          <div class="row">
            <div class="col-md-12">
              <ul class="nav panel-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-wrench" aria-hidden="true"></i> Travaux</a></li>
                <li><a href="#tab2" data-toggle="tab"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Shopping</a></li>
                <li><a href="#tab3" data-toggle="tab"><i class="fa fa-handshake-o" aria-hidden="true"></i> Services</a></li>
                <li><a href="#tab4" data-toggle="tab"><i class="fa fa-heartbeat" aria-hidden="true"></i> Santé</a></li>
                <li><a href="#tab5" data-toggle="tab"><i class="fa fa-glass" aria-hidden="true"></i> Goûts et Saveurs</a></li>
                <li><a href="#tab6" data-toggle="tab"><i class="fa fa-cutlery" aria-hidden="true"></i> Sortis et Loisirs</a></li>
              </ul>
            </div>
          </div>
          <div class="row">
            <div class="panel-body">
              <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                  <div class="category-sec-link">
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-01.png"> </div>
                        <div class="ilss-txt">
                          <h4>Electricien</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-02.png"> </div>
                        <div class="ilss-txt">
                          <h4>Plombier</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-03.png"> </div>
                        <div class="ilss-txt">
                          <h4>Carreleur</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-04.png"> </div>
                        <div class="ilss-txt">
                          <h4>Peintre</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-05.png"> </div>
                        <div class="ilss-txt">
                          <h4>Maçon</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2 col-sm-4">
                      <div class="intrst-link-sub-sec">
                        <div class="ilss-img"> <img src="<?php echo base_url();?>assets/img/icon-06.png"> </div>
                        <div class="ilss-txt">
                          <h4>Menuisier</h4>
                          <p><a href="#">view more</a></p>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="more-cat-link">
                    <div class="col-md-12"> <a href="#">View all categories</a> </div>
                  </div>
                </div>
                <div class="tab-pane" id="tab2">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab3">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab4">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab5">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
                <div class="tab-pane" id="tab6">
                  <div class="col-md-12">
                    <h3>No Details</h3>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="popu-countries">
    <div class="container">
      <div class="interstng-sec-head">
        <div class="row">
          <div class="col-md-12">
            <div class="row">
              <h2>Most popular companies</h2>
            </div>
          </div>
        </div>
      </div>
      <div class="popu-con-section">
      
      		<div class="customNavigation">
                <a class="btn prev">Previous</a>
                <a class="btn next">Next</a>
              </div>
              <div id="owl-demo-pro" class="owl-carousel">
			  <?php foreach ($publishonhome as $rows) {
            ?>
                
                <div class="item">
                <div class="popu-con-sub">
              <div class="popu-img"> <img src="<?php echo base_url();?>assets/img/popu.jpg">
                <div class="popu-ovrly"> <a href="#">
                  <h3><?php echo $rows['user_company_name']; ?></h3>
                  </a> </div>
              </div>
              <div class="popu-txt">
                <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                <div class="like-comm">
                  <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['counts']; ?></div>
                  <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>
                </div>
              </div>
            </div>
            </div>

          <?php
          }
          ?>
              </div>
              
      
        <div class="row">
          <?php /*?><?php foreach ($publishonhome as $rows) {
            ?>
          <div class="col-md-3 col-sm-6">
            <div class="popu-con-sub">
              <div class="popu-img"> <img src="<?php echo base_url();?>assets/img/popu.jpg">
                <div class="popu-ovrly"> <a href="#">
                  <h3><?php echo $rows['user_company_name']; ?></h3>
                  </a> </div>
              </div>
              <div class="popu-txt">
                <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                <div class="like-comm">
                  <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['counts']; ?></div>
                  <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>
                </div>
              </div>
            </div>
          </div>
          <?php
          }
          ?><?php */?>
          <!--
          <div class="col-md-3 col-sm-6">

            <div class="popu-con-sub">

              <div class="popu-img">

                <img src="<?php echo base_url();?>assets/img/popu-2.jpg">

                <div class="popu-ovrly">

                  <a href="#"><h3>The Fishery</h3></a>

                </div>

              </div>

              <div class="popu-txt">

                <div class="popu-loca">

                  <i class="fa fa-map-marker"></i><span>Rouen, France</span>

                </div>

                <div class="like-comm">

                   <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i>500</div>

                   <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>

                </div>

              </div>

            </div>

          </div>

          <div class="col-md-3 col-sm-6">

            <div class="popu-con-sub">

              <div class="popu-img">

                <img src="<?php echo base_url();?>assets/img/popu-3.jpg">

                <div class="popu-ovrly">

                  <a href="#"><h3>HURE COMPANY</h3></a>

                </div>

              </div>

              <div class="popu-txt">

                <div class="popu-loca">

                  <i class="fa fa-map-marker"></i><span>Rouen, France</span>

                </div>

                <div class="like-comm">

                   <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i>500</div>

                   <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>

                </div>

              </div>

            </div>

          </div>

          <div class="col-md-3 col-sm-6">

            <div class="popu-con-sub">

              <div class="popu-img">

                <img src="<?php echo base_url();?>assets/img/popu-4.jpg">

                <div class="popu-ovrly">

                  <a href="#"><h3>BALENZO</h3></a>

                </div>

              </div>

              <div class="popu-txt">

                <div class="popu-loca">

                  <i class="fa fa-map-marker"></i><span>Rouen, France</span>

                </div>

                <div class="like-comm">

                   <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i>500</div>

                   <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>

                </div>

              </div>

            </div>

          </div>
--> 
        </div>
      </div>
    </div>
  </section>
  <section class="other-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="other-sec-sub">
            <div class="oss-img"> <img src="<?php echo base_url();?>assets/img/other-01.png"> </div>
            <div class="oss-txt">
              <h3>Ne cherchez plus, les bons plans viennent à vous !</h3>
              <p>Promotions, offres spéciales, soldes, ventes privées, ventes flash, liquidation... Vous êtes informés en temps réel, soyez les premiers à en profiter.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="other-sec-sub">
            <div class="oss-img"> <img src="<?php echo base_url();?>assets/img/other-02.png"> </div>
            <div class="oss-txt">
              <h3>Profitez de la Communauté Otourdemoi</h3>
              <p>Découvrez les avis des autres clients pour faire le bon choix.<br>
                Partagez vos expériences.</p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="other-sec-sub">
            <div class="oss-img"> <img src="<?php echo base_url();?>assets/img/other-03.png"> </div>
            <div class="oss-txt">
              <h3>Tchatez en direct avec des Pros</h3>
              <p>Ils répondent à toutes vos questions en quelques minutes.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="home-vdeo-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <div class="vd--txt">
            <h3><?PHP echo $section5['section']; ?></h3>
            <?php
            echo $section5['content'];
            ?>
          </div>
        </div>
        <div class="col-md-7">
          <div class="vdo-sec-main"> <img src="<?php echo base_url();?>assets/img/vdo-img.jpg"> </div>
        </div>
      </div>
    </div>
  </section>
  <section class="contxt-location-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img61 = $section61['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img61"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section61['section']; ?></h4>
              <p><?php echo $section61['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img62 = $section62['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img62"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section62['section']; ?></h4>
              <p><?php echo $section62['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img63 = $section63['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img63"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section63['section']; ?></h4>
              <p><?php echo $section63['content']; ?></p>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img64 = $section64['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img64"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section64['section']; ?></h4>
              <p><?php echo $section64['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img65 = $section65['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img65"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section65['section']; ?></h4>
              <p><?php echo $section65['content']; ?></p>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="cls-sub">
            <div class="cls-sub-img">
              <?php
              $img66 = $section66['uploaded_file_path'];
              ?>
              <img src="<?php echo base_url("assets/img/$img66"); ?>"> </div>
            <div class="cls-sub-txt">
              <h4><?php echo $section66['section']; ?></h4>
              <p><?php echo $section66['content']; ?></p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="info-parralax">
    <div class="container">
      <div class="col-md-offset-7 col-md-5">
        <div class="info-parralax-txt">
          <h3><?php echo $section7['section']; ?></h3>
          <?php
          echo $section7['content'];
          ?>
        </div>
      </div>
    </div>
  </section>
  <section class="test-sec">
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <div class="popu-cont-test">
            <div class="interstng-sec-head">
              <div class="col-md-12">
                <h2>Most popular companies</h2>
              </div>
            </div>
            <div class="popu-con-section">
              <div class="row">
              
              <div id="owl-demo-down" class="owl-carousel">
			  <?php foreach ($toprated as $rows) {
            ?>
                
                <div class="item">
                <div class="popu-con-sub">
              <div class="popu-img"> <img src="<?php echo base_url();?>assets/img/popu.jpg">
                <div class="popu-ovrly"> <a href="#">
                  <h3><?php echo $rows['user_company_name']; ?></h3>
                  </a> </div>
              </div>
              <div class="popu-txt">
                <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                <div class="like-comm">
                  <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['counts']; ?></div>
                  <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>
                </div>
              </div>
            </div>
            </div>

          <?php
          }
          ?>
              </div>
              
               <?php /*?> <?php
              foreach ($toprated as $rows) {
            ?>
                <div class="col-md-6">
                  <div class="popu-con-sub">
                    <div class="popu-img"> <img src="<?php echo base_url();?>assets/img/popu-3.jpg">
                      <div class="popu-ovrly"> <a href="#">
                        <h3><?php echo $rows['user_company_name']; ?></h3>
                        </a> </div>
                    </div>
                    <div class="popu-txt">
                      <div class="popu-loca"> <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> </div>
                      <div class="like-comm">
                        <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['counts']; ?></div>
                        <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>
                      </div>
                    </div>
                  </div>
                </div>
                <?php
  # code...
}
   
   ?><?php */?>
                <!--  <div class="col-md-6">

                  <div class="popu-con-sub">

                    <div class="popu-img">

                      <img src="<?php echo base_url();?>assets/img/popu-4.jpg">

                      <div class="popu-ovrly">

                        <a href="#"><h3>BALENZO</h3></a>

                      </div>

                    </div>

                    <div class="popu-txt">

                      <div class="popu-loca">

                        <i class="fa fa-map-marker"></i><span>Rouen, France</span>

                      </div>

                      <div class="like-comm">

                         <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i>500</div>

                         <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i>no comment</div>

                      </div>

                    </div>

                  </div>

                </div> --> 
                
              </div>
              
              <!--  end loop here  --> 
              
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="test-slider">
            <div class="row">
              <div class="interstng-sec-head">
                <div class="col-md-12">
                  <h2>Testimonial</h2>
                </div>
              </div>
            </div>
            <div id="owl-demo" class="owl-demo owl-carousel owl-theme">
              <?php
                  if(!empty($testimonials))
                    foreach($testimonials as $testi){
                  ?>
              <div class="item">
                <div class="test-sub">
                  <h4><?php echo strtoupper($testi['tst_by']); ?></h4>
                  <h5><?php echo $testi['tst_designation']; ?></h5>
                  <p><?php echo $testi['tst_content']; ?></p>
                </div>
              </div>
              <?php
                    }
                  ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="info-parralax-2">
    <div class="container">
      <div class="col-md-5">
        <div class="info-parralax-txt">
          <h3><?php echo $section9['section']; ?></h3>
          <?php
            echo $section9['content'];
          ?>
        </div>
      </div>
    </div>
  </section>
</div>

    <style>

    </style>


    <script>
    $(document).ready(function() {

      var owl = $("#owl-demo-pro");

      owl.owlCarousel({
		autoPlay : 3000,
        stopOnHover : true,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0;
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });
    </script>
       <script>
    $(document).ready(function() {

      var owl = $("#owl-demo-down");

      owl.owlCarousel({
		autoPlay : 3000,
        stopOnHover : true,
      items : 2, //10 items above 1000px browser width
      itemsDesktop : [1000,2], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0;
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });
    </script>