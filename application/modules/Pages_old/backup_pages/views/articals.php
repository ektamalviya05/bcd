<div class="main-content about-us">
	<section class="main-search-sec">
		<div class="container">
			<div class="header-srch">
				<div class="row">
					<div class="col-md-12">
						<div class="main-srch-heading">
							<h1><?php echo $title; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<section class="about-sec">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-content">
					<?php
						echo $details;
					?>
				</div>
			</div>
		</div>
	</div>
</section>