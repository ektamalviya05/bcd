<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{

   public function __construct() {
        $this->userTbl = 'users';
        $this->userTbl1 = 'otd_page_contents';
        $this->userTblSubs = 'subscribers';
        $this->testimonial = "otd_testimonials";
        $this->contentTbl = "site_contents";
        $this->catTbl = "otd_business_category";
        $this->bannerTbl = "otd_home_banner";
        $this->userCatTbl = "otd_user_business_category";
        $this->userHomeCatTbl = "otd_home_cat_tab";
        $this->addTbl = "otd_advertisement";
        $this->pageHeadTbl = "otd_page_diff_lang_content";
        $this->ftTbl = "otd_footer_content";
        $this->pytTbl = "otd_user_option_payment";
        $this->usOptTbl = "otd_user_option";
    }



 public function getHeadOfListProfiles($params = array())
    {

        $lat="48.8566140";
        $lng = "2.3522220";
        //$start = 0;
        //$limit = 1;
        
$dist_range =  -1;

 if(array_key_exists("cur_latlng",$params))
        {
                    
                   $lat= $params['cur_latlng'][0]; 
                  $lng = $params['cur_latlng'][1];
    }
     


        $dist = "round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
   * sin( radians(user_lat)))), 2) AS distance";
  
        $dist2 = "(round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
   * sin( radians(user_lat)))), 2)) ";
   

$sql_in = "SELECT opt_user_id FROM `otd_user_option` uo left join `otd_option_master` om on uo.opt_option_id = om.opt_id  where uo.opt_option_status =1 and DATE(opt_option_end_date) > DATE(NOW())";
        $sql_main = "select *, ".$dist."  from vv_users_list where user_type = 'Professional' and user_id in (".$sql_in.") ";
        
        if(array_key_exists("user_company_address", $params))
        {
 $sql_main = "select *, ".$dist." from vv_users_list where user_type = 'Professional' and user_company_address like '%".$params['user_company_address'] ."%'" ;

        }

        $sql="";
        if(array_key_exists("conditions",$params))
        {
            $sql =" and (";
            foreach ($params['conditions'] as $key => $value) 
            {
                
                if($key == 'Categories' || $key == 'categories')
                {
                $sql .= $key. " like '%". $value."%' Or " ;    
                }
                else
                {
                    $sql .= $key. " like '%". $value."%' Or " ;
                }
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
        }

        if(array_key_exists("extraFilter",$params))
        {
             if($sql != "")
               {
           
             $sql .=" Or ( ";
           
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
            }
            else
            {
            $sql =" and (";
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";


            }


        }
      

         if(array_key_exists("keyword",$params))
         {
            if($params['keyword'] != "")
            {
               if($sql != "")
               {
                 $sql .=" Or ( ";
                 $fields = $this->db->list_fields('vv_users_list');
                    foreach ($fields as $field)
                    {
                        $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                    }
                    $sql = rtrim($sql,"Or ");
                    $sql .= ") ";

                }
                else
                {
                $sql =" And (";
                $fields = $this->db->list_fields('vv_users_list');
                foreach ($fields as $field)
                    {
                      $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                     }
                $sql = rtrim($sql,"Or ");
                $sql .= ") ";
                }
            }
        }

      

    $ordby = " order by  rand() limit 10";
     
  
        
            $fullQuery = $sql_main. $sql. $ordby;

            $query = $this->db->query($fullQuery);
            
            //print_r($params);

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
//print_r($result);
//echo $this->db->last_query();
//echo "<br><br>SQL: ".$fullQuery;
//die();
        return $result;
    }

     /*
    * get rows from the page table
    */
   public function getSearchRows($params = array())
    {

        $lat="48.8566140";
        $lng = "2.3522220";
        //$start = 0;
        //$limit = 1;
        
$dist_range =  is_null($params['distance'])?5: $params['distance']; 
 if(array_key_exists("cur_latlng",$params))
        {
                    
                   $lat= $params['cur_latlng'][0]; 
                  $lng = $params['cur_latlng'][1];
    }
     


        $dist = "round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
   * sin( radians(user_lat)))), 2) AS distance";
  
        $dist2 = "(round(( 3959 * acos( cos( radians(".$lat. ") ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(".$lng. ")) + sin(radians(".$lat.")) 
   * sin( radians(user_lat)))), 2)) ";
   

        $sql_main = "select *, ".$dist."  from vv_users_list where user_type = 'Professional' ";
        
        if(array_key_exists("user_company_address", $params))
        {
 $sql_main = "select *, ".$dist." from vv_users_list where user_type = 'Professional' and user_company_address like '%".$params['user_company_address'] ."%'" ;

        }

        $sql="";
        if(array_key_exists("conditions",$params))
        {
            $sql =" and (";
            foreach ($params['conditions'] as $key => $value) 
            {
                
                if($key == 'Categories' || $key == 'categories')
                {
                $sql .= $key. " like '%". $value."%' Or " ;    
                }
                else
                {
                    $sql .= $key. " like '%". $value."%' Or " ;
                }
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
        }

        if(array_key_exists("extraFilter",$params))
        {
             if($sql != "")
               {
           
             $sql .=" Or ( ";
           
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
            }
            else
            {
            $sql =" and (";
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filters like '%". $value."%' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";


            }


        }
      

         if(array_key_exists("keyword",$params))
         {
            if($params['keyword'] != "")
            {
               if($sql != "")
               {
                 $sql .=" Or ( ";
                 $fields = $this->db->list_fields('vv_users_list');
                    foreach ($fields as $field)
                    {
                        $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                    }
                    $sql = rtrim($sql,"Or ");
                    $sql .= ") ";

                }
                else
                {
                $sql =" And (";
                $fields = $this->db->list_fields('vv_users_list');
                foreach ($fields as $field)
                    {
                      $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                     }
                $sql = rtrim($sql,"Or ");
                $sql .= ") ";
                }
            }
        }

         if(array_key_exists("start",$params) && array_key_exists("limit",$params))
         {
            $start = $params['start'];
            $limit = $params['limit'];

    $ordby = " and ".$dist2. " < ".$dist_range." order by distance ASC limit ". $limit. " offset ". $start;
           
         }
         else
         {
                $ordby = " and ".$dist2. " < ".$dist_range." order by distance ASC ";
           
            //echo  "yahan";print_r($params);
         }
  
        
            $fullQuery = $sql_main. $sql. $ordby;

            $query = $this->db->query($fullQuery);
            
            //print_r($params);

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
//print_r($result);
//echo $this->db->last_query();
//echo "<br><br>SQL: ".$fullQuery;
//die();
        return $result;
    }

    /*
    * get rows from the page table
    */
    function getPageRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userTbl1);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
              
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }


function getExtraFilters(){

$this->db->select('*');
$this->db->from('otd_business_category');
$this->db->where("cat_status", 1);
$this->db->where("type !=", 1);
$this->db->order_by("cat_name", "ASC");
$query = $this->db->get();
return $query;
}


    /*
    * Get testimonials
    */
    function getTestimonials(){
        $query = $this->db->order_by("tst_date", "DESC")->get_where($this->testimonial, array("tst_status"=>1));
        return $query->result_array();
    }

    /**
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
       $address = str_replace(" ", "+", $address);
       $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
       $json = json_decode($json);
       if((isset($json)) && ($json->{'status'} == "OK")){
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
       }else{
         $lat = -25.274398; 
         $long = 133.775136; 
       }       
       $response = array('lat'=>$lat,'lng'=>$long);
       return $response;
    }

function getFollowersCount($user_id)
{
    $sql = "select followed_user_id, count(*) as total_followers from otd_business_follower where followed_user_id = ".$user_id. " group by followed_user_id";
           $query = $this->db->query($sql);

           return $query->num_rows();
}
function getBusinessCategories(){

$this->db->select('*');
$this->db->from('otd_business_category');
$this->db->where("cat_status", 1);
$this->db->order_by("cat_name", "ASC");
$query = $this->db->get();

//        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        return $query;
    }

     function getTopRatedCompanies()
    {
 
//  $sql = "SELECT * FROM `users` where toprated = 1 and status = 1 and user_type = 'Professional' ORDER BY `created` DESC limit 2";

 /*$sql = "SELECT u.*, c.counts FROM `users`u left join vvfollower_counts c on 
 c.followed_user_id = u.user_id 
 where u.toprated = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 3";*/     

 $sql = "SELECT u.* FROM `vv_users_list`u  where u.toprated = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 3";

       $query = $this->db->query($sql);

return $query->result_array();


    }

     function getPublishOnHomeCompanies()
    {

  //$sql = "SELECT * FROM `users` where publishonhome = 1 and status = 1 and user_type = 'Professional' ORDER BY `created` DESC limit 4";
    

    /*$sql = "SELECT u.*, c.counts FROM `users`u left join vvfollower_counts c on c.followed_user_id = u.user_id where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 7";   */

 $sql = "SELECT u.* FROM `vv_users_list`u  where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 7";


       $query = $this->db->query($sql);

return $query->result_array();


    }


    function getArticles($args){
        $query = $this->db->get_where($this->contentTbl, array("content_id"=>$args));
        return $query->row_array();
    }

    /*
    * get categories lists
    */   
    function getCategoryRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->catTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get Banner Images
    */
    function getBannerImages($params = array()){
        $this->db->select('*');
        $this->db->from($this->bannerTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("bn_id",$params)){
            $this->db->where('bn_id',$params['content_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get categories lists
    */   
    function getCategoryUsersRelationRows($params = array()){

        $this->db->select("*, $this->userTbl.user_id AS mainuserid");
        $this->db->join($this->userCatTbl, "$this->userCatTbl.user_id = $this->userTbl.user_id");
        $this->db->from($this->userTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("groupby",$params)){
            $this->db->group_by($params['groupby']);           
        }
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

     /*
    * get rows for home category tabs list
    */
    function getHomeCatgoryTabRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userHomeCatTbl);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get categories lists with corressponding tabs
    */   
    function getCategoryFromTab($params = array()){

        $this->db->select('*');
        $this->db->from($this->catTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        
        if(array_key_exists("tab_id",$params)){
            $this->db->where('tab_id',$params['tab_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }



    function getDistanceLimit()
    {
$this->db->select("*");
$this->db->from("otd_options");

       $this->db->where("option_name","distance");
        $query = $this->db->get();
            
        $result = $query->row_array();

        return $result;
    }


    /*
    * get rows for google advertisement script
    */
    function getGoogleAdd(){
        $query = $this->db->get_where($this->contentTbl, array("content_id"=>26));
        return $query->row_array();
    }

    /*
    * Get rows from manual advertisement table
    */
    function getManualAddRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->addTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        
        if(array_key_exists("add_id",$params)){
            $this->db->where('add_id',$params['add_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get rows from the page heading content table
    */
    function getHeadContentRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->pageHeadTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
              
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("ct_id",$params)){
            $this->db->where('ct_id',$params['ct_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get heading for content
    */
    public function getHeadings($params= array(), $section="", $get =""){
        foreach($params as $data){
            if(in_array($section, $data))
                return $data[$get];
        }
    }

    /*
    * get rows from text content table
    */
    function getTextContent($args){
        if(!empty($args)){
            return $this->db->get_where($this->ftTbl, array("ft_id"=>$args))->row_array();
        }else{
            return false;
        }
    }

    function updateTransaction($params = array(), $where =array() ){
        $this->db->update($this->pytTbl, $params, $where);
        return true;
    }

    function getUserRowsFromOption($args = ""){
        return $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id")
                        ->get_where($this->usOptTbl, array("opt_user_option_id"=>$args))->row_array();
    }
}
?>