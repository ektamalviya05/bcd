<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){	 

	 	parent:: __construct();
	 	$this->load->model("Page");

$this->load->library("pagination");
		
	 }
	 	
	public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array();
        $this->template->set('title', 'Home');
        $lang = $this->session->userdata("lang");
        if(empty($lang))
            $lang = "English";
        $data['business_categories'] = $this->Page->getBusinessCategories();
        // $con['conditions'] = array("tst_status"=>1);
        $data['testimonials'] = $this->Page->getTestimonials();
        $data['section2'] = $this->Page->getArticles(24);
        $data['section31'] = $this->Page->getArticles(21);
        $data['section32'] = $this->Page->getArticles(22);
        $data['section33'] = $this->Page->getArticles(23);
        $data['section5'] = $this->Page->getArticles(14);
        $data['section61'] = $this->Page->getArticles(15);
        $data['section62'] = $this->Page->getArticles(16);
        $data['section63'] = $this->Page->getArticles(17);
        $data['section64'] = $this->Page->getArticles(18);
        $data['section65'] = $this->Page->getArticles(19);
        $data['section66'] = $this->Page->getArticles(20);
        $data['section7'] = $this->Page->getArticles(12);
        $data['section9'] = $this->Page->getArticles(13);
        $data['section6Bk'] = $this->Page->getArticles(25);
        $data['homecategorytab'] = $this->Page->getHomeCatgoryTabRows();        
        // $data['headings'] = $this->Page->getHeadContentRows(array("conditions"=>array("ct_page_title"=>"Home", "ct_language"=>$lang)));
        $data['headings'] = $this->Page->getTextContent(9);        
        $ban['conditions'] = array("bn_status"=>1);
        $ban['sorting']  = array("bn_id"=>"RANDOM");
        $ban['limit'] = 3;
        $data['bn_images'] = $this->Page->getBannerImages($ban);
        $data['publishonhome'] = $this->Page->getPublishOnHomeCompanies();
        $data['toprated'] = $this->Page->getTopRatedCompanies();
        $data['featuredCategories'] = $this->Page->getCategoryRows(array("conditions"=>array("cat_status"=>1, "cat_homedisplay"=>1)));        
      
        $this->template->load('home_layout', 'contents' , 'home', $data);
    }

    function get_client_ip(){
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


public function businessListing()
{

    $data = array();
    

    $dist = $this->Page->getDistanceLimit();

     
    $dis_range = $dist['option_value'] > 0 ?$dist['option_value']:5;
    
    if($this->input->post("searchSubmit"))
    {
		 
        $location = $this->input->post("user_address");
		 
        $business = $this->input->post("business_category1");
        $keyword = $this->input->post("keyword");
        $extra_fltr = $this->input->post("extra_fltr");
        $searchedcity = $this->input->post("searchedcity");
        

        $cur_latlng = explode(",", $this->get_lat_long($location));
		
        if(sizeof($cur_latlng) > 0 and $cur_latlng[0] !='' and $cur_latlng[1] !='')
            {
                $con['cur_latlng'] = $cur_latlng;
            }

        $data['location'] = $location;
        $data['category'] = $business;
        $data['keyword'] = $keyword;
        $data['extraFilter'] = $extra_fltr;
        $data["searchedcity"] = $searchedcity;

if($searchedcity != "")
{
    $con['searchedcity'] = $searchedcity;

}
        if($location != "")
        {
            $con['user_company_address'] = $location;
        }
        if(sizeof($extra_fltr) > 0)
        {
            $con['extraFilter'] = $extra_fltr;
        }
        if($business != "")
        {
            $con['conditions']['categories'] = $business;
        }

        $con['distance'] = $dis_range;
        $con['keyword'] = $keyword;
       
              
    }
    else
    {
       
        $locationlatlng = $this->session->userdata("currentlatlng");
        $cur_latlng = $locationlatlng;

        if($locationlatlng == "")
        {
            $location = "France";
            $cur_latlng = explode(",", $this->get_lat_long($location));
        }
       
        

        if(sizeof($cur_latlng) > 0 and $cur_latlng[0] !='' and $cur_latlng[1] !='')
        {
			 
            $con['cur_latlng'] = $cur_latlng;
        }
        $data["searchedcity"] = "";
        $data['location'] = "";
        $data['category'] = "";
        $data['keyword'] = "";
        $data['extraFilter'] = "";
 
        $con['keyword'] = "";

        if(isset($_GET['category']))
        {
            $cat = urldecode($_GET['category']);   
            $con['conditions']['categories'] = $cat;
            $data['category'] = $cat;
        }
        if($location != "")
        {
            $con['user_company_address'] = $location;
        }
        else {
            $con['user_company_address'] = "France";
        }
           
        }
     
      /*Pagination code*/

        $con['returnType']="count";
        $totalrecord = $result = $this->Page->getSearchRows($con); 
        $data['record_found'] = $totalrecord;
        $totalrows = $totalrecord;

   

        $config['base_url'] = base_url()."Pages/businessListing/";
        $config['total_rows'] = $totalrows;
        $config['per_page'] = 20;
        $config['uri_segment'] = 3;
        $config['num_links'] = $totalrows / 1;
        $config['cur_tag_open'] = '&nbsp;<a class="current">';
        $config['cur_tag_close'] = '</a>';
        $config['next_link'] = '<b> Next </b>';
        $config['prev_link'] = '<b> Previous </b>';
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = round($choice);
        $this->pagination->initialize($config);
        $page = ($this->uri->segment(3));
        $con['limit'] = $config['per_page'];
        $start =($page-1) * $config['per_page'];

        if($start == 0)
        {
          $start = 0;
        }
        else
        {
          $start = ($start/$config['per_page'] )+1;
        }
        $con['start'] = $start;
        $data['userlist'] = array();

  


    if($totalrows > 0){

        $con['returnType']="";

       $data['ALaUNE']  = $this->Page->getALaUNEProfiles($con);
        $data['headprofile']  = "";



        $result = $this->Page->getSearchRows($con);
        $data['search_results'] = $result;
        $data['businessMarker'] =  $result;
        $data["record_found"] = $totalrows; 
    }
    else
    {
      $data["record_found"] = 0; 
      $data['search_results'] = null;
      $data['businessMarker'] =  null;
    }

    $data["links"] = $this->pagination->create_links();
    $data['page'] = $page;


        /*End Pagination Code*/

        $this->load->helper("cookie");
        $data['business_categories'] = $this->Page->getBusinessCategories();
        $data['busienss_filters'] = $this->Page->getExtraFilters();
        $data['googleadd'] = $this->Page->getGoogleAdd();
        $data['manualadd'] = $this->Page->getManualAddRows(array("sorting"=>array("add_display"=>"ASC"), "conditions"=>array("add_status"=>1)));
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Find NearBy Attraction');
        $this->template->load('home_layout', 'contents' , 'searchResult', $data);
}


// function to get  the address
function get_lat_long($address){
$region = "";
$latlong="";
    $address = str_replace(" ", "+", $address);
    if($address == "")
    {
        $address = "France";
    }
    if($address != "")
    {
    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);
    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    $latlong = $lat.','.$long;
    //echo $address."<br><br>".$latlong;
    //die();
    }
    else
    {
        $lat="48.8566140";
        $long = "2.3522220";
$latlong = $lat.','.$long;
    }
    return $latlong;
}



	public function searchResult(){
		$location = $this->input->post("user_address");
		$business = $this->input->post("user_categories");
		$keyword = $this->input->post("keyword");
		$this->load->helper("cookie");
		if(!empty($location)){
			set_cookie(array('name' => 'location',
					        'value' => $location,
					        'expire' => 3600
					        ));
		}
		if(!empty($business)){
			set_cookie(array('name' => 'business',
					        'value' => $business,
					        'expire' => 3600
					        ));
		}
		if(!empty($keyword)){
			set_cookie(array('name' => 'kayword',
					        'value' => $keyword,
					        'expire' => 3600
					        ));
		}


		redirect(base_url("Pages/businessListing"));		
	}

	public function content($args = NULL){
		header("Access-Control-Allow-Origin: *");
        $data = array();
        $con['conditions'] = array("page_url"=>$args);
        $data['details'] = $this->Page->getPageRows($con);
        $con['conditions'] = array("pg_cat"=>$data['details'][0]['pg_cat'], "pg_status"=>1);
        $con["sorting"] = array("pg_display_number" => "ASC");
        $data['similar_pages'] = $this->Page->getPageRows($con);
        $this->template->set('title', $data['details'][0]['pg_title']);
        $this->template->load('home_layout', 'contents' , 'pages', $data);
	}
	
	function readMore($args = NULL){		
		$data = array();
        if($args == "personal"){        	    	
        	$query = $this->db->get_where("site_contents", array("content_id"=>4));                        
            foreach($query->result() as $cnt){}
            $data['details'] = $cnt->content;
        	$this->template->set('title', 'Personal Account');
        	$data['title'] = "Personal Account";
        }
        else if($args == "professional"){
        	$query = $this->db->get_where("site_contents", array("content_id"=>1));
            foreach($query->result() as $cnt){}
            $data['details'] = $cnt->content;
        	$this->template->set('title', 'Professional Account');
        	$data['title'] = "Professional Account";
        }
        header("Access-Control-Allow-Origin: *");
        $this->template->load('home_layout', 'contents' , 'articals', $data);
    }


    public function getCategories($catType)
    {

      
    }


function getAddress($latitude,$longitude){
    if(!empty($latitude) && !empty($longitude)){
        //Send request and receive json data by address
        $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false'); 
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        //Get address from json data
        $address = ($status=="OK")?$output->results[1]->formatted_address:'';
        //Return address of the given latitude and longitude
        if(!empty($address)){
            return $address;
        }else{
            return false;
        }
    }else{
        return false;   
    }
}
    
    function goodPlace(){
        if(!empty($this->input->post())){
            $locat = $this->input->post("location");
            $keyword = $this->input->post("keyword");
            $eventtype = $this->input->post("brand_name");
            $this->session->set_userdata("eventtype", $eventtype);
            $this->session->set_userdata("locat", $locat);
            $this->session->set_userdata("keyword", $keyword);
            redirect(base_url("Pages/goodPlace"));
        }
        $data = array();
        $data['listing'] = $this->Page->getEventsRows(array("conditions"=>array("event_status"=>1, "event_goodplace"=>1, "event_publish_start <="=>date("Y-m-d"), "event_publish_end >="=> date("Y-m-d"), "opt_option_status"=>1), "sorting"=>array("event_publish_start"=>"DESC")));
        $data['evttype'] = $this->Page->getEventTypeList();
        // $data['eventmarkers'] = $data['evttype'];
        // echo "<pre>";
        // print_r($data['evttype']);
        // exit;
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Find NearBy Attraction');
        $this->template->load('home_layout', 'contents' , 'goodPlace', $data);
    }

    function clearGoodPlaceFilter(){
        $this->session->unset_userdata("eventtype");
        $this->session->unset_userdata("locat");
        $this->session->unset_userdata("keyword");
        redirect(base_url("Pages/goodPlace"));
    }


/*
* external function for user module
*/

    public function option_package()
    {
        $data = array();
        $this->template->load('home_layout', 'contents' , 'option_package', $data);
    }

    function paymentIpn(){  
        $this->load->library("email");
        $paypalInfo = $this->input->post();        
        //insert the transaction data into the database

        $request_id = $paypalInfo['custom'];
        $arr = array("pyt_txn_id" => $paypalInfo['txn_id'],
                     "pyt_amount" => $paypalInfo['mc_gross'],
                     "pyt_fee" => $paypalInfo['mc_fee'],
                     "pyt_email" =>$paypalInfo['payer_email'],
                     "pyt_date" =>date("Y-m-d H:i:s", strtotime($paypalInfo['payment_date'])),
                     "pyt_txn_status" =>$paypalInfo['payment_status']
                    );

        // $request_id = 15;
        // $arr = array("pyt_txn_id" => "fsadfsdfsadfsadfsfadf",
        //              "pyt_amount" => 456465,
        //              "pyt_fee" => 2.03,
        //              "pyt_email" =>"tesst@test.omc",
        //              "pyt_date" =>date("Y-m-d H:i:s", strtotime("now")),
        //              "pyt_txn_status" =>"Completed"
        //             );
        $check = $this->Page->updateTransaction($arr, array("pyt_id"=>$request_id));
        

        // $data['head_list']="Purchased";

        //send email here
        // $email = $this->session->userData["user_email"];

        
        $userdetails = $this->Page->getUserRowsFromOption($request_id);        
        $email = $userdetails['user_email'];
        $optiondetails = $this->Page->getOptionDetails($request_id);
        if(!empty($optiondetails)){
            foreach($optiondetails as $txnopt){
                $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                $opt_option_price = $txnopt['opt_option_price'];
                $opt_option_id = $txnopt['opt_option_id'];
                $checkduration = $txnopt['otp_option_duration'];
                // if($opt_option_id == 7){
                //     $this->Page->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                //     $eventDetails = $this->Page->getEventsRows(array("event_id"=>$txnopt['opt_event_id']));
                //     $msginner = "<p>Hello,</p>
                //                 <p>One event is organized by ".ucwords($eventDetails['user_firstname']." ".$eventDetails['user_lastname']).", Event details are given below</p>
                //                 <p><b>Event Name: </b>".$eventDetails['event_name']."</p>
                //                 <p><b>Type: </b>".$eventDetails['evt_type']."</p>
                //                 <p><b>Start Date: </b>".$eventDetails['event_start_date']."</p>
                //                 <p><b>End Date: </b>".$eventDetails['event_end_date']."</p>                                        
                //                 <p><b>People Strength: </b>".$eventDetails['event_per_ltd']."</p>
                //                 <p><b>Price: </b>$".$eventDetails['event_price']."</p>
                //                 <br/>
                //                 <p>Please <a href=".base_url("Users/eventsView/").$eventDetails['event_id'].">click here</a> to view event.</p>
                //                 <br/>
                //                 <p>Thank You</p>";
                //     $subject = "Event Alert";
                //     $msg = $this->sendMailFormat($subject, $msginner);
                //     $this->Page->updateEvents(array("event_status"=>1), $txnopt['opt_user_id'], $txnopt['opt_event_id'], $msg);
                // }
                if($txnopt['otp_option_duration'] == "1 Day" ){
                    if($opt_option_id != 7 && $opt_option_id != 8){
                        $this->Page->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                    }
                    // if($opt_option_id == 8){
                    //     $eventDetails = $this->Page->getEventsRows(array("event_id"=>$txnopt['opt_event_id']));
                    //     $msginner = "<p>Hello,</p>
                    //                 <p>One event is organized by ".ucwords($eventDetails['user_firstname']." ".$eventDetails['user_lastname']).", Event details are given below</p>
                    //                 <p><b>Event Name: </b>".$eventDetails['event_name']."</p>
                    //                 <p><b>Type: </b>".$eventDetails['evt_type']."</p>
                    //                 <p><b>Start Date: </b>".$eventDetails['event_start_date']."</p>
                    //                 <p><b>End Date: </b>".$eventDetails['event_end_date']."</p>                                        
                    //                 <p><b>People Strength: </b>".$eventDetails['event_per_ltd']."</p>
                    //                 <p><b>Price: </b>$".$eventDetails['event_price']."</p>
                    //                 <br/>
                    //                 <p>Please <a href=".base_url("Users/eventsView/").$eventDetails['event_id'].">click here</a> to view event.</p>
                    //                 <br/>
                    //                 <p>Thank You</p>";
                    //     $subject = "Event Alert";
                    //     $msg = $this->sendMailFormat($subject, $msginner);
                    //     $this->Page->updateEvents(array("event_status"=>1), $txnopt['opt_user_id'], $txnopt['opt_event_id'], $msg);
                    // }
                    $date1 = date_create($txnopt['opt_option_active_date']);
                    $date2 = date_create($txnopt['opt_option_end_date']);        
                    $difference = date_diff($date1,$date2);
                    $diff = $difference->format('%a');
                    if($diff > 0)
                        $txnopt['otp_option_duration'] = $diff." Days start from ".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'];
                    else
                        $txnopt['otp_option_duration'] = "1 Day start from ".$txnopt['opt_option_active_date'];
                }
                if($txnopt['opt_option_id'] == 6){
                    $otp_option_duration = $txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];
                }else{
                    $otp_option_duration = $txnopt['otp_option_duration'];
                }
                $mstopt = $this->Page->getMasterOption($txnopt['opt_option_id']);
                 
                // $subject = "Head of List Option Purchased";
                $subject = $mstopt['opt_name']." Option Purchased";

                $optiondetails = "Option Perchased: ".$mstopt['opt_name']."<br>Purchase Date: $opt_option_purchase_date<br>Duration: $otp_option_duration<br>Option Price: $opt_option_price<br>";
                if($checkduration == "1 Day"){
                    $optiondetails .= "Please <a href='".base_url()."/Users/ActivatePlan/".$opt_option_id."'>Activate Your Option Plane (Click here)</a><br><br>";
                }else{
                    $optiondetails .= "Your option is activated, Please check your mes option for further details.";
                }

                $msginner = "Thanks for Adding ".$mstopt['opt_name']." option to Your Business Profile. We will process your request and infom you soon on your registered email ID. <br><br>Details are: ". $optiondetails;                
                $msg = $this->sendMailFormat($subject, $msginner);
                $fromemail = "votive.pradeep01@gmail.com";
                $fromname = "Support Team";

                $x= $this->send_mail($email, $subject, $msg);

                  //Admin
                $emailadmin = "votive.pradeep01@gmail.com";

                $optiondetails = "Option Perchased: ".$mstopt['opt_name']."<br>Purchase Date: $opt_option_purchase_date<br>Duration: $otp_option_duration<br>Option Price: $opt_option_price<br>Please <a href='".base_url()."/Users/ActivatePlan/".$opt_option_id."'>View Details</a><br><br>";

                $msginner = "Thanks for Adding ".$mstopt['opt_name']." option to Your Business Profile. We will process your request and infom you soon on your registered email ID. <br><br>Details are: ". $optiondetails;
                
                $msgadmin = $this->sendMailFormat($subject, $msginner);
                $fromemail = "votive.pradeep01@gmail.com";
                $fromname = "Support Team";

                $x= $this->send_mail($emailadmin, $subject, $msgadmin);
            }
        }
    }

    public function sendMailFormat($subject, $msginner)
    {        
        $format = '<html><head><meta charset="utf-8"><title>'.$subject.'</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"></head>
                    <body style="font-family: open Sans;font-size: 13px; line-height:20px;"><div style="padding: 0 10px;"><div style="max-width:550px;width:85%;padding:30px;margin:30px auto;border:1px solid #e2e2e2; overflow:hidden; background-position:center;background-size:cover;text-align: center;color: #fff;"><div style="padding:30px;border:2px solid #fff;background:rgba(0, 0, 0, 0.63);">
                    <div style="text-align:center">';        
        $format .='<a href="http://votivephp.in/VotiveYellowPages/" style="display:block;overflow:hidden;padding: 0 0 0 0;"><img style="max-width:270px;width: 100%;" src="http://votivephp.in/VotiveYellowPages/assets/img/logo.png"></a></div><div style="border-top: 1px dotted #fff; display: block; margin-top:5px; margin-bottom:5px;"></div><div class="mailbody" style="min-height: 250px;"><h3>'.$subject.'</h3><div>'.$msginner.'</div></div>';
        $format .= '<div><div style="border-top: 1px dotted #fff; display: block;margin-top:5px; margin-bottom:5px;"></div><p style="margin:0 0 8px 0">Regards,</p><p style="margin:0 0 8px 0">Sales Team</p><p style="margin:0 0 8px 0"><a href="http://votivephp.in/VotiveYellowPages/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">http://votivephp.in/VotiveYellowPages/</a></p></div></div></div></div></body></html>';
        return $format;
    }

    function send_mail($email, $subject="Test Mail", $msg="Testing mail"){
        $from = "votive.pradeep01@gmail.com";
        $this->load->library('email');
        $config['protocol']    = 'smtp';
        $config['smtp_host']    = 'ssl://smtp.gmail.com';
        $config['smtp_port']    = '465';
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $from;
        $config['smtp_pass']    = 'votive123456';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; 
        $config['validation'] = TRUE;      
        $this->email->initialize($config);
        $fromname = "Sales Team";
        $fromemail ="votive.pradeep01@gmail.com";
        $mpass = "votive123456";
        $to = "Lloyd@otourdemoi.fr, votive.pradeep01@gmail.com, ". $email;
        $from = "votive.pradeep01@gmail.com";
        $subject=$subject;
        $message=$msg;
        $cc=false;


        $this->email->from($fromemail, $fromname);
        $this->email->to($to);
        $this->email->subject($subject);       
        $this->email->message($msg);  
        if($cc){
        $this->email->cc($cc);
        }

        /*      if(!empty($bcc)){
        $this->email->bcc($bcc);
        }
        */

        if(!$this->email->send()){
        return $this->email->print_debugger();
        }else{
        return true;
        }
    }

     public function sendcontactmail(){
        extract($_POST);
		$subject = "Contact";
                    $msginner = "<table><tr><th>Name</th><td>'".$name."'</td></tr><tr><th>Email</th><td>'".$email."'</td></tr><tr><th>Email</th><td>'".$contact."'</td></tr><tr><th>Email</th><td>'".$message."'</td></tr></table>";
					$msg = $this->sendMailFormat($subject, $msginner, 13);
					
                    $fromemail = SENDER_EMAIL;
                    $fromname = SENDER_NAME;

                     $x = $this->send_mail($email, $subject, $msg);
					 $data['success_msg'] = 'Message send successfully.';
					 $data['status'] = 200;
		             echo json_encode($data);
    }
	
/*
* external function for user modules end
*/

}


/*

SELECT 
    ( 3959 * acos( cos( radians(42.290763) ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(-71.35368)) + sin(radians(42.290763)) 
   * sin( radians(user_lat)))) AS distance 
FROM vv_users_list 

ORDER BY distance
*/

