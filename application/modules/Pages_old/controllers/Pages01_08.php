<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){	 

	 	parent:: __construct();
	 	$this->load->model("Page");
		
	 }
	 	
	public function index()
    {
        header("Access-Control-Allow-Origin: *");
        $data = array();
        $this->template->set('title', 'Home');
        $data['business_categories'] = $this->Page->getBusinessCategories();
        // $con['conditions'] = array("tst_status"=>1);
        $data['testimonials'] = $this->Page->getTestimonials();
        $data['section2'] = $this->Page->getArticles(24);
        $data['section31'] = $this->Page->getArticles(21);
        $data['section32'] = $this->Page->getArticles(22);
        $data['section33'] = $this->Page->getArticles(23);
        $data['section5'] = $this->Page->getArticles(14);
        $data['section61'] = $this->Page->getArticles(15);
        $data['section62'] = $this->Page->getArticles(16);
        $data['section63'] = $this->Page->getArticles(17);
        $data['section64'] = $this->Page->getArticles(18);
        $data['section65'] = $this->Page->getArticles(19);
        $data['section66'] = $this->Page->getArticles(20);
        $data['section7'] = $this->Page->getArticles(12);
        $data['section9'] = $this->Page->getArticles(13);
        $data['section6Bk'] = $this->Page->getArticles(25);
        $data['homecategorytab'] = $this->Page->getHomeCatgoryTabRows();        
        $ban['conditions'] = array("bn_status"=>1);
        $ban['sorting']  = array("bn_id"=>"RANDOM");
        $ban['limit'] = 3;
        $data['bn_images'] = $this->Page->getBannerImages($ban);       
        $data['publishonhome'] = $this->Page->getPublishOnHomeCompanies();
        $data['toprated'] = $this->Page->getTopRatedCompanies();
        $data['featuredCategories'] = $this->Page->getCategoryRows(array("conditions"=>array("cat_status"=>1, "cat_homedisplay"=>1)));        
        // $data['tabCategories'] = $this->Page->getCategoryRows(array("conditions"=>array("cat_status"=>1, "cat_hometabdisplay"=>1), "sorting"=>array("cat_id"=>"RANDOM"), "limit"=>6 ));
       
        $this->template->load('home_layout', 'contents' , 'home', $data);
    }

function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }


public function businessListing(){

$data = array();

if($this->input->post("searchSubmit"))
{
        $location = $this->input->post("user_address");
        $business = $this->input->post("business_category1");
        $keyword = $this->input->post("keyword");
        $extra_fltr = $this->input->post("extra_fltr");



        $cur_latlng = explode(",", $this->get_lat_long($location));

if(sizeof($cur_latlng) > 0)
{
    $con['cur_latlng'] = $cur_latlng;
}




$data['location'] = $location;
$data['category'] = $business;
$data['keyword'] = $keyword;
$data['extraFilter'] = $extra_fltr;

if($location != "")
{
    $con['conditions']['user_company_address'] = $location;
}

if(sizeof($extra_fltr) > 0)
{
    $con['extraFilter'] = $extra_fltr;
}


if($business != "")
{
    $con['conditions']['categories'] = $business;
}

$con['keyword'] = $keyword;
$con['returnType']="";
$result = $this->Page->getSearchRows($con);
$data['search_results'] = $result;
$data['businessMarker'] =  $result;
$con['returnType']="count";
$totalrecord = $result = $this->Page->getSearchRows($con); 
$data['record_found'] = $totalrecord;

//print_r($result);
//die();
}
else
{


$locationlatlng = $this->session->userdata("currentlatlng");

  $cur_latlng = $locationlatlng;
  //explode(",", $this->get_lat_long($locationlatlng));

if(sizeof($cur_latlng) > 0)
{
    $con['cur_latlng'] = $cur_latlng;
}


$data['location'] = "";
$data['category'] = "";
$data['keyword'] = "";
$data['extraFilter'] = "";

$con['keyword'] = "";


    if(isset($_GET['category']))
    {
    $cat = urldecode($_GET['category']);   
    $con['conditions']['categories'] = $cat;
    $data['category'] = $cat;
   
    }



$result = $this->Page->getSearchRows($con);
$data['search_results'] = $result;
$data['businessMarker'] =  $result;
$con['returnType']="count";
$totalrecord = $result = $this->Page->getSearchRows($con); 
$data['record_found'] = $totalrecord;

/*print_r($data['businessMarker']);
die("Die2");*/

}
        $this->load->helper("cookie");
        header("Access-Control-Allow-Origin: *");
       
        $this->template->set('title', 'Find NearBy Attraction');
        $data['business_categories'] = $this->Page->getBusinessCategories();
        $data['busienss_filters'] = $this->Page->getExtraFilters();

        // if(!empty($location))
        // $data['latlong'] = $this->Page->get_lat_long($location);
        $this->template->load('home_layout', 'contents' , 'searchResult', $data);
    }


// function to get  the address
function get_lat_long($address){
$region = "";
$latlong="";
    $address = str_replace(" ", "+", $address);
    if($address != "")
    {
    $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=$region");
    $json = json_decode($json);
    $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
    $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
    $latlong = $lat.','.$long;
    //echo $address."<br><br>".$latlong;
    //die();
    }
    else
    {
        $lat="48.8566140";
        $long = "2.3522220";
$latlong = $lat.','.$long;
    }
    return $latlong;
}



	public function searchResult(){
		$location = $this->input->post("user_address");
		$business = $this->input->post("user_categories");
		$keyword = $this->input->post("keyword");
		$this->load->helper("cookie");
		if(!empty($location)){
			set_cookie(array('name' => 'location',
					        'value' => $location,
					        'expire' => 3600
					        ));
		}
		if(!empty($business)){
			set_cookie(array('name' => 'business',
					        'value' => $business,
					        'expire' => 3600
					        ));
		}
		if(!empty($keyword)){
			set_cookie(array('name' => 'kayword',
					        'value' => $keyword,
					        'expire' => 3600
					        ));
		}


		redirect(base_url("Pages/businessListing"));		
	}

	public function content($args = NULL){
		header("Access-Control-Allow-Origin: *");
        $data = array();
        $con['conditions'] = array("pg_meta_tag"=>$args);
        $data['details'] = $this->Page->getPageRows($con);
        $con['conditions'] = array("pg_cat"=>$data['details'][0]['pg_cat'], "pg_status"=>1);
        $data['similar_pages'] = $this->Page->getPageRows($con);
        $this->template->set('title', $data['details'][0]['pg_title']);
        $this->template->load('home_layout', 'contents' , 'pages', $data);
	}
	
	function readMore($args = NULL){		
		$data = array();
        if($args == "personal"){        	    	
        	$query = $this->db->get_where("site_contents", array("content_id"=>4));                        
            foreach($query->result() as $cnt){}
            $data['details'] = $cnt->content;
        	$this->template->set('title', 'Personal Account');
        	$data['title'] = "Personal Account";
        }
        else if($args == "professional"){
        	$query = $this->db->get_where("site_contents", array("content_id"=>1));
            foreach($query->result() as $cnt){}
            $data['details'] = $cnt->content;
        	$this->template->set('title', 'Professional Account');
        	$data['title'] = "Professional Account";
        }
        header("Access-Control-Allow-Origin: *");
        $this->template->load('home_layout', 'contents' , 'articals', $data);
    }


    public function getCategories($catType)
    {

      
    }


function getAddress($latitude,$longitude){
    if(!empty($latitude) && !empty($longitude)){
        //Send request and receive json data by address
        $geocodeFromLatLong = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($latitude).','.trim($longitude).'&sensor=false'); 
        $output = json_decode($geocodeFromLatLong);
        $status = $output->status;
        //Get address from json data
        $address = ($status=="OK")?$output->results[1]->formatted_address:'';
        //Return address of the given latitude and longitude
        if(!empty($address)){
            return $address;
        }else{
            return false;
        }
    }else{
        return false;   
    }
}



}


/*

SELECT 
    ( 3959 * acos( cos( radians(42.290763) ) * cos( radians( user_lat ) ) 
   * cos( radians(user_long) - radians(-71.35368)) + sin(radians(42.290763)) 
   * sin( radians(user_lat)))) AS distance 
FROM vv_users_list 

ORDER BY distance
*/