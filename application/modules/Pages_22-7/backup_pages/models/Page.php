<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{

   public function __construct() {
        $this->userTbl = 'users';
        $this->userTbl1 = 'otd_page_contents';
        $this->userTblSubs = 'subscribers';
        $this->testimonial = "otd_testimonials";
        $this->contentTbl = "site_contents";
        $this->bannerTbl = "otd_home_banner";
    }

     /*
    * get rows from the page table
    */
   public function getSearchRows($params = array())
    {
        $sql_main = "select * from vv_users_list";
        $sql="";
        if(array_key_exists("conditions",$params)){
            $sql =" where (";
            foreach ($params['conditions'] as $key => $value) {
            $sql .= $key. " = '". $value."' and " ;
            $sql = rtrim($sql,"and ");
            $sql .= ") ";
        }
        }
      

         if(array_key_exists("keyword",$params))
         {
            if($params['keyword'] != "")
            {
               if($sql != "")
               {
                 //$this->db->or_like('user_company_name', $params['keyword']); 
                   $sql .= " OR (user_company_name  like '%". $params['keyword']. "%')";      
               }
               else
               {
                    $sql .= "where (user_company_name  like '%". $params['keyword']. "')";
               }
            }
        }
  
       $fullQuery = $sql_main. $sql;

       /*if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                }
        }
            if(array_key_exists("start",$params) && array_key_exists("limit",$params))
            {
                $this->db->limit($params['limit'],$params['start']);
               
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }*/
          // $query = $this->db->get();

           $query = $this->db->query($fullQuery);

          if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }

    echo $this->db->last_query();
    echo "SQL: ".$fullQuery;
 //die();
        return $result;
    }

    /*
    * get rows from the page table
    */
    function getPageRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userTbl1);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
              
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get testimonials
    */
    function getTestimonials(){
        $query = $this->db->order_by("tst_date", "DESC")->get_where($this->testimonial, array("tst_status"=>1));
        return $query->result_array();
    }

    /**
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
       $address = str_replace(" ", "+", $address);
       $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
       $json = json_decode($json);
       if((isset($json)) && ($json->{'status'} == "OK")){
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
       }else{
         $lat = -25.274398; 
         $long = 133.775136; 
       }       
       $response = array('lat'=>$lat,'lng'=>$long);
       return $response;
    }

    function getBusinessCategories(){

$this->db->select('*');
$this->db->from('otd_business_category');
$this->db->where("cat_status", 1);
$this->db->order_by("cat_name", "ASC");
$query = $this->db->get();




//        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        return $query;
    }

     function getTopRatedCompanies()
    {
 
//  $sql = "SELECT * FROM `users` where toprated = 1 and status = 1 and user_type = 'Professional' ORDER BY `created` DESC limit 2";

 $sql = "SELECT u.*, c.counts FROM `users`u left join vvfollower_counts c on 
 c.followed_user_id = u.user_id 
 where u.toprated = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY `created` DESC limit 2";     
       $query = $this->db->query($sql);

return $query->result_array();


    }

     function getPublishOnHomeCompanies()
    {

  //$sql = "SELECT * FROM `users` where publishonhome = 1 and status = 1 and user_type = 'Professional' ORDER BY `created` DESC limit 4";
    

    $sql = "SELECT u.*, c.counts FROM `users`u left join vvfollower_counts c on c.followed_user_id = u.user_id where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY `created` DESC limit 4";   
       $query = $this->db->query($sql);

return $query->result_array();


    }

    function getArticles($args){
        $query = $this->db->get_where($this->contentTbl, array("content_id"=>$args));
        return $query->row_array();
    }

    function getBannerImages($params = array()){
        $this->db->select('*');
        $this->db->from($this->bannerTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("bn_id",$params)){
            $this->db->where('bn_id',$params['content_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }
}
?>