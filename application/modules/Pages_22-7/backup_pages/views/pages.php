<div class="main-content about-us">
	<section class="main-search-sec">
		<div class="container">
			<div class="header-srch">
				<div class="row">
					<div class="col-md-12">
						<div class="main-srch-heading">
							<h1><?php echo $details[0]['pg_title']; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<nav class="navbar navbar-default in-page-nav">
    <div class="container">
        <!-- <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div> -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            	<?php
            	foreach($similar_pages as $pages){
            		?>
            		<li class="<?php if($this->uri->segment(3) == $pages['pg_meta_tag']) echo "active"; ?>" >
	                    <a href="<?php echo $pages['pg_meta_tag']; ?>"><?php echo $pages['pg_title']; ?></a>
	                </li>
            		<?php
            	}
            	?>                
            </ul>
        </div>
    </div>
</nav>

<section class="about-sec">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about-content">
					<?php
						echo $details[0]['pg_content'];
					?>
				</div>
			</div>
		</div>
	</div>
</section>