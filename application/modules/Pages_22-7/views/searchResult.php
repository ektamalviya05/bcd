<div class="main-content">
  <div class="main-reg-content">
    <section class="filter-sec">
      <div class="container">
        <form name="searchForm" action="<?php echo base_url("Pages/businessListing"); ?>" method="post">
          <div class="main-srch-form-sec">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
        <input id="autocomplete" type="text" name="user_address" value="<?php echo $location;  ?>" class="form-control add-form" onFocus="geolocate()" class="form-control" placeholder="VILLE,CODE POSTAL">
        
          </input>

                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
        <input type="text" name="business_category1" id="business_category1" value="<?php echo $category;  ?>" 
                  list='categories1'
                  name='user_categories'
                  id="business_category"
                  class="form-control cat-form" placeholder="Entrez une catégorie">
                  <datalist id="categories1">
                    <?php
                    

                    foreach($business_categories->result() as $cate){
                      ?>

                      <option value="<?php echo $cate->cat_name; ?>"><?php echo $cate->cat_name; ?></option>
                      <?php
                    }
                    ?>                        
                  </datalist>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
        <input type="text" class="form-control" value="<?php echo $keyword;  ?>" name="keyword" placeholder="KEYWORD">
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <a class="btn btn-default extra-filter" href="javascript:void()">Extra Filters</a>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <button class="btn btn-default main-sbmit" name="searchSubmit" value="Search" type="submit">Search</button>
              </div>
            </div>
          </div>
          <div class="extra-filter-sec">
            <div class="row">
              <div class="col-md-12">
                <div class="etra-fil-head">
                  <h3>Select Extra Filters</h3>
                </div>
                <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark" style="max-height: 220px!important;background: #212121; padding: 15px; height: auto; overflow: hidden;">
                                   
                   <?php
               
                    foreach($busienss_filters->result() as $cate){
                      $chk="";
                      foreach ($extraFilter as $key => $value) {
                       if($cate->cat_name==$value)
                       {
                        $chk="checked";
                       }
                      }
                      ?>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                   <input type="checkbox" <?php echo $chk; ?> id="<?php echo $cate->cat_id; ?>" value="<?php echo $cate->cat_name; ?>" name="extra_fltr[]" />
                  <label class="chk-box-label" for="<?php echo $cate->cat_id; ?>"></label><span><?php echo $cate->cat_name; ?></span>
                 
                    </div>
                  </div>
                  <?php
                    }
                    ?> 

                </div>
              </div>

            </div>
          </div>
        </form>
      </div>
    </section>
    <section class="main-map-sec">
      <div class="map-main">
     
     
     <div class="right_map_sce">   <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14722.96212982255!2d75.92889325!3d22.700701!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1496980678600" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
     
     </div>
     
      <div id="toggle-map-button">
      <a href="javascript:void(0)"><i class="fa fa-expand" aria-hidden="true"></i></a>
      </div>  
     
      <div class="google-ad" style="float:right;">
     
      <img src="<?php echo base_url();?>assets/img/ads-sec.png">
     
      </div>
     
      </div>
     
     
      <div class="main-listing-sec">
        
<?php 
if($record_found>0)
{
    echo "<h3>Total (".$record_found.") Record Found</h3>";
foreach ($search_results as $row) {
  ?>
        <div class="listing-sub">
          <div class="image-lis-sub">
            <img src="<?php echo base_url("assets/profile_pics/").$row['user_profile_pic']; ?>">
          </div>
          <div class="image-txt-sub">
            <div class="img-txt-subhead">
              <h3><?php echo $row['user_company_name'] != ""? $row['user_company_name']: "Name not in DB"; ?></h3>
              <span class="rating-main">
                <ul>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                </ul>
              </span>
            </div>
            <p class="add-l"><i class="fa fa-map" aria-hidden="true"></i><?php echo $row['user_company_address'] != ""? $row['user_company_address']: "NA"; ?></p>
            <p class="catgry-l"><span class="f-cat"><i class="fa fa-bars" aria-hidden="true"></i> Categorie(s): </span><span><?php echo $row['categories'] != ""? $row['categories']: "NA"; ?></span></p>
            <p class="distance-l"><span class="f-cat"><i class="fa fa-map-marker"></i>Distance: </span><span><?php echo $row['distance']. ' KM'; ?></span></p>
            <div class="dsply-num">
<?php
  if($this->session->userdata('isUserLoggedIn')){

?>
              <a href="<?php echo base_url()."Users/user_profile/".$row['user_id'];  ?>" target="_blank" class="btn btn-default"><i class="fa fa-phone"></i>Afficher le numéro</a>
              <?php
            }
            else
            {
?>
<a class="btn btn-default"  href="javascript:;" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i>Afficher le numéro</a>
<?php
            }
            ?>
            </div>
          </div>
        </div>
      <?php

      # code...
}
}
else
{
  echo "<h3>No Record Found</h3>";
}
      ?>
        <?php //print_r($search_results); ?>       
      </div>    
    </section>
  </div>
</div>


<script>
$("#toggle-map-button a").click(function(){
   $(".map-main").toggleClass("stretch-m");
   $(".main-listing-sec").toggleClass("stretch-l");

});
</script>


