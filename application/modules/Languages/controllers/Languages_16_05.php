<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class Languages extends MX_Controller{
	function __construct(){
		parent::__construct();
		$userid = $this->session->userdata("user_id");
		if(empty($userid))
			redirect("Admin");
		$this->load->model("Lang_model");
		$this->load->library("session");		
		$this->load->library("form_validation");
	}

	function index($args = "Home"){
		$data = array();
		header("Access-Control-Allow-Origin: *");
		$args = str_replace("-", " ", $args);
		$data['languages'] = $this->Lang_model->getRows(array("select"=>"*", "sorting"=>array("lang_id"=>"DESC")));
		$data['heading'] = $args;
		// echo "<pre>";
		// print_r($data['languages']);
		// exit;
		$this->template->set('title', 'Language Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
	}
	
	function Add($args = "Add"){
		 $data = array();
		 header("Access-Control-Allow-Origin: *");
		 $args = str_replace("-", " ", $args);
         $data['heading'] = 'Add language';
         $data['action'] = base_url('Languages/Add');
		 $this->template->set('title', 'Add Language');
	     $this->template->load('admin_layout', 'contents' , 'edit', $data);
	}
	function edit($id=''){
		 $data = array();
		 header("Access-Control-Allow-Origin: *");
		 $data['language'] = $this->Lang_model->getRows(array("select"=>"*", "conditions"=>array("lang_id"=>$id),"returnType"=>"single"));
          
         $data['heading'] = 'Edit language';
         $data['action'] = base_url('Languages/Edit/'.$id);
		 $this->template->set('title', 'Edit Language');
	     $this->template->load('admin_layout', 'contents' , 'edit', $data);
	}

    function update_language(){
		  $data['lang_name'] = $this->input->post('name');
		  $data['lang_status'] = $this->input->post('status');
		  
		  if($this->input->post('lang_id')){
			  
			  $lang_id = $this->input->post('lang_id');
			   
			  $where = array("lang_id"=>$lang_id);
			  $result = $this->Lang_model->insertTable("0",$data,$where);
			  $array = array();
			  if($result){
				   $array['status'] = 200;
				   $array['msg'] = 'Language Update successfully';
				  
			  }else{
				   $array['status'] = 300;
				   $array['msg'] = 'Language not update';
			  }			  
			  
		  }else{
			  $result = $this->Lang_model->insertTable("1",$data);
			  $array = array();
			  if($result){
				   $array['status'] = 200;
				   $array['msg'] = 'Language inserted successfully';
				  
			  }else{
				   $array['status'] = 300;
				   $array['msg'] = 'Language not inserted';
			  }
		 } 
		  echo json_encode($array);
		  
	}

     function delete_language(){
		 $id = $this->input->post('id');
		 $this->Lang_model->deleteTable($id);
		 //echo $this->db->last_query();
		 $array['status'] = 200;
		 echo json_encode($array);
	 }	

 
}