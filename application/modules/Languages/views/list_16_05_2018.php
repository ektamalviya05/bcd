<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
      <!-- <div class="import-create dash-counter">
          <h2>Create</h2>
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("ETemplate/add"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Add</h3>
                            <p>New Template</p>
                        </div>
                    </a>
                </div>
            </div>            
          </div>
      </div> -->
        <div class="import-create dash-counter">
          <!-- <h2>Create</h2> -->
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url('Languages/Add'); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Add</h3>
                            <p>New Language</p>
                        </div>
                    </a>
                </div>
            </div>
 
          </div>
      </div>

    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2> Language List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th>No.</th>
                  <th >Name</th>
                  <th> Status</th> 
                  <th >Action</th>
                </tr>
                <?php                
                if(!empty($languages)){
                  $count = 1;
                  foreach($languages as $lang){
                  ?>
                  <tr>
                    <td><?php echo $count; ?></td>
                    <td ><?php echo $lang['lang_name'] ?></td>
                    <td ><?php echo ($lang['lang_status']==1)? 'active':'inactive'; ?></td>
 
                    <td>
                      <div class="link-del-view">
                        <div class="tooltip-2"><a href="<?php echo base_url("Languages/edit/").$lang['lang_id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                          <span class="tooltiptext">Edit</span>
                        </div>  

						<div class="tooltip-2">
						      <a href="javascript:;"   data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="delete_language" modal-aria="<?php echo$lang['lang_id'];?>" data-original-title="" title=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
						      <span class="tooltiptext">Delete</span>
						</div>                        
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
             <div class="read-more">
                      
                        </div>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>