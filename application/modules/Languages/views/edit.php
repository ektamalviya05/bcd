<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <input type="hidden" id="editid" value="<?php echo $cat['cat_id']; ?>">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $heading;?></h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <form id="saveLanguage" enctype="multipart/form-data" method="post" action="<?php echo $action; ?>" data-parsley-validate class="form-horizontal form-label-left">    
                        <?php if(!empty($language)) { ?>
                       <input type="hidden" name="lang_id" value="<?php echo $language['lang_id'];?>" > 
                        <?php } ?>					   
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Name:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="name" value="<?php if(!empty($language)) {  echo $language['lang_name']; }?>">                          
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Status</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <select class="form-control"  name="status">
						         <option value="0" <?php if(!empty($language) and $language['lang_status']==0) {  echo "selected";}?>>inactive</option>
						         <option value="1" <?php if(!empty($language) and $language['lang_status']==1) {  echo "selected";}?>>active</option>
						   </select>                          
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id="ersbmsg">
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" id="catsub" class="btn btn-success">Save</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>                          
                          <div id="catupmsg">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
 