<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends MX_Controller {

	public function __construct(){
	 	parent:: __construct();
	 	$this->load->model("Chat_model");
	 	$this->sesid = $this->session->userdata("user_id");
	 	if(empty($this->sesid)){
	 		redirect(base_url());
	 	}
	}

	function index(){
		
	}

	function checkStatus(){
		$unreadmsg = $this->Chat_model->getMessagesRows(array("conditions"=>array("msg_reciever"=>$this->sesid, "msg_flag"=>0, "us_notify"=>0), "grouping"=>"msg_sender"));
		if(!empty($unreadmsg)){
			$arr = array();
			foreach($unreadmsg as $msg){
				$arr[] = $msg['msg_sender'];
			}
			echo json_encode(array("status"=>200, "details"=>$arr));
		}else{
			echo json_encode(array("status"=>1));
		}
	}

	/*
	*	Get All user messages
	*/
	function getMessages(){
		if(!empty($this->sesid)){
			$unreadmsg = $this->Chat_model->getMessagesRows(array("conditions"=>array("msg_reciever"=>$this->sesid, "window_open"=>0), "grouping"=>"msg_sender"));
			$data = array();
			$recvr = $this->Chat_model->getUserRows(array("user_id"=>$this->sesid));
			if(!empty($unreadmsg)){
				foreach($unreadmsg as $msg){
					$sender = $this->Chat_model->getUserRows(array("user_id"=>$msg['msg_sender']));				
					$user = array("roomId"=>$sender['user_id']);
					if($sender['user_type'] == "Professional"){
						if(!empty($sender['bs_name']))							
							$user["user"] = substr(ucwords($sender['bs_name']), 0, 10);
						else
							$user["user"] = substr(ucwords($sender['user_firstname']." ".$sender['user_lastname']), 0, 10);
					}
					else{
						$user["user"] = substr(ucwords($sender['user_firstname']." ".$sender['user_lastname']), 0, 10);
					}
					$messages = $this->Chat_model->getSingleMessage($sender['user_id']);
					$chat = array();
					$cnt = 1;
					if($messages->num_rows() > 0){
						foreach($messages->result() as $msg){
							if($cnt == 1)
								$user['timestmp'] = array("id"=>"msg".$msg->msg_id, "dt"=>$msg->msg_date);

							if($msg->msg_reciever == $this->sesid){
								$chat[] = array("msgid"=>$msg->msg_id,
												"img"=> base_url("assets/profile_pics/").$sender['user_profile_pic'],
												"msg_type"=>1,
												"msg"=>$msg->msg_content,
												"mdate"=>date("d F, H:i", strtotime($msg->msg_date))
												);
							}else{
								$chat[] = array("msgid"=>$msg->msg_id,
												"img"=> base_url("assets/profile_pics/").$recvr['user_profile_pic'],
												"msg_type"=>2,
												"msg"=>$msg->msg_content,
												"mdate"=>date("d F, H:i", strtotime($msg->msg_date))
												);
							}
							$cnt++;
						}
					}
					$user['chat'] = $chat;
					$query = $this->Chat_model->getSingleMessageTimestamp($sender['user_id'], $user['timestmp']['dt']);
					if($query->num_rows() == 0){
						$user['timestmp']["dt"] = "";
					}
					$data[] = $user;
				}
				echo json_encode(array("status"=>200, "details"=>$data), JSON_NUMERIC_CHECK);
			}else{
				echo json_encode(array("status"=>1));
			}
		}else{
			echo json_encode(array("status"=>1));
		}
	}

	/*
	* Get One to one messages
	*/
	function userMessages(){
		if(!empty($this->sesid)){
			$recipents = $this->input->post("roomId");
			$messages = $this->Chat_model->getSingleMessage($recipents);
			$chat = array();
			$recvr = $this->Chat_model->getUserRows(array("user_id"=>$this->sesid));
			$sender = $this->Chat_model->getUserRows(array("user_id"=>$recipents));
			$user = array("roomId"=>$sender['user_id']);
			if($sender['user_type'] == "Professional"){
				if(!empty($sender['bs_name']))
					$user["user"] = substr(ucwords($sender['bs_name']), 0, 10);
				else
					$user["user"] = substr(ucwords($sender['user_firstname']." ".$sender['user_lastname']), 0, 10);
			}
			else{
				$user["user"] = substr(ucwords($sender['user_firstname']." ".$sender['user_lastname']), 0, 10);
			}
			$chat = array();
			$cnt = 1;
			if($messages->num_rows() > 0){
				foreach($messages->result() as $msg){
					if($cnt == 1)
						$user['timestmp'] = array("id"=>"msg".$msg->msg_id, "dt"=>$msg->msg_date);
					if($msg->msg_reciever == $this->sesid){
						$chat[] = array("msgid"=>$msg->msg_id,
										"img"=> base_url("assets/profile_pics/").$sender['user_profile_pic'],
										"msg_type"=>1,
										"msg"=>$msg->msg_content,
										"mdate"=>date("d F, H:i", strtotime($msg->msg_date))
										);
					}else{
						$chat[] = array("msgid"=>$msg->msg_id,
										"img"=> base_url("assets/profile_pics/").$recvr['user_profile_pic'],
										"msg_type"=>2,
										"msg"=>$msg->msg_content,
										"mdate"=>date("d F, H:i", strtotime($msg->msg_date))
										);
					}
					$cnt++;
				}
				$query = $this->Chat_model->getSingleMessageTimestamp($recipents, $user['timestmp']['dt']);
				if($query->num_rows() == 0){
					$user['timestmp']["dt"] = "";
				}
			}else{
				$user['timestmp'] = array("id"=>"", "dt"=>"");
			}
			$user['chat'] = $chat;
			echo json_encode(array("status"=>200, "details"=>$user));
		}else{
			echo json_encode(array("status"=>1));
		}
	}

	/*
	* Get One to one messages according to time stamp
	*/
	function timestmpMessages(){
		if(!empty($this->sesid)){
			$recipents = $this->input->post("roomId");
			$tmpstmp = $this->input->post("tmpstmp");			
			$messages = $this->Chat_model->getSingleMessageTimestamp($recipents, $tmpstmp);
			$chat = array();
			$recvr = $this->Chat_model->getUserRows(array("user_id"=>$this->sesid));
			$sender = $this->Chat_model->getUserRows(array("user_id"=>$recipents));
			$user = array("roomId"=>$sender['user_id']);
			if($sender['user_type'] == "Professional"){
				$user["user"] = substr(ucwords($sender['bs_name']), 0, 10);
			}
			else{
				$user["user"] = substr(ucwords($sender['user_firstname']." ".$sender['user_lastname']), 0, 10);
			}
			$chat = array();
			$cnt = 1;
			if($messages->num_rows() > 0){
				foreach($messages->result() as $msg){
					if($cnt == 1)
						$user['tmstmp'] = array("id"=>"msg".$msg->msg_id, "dt"=>$msg->msg_date);
					if($msg->msg_reciever == $this->sesid){
						$chat[] = array("msgid"=>$msg->msg_id,
										"img"=> base_url("assets/profile_pics/").$sender['user_profile_pic'],
										"msg_type"=>1,
										"msg"=>$msg->msg_content,
										"mdate"=>date("d F, H:i", strtotime($msg->msg_date))
										);
					}else{
						$chat[] = array("msgid"=>$msg->msg_id,
										"img"=> base_url("assets/profile_pics/").$recvr['user_profile_pic'],
										"msg_type"=>2,
										"msg"=>$msg->msg_content,
										"mdate"=>date("d F, H:i", strtotime($msg->msg_date))
										);
					}
					$cnt++;
				}
			}
			$query = $this->Chat_model->getSingleMessageTimestamp($recipents, $user['tmstmp']['dt']);
			if($query->num_rows() == 0){
				$user['tmstmp']["dt"] = "";
			}
			$user['chat'] = $chat;
			echo json_encode(array("status"=>200, "details"=>$user));
		}else{
			echo json_encode(array("status"=>1));
		}
	}

	 /*
    * Send message to other users
    */
    function sendUserMessage(){
    	if(!empty($this->sesid)){
            $id = $this->Chat_model->insertTable(1, array("msg_sender"=>$this->sesid,
                                                          "msg_reciever"=>$this->input->post("roomId"),
                                                          "msg_content"=>$this->input->post("msg")));
            if($id){
                $msg = $this->Chat_model->getMessagesRows(array("msg_id"=>$id));
                $user = $this->Chat_model->getUserRows(array("user_id"=>$this->sesid));
                $message = '<div id="msg'.$msg['msg_id'].'" class="row msg_container base_sent"><div class="col-md-10 col-xs-10"><div class="messages msg_receive"><p>'.$msg['msg_content'].'</p><time datetime="2009-11-13T20:00">'.date("d F, H:i", strtotime($msg['msg_date'])).'</time></div></div><div class="col-md-2 col-xs-2 avatar"><img src="'.base_url("assets/profile_pics/").$user['user_profile_pic'].'" class=" img-responsive "></div></div>';
                echo json_encode(array("status"=>200, "content"=>$message));
            }
            else{
                echo json_encode(array("status"=>1));
            }
        }
        else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Your session is expired, Please login again.</label>"));
        }
    }    

    /*
    * update message status
    */
	function status(){
		$roomId = $this->input->post("roomId");
		$ntstatus = $this->input->post("ntstatus");
		$rdstatus = $this->input->post("rdstatus");
		$open = $this->input->post("open");
		if(!empty($roomId)){
			$ar = array("us_notify"=>$ntstatus);
			if(!empty($rdstatus))
				$ar["msg_flag"] = $rdstatus;
			if(!empty($open))
				$ar['window_open'] = $open;
			$check = $this->Chat_model->insertTable(2, $ar, array("msg_sender"=>$roomId, "msg_reciever"=>$this->sesid));
			echo json_encode(array("status"=>200));
		}else{
			echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));
		}
	}
}