<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat_model extends CI_Model {
	function __construct(){
		parent::__construct();
		$this->msgTbl = "otd_user_messaging";
    $this->usTbl = "users";
    $this->businessTbl = 'otd_business_details';
    $this->ses = $this->session->userdata("user_id");
	}
	
  /*
  * get rows from the langauges table
  */
  function getMessagesRows($params = array()){
     $this->db->select('*');
     $this->db->from($this->msgTbl);
     //fetch data by conditions
     if(array_key_exists("conditions",$params)){
         foreach ($params['conditions'] as $key => $value) {
             $this->db->where($key,$value);
         }
     }
      if(array_key_exists("grouping",$params)){
        $this->db->group_by($params['grouping']);
      }
     if(array_key_exists("msg_id",$params)){
         $this->db->where('msg_id',$params['msg_id']);
         $query = $this->db->get();
         $result = $query->row_array();
     }else{            
         //set start and limit
         if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
             $this->db->limit($params['limit'],$params['start']);                
         }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
             $this->db->limit($params['limit']);
         }
         $query = $this->db->get();


         if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
             $result = $query->num_rows();                
         }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
             $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
         }else{
             $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
         }
     }
     return $result;
  }

  function getSingleMessage($args){
    return $this->db->query("SELECT * FROM (SELECT * FROM $this->msgTbl WHERE (msg_sender = $args AND msg_reciever= $this->ses) OR (msg_sender = $this->ses AND msg_reciever= $args) ORDER BY msg_date DESC LIMIT 10) AS T1 ORDER BY msg_id ASC");    
  }

  function getSingleMessageTimestamp($args, $timestamp){
    return $this->db->query("SELECT * FROM (SELECT * FROM $this->msgTbl WHERE msg_date < '$timestamp' AND ((msg_sender = $args AND msg_reciever= $this->ses) OR (msg_sender = $this->ses AND msg_reciever= $args)) ORDER BY msg_date DESC LIMIT 10) AS T1 ORDER BY msg_id ASC");    
  }

  function getUserRows($params = array()){
    $this->db->select('*');
    $this->db->from($this->usTbl);
    $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->usTbl.user_id", "left");
    //fetch data by conditions
    if(array_key_exists("conditions",$params)){
       foreach ($params['conditions'] as $key => $value) {
           $this->db->where($key,$value);
       }
    }

    if(array_key_exists("sorting",$params)){
        foreach($params['sorting'] as $key => $value) {
            $this->db->order_by($key, $value);
            // $this->session->set_userdata('order_by',$value);
        }
    }
    if(array_key_exists("grouping",$params)){
      $this->db->group_by($params['grouping']);
    }
    if(array_key_exists("user_id",$params)){
       $this->db->where('user_id',$params['user_id']);
       $query = $this->db->get();
       $result = $query->row_array();
    }else{            
       //set start and limit
       if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
           $this->db->limit($params['limit'],$params['start']);                
       }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
           $this->db->limit($params['limit']);
       }
       $query = $this->db->get();


       if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
           $result = $query->num_rows();                
       }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
           $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
       }else{
           $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
       }
    }
    return $result;
  }

   /*
    * Insert / Update into Message table
    */

    function insertTable($opr = 1, $para = array(), $where = array()){
      if($opr == 1){ // insert testimonial
        $this->db->insert($this->msgTbl, $para);
        $insert_id = $this->db->insert_id();
        if($this->db->affected_rows() > 0)
          return $insert_id;
        else
          return false;
      }
      else{ // update existing testimonial
        $this->db->update($this->msgTbl, $para, $where);
        return true;
      }
    }
}
