<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Content_model extends CI_Model{
    function __construct() {
        $this->contTbl = 'site_contents';
    }

    
    /*
     * get rows from the Site Content table
     */
    function getRows($params = array()){

      $this->db->select('*');
        $this->db->from($this->contTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach ($params['sorting'] as $key => $value) {

                $this->db->order_by($value, $order_mode);
                $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

   //echo "<br>".$this->db->last_query()."<br>";
        return $result;
    }

    /*
     * Insert user information
     */
    public function insert($data = array()) {

         //insert user data to users table
        $insert = $this->db->insert($this->contTbl, $data);
        
        //return the status

        if($insert){
          //return $this->db->insert_id();
            return true;
        }else{
            return false;
        }
    }


}