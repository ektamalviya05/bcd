<!--page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Email Settings <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
             
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($listing)){                      
                    ?>
                    <form id="updatecontent" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/contentUpdate/$page"); ?>' data-parsley-validate class="form-horizontal form-label-left" autocomplete="off">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Admin Email:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input type="text" class="form-control" name="label1" id="label1" value="<?php echo $listing["label1"]; ?>"/>
                          <?php echo form_error('label1'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Sender Email:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input readonly class="form-control" type="text" name="label2" id="label2" value="<?php echo $listing["label2"]; ?>"/>
                          <?php echo form_error('label2'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Sender Name:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" name="label3" id="label3" value="<?php echo $listing["label3"]; ?>" />
                          <?php echo form_error('label3'); ?>
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" id="catsub" class="btn btn-success">Update</button>                          
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>                          
                            <div id="catupmsg">
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                    <?php
                    }
                    ?>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content