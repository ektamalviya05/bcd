<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Advertisement <a href="javascript:window.history.back()" class="btn btn-default btn-back">  <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>              
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    if(!empty($details)){                      
                      ?>
                      <form id="editcountry" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                                            
                       
                        <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Category Name:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <input type="text" name="name" class="form-control" placeholder="Category Name" value="<?php echo $details[0]['name']; ?>"/>
                            <?php echo form_error('name'); ?>
                          </div>
                        </div>                      
                       
                       <!--  <div class="form-group">
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <label>Status:</label>
                          </div>
                          <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="th-sele">
                              <select class="form-control" name="status">
                                <option value="">Advertisement Status</option>
                                <option value="1" <?php if($details["add_status"] == 1) echo "selected"; ?>>Publish</option>
                                <option value="0" <?php if($details["add_status"] == 0) echo "selected"; ?>>Unpublish</option>
                              </select>
                              <?php echo form_error('status'); ?>
                            </div>
                          </div>
                        </div> -->
                        <div class="form-group">
                          <div class="col-md-6 col-sm-6 col-xs-12 ">
                            <input type="hidden" name="id" value="<?php echo $details[0]['id']; ?>">
                            <button type="submit" class="btn btn-success" id="addsubmit">Save</button>
                            <div id="addmsg"></div>
                          </div>
                        </div>
                      </form>
                      <?php
                    }
                    ?>                    
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->        