<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h2><?php echo $heading; ?></h2>
              </div>              
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <h4>Personal Registration Newsletter 1</h4>
                    <?php
                    foreach($personal1 as $art){ }
                    	$content_id = $art['content_id'];
                    ?>
                    <form id="updateregnewsletter1" enctype="multipart/form-data" method="post" action='' data-parsley-validate class="form-horizontal form-label-left">                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template"><?php echo $art['content']; ?></textarea>
                          <?php echo form_error('template'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <div id="content1"></div> 
                          <input type="hidden" value="content1" name="msgbox"/>
                          <input type="hidden" name="id" value="<?php echo $content_id; ?>" />
                          <button type="submit" class="btn btn-success">Update</button>                          
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="x_content">
                    <br />
                    <h4>Personal Registration Newsletter 2</h4>
                    <?php
                    foreach($personal2 as $art){ }
                      $content_id = $art['content_id'];
                    ?>
                    <form id="updateregnewsletter2" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/updateArtical/$content_id"); ?>' data-parsley-validate class="form-horizontal form-label-left">                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template1"><?php echo $art['content']; ?></textarea>
                          <?php echo form_error('template1'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <div id="content2"></div>
                          <input type="hidden" value="content2" name="msgbox"/>
                          <input type="hidden" name="id" value="<?php echo $content_id; ?>" />
                          <button type="submit" class="btn btn-success">Update</button>                          
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="x_content">
                    <br />
                    <h4>Professional Registration Newsletter 1</h4>
                    <?php
                    foreach($professional1 as $art){ }
                      $content_id = $art['content_id'];
                    ?>
                    <form id="updateregnewsletter3" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/updateArtical/$content_id"); ?>' data-parsley-validate class="form-horizontal form-label-left">                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template2"><?php echo $art['content']; ?></textarea>
                          <?php echo form_error('template2'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <div id="content3"></div>
                          <input type="hidden" value="content3" name="msgbox"/>
                          <input type="hidden" name="id" value="<?php echo $content_id; ?>" />
                          <button type="submit" class="btn btn-success">Update</button>                          
                        </div>
                      </div>
                    </form>
                  </div>
                  <div class="x_content">
                    <br />
                    <h4>Professional Registration Newsletter 2</h4>
                    <?php
                    foreach($professional2 as $art){ }
                      $content_id = $art['content_id'];
                    ?>
                    <form id="updateregnewsletter4" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/updateArtical/$content_id"); ?>' data-parsley-validate class="form-horizontal form-label-left">                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template3"><?php echo $art['content']; ?></textarea>
                          <?php echo form_error('template3'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <div id="content4"></div>
                          <input type="hidden" value="content4" name="msgbox"/>
                          <input type="hidden" name="id" value="<?php echo $content_id; ?>" />
                          <button type="submit" class="btn btn-success">Update</button>                          
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->
        