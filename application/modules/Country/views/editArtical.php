<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $heading; ?>
                <a href="javascript:window.history.back()" class="btn btn-default btn-back"> <i class="fa fa-arrow-left"></i> Back </a></h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <?php
                    foreach($details as $art){ }
                    	$content_id = $art['content_id'];
                    ?>
                    <form id="updateArticals" enctype="multipart/form-data" method="post" action='<?php echo base_url("Content/updateArtical/$content_id"); ?>' data-parsley-validate class="form-horizontal form-label-left">                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template"><?php echo $art['content']; ?></textarea>
                          <?php echo form_error('template'); ?>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" class="btn btn-success">Update Article</button>                          
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->
        <script type="text/javascript">
        	CKEDITOR.replace( 'template', {
			    language: 'en',
			    uiColor: '#9AB8F3',
			    allowedContent:true,
			});
        </script>