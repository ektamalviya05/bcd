<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends MX_Controller {

	public function __construct(){	 

	 	parent:: __construct();
    $userid = $this->session->userdata("user_id");
    if(empty($userid)) {redirect("Admin");}
		/* Load the libraries and helpers */

		$this->load->library("pagination");
		$this->load->library("form_validation");
 		$this->load->helper(array('form', 'url'));
		$this->load->model('Content_model');

	 }
	 	
public function index1()
	{



$data = array();
$segcount = $this->uri->total_segments();
$order_by = "section";
$order="";
$type ="";
$page = 0;

//echo "test";die;


$con['sorting'] = array('order'=>$order_by);
$con['returnType'] ="count";
$data = array();
//$con['conditions'] = array('user_type !='=> 'Admin');

$totalrows = $this->Content_model->getRows($con);
$config['base_url'] = base_url()."Content";
$config['total_rows'] = $totalrows;
$config['per_page'] = 10;
$config['uri_segment'] = 3;
$config['num_links'] = $totalrows / 10;
$config['cur_tag_open'] = '&nbsp;<a class="current">';
$config['cur_tag_close'] = '</a>';
$config['next_link'] = '<b> Next </b>';
$config['prev_link'] = '<b> Previous </b>';

$choice = $config["total_rows"] / $config["per_page"];
$config["num_links"] = round($choice);
$this->pagination->initialize($config);

$page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 1;

$con['limit'] = $config['per_page'];
$start =($page-1) * $config['per_page'];

if($start == 0)
{
  $start = 0;
}
else
{
  $start = ($start/$config['per_page'] )+1;
}
$con['start'] = $start;

          
if($totalrows > 0){
$con['returnType'] ="";	
$data['contlist'] = $this->Content_model->getRows($con);
$data["record_found"] = $totalrows; 
}
else
{
 $data['contlist'] = array();
 $data["record_found"] = 0; 
}
$data["links"] = $this->pagination->create_links();
$data['page'] = $page;



  if($this->session->userdata('isUserLoggedIn'))
  {
    
	header("Access-Control-Allow-Origin: *");
	$this->template->set('title', 'Content Management');
    $this->template->load('admin_layout', 'contents' , 'list_contents', $data);
	}
	else
	{

	}
	

}

public function addcontent()
{

 		$data = array();
        $data['error_msg']=""; 
        $userData = array();
        $data['error_msg_cap'] = "";
     
        if($this->input->post('contentSubmit')){

           //$this->form_validation->set_rules('section', 'Section', 'required');
           //$this->form_validation->set_rules('userfile', 'Sub Section', 'required');
        
        $content_type = strip_tags($this->input->post('content_type'));

        	//if($this->form_validation->run() == true)
            	//{
        
           
if($content_type != "text")
{
$file_name = $this->do_upload($content_type);
}
 else
 {
 	$file_name = "";
 }   		
			if($file_name != -1)
{
			$userData = array(
                'section' => strip_tags($this->input->post('section')),
                'sub_section' => strip_tags($this->input->post('sub-section')),
                'content' => strip_tags($this->input->post('content')),
                'uploaded_file_path' => $file_name,
                 );

				$insert = $this->Content_model->insert($userData);
			}
			else{
				$insert = 0;
			}	
                 if($insert >= 0){
                       redirect(base_url()."Content");
                    }else{
                          $data['error_msg'] = 'Some problems occured, please try again.';
                        }

           		}
            
    	//	}


       				$data['user'] = $userData;
        			header("Access-Control-Allow-Origin: *");
                     $this->template->set('title', 'Personal Account Creation');
                     $this->template->load('admin_layout', 'contents' , 'add_content', $data);

}


public function do_upload($type)
        {
 
 if($type == "img")
 {
 	$config['upload_path'] = 'assets/content_image/'; //The path where the image will be save
 	$config['allowed_types'] = 'gif|jpg|png'; //Images extensions accepted
    $config['max_size']    = '2048'; //The max size of the image in kb's
    $config['max_width']  = '1024'; //The max of the images width in px
    $config['max_height']  = '768'; //The max of the images height in px
    $config['overwrite'] = TRUE; 
  }
  else if($type == "pdf")
  {
  	$config['upload_path'] = 'assets/content_image/'; //The path where the image will be save
 	$config['allowed_types'] = 'pdf'; //Images extensions accepted
    $config['max_size']    = '10280'; //The max size of the image in kb's
   // $config['max_width']  = '1024'; //The max of the images width in px
   // $config['max_height']  = '768'; //The max of the images height in px
    $config['overwrite'] = TRUE; 
  }  
    $this->load->library('upload', $config); //Load the upload CI library
    


                if ( ! $this->upload->do_upload('userfile'))
                {
                        $error = $this->upload->display_errors();
                        return -1;

                }
                else
                {
                      	$file_info = $this->upload->data();
    					$file_name = $file_info['file_name']; 
                        return $file_name;                }
        		}


    function editReadMore($args){
      if($args == "personal"){
        $data['heading'] = "Personal Account Article";
        $con['conditions'] = array("content_id"=>4);
        $data['details'] = $this->Content_model->getRows($con);        
      }
      else if($args == "professional"){
        $data['heading'] = "Professional Account Article";
        $con['conditions'] = array("content_id"=>1);
        $data['details'] = $this->Content_model->getRows($con);        
      }
      else if($args == "footer"){
        $data['heading'] = "Footer Article";
        $con['conditions'] = array("content_id"=>11);
        $data['details'] = $this->Content_model->getRows($con);        
      }
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Edit Articles');
      $this->template->load('admin_layout', 'contents' , 'editArtical', $data);
    }

    function updateArtical($args){
      $arr = array("content"=>$this->input->post("template"));
      $id = array("content_id"=>$args);
      $check = $this->Content_model->editArticles($id, $arr);      
      if($args == 4){
        redirect(base_url("Content/editReadMore/personal"));
      }else if($args == 1){        
        redirect(base_url("Content/editReadMore/professional"));
      }else if($args == 11){        
        redirect(base_url("Content/editReadMore/footer"));
      }else if($args == 26){        
        redirect(base_url("Content/googleAdScript"));
      }
      // else
      //   redirect(base_url());
    }

    function newsLetterRegistrations(){
      // $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
      // $this->form_validation->set_rules("")
      $con['conditions'] = array("content_id"=>7);
      $data['personal1'] = $this->Content_model->getRows($con);
      $con['conditions'] = array("content_id"=>8);
      $data['personal2'] = $this->Content_model->getRows($con);
      $con['conditions'] = array("content_id"=>9);
      $data['professional1'] = $this->Content_model->getRows($con);
      $con['conditions'] = array("content_id"=>10);
      $data['professional2'] = $this->Content_model->getRows($con);
      $data['heading'] = "Update registration newsletter content";
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Newsletters');

      $this->template->load('admin_layout', 'contents' , 'RegistrationNewsletter', $data);
    }

    function updateregNLetter(){         
        $arr = array("content"=>$this->input->post("template"));
        $id = array("content_id"=>$this->input->post("id"));
        $msgbox = $this->input->post("msgbox");
        $check = $this->Content_model->editArticles($id, $arr);
        if($check){
          echo json_encode(array("status"=>200, "render"=>$msgbox, "message"=>"<label style='color:#0F0'>Content updated successfully</label>"));
        }
        else{
          echo json_encode(array("status"=>1, "render"=>$msgbox, "message"=>"<label style='color:#F00'>Content not updated</label>"));
        }
    }

    public function pagesList(){
      $data = array();
      $con["sorting"] = array("pg_date_created"=>"DESC");
      $con['conditions'] = array("pg_status != "=>3);
      $data['details'] = $this->Content_model->getPageRows($con);
      if(empty($data['details']))
        $data['details'] = array();
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Static Pages');
      $this->template->load('admin_layout', 'contents' , 'pageslisting', $data);
    }

    public function trashPageListing(){
      $data = array();
      $con["sorting"] = array("pg_date_created"=>"DESC");
      $con['conditions'] = array("pg_status"=>3);
      $data['details'] = $this->Content_model->getPageRows($con);
      if(empty($data['details']))
        $data['details'] = array();
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Newsletters');
      $this->template->load('admin_layout', 'contents' , 'trashpageslisting', $data);
    }

    public function addPage(){
      $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
      $this->form_validation->set_rules("title", "title", "trim|required", array("required"=>"Please enter %s"));
      
      $this->form_validation->set_rules("meta_title", "meta_title", "trim|required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("page_url", "page_url", "trim|required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("meta_description", "meta_description", "trim|required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("meta_keyword", "meta_keyword", "trim|required", array("required"=>"Please enter %s"));

      
      $this->form_validation->set_rules("template", "content", "required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("category", "category", "required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("status", "status", "required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("display_order", "display order", "required", array("required"=>"Please enter %s"));
      
      
      if($this->form_validation->run() == FALSE){
        $data = array();
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Newsletters');
        $this->template->load('admin_layout', 'contents' , 'addpage', $data);
      }
      else{
        $arr = array("pg_title"=>$this->input->post("title"),
                     "pg_meta_tag"=>url_title($this->input->post("title")),
                     "pg_content"=>$this->input->post("template"),
                     "pg_type"=>1,
                     
                     "page_url"=>$this->input->post("page_url"),
                     "meta_title"=>$this->input->post("meta_title"),
                     "meta_description"=>$this->input->post("meta_description"),
                     "meta_keyword"=>$this->input->post("meta_keyword"),

                     "pg_cat"=>$this->input->post("category"),
                     "pg_status"=>$this->input->post("status"),
                     "pg_display_number"=>$this->input->post("display_order")
                    );
        $check = $this->Content_model->insertPage($arr);
        if($check){
          $this->session->set_flashdata("success", "Page added successfully");
          redirect(base_url("Content/pagesList"));
        }
        else{
          $this->session->set_flashdata("error", "Page not added, Please try again");
          redirect(base_url("Content/pagesList"));
        }
      }
    }

    public function editPage($args){
      $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
      $this->form_validation->set_rules("title", "title", "trim|required", array("required"=>"Please enter %s"));
      //$this->form_validation->set_rules("title_ar", "title_ar", "trim|required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("template", "content", "required", array("required"=>"Please enter %s"));
      //$this->form_validation->set_rules("category", "category", "required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("status", "status", "required", array("required"=>"Please enter %s"));
      //$this->form_validation->set_rules("display_order", "display order", "required", array("required"=>"Please enter %s"));
      
      $this->form_validation->set_rules("meta_title", "meta_title", "trim|required", array("required"=>"Please enter %s"));
     // $this->form_validation->set_rules("page_url", "page_url", "trim|required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("meta_description", "meta_description", "trim|required", array("required"=>"Please enter %s"));
      $this->form_validation->set_rules("meta_keyword", "meta_keyword", "trim|required", array("required"=>"Please enter %s"));

      
      if($this->form_validation->run() == FALSE){        
        $data = array();
        $con['conditions'] = array("pg_id"=>$args);
        $data['details'] = $this->Content_model->getPageRows($con);
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Newsletters');
        $this->template->load('admin_layout', 'contents' , 'editpage', $data);
      }
      else{
        $arr = array("pg_title"=>$this->input->post("title"),
                     "pg_title_ar"=>$this->input->post("title_ar"),
                     "pg_meta_tag"=>url_title($this->input->post("title")),
                     
                     "page_url"=>$this->input->post("page_url"),
                     "meta_title"=>$this->input->post("meta_title"),
                     "meta_description"=>$this->input->post("meta_description"),
                     "description"=>$this->input->post("description"),
                     "description_ar"=>$this->input->post("description_ar"),
                     "meta_keyword"=>$this->input->post("meta_keyword"),

                     "pg_content"=>$this->input->post("template"),
                     "pg_content_ar"=>$this->input->post("template1"),
                     "pg_type"=>1,
                     "pg_cat"=>$this->input->post("category"),
                     "pg_status"=>$this->input->post("status"),
                     "pg_display_number"=>$this->input->post("display_order")
                    );
        $check = $this->Content_model->editPage(array("pg_id"=>$args), $arr);        
        if($check){
          $this->session->set_flashdata("success", "Page added successfully");
          redirect(base_url("Content/pagesList"));
        }
        else{
          $this->session->set_flashdata("error", "Page not added, Please try again");
          redirect(base_url("Content/pagesList"));
        }
      }
    }

    function trashPage(){
      $id = $this->input->post("id");
      if(!empty($id)){
        $this->db->update("otd_page_contents", array("pg_status"=>3), array("pg_id"=>$id));
        if($this->db->affected_rows() > 0){
          echo json_encode(array("status"=>200));
        }
        else{
          echo json_encode(array("status"=>1));
        }
      }
    }

    public function trashbackPage(){
      $id = $this->input->post("id");
      if(!empty($id)){
        $this->db->update("otd_page_contents", array("pg_status"=>0), array("pg_id"=>$id));
        if($this->db->affected_rows() > 0){
          echo json_encode(array("status"=>200));
        }
        else{
          echo json_encode(array("status"=>1));
        }
      }
    }

    public function trashbackAndPublishPage(){
      $id = $this->input->post("id");
      if(!empty($id)){
        $this->db->update("otd_page_contents", array("pg_status"=>1), array("pg_id"=>$id));
        if($this->db->affected_rows() > 0){
          echo json_encode(array("status"=>200));
        }
        else{
          echo json_encode(array("status"=>1));
        }
      }
    }


    // function deletePage(){
    //   $id = $this->input->post("id");
    //   if(!empty($id)){
    //     $this->db->delete("otd_page_contents", array("pg_id"=>$id));
    //     if($this->db->affected_rows() > 0){
    //       echo json_encode(array("status"=>200));
    //     }
    //     else{
    //       echo json_encode(array("status"=>1));
    //     }
    //   }
    // }

    function pageUpdateStatus(){
      $id = $this->input->post('id');
      $val = $this->input->post('value');
      if(!empty($id)){
        $this->db->update("otd_page_contents", array("pg_status"=>$val), array("pg_id"=>$id));
        if($this->db->affected_rows() > 0){
          echo json_encode(array("status"=>200));
        }
        else{
          echo json_encode(array("status"=>1));
        }
      }
    }

    function homeSection($args = Null){
      if(!empty($args)){        
        $data = array();
        if($args == 2){
          $data['heading'] = "Edit Category Section";
          $this->template->set('title', 'Edit Category Section');
          $con['conditions'] = array("content_id"=>24);
          $data['details'] = $this->Content_model->getRows($con);
        }else if($args == 3) {
          $data['heading'] = "Edit Home Promotion Section";
          $this->template->set('title', 'Edit Home Promotion Section');
          $data['details'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>21), "returnType"=>"single"));
          $data['details1'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>22), "returnType"=>"single"));
          $data['details2'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>23), "returnType"=>"single"));
        }else if($args == 5) {
          $data['heading'] = "Edit Home About Section";
          $this->template->set('title', 'Edit Home About Section');
          $con['conditions'] = array("content_id"=>14);
          $data['details'] = $this->Content_model->getRows($con);
        }else if($args == 6){
          $data['heading'] = "Edit Home Services Section";
          $this->template->set('title', 'Edit Home Services Section');
          $data['details'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>15), "returnType"=>"single"));
          $data['details1'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>16), "returnType"=>"single"));
          $data['details2'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>17), "returnType"=>"single"));
          $data['details3'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>18), "returnType"=>"single"));
          $data['details4'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>19), "returnType"=>"single"));
          $data['details5'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>20), "returnType"=>"single"));
        }else if($args == 7){
          $data['heading'] = "Edit Home Business Section";
          $this->template->set('title', 'Edit Home Business Section');
          $con['conditions'] = array("content_id"=>12);
          $data['details'] = $this->Content_model->getRows($con);
        }else if ($args == 9) {
          $data['heading'] = "Edit Home Support Section";
          $this->template->set('title', 'Edit Home Support Section');
          $con['conditions'] = array("content_id"=>13);
          $data['details'] = $this->Content_model->getRows($con);
        }else if($args == "SocialLinks"){
          $data['heading'] = "Social Links";
          $this->template->set('title', 'Social Links');
          $data['details'] = $this->Content_model->getSocialRows(array("sc_id"=>1));
          $data['details1'] = $this->Content_model->getSocialRows(array("sc_id"=>2));
          $data['details2'] = $this->Content_model->getSocialRows(array("sc_id"=>3));
          $data['details3'] = $this->Content_model->getSocialRows(array("sc_id"=>4));
          $data['details4'] = $this->Content_model->getSocialRows(array("sc_id"=>5));
        }else if($args == "purchaseReceipt"){
          $data['heading'] = "Purchase Receipt";
          $this->template->set('title', 'Purchase Receipt');
          $data['details'] = $this->Content_model->getRows(array("conditions"=>array("content_id"=>27), "returnType"=>"single"));
 
        }else{
          redirect(base_url("Admin"));
        }
        $data['which'] = $args;                   
        header("Access-Control-Allow-Origin: *");        
        $this->template->load('admin_layout', 'contents' , 'homesections', $data);
      }
      else{
        redirct(base_url("Admin"));
      }
    }
    function update_receipt(){
          $arr["content"] = $this->input->post("receipt");
          $id = array("content_id"=>27);
          $check = $this->Content_model->editArticles($id, $arr);$array = array('status'=>200,
		                 'msg'=>'<label class="text-success">Receipt Update successfully.</label>'
						 );		 
		  echo json_encode($array);
	}
    function updatehomeSection($args){
      if(!empty($args)){
        if($args == 2){          
          $arr["content"] = $this->input->post("template");
          $arr["section"] = $this->input->post("title");
          $id = array("content_id"=>24);
          $check = $this->Content_model->editArticles($id, $arr);
          $this->session->set_flashdata("error", "<label class='text-success'>Content updated successfully.</label>");          
          redirect("Content/homeSection/$args");
        }
        if($args == 7){
          if(!empty($_FILES['backgroundimage']['name'])){            
            $ext = pathinfo($_FILES['backgroundimage']['name'], PATHINFO_EXTENSION);
            if($ext == "png" || $ext == "jpg" || $ext == "jpeg" || $ext == "JPEG" ||$ext == "PNG" || $ext == "JPG"){
              $query = $this->Content_model->getRows(array("content_id"=>12));              
              if(!empty($query['background_img_path'])){
                unlink('./././assets/img/'.$query['background_img_path']);
              }
              $random = $this->generateRandomString(10);              
              $file_name = $random.".".$ext;
              $dirpath = './././assets/img/'.$file_name;            
              if(move_uploaded_file($_FILES['backgroundimage']['tmp_name'], $dirpath)){
                  $arr['background_img_path'] = $file_name;
              }
            }
            else{
              $this->session->set_flashdata("error", "<label class='text-danger'>Please upload png|jpg images only.</label>");
              redirect("Content/homeSection/$args");
            }            
          }
          $arr["content"] = $this->input->post("template");
          $arr["section"] = $this->input->post("title");
          $id = array("content_id"=>12);
          $check = $this->Content_model->editArticles($id, $arr);
          $this->session->set_flashdata("error", "<label class='text-success'>Content updated successfully.</label>");          
          redirect("Content/homeSection/$args");
        }
        else if($args == 9){
          if(!empty($_FILES['backgroundimage']['name'])){            
            $ext = pathinfo($_FILES['backgroundimage']['name'], PATHINFO_EXTENSION);
            if($ext == "png" || $ext == "jpg" || $ext == "jpeg" || $ext == "JPEG" ||$ext == "PNG" || $ext == "JPG"){
              $query = $this->Content_model->getRows(array("content_id"=>13));            
              if(!empty($query['background_img_path'])){
                unlink('./././assets/img/'.$query['background_img_path']);
              }
              $random = $this->generateRandomString(10);              
              $file_name = $random.".".$ext;
              $dirpath = './././assets/img/'.$file_name;            
              if(move_uploaded_file($_FILES['backgroundimage']['tmp_name'], $dirpath)){
                  $arr['background_img_path'] = $file_name;
              }
            }
            else{
              $this->session->set_flashdata("error", "<label class='text-danger'>Please upload png|jpg images only.</label>");
              redirect("Content/homeSection/$args");
            }            
          }
          $arr["content"] = $this->input->post("template");
          $arr["section"] = $this->input->post("title");
          $id = array("content_id"=>13);
          $check = $this->Content_model->editArticles($id, $arr);
          $this->session->set_flashdata("error", "<label class='text-success'>Content updated successfully.</label>");          
          redirect("Content/homeSection/$args");
        }
        else if($args == 5){
          $arr = array();
          if(!empty($_FILES['about_video']['name'])){            
            $size = round(($_FILES['about_video']['size'] / (1024*1024)), 1);
            if($size < 15){
              $ext = pathinfo($_FILES['about_video']['name'], PATHINFO_EXTENSION);
              if($ext == "mp4"){
                $query = $this->Content_model->getRows(array("content_id"=>14));            
                if(!empty($query['uploaded_file_path'])){
                  unlink('./././assets/img/homevideo/'.$query['uploaded_file_path']);
                }
                $random = $this->generateRandomString(10);              
                $file_name = $random.".".$ext;
                $dirpath = './././assets/img/homevideo/'.$file_name;            
                if(move_uploaded_file($_FILES['about_video']['tmp_name'], $dirpath)){
                    $arr['uploaded_file_path'] = $file_name;
                }
              }
              else{
                $this->session->set_flashdata("error", "<label class='text-danger'>Please upload mp4 video only.</label>");
                redirect("Content/homeSection/$args");
              }
            }
            else{
              $this->session->set_flashdata("error", "<label class='text-danger'>Upload only less then 15mb video.</label>");
              redirect("Content/homeSection/$args");
            }
          }
          $arr["content"] = $this->input->post("template");
          $arr["section"] = $this->input->post("title");
          $arr['video_type'] = $this->input->post("vdtype");
          if($arr['video_type'] == 1)
            $arr['background_img_path'] = $this->input->post("youtubeurl");
          $id = array("content_id"=>14);
          $check = $this->Content_model->editArticles($id, $arr);
          $this->session->set_flashdata("success", "<label class='text-success'>Content updated successfully.</label>");
          redirect("Content/homeSection/$args");
        }
      }
      else{
        redirct(base_url("Admin"));
      }
    }

    function UpdateSecton6($args){
      if(!empty($args)){
        $arr = array("content"=>$this->input->post("template1"), "section"=>$this->input->post("title"));        
        if(!empty($_FILES['secimg']['name'])){
          $random = $this->generateRandomString(10);
          $ext = pathinfo($_FILES['secimg']['name'], PATHINFO_EXTENSION);        
          $file_name = $random.".".$ext;
          $dirpath = './././assets/img/'.$file_name;
          if($ext == "png" ){
            if(move_uploaded_file($_FILES['secimg']['tmp_name'], $dirpath)){
                $arr['uploaded_file_path'] = $file_name;
            }
          }
          else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only png file</label>"));
            exit;
          }
        }
        if($args == 1){ // id 15
          $id = array("content_id"=>15);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);                    
        }
        else if($args == 2){
          $id = array("content_id"=>16);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        else if($args == 3){
          $id = array("content_id"=>17);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        else if($args == 4){
          $id = array("content_id"=>18);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        else if($args == 5){
          $id = array("content_id"=>19);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        else if($args == 6){
          $id = array("content_id"=>20);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        // if($check){
          echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Sub-section updated successfully.</label>"));
        // }
        // else{
        //   echo json_encode(array("status"=>200, "msg"=>"<label class='text-danger'>Sub-section not updated.</label>"));
        // }
      }
      else{
        echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));
      }
    }

    function UpdateSectonBackgroundImage($args){
      if(!empty($args)){
        if($args == 7){
          $arr = array("content"=>$this->input->post("template1"), "section"=>$this->input->post("title"));        
          if(!empty($_FILES['secimg']['name'])){
            $random = $this->generateRandomString(10);
            $ext = pathinfo($_FILES['secimg']['name'], PATHINFO_EXTENSION);        
            $file_name = $random.".".$ext;
            $dirpath = './././assets/img/'.$file_name;
            if($ext == "jpg" || $ext == "JPG" || $ext == "jpeg" || $ext == "jpeg"){
              if(move_uploaded_file($_FILES['secimg']['tmp_name'], $dirpath)){
                  $arr['uploaded_file_path'] = $file_name;                  
                  $id = array("content_id"=>25);
                  $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
                  if(!empty($arr['uploaded_file_path'])){
                    if(!empty($query['uploaded_file_path'])){
                      $dirpath = './././assets/img/'.$query['uploaded_file_path'];
                      unlink($dirpath);
                    }
                  }
                  $check = $this->Content_model->editArticles($id, $arr);
                  echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Background image uploaded successfully.</label>"));
              }
            }
            else{
              echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only jpg image</label>"));
              exit;
            }
          }
          else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select an image to upload.</label>"));
          }
        }
        else{
          echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));
        }
      }
      else{
        echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));
      }
    }

    function UpdateSecton3($args){
      if(!empty($args)){
        $arr = array("content"=>$this->input->post("template1"), "section"=>$this->input->post("title"));        
        if(!empty($_FILES['secimg']['name'])){
          $random = $this->generateRandomString(10);
          $ext = pathinfo($_FILES['secimg']['name'], PATHINFO_EXTENSION);        
          $file_name = $random.".".$ext;
          $dirpath = './././assets/img/'.$file_name;
          if($ext == "png" || $ext == "jpg" || $ext == "gif" || $ext == "jpeg" || $ext == "JPEG" ||$ext == "PNG" || $ext == "GIF" || $ext == "JPG"){
            if(move_uploaded_file($_FILES['secimg']['tmp_name'], $dirpath)){
                $arr['uploaded_file_path'] = $file_name;
            }
          }
          else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only png|jpg|gif|jpeg files</label>"));
            exit;
          }
        }
        if($args == 1){ // id 15
          $id = array("content_id"=>21);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);                    
        }
        else if($args == 2){
          $id = array("content_id"=>22);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        else if($args == 3){
          $id = array("content_id"=>23);
          $query = $this->Content_model->getRows(array("conditions"=>$id, "returnType"=>'single'));          
          if(!empty($arr['uploaded_file_path'])){
            if(!empty($query['uploaded_file_path'])){
              $dirpath = './././assets/img/'.$query['uploaded_file_path'];
              unlink($dirpath);
            }
          }
          $check = $this->Content_model->editArticles($id, $arr);
        }
        // if($check){
          echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Sub-section updated successfully.</label>"));
        // }
        // else{
        //   echo json_encode(array("status"=>200, "msg"=>"<label class='text-danger'>Sub-section not updated.</label>"));
        // }
      }
      else{
        echo json_encode(array("status"=>1, "msg"=>"Invalid Access"));
      }
    }

    function updateSocialLinks(){
      $facebook = $this->input->post("facebook");
      $twitter = $this->input->post("twitter");
      $instagram = $this->input->post("instagram");
      $google_plus = $this->input->post("google_plus");
      $pinterest = $this->input->post("pinterest");
      if(!empty($facebook) && !empty($twitter) && !empty($instagram)){
        $this->Content_model->updateSocialLinks(array("sc_links"=>$facebook), array("sc_id"=>1));
        $this->Content_model->updateSocialLinks(array("sc_links"=>$twitter), array("sc_id"=>2));
        $this->Content_model->updateSocialLinks(array("sc_links"=>$instagram), array("sc_id"=>3));
        $this->Content_model->updateSocialLinks(array("sc_links"=>$google_plus), array("sc_id"=>4));
        $this->Content_model->updateSocialLinks(array("sc_links"=>$pinterest), array("sc_id"=>5));
        echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Links updated successfully.</label>"));
      }
      else{
        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Required parameters missing</label>"));
      }
    }

    function userDashboardMessoptionssideBar(){
      $data = array();
      header("Access-Control-Allow-Origin: *");
      $data['listing'] = $this->Content_model->getContentRows(array("ft_id"=>27));
      $data['page'] = 27;
      $data['debitimages'] = $this->Content_model->getDebitCartRows(array("sorting"=>array("dbt_display_order"=>"ASC")));
      $this->template->set('title', 'CMS');
      $this->template->load('admin_layout', 'contents' , 'userdashboardsidesection', $data);
    }

    function uploadDebitCardImage(){
      if(!empty($_FILES['ban_file']['name']) && !empty($this->input->post("imglink"))){
        $random = $this->generateRandomString(10);
        $ext = pathinfo($_FILES['ban_file']['name'], PATHINFO_EXTENSION);        
        $file_name = $random.".".$ext;
        $dirpath = './././assets/img/debitcardimg/'.$file_name;
        if($ext == "png" || $ext == "jpg" || $ext == "gif" || $ext == "jpeg" || $ext == "JPEG" ||$ext == "PNG" || $ext == "GIF" || $ext == "JPG"){
          if(move_uploaded_file($_FILES['ban_file']['tmp_name'], $dirpath)){
            $para = array("dbt_image_path"=>$file_name, "dbt_image_link"=>$this->input->post("imglink"), "dbt_display_order"=>$this->input->post("order"), "dbt_status"=>1);
              $check = $this->Content_model->insertDebitCardTable(1, $para);
              if($check){
                $cont = "<label class='text-success'>Details added successfully.</label>";
                echo json_encode(array("status"=>200, "msg"=>$cont));
              }
              else{
                $cont = "<label class='text-danger'>Details not added, please try after some time.</label>";
                echo json_encode(array("status"=>1, "msg"=>$cont));
              }
          }
        }
        else{
          echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Please select only png|jpg|gif|jpeg files</label>"));
          exit;
        }
      }
      else{
        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Invalid Access</label>"));
      }
    }

    function debitCardStatusUpdate(){
      $id = $this->input->post("id");
      if(!empty($id)){
        $con['conditions'] = array("dbt_id"=>$id);
        $check = $this->Content_model->getDebitCartRows($con);
        if(count($check) > 0){
          $array = array("dbt_status"=>$this->input->post("mode"));
          $opr = 2; //Update mode
          $check = $this->Content_model->insertDebitCardTable($opr, $array, array("dbt_id"=>$id));
          if($check)
            echo json_encode(array("status"=>200));
        }
        else{
          echo json_encode(array("status"=>1));
        }
      }
      else{
        echo json_encode(array("status"=>1));
      }
    }

    function removeDebitCard(){
      $id = $this->input->post("id");
      if(!empty($id)){        
        $check = $this->Content_model->getDebitCartRows(array("dbt_id"=>$id));
        if(count($check) > 0){
          $existingpic = $check['dbt_image_path'];
          if(file_exists("./././assets/img/debitcardimg/$existingpic")){
            unlink("./././assets/img/debitcardimg/$existingpic");
          }
          $check = $this->Content_model->removeDebitCard(array("dbt_id"=>$id));
          if($check)
            echo json_encode(array("status"=>200));
        }
        else{
          echo json_encode(array("status"=>1));
        }
      }
      else{
        echo json_encode(array("status"=>1));
      }
    }

    public function generateRandomString($length = 12) {
        // $length = 12;
      $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $charactersLength = strlen($characters);
      $randomString = '';
      for ($i = 0; $i < $length; $i++) {
          $randomString .= $characters[rand(0, $charactersLength - 1)];
      }
      return $randomString;
    }


    public function setSearchDistanceLimit()
    {

      $data= array();
      $data["status"] ="";
      if($this->input->post('limitSubmit'))
       {
        $lmt = $this->input->post('distancelimit');
        if(!empty($lmt)){
        $this->db->update("otd_options", array("option_value"=>$lmt), array("option_name"=>'distance'));
        if($this->db->affected_rows() > 0)
        {
        $data["status"] = "Ok";
        }
        else
        {
          $data["status"] = "err";
        }
      }
    }
$dist = $this->Content_model->getDistanceLimit();
$data['distance'] = $dist['option_value'];
header("Access-Control-Allow-Origin: *");
$this->template->set('title', 'Content Management');
$this->template->load('admin_layout', 'contents' , 'options', $data);
}


  function googleAdScript(){
    $data = array();
    $data['heading'] = "Google Advertisement Script";
    $con['conditions'] = array("content_id"=>26);
    $data['details'] = $this->Content_model->getRows($con);    
    header("Access-Control-Allow-Origin: *");
    $this->template->set('title', 'Advertisement Management');
    $this->template->load('admin_layout', 'contents' , 'googleadd', $data);
  }

  function index(){
    $data = array();
    $con['conditions'] = array("add_status!="=>3);
    $data['details'] = $this->Content_model->getAddRows($con);
    if(empty($data['details']))
      $data['details'] = array();
    header("Access-Control-Allow-Origin: *");
    $this->template->set('title', 'Country Management');
    $this->template->load('admin_layout', 'contents' , 'addlisting', $data);
  }

  function addCountry(){
    $this->form_validation->set_rules("name", "link", "required", array("required"=>"Please enter %s"));
    //$this->form_validation->set_rules("display_order", "display order", "required", array("required"=>"Please select %s"));
    //$this->form_validation->set_rules("status", "status", "required", array("required"=>"Please select %s"));
    if($this->form_validation->run() == FALSE){
      $data = array();
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Country Management');
      $this->template->load('admin_layout', 'contents' , 'addadvertisement', $data);  
    }else{

      //echo "test";die;
      $para = array(
        //"add_url"=>$this->input->post("link"),
                    "name"=>$this->input->post("name"),
                    "arabic"=>$this->input->post("arabic")
                    //"add_status"=>$this->input->post("status")
                    );
      /*if(!empty($_FILES['ad_image']['name'])){
        $random = $this->generateRandomString(10);
        $ext = pathinfo($_FILES['ad_image']['name'], PATHINFO_EXTENSION);        
        $file_name = $random.".".$ext;
        $dirpath = './././assets/img/advertise/'.$file_name;
        if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "gif" || $ext == "GIF" ){
          if(move_uploaded_file($_FILES['ad_image']['tmp_name'], $dirpath)){
            $para['add_img_path'] = $file_name;
          }
        }
        else{
          echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Please select only png|jpg|gif file</label>"));
          exit;
        }
      }     */ 
      //print_r("<pre/>");
      //print_r($para);die;
      $check = $this->Content_model->insertAddTable(1, $para);
      if($check){
        echo json_encode(array("status"=>200, "type"=>1, "msg"=>"<label class='text-success'>Country added successfully.</label>"));
      }else{
        echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Country not added, Please try again.</label>"));      
      }
    }    
  }

  function editCountry($args = NULL){

    //echo "test";die;
    $this->form_validation->set_rules("name", "link", "required", array("required"=>"Please enter %s"));
    //$this->form_validation->set_rules("display_order", "display order", "required", array("required"=>"Please select %s"));
    //$this->form_validation->set_rules("status", "status", "required", array("required"=>"Please select %s"));
    if($this->form_validation->run() == FALSE){
      $data = array();
      $con['conditions'] = array("id="=>$args);
      $data['details'] = $this->Content_model->getAddRows($con);   

      //echo $args;die;

      //print_r("<pre/>");
      //print_r($data['details']);
      //die;   
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Country Management');
      $this->template->load('admin_layout', 'contents' , 'editadvertisement', $data);  
    }else{
      $para = array(
                   //"add_url"=>$this->input->post("link"),
                    "name"=>$this->input->post("name"),
                    //"add_status"=>$this->input->post("status")
                    );
      // if(!empty($_FILES['ad_image']['name'])){
      //   $random = $this->generateRandomString(10);
      //   $ext = pathinfo($_FILES['ad_image']['name'], PATHINFO_EXTENSION);        
      //   $file_name = $random.".".$ext;
      //   $dirpath = './././assets/img/advertise/'.$file_name;
      //   if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "gif" || $ext == "GIF" ){          
      //     if(move_uploaded_file($_FILES['ad_image']['tmp_name'], $dirpath)){
      //       $query = $this->Content_model->getAddRows(array("add_id"=>$this->input->post("addid")));              
      //       if(!empty($query['add_img_path'])){
      //         unlink('./././assets/img/advertise/'.$query['add_img_path']);
      //       }
      //       $para['add_img_path'] = $file_name;
      //     }
      //   }
      //   else{
      //     echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Please select only png|jpg|gif file</label>"));
      //     exit;
      //   }
      // }
      $check = $this->Content_model->insertAddTable(2, $para, array("id"=>$this->input->post("id")));
      if($check){
        echo json_encode(array("status"=>200, "type"=>1, "msg"=>"<label class='text-success'>country update successfully.</label>"));
      }else{
        echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Advertisement not update, Please try again.</label>"));      
      }
    }    
  }

  /*function editAdervertisement($args = NULL){
    $this->form_validation->set_rules("link", "link", "required", array("required"=>"Please enter %s"));
    $this->form_validation->set_rules("display_order", "display order", "required", array("required"=>"Please select %s"));
    $this->form_validation->set_rules("status", "status", "required", array("required"=>"Please select %s"));
    if($this->form_validation->run() == FALSE){
      $data = array();
      $data['details'] = $this->Content_model->getAddRows(array("add_id"=>$args));      
      header("Access-Control-Allow-Origin: *");
      $this->template->set('title', 'Manual Advertisement Management');
      $this->template->load('admin_layout', 'contents' , 'editadvertisement', $data);  
    }else{
      $para = array("add_url"=>$this->input->post("link"),
                    "add_display"=>$this->input->post("display_order"),
                    "add_status"=>$this->input->post("status")
                    );
      if(!empty($_FILES['ad_image']['name'])){
        $random = $this->generateRandomString(10);
        $ext = pathinfo($_FILES['ad_image']['name'], PATHINFO_EXTENSION);        
        $file_name = $random.".".$ext;
        $dirpath = './././assets/img/advertise/'.$file_name;
        if($ext == "png" || $ext == "PNG" || $ext == "jpg" || $ext == "JPG" || $ext == "gif" || $ext == "GIF" ){          
          if(move_uploaded_file($_FILES['ad_image']['tmp_name'], $dirpath)){
            $query = $this->Content_model->getAddRows(array("add_id"=>$this->input->post("addid")));              
            if(!empty($query['add_img_path'])){
              unlink('./././assets/img/advertise/'.$query['add_img_path']);
            }
            $para['add_img_path'] = $file_name;
          }
        }
        else{
          echo json_encode(array("status"=>1, "type"=>2, "msg"=>"<label class='text-danger'>Please select only png|jpg|gif file</label>"));
          exit;
        }
      }
      $check = $this->Content_model->insertAddTable(2, $para, array("add_id"=>$this->input->post("addid")));
      if($check){
        echo json_encode(array("status"=>200, "type"=>1, "msg"=>"<label class='text-success'>Advertisement update successfully.</label>"));
      }else{
        echo json_encode(array("status"=>1, "type"=>1, "msg"=>"<label class='text-danger'>Advertisement not update, Please try again.</label>"));      
      }
    }    
  }*/

  function trashaddListing(){
    $data = array();
    $con["sorting"] = array("add_id"=>"DESC");
    $con['conditions'] = array("add_status"=>3);
    $data['details'] = $this->Content_model->getAddRows($con);
    if(empty($data['details']))
      $data['details'] = array();
    header("Access-Control-Allow-Origin: *");
    $this->template->set('title', 'Trash Advertisement Management');
    $this->template->load('admin_layout', 'contents' , 'trashaddlisting', $data);
  }

  function trashAdvertisement(){

    //echo "test";die;
    $id = $this->input->post("id");
    $status = $this->input->post("status");
    $check = $this->Content_model->insertAddTable(2, array("add_status"=>$status), array("id"=>$id));
    if($check){
      echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Advertisement updated successfully.</label>"));
    }else{
      echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Advertisement not updated, Please try again.</label>"));
    }
  }

  function contentManagement($args = 1){
    $data = array();
    $data['listing'] = $this->Content_model->getContentRows(array("ft_id"=>$args));
    $data['page'] = $args;
    if($args == 2)
      $content = "sitetext/login";
    else if($args == 3)
      $content = "sitetext/register";
    else if($args == 4)
      $content = "sitetext/forget";
    else if($args == 5)
      $content = "sitetext/header";
    else if($args == 6)
      $content = "sitetext/prosignup";
    else if($args == 7)
      $content = "sitetext/persignup";
    else if($args == 8)
      $content = "sitetext/loginpage";
    else if($args == 9)
      $content = "sitetext/home";
    else if($args == 10)
      $content = "sitetext/busprofile";
    else if($args == 11)
      $content = "sitetext/claimbusiness";
    else if($args == 12)
      $content = "sitetext/businesslisting";
    else if($args == 13)
      $content = "sitetext/review";
    else if($args == 14)
      $content = "sitetext/newsletter";
    else if($args == 15)
      $content = "sitetext/dashboardtext";
    else if($args == 16)
      $content = "sitetext/recomenduser";
    else if($args == 17)
      $content = "sitetext/follow";
    else if($args == 18)
      $content = "sitetext/message";
    else if($args == 19)
      $content = "sitetext/eventdetails";
    else if($args == 20)
      $content = "sitetext/newnumber";
    else if($args == 21)
      $content = "sitetext/registrationconfirm";
    else if($args == 23)
      $content = "sitetext/nwsthnkyou";
    else if($args == 24)
      $content = "sitetext/clmthnkyou";
    else if($args == 25)
      $content = "sitetext/paymentthankyou";
    else if($args == 26)
      $content = "sitetext/formoptionselectionthankyou";
    else if($args == 28)
      $content = "sitetext/businessregconfirm";
    else if($args == 29)
      $content = "sitetext/forgetThankyou";
    else if($args == 30)
      $content = "sitetext/cartpage";
    else if($args == 31)
      $content = "sitetext/paymentmethod";
    else if($args == 32)
      $content = "sitetext/slimpayuserinfo";
    else if($args == 33)
      $content = "sitetext/slimpayconfirmmandate";
    else if($args == 34)
      $content = "sitetext/slimpaythankyou";
    else if($args == 35)
      $content = "sitetext/slimpaytransactionfailed";
    else
      $content = "sitetext/footer";
    header("Access-Control-Allow-Origin: *");
    $this->template->set('title', 'CMS / Translations');
    $this->template->load('admin_layout', 'contents' , $content, $data);
  }

  function emailSettings(){
    $data['listing'] = $this->Content_model->getContentRows(array("ft_id"=>22));
    $data['page'] = 22;
    header("Access-Control-Allow-Origin: *");
    $this->template->set('title', 'Email Settings');
    $this->template->load('admin_layout', 'contents' , "sitetext/emailsetting", $data);
  }

  function contentUpdate($args){
    if(!empty($args)){
      $check = $this->Content_model->contentUpdate($this->input->post(), array("ft_id"=>$args));
      echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Content updated successfully.</label>"));
    }else{
      echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Required parameters missing.</label>"));
    }
  }



}