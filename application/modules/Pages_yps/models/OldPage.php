<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Page extends CI_Model{

   public function __construct() {
        $this->userTbl = 'users';
        $this->userTbl1 = 'otd_page_contents';
        $this->userTblSubs = 'subscribers';
        $this->testimonial = "otd_testimonials";
        $this->contentTbl = "site_contents";
        $this->catTbl = "otd_business_category";
        $this->bannerTbl = "otd_home_banner";
    }

     /*
    * get rows from the page table
    */
   public function getSearchRows($params = array())
    {
        $sql_main = "select * from vv_users_list where user_type = 'Professional' ";
        $sql="";
        if(array_key_exists("conditions",$params))
        {
            $sql =" and (";
            foreach ($params['conditions'] as $key => $value) 
            {
                $sql .= $key. " = '". $value."' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
        }

        if(array_key_exists("extraFilter",$params))
        {
            $sql =" and (";
            foreach ($params['extraFilter'] as $key => $value) 
            {
                $sql .= " extra_filter = '". $value."' Or " ;
            
            }
            $sql = rtrim($sql,"Or ");
            $sql .= ") ";
        }
      

         if(array_key_exists("keyword",$params))
         {
            if($params['keyword'] != "")
            {
               if($sql != "")
               {
                 $sql .=" Or ( ";
                 $fields = $this->db->list_fields('vv_users_list');
                    foreach ($fields as $field)
                    {
                        $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                    }
                    $sql = rtrim($sql,"Or ");
                    $sql .= ") ";

                }
                else
                {
                $sql =" And (";
                $fields = $this->db->list_fields('vv_users_list');
                foreach ($fields as $field)
                    {
                      $sql .= $field. " like '%". $params['keyword']. "%' Or "; 
         
                     }
                $sql = rtrim($sql,"Or ");
                $sql .= ") ";
                }
            }
        }
  
            $fullQuery = $sql_main. $sql;

            $query = $this->db->query($fullQuery);
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }

echo $this->db->last_query();
echo "<br><br>SQL: ".$fullQuery;
//die();
        return $result;
    }


    /*
    * get rows from the page table
    */
    function getPageRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->userTbl1);

       
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
              
            }
        }
        

        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }


        if(array_key_exists("content_id",$params)){
            $this->db->where('content_id',$params['content_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Get testimonials
    */
    function getTestimonials(){
        $query = $this->db->order_by("tst_date", "DESC")->get_where($this->testimonial, array("tst_status"=>1));
        return $query->result_array();
    }

    /**
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){
       $address = str_replace(" ", "+", $address);
       $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
       $json = json_decode($json);
       if((isset($json)) && ($json->{'status'} == "OK")){
        $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
        $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
       }else{
         $lat = -25.274398; 
         $long = 133.775136; 
       }       
       $response = array('lat'=>$lat,'lng'=>$long);
       return $response;
    }

function getFollowersCount($user_id)
{
    $sql = "select followed_user_id, count(*) as total_followers from otd_business_follower where followed_user_id = ".$user_id. " group by followed_user_id";
           $query = $this->db->query($sql);

           return $query->num_rows();
}

function getBusinessCategories(){

$this->db->select('*');
$this->db->from('otd_business_category');
$this->db->where("cat_status", 1);
$this->db->where("type", 1);
$this->db->order_by("cat_name", "ASC");
$query = $this->db->get();

//        $query = $this->db->order_by("cat_name", "ASC")->get_where("otd_business_category", array("cat_status"=>1));
        return $query;
    }

function getExtraFilters(){

$this->db->select('*');
$this->db->from('otd_business_category');
$this->db->where("cat_status", 1);
$this->db->where("type", 2);
$this->db->order_by("cat_name", "ASC");
$query = $this->db->get();
return $query;
}


     function getTopRatedCompanies()
    {
 
//  $sql = "SELECT * FROM `users` where toprated = 1 and status = 1 and user_type = 'Professional' ORDER BY `created` DESC limit 2";

 /*$sql = "SELECT u.*, c.counts FROM `users`u left join vvfollower_counts c on 
 c.followed_user_id = u.user_id 
 where u.toprated = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 3";*/     

 $sql = "SELECT u.* FROM `vv_users_list`u  where u.toprated = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 3";

       $query = $this->db->query($sql);

return $query->result_array();


    }

     function getPublishOnHomeCompanies()
    {

  //$sql = "SELECT * FROM `users` where publishonhome = 1 and status = 1 and user_type = 'Professional' ORDER BY `created` DESC limit 4";
    

    /*$sql = "SELECT u.*, c.counts FROM `users`u left join vvfollower_counts c on c.followed_user_id = u.user_id where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 7";   */

 $sql = "SELECT u.* FROM `vv_users_list`u  where u.publishonhome = 1 and u.status = 1 and u.user_type = 'Professional' ORDER BY rand() limit 7";


       $query = $this->db->query($sql);

return $query->result_array();


    }


    function getArticles($args){
        $query = $this->db->get_where($this->contentTbl, array("content_id"=>$args));
        return $query->row_array();
    }

    /*
    * get categories lists
    */
    /*
  * get rows from the testimonail table
  */
    function getCategoryRows($params = array()){

        $this->db->select('*');
        $this->db->from($this->catTbl);  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("cat_id",$params)){
            $this->db->where('cat_id',$params['cat_id']);
            $query = $this->db->get();
            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function getBannerImages($params = array()){
        $this->db->select('*');
        $this->db->from($this->bannerTbl);
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }

        if(array_key_exists("bn_id",$params)){
            $this->db->where('bn_id',$params['content_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit

            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);
                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

}
?>