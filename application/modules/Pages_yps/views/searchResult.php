 

 <style>
       #map {
        height: 100%;
        width: 100%;
        float: left;
        border: 1px solid #ffc;
      }
a:hover
{

  text-decoration-line: none;
}


    </style>

<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/blog_rating.css"/>
 <?php
 $formcnt = $this->Page->getTextContent(12);
 ?>
<div class="main-content">
  <div class="main-reg-content">
    <section class="filter-sec">
      <div class="container">
        <form name="searchForm" id="searchForm" action="<?php echo base_url("Pages/setSearchDelimiters"); ?>" method="post">
          <div class="main-srch-form-sec">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
                  <input id="autocomplete" type="text" name="user_address" value="<?php echo $this->session->userdata("search_location");  ?>" class="form-control add-form" onFocus="geolocate()" class="form-control" placeholder="<?php echo $formcnt['label1']; ?>">
                  <input id="currentlatlng" name="currentlatlng" value="" type="hidden" />
                  <input id="searchedcity" name="searchedcity" value="<?php echo $searchedcity; ?>" type="hidden" />
                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
                  <input type="text" name="business_category1" id="business_category1" value="<?php echo $this->session->userdata("search_category");  ?>" 
                  list='categories1'
                  name='user_categories'
                  id="business_category"
                  class="form-control cat-form" placeholder="<?php echo $formcnt['label2']; ?>">
                  <datalist id="categories1">
                    <?php
                       foreach($business_categories->result() as $cate){
                      ?>
                      <option value="<?php echo $cate->cat_name; ?>"><?php echo $cate->cat_name; ?></option>
                      <?php
                    }
                    ?>                        
                  </datalist>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
                    <input type="text" class="form-control" value="<?php echo $this->session->userdata("search_keyword");  ?>" name="keyword" placeholder="<?php echo $formcnt['label3']; ?>">
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <a class="btn btn-default extra-filter" href="javascript:void()"><?php echo $formcnt['label4']; ?></a>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <button class="btn btn-default main-sbmit" name="searchSubmit" value="Search" type="submit"><?php echo $formcnt['label6']; ?></button>
              </div>
            </div>
          </div>
          <div class="extra-filter-sec">
            <div class="row">
              <div class="col-md-12">
                <div class="etra-fil-head">
                  <h3><?php echo $formcnt['label5']; ?></h3>
                </div>
                <div class="col-md-4">
                  <div class="hm-ctgry-hed">
                  	<h4>Services</h4>
                  </div>
                  <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark" style="max-height: 220px!important; min-height: 220px!important; background: #212121; padding: 15px; height: auto; overflow: hidden;">                
                    <div class="ctglrty-list">
                      <?php
                      foreach($service_filter as $srsflt){
                        $chk="";
                        if(!empty($extraFilter)){
                          foreach ($extraFilter as $key => $value) {
                            if($srsflt['cat_name']==$value)
                            {
                              $chk="checked";
                            }
                          }
                        }
                      ?>
                  	    <div class="checkbox-main <?php echo $chk; ?>">
                            <div class="squaredThree">
                              <input type="checkbox" <?php echo $chk; ?> id="<?php echo $srsflt['cat_id']; ?>" value="<?php echo $srsflt['cat_name']; ?>" name="extra_fltr[]">
                              <label class="chk-box-label" for="<?php echo $srsflt['cat_id']; ?>"></label><span> <?php echo $srsflt['cat_name']; ?></span>                 
                            </div>
                        </div>
                      <?php
                      }
                      ?>
                    </div>
                	</div>
                </div>
                <div class="col-md-4">
                  <div class="hm-ctgry-hed">
                  	<h4>Products</h4>
                  </div>
                  <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark" style="max-height: 220px!important; min-height: 220px!important; background: #212121; padding: 15px; height: auto; overflow: hidden;">                
                    <div class="ctglrty-list">
                	    <?php
                      foreach($product_filter as $prdflt){
                        $chk="";
                        if(!empty($extraFilter)){
                          foreach ($extraFilter as $key => $value) {
                            if($prdflt['cat_name']==$value)
                            {
                              $chk="checked";
                            }
                          }
                        }
                      ?>
                        <div class="checkbox-main <?php echo $chk; ?>">
                            <div class="squaredThree">
                              <input type="checkbox" <?php echo $chk; ?> id="<?php echo $prdflt['cat_id']; ?>" value="<?php echo $prdflt['cat_name']; ?>" name="extra_fltr[]">
                              <label class="chk-box-label" for="<?php echo $prdflt['cat_id']; ?>"></label><span> <?php echo $prdflt['cat_name']; ?></span>                 
                            </div>
                        </div>
                      <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="hm-ctgry-hed">
                  	<h4>Others</h4>
                  </div>
                  <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark" style="max-height: 220px!important; min-height: 220px!important; background: #212121; padding: 15px; height: auto; overflow: hidden;">                
                    <div class="ctglrty-list">
                  	  <?php
                      foreach($other_filter as $othflt){
                        $chk="";
                        if(!empty($extraFilter)){
                          foreach ($extraFilter as $key => $value) {
                            if($othflt['cat_name']==$value)
                            {
                              $chk="checked";
                            }
                          }
                        }
                      ?>
                        <div class="checkbox-main <?php echo $chk; ?>">
                            <div class="squaredThree">
                              <input type="checkbox" <?php echo $chk; ?> id="<?php echo $othflt['cat_id']; ?>" value="<?php echo $othflt['cat_name']; ?>" name="extra_fltr[]" >
                              <label class="chk-box-label" for="<?php echo $othflt['cat_id']; ?>"></label><span> <?php echo $othflt['cat_name']; ?></span>                 
                            </div>
                        </div>
                      <?php
                      }
                      ?>
                    </div>
                	</div>
                </div>
                
                <?php /*?><div class="efs-sub mCustomScrollbar" data-mcs-theme="dark" style="max-height: 220px!important;background: #212121; padding: 15px; height: auto; overflow: hidden;">
                
                	
                                   
                   <?php
               
                    foreach($busienss_filters->result() as $cate){
                      $chk="";
                      if(!empty($extraFilter)){
                        foreach ($extraFilter as $key => $value) {
                          if($cate->cat_name==$value)
                          {
                            $chk="checked";
                          }
                        }
                      }
                      ?>
                      
                      <div class="checkbox-main">
                        <div class="squaredThree">
                          <input type="checkbox" <?php echo $chk; ?> id="<?php echo $cate->cat_id; ?>" value="<?php echo $cate->cat_name; ?>" name="extra_fltr[]" />
                          <label class="chk-box-label" for="<?php echo $cate->cat_id; ?>"></label><span><?php echo $cate->cat_name; ?></span>                 
                        </div>
                      </div>
                      
                    <?php
                    }
                    ?>
                </div><?php */?>
              </div>

            </div>
          </div>
        </form>
      </div>
    </section>
    <?php
    $mapsize = get_cookie("mapsize");
    if($mapsize == 1){
      $mapsize = "for-map-full";      
    }else{
      $mapsize = "for-map-hide";
    }
    ?>
    <section class="main-map-sec <?php echo $mapsize; ?>">
      <div class="container">
        <div class="row">

          <div class="main-listing-sec">
          	<div class="curnt-filter">

            <?php
                      foreach($service_filter as $srsflt){
                        $chk="";
                        if(!empty($extraFilter)){
                          foreach ($extraFilter as $key => $value) {
                            if($srsflt['cat_name']==$value)
                            {
                              $chk="checked";
                              ?>
                              <div class="btn btn_close "id="<?php echo $srsflt['cat_id']; ?>"><?php echo $srsflt['cat_name']; ?>
                                <span class="close-filter">×</span>
                              </div>
                              <?php
                            }
                          }
                        }
                      ?>
                  	  
                       
                      <?php
                      }
                      ?>

<?php
                      foreach($product_filter as $psrsflt){
                        $chk="";
                        if(!empty($extraFilter)){
                          foreach ($extraFilter as $key => $value) {
                            if($psrsflt['cat_name']==$value)
                            {
                              $chk="checked";
                              ?>
                              <div class="btn btn_close" id="<?php echo $psrsflt['cat_id']; ?>"><?php echo $psrsflt['cat_name']; ?>
                                  <span class="close-filter">×</span>
                              </div>
                              <?php
                            }
                          }
                        }
                      ?>
                  	  
                       
                      <?php
                      }
                      ?>

<?php
                      foreach($other_filter as $osrsflt){
                        $chk="";
                        if(!empty($extraFilter)){
                          foreach ($extraFilter as $key => $value) {
                            if($osrsflt['cat_name']==$value)
                            {
                              $chk="checked";
                              ?>
                              <div class="btn btn_close" id="<?php echo $osrsflt['cat_id']; ?>"><?php echo $osrsflt['cat_name']; ?>
                                  <span class="close-filter">×</span>
                              </div>
                              <?php
                            }
                          }
                        }
                      ?>
                  	  
                       
                      <?php
                      }
                      ?>

            </div>
            <br>
            <?php 
            if(!empty($ALaUNE)){
              ?>
              <h3><?php echo $formcnt['label7']; ?></h3>
              <div id="owl-demo-pro-head" class="owl-carousel owl-carousel1">
              <?php
                foreach ($ALaUNE as $rows) {                  
                  ?>
                  <a href="<?php echo base_url("Users/user_profile/").$rows['user_id']; ?>" target="_blank">
                    <div class="item">
                      <div class="popu-con-sub">
                        <div class="popu-img">  
                        <!-- <img src="<?php echo base_url();?>assets/img/popu.jpg">-->
                          <!--<img src="<?php //echo base_url("assets/profile_pics/").$rows['user_profile_pic']; ?>"> -->
                          <?php
                            profile_pic($rows['user_profile_pic'], $rows['CategoryId']);
                          ?>
                          <div class="popu-ovrly"> 
                            <h3><?php echo $rows['user_company_name']; ?></h3>                            
                          </div>
                        </div>
                        <div class="popu-txt">
                          <div class="popu-loca"> 
                            <i class="fa fa-map-marker"></i><span><?php echo $rows['user_company_address']; ?></span> 
                          </div>
                          <div class="like-comm">
                            <div class="popu-liked"><i class="fa fa-thumbs-up" aria-hidden="true"></i><?php echo $rows['total_recommand']; ?></div>
                            <div class="popu-comm"><i class="fa fa-comments" aria-hidden="true"></i><?php echo $rows['total_review']; ?> avi(s)</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </a>
                  <?php
                }
                ?>
              </div>
            <?php
            }
            ?>


            <?php 
            if(!empty($search_results))
            {
              $xid=1;
                echo "<h3>(".number_format($totalrecord).") ".$formcnt['label8']."</h3>"; 
                ?>
                <div class="search-pagination">
                  <?php echo $links; ?>                  
                </div>
                <br>
                <?php
              foreach ($search_results as $row) {
                $lnk="";
                $lnkclose = "</a>";
                if($this->session->userdata('isUserLoggedIn')){

                $lnk ='<a style="color:#000;" href="'.base_url()."Users/user_profile/".$row['user_id'].'" target="_blank">';
                }
                else
                {
                  //$lnk ='<a href="javascript:;" data-toggle="modal" data-target="#myModal">';
                  $lnk ='<a style="color:#000;" href="'.base_url()."Users/user_profile/".$row['user_id'].'" target="_blank">';
                }

                echo $lnk;
      	        $ovmous = $xid
                ?>
                <div class="listing-sub" onmouseout="out(<?php echo $ovmous;?>);" onmouseover="hover(<?php echo $ovmous;?>)" id="pin-<?php echo $xid++; ?>">
                  <div class="image-lis-sub">
				     <?php
                         profile_pic($row['user_profile_pic'],$row['cate_id']);

                     ?>
                  </div>
                  <div class="image-txt-sub">
                    <div class="img-txt-subhead">
                     <input type="hidden" name="blog_content_id_<?php echo $row['user_id']; ?>" id="blog_content_id<?php echo $row['user_id']; ?>" value="<?php echo $row['user_id']; ?>"/>
                      <h3><?php echo $row['user_company_name'] != ""? $row['user_company_name']: "Name not in DB"; ?></h3>
                     
                        <span class="rating-main">
                          <ul>

                            <?php
                            // $overall_vote_rate = $row['vote_count'];
                            if(!empty($row['totalvoter'])){
                              $overall_vote_rate = $row['totalvotesum'] / $row['totalvoter'];
                            }else{
                              $overall_vote_rate = 0;
                            }
                            $stars = '';
                              for ($i = 1; $i <= floor($overall_vote_rate); $i++) {
                                $stars .= ' <li><i class="fa fa-star"></i></li>';
                              }
                              echo $stars;
                              ?>
                           
                          </ul>
                        </span>
                    </div>
                    <p class="add-l"><i class="fa fa-map" aria-hidden="true"></i><?php echo $row['user_company_address'] != ""? $row['user_company_address']: "NA"; ?></p>
                    <p class="catgry-l"><span class="f-cat"><i class="fa fa-bars" aria-hidden="true"></i> Categorie(s): </span><span><?php echo $row['categories'] != ""? $row['categories']: "NA"; ?></span></p>
                    <p class="distance-l"><span class="f-cat"><i class="fa fa-map-marker"></i>Distance: </span><span><?php echo $row['distance']. ' KM'; ?></span></p>
                    <?php
                    echo $lnkclose;
                    ?>
                    <div class="dsply-num">
                      <?php
                      if($this->session->userdata('isUserLoggedIn')){
                      ?>
                        <!--  <a onclick="showphone('opnph_<?php echo $row['user_id']; ?>');" target="_blank" class="btn btn-default"><i class="fa fa-phone"></i><span id="txtopnph_<?php echo $row['user_id']; ?>">Afficher le numéro </span><span style="display:none" id="opnph_<?php echo $row['user_id']; ?>"><?php echo $row['user_phone']; ?></span></a> -->

                        <a style="color:#000;" href="<?php echo base_url().'Users/user_profile/'.$row['user_id'].'/openphone'; ?>" target="_blank"><i class="fa fa-phone"></i>Afficher le numéro </a>

                      <?php
                      }
                      else
                      {
                      ?>
                        <a style="color:#000;" href="<?php echo base_url().'Users/user_profile/'.$row['user_id']. '/openphonemodal'; ?>" target="_blank"><i class="fa fa-phone"></i>Afficher le numéro </a>
                        <!-- <a class="btn btn-default"  href="javascript:;" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i>Afficher le numéro</a> -->
                      <?php
                       }
                      ?>
                    </div>
                  </div>
                </div>
              <?php
              }
            }
            else
            {
              echo "<h3>".$formcnt['label9']."</h3>";
            }
            ?>
            <div class="search-pagination">
              <?php //print_r($search_results);
                echo $links;
              ?>
            </div>
          </div> 

          <div class="map-main">      
            <div id="map"></div>      
            <div id="map-full-but">
       		    <button type="button" class="btn btn-info btn-lg map-wrapper-name for-fulmp"><i class="fa fa-external-link"></i> <?php echo $formcnt['label10']; ?> </button>
              <button type="button" class="btn btn-info btn-lg map-wrapper-name for-srtmb"><i class="fa fa-external-link"></i> <?php echo $formcnt['label11']; ?> </button>
            </div>
          
            <div class="google-ad">
         
              <?php
             // echo $googleadd['content'];
              ?>
         
            </div>

            <div class="manual-add">              
              <?php
              if(!empty($manualadd)){
                ?>
                <ul class="">

                <?php
                foreach($manualadd as $add){
                  ?>
                  <li><a href="<?php echo $add['add_url']; ?>" target="_blank"><img src="<?php echo base_url("assets/img/advertise/").$add['add_img_path']; ?>" alt="Image not found"/></a></li>
                  <?php
                }
                ?>
                </ul>
                <?php
              }
              ?>
            </div>
          </div>     
        </div>
      </div>   
    </section>
  </div>
</div>

