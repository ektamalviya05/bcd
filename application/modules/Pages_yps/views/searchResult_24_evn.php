 <style>
       #map {
        height: 100%;
        width: 100%;
        float: left;
        border: 1px solid #ffc;
      }



    </style>

<link rel="stylesheet" href="<?php echo base_url();?>assets/css/jquery.rateyo.css">


<div class="main-content">
  <div class="main-reg-content">
    <section class="filter-sec">
      <div class="container">
        <form name="searchForm" action="<?php echo base_url("Pages/businessListing"); ?>" method="post">
          <div class="main-srch-form-sec">
            <div class="row">
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
        <input id="autocomplete" type="text" name="user_address" value="<?php echo $location;  ?>" class="form-control add-form" onFocus="geolocate()" class="form-control" placeholder="VILLE,CODE POSTAL">
         <input id="currentlatlng" name="currentlatlng" value="" type="hidden" />
          </input>

                </div>
              </div>
              <div class="col-md-3 col-sm-3 col-xs-4">
                <div class="form-group">
        <input type="text" name="business_category1" id="business_category1" value="<?php echo $category;  ?>" 
                  list='categories1'
                  name='user_categories'
                  id="business_category"
                  class="form-control cat-form" placeholder="Entrez une catégorie">
                  <datalist id="categories1">
                    <?php
                       foreach($business_categories->result() as $cate){
                      ?>
                      <option value="<?php echo $cate->cat_name; ?>"><?php echo $cate->cat_name; ?></option>
                      <?php
                    }
                    ?>                        
                  </datalist>
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-4">
                <div class="form-group">
        <input type="text" class="form-control" value="<?php echo $keyword;  ?>" name="keyword" placeholder="KEYWORD">
                </div>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <a class="btn btn-default extra-filter" href="javascript:void()">Extra Filters</a>
              </div>
              <div class="col-md-2 col-sm-2 col-xs-6">
                <button class="btn btn-default main-sbmit" name="searchSubmit" value="Search" type="submit">Search</button>
              </div>
            </div>
          </div>
          <div class="extra-filter-sec">
            <div class="row">
              <div class="col-md-12">
                <div class="etra-fil-head">
                  <h3>Select Extra Filters</h3>
                </div>
                <div class="efs-sub mCustomScrollbar" data-mcs-theme="dark" style="max-height: 220px!important;background: #212121; padding: 15px; height: auto; overflow: hidden;">
                                   
                   <?php
               
                    foreach($busienss_filters->result() as $cate){
                      $chk="";
                      foreach ($extraFilter as $key => $value) {
                       if($cate->cat_name==$value)
                       {
                        $chk="checked";
                       }
                      }
                      ?>
                  <div class="checkbox-main">
                    <div class="squaredThree">
                   <input type="checkbox" <?php echo $chk; ?> id="<?php echo $cate->cat_id; ?>" value="<?php echo $cate->cat_name; ?>" name="extra_fltr[]" />
                  <label class="chk-box-label" for="<?php echo $cate->cat_id; ?>"></label><span><?php echo $cate->cat_name; ?></span>
                 
                    </div>
                  </div>
                  <?php
                    }
                    ?> 

                </div>
              </div>

            </div>
          </div>
        </form>
      </div>
    </section>
    <section class="main-map-sec">
    <div class="container">
    <div class="row">
      <div class="main-listing-sec">
        
<?php 
if($record_found>0)
{
  $xid=0;
    echo "<h3>Total (".$record_found.") Record Found</h3>";
foreach ($search_results as $row) {
  ?>
        <div class="listing-sub" id="pin-<?php echo $xid++; ?>">
          <div class="image-lis-sub">
            <img src="<?php echo base_url("assets/profile_pics/").$row['user_profile_pic']; ?>">
          </div>
          <div class="image-txt-sub">
            <div class="img-txt-subhead">
              <h3><?php echo $row['user_company_name'] != ""? $row['user_company_name']: "Name not in DB"; ?></h3>
              <span class="rating-main">
                <ul>
                  
<?php
$numberstar = 2.5;
display_star($numberstar);
?>
                  <!--<li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>
                  <li><i class="fa fa-star"></i></li>-->
                </ul>
              </span>

            </div>
          
<div>
      <div id="rateYo1" style="margin: 10px auto"></div>
      <div class="counter" style="float: left;
                            font-weight: bold;
                            margin-left: 10px;
                            margin-top: 7px;"></div>
      <div style="clear: both"></div>
    </div>

    <div class="rateyo-readonly-widg"></div>

    <div class="rateyo"></div>
    <div class="rateyo"></div>
    <div class="rateyo"></div>
    <div class="rateyo"></div>
    <div class="rateyo"
         data-rateyo-rating="50%"
         data-rateyo-rtl="true"
         data-rateyo-spacing="10px"
         data-rateyo-rated-fill="#FF0000"
         data-rateyo-num-stars="10"></div>



            <p class="add-l"><i class="fa fa-map" aria-hidden="true"></i><?php echo $row['user_company_address'] != ""? $row['user_company_address']: "NA"; ?></p>
            <p class="catgry-l"><span class="f-cat"><i class="fa fa-bars" aria-hidden="true"></i> Categorie(s): </span><span><?php echo $row['categories'] != ""? $row['categories']: "NA"; ?></span></p>
            <p class="distance-l"><span class="f-cat"><i class="fa fa-map-marker"></i>Distance: </span><span><?php echo $row['distance']. ' KM'; ?></span></p>
            <div class="dsply-num">
<?php
  if($this->session->userdata('isUserLoggedIn')){

?>
              <a href="<?php echo base_url()."Users/user_profile/".$row['user_id'];  ?>" target="_blank" class="btn btn-default"><i class="fa fa-phone"></i>Afficher le numéro</a>
              <?php
            }
            else
            {
?>
<a class="btn btn-default"  href="javascript:;" data-toggle="modal" data-target="#myModal"><i class="fa fa-phone"></i>Afficher le numéro</a>
<?php
            }
            ?>
            </div>
          </div>
        </div>
      <?php

      # code...
}
}
else
{
  echo "<h3>No Record Found</h3>";
}
      ?>
        <?php //print_r($search_results); ?>       
      </div> 
      <div class="map-main">
     
     
   <!--   <div class="right_map_sce">   <iframe src="https://www.google.com/maps/embed?pb=!1m10!1m8!1m3!1d14722.96212982255!2d75.92889325!3d22.700701!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1496980678600" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
     
     </div> -->
     
     <!--  <div id="toggle-map-button">
      <a href="javascript:void(0)"><i class="fa fa-expand" aria-hidden="true"></i></a>
      </div>  
      -->
      
      <div id="map"></div>  
      
      <div id="map-wrapper">
      <a href="#" id="myfullmap"><i class="fa fa-external-link"></i> Full Map </a>
      </div>
      <div class="google-ad">
     
      <img src="<?php echo base_url();?>assets/img/addis.jpg">
     
      </div>
     
      </div>
     
     </div>
     </div>
         
    </section>
  </div>
</div>



 <script>



  function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }


  var allMyMarkers = [];
    var markers = [
     <?php 
   if(isset($businessMarker)){
  //  print_r($businessMarker);
    $c = count($businessMarker) - 1;
    $i = 0;
    foreach ($businessMarker as $businessL) {?>
    {
        "title": '<?php if(isset($businessL['user_company_address'])){echo $businessL['user_company_address'];}?>',
        "lat": '<?php if(isset($businessL['user_lat'])){echo $businessL['user_lat'];}?>',
        "lng": '<?php if(isset($businessL['user_long'])){echo $businessL['user_long'];}?>',
        "description": '<?php if(isset($businessL['user_company_name'])){echo $businessL['user_company_name'];}?>'
    }
   <?php if($i < $c)
   {echo ",";}?>
   <?php $i++;
    }}?> 
    ];





       function LoadMap() {
   

        var mapOptions = {
            center: new google.maps.LatLng(markers[1].lat, markers[1].lng),
            zoom:12,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("map"), mapOptions);
 
         var iconicon = {
              url: "http://localhost/hmvc_site/assets/img/map-marker-wyootwo.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
//var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
//var image = 'http://localhost/hmvc_site/assets/img/map-marker-wyootwo.png';
        //Create and open InfoWindow.
        var infoWindow = new google.maps.InfoWindow();
 
        for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: iconicon,
                id: i,
                title: data.title
            });
            allMyMarkers.push(marker);
 
            //Attach click event to the marker.
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
                    infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/>'+ data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);
        }


       $('.listing-sub').hover(function(){
                var selectedID = $(this).attr('id');
               //alert(selectedID);
                toggleBounce(selectedID);

               
        });
    }


function toggleBounce(selectedID) {
        var pinID = selectedID.split('-');
        //alert(pinID);
        // loop through our array & check with marker has same ID with the text
        for(var j=0;j<allMyMarkers.length;j++){
     
                if(allMyMarkers[j].id == pinID[1]){
                        // alert(allMyMarkers[j].id + "  " + pinID[1]);
                        if (allMyMarkers[j].getAnimation() != null) {
                                allMyMarkers[j].setAnimation(null);
                        } else {
                                allMyMarkers[j].setAnimation(google.maps.Animation.BOUNCE);
                                
                                var mapOptions = {
                                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                                zoom:12,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                                var map = new google.maps.Map(document.getElementById("map"), mapOptions);


                              var iconicon = {
              url: "http://localhost/hmvc_site/assets/img/map-marker-wyootwo.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
                             //   var image = 'http://localhost/hmvc_site/assets/img/map-marker.png';
                                    

                                //Create and open InfoWindow.
                                var infoWindow = new google.maps.InfoWindow();

                                for (var i = 0; i < markers.length; i++) {

                                   if(pinID[1] == i)
                                    {

                              iconicon = {
              url: "http://localhost/hmvc_site/assets/img/map-marker.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };

                                     // break;
                                    }
                                    else
                                    {
                                       iconicon = {
              url: "http://localhost/hmvc_site/assets/img/map-marker-wyootwo.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
                                    }

                                    var data = markers[i];
                                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                                    var marker = new google.maps.Marker({
                                        position: myLatlng,
                                        map: map,
                                        icon: iconicon,
                                        id: i,
                                        title: data.title
                                    });
                                    allMyMarkers.push(marker);

                                    (function (marker, data) {
                                        google.maps.event.addListener(marker, "click", function (e) {
                                        infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/>'+ data.description + "</div>");
                                        infoWindow.open(map, marker);
                                        });
                                    })(marker, data);

                                  
                                }
               
                                map.setCenter(allMyMarkers[j].getPosition());
                        }
                        break; // stop continue looping
                }
        }
} // end toggleBounce


 $(document).ready(function() {
getLocation();
 LoadMap(); 

 $("myfullmap").on("click", function(){
alert("HHHH");

 })
 });

</script>


<script>
function getLocation() {
   var geolocation = navigator.geolocation;
   geolocation.getCurrentPosition(showLocation);
}
function showLocation(position) {
 //   x.innerHTML = "Latitude: " + position.coords.latitude + 
  //  "<br>Longitude: " + position.coords.longitude; 
GetAddress(position.coords.latitude, position.coords.longitude);

}



function GetAddress(lat, lng) {
           // var lat = parseFloat(document.getElementById("txtLatitude").value);
            //var lng = parseFloat(document.getElementById("txtLongitude").value);
            var latlng = new google.maps.LatLng(lat, lng);
            
             document.getElementById('currentlatlng').value = lat + "," + lng;
            
            var geocoder = geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                     // alert(document.getElementById('autocomplete').value );
                      if(document.getElementById('autocomplete').value == "")
                      {
                      document.getElementById('autocomplete').value = results[1].formatted_address;
                       }
                       else  
                    {
                     // alert("Location: " + results[1].formatted_address);
                    }

                    }
                }
            });
        }

</script>



    <script>
 $(document).ready(function() {

        var rating = 1.6;

        $(".counter").text(rating);

        $("#rateYo1").on("rateyo.init", function () { console.log("rateyo.init"); });

        $("#rateYo1").rateYo({
          rating: rating,
          numStars: 5,
          precision: 2,
          starWidth: "64px",
          spacing: "5px",
    rtl: true,
          multiColor: {

            startColor: "#000000",
            endColor  : "#ffffff"
          },
          onInit: function () {

            console.log("On Init");
          },
          onSet: function () {

            console.log("On Set");
          }
        }).on("rateyo.set", function () { console.log("rateyo.set"); })
          .on("rateyo.change", function () { console.log("rateyo.change"); });

        $(".rateyo").rateYo();

        $(".rateyo-readonly-widg").rateYo({

          rating: rating,
          numStars: 5,
          precision: 2,
          minValue: 1,
          maxValue: 5
        }).on("rateyo.change", function (e, data) {
        
          console.log(data.rating);
        });
      });
    </script>

    <script src="<?php echo base_url();?>assets/js/jquery.rateyo.js"></script>