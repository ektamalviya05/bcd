<div class="main-content about-us">
	<section class="main-search-sec">
		<div class="container">
			<div class="header-srch">
				<div class="row">
					<div class="col-md-12">
						<div class="main-srch-heading">
							<h1><?php echo $details[0]['pg_title']; ?></h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<nav class="navbar navbar-default in-page-nav">
    <div class="container">
        <!-- <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div> -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
            	<?php
            	foreach($similar_pages as $pages){
					//print_r($pages);
            		?>
            		<li class="<?php if($this->uri->segment(3) == $pages['pg_meta_tag']) echo "active"; ?>" >
	            <?php       
					if($pages['page_url'] != "")
                {
					$urls =  base_url("articles/").$pages['page_url'];  
                  ?>
                <a href='<?php echo $urls; ?>'>
				  <?php echo $pages['pg_title']; ?></a>
                <?php
                }
                else {
					$urls =  base_url("articles/").$pages['pg_meta_tag'];  
					
                  ?>
                 <a href='<?php echo $urls; ?>'><?php echo $pages['pg_title']; ?></a>
               <?php
                }
                ?>


					  
	                </li>
            		<?php
            	}
            	?>                
            </ul>
        </div>
    </div>
</nav>

<section class="about-sec">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				 
            <?php if($details[0]['page_url']=='Contactez-nous'){ ?>
			<div class="row">				
				<div class="col-md-7">
				 <div id="sucsessmsg"></div>
					<div class="reg-form-main">
						<form id="contactform" >
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									    <input type="text" name="name" class="form-control" placeholder="Nome et Prénom">
									</div>
								</div>
 
							</div>
							<!-- <div class="row">
								<div class="col-md-12">
									<div class="form-group"> -->
									    <input type="hidden" name="contact" class="form-control" placeholder="Contact">
									<!-- </div>
								</div>

 
							</div> -->

							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									    <input type="text" name="email" class="form-control" placeholder="Email">
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-12">
									<div class="form-group">
									    <textarea name="message" class="form-control" rows="4" placeholder="Message"></textarea>
									</div>
								</div>
							</div>

							
							
							<div class="row">
								<div class="col-md-12">
									<div class="btn-div">
										<button class="btn btn-default form-btn" type="submit">Envoyer</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-5">
					<div class="reg-text">
					 <?php
						echo $details[0]['pg_content'];
					 ?>
					</div>
				</div>
			</div>
 				
			<?php }else{ ?>
				<div class="about-content">
				  <?php
						echo $details[0]['pg_content'];
					 ?>
				</div>				
				
			 <?php } ?>
				 
				

				
			</div>
		</div>
	</div>
</section>