<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Listing extends MX_Controller {

    public function __construct() {
         parent:: __construct();

         $this->load->library('form_validation', 'session');
        // $this->load->model('Listing');
         //$this->userTbl = 'users';
        //is_user_login();
    }

    public function index() 
    {	
		
     	$data = array();
     	$this->db->select('cat_name');
    	$this->db->from('otd_business_category');
    	$this->db->where('cat_parent',0);
		$this->db->where('cat_status !=',3);
		$que = $this->db->get();
		$num_rows = $que->num_rows();
		
		$data['result'] = "fdsaf";
		
		
		$this->template->load('home_layout', 'contents', 'add_listing', $data);
    }
	public function add_listing(){
		
		$data['upload_path'] = 'uploads/business/'; 
		$data['allowed_types'] = 'jpg|jpeg|png|gif';
		$data['encrypt_name'] = true;

		$this->load->library('upload',$data);

		$uploadfile ="";
		if($this->upload->do_upload('userfile'))
		{ 
			$attachment_data = array('upload_data' => $this->upload->data());
			$uploadfile = $attachment_data['upload_data']['file_name'];
		  		 
		}
		
		
		$userData = array(
                'business_title	' 		=> strip_tags($this->input->post('title')),
				'business_email	' 		=> strip_tags($this->input->post('email')),
				'business_mobile'	 	=> strip_tags($this->input->post('mobile')),
				//'business_price'	 	=> strip_tags($this->input->post('price')),
				'business_address'	 	=> strip_tags($this->input->post('location')),
				'cat_id	' 				=> strip_tags($this->input->post('category')),
				'business_image1' 		=> $uploadfile,
				'features_ads'			=> 'yes',
				
               
            );
			$this->db->insert('bdc_business', $userData);
			//redirect()
		
	}
    /*public function editprofile() {
        $data = array();
        if ($this->input->post()) {
            $user_id = $this->session->userdata('user_id');
            $userData = array(
                'user_firstname' => strip_tags($this->input->post('user_firstname')),
                'user_lastname' => strip_tags($this->input->post('user_lastname')),
                'user_phone' => strip_tags($this->input->post('user_phone')),
                'user_address' => strip_tags($this->input->post('user_address')),
                'user_password' => md5($this->input->post('user_password')),
                'website' => $this->input->post('website'),
                'about' => $this->input->post('about')
            );
            if (!empty($_FILES['picture']['name'])) {
                $file_element_name = 'picture';
                $config['upload_path'] = './uploads/profile/';
                $config['allowed_types'] = 'jpg|jpeg|png|gif';
                $config['max_size'] = 1024 * 20;
                $config['encrypt_name'] = TRUE;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload($file_element_name)) {
                    $this->upload->display_errors('', '');
                } else {
                    $data = $this->upload->data();
                    $userData['user_profile_pic'] = $data['file_name'];
                }
                @unlink($_FILES[$file_element_name]);
            }
            if($this->input->post('password')){
                $userData['user_password']= md5($this->input->post('password'));
            }
            $this->db->where('user_id', $user_id);
            $this->db->update($this->userTbl, $userData);
            
        }
    }*/

}
