
<div style="padding:50px 0;background: #fff;">
	
	<section>
		<div class="container">
			<div class="row">
				<div class="col-sm-9">
					<div class="listing-add">
						<form action="<?php echo base_url();?>Users/add_listing" enctype= "multipart/form-data" method="post" name="addlisting" id="addlisting">
							<div class="row">
								<div class="col-sm-6">
									<label>Title</label>
									<input type="text" name="title" id="title" value="" >
								</div>
								
								<div class="col-sm-6">
									<label>Email</label>
									<input type="email" name="email" id="email" value="">
								</div>

							</div>

							<div class="row">
								<div class="col-sm-6">
									<label>Mobile</label>
									<input type="text" name="mobile" id="mobile" value="">
								</div>
								
								<div class="col-sm-6">
									<label>Price</label>
									<input type="number" name="price" id="price" value="">
								</div>
							</div>

							<div class="row">
								<div class="col-sm-6">
									<label>Location</label>
									<input type="text" name="location" id="location" value="">
								</div>
								<div class="f-login col-sm-6">
									<label>Category</label>
									
									<div class="cat-sel">
										<select name="category">
											
											<option value="1">Builder</option>
											<option value="2">Painter</option>
											<option value="3">Pipe Fitter</option>
											<option value="4">Electrician</option>
											<option value="5">Mason</option>
											<option value="6">Plumber</option>
											<option value="7">Carpenter</option>
										</select>
									</div>
									
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<div class="uploader-first">
				                      <div class="upload-sec">
				                      	<span>Upload Related images</span>
				                          <label>
				                              <input type="file" name="userfile" class="multiple_input" multiple="">
				                          </label>
				                      </div>
				                  </div>
								</div>
							</div>


							<div class="row">
								<div class="col-sm-12">
									<div class="f-login text-box">
										<label>Description</label>
										<textarea class="te-box" name="description" id="description"></textarea>
									</div>
								</div>
							</div>
							

			              <div class="row">
			                <div class="col-sm-12 sbt-btn">
			                  <input type="submit" name="submit" value="Add Listing">
							 
			                </div>
			              </div>
						</form>
					</div>
				</div>
				<div class="col-sm-3">
					<div class="right-add">
						<img src="<?php echo base_url();?>/assets/img/Property-Ad.jpg">
					</div>
				</div>
			</div>
		</div>
	</section>
</div>