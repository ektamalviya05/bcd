<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Listing extends CI_Model {

    function __construct() {
        $this->userTbl = 'users';
        
    }
    function getProfileDetail($id){
        $this->db->select('*')->from($this->userTbl);
        $this->db->where('user_id',$id);
        $query=$this->db->get();
        if($query->num_rows()>0){
            return $query->row_array();
        }
    }
    
}
