    <script type="text/javascript">
/*      var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M'
        });
      };*/
    </script>

    <!-- <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> -->
    <span id="shownumberexits" data-dismiss="modal" data-toggle="modal" data-target="#myModaComp"></span>
    <div class="row">
      <div class="col-md-4">
        <div class="reg-text">
          <h3><?php echo $pagecontent['label1']; ?></h3>
          <p><?php echo $pagecontent['label3']; ?></p>
        </div>
      </div>
      <div class="col-md-8">
        <div class="reg-form-main">
		
		      <input type="hidden" id="pro_reg_succ" value="<?php echo $this->session->flashdata("res_suc"); ?>" > 
          <form action="" method="post" name="myForm">
            <span><?php echo $pagecontent['label2']; ?></span>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="gender-main"><?php echo $pagecontent['label4']; ?></label>
                  <div class="for-radio">
                    <div class="btn-group" data-toggle="buttons">
                      
                      <?php
                      if(!empty($user['user_gender']) && $user['user_gender'] == 'Female'){
                      $fcheck = 'checked="checked"';
                      $mcheck = '';
                      }else{
                      $mcheck = 'checked="checked"';
                      $fcheck = '';
                      }
                      ?>
                      <label class="btn btn-default active">
                        <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male"><?php echo $pagecontent['label23']; ?>
                      </label>
                      <label class="btn btn-default">
                        <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female"><?php echo $pagecontent['label24']; ?>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control" required="" name="user_phone"  placeholder="<?php echo $pagecontent['label5']; ?>" value="<?php echo set_value('user_phone'); ?>">
                  <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>
                  <input type="hidden" name="user_type" value="Professional">
                  <?php
                  //print_r(form_error('user_company_number'));
                  $str = form_error('user_company_number');
                  $newphrase = str_replace("<p>", "", $str);
                  $newphrase = str_replace("</p>", "", $newphrase );
                  //echo  $newphrase;
                  if( trim($newphrase) == "This Company Number already exists.")
                  {
                 
                  ?>
                  <script type="text/javascript">
                     
                  $(document).ready(function() {
                  //$("#shownumberexits").trigger("click");
                  // openModal();
                  });
                  // function openModal(){
                  //   $('#myModaComp').modal();
                  // }
                  </script>
                  <?php
                  }
                  ?>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  
                  <input type="text" class="form-control " required="" name="user_firstname" placeholder="<?php echo $pagecontent['label6']; ?>"  value="<?php echo set_value('user_firstname'); ?>">
                  <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  
                  <input type="text" class="form-control " required="" name="user_lastname" placeholder="<?php echo $pagecontent['label7']; ?>"  value="<?php echo set_value('user_lastname'); ?>">
                  <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" class="form-control " required="" name="user_company_name" placeholder="<?php echo $pagecontent['label8']; ?>"  value="<?php echo set_value('user_company_name'); ?>">
                  <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="text" required="" class="form-control" name="user_company_number" placeholder="<?php echo $pagecontent['label9']; ?>" value="<?php echo set_value('user_company_number'); ?>">
                  <?php echo form_error('user_company_number','<span class="help-block">','</span>'); ?>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group check-box-inline">
                  <label class="language-main"><?php echo $pagecontent['label10']; ?></label>
                  <?php
                  if(!empty($languages)){
                  foreach($languages as $lang1){
                  ?>
                  <label class="checkbox-inline">
                    <input type="checkbox" name="lang[]" value="<?php echo $lang1['lang_id']; ?>">
                    <?php echo ucwords($lang1['lang_name']); ?>
                  </label>
                  <?php
                  }
                  }
                  ?>
                  <!-- <label class="checkbox-inline">
                    <input type="checkbox" value="2" name="user_language[]">English
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="3" name="user_language[]">French
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="4" name="user_language[]">Spanish
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="5" name="user_language[]">Arab
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="6" name="user_language[]">Italion
                  </label>
                  <label class="checkbox-inline">
                    <input type="checkbox" value="7" name="user_language[]">Germen
                  </label> -->
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <!-- <input type="text" class="form-control" name="user_categories" placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)"> -->
                  <input class='form-control'
                  multiple='multiple'
                  list='categories'
                  name='user_categories'
                  id="business_category"
                  type='text' placeholder="<?php echo $pagecontent['label11']; ?>" value="<?php echo set_value("user_categories"); ?>">
                  <datalist id="categories">
                  <?php
                  foreach($business_categories->result() as $cate){
                  ?>
                  <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                  <?php
                  }
                  ?>
                  </datalist>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input id="autocomplete" placeholder="<?php echo $pagecontent['label12']; ?>" required="" name="user_company_address" class="form-control " onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>">
                  <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  
                  <input type="email" class="form-control" required="" name="user_email"  placeholder="<?php echo $pagecontent['label13']; ?>"  value="<?php echo set_value('user_email'); ?>">
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="form-control " required="" name="user_password"  id="user_password" placeholder="<?php echo $pagecontent['label14']; ?>" >
                  <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="password" class="form-control " name="conf_password" placeholder="<?php echo $pagecontent['label15']; ?>" >
                  <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
            </div>

            <div>
              <br><p>&nbsp;</p>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="terms-offer">
                    <input type="checkbox" name="auth_check" id="auth_check" value="1" checked="checked"><span><?php echo $pagecontent['label17']; ?></span>
                  </label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="terms-offer">
                    <input type="checkbox" name="sms_check" id="sms_check" value="1" checked="checked"><span><?php echo $pagecontent['label18']; ?></span>
                  </label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="terms-offer">
                    <input type="checkbox" name="confidential" required="" id="proconfidential"  value="1"><span id="proconfidentialtxt"><?php echo $pagecontent['label19']; ?></span>
                  </label>
                </div>
                <br>
                <!--<div id="html_element"></div>-->
                <div class="g-recaptcha" data-sitekey="6LdypjcUAAAAACEtrAUGR-au5-Ad-3u6Nf4wRZia">
                  
                </div>
                <?php echo form_error('g-recaptcha-response','<span class="help-block">','</span>'); ?>
                <?php
                //echo '<span class="help-block">'. $error_msg_cap.'</span>'
                ?>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <br>
                <div class="btn-div">
                  <button class="btn btn-default form-btn" value="S'enregistrer" name="regisSubmit" type="submit"><?php echo $pagecontent['label20']; ?>
                  </button>
                </div>
              </div>
              <p class="footInfo"><?php echo $pagecontent['label21']; ?>
                <a href="<?php echo base_url(); ?>Users/login"><?php echo $pagecontent['label22']; ?></a>
              </p>
            </div>
          </form>
        </div>
      </div>
    </div>
<!-- thank pop up for Proffesnol user -->
<div class="modal fade" id="pro_user_thank" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">         
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title">Thank you</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         <span>
              Thank you for registration.
         </span>          
        </div>
      </div>
      <div class="modal-footer">
        <a href="<?php echo base_url();?>" class="btn btn-default" > Close</a>
      </div>
    </div>
  </div>
</div>
<!-- thank pop up for Proffesnol end -->	
 	