<pre>
  <?php
  // print_r($package);  
  ?>
</pre>
<section class="cart">
  <div class="container">
    <div class="row">      
      <div class="cart-table details-fill">
      	<div class="text-right back-div">
      		<a href="<?php echo base_url("Users/account/messoptions/purchase") ?>" class="pkg-back"><i class="fa fa-arrow-left"></i> Back</a>
      	</div>
        <div class="pkg-heading">
          <h3><?php echo $package['opt_name']; ?></h3>
          <div class="pkg-desc">
            <?php
            echo $package['opt_description'];
            ?>
          </div>
        </div>
        <div class="pkg-options">
          <?php
          foreach($options as $opts){
            ?>
            <div class="opt-main-div">
              <div class="opt-opt-head">
                <h4><?php echo $opts['opt_name']; ?></h4>
                <div class="opt-desc">
                  <div class="opt-descmain"><?php echo $opts['opt_description']; ?></div>
                  <?php
                  if($useroptions['opt_option_status'] == 1){
                    if($opts['opt_option_id'] == 7 || $opts['opt_option_id'] == 8){                      
                      if($opts['opt_option_status']){
                        ?>
                        <button type="button" class="btn btn-xs btn-default"><i class="fa fa-thumbs-up"></i> <?php echo $labels['label87']; ?></button>
                        <a class="btn btn-info btn-xs" target="_blank" href="<?php echo base_url("Users/myEvents/").$opts['opt_event_id']; ?>" ><i class="fa fa-eye"></i> <?php echo $labels['label89']; ?></a>
                        <?php
                      }else{
                      ?>                      
                        <div class="option-content">
                          <button class="btn btn-success btn-xs event1" data-toggle="modal" data-target="#createEventmodal" details="<?php echo $opts['opt_user_option_id']; ?>" list-id="<?php echo $opts['opt_event_id']; ?>" ><i class="fa fa-edit"></i> <?php echo $labels['label93']; ?></button>
                        </div>
                        <?php
                      }
                    }else{
                      if($opts['opt_option_active_date'] != "0000-00-00"){
                        $startd = date("d", strtotime($opts['opt_option_active_date']));
                      }else{
                        $startd = 00;
                      }                      
                      ?>
                        <form name="packageform" method="post" action="<?php echo base_url("Users/updatepackageoption/").$opts['opt_user_option_id']."/".$opts['opt_pkg_id']; ?>" autocomplete="off">
                          <div class="form-group col-opt-input-text">
                            <select class="form-control" name="stdate" <?php if($opts['opt_option_status'] == 1 || $opts['opt_option_status'] == 3) echo "disabled"; ?>>
                              <option value="">Select Start Date</option>
                              <?php                              
                              for($i=1;$i<=31;$i++){
                              ?>                        
                              <option value="<?php echo $i ?>" <?php if($startd == $i) echo "selected"; ?>><?php echo $i ?></option>
                              <?php
                              }
                              ?>
                            </select>                                                        
                            <div class='stdatemsg error1'><?php echo form_error("stdate"); ?></div>
                          </div>
                          <?php
                          if($opts['opt_option_id'] == 2){
                          ?>
                            <div class="form-group col-opt-input-text">
                              <label class="form-label">Visibile For Categories:</label>
                              <?php  
                                $catname = $this->User->getCategoriesAndFiltersByName(1);
                                if(!empty($catname))
                                  echo $catname;
                                else
                                  echo "N/A";
                              ?>
                            </div>
                            <div class="form-group col-opt-input-text">
                              <label class="form-label">Visibile For City:</label>
                              <?php echo $user['user_company_address']; ?>
                            </div>
                            <div class="form-group col-opt-input-text">
                              <label class="form-label">City:</label>                                                    
                              <input <?php if($opts['opt_option_status'] == 1 || $opts['opt_option_status'] == 3) echo "readonly"; ?> type="text" name="city" class="form-control citycomplete" onfocus="geolocate()" placeholder="Enter city" value="<?php echo $opts['otp_search_city']; ?>">
                              <div class='citymsg error1'><?php echo form_error("city"); ?></div>
                            </div>
                          <?php
                          }
                          ?>                          
                          <div class="form-group col-opt-input-text">
                            <div id="blockdatemsg"></div>
                            <input type="hidden" name="opt_type" value="<?php echo $opts['opt_option_id']; ?>">
                            <input type="hidden" name="opt_id" value="<?php echo $opts['opt_user_option_id']; ?>">
                            <?php
                            if($opts['opt_option_status']){
                              ?>
                              <button type="button" class="btn btn-xs btn-default"><i class="fa fa-thumbs-up"></i> <?php echo $labels['label87']; ?></button>
                              <?php
                            }else{
                            ?>
                              <button type="submit" class="update-btn btn-xs pkgupdate-btn"> <?php echo $labels['label88']; ?></button>
                            <?php
                            }
                            ?>
                          </div>
                        </form>
                      <?php
                    }
                    if($opts['opt_option_status'] == 1){
                      ?>
                      <div class="remain-days">
                        <?php
                        $arr = array(1,2,3,4,5,6,7,8,9);
                        // if(in_array($opts['opt_mnth_strt_day'], $arr)){
                        //   $udate = date("Y-m-0".$opts['opt_mnth_strt_day']);
                        // }else{
                        //   $udate = date("Y-m-".$opts['opt_mnth_strt_day']);
                        // }
                        $udate = $opts['opt_option_active_date'];
                        $nextdate = date("Y-m-d", strtotime($udate."+ ".$opts['otp_option_duration']));
                        // if($nextdate > date("Y-m-d")){
                        if($udate > date("Y-m-d")){                                  
                          $date1 = date_create(date("Y-m-d"));
                          $date2 = date_create($udate);        
                          $difference = date_diff($date1,$date2);
                          $diff = $difference->format('%a');
                          if($diff > 1){
                            ?>
                            <span class="remain-div text-success"><?php echo $diff." Days remaining in plan activation."; ?></span>
                            <?php
                          }else{
                            ?>
                            <span class="remain-div text-success"><?php echo $diff." Day remaining in plan activation."; ?></span>
                            <?php
                          }
                        }else if($udate <= date("Y-m-d") && $nextdate >= date("Y-m-d")){
                          $date1 = date_create(date("Y-m-d"));
                          $date2 = date_create($nextdate);        
                          $difference = date_diff($date1,$date2);
                          $diff = $difference->format('%R%a');
                          if($diff > 1){
                            ?>
                            <span class="remain-div text-success"><?php echo str_replace("+", "", $diff)." Days Remaining."; ?></span>
                            <?php
                          }else if($diff == 1 ||$diff == "+1"){
                            ?>
                            <span class="remain-div text-success"><?php echo "1 Day Remaining."; ?></span>
                            <?php
                          }else if($diff < 0 || $diff == 0){
                            ?>
                            <span class="remain-div text-danger">Notice: This Plan is expired for this month</span>
                            <?php
                          }
                        }else{
                          ?>
                          <span class="remain-div text-danger">Notice: This Plan is expired for this month</span>
                          <?php
                        }
                        ?>
                      </div>
                      <?php
                    }                            
                  }else{
                    ?>
                    <div class="form-group col-opt-input-text">
                      <p class="btn btn-xs btn-default"><i class="fa fa-ban"></i> Package plan is not activated.</p>
                    </div>
                    <?php
                  }
                  ?>
                </div>
              </div>
            </div>
            <hr>
            <?php
          }
          ?>
        </div>
        <div class="text-right back-div">
      		<a href="<?php echo base_url("Users/account/messoptions/purchase") ?>" class="pkg-back"><i class="fa fa-arrow-left"></i> Back</a>
      	</div>
      </div>
    </div>
  </div>
</section>

<!-- Create Event model-->  
<div id="createEventmodal" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form id="createevent" method="POST" action="" autocomplete="off">
        <div class="modal-header">
          <div class="prof-msg-details">
            <div class="row">
              <div class="col-xs-12">
                <div class="msg-pro-pic-sec">
                  <div class="msh-prof-details">
                    <h3>Create Event</h3>
                  </div>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-body review-body" data-mcs-theme="dark">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label"><?php echo $labels['label35']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label35']; ?></span>
                    </span>
                  </label>
                  <input type="text" name="ename" class="form-control" placeholder="<?php echo $labels['label35']; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label36']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label36']; ?></span>
                    </span>
                  </label>
                  <select name="eventtype" class="form-control">
                    <option value="">Select Type</option>
                    <?php
                    $eventTypeList = $this->User->getEventTypeList();
                    foreach($eventTypeList as $evttype){
                    ?>
                    <option value="<?php echo $evttype['evt_type_id']; ?>"><?php echo $evttype['evt_type']; ?></option>
                    <?php
                    }                    
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row publicationdiv"></div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label"><?php echo $labels['label39']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label39']; ?></span>
                    </span>
                  </label>
                  <input type="text" readonly name="esdate" class="form-control datetimepickerfuture" placeholder="<?php echo $labels['label39']; ?>e">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label40']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label40']; ?></span>
                    </span>
                  </label>
                  <input type="text" readonly name="eedate" class="form-control datetimepickerfuture" placeholder="<?php echo $labels['label40']; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label41']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label41']; ?></span>
                    </span>
                  </label>
                  <input type="number" min="1" name="noperson" class="form-control" placeholder="<?php echo $labels['label41']; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label42']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label42']; ?></span>
                    </span>
                  </label>
                  <input type="text" name="eventprice" class="form-control" placeholder="<?php echo $labels['label42']; ?>" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label43']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label43']; ?></span>
                    </span>
                  </label>
                  <input type="text" name="evtaddress" class="form-control citycomplete" onfocus="geolocate()" placeholder="<?php echo $labels['label43']; ?>" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label59']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label45']; ?></span>
                    </span>
                  </label>
                  <input type="file" multiple="true" name="evtfile[]" id="evtfile" class="form-control" />
                  <div id="evtimgmsg"></div>
                  <div id="evtimgpreview"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label"><?php echo $labels['label44']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label44']; ?></span>
                    </span>
                  </label>
                  <textarea class="textarea-main" name="description" placeholder="<?php echo $labels['label44']; ?>"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <input type="hidden" value="" id="usoption" name="usoption"/>
            <input type="hidden" value="" id="uslist" name="uslist" />
            <input type="hidden" value="1" name="urlpackage" id="urlpackage"/>
            <button type="submit" id="evtsubmit" class="btn" name="optionPurchaseSubmit"> <?php echo $labels['label60']; ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $labels['label61']; ?></button>
            <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
            <div class="eventmsg"></div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Create event model end -->