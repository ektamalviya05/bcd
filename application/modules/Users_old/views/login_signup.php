<div class="row">
        <div class="col-md-5">
          <input type="hidden" id="glogintext" value="<?php echo $pagecontent['label5']; ?>" />
          <div class="reg-text">
            <h3><?php echo $pagecontent['label1']; ?></h3>
            <p><?php echo $pagecontent['label3']; ?></p>
          </div>
          <div class="row">
            <div class="social-media-login page-l">
              <div class="col-sm-12">
                <h3><?php echo $pagecontent['label2']; ?></h3>
              </div>
              <div class="col-sm-6">
                <a href="javascript:;" class="btn btn-default fb-so" onclick="Login()"><i class="fa fa-facebook"></i><?php echo $pagecontent['label4']; ?></a>
              </div>              
              <div class="col-sm-6">
                <div class="g-signin2" data-onsuccess="onSignIn"><?php echo $pagecontent['label5']; ?></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-7">
          <div class="reg-form-main">

    <form action="" method="post" name="myForm" >

<span><?php echo $pagecontent['label6']; ?></span>


<div class="row">
                <div class="col-md-8">
                  <div class="form-group">
                    <label class="gender-main"><?php echo $pagecontent['label7']; ?></label>
                    <div class="for-radio">
                      <div class="btn-group" data-toggle="buttons">
                        
 <?php
            if(!empty($user['user_gender']) && $user['user_gender'] == 'Female'){
                $fcheck = 'checked="checked"';
                $mcheck = '';
            }else{
                $mcheck = 'checked="checked"';
                $fcheck = '';
            }
            ?>

                        <label class="btn btn-default active">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male"><?php echo $pagecontent['label8']; ?>
                        </label>
                        <label class="btn btn-default">
                          <input type="radio" name="user_gender" <?php echo $fcheck; ?> value="Female"><?php echo $pagecontent['label9']; ?>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                
              </div>






 <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter first name." class="form-control" name="user_firstname" placeholder="<?php echo $pagecontent['label10']; ?>"  value="<?php echo set_value('user_firstname'); ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="text" title="Please enter last name."  class="form-control"  name="user_lastname" placeholder="<?php echo $pagecontent['label11']; ?>"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                 
   <input type="hidden" name="user_type" value="Personal">
                  </div>
                </div>
              </div>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="email" class="form-control" title="Please enter a valid email."   name="user_email"  placeholder="<?php echo $pagecontent['label12']; ?>"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter telephone number."  required="" maxlength="10" class="form-control" name="user_phone"  placeholder="<?php echo $pagecontent['label13']; ?>" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div>


<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
          
  <input id="autocomplete" placeholder="<?php echo $pagecontent['label14']; ?>" 
  name="user_address" class="form-control" onFocus="geolocate()" type="text"  value="<?php echo set_value('user_address'); ?>"></input>
   <?php echo form_error('user_address','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                <input type="password" class="form-control" name="user_password" id="user_password" placeholder="<?php echo $pagecontent['label15']; ?>" >
          <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="password" required="" class="form-control" name="conf_password" placeholder="<?php echo $pagecontent['label16']; ?>" title="Password must be match."  >
          <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>

<div><br><p>&nbsp;</p></div>
        
        <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="auth_check" id="auth_check" value="1" checked="checked"><span><?php echo $pagecontent['label17']; ?></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="sms_check" id="sms_check" value="1" checked="checked"><span><?php echo $pagecontent['label18']; ?></span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="confidential" required="" id="personalconfidential"  value="1"><span><?php echo $pagecontent['label19']; ?></span>
                    </label>
                  </div>
                    <div class="g-recaptcha" data-sitekey="6LdypjcUAAAAACEtrAUGR-au5-Ad-3u6Nf4wRZia"></div>
 <?php echo form_error('g-recaptcha-response','<span class="help-block">','</span>'); ?>
                   <?php

//echo '<span class="help-block">'. $error_msg_cap.'</span>'
                   ?>

                </div>
              </div>

    <div class="row">
                <div class="col-md-12">
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" value="S'enregistrer" name="regisSubmit" type="submit"><?php echo $pagecontent['label20']; ?></button>

                  </div>
                </div>
                <p class="footInfo"><?php echo $pagecontent['label21']; ?> <a href="<?php echo base_url(); ?>Users/login"><?php echo $pagecontent['label22']; ?></a></p>  
              </div>

    </form>
    </div>
    </div>
    </div>

<script type="text/javascript">
    $(function() {
  $("form[name='myForm']").validate({
    rules: {
      user_firstname: "required",
      user_lastname: "required",
      user_email: {
        required: true,
        email: true
      },
      user_address:"required",
      user_password: {
        required: true,
        minlength: 6
      },
      conf_password:{
        required: true,
        equalTo: "#user_password",
        minlength: 6
      }
    },
    // Specify validation error messages
    messages: {
      user_firstname: "Veuillez saisir votre prénom",
      user_lastname: "Veuillez saisir votre nom",
      user_password: {
        required: "Veuillez saisir un mot de passe",
        minlength: "Votre mot de passe doit contenir 6 caractères minimum"
      },
      conf_password: {
        required: "Veuillez saisir un mot de passe",
        minlength: "Votre mot de passe doit contenir 6 caractères minimum",
        equalTo: "Vos mot de passe doivent être identique"
      },
      user_email: "Veuillez saisir votre adresse email",
      user_address: "Veuillez saisir votre adresse"
    },
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    