<div class="main-dashboard">
  <section class="dash-board-sec">
    <div class="container">
      <div class="dash-board-head">
        <div class="row">
          <div class="col-md-4">
            <div class="header-link-dash"> <a href="<?php echo base_url("Pages/businessListing"); ?>" class="btn btn-default"><i class="fa fa-arrow-left"> </i>Page de recherche</a> <a href="<?php echo base_url("Users/user_profile"). "/".$this->session->userdata('user_id'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"> </i>Mon profil</a> </div>
          </div>
        </div>
      </div>
      <div class="dash-board-inner">
        <div class="panel panel-primary">
          <div class="dash-link-main">
            <ul class="nav panel-tabs1">
              <li class="<?php if($this->uri->segment(3) != "inbox" && $this->uri->segment(3) != "notifications") echo "active"; ?>"><a href="#tab1" data-toggle="tab">Mes Informations</a></li>
              <li ><a href="#tab2" data-toggle="tab">Mes commerçants</a></li>
              <li class="<?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "notifications") echo "active"; ?>"><a  id="messagetab" href="#tab3" data-toggle="tab">Mes Messages</a></li>
              <?php
              if($user['user_type'] == "Professional"){
              ?>
                <li><a href="#tab4" data-toggle="tab">Mes Options</a></li>
                <li><a href="#tab5" data-toggle="tab">Create Event</a></li>
              <?php
              }
              ?>              
            </ul>
          </div>
          <div class="panel-body">
            <div class="tab-content">
              <div class="tab-pane <?php if($this->uri->segment(3) != "inbox" && $this->uri->segment(3) != "notifications" ) echo "active"; ?>" id="tab1">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs2">
                          <li class="active"><a href="#tab-sub1" data-toggle="tab">Informations personnelles</a></li>
                          <li><a href="#tab-sub2" data-toggle="tab">Account parameter</a></li>
                          <?php
                          if($user['user_type'] == "Professional"){
                          ?>
                          <li><a href="#tab-sub3" data-toggle="tab">Mon entreprise</a></li>
                          <?php
                          }
                          ?>
                          <li><a href="#tab-sub4" data-toggle="tab">Boite à idée</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab-sub1">
                            <form id="personalinfoupdate" method="post" action="">
                              <div class="mes-info-sec">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label" for="usr">Prénom: 
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label1']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" class="form-control" name="first_name" value="<?php echo $user['user_firstname']; ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label" for="usr">Nom: 
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label2']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" class="form-control" name="last_name" value="<?php echo $user['user_lastname']; ?>">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="language-main">LANGUE
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label3']; ?></span>
                                        </span>
                                      </label>
                                      <?php
                                      $lang = $user['user_language'];
                                      if(!empty($lang))
                                        $lang = explode(",", $lang);                                    
                                      else
                                        $lang = array();
                                      ?>
                                      <div class="check-box-inline">
                                        <?php
                                        if(!empty($languages)){
                                          foreach($languages as $lang1){
                                            ?>
                                            <label class="checkbox-inline">
                                              <input type="checkbox" name="lang[]" value="<?php echo $lang1['lang_id']; ?>" <?php if(in_array($lang1['lang_id'], $lang)) echo "checked"; ?>>
                                              <?php echo ucwords($lang1['lang_name']); ?>
                                            </label>
                                            <?php
                                          }
                                        }
                                        ?>
                                        <!-- <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="2" <?php if(in_array(2, $lang)) echo "checked"; ?>>
                                          English 
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="3" <?php if(in_array(3, $lang)) echo "checked"; ?>>
                                          French </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="4" <?php if(in_array(4, $lang)) echo "checked"; ?>>
                                          Spanish </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="5" <?php if(in_array(5, $lang)) echo "checked"; ?>>
                                          Arab </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="6" <?php if(in_array(6, $lang)) echo "checked"; ?>>
                                          Italion </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="7" <?php if(in_array(7, $lang)) echo "checked"; ?>>
                                          Germen </label> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="gender-main">GENRE
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label4']; ?></span>
                                        </span>
                                      </label>
                                      <div class="for-radio">
                                        <div class="btn-group" data-toggle="buttons">
                                          <label class="btn btn-default <?php if($user['user_gender'] == "Male" || $user['user_gender'] == "male") echo "active"; ?>">
                                            <input type="radio" name="douhavekidsprice" <?php if($user['user_gender'] == "Male" || $user['user_gender'] == "male") echo "checked"; ?> value="Male">
                                            Homme </label>
                                          <label class="btn btn-default <?php if($user['user_gender'] == "Female" || $user['user_gender'] == "female") echo "active"; ?>">
                                            <input type="radio" name="douhavekidsprice" <?php if($user['user_gender'] == "Female" || $user['user_gender'] == "female") echo "checked"; ?> value="Female">
                                            Femme </label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label date" for="usr">Date de naissance:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label5']; ?></span>
                                        </span>
                                      </label>
                                      <p>
                                        <input type="text" name="dob" class="form-control datepickerpast" placeholder="Date" value="<?php if($user['user_dob'] != "0000-00-00") echo $user['user_dob']; ?>">
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                      <div class="update-button">
                                          <button class="btn btn-default profsubmit" type="submit">Mise à jour</button>
                                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span> 
                                          <div id="profilemsg"></div>
                                      </div>                                      
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="tab-pane" id="tab-sub2">
                            <div class="mes-info-sec">
                              <div class="row">
                                <form method="post" action="" id="changeemail" autocomplete="off" >
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">New Email:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label6']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" name="email" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">Password:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label7']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="password" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <button type="submit" class="chnge-mail btn btn-default upmail">Change mail</button>
                                    <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                    <div id="updateemailmsg"></div>
                                  </div>
                                </form>
                              </div>
                              <div class="row">
                                <form method="post" action="" id="updatepassword">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label">Old Password:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label8']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="oldpass" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label">New Password:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label9']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="newpass" id="newpass" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label confm-pass">Confirm New Password:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label10']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="newcpass" class="form-control cnfm-pass">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <button type="submit" class="chnge-mail btn btn-default upps">Change Password</button>
                                    <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                    <div id="updatepsmsg"></div>
                                  </div>
                                </form>
                              </div>
                              <div class="row">
                                <form id="userdeactivate" method="post" action="">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label confm-pass">Password:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label11']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="dpass" class="form-control cnfm-pass">
                                    </div>
                                    <button type="submit" class="chnge-mail btn btn-default deactivateac">Delete my account</button>
                                    <span style="display:none;" class="dloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                    <div id="dacctmsg"></div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <?php
                          if($user['user_type'] == "Professional"){
                            ?>
                            <div class="tab-pane" id="tab-sub3">                            
                              <div class="mes-info-sec">
                                <form id="businessupdate" method="post">
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="form-label">Nom de l'entreprise:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label12']; ?></span>
                                          </span>
                                        </label>
                                        <input type="text" name="cm_name" value="<?php echo $user['user_company_name']; ?>" class="form-control">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="form-label">Numéro de SIRET:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label13']; ?></span>
                                          </span>
                                        </label>
                                        <input type="text" name="cm_number" value="<?php echo $user['user_company_number']; ?>" class="form-control">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="form-label">Site Web:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label14']; ?></span>
                                          </span>  
                                        </label>
                                        <input type="text" name="cm_url" value="<?php echo $business['bs_website']; ?>" class="form-control">
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="form-label">Date de création:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label15']; ?></span>
                                          </span>
                                        </label>
                                        <p>
                                          <input type="text" name="cm_opening" value="<?php if($business['bs_opening'] != "0000-00-00") echo $business['bs_opening']; ?>" class="form-control datepickerpast">
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="form-label">Telephone:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label16']; ?></span>
                                          </span>
                                        </label>
                                        <div class="existingphone">
                                          <input type="text" name="cm_phnumber" value="<?php echo $user['user_phone']; ?>" class="form-control">                                          
                                        </div>
                                        <div class="extranumbers">
                                          <?php
                                          if(!empty($extraphone)){
                                            foreach($extraphone as $nb){
                                              ?>
                                              <div class="pardiv">
                                                <span class="div1">
                                                    <input type="text" readonly class="form-control" value="<?php echo $nb['phn_label']; ?>" />
                                                </span>
                                                <span class="div2">
                                                    <input type="text" class="form-control" readonly value="<?php echo $nb['phn_number']; ?>"><button type="button" class="removeextnumber" modal-aria="<?php echo $nb['phn_id']; ?>"><i class="fa fa-times"></i></button>
                                                </span>                                                
                                              </div>
                                              <?php
                                            }
                                          }
                                          ?>
                                        </div>
                                        <div class="addnewphone">                                          
                                        </div>
                                        <button type="button" id="addphone" data-toggle="modal" data-target="#addphonenumbers" class="btn btn-default"><i class="fa fa-plus"></i> Add Number</button>
                                      </div>
                                    </div>
                                    <div class="col-md-6">
                                      <div class="form-group">
                                        <label class="form-label">Identifiant Twitter:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label17']; ?></span>
                                          </span>
                                        </label>
                                        <input type="text" value="<?php echo $business['bs_twitter']; ?>" name="cm_twitter" class="form-control">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="form-label">Adresse mail du parrain:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label18']; ?></span>
                                          </span>
                                        </label>
                                        <input type="email" name="cm_email" value="<?php echo $user['user_email']; ?>" class="form-control">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="catergory-containr">
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="form-label">Choix de votre/vos catégroie(s):
                                            <span class="tooltip">?
                                              <span class="tooltiptext"><?php echo $suggestion['label19']; ?></span>
                                            </span>
                                          </label>                                        
                                          <input class='form-control'                     
                                               multiple='multiple'
                                               list='categories'
                                               name='user_categories'
                                               id="business_category"
                                               type='text' placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)" value="<?php echo $this->User->getCategoriesAndFilters(1); ?>">

                                              <datalist id="categories">
                                                  <?php
                                                  foreach($business_categories->result() as $cate){
                                                    ?>
                                                    <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                    <?php
                                                  }
                                                  ?>                        
                                              </datalist>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                                  <div class="catergory-containr">                                   
                                    <div class="row">
                                      <div class="col-md-12">
                                        <div class="form-group">
                                          <label class="form-label">Vos prestations:
                                            <span class="tooltip">?
                                              <span class="tooltiptext"><?php echo $suggestion['label20']; ?></span>
                                            </span>
                                          </label>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="ano-cat-sub">
                                          <div class="anno-cat-head">
                                            <h3>Services</h3>
                                          </div>
                                          <div class="service_ul" data-mcs-theme="dark">
                                            <input class='form-control'                     
                                                   multiple='multiple'
                                                   list='servicefilterslist'
                                                   name='servicefilters'
                                                   id="servicefilters"
                                                   type='text' placeholder="VOS PRESTATIONS" value="<?php echo $this->User->getCategoriesAndFilters(2); ?>">

                                            <datalist id="servicefilterslist">
                                                <?php
                                                foreach($service_filters->result() as $cate){
                                                  ?>
                                                  <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                  <?php
                                                }
                                                ?>                        
                                            </datalist>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="ano-cat-sub">
                                          <div class="anno-cat-head">
                                            <h3>Product</h3>
                                          </div>
                                          <div class="service_ul" data-mcs-theme="dark">
                                            <input class='form-control'                     
                                                   multiple='multiple'
                                                   list='productfilterslist'
                                                   name='productfilters'
                                                   id="productfilters"
                                                   type='text' placeholder="VOS PRESTATIONS" value="<?php echo $this->User->getCategoriesAndFilters(3); ?>">

                                            <datalist id="productfilterslist">
                                                <?php
                                                foreach($product_filters->result() as $cate){
                                                  ?>
                                                  <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                  <?php
                                                }
                                                ?>                        
                                            </datalist>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="col-md-4">
                                        <div class="ano-cat-sub">
                                          <div class="anno-cat-head">
                                            <h3>Other</h3>
                                          </div>
                                          <div class="service_ul" data-mcs-theme="dark">
                                            <input class='form-control'                     
                                                   multiple='multiple'
                                                   list='otherfilterslist'
                                                   name='otherfilters'
                                                   id="otherfilters"
                                                   type='text' placeholder="VOS PRESTATIONS" value="<?php echo $this->User->getCategoriesAndFilters(4); ?>">

                                            <datalist id="otherfilterslist">
                                                <?php
                                                foreach($other_filters->result() as $cate){
                                                  ?>
                                                  <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                  <?php
                                                }
                                                ?>                        
                                            </datalist>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="form-label">Veuillez saisir votre adresse:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label21']; ?></span>
                                          </span>
                                        </label>
                                        <input type="text" id="autocomplete" onFocus="geolocate()" value="<?php echo $user['user_company_address']; ?>" name="cm_address" class="form-control">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="row text-area">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="form-label">Decrire votre entreprise:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label22']; ?></span>
                                          </span>
                                        </label>
                                        <textarea class="form-control text-main" name="cm_desc" id="cm_desc" rows="4"><?php echo $business['bs_desc']; ?></textarea>
                                        <span class="max-char">Nombre de caractères: <span id="desclength"><?php echo 500 - strlen($business['bs_desc']); ?></span></span> </div>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-md-12">
                                        <div class="update-button">
                                          <button class="btn btn-default bsbutton">Mise à jour</button>
                                          <span style="display:none;" class="bsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                          <div id="bsmsg"></div>
                                        </div>
                                    </div>
                                  </div>
                                </form>
                                <hr>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label">Prices:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label23']; ?></span>
                                        </span>
                                      </label>
                                      <form id="uploadbsprice" enctype="multipart/form-data" method="post" action="">
                                        <div class="col-md-9">
                                          <input type="file" name="bs_price_pdf" class="form-control form-file">
                                          <label style="color:#F00;" id="prcmsg"></label>
                                        </div>
                                        <div class="col-md-3 update-button">
                                          <button class="btn btn-default prbutton">Upload</button>
                                          <span style="display:none;" class="prloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                        </div>                                      
                                      </form>
                                    </div>
                                    <div class="price-listing">
                                      <?php
                                      if(!empty($prstable)){
                                        foreach($prstable as $prs){
                                          ?>
                                          <div class="file-uploaded">
                                            <div class="file-upl-name">
                                              <p><?php echo $prs['pr_metaname']; ?></p>
                                            </div>
                                            <div class="file-upload-del"> <a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="<?php echo $prs['pr_id']; ?>"><i class="fa fa-close"></i></a> </div>
                                          </div>
                                          <?php
                                        }
                                      }
                                      ?>
                                    </div>

                                  </div>
                                </div>
                                <hr>
                                <div class="row time-intervl">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label">Vos horaires d’ouverture:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label24']; ?></span>
                                        </span>
                                      </label>
                                    </div>
                                  </div>
                                  <form method="post" action="" id="openingtime">
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Monday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label25']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input"> 
                                          <input type="text" name="mon1" value="<?php echo $opentiming['mon1']; ?>"  class="timepicker form-control">
                                          <input type="text" name="mon2" value="<?php echo $opentiming['mon2']; ?>"  class="timepicker form-control">
                                          <label class="or-label">and</label>
                                           <input type="text" name="mon3" value="<?php echo $opentiming['mon3']; ?>" class="timepicker form-control">
                                           <input type="text" name="mon4" value="<?php echo $opentiming['mon4']; ?>" class="timepicker form-control">
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Tuesday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label26']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input">
                                          <input type="text" name="tues1" value="<?php echo $opentiming['tues1']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="tues2" value="<?php echo $opentiming['tues2']; ?>" class="timepicker form-control"/>
                                          <label class="or-label">and</label>
                                          <input type="text" name="tues3" value="<?php echo $opentiming['tues3']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="tues4" value="<?php echo $opentiming['tues4']; ?>" class="timepicker form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Wednesday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label27']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input">
                                          <input type="text" name="wed1" value="<?php echo $opentiming['wed1']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="wed2" value="<?php echo $opentiming['wed2']; ?>" class="timepicker form-control"/>
                                          <label class="or-label">and</label>
                                          <input type="text" name="wed3" value="<?php echo $opentiming['wed3']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="wed4" value="<?php echo $opentiming['wed4']; ?>" class="timepicker form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Thursday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label28']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input">
                                          <input type="text" name="thur1" value="<?php echo $opentiming['thur1']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="thur2" value="<?php echo $opentiming['thur2']; ?>" class="timepicker form-control"/>
                                          <label class="or-label">and</label>
                                          <input type="text" name="thur3" value="<?php echo $opentiming['thur3']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="thur4" value="<?php echo $opentiming['thur4']; ?>" class="timepicker form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Friday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label29']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input">
                                          <input type="text" name="fri1" value="<?php echo $opentiming['fri1']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="fri2" value="<?php echo $opentiming['fri2']; ?>" class="timepicker form-control"/>
                                          <label class="or-label">and</label>
                                          <input type="text" name="fri3" value="<?php echo $opentiming['fri3']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="fri4" value="<?php echo $opentiming['fri4']; ?>" class="timepicker form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Saturday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label30']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input">
                                          <input type="text" name="sat1" value="<?php echo $opentiming['sat1']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="sat2" value="<?php echo $opentiming['sat2']; ?>" class="timepicker form-control"/>
                                          <label class="or-label">and</label>
                                          <input type="text" name="sat3" value="<?php echo $opentiming['sat3']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="sat4" value="<?php echo $opentiming['sat4']; ?>" class="timepicker form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="form-group time-shift">
                                        <label class="form-label">Sunday:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label31']; ?></span>
                                          </span>
                                        </label>
                                        <div class="time-input">
                                          <input type="text" name="sun1" value="<?php echo $opentiming['sun1']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="sun2" value="<?php echo $opentiming['sun2']; ?>" class="timepicker form-control"/>
                                          <label class="or-label">and</label>
                                          <input type="text" name="sun3" value="<?php echo $opentiming['sun3']; ?>" class="timepicker form-control"/>
                                          <input type="text" name="sun4" value="<?php echo $opentiming['sun4']; ?>" class="timepicker form-control"/>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-12">
                                      <div class="update-button">
                                        <button class="btn btn-default bsopbutton">Mise à jour</button>
                                        <span style="display:none;" class="bsoploading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                        <div id="bsopmsg"></div>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                                <hr>
                                <div class="row add-photos">
                                  <div class="col-md-12">                                  
                                    <div class="form-group">
                                      <label class="form-label">Ajoutez vos photos:
                                        <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label32']; ?></span>
                                          </span>
                                      </label>
                                      <form id="uploadbsphoto" enctype="multipart/form-data" method="post" action="">
                                        <div class="row">
                                          <div class="col-md-9">
                                            <input type="file" name="bs_photos" class="form-control form-file">  
                                            <label style="color:#F00;" id="ptgmsg"></label>
                                          </div>
                                          <div class="col-md-3 update-button">
                                            <button class="btn btn-default ptgbutton">Upload</button>
                                            <span style="display:none;" class="ptgloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                          </div>
                                        </div>

                                        <div class="row">
                                          <div class="col-sm-12">
                                            <div class="light-list-gallery">
                                              <ul id="img-gallry-box">
                                                <?php
                                                if(!empty($gallery)){
                                                  foreach($gallery as $photo){
                                                    $path = $photo['ph_path'];
                                                ?>
                                                  <li>
                                                    <div class="up-img">
                                                      <a href="<?php echo base_url("assets/photogallery/$path"); ?>" data-lightbox="example-set">
                                                        <img src="<?php echo base_url("assets/photogallery/$path"); ?>">
                                                      </a>
                                                       <div class="del-cross">
                                                          <a href="javascript:;" modal-aria="<?php echo $photo['ph_id']; ?>" class="make-profile"><i class="fa fa-user"></i></a><br>
                                                          <a href="javascript:;" modal-aria="<?php echo $photo['ph_id']; ?>" class="delete-galphoto"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                  </li>
                                                <?php
                                                  }
                                                }
                                                ?>
                                              </ul>
                                            </div>
                                          </div>
                                        </div>

                                      </form>
                                    </div>
                                  </div>
                                </div>                                
                              </div>                            
                            </div>
                            <?php
                          }
                          ?>
                          <div class="tab-pane" id="tab-sub4">
                            <div class="mes-info-sec mesg-send">
                              <form id="boxideamessages">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="form-label">Titre:
                                      <span class="tooltip">?
                                        <span class="tooltiptext"><?php echo $suggestion['label33']; ?></span>
                                      </span>
                                    </label>
                                    <input type="text" name="title" class="form-control">
                                  </div>
                                </div>
                              </div>
                              <div class="row text-area">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="form-label">Message:
                                      <span class="tooltip">?
                                        <span class="tooltiptext"><?php echo $suggestion['label34']; ?></span>
                                      </span>
                                    </label>
                                    <textarea class="form-control text-main" name="message" rows="4"></textarea>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <button class="btn btn-default" id="msgsubmit" type="submit">Envoyer</button>
                                  <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span> 
                                  <div id="msgmessag"></div>
                                </div>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab2">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs3">
                          <li class="active"><a href="#tab2-sub1" data-toggle="tab">Mes favoris</a></li>
                          <li><a href="#tab2-sub2" data-toggle="tab">Bon plan de mes favoris</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab2-sub1">
                            <div class="mes-commercant">
                                <?php
                                if(!empty($following)){
                                  foreach($following as $flw){
                                  ?>
                                    <div class="listing-sub-mes">
                                      <div class="listing-sub">
                                        <div class="image-lis-sub">
                                          <?php $picpath = $flw['user_profile_pic'];
                                            $flid = $flw['usersid'];
                                          ?>
                                          <img src="<?php echo base_url("assets/profile_pics/$picpath"); ?>">
                                        </div>
                                        <div class="image-txt-sub">
                                          <div class="row">
                                            <div class="col-md-8">
                                              <div class="its-main">
                                                <div class="img-txt-subhead">
                                                  <h3><?php echo ucwords($flw['user_company_name']); ?></h3>
                                                  <?php
                                                  $totalreview = $this->User->getTotalReview($flid);
                                                  if(!empty($totalreview['total']))
                                                    $overall_vote_rate = round($totalreview['sum'] / $totalreview['total']);                                                
                                                  else
                                                    $overall_vote_rate = 0;
                                                  $stars = '';
                                                  $stars1 = '';
                                                                                                                                                          
                                                  ?>
                                                  <span class="rating-main">
                                                    <ul>
                                                      <?php 
                                                      for ($i = 1; $i <= $overall_vote_rate; $i++) {
                                                        $stars .= '<li><i class="fa fa-star" id="' . $i . ' aria-hidden="true"></i></li>';

                                                      }
                                                       // echo $i;
                                                      for ($ix = $overall_vote_rate+1; $ix <= 5; $ix++) {
                                                        $stars1 .= '<li><i class="fa fa-star-o" id="' . $ix . ' aria-hidden="false"></i></li>';
                                                      }
                                                      echo $stars. $stars1;
                                                      ?>
                                                      <!-- <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li> -->
                                                    </ul>
                                                  </span>
                                                </div>
                                                <p class="add-l"><i class="fa fa-map" aria-hidden="true"></i> <?php echo $flw['user_company_address'] != ""? $flw['user_company_address']: "NA"; ?></p>
                                                <p class="catgry-l"><span class="f-cat"><i class="fa fa-bars" aria-hidden="true"></i> Categorie(s): </span><span><?php if(!empty($flw['categories'])) echo $flw['categories']; echo "NA"; ?></span></p>
                                                <p class="distance-l"><span class="f-cat"><i class="fa fa-map-marker"></i>Distance: </span><span><?php if(!empty($flw['distance'])) echo $flw['distance']." KM"; else echo "NA"; ?></span></p>
                                              </div>
                                            </div>
                                            <div class="col-md-4">
                                              <div class="list-status">
                                                <ul>
                                                  <li><a href="<?php echo base_url("Users/user_profile/$flid");?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i>Afficher le numéro</a></li>
                                                  <li><a href="javascript:;" class="unfollow-user" modal-aria="<?php echo $flid; ?>" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i>Unfollow</a></li>
                                                  <li>
                                                    <a href="javascript:;" data-toggle="modal" data-target="#chat-modal" class="usermessges" modal-aria="<?php echo $flid; ?>"><i class="fa fa-envelope" aria-hidden="true"></i><?php if($flw['is_online']) echo "Live Chat"; else echo "Message" ?></a>                                                    
                                                  </li>
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php
                                  }
                                }
                                ?>                                
                            </div>
                          </div>
                          <div class="tab-pane" id="tab2-sub2">
                            <div class="mes-commercant">
                              <?php
                              if(!empty($followingEvents)){
                                foreach($followingEvents as $evnt){
                                  $picpath = $evnt['user_profile_pic'];                                  
                                  ?>
                                  <div class="listing-bon-plan">
                                    <div class="evnt-listing">
                                      <ul class="event-list">
                                        <li>
                                          <div class="mang-envt-time">
                                            <span class="day"><?php echo date("d", strtotime($evnt['event_date'])); ?></span>
                                            <span class="month"><?php echo date("M", strtotime($evnt['event_date'])); ?></span>
                                            <span class="year"><?php echo date("Y", strtotime($evnt['event_date'])); ?></span>                                            
                                          </div>
                                          <div class="mang-evnt-dicrp">
                                            <h2 class="title"><?php echo ucwords($evnt['event_name']); ?></h2>
                                            <p class="desc"><?php echo substr($evnt['event_description'], 0, 100); ?></p>
                                          </div>
                                          <div class="mang-evnt-usr-pro">
                                            <div class="profile-image">
                                              <img src="<?php echo base_url("assets/profile_pics/$picpath"); ?>"/>
                                            </div>
                                            <div class="mang-evnt-usr-profile ">
                                              <strong><?php echo ucwords($evnt['user_company_name']); ?></strong>
                                              <article>Event leader</article>
                                            </div>
                                          </div>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                  <?php
                                }
                              }else{
                                ?>
                                <div class="listing-bon-plan">
                                  <div class="evnt-listing">
                                  	<ul class="event-list">
                            					<li>
                            						<div class="mang-envt-time">
                            							<span class="day">4</span>
                            							<span class="month">Jul</span>
                            							<span class="year">2014</span>
                            							
                            						</div>
                            						<div class="mang-evnt-pic">
                                        	<img alt="Independence Day" src="https://farm4.staticflickr.com/3100/2693171833_3545fb852c_q.jpg" />
                                        </div>
                            						<div class="mang-evnt-dicrp">
                            							<h2 class="title">Independence Day</h2>
                            							<p class="desc">United States Holiday</p>
                            						</div>
                            						<div class="mang-evnt-usr-pro">
                            							<div class="profile-image">
                                            <img src="http://api.randomuser.me/portraits/med/men/71.jpg"/>
                                          </div>
                                          <div class="mang-evnt-usr-profile ">
                                            <strong>Robert White</strong>
                                            <article>Event leader and founder of this group</article>
                                          </div>
                            						</div>
                            					</li>
                                    </ul>
                                  </div>
                                </div>
                                <?php
                              }
                              ?>
                              
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane <?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "notifications") echo "active"; ?>" id="tab3">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs4">
                          <li class="<?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "") echo "active"; ?>"><a id="messagetab1" href="#tab3-sub1" data-toggle="tab">Message</a></li>
                          <li class="<?php if($this->uri->segment(3) == "notifications") echo "active"; ?>"><a id="notiftab1" href="#tab3-sub2" data-toggle="tab">Notification</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane <?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "") echo "active"; ?>" id="tab3-sub1">
                            <div class="mes-messages">
                            <?php
                            if(!empty($messagelisting)){
                              foreach($messagelisting->result() as $msg){
                                $query = $this->db->select("msg_content, msg_date, msg_reciever")->get_where("otd_user_messaging", array("msg_id"=>$msg->mx_id));
                                foreach($query->result() as $lstmsg){ }
                                $user_pic = $msg->user_profile_pic;
                              ?>
                                <div class="msg-sub-list">
                                    <div class="msg-pro-pic">
                                        <!-- <i class="fa fa-user"></i> -->
                                        <img src="<?php echo base_url("assets/profile_pics/$user_pic"); ?>"/>
                                    </div>
                                    <div class="msg-text">
                                        <h4 class="usr-name"><?php echo ucwords($msg->user_firstname." ".$msg->user_lastname); ?> <?php if($msg->unread > 0) echo "<span class='unread-msg'>".$msg->unread."</span>"; ?></h4>
                                        <p class="msg-show">
                                          <?php 
                                          //echo $msg->msg_content;
                                          if($lstmsg->msg_reciever == $this->session->userdata("user_id"))
                                            echo '<i class="fa fa-arrow-left"></i> ';
                                          else
                                            echo '<i class="fa fa-arrow-right"></i> ';                                          
                                          ?>
                                          <?php
                                          echo $lstmsg->msg_content;
                                          ?>
                                        </p>
                                    </div>
                                    <div class="msg-button-date">
                                        <span class="msg-date">
                                            <?php echo date("m/d/Y H:i a", strtotime($lstmsg->msg_date)); ?>
                                        </span>
                                        <span class="btn-msg">
                                            <button data-toggle="modal" data-target="#chat-modal" class="btn btn-default usermessges" modal-aria="<?php echo $msg->user_id; ?>">View</button>
                                        </span>
                                    </div>
                                </div>
                              <?php
                              }
                            }
                            ?>                                
                            </div>
                          </div>
                          <div class="tab-pane <?php if($this->uri->segment(3) == "notifications") echo "active"; ?>" id="tab3-sub2">
                            <div class="mes-notification">
                              <?php
                              if(!empty($notificationslisting)){
                                foreach($notificationslisting as $nt){
                                  ?>
                                  <div class="noti-sub">
                                    <span class="user-noti-link"><a href="#"><?php echo $nt['user_firstname']; ?></a></span> 
                                    <?php
                                    if($nt['nt_type'] == 1)
                                      echo "started following you.";
                                    if($nt['nt_type'] == 2)
                                      echo "has organized an event.";
                                    if($nt['nt_type'] == 3)
                                      echo "has submited a review on you profile.";
                                    ?>
                                    <span class="date-time">
                                        <span class="calender-not"><i class="fa fa-calendar"></i><?php echo date("d/m/Y", strtotime($nt['nt_date'])); ?></span>
                                        <span class="clock-not"><i class="fa fa-clock-o"></i><?php echo date("H:i", strtotime($nt['nt_date'])); ?></span>
                                    </span>
                                  </div>
                                  <?php
                                }
                              }else{
                                ?>
                                <div class="noti-sub">
                                  Sorry, Notification not available.
                                </div>
                                <?php
                              }
                              ?>                                
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              if($user['user_type'] == "Professional"){
              ?>
              <div class="tab-pane" id="tab4">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs5">
                          <li class="active"><a href="#tab4-sub1" data-toggle="tab">Acheter une options</a></li>
                          <li><a href="#tab4-sub2" data-toggle="tab">Reciept and Document</a></li>
                          <li><a href="#tab4-sub3" data-toggle="tab">Gestions des options</a></li> 
                        </ul>
                        <div class="side-txt-tab">
                            <div class="logo-paymnt">
                                <div class="main-pay">
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-visa.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-ca_e.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-cb.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-3dscan.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-master.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-sli-pay.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="logo-paymnt-sub">
                                            <a href="#"><img src="<?php echo base_url("assets/img/card-b.jpg"); ?>"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <p>Otourdemoi.Pro s'appuie sur les dernières normes de sécurité et ne dispose à aucun moment de vos informations bancaires.</p>
                             

                            <p>Otourdemoi.Pro ne collecte pas vos données bancaires et a choisi des partenaires, établissements bancaires et de paiement, reconnus et assurant un niveau de sécurité fort en conformité avec la réglementation Française.</p>
                             
                            <p>La transaction s'effectue par paiement sécurisé standard SSL de nos partenaires, CREDIT AGRICOLE et Slimpay, établissements agréés par la banque de France.</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab4-sub1">
                            <div class="mes-opt-one">
                                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                      <h4 class="panel-title">
                                     <!--  <div class="checkbox">
                                        <label><input type="checkbox" value=""></label>
                                      </div> --> 
                                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                       <h3>Head of List</h3>
                                      </a>
                                      </h4>
                                    </div>
                                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                      <div class="panel-body">
                                      <form name="optionPurchaseForm" id="optionPurchaseForm" method="post" action="<?php echo base_url();?>Users/account">

                                        <div class="abo-pre-txt">
                                            <h3>Head of List Option</h3>


        <p><?php echo $head_of_list['opt_description']; ?></p>
        <input type="hidden" name="opt_name" value="<?php echo $head_of_list['opt_name']; ?>">
        <input type="hidden" name="opt_id" value="<?php echo $head_of_list['opt_id']; ?>">
        
        <p></p>
        <h4>Option Pricing</h4>
        <p>

        <?php
/*echo "<br><br><pre>";
print_r($user_price_options);
echo "</pre>";*/
?>
        <?php
/*echo "<br><br><pre>";
print_r($price_options);
echo "</pre>";*/

        foreach ($price_options as $prows) {
          # code...
       
if($prows['opt_price_type'] == $user_price_options[0]['otp_option_duration'] )
{
echo "<input type='radio' value='".$prows['opt_price_type']."' name='price_type' checked='checked'> ".$prows['opt_price_type']. "</input>
       <input type='hidden' name='price_type_id' value='".$prows['opt_price_id']."' />
       <input type='hidden' value='".$prows['opt_price']."' name='opt_price' /> (Price: ".$prows['opt_price']. ")<br>" ;
       
}
else
{
   echo "<input type='radio'  value='".$prows['opt_price_type']."' name='price_type'> ".$prows['opt_price_type']. "</input>
       <input type='hidden' name='price_type_id' value='".$prows['opt_price_id']."' />
       <input type='hidden' value='".$prows['opt_price']."' name='opt_price' /> (Price: ".$prows['opt_price']. ")<br>" ;
        

}
    
}

?>
<br><br>
<?php
if($user_price_options[0]['opt_option_status'] == 0)
{
?>
<a href="#" >Your option is not active. Please check you email and activate the option</a>
<?php
}
else if($user_price_options[0]['opt_option_status'] == 3)
{
?>
<a href="#" >Your option is Expired. Please purchase the option</a>
<input type="submit" name="optionPurchaseSubmit" value="Buy This Option" />
<?php
}
else if($user_price_options[0]['opt_option_status'] == 1)
{
?>
<a href="#" >You have an active option. </a>
<?php
}
else
{
?>
<input type="submit" name="optionPurchaseSubmit" value="Buy This Option" />
<?php
}
?>

        </p>


                                            </div>
        </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                      <h4 class="panel-title">
                                      <div class="checkbox">
                                        <label><input type="checkbox" value=""></label>
                                      </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">ABONNEMENT PREMIUM</a> </h4>
                                    </div>
                                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <h3>Otourdemoi.Pro, première communauté entre particuliers et professionnels</h3>
                                            <p>Nous mettons à votre disposition un ensemble de moyens de communication pour vous aider à :
                                            Trouver de nouveaux clients et faites les venir à vous,
                                            Augmenter votre chiffre d’affaires,
                                            Etre plus visible sur Internet,
                                            Communiquer facilement avec vos clients et futurs clients sur votre actualité, vos actions commerciales, vos nouveaux produits/services…</p>
                                            <div class="sub-txt">
                                                <h4>L’abonnement « Premium » inclus les services suivants :</h4>
                                                <p class="high-light-txt">Apparaisser en tête de liste dans les recherches (« Tête de liste »)</p>
                                                <ul>
                                                    <li>Soyez mis en avant dans les résultats de recherches (« visibilité »)</li>
                                                    <li>Bénéficier d’un « Bon Plan » supplémentaire – 1 semaine /mois</li>
                                                    <li>Faites de la publicité avec un « Bon Plan Plus » - 1 semaine /mois</li>
                                                    <li>Améliorer votre profil avec « Profil Plus »</li>
                                                    <li>Ajouter plus de photos avec le « Pack 10 photos »</li>
                                                </ul>
                                                <p>EXCLUSIVITE : Un photographe professionnel pour vous !</p>
                                            </div>
                                            <div class="sub-txt">
                                                <p>L’abonnement Premium inclus également l’intervention d’un photographe professionnel pour mettre en valeur votre magasin/entreprise, vos produits, vos savoir-faire et/ou collaborateurs. En savoir plus</p>
                                            </div>
                                            <div class="sub-txt">
                                                <h5>Programme de Parrainage Otourdemoi.Pro :</h5>
                                                <p>Nous sommes heureux de vous offrir un chèque de 45 € pour chaque souscription d’un abonnement Premium.</p>
                                            </div>
                                            <div class="sub-txt">
                                                <h5>Aider les enfants malades et les personnes fragiles hospitalisées en France</h5>
                                                <p>Pour chaque abonnement « Premium » souscrit, Otourdemoi.Pro reverse 10€ pour soutenir la Fondation « Hôpitaux de Paris, Hôpitaux de France » ainsi que l’opération « Pièce Jaunes » qui permet d’aider les enfants malades et les personnes fragiles hospitalisées en France. <a href="#">En savoir plus</a></p>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                      <div class="checkbox">
                                        <label><input type="checkbox" value=""></label>
                                      </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">PACK CRÉATION GRAPHIQUE</a> </h4>
                                    </div>
                                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <p>En complément de nos solutions de communication dédiées pour les entreprises, Otourdemoi vous proposedifférentes prestations de création graphique pour les petits budgets jusqu’aux campagnes design sur mesure.</p>
                                            <div class="sub-txt">
                                                <p class="high-light-txt">Toutes nos prestations sont réalisées exclusivement par des infographistes ou designer professionnels.</p>
                                            </div>
                                            <div class="sub-txt">
                                                <h5>Voici des exemples de prestation que nous proposons :</h5>
                                                <p>Création graphique (logo, retouche photo, mailing invitation client, flyers, dépliants, bâches publicitaires, plaquettes, brochures, adhésif pro, …)</p>
                                                <p>Création d’animation (logo, encart publicitaire, bannières…)</p>
                                                <p>Création de votre publicité WEB ou spécifique pour application mobile ou tablette,
                                                Mini vidéo de quelques secondes pour animer/dessiner votre logo ou en image en 3D,
                                                Pack vidéo de 10 secondes, 30 secondes… jusqu’à 10 minutes pour présenter votre entreprise, vos savoir-faire, vos réalisations, pour mettre en ligne sur votre site WEB, blog, réseaux sociaux ou encore pour diffuser vos propres spots publicitaires en magasins…</p>
                                                <p>Réalisation de Vidéo Test produit avec témoignage client</p>
                                            </div>
                                            <div class="sub-txt">
                                                <h3>Différencier vous et donner une image moderne et dynamique de votre entreprise à travers nos créations graphiques professionnelles sur mesure</h3>
                                                <p>Possibilité de faire intervenir des acteurs professionnels, voix-off, comédiens sur demande. Contactez-nous pour plus de renseignements et obtenir un devis personnalisé</p>
                                            </div>
                                            <div class="form-sub-txt">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Prénom
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Nom
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Genre
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="btn-group radio-btn" data-toggle="buttons">
                                                                <label class="btn btn-default active">
                                                                  <input type="radio" name="douhavekidsprice" checked="" value="yes">
                                                                  Homme </label>
                                                                <label class="btn btn-default">
                                                                  <input type="radio" name="douhavekidsprice" value="no">
                                                                  Femme </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Téléphone
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Budget
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Description
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <textarea class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Date
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <p id="datepairExample">
                                                              <input type="text" class="form-control date" aria-describedby="basic-addon1">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-offset-5 col-md-7">
                                                        <div class="button-sbmt">
                                                            <button class="btn btn-default">soumettre</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                      <div class="checkbox">
                                        <label><input type="checkbox" value=""></label>
                                      </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree">PUBLICITÉ SUR LE BON COIN</a> </h4>
                                    </div>
                                    <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <div class="sub-txt">
                                                <h5>Vous souhaitez communiquer sur leboncoin.fr ?</h5>
                                                 
                                                <p>Et être visible de millions de clients potentiels géolocalisés dans votre ville, à proximité de votre entreprise ou commerce ? Faites-vous connaitre !</p>
                                                 
                                                <p>Contacter le service client d’Otourdemoi.Pro pour un devis gratuit personnalisé.</p>
                                                 
                                                <p>Otourdemoi.Pro négocie pour vous les meilleurs tarifs directement avec leboncoin.fr !</p>
                                                
                                                <p> 
                                                Pas besoin de compétence informatique, ni d’un infographiste, Otourdemoi.Pro vous propose des campagnes de publicité clé en main
                                                </p>
                                                <p> 
                                                Nous nous chargeons des créations graphiques de vos bannières, pavés, etc... de la mise en ligne sur le site et nous vous fournissons un rapport de campagne de vos publicités.
                                                </p>
                                                 
                                                Nous vous proposons plusieurs formats de publicité au choix selon vos envies :
                                                <ul>
                                                    <li>Bannière</li>
                                                    <li>Pavé</li>
                                                    <li>Native Ads</li>
                                                    <li>Box</li>
                                                </ul>
                                            </div>
                                            <div class="form-sub-txt">
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Prénom
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Nom
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Genre
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <div class="btn-group radio-btn" data-toggle="buttons">
                                                                <label class="btn btn-default active">
                                                                  <input type="radio" name="douhavekidsprice" checked="" value="yes">
                                                                  Homme </label>
                                                                <label class="btn btn-default">
                                                                  <input type="radio" name="douhavekidsprice" value="no">
                                                                  Femme </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Téléphone
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Budget
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <input type="text" class="form-control">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Description
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <textarea class="form-control" rows="3"></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-md-5">
                                                            <label>Date
                                                              <span class="tooltip">?
                                                                <span class="tooltiptext">message</span>
                                                              </span>
                                                            </label>
                                                        </div>
                                                        <div class="col-md-7">
                                                            <p id="datepairExample">
                                                              <input type="text" class="form-control date" aria-describedby="basic-addon1">
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-offset-5 col-md-7">
                                                        <div class="button-sbmt">
                                                            <button class="btn btn-default">soumettre</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value=""></label>
                                        </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">PROFIL PLUS</a> </h4>
                                    </div>
                                    <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <div class="sub-txt">
                                                <p>L’option « Profil Plus » vous permet <span class="hghlight-txt">d’améliorer votre profil et de vous différencier des autres entreprises.</span></p>
                                                 
                                                <p>La zone de « Description » de votre profil Entreprise passera de <strong>500 à 3 000 caractères.</strong></p>
                                                
                                                <p>Vous pourrez ainsi décrire plus précisément votre entreprise, votre activité, vos produits et services, vos savoir-faire ou encore vos spécialités...</p>
                                                <div class="form-group radio-mony">
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="15€"> 1year = 8€</label>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value=""></label>
                                        </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">VISIBILITÉ</a> </h4>
                                    </div>
                                    <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <div class="sub-txt">
                                                
                                                <p>L’option « visibilité » augmente le nombre de visite sur votre profil Entreprise.</p>
                                                 
                                                <p><span class="high-light-txt-2">Votre entreprise sera plus visible que les autres</span> dans les résultats de recherches des internautes.</p>
                                                 
                                                <p>L’ajout d’un bandeau de couleur incluant une zone de texte <strong>attire l’attention sur votre entreprise.</strong></p>
                                                <div class="form-group radio-mony">
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="15€"> 1 month = 15€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="72€"> 6 month = 72€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="5€"> 1 week = 5€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="1€"> Pay per day = 1€</label>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value=""></label>
                                        </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="false" aria-controls="collapseSeven">PACK PHOTOS</a> </h4>
                                    </div>
                                    <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSeven">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <div class="sub-txt">
                                                
                                                <p>L’option « pack photos » permet d’améliorer votre profil, de vous mettre en valeur et de vous différencier des autres entreprises.</p>
                                                 
                                                <p>L’ajout de <span class="high-light-txt-2">belles photos attire 5 à 7 fois plus de clients</span>, c’est un atout indéniable</p>
                                                 
                                                L’ajout d’un bandeau de couleur incluant une zone de texte <strong>attire l’attention sur votre entreprise.</strong>
                                                <div class="form-group radio-mony">
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="15€">10 Pictures 1year = 36€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="72€"> 20 Pictures 1year = 48€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="5€">50 Pictures 1year = 60€</label>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value=""></label>
                                        </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight">BON PLAN</a> </h4>
                                    </div>
                                    <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <div class="sub-txt">
                                                <button class="acti-valid btn btn-default" id="av-btn">Activer votre Bon Plan GRATUIT.</button>
                                                <div class="act-valid-sec">
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <div class="col-md-4">
                                                                <label>Nom de votre Bon Plan</label>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text" class="form-control">
                                                            </div>
                                                            <div class="col-md-2">
                                                                <button class="validate-btn btn btn-default">Valider</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                      <h4 class="panel-title">
                                        <div class="checkbox">
                                          <label><input type="checkbox" value=""></label>
                                        </div>
                                      <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">BON PLAN PLUS</a> </h4>
                                    </div>
                                    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                      <div class="panel-body">
                                        <div class="abo-pre-txt">
                                            <div class="sub-txt">
                                                <p>Une entreprise qui propose des « Bons Plans Plus » attire 15 fois plus de clients qu’une entreprise qui n’en propose pas !</p>
                                                <p>Un « Bon Plan Plus » est identique à un « Bon Plan » mais il inclut en plus un espace publicitaire qui vous permettra d’être visible par des milliers de personnes dans la même zone géographique que votre entreprise</p>
                                                <p>
                                                Un « Bon Plan Plus » vous permet de faire de la publicité sur vos actions commerciales ponctuelles ou sur le long terme.</p>
                                                <h3 class="hight-lght-4">Garder le contact avec vos clients et prospects !</h3>
                                                <p>
                                                Il n’a jamais été aussi facile de tenir informé en temps réel tous vos clients et prospects !</p>
                                                <p>
                                                Chaque « Bon Plan Plus » sera publié sur l’espace publicitaire des bons plans ainsi que sur votre profil Entreprise et il sera automatiquement diffusé sur les mobiles et les mails de tous vos clients et de toutes les personnes qui auront cliqué sur « J’aime cette entreprise » sur votre profil Entreprise.</p>
                                                <p>
                                                Communiquer simplement et efficacementsur votre actualité, vos actions commerciales :
                                                </p>
                                                <p>
                                                Promotion, animation, plat du jour, vente privée, soirée à thèmes, nouveaux produits, nouvelle collection, cours de cuisine/œnologie…, porte ouverte, dégustation, visite de chantier, démonstration, essai de véhicule, ventes flash, offres spéciales pour Noël, fêtes des mères, St Valentin, coupe du monde, Halloween…, ouverture exceptionnelle…</p>
                                                <div class="form-group radio-mony">
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="15€">10 Pictures 1year = 36€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="72€"> 20 Pictures 1year = 48€</label>
                                                    <label class="rdio-mny"><input type="radio" name="value-m" value="5€">50 Pictures 1year = 60€</label>
                                                </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  

                                </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab4-sub2">
                            <div class="mes-opt-two">
                                <div class="select-form">
                                    <select id="optionlist" name="optionlist" class="form-control">
                                        <option value="0">Select Option</option>
                                        <option value="1">Profil plus</option>
                                        <option value="2" selected="selected">Visibilité</option>
                                        <option value="3">Tête de liste</option>
                                        <option value="4">A la Une</option>
                                        <option value="5">Pack Photos</option>
                                        <option value="6">Events</option>
                                        <option value="8">Encart publicitaire dans votre ville (Page d'acceuil)</option>
                                        <option value="9">Bon Plan</option> <option value="10">Bon Plan Plus</option>
                                        <option value="11">Exclusivité - Touch &amp; Go – Publicité sur mobiles géolocalisés en temps réel </option>
                                        <option value="12">Envoi de SMS publicitaires à des clients géolocalisés à 200M de votre point de vente (en partenariat avec SFR)</option>
                                        <option value="13">Pack Sms Géociblé</option>
                                        <option value="14">Pack Email B2B</option>
                                        <option value="16">Pack Email B2C</option>
                                        <option value="18">Pack de création graphique</option>
                                        <option value="19">A la Une Plus</option>
                                        <option value="20">Publicité sur internet</option>
                                        <option value="21">Publicité sur Facebook</option>
                                    </select>
                                </div>
                                <div class="content-select">
                                    
                                    <table>
                                        <tr>
                                            <th class="head-cs">Option</th>
                                            <th class="head-cs">Date of purchase</th>
                                            <th class="head-cs">Facture</th>
                                        </tr>
                                        <tr class="cs-cnt">
                                            <td>Dummy Content</td>
                                            <td>Dummy Content</td>
                                            <td>Dummy Content</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                          </div>
                          <div class="tab-pane" id="tab4-sub3">
                            <div class="mes-opt-thre">
                                <div class="facture-sec">
                                    <div class="facture-link">
                                        <a href="#" class="btn btn-default">Facture</a>
                                    </div>
                                    <div class="table-responsive">
                                        <table>
                                            <tr>
                                                <th class="head-cs">Option</th>
                                                <th class="head-cs">Durée achetée</th>
                                                <th class="head-cs">Prix</th>
                                                <th class="head-cs">Date d'achat</th>
                                                <th class="head-cs">Date de fin de</th>
                                                <th class="head-cs">Date de début de</th>
                                                <th class="head-cs">Modifier</th>
                                                <th class="head-cs">Facture</th>
                                                <th class="head-cs">Information</th>
                                            </tr>
                                            <tr class="cs-cnt">
                                                <td>Bon Plan Plus</td>
                                                <td>1 month</td>
                                                <td>7€</td>
                                                <td>2017-05-08 20:47:40</td>
                                                <td>2017-05-08</td>
                                                <td>2017-06-07</td>
                                                <td><a href="#"><i class="fa fa-edit"></i></a></td>
                                                <td><a href="#"><i class="fa fa-download"></i></a></td>
                                                <td><a href="#"><i class="fa fa-info-circle" aria-hidden="true"></i></a></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane event-create" id="tab5">
                <div class="mes-info-sec">
                  <form method="post" action="" id="createevent" autocomplete="off" >
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Event name:
                            <span class="tooltip">?
                              <span class="tooltiptext"><?php echo $suggestion['label35']; ?></span>
                            </span>
                          </label>
                          <input type="text" name="ename" class="form-control">
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="form-label">Event Date:
                            <span class="tooltip">?
                              <span class="tooltiptext"><?php echo $suggestion['label36']; ?></span>
                            </span>
                          </label>
                          <input type="text" name="edate" class="form-control datepickerfuture">
                        </div>
                      </div>
                    </div>
                    <div class="row">                      
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="form-label confm-pass">Start Time:
                              <span class="tooltip">?
                                <span class="tooltiptext"><?php echo $suggestion['label37']; ?></span>
                              </span>
                            </label>
                            <input type="text" name="estime" class="form-control timepicker">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label class="form-label confm-pass">End Time:
                              <span class="tooltip">?
                                <span class="tooltiptext"><?php echo $suggestion['label38']; ?></span>
                              </span>
                            </label>
                            <input type="text" name="eetime" class="form-control timepicker">
                          </div>
                        </div>                      
                    </div>
                    <div class="row">                      
                      <div class="col-md-12">
                        <div class="form-group">
                          <label class="form-label">Event details:
                            <span class="tooltip">?
                              <span class="tooltiptext"><?php echo $suggestion['label39']; ?></span>
                            </span>
                          </label>
                          <textarea class="textarea-main" name="description" placeholder="Description"></textarea>
                        </div>
                      </div>                        
                      <div class="col-md-12">
                        <button type="submit" class="chnge-mail btn btn-default upps">Create Event</button>
                        <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                        <div id="eventmsg"></div>
                      </div>                      
                    </div>                    
                  </form>
                </div>
              </div>
              <?php
              }
              ?>

              

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<!-- User Chat model -->
  <div id="chat-modal" class="modal chat-modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              
              <div class="prof-msg-details">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="msg-pro-pic-sec">
                              <div class="msg-profile-pic">
                                  <!-- <i class="fa fa-user"></i> -->
                                  <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg"/>
                              </div>
                              <div class="msh-prof-details">
                                  <a href="#"><h3 id="msgusername"></h3></a>
                              </div>
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-body messagebody" data-mcs-theme="dark">
            <div class="usermessageslisting">
            </div>
          </div>
          <div class="modal-footer">
              <div class="row">
                  <div class="col-md-12">
                      <div class="reply-sec-msg">
                          <form method="post" action="" id="sendusermessage">
                              <div class="form-group">
                                <textarea class="form-control mCustomScrollbar" wrap="off" rows="2" id="comment" placeholder="Enter Your Message..." name="usermessage"></textarea>
                              </div>
                              <input type="hidden" id="tosend" name="tosend" value="" />
                              <button type="submit" class="btn btn-default msgsubmit">Reply</button><span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span> 
                          </form>
                      </div>
                  
                  </div>
              </div>
          </div>
      </div>

    </div>
  </div>
<!-- User Chat Model end -->

<!-- User Chat model -->
  
  <div id="addphonenumbers" class="modal chat-modal fade" role="dialog">
    <div class="modal-dialog"> 
      
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <div class="prof-msg-details">
            <div class="row">
              <div class="col-xs-12">
                <div class="msg-pro-pic-sec">
                  <div class="msh-prof-details">
                    <h3>Add New Number</h3>
                  </div>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-body review-body messagebody" data-mcs-theme="dark">
          <div class="col-md-12">
            <div class="reply-sec-msg">
              <form method="post" action="<?php echo base_url("Users/addNewPhone"); ?>" id="checkavailablenumbers">
                <div class="form-group">
                  <label>Number Type</label>
                  <input type="text" class="form-control" placeholder="Enter Type" name="phlabel" />
                </div>
                <div class="form-group">
                  <label>Phone Number</label>
                  <input type="text" class="form-control" placeholder="Enter Numbers" name="phone" />
                  <div id="nummsg"></div>
                </div>
                <button type="submit" class="btn btn-default addnm">Add</button>
                <button type="button" class="btn btn-default closeaddnum" data-dismiss="modal">Close</button>
              </form>
            </div>
          </div>
        </div>
        <!-- <div class="modal-footer">
            </div> --> 
      </div>
    </div>
  </div>
<!-- User Chat Model end -->