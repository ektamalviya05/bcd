<section class="paymnt-method">	
<div class="container">	
	<div class="row">
		<div class="paymentCont">
			<?php
				// echo "<pre>";
	   //          print_r($this->session->userdata("purchase_option"));
	   //          echo "</pre>";
	            $purchase_option = $this->session->userdata("purchase_option");
	            $onlyslimpay = false;
	            if($purchase_option['opt_option_type'] == 4){
	            	$onlyslimpay = true;
	            }
            ?>
			<div class="headingWrap">
					<h3 class="headingTop text-center"><?php echo $texts['label1']; ?></h3>	
					<p class="text-center"><?php echo $texts['label2']; ?></p>
			</div>
			<div class="paymentWrap">
				<div class="btn-group paymentBtnGroup btn-group-justified" >
					<a href="javascript:;" class="btn paymentMethod" data-toggle="modal" data-target="#userDetailsModal">
		            	<div class="method visa"></div>		               
		            </a>
					<?php
					if($onlyslimpay){
						?>
						<label class="btn paymentMethod">
			            	<div class="method master-card disablemode"></div>		                
			            </label>
			            <label class="btn paymentMethod">
		            		<div class="method amex disablemode"></div>		                
			            </label>	
			            <?php
					}else{
						?>		            
			            <a href="javascript:;"  class="btn paymentMethod">
			            	<div class="method master-card"></div>		                
			            </a>
			            <a href="<?php echo base_url("Users/paypalInteraction"); ?>" class="btn paymentMethod">
		            		<div class="method amex"></div>
			            </a>			            
			            <?php
		        	}
		            ?>
		            
		             
		         
		        </div>        
			</div>
            
            <div class="abt-paymnt">
            	<?php echo $texts['label3']; ?>
            </div>
			<div class="footerNavWrap clearfix">
				<a href="<?php echo base_url("Users/account/messoptions"); ?>" class="btn btn-success pull-left btn-fyi-black"> <i class="fa fa-angle-left"></i><?php echo $texts['label4']; ?></a>
				<!-- <div class="btn btn-success pull-right btn-fyi-yellow"><i class="fa fa-money"></i>Pay Now</div> -->
			</div>
		</div>
	</div>
</div>
</section>
<!-- Business user details model-->  
	<div id="userDetailsModal" class="modal chat-modal fade" role="dialog">
	  	<div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      	<form id="checkslimpayuserdetails" method="POST" action="<?php echo base_url("Users/slimpayInteraction"); ?>" autocomplete="off">
			        <div class="modal-header">
			          	<div class="prof-msg-details">
				            <div class="row">
				              	<div class="col-xs-12">
					                <div class="msg-pro-pic-sec">
						                <div class="msh-prof-details">
						                  <h3><?php echo $userinfoconfirm['label1']; ?></h3>
						                </div>
						                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>	                  	
					                </div>
				              	</div>
				            </div>
			          	</div>
			        </div>
			        <div class="modal-body review-body" data-mcs-theme="dark">
			        	<div class="col-sm-12">
			        		<div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label"><?php echo $userinfoconfirm['label17']; ?>
					                  		<span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label19']; ?></span>
						                    </span>
						                </label>
					                    <div class="btn-group" data-toggle="buttons">					                      
											<label class="btn btn-default <?php if($userdetails['bs_honorific'] == "Mr") echo "active"; ?>">
					                        	<input type="radio" name="honorificPrefix" <?php if($userdetails['bs_honorific'] == "Mr") echo "checked"; ?> value="Mr"> <?php echo $userinfoconfirm['label2']; ?>
					                      	</label>
					                      	<label class="btn btn-default <?php if($userdetails['bs_honorific'] == "Miss") echo "active"; ?>">
					                        	<input type="radio" name="honorificPrefix" <?php if($userdetails['bs_honorific'] == "Miss") echo "checked"; ?> value="Miss"> <?php echo $userinfoconfirm['label3']; ?>
					                      	</label>
					                      	<label class="btn btn-default <?php if($userdetails['bs_honorific'] == "Mrs") echo "active"; ?>">
					                        	<input type="radio" name="honorificPrefix" <?php if($userdetails['bs_honorific'] == "Mrs") echo "checked"; ?> value="Mrs"> <?php echo $userinfoconfirm['label4']; ?>
					                      	</label>
					                    </div>					                  
					                </div>
					            </div>
					            <div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label"><?php echo $userinfoconfirm['label5']; ?>:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label20']; ?></span>
						                    </span>
						                </label>
					                  	<input type="text" name="phone" value="<?php echo $userdetails['user_phone']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label5']; ?>">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label"><?php echo $userinfoconfirm['label6']; ?>:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label21']; ?></span>
						                    </span>
						                </label>
					                  	<input type="text" name="fname" value="<?php echo $userdetails['user_firstname']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label6']; ?>">
					                </div>
				              	</div>
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label"><?php echo $userinfoconfirm['label7']; ?>:
						                    <span class="tooltip">?
						                      <span class="tooltiptext"><?php echo $userinfoconfirm['label22']; ?></span>
						                    </span>
					                  	</label>
					                  	<input type="text" name="lname" value="<?php echo $userdetails['user_lastname']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label7']; ?>">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label"><?php echo $userinfoconfirm['label8']; ?>:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label23']; ?></span>
						                    </span>
						                </label>
					                  	<input type="text" name="companyname" value="<?php echo $userdetails['user_company_name']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label8']; ?>">
					                </div>
				              	</div>
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label"><?php echo $userinfoconfirm['label9']; ?>:
						                    <span class="tooltip">?
						                      <span class="tooltiptext"><?php echo $userinfoconfirm['label24']; ?></span>
						                    </span>
					                  	</label>
					                  	<input type="text" readonly="readonly" value="<?php echo $userdetails['user_company_number']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label9']; ?>">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-12">
					                <div class="form-group">
						                <label class="form-label"><?php echo $userinfoconfirm['label10']; ?>:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label25']; ?></span>
						                    </span>
						                </label>
					                  	<input placeholder="<?php echo $userinfoconfirm['label10']; ?>" readonly="readonly" class="form-control " type="text" value="<?php echo $userdetails['user_email']; ?>">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				              	<div class="col-md-12">
					                <div class="form-group">
						                <label class="form-label"><?php echo $userinfoconfirm['label11']; ?>:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label26']; ?></span>
						                    </span>
						                </label>
					                  	<input id="autocomplete" placeholder="<?php echo $userinfoconfirm['label11']; ?>" name="company_address" class="form-control " onfocus="geolocate()" type="text" value="<?php echo $userdetails['user_company_address']; ?>">
					                </div>
				              	</div>				              	
				            </div>
				            <div class="row">
				              	<div class="col-md-6">
					                <div class="form-group">
						                <label class="form-label"><?php echo $userinfoconfirm['label12']; ?>:
						                    <span class="tooltip">?
						                      	<span class="tooltiptext"><?php echo $userinfoconfirm['label27']; ?></span>
						                    </span>
						                </label>
					                  	<input type="text" name="usercity" value="<?php echo $userdetails['bs_city']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label12']; ?>">
					                </div>
				              	</div>
				              	<div class="col-md-6">
					                <div class="form-group">
					                  	<label class="form-label"><?php echo $userinfoconfirm['label13']; ?>:
						                    <span class="tooltip">?
						                      <span class="tooltiptext"><?php echo $userinfoconfirm['label28']; ?></span>
						                    </span>
					                  	</label>
					                  	<input type="text" name="postalcode" value="<?php echo $userdetails['bs_zipcode']; ?>" class="form-control" placeholder="<?php echo $userinfoconfirm['label13']; ?>">
					                </div>
				              	</div>
				            </div>
				            <div class="row">
				            	<div class="col-md-12">
					            	<div class="form-group termlink">
					            		<label><input type="checkbox" checked name="acceptterms" id="proconfidential" value='1'/> <?php echo $userinfoconfirm['label14']; ?></label>
					            	</div>
					            </div>
				            </div>
			          	</div>
			        </div>
			        <div class="modal-footer">
			          	<div class="form-group">
				            <button type="button" id="evtsubmit" class="btn" name="confirmuserdetails"> <?php echo $userinfoconfirm['label15']; ?></button>
				            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $userinfoconfirm['label16']; ?></button>
				            <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
				            <div class="userdetailsermsg"></div>
			          	</div>
			        </div>
		      	</form>
		    </div>
	  	</div>
	</div>
<!-- Business user details model end -->
<!-- Confirm for slimpay -->
	<div class="modal fade" id="usemandateconfirmbox" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
			  	<div class="modal-header"> 
				    <h4 class="modal-title"></h4>
				    <div class="title-imge">
				      	<img src="<?php echo base_url();?>assets/img/logo.png">
				    </div>
			  	</div>
			  	<div class="modal-body">
				    <div class="modal-login-txt">
				    	<h3><?php echo $usermandateexists['label3']; ?></h3>
				    </div>
			  	</div>
			  	<div class="modal-footer">
			  		<button type="button" id="evtsubmit" class="btn" name="confirmuserpermission"> <?php echo $usermandateexists['label1']; ?></button>
			     	<button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $usermandateexists['label2']; ?></button>
			     	<span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
			 	</div>
			</div>
		</div>
	</div>
<!-- Confirm for slimpay -->

<!-- Slimpay transation failed message -->
    <div class="modal fade" id="slimpaytransactionfailed" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header"> 
            <h4 class="modal-title"><?php echo $transfailed['label1'] ?></h4>
            <div class="title-imge">
              <img src="<?php echo base_url();?>assets/img/logo.png">
            </div>
          </div>
          <div class="modal-body">
            <div class="modal-login-txt">
              <h3><?php echo $transfailed['label2'] ?></h3>         
            </div>
          </div>
          <div class="modal-footer">
             <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $transfailed['label3'] ?></button>
          </div>
        </div>
      </div>
    </div>      
<!-- Slimpay transation failed message end-->  