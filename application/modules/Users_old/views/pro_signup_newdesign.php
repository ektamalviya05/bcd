    <script type="text/javascript">
      var onloadCallback = function() {
        grecaptcha.render('html_element', {'sitekey' : '6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M'
        });
      };
    </script>

<div class="form_container">
    <form action="" method="post">



      <div class="row">
        <div class="col-md-5">
          <div class="reg-text">
            <h3>Create your account <strong>Professional</strong></h3>
            <p>Otourdemoi.Pro is not a directory of professionals as it already exists ... It is a unique community aimed at bringing together professionals from the private sector by promoting exchanges and strengthening the ties of trust and proximity. Together, let us act to preserve quality products and services, at the right price and a stone's throw from our home. Choosing crafts means choosing quality and trust!</p>
          </div>
        </div>
        <div class="col-md-7">
          <div class="reg-form-main">
            <form>
            <!--   <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" class="form-control inline_block" name="user_firstname" placeholder="PRÉNOM"  value="<?php echo set_value('user_firstname'); ?>">
          error<?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="text" class="form-control inline_block" name="user_lastname" placeholder="NOM"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>
             <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label class="gender-main">GENRE</label>
                    <div class="for-radio">
                      <div class="btn-group" data-toggle="buttons">
                        
 <?php
            if(!empty($user['user_gender']) && $user['user_gender'] == 'Female'){
                $fcheck = 'checked="checked"';
                $mcheck = '';
            }else{
                $mcheck = 'checked="checked"';
                $fcheck = '';
            }
            ?>

                        <label class="btn btn-default active">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Male">Homme
                        </label>
                        <label class="btn btn-default">
                          <input type="radio" name="user_gender" <?php echo $mcheck; ?> value="Female">Femme
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" class="form-control inline_block" name="user_phone"  placeholder="TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div> -->
           <!--   <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" class="form-control inline_block" name="user_company_name" placeholder="NOM DE L'ENTREPRISE"  value="<?php echo set_value('user_company_name'); ?>">
          <?php echo form_error('user_company_name','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" class="form-control" placeholder="NUMÉRO DE SIRET">
                  </div>
                </div>
              </div>-->
          <!--    <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="language-main">LANGUE</label>
                    <div class="check-box-inline">

                      <label class="checkbox-inline">
                          <input type="checkbox" value="English" name="user_language[]">English
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="French" name="user_language[]">French
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="Spanish" name="user_language[]">Spanish
                        </label>
                        <label class="checkbox-inline">
                          <input type="checkbox" value="Arab" name="user_language[]">Arab
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="italion" name="user_language[]">italion
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="Germen" name="user_language[]">Germen
                        </label>



                    </div>
                  </div>
                </div>
              </div>-->
              <!--<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
              <input type="text" class="form-control" name="user_categories" placeholder="CHOIX DE VOTRE/VOS CATÉGROIE(S)">

              

                  </div>
                </div>
              </div>-->
             <!-- <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                        
           
  <input id="autocomplete" placeholder="VEUILLEZ SAISIR VOTRE ADRESSE" 
  name="user_company_address" class="form-control inline_block" onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>"></input>
   
         <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?>
        


                  </div>
                </div>
              </div>-->

              <!--<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      
                      <input type="email" class="form-control inline_block" name="user_email"  placeholder="Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                <input type="password" class="form-control inline_block" name="user_password" placeholder="MOT DE PASSE" >
          <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="password" class="form-control inline_block" name="conf_password" placeholder="CONFIRMER LE MOT DE PASSE" >
          <?php echo form_error('conf_password','<span class="help-block">','</span>'); ?>
                  </div>
                </div>
              </div>-->
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="auth_check" id="auth_check" value="1" checked="checked"><span>Souhaitez-vous recevoir des offres promotionnelles de la part d’Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)</span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="sms_check" id="sms_check" value="1" checked="checked"><span>Souhaitez-vous recevoir sur votre mobile des offres promotionnelles de la part d’Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)</span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="confidential" required="" id="confidential"  value="1"><span>Acceptez les conditions générales</span>
                    </label>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="recaptcha">
                    <img src="<?php echo base_url();?>assets/img/robot.png">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" name="regisSubmit" type="submit">S'enregistrer</button>

                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
  
</form>
</div>