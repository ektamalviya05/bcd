<?php
echo "<pre>";
print_r($business);
echo "</pre>";
?>
  <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
    <span id="shownumberexits" data-dismiss="modal" data-toggle="modal" data-target="#myModaComp"></span>

<?php
if( trim($is_claiming) == "yes")
{
?>
<script type="text/javascript">
$(document).ready(function() {
  $("#shownumberexits").trigger("click");
});
</script>
<?php
}
?>



<div class="modal fade" id="myModaComp" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Claim Your Business</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <div class="row">
           <div class="col-md-12">
             <form action="" method="post" name="myForm" >
				<span>(*) Required Fields</span>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" class="form-control " readonly="" name="user_company_name" value="<?php echo $business['user_company_name']; ?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" readonly="" class="form-control" name="user_company_number" value="<?php echo $business['user_company_number']; ?>">
                       
                  </div>
                </div>
</div>

 <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter first name." class="form-control" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo set_value('user_firstname'); ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="text" title="Please enter last name."  class="form-control"  name="user_lastname" placeholder="NOM"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                 
                     </div>
                </div>
</div>


<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="email" class="form-control" title="Please enter a valid email."   name="user_email"  placeholder="*Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter telephone number."  required="" maxlength="10" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

</div>

<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                     <input id="autocomplete" placeholder="*VEUILLEZ SAISIR VOTRE ADRESSE" required=""
  name="user_company_address" class="form-control " onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>"></input>
   
         <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?>
        


                  </div>
                </div>
</div>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="email" class="form-control" title="Please enter a valid email."   name="user_email"  placeholder="*Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter telephone number."  required="" maxlength="10" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>
                
</div>

<div class="row">
        <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="confidential" required="" id="personalconfidential"  value="1"><span>*Acceptez les conditions générales</span>
                    </label>
                  </div>
                    <div class="g-recaptcha" data-sitekey="6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M"></div>
 <?php echo form_error('g-recaptcha-response','<span class="help-block">','</span>'); ?>
		</div>
</div>

<div class="row">
                <div class="col-md-12">
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" value="S'enregistrer" name="claimSubmit" type="submit">S'enregistrer</button>
                  </div>
                </div>
</div>

    </form>
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
  

  
      </div>
    </div>
  </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>

<script type="text/javascript">
    $(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='myForm']").validate({
	rules: {
      user_phone:"required",
      user_firstname: "required",
      user_lastname: "required",
      user_email: {
        required: true,
        email: true
      },
      user_company_address:"required",
      confidential:"required"      
    },
    messages: {
      user_phone:"Please enter phone number",
      user_firstname: "Please enter your firstname",
      user_lastname: "Please enter your lastname",
      user_categories: "Please select business category",
      user_company_address:"Please enter company address",      
      confidential:"Please select terms & conditions",
      user_email: {
        required:"Please enter a valid email address",
        email:"Please enter valid email address"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    

<script>
       var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'],  componentRestrictions: {country: "fr"}});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script> 
    <!--
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script> 
-->