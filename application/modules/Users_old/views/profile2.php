<section>
	<div class="usr-map-section">
    	<div class="busines-map">
        	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3306.335754803177!2d-118.2671437847854!3d34.03525748061051!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c7c58fb261e9%3A0x6e3f7be3b4ca5c3a!2s1530+S+Olive+St%2C+Los+Angeles%2C+CA+90015%2C+USA!5e0!3m2!1sen!2sin!4v1500438416557" width="" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div> 
    </div>
</section>

<div class="usr-profile-wrapper">
	<div class="container">
    	<div class="col-md-12">
       
        	<div class="usr-busns-dtl">
               <div class="usr-profile-dtl-left">
                	<div class="pg-list-1-left"> <a href="#"><h3>Taj Luxury Hotel &amp; Resorts</h3></a>
					<div class="list-rat-ch"> <span>5.0</span> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> <i class="fa fa-star" aria-hidden="true"></i> </div>
					<h4>Express Avenue Mall, Los Angeles</h4>
					<p><b>Address:</b> 28800 Orchard Lake Road, Suite 180 Farmington Hills, Los Angeles, USA.</p>
					<div class="list-number pag-p1-phone">
						<ul>
							<li><a href="#"><i class="fa fa-phone" ></i> +01 1245 2541</a></li>
							<li><a href="#"><i class="fa fa-envelope" ></i> localdir@webdir.com</a></li>
							<li><a href="#"><i class="fa fa-user" ></i> johny depp</a></li>
                            <li><a href="#"><i class="fa fa-globe"></i> http:tajhotel.com</a></li>
                            <li><a href="#"><i class="fa fa-clock-o"></i> Daily : 10 am to 12 pm</a></li>
						</ul>
					</div>
				</div>
                <div class="busns-dtl-pera">
                  <div class="years-in-business"><div class="count"><div class="number">97</div></div><span><b>YEARS</b> IN BUSINESS</span></div>
                  <dl class="lines">                                              
                      <dt>Spoken Languages</dt>
                      <dd>Arabic, Chinese, English, Korean, Spanish</dd>  
                  </dl>
                                   
                </div>
                </div>
                
                <div class="usr-busns-img">
            	<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
            </div>
                </div>

        
        
        </div>
    	<div class="col-md-8">
        	<div class="usr-profile-dtl">
            	
                
                <div class="usr-busns-dtl">
                    
                	<div class="busns-dtl-lable">Business Description</div>
                    <div class="busns-dtl-pera">
                    <p>Taj Luxury Hotels & Resorts presents award winning luxury hotels and resorts in India, Indonesia, Mauritius, Egypt and Saudi Arabia.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution </p>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet.</p>
                    </div>
                </div>
                
                <div class="usr-busns-dtl">
                	<div class="busns-dtl-lable">FEATURES</div>
                    <div class="busns-dtl-pera">
                    <ul class="bullets">
                        <li><i class="fa fa-check"></i>Free Parking</li>
                        <li><i class="fa fa-check"></i>Cards Accepted</li>
                        <li><i class="fa fa-check"></i>Wi-Fi</li>
                        <li><i class="fa fa-check"></i>Air Condition</li>
                        <li><i class="fa fa-check"></i>Reservations</li>
                        <li><i class="fa fa-check"></i>Teambuildings</li>
                        <li><i class="fa fa-check"></i>Places to seat</li>
                        <li><i class="fa fa-check"></i>Winery</li>
                        <li><i class="fa fa-check"></i>Draft Beer</li>
                        <li><i class="fa fa-check"></i>LCD</li>
                        <li><i class="fa fa-check"></i>Saloon</li>
                        <li><i class="fa fa-check"></i>Free Access</li>
                        <li><i class="fa fa-check"></i>Terrace</li>
                        <li><i class="fa fa-check"></i>Minigolf</li>
                    </ul>
                    </div>
                </div>
                
                <div class="usr-busns-dtl">
                	<div class="busns-dtl-lable">OPENING HOURS</div>
                    <div class="busns-dtl-pera">
                    <dl class="lines">
                        <dt>Monday</dt>
                        <dd>08:00 am - 11:00 pm</dd>
                        <dt>Tuesday</dt>
                        <dd>08:00 am - 11:00 pm</dd>
                        <dt>Wednesday</dt>
                        <dd>08:00 am - 11:00 pm</dd>
                        <dt>Thursday</dt>
                        <dd>08:00 am - 11:00 pm</dd>
                        <dt>Friday</dt>
                        <dd>08:00 am - 11:00 pm</dd>
                        <dt>Saturday</dt>
                        <dd>08:00 am - 11:00 pm</dd>
                    </dl>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
        	<div class="usr-profile">     
             <div class="usr-busns-dtl">
             	<div id="gallery-con">
	<div id="gallery-main">
		<img src="./images/content/001.jpg" />
	</div>
	<div id="gallery-hidden">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
		<img src="<?php echo base_url("assets/img/XsR67KZ0Oc.jpg"); ?>">
	</div>
	<div id="thumbnails">
		<div id="left-arrow" class="ui-button"><div class="icon icon-arrow-left"></div></div>
		<div id="thumbcon">

		</div>
		<div id="right-arrow" class="ui-button"><div class="icon icon-arrow-right"></div></div>
	</div>

</div>
             </div>       
            </div>
        </div>
    </div>
</div>

<script>
//SET THESE VARS
var $transitionLength = 400;
var $timeBetweenTransitions = 4000;

//STORAGE
var imageCount = 0;
var currentImageIndex = 0;
var currentScrollIndex = 1;
var $imageBank = [];
var $thumbBank = [];
var $mainContainer = $("#gallery-main");
var $thumbContainer = $("#thumbcon");
var $progressBar = $("#progressbar");
var currentElement;

//CONTROLS
var $go = true;

$(document).ready(function(){

	$("#gallery-hidden img").each(function() {
		$imageBank.push($(this).attr("id", imageCount));
		imageCount++;
	});

	generateThumbs();

	setTimeout(function () {
		imageScroll(0);
	}, $timeBetweenTransitions);

	$('#left-arrow').click(function () {
		thumbScroll("left");
		toggleScroll(true);
    });

	$('#right-arrow').click(function () {
		thumbScroll("right");
		toggleScroll(true);
    });

	$('#thumbcon img').on('click',function () {

		imageFocus(this);
	});

	$('#playtoggle').click(function () {
		toggleScroll(false);
	});
});

function progress(imageIndex){
	var parts = 960/imageCount-1;
	var pxProgress = parts*(imageIndex+1);

	$progressBar.css({ width: pxProgress , transition: "all 0.7s ease"});
}

function imageFocus(focus){
	for(var i = 0; i < imageCount; i++){
		if($imageBank[i].attr('src') == $(focus).attr('src')){
			$mainContainer.fadeOut($transitionLength);
			$thumbBank[currentImageIndex].removeClass("selected");
			setTimeout(function () {
				$mainContainer.html($imageBank[i]);
				$thumbBank[i].addClass("selected");
				$mainContainer.fadeIn($transitionLength);
			}, $transitionLength);
			currentScrollIndex = i+1;
			currentImageIndex = i;
			progress(currentImageIndex);
			toggleScroll(true);

			return false;
		}
	}
}

function toggleScroll(bool){
	if($go){
		$go = false;
		$('#playtoggle').children().removeClass('icon-pause').addClass('icon-play');
	}else{
		$go = true;
		$('#playtoggle').children().removeClass('icon-play').addClass('icon-pause');
	}

	if(bool){
		$go = false;
		$('#playtoggle').children().removeClass('icon-pause').addClass('icon-play');
	}
}

function autoScroll(){
	if(currentScrollIndex >= 0 || currentScrollIndex < imageCount){
		if(currentScrollIndex+1 > imageCount){
			$thumbBank[0].css({ marginLeft: "0" , transition: "all 1.0s ease"});
			currentScrollIndex = 1;
		}else if(currentScrollIndex+1 >= 3){
			if(currentScrollIndex+2 >= imageCount){

			}else
				$thumbBank[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});

			currentScrollIndex++;
		}else{
			currentScrollIndex++;
		}
	}
}

function thumbScroll(direction){
	if(currentScrollIndex >= 0 || currentScrollIndex < imageCount){
		var marginTemp = currentScrollIndex;
		if(direction == "left"){
			if(currentScrollIndex-3 <= 0){
				var k = ((imageCount-4)*200)-5;
				$thumbBank[0].css({ marginLeft: -k , transition: "all 1.0s ease"});
				currentScrollIndex = imageCount-1;
			}else{
				$thumbBank[0].css({ marginLeft: "+=200" , transition: "all 1.0s ease"});
				currentScrollIndex--;
			}
		}else if(direction == "right"){
			if(currentScrollIndex+3 >= imageCount){
				$thumbBank[0].css({ marginLeft: "5px" , transition: "all 1.0s ease"});
				currentScrollIndex = 1;
			}else{
				$thumbBank[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});
				currentScrollIndex++;
			}
		}
	}
}

function generateThumbs(){
	progress(currentImageIndex);
	for(var i = 0; i < imageCount; i++){

		var $tempObj = $('<img id="'+i+'t" class="thumb" src="'+$imageBank[i].attr('src')+'" />');

		if(i == 0)
			$tempObj.addClass("selected");

		$thumbContainer.append($tempObj);
		$thumbBank.push($tempObj);

	}
}

function imageScroll(c){
	if($go){

		$thumbBank[c].removeClass("selected");

		c++

		if(c == $imageBank.length)
			c = 0;

		$mainContainer.fadeOut($transitionLength);
		setTimeout(function () {
			$mainContainer.html($imageBank[c]);
			$thumbBank[c].addClass("selected");
			autoScroll("left");
			$mainContainer.fadeIn($transitionLength);
		}, $transitionLength);

	}

	progress(c);

	setTimeout(function () {
		imageScroll(currentImageIndex);
	}, $timeBetweenTransitions);

	currentImageIndex = c;
}


</script>

<!-- Pradeep  -->


    <span id="shownumberexits" data-dismiss="modal" data-toggle="modal" data-target="#myModaComp"></span>

<?php
if( trim($is_claiming) == "yes")
{
?>
<script type="text/javascript">
$(document).ready(function() {
  $("#shownumberexits").trigger("click");
});
</script>
<?php
}
?>



<div class="modal fade" id="myModaComp" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Claim Your Business</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <div class="row">
           <div class="col-md-12">
             <form action="<?php echo base_url().'Users/claim_user_profile';?>" method="post" name="myForm" >
				<span>(*) Required Fields</span>

<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     <input type="text" class="form-control " readonly="" name="user_company_name" value="<?php echo $business['user_company_name']; ?>">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      <input type="text" readonly="" class="form-control" name="user_company_number" value="<?php echo $business['user_company_number']; ?>">
                       
                  </div>
                </div>
</div>

 <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter first name." class="form-control" name="user_firstname" placeholder="*PRÉNOM"  value="<?php echo set_value('user_firstname'); ?>">
          <?php echo form_error('user_firstname','<div class="help-block">','</div>'); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="text" title="Please enter last name."  class="form-control"  name="user_lastname" placeholder="NOM"  value="<?php echo set_value('user_lastname'); ?>">
          <?php echo form_error('user_lastname','<span class="help-block">','</span>'); ?>
                 
                     </div>
                </div>
</div>


<div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                      
                      <input type="email" class="form-control" title="Please enter a valid email."   name="user_email"  placeholder="*Email"  value="<?php echo set_value('user_email'); ?>">
          <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group">
                     
                      <input type="text" title="Please enter telephone number."  required="" maxlength="10" class="form-control" name="user_phone"  placeholder="*TELEPHONE" value="<?php echo set_value('user_phone'); ?>">
            <?php echo form_error('user_phone','<span class="help-block">','</span>'); ?>

                  </div>
                </div>

</div>

<div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                     <input id="autocomplete" placeholder="*VEUILLEZ SAISIR VOTRE ADRESSE" required=""
  name="user_company_address" class="form-control " onFocus="geolocate()" type="text"  value="<?php echo set_value('user_company_address'); ?>"></input>
   
         <?php echo form_error('user_company_address','<span class="help-block">','</span>'); ?>
        


                  </div>
                </div>
</div>



<div class="row">
        <div class="col-md-12">
                  <div class="form-group">
                      <label class="terms-offer">
                        <input type="checkbox" name="confidential" required="" id="personalconfidential"  value="1"><span>*Acceptez les conditions générales</span>
                    </label>
                  </div>
                    <div class="g-recaptcha" data-sitekey="6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M"></div>
 <?php echo form_error('g-recaptcha-response','<span class="help-block">','</span>'); ?>
		</div>
</div>

<div class="row">
                <div class="col-md-12">
                  <div class="btn-div">
                    <button class="btn btn-default form-btn" value="S'enregistrer" name="claimSubmit" type="submit">S'enregistrer</button>
                  </div>
                </div>
</div>

    </form>
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
  

  
      </div>
    </div>
  </div>
</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url("assets/js/jquery.flexdatalist.js");?>"></script>
<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
<script src="<?php echo base_url("assets/js/custom.js");?>"></script>
<script>
     var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>

<script type="text/javascript">
    $(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='myForm']").validate({
	rules: {
      user_phone:"required",
      user_firstname: "required",
      user_lastname: "required",
      user_email: {
        required: true,
        email: true
      },
      user_company_address:"required",
      confidential:"required"      
    },
    messages: {
      user_phone:"Please enter phone number",
      user_firstname: "Please enter your firstname",
      user_lastname: "Please enter your lastname",
      user_categories: "Please select business category",
      user_company_address:"Please enter company address",      
      confidential:"Please select terms & conditions",
      user_email: {
        required:"Please enter a valid email address",
        email:"Please enter valid email address"
      }
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    



    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script>