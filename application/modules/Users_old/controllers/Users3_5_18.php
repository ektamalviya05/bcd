<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends MX_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){	 

	 	parent:: __construct();

		/* Load the libraries and helpers */
      
		$this->load->library('form_validation','session');
        $this->load->model('User');
   
    }

    /*
     * User account information without ajax
     */
    public function forgetpassword_old(){
        $data = array();

        if($this->input->post('passwordSubmit'))
        {      
            $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email');
           
            $email = $this->input->post('user_email');

            if ($this->form_validation->run() == true) {
                //$con['returnType'] = 'single';
                $con['conditions'] = array(
                    'user_email'=> $email,
                    'status' => '1'
                );
                $con['returnType'] = "single";

              
             
                $checkUser = $this->User->getRows($con);

                if(!empty($checkUser)){
                    $temppass = $this->generateRandomPass();
                    $temppass_md5 = md5($temppass);
                    $upd = $this->User->update_password($email, $temppass_md5);
                    if($upd >=1)
                    $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                    $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>13));
                    $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                    //$email = $userData['user_email'];
                    $subject = $templatecenter['tmplate_subject'];
                    $msginner = "Please find your temporary password below<br><br>". $temppass. "<br><br>Please login to your dashboard with temp password and Change your password. <br><br>
                    <a href='".base_url()."'> Login Here</a><br><br>";

                    $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%Address%%", "%%Phone%%", "%%Gender%%", "%%TempPassword%%");
                    $real = array($checkUser['user_firstname'], $checkUser['user_lastname'], $checkUser['user_email'], $checkUser['user_address'], $checkUser['user_phone'], $checkUser['user_gender'], $temppass);
                    $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                    $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                    
                    $x= $this->send_mail($email, $subject, $msg);
                    //echo $x;
                    $data['success_msg'] = 'Your Password has been sent to your email. Please Check and Login to your account.';
                   
                }else{
                    $data['error_msg'] = 'Wrong email, please try again.';
                }
            }
        }
        header("Access-Control-Allow-Origin: *");                     
        $this->template->set('title', 'Request New Password');
        $this->template->load('inner_layout', 'contents' , 'password_page', $data);      
    }
	
	/*With ajax*/
    public function forgetpassword(){
			 $data = array();        

			 $email = $this->input->post('user_email');
			//$con['returnType'] = 'single';
			 $con['conditions'] = array(
				'user_email'=> $email,
				'status' => '1'
			 );
			 $con['returnType'] = "single";
			 $checkUser = $this->User->getRows($con);

			 if(!empty($checkUser)){
				$temppass = $this->generateRandomPass();
				$temppass_md5 = md5($temppass);
				$upd = $this->User->update_password($email, $temppass_md5);
				if($upd >=1)
				$templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
				$templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>13));
				$templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
				//$email = $userData['user_email'];
				$subject = $templatecenter['tmplate_subject'];
				$msginner = "Please find your temporary password below<br><br>". $temppass. "<br><br>Please login to your dashboard with temp password and Change your password. <br><br>
				<a href='".base_url()."'> Login Here</a><br><br>";

				$dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%Address%%", "%%Phone%%", "%%Gender%%", "%%TempPassword%%");
				$real = array($checkUser['user_firstname'], $checkUser['user_lastname'], $checkUser['user_email'], $checkUser['user_address'], $checkUser['user_phone'], $checkUser['user_gender'], $temppass);
				$msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
				$msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
				
				$x= $this->send_mail($email, $subject, $msg);
				//echo $x;
				$data['success_msg'] = 'Your Password has been sent to your email. Please Check and Login to your account.';
				$data['status'] = '200';
			   
			 }else{
				$data['success_msg'] = '<label class="error">Veuillez verifier votre adresse mail.</label>';
				$data['status'] = '300';
			 }
			 
			 echo json_encode($data);
		
       
       
    }
public function submitOptionPlan()
{
     $data = array();
        $data['head_list']="";
        if($this->session->userdata('isUserLoggedIn')){

            if($this->input->post('optionPaymentSubmit'))
            {
               
                $opt_user_id = $this->session->userdata('user_id');
                $opt_option_id = $this->input->post('opt_id');
                $opt_option_purchase_date = date("Y/m/d", time());
                
                $otp_option_duration= $this->input->post('price_type');
                
                $opt_option_price= $this->input->post('opt_price');

                $opt_option_active_date = null;
                $opt_option_end_date = null;
                $opt_option_inactive_validity=null;


                $optionbuy = array("opt_user_id" => $opt_user_id, "opt_option_id" => $opt_option_id, "opt_option_purchase_date" => $opt_option_purchase_date, "otp_option_duration" => $otp_option_duration, "opt_option_price" => $opt_option_price, "opt_option_active_date" => $opt_option_active_date, "opt_option_end_date"=>$opt_option_end_date,  "opt_option_inactive_validity"=> $opt_option_inactive_validity);


                $y=$this->User->insertUserOption($optionbuy);

                if($y)
                {
                 $data['head_list']="Purchased";

                 //send email here
                 $email = $this->session->userData["user_email"];

                   $subject = "Head of List Option Purchased";
                   $msginner = "Thanks for Adding Head of List option to Your Business Profile. We will process your request and infom you soon on your registered email ID.";

                    $msg = $this->sendMailFormat($subject, $msginner);
                    $fromemail = "votive.pradeep01@gmail.com";
                    $fromname = "Support Team";

                    $x= $this->send_mail($email, $subject, $msg);


                }
                else
                {
                  $data['head_list']="Not Purchased";   
                }

            }
            else
            {
                 $data['head_list']="Not Purchased";


            }
              header("Access-Control-Allow-Origin: *");                     
             $this->template->set('title', 'User Dashboard');
             $this->template->load('home_layout', 'contents' , 'user_option_purchased', $data);
        }else{
            redirect(base_url().'Users/login');
        }

}

    /*
    * User account information
    */
    public function account(){
        $data = array();
        $data['head_list']="";
        $this->load->library("cart");
        if($this->session->userdata('isUserLoggedIn')){
            $this->session->unset_userdata("slimpay_orderstatus");
            $this->load->library("Slimpay_lib");
            $data['user_purchasd_options'] = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata('user_id'), "opt_option_type "=>1, "opt_option_status !="=>2, "opt_status"=>1, "pyt_txn_status "=>"Completed"));
            $data['user_package_options'] = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata('user_id'), "opt_option_type "=>4, "opt_option_status !="=>2, "opt_status"=>1, "pyt_txn_status "=>"Completed"));
            $data['user_purchased_static_options'] = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata('user_id'), "opt_option_type "=>3, "opt_option_status !="=>2, "pyt_txn_status "=>"Completed" ));
            $data['user_purchasd_adv_options'] = $this->User->getUserAdvOptions(array("user_id"=> $this->session->userdata('user_id'), "opt_option_status !="=>2));
            $data['user'] = $this->User->getRows(array('user_id'=>$this->session->userdata('user_id')));
            $data['business'] = $this->User->getBusinessRows(array('user_id'=>$this->session->userdata('user_id')));
            $data['extraphone'] = $this->User->getExtraNumberRows(array("conditions"=>array("phn_user"=>$this->session->userdata('user_id'))));
            $data['business_categories'] = $this->User->getBusinessCategories(1);
            $data['service_filters'] = $this->User->getBusinessCategories(2);
            $data['product_filters'] = $this->User->getBusinessCategories(3);
            $data['other_filters'] = $this->User->getBusinessCategories(4);
            $data['prstable'] = $this->User->getBusinessPriceRows(array("sorting"=>array("pr_id"=>"DESC"),  "conditions"=>array('pr_user'=>$this->session->userdata('user_id'))));
            $data['opentiming'] = $this->User->getBusinessOpeningRows(array('user_id'=>$this->session->userdata('user_id')));
            $data['gallery'] = $this->User->getGalleryRows(array("sorting"=>array("ph_id"=>"DESC"),  "conditions"=>array('ph_user'=>$this->session->userdata('user_id'))));
            $data['following'] = $this->User->getFollowingRows(array("sorting"=>array("followed_since"=>"DESC"),  "conditions"=>array('follower_user_id'=>$this->session->userdata('user_id'))));      
            $data['followingEvents'] = $this->User->getFollowingEvents();
            $data['myEvents'] = $this->User->getEventsRows(array("conditions"=>array("created_by_user"=>$this->session->userdata('user_id'), "event_status"=>1, "opt_option_status"=>1)));
            $data['eventTypeList'] = $this->User->getEventTypeList();
            $data['languages'] = $this->User->getLanguageRows(array("conditions"=>array("lang_status"=>1)));
            $data['messagelisting'] = $this->User->userMessages();
            $data['notificationslisting'] = $this->User->getNotificationsRows(array("conditions"=>array("nt_to"=>$this->session->userdata('user_id')), "sorting"=>array("nt_id"=>"DESC")));            
            if($this->uri->segment(3) == "notifications"){
                $this->User->notiUpdate(array("nt_read"=>1, "nt_flag"=>1), array("nt_to"=>$this->session->userdata('user_id')));
            }
            $data['suggestion'] = $this->User->getSuggestions();
            $arr = $this->User->get_options("Head of List");
            
            $data['head_of_list'] = $arr[0];
            $opt_id = $data['head_of_list']['opt_id'];
            $data['price_options'] = $this->User->get_options_price($opt_id);


            //mes option 1
            $data['mesoption1'] = $this->User->getMesOptionDetails(1);
          
            //mes option 2
            $data['mesoption2'] = $this->User->getMesOptionDetails(2);
            //mes option 3
            $data['mesoption3'] = $this->User->getMesOptionDetails(3);
            //mes option 4
            $data['mesoption4'] = $this->User->getMesOptionDetails(4);
            //mes option 5
            $data['mesoption5'] = $this->User->getMesOptionDetails(5);
            //mes option 6
            $data['mesoption6'] = $this->User->getMesOptionDetails(6);
            //mes option 6
            $data['mesoption7'] = $this->User->getMesOptionDetails(7);
            //mes option 8
            $data['mesoption8'] = $this->User->getMesOptionDetails(8);

            //mes option 9
            $data['mesoption9'] = $this->User->getMesOptionFormDetails();
            // print_r(  $data['mesoption9']);
            //die("OK");
           

            //  $data['mesoption10'] = $this->User->getMesOptionDetails(10);
            $data['mesoption10'] = $this->User->getMesOptionDetailsStatic();
            $data['packages'] = $this->User->getActiavatedPackageRows(array("conditions"=>array("opt_type"=>4, "opt_status"=>1)));            

            $data['user_price_options'] = $this->User->get_user_options_price($this->session->userdata('user_id'));
            $data['desclimit'] = $this->User->get_userPurchasedOptionActivatedByType(4);
            $data['searchbannerdisplay'] = $this->User->get_userPurchasedOptionActivatedByType(5);
            $data['imglimit'] = $this->User->get_userPurchasedOptionActivatedByType(6);
            $data['labels'] = $this->User->getTextContent(15);
            $data['debitcardimages'] = $this->User->getDebitCardRows();
            $data['mandatereference'] = $this->User->getUserMandate(array("slmref_userid"=>$this->session->userdata('user_id')));
            $data['uploaddocuments'] = $this->User->getUploadDocuments(array("conditions"=>array("dc_userid"=>$this->session->userdata('user_id'))));
             // echo "<pre>"; 
             // print_r($data['debitcardimages']);
             // echo "</pre>";
             //  die("FSF");
            header("Access-Control-Allow-Origin: *");                     
            $this->template->set('title', 'User Dashboard');
            $this->template->load('home_layout', 'contents' , 'user_dashboard', $data);
        }else{
            redirect(base_url().'Users/login');
        }
    }

    /*
    * get mandate reference details
    */
    function getmandateReferenceDetails(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $this->load->library("Slimpay_lib");
            $mandatereference = $this->User->getUserMandate(array("slmref_userid"=>$this->session->userdata('user_id')));            
            $msg = '<br>
                    <div class="col-sm-12">';
            $msg .= '<div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-4">
                            <label>Mandate Reference:</label>
                        </div>
                        <div class="col-md-8 col-sm-8 col-xs-8">'.$mandatereference['slmref_mandateref'].'</div>
                    </div>';
              
            if(!empty($mandatereference['slmref_mandateref'])){
                $mandatref = $this->slimpay_lib->getMandateState(array('reference'=>$mandatereference['slmref_mandateref']));
                
                $msg .= '<div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-4">
                            <label>Mandate State:</label>
                          </div>
                          <div class="col-md-8 col-sm-8 col-xs-8">
                            '.ucwords($mandatref['state']).'
                          </div>
                        </div>';                        
                
                $bankdetails = $this->slimpay_lib->getBankAccount(array("reference"=>$mandatref['reference']));
                if(!empty($bankdetails)){
                    $msg .= '<div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <label>IBAN:</label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                  '.$bankdetails['iban'].'
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <label>BIC</label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                  '.$bankdetails['bic'].'
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <label>Institution Name</label>
                                </div>
                                <div class="col-md-8 col-sm-8 col-xs-8">
                                  '.$bankdetails['institutionName'].'
                                </div>
                            </div>';                  
                }
                $msg .= '<div class="row">
                          <div class="col-md-4 col-sm-4 col-xs-4">
                            <label>Date Signed:</label>
                          </div>
                          <div class="col-md-8 col-sm-8 col-xs-8">
                            '.date("d M, Y h:i A", strtotime($mandatref['dateSigned'])).'
                          </div>
                        </div>';
            }
            $msg .= '</div>';
            echo json_encode(array("status"=>200, "msg"=>$msg));
        }else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé."));
        }
    }

    /*
    * purhchased package details
    */
    public function purchasedPackage($args){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $data['user'] = $this->User->getRows(array('user_id'=>$ses));
            $data['useroptions'] = $this->User->getUserOptions(array("opt_user_id"=>$ses, "opt_user_option_id"=>$args));        
            $data['package'] = $this->User->getPackageRows(array("opt_id"=>$data['useroptions'][0]['opt_option_id']));        
            $data['options'] = $this->User->getUserPurchasedPackageOptionRows(array("conditions"=>array("opt_pkg_id"=>$data['useroptions'][0]['opt_user_option_id'])));
            // echo "<pre>";
            // print_r($data['useroptions']);
            // print_r($data['package']); 
            // print_r($data['options']);        
            // echo "</pre>";
            // exit;
            $data['suggestion'] = $this->User->getSuggestions();
            $data['labels'] = $this->User->getTextContent(15);
            $data["useroptions"] = $data['useroptions'][0];
            header("Access-Control-Allow-Origin: *");
            $this->template->set('title', 'User Dashboard');
            $this->template->load('home_layout', 'contents' , 'purchasedpackageoptions', $data);
        }else{
            $this->session->set_flashdata("error", "You are not login, Please login first");
            redirect(base_url());
        }
    }

    /*
    * activate and update purchased package options
    */
    public function updatepackageoption($args, $package){        
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){            
            if(!empty($args)){                
                $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
                $this->form_validation->set_rules("stdate", "start day", "required", array("required"=>"Please select %s") );
                $opt_type = $this->input->post("opt_type");
                if($opt_type == 2){
                    $this->form_validation->set_rules("city", "city", "required", array("required"=>"Please select %s") );
                }
                if($this->form_validation->run() == FALSE){
                    $data['user'] = $this->User->getRows(array('user_id'=>$ses));
                    $data['useroptions'] = $this->User->getUserOptions(array("opt_user_id"=>$ses, "opt_user_option_id"=>$package)); 
                    $data['package'] = $this->User->getPackageRows(array("opt_id"=>$data['useroptions'][0]['opt_option_id']));        
                    $data['options'] = $this->User->getUserPurchasedPackageOptionRows(array("conditions"=>array("opt_pkg_id"=>$data['useroptions'][0]['opt_user_option_id'])));                    
                    $data['suggestion'] = $this->User->getSuggestions();
                    $data['labels'] = $this->User->getTextContent(15);
                    header("Access-Control-Allow-Origin: *");
                    $this->template->set('title', 'User Dashboard');
                    $this->template->load('home_layout', 'contents' , 'purchasedpackageoptions', $data);
                }else{
                    $useroptions = $this->User->getUserOptions(array("opt_user_id"=>$ses, "opt_user_option_id"=>$package));
                    $userdetails = $this->User->getUserRowsFromOption($this->input->post("opt_id"));
                    $packageoptiondetails =  $this->User->getUserOptions(array("opt_user_id"=>$ses, "opt_user_option_id"=>$this->input->post("opt_id")));
                    $pkg_pyt_status = $useroptions[0]['pyt_txn_status'];
                    $pkg_active_status = $useroptions[0]['opt_option_status'];                    
                    if($pkg_active_status == 1){
                        $duration = $useroptions[0]['otp_option_duration'];
                        $packageduration = $packageoptiondetails[0]['otp_option_duration'];
                        $date = $this->input->post("stdate");
                        $date = date("Y-m-$date");
                        $exp_date = date("Y-m-d", strtotime($date." + $packageduration"));
                        $array = array("opt_option_status"=>1,
                                       "opt_option_active_date"=>$date,
                                       "opt_option_end_date"=>$exp_date
                                      );
                        if($opt_type == 2){
                            $latlong = $this->User->get_lat_long($this->input->post("city"));
                            $array['otp_search_city']= $this->input->post("city");
                            $array['opt_city_lat'] = $latlong['lat'];
                            $array['opt_city_long'] = $latlong['lng'];
                        }
                        $check = $this->User->updateUserOption($array, array("opt_user_option_id"=>$this->input->post("opt_id")));
                        $email = $useroptions['user_email'];
                        $opt_option_purchase_date = $userdetails['opt_option_purchase_date'];

                        //Fetch Email Templates
                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>20));
                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer

                        $subject = $templatecenter['tmplate_subject'];
                        $dummy = array("%%Firstname%%",
                                       "%%LastName%%", 
                                       "%%Email%%", 
                                       "%%BusinessName%%", 
                                       "%%CompanyNumber%%", 
                                       "%%Address%%", 
                                       "%%Phone%%", 
                                       "%%Gender%%", 
                                       "%%OptionName%%", 
                                       "%%OptionsDescription%%", 
                                       "%%PurchaseDate%%", 
                                       "%%OptionDuration%%", 
                                       "%%OptionPrice%%", 
                                       "%%OptionsVisibleCity%%", 
                                       "%%OptionActiveStartDate%%",
                                       "%%OptionActiveEndDate%%" );
                        $real = array($userdetails['user_firstname'], 
                                      $userdetails['user_lastname'],
                                      $userdetails["user_email"],
                                      $userdetails["user_company_name"],
                                      $userdetails['user_company_number'],
                                      $userdetails["user_company_address"],
                                      $userdetails['user_phone'],
                                      $userdetails['user_gender'],
                                      $userdetails['opt_name'],
                                      $userdetails['opt_description'],
                                      $userdetails['opt_option_purchase_date'],
                                      $userdetails['otp_option_duration'],
                                      $userdetails['opt_option_price'],
                                      $userdetails['otp_search_city'],
                                      $userdetails['opt_option_active_date'],
                                      $userdetails['opt_option_end_date']
                                    );
                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                        $x= $this->send_mail($userdetails["user_email"], $subject, $msg);        
                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);                        
                        $this->session->set_flashdata("success", "Option activated successfully.");
                        redirect(base_url("Users/purchasedPackage/$package"));
                    }else{
                        $this->session->set_flashdata("error", "Your package is not activated yet");
                        redirect(base_url("Users/account/messoptions"));                        
                    }
                }
            }else{
                $this->session->set_flashdata("error", "Accès refusé");
                redirect(base_url());
            }
        }else{
            $this->session->set_flashdata("error", "You are not login, Please login first");
            redirect(base_url());
        }
    }

    /*
    * Display and print User Purchase
    */
    public function userPurchase($args = NULL){
        $ses = $this->session->userData("user_id");
        if(!empty($ses) && !empty($args)){
            $query = $this->User->getUserOptions(array("opt_user_id"=> $ses, "opt_user_option_id"=>$args));
            if($query){                
                $data = array();
                $data['details'] = $details =  $query[0];
 
				$receipt = $this->User->site_contents(27);
				$dummy = array("%%order_id%%", "%%purchase_date%%", "%%amount%%", "%%description%%", "%%option_name%%", "%%option_duration%%");
				$real = array($details['pyt_id'],
                			  $details['opt_option_purchase_date'],
                			  $details['pyt_amount'],
                			  $details['opt_description'],
                			  $details['opt_name'],
                			  $details['otp_option_duration'],
				            );
				$data['msg'] = str_replace($dummy, $real,$receipt);				 
                header("Access-Control-Allow-Origin: *");
                $this->template->set('title', 'User Dashboard');
                $this->template->load('home_layout', 'contents' , 'userpurchasedetails', $data);
            }
        }else{
            $this->session->set_flashdata("error", "Accès refusé");
            redirect(base_url());
        }
    }

    /*
    * update user notification on click
    */
    public function updateNotif(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $this->User->notiUpdate(array("nt_read"=>1, "nt_flag"=>1), array("nt_to"=>$ses));
        }
    }

    /*
    * fetching option pricing according to duration
    */
    function optionPrice(){
        error_reporting(E_ALL);        
        $date1 = date_create(date("Y-m-d", strtotime($this->input->post('startdate'))));
        $date2 = date_create(date("Y-m-d", strtotime($this->input->post('enddate'))));
        $difference = date_diff($date1,$date2);
        $diff = $difference->format('%a');        
        // $date1 = new DateTime($this->input->post('startdate'));
        // $date2 = new DateTime($this->input->post('enddate'));
        // $diff = $date2->diff($date1)->format("%a");
        
        $query = $this->User->get_single_options_pricebytype(array("opt_price_id"=>$this->input->post("option")));        
        $pagetext = $this->User->getTextContent(30);
        if(!empty($query)){
            if($diff > 0)
                $totalprice = $pagetext['label6'].$diff*$query['opt_price']." Pour $diff jours";
            else
                $totalprice = $pagetext['label6'].$query['opt_price']." Pour 1 jour";

            if($query['opt_type_id'] == 7){
                $check = $this->User->getMyFreeEvents();
                if(empty($check) && count($check) == 0){
                    $totalprice = "1 événement gratuit";
                }
            }
            echo json_encode(array("status"=>200, "price"=>"<span><strong>".$pagetext['label8'].": </strong>$totalprice</label><hr>"));
        }else{
            if($diff > 0)
                $totalprice = "$".$diff*10 ." Pour $diff jours";
            else
                $totalprice = '$10 Pour 1 jour';


            if($query['opt_type_id'] == 7){
                $check = $this->User->getMyFreeEvents();
                if(empty($check) && count($check) == 0){
                    $totalprice = "1 événement gratuit";
                }
            }
            echo json_encode(array("status"=>200, "price"=>"<span><strong>".$pagetext['label8'].": </strong>$totalprice</label><hr>"));
        }
    }

    /*
    * Check Actiated option date
    */
    function checkActivatedOptionDate(){
        $startdate = $this->input->post('startdate');
        $ses = $this->session->userdata("user_id");
        $optionid = $this->input->post("option");
        if(!empty($ses) && !empty($startdate) && !empty($optionid)){
            $details = $this->User->get_userPurchasedOptionActivatedDateByType($optionid, $startdate);
            if($details->num_rows() > 0){
                echo json_encode(array("status"=>2, "msg"=>"<label class='text-danger'>Vous avez déjà acheté cette option pour ces mêmes dates.</label>"));
            }else{
                echo json_encode(array("status"=>200, "msg"=>"success"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! Certains champs obligatoire n'ont pas été saisis</label>"));
        }        
    }

    /*
    * Check Actiated option date
    */
    function checkActivatedOptionDateForPackageOptions(){
        $startdate = $this->input->post('startdate');
        $startdate = date("Y-m-".$startdate);
        if($startdate < date("Y-m-d")){
            $startdate = date("Y-m-d", strtotime($startdate." + 1 Month"));
        }
        $ses = $this->session->userdata("user_id");
        $optionid = $this->input->post("option");
        if(!empty($ses) && !empty($startdate) && !empty($optionid)){
            $details = $this->User->get_userPurchasedOptionActivatedDateByType($optionid, $startdate);
            if($details->num_rows() > 0){
                echo json_encode(array("status"=>2, "msg"=>"<label class='text-danger'>Vous avez déjà acheté cette option pour ces mêmes dates.</label>"));
            }else{
                echo json_encode(array("status"=>200, "msg"=>"success"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! Certains champs obligatoire n'ont pas été saisis</label>"));
        }
    }

    private function checkActivatedOptionDateForClassOnly($optionid, $startdate){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses) && !empty($startdate) && !empty($optionid)){
            $details = $this->User->get_userPurchasedOptionActivatedDateByType($optionid, $startdate);
            if($details->num_rows() > 0){
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }        
    }

    function completePaymentAction(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){   
            if(empty($this->session->userdata("purchase_option"))){
                $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
                redirect(base_url("Users/account/messoptions"));
            }         
            $data = array();
            $data['userdetails'] = $this->User->getRows(array("user_id"=>$ses));
            $data["texts"] = $this->User->getTextContent(31);
            $data["userinfoconfirm"] = $this->User->getTextContent(32);
            $data["usermandateexists"] = $this->User->getTextContent(33);
            $data["transfailed"] = $this->User->getTextContent(35);
            header("Access-Control-Allow-Origin: *");
            $this->template->set('title', 'Select Payment Method');
            $this->template->load('home_layout', 'contents' , 'selectpaymentmethod', $data);
        }else{
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    function setSelectedOptionParameters(){
        if(!empty($this->session->userdata("user_id"))){            
            $this->session->unset_userdata("purchase_option");
            $opt_user_id = $this->session->userdata('user_id');
            $opt_option_id = $this->input->post('opt_id');   
            $opt_type = $this->input->post('opt_type');
            $optid = $this->input->post('price_type');
            if(!empty($opt_option_id) && !empty($optid)){
                $startfrom = $this->input->post("perdaysdate");
                $endfrom = $this->input->post("perdayedate");
                $opt_option_purchase_date = date("Y/m/d", time());
                $package = $this->User->get_single_options_price($optid);
                $otp_option_duration = $package['opt_price_type'];
                $opt_option_price = $package['opt_price'];
                $otp_option_qnty = $package['opt_qnty'];
                $opt_option_active_date = null;
                $opt_option_end_date = null;
                $opt_option_inactive_validity=null;
                $otp_search_city = "";
                $opt_city_lat = "";
                $opt_city_long = "";
                if(empty($opt_type))
                {
                   $opt_type=1;
                }
                
                if($otp_option_duration == "1 Day"){
                    if($opt_option_id == 2){                        
                        $otp_search_city = $this->input->post("city");
                        $latlong = $this->User->get_lat_long($this->input->post("city"));                        
                        $opt_city_lat = $latlong['lat'];
                        $opt_city_long = $latlong['lng'];
                    }
                    $opt_option_active_date = $startfrom;
                    $opt_option_end_date = $endfrom;
                    $date1 = date_create($opt_option_active_date);
                    $date2 = date_create($opt_option_end_date);        
                    $difference = date_diff($date1,$date2);
                    $diff = $difference->format('%a');
                    if($diff > 0 )
                        $opt_option_price = $diff*$opt_option_price;
                }                
                $optionbuy = array("opt_user_id" => $opt_user_id,
                                   "opt_option_id" => $opt_option_id,
                                   "opt_option_type" => $opt_type,
                                   "opt_option_purchase_date" => $opt_option_purchase_date,
                                   "otp_option_duration" => $otp_option_duration,
                                   "otp_option_qnty"=>$otp_option_qnty,
                                   "opt_option_price" => $opt_option_price,
                                   "otp_search_city"=>$otp_search_city,
                                   "opt_city_lat"=>$opt_city_lat,
                                   "opt_city_long"=>$opt_city_long,
                                   "opt_option_active_date" => $opt_option_active_date,
                                   "opt_option_end_date"=>$opt_option_end_date,
                                   "opt_option_inactive_validity"=> $opt_option_inactive_validity);            
                $this->session->set_userdata("purchase_option", $optionbuy);
                redirect(base_url("Users/completePaymentAction"));
            }else{
                $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
                redirect(base_url("Users/account/messoptions"));
            }            
        }
        else
        {
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    /*
    * Select paypal payment action
    */
    function paypalInteraction(){
        if(!empty($this->session->userdata("user_id")) && !empty($this->session->userdata("purchase_option"))){            
            $purchase_option = $this->session->userdata('purchase_option');
            $opt_user_id = $this->session->userdata('user_id');
            $opt_option_id = $purchase_option['opt_option_id'];
            $opt_type = $purchase_option['opt_option_type'];            
            $opt_option_purchase_date = date("Y/m/d", time());            
            $otp_option_duration = $purchase_option['otp_option_duration'];
            $opt_option_price = $purchase_option['opt_option_price'];
            $otp_option_qnty = $purchase_option['otp_option_qnty'];
            $opt_option_active_date = $purchase_option['opt_option_active_date'];
            $opt_option_end_date = $purchase_option['opt_option_end_date'];
            $opt_option_inactive_validity=$purchase_option['opt_option_inactive_validity'];
            $otp_search_city = $purchase_option['otp_search_city'];
            $opt_city_lat = $purchase_option['opt_city_lat'];
            $opt_city_long = $purchase_option['opt_city_long'];
            $optionbuy = array("opt_user_id" => $opt_user_id,
                               "opt_option_id" => $opt_option_id,
                               "opt_option_type" => $opt_type,
                               "opt_option_purchase_date" => $opt_option_purchase_date,
                               "otp_option_duration" => $otp_option_duration,
                               "otp_option_qnty"=>$otp_option_qnty,
                               "opt_option_price" => $opt_option_price,
                               "otp_search_city"=>$otp_search_city,
                               "opt_city_lat"=>$opt_city_lat,
                               "opt_city_long"=>$opt_city_long,
                               "opt_option_active_date" => $opt_option_active_date,
                               "opt_option_end_date"=>$opt_option_end_date,
                               "opt_option_inactive_validity"=> $opt_option_inactive_validity);            
            $arr = array("pyt_txn_status"=>"Pending");
            $check = $this->User->insertTransaction($arr);
            if($check)
            {
                if($opt_option_id == 7 || $opt_option_id == 8){
                    $arr = array("event_status"=>0,
                                 "event_is_free"=>0,
                                 "created_by_user"=>$opt_user_id
                                );
                    if($otp_option_duration == "1 Day"){
                        $arr['event_publish_start'] = $opt_option_active_date;
                        $arr['event_publish_end'] = $opt_option_end_date;
                    }
                    if($opt_option_id == 8)
                        $arr['event_goodplace'] = 1;
                    $optionbuy['opt_event_id'] = $this->User->insertEventwithougNoti($arr);
                }
               // if($opt_option_id == 10){
                // if($opt_type == 3){
                //     $userdetails = $this->User->getRows(array("user_id"=>$opt_user_id));
                //     $subject = "Mess Option Purchase";
                //     $msginner = "<br><p>Purchase details are given below:</p>
                //                  <p>Purchase option type: ".$package['opt_text']."</p>
                //                  <p>Option Price: ".$package['opt_price']."</p>";

                //     $msg = $this->sendMailFormat($subject, $msginner, 23);
                //     $x= $this->send_mail($userdetails['user_email'], $subject, $msg);

                //       //Admin
                //     $emailadmin = ADMIN_EMAIL;
                //     $msgadmin = $this->sendMailFormat($subject, $msginner, 23);
                //     $x= $this->send_mail($emailadmin, $subject, $msgadmin);
                // }
                $optionbuy['opt_tran_id'] = $check;
                $y = $this->User->insertUserOption($optionbuy);

                // $y = $this->User->insertUserOption($optionbuy);
                //Paypal Code Integration start
                    $this->load->library("paypal_lib");
                    //Set variables for paypal form
                    $returnURL = base_url().'Users/completePayment'; //payment success url
                    $cancelURL = base_url().'Users/cancelPayment'; //payment cancel url
                    $notifyURL = base_url().'Pages/paymentIpn'; //ipn url
                    //get particular product data                    
                    $this->paypal_lib->add_field('return', $returnURL);
                    $this->paypal_lib->add_field('cancel_return', $cancelURL);
                    $this->paypal_lib->add_field('notify_url', $notifyURL);
                    $this->paypal_lib->add_field('item_name', "YellowPages");
                    $this->paypal_lib->add_field('custom', $check);
                    $this->paypal_lib->add_field('item_number',  "1");
                    $this->paypal_lib->add_field('amount', $opt_option_price );
                    $this->paypal_lib->paypal_auto_form();
                //Paypal Code Integration End
                            
            }
            else
            {
                $this->session->set_flashdata("error", "<label class='text-danger'>Option non achetée, veuillez réessayer.</label>");
                redirect(base_url("Users/account"));
            }
            
        }
        else
        {
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }


    /*
    * Select paypal payment action
    */
    function slimpayInteraction(){
       if(!empty($this->session->userdata("user_id")) && !empty($this->session->userdata("purchase_option"))){
            if(empty($this->input->post())){
                redirect(base_url("Users/completePaymentAction"));
            }
            $ses = $this->session->userdata("user_id");
            $userdetails = $this->User->getRows(array("user_id"=>$ses));
            $purchase_option = $this->session->userdata('purchase_option');
            $opt_user_id = $this->session->userdata('user_id');
            $opt_option_id = $purchase_option['opt_option_id'];
            $opt_type = $purchase_option['opt_option_type'];            
            $opt_option_purchase_date = date("Y/m/d", time());            
            $otp_option_duration = $purchase_option['otp_option_duration'];
            $opt_option_price = $purchase_option['opt_option_price'];
            $otp_option_qnty = $purchase_option['otp_option_qnty'];
            $opt_option_active_date = $purchase_option['opt_option_active_date'];
            $opt_option_end_date = $purchase_option['opt_option_end_date'];
            $opt_option_inactive_validity=$purchase_option['opt_option_inactive_validity'];
            $otp_search_city = $purchase_option['otp_search_city'];
            $opt_city_lat = $purchase_option['opt_city_lat'];
            $opt_city_long = $purchase_option['opt_city_long'];

            $udetails = array("user_phone"=> $this->input->post("phone"),
                              "user_firstname"=>$this->input->post("fname"),
                              "user_lastname"=>$this->input->post("lname"),
                              "user_company_name"=>$this->input->post("companyname")
                             );
            $bsdetails = array("bs_phone"=> $this->input->post("phone"),
                               "bs_name"=>$this->input->post("companyname"),
                               "bs_city"=>$this->input->post("usercity"),
                               "bs_zipcode"=>$this->input->post("postalcode"),
                               "bs_honorific"=>$this->input->post("honorificPrefix")
                              );
            $this->User->updateTable("users", $udetails, array("user_id"=>$ses));
            $this->User->updateTable("otd_business_details", $bsdetails, array("bs_user"=>$ses));

            echo "I am here $opt_type";
            if($opt_type == 4){
                $pkgoptions = $this->User->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$opt_option_id)));                    
                $optionbuy = array("opt_user_id" => $opt_user_id,
                                   "opt_option_id" => $opt_option_id,
                                   "opt_option_type" => $opt_type,
                                   "opt_option_purchase_date" => $opt_option_purchase_date,
                                   "otp_option_duration" => $otp_option_duration,
                                   "otp_option_qnty"=>$otp_option_qnty,
                                   "opt_option_price" => $opt_option_price,
                                   "otp_search_city"=>$otp_search_city,
                                   "opt_option_active_date" => $opt_option_active_date,
                                   "opt_option_end_date"=>$opt_option_end_date,
                                   "opt_option_inactive_validity"=> $opt_option_inactive_validity);            
                $arr = array("pyt_txn_status"=>"Pending", "pyt_amount"=>$opt_option_price, "py_type"=>2);
                $check = $this->User->insertTransaction($arr);
                $optionbuy['opt_tran_id'] = $check;
                $pkgid = $this->User->insertUserOption($optionbuy);
                if(!empty($pkgoptions)){
                    foreach($pkgoptions as $pkopt){                        
                        $optionbuy1 = array("opt_user_id" => $opt_user_id,
                                           "opt_option_id" => $pkopt['pkg_inc_optid'],
                                           "opt_option_type" => 5,
                                           "opt_pkg_id"=>$pkgid,
                                           "opt_option_purchase_date" => $opt_option_purchase_date,
                                           "otp_option_duration" => $pkopt['pkg_opt_duration'],
                                           "otp_option_qnty"=>$pkopt['pkg_opt_qty'],
                                           "opt_option_price" => 0,
                                           "otp_search_city"=>"",
                                           "opt_option_active_date" => "",
                                           "opt_option_end_date"=>"",
                                           "opt_option_inactive_validity"=> "",
                                           "opt_tran_id"=>0
                                       );
                        if($pkopt['opt_id'] == 7 || $pkopt['opt_id'] == 8){
                            $arr = array("event_status"=>0,
                                         "event_is_free"=>0,
                                         "created_by_user"=>$opt_user_id
                                        );
                            if($pkopt['opt_id'] == 8)
                                $arr['event_goodplace'] = 1;
                            $optionbuy1['opt_event_id'] = $this->User->insertEventwithougNoti($arr);
                        }                        
                        $y = $this->User->insertUserOption($optionbuy1);
                    }
                }
                
                //Slimpay Integration start
                    if($check > 0){                                                
                        $this->load->library("Slimpay_lib");
                        $jsonobj = array("reference"=>"BUSINESSUSER_".$ses,
                                         "address1"=>$this->input->post("company_address"),
                                         "address2"=>$this->input->post("company_address"),
                                         "city"=>$bsdetails['bs_city'],
                                         "postalCode"=>$bsdetails['bs_zipcode'],
                                         "country"=>'FR',
                                         "honorificPrefix"=>$bsdetails['bs_honorific'],
                                         "familyName"=>$udetails['user_lastname'],
                                         "givenName"=>$udetails['user_firstname'],
                                         "email"=>$userdetails['user_email'],
                                         "telephone"=>$udetails['user_phone'],
                                         "amount"=>$opt_option_price,
                                         "orderid"=>$check,
                                         "label"=>"First payment for your subscription",
                                         "executionDate"=>date("Y-m-d")
                                        );
                        $mandateexists = $this->User->getUserMandate(array("slmref_userid"=>$opt_user_id));
                        if(!empty($mandateexists)){                            
                            $mandateref = $mandateexists['slmref_mandateref'];
                            $mandatestate = $this->slimpay_lib->getMandateState(array("reference"=>$mandateref));                            
                            if($mandatestate['state'] == "active"){
                                $jsonobj['mdreference'] = $mandateref;                                
                                $payments = $this->slimpay_lib->makeDirectPayment($jsonobj);                                
                                if($payments){
                                    $this->session->unset_userdata("purchase_option");
                                    $optiondetails = $this->User->getOptionDetails($check);
                                    if(!empty($optiondetails)){
                                        $purchasedoptions = "";
                                        foreach($optiondetails as $txnopt){
                                            $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                                            $opt_option_price = $txnopt['opt_option_price'];
                                            $opt_option_id = $txnopt['opt_option_id'];
                                            $checkduration = $txnopt['otp_option_duration'];
                                            $opt_type = $txnopt['opt_option_type'];                
                                            $mstopt = $this->User->getMasterOption($txnopt['opt_option_id']);                
                                            if($opt_type == 4){
                                                $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                            }else{
                                                if($txnopt['otp_option_duration'] == "1 Day" ){
                                                    if($opt_option_id != 7 && $opt_option_id != 8){
                                                        $this->User->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                                                    }                                        
                                                    $date1 = date_create($txnopt['opt_option_active_date']);
                                                    $date2 = date_create($txnopt['opt_option_end_date']);        
                                                    $difference = date_diff($date1,$date2);
                                                    $diff = $difference->format('%a');
                                                    if($diff > 0)
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                                    else
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                                }
                                                if($txnopt['opt_option_id'] == 6){
                                                    $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                                }else{
                                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                }
                                            }
                                        }
                                    }
                                    $transdetails = $this->User->getSlimpayUserDetails($check);
                                    $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                                    $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>34));
                                    $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                                    $subject = $templatecenter['tmplate_subject'];
                                    $dummy = array("%%Firstname%%",
                                                   "%%LastName%%", 
                                                   "%%Email%%", 
                                                   "%%BusinessName%%", 
                                                   "%%CompanyNumber%%", 
                                                   "%%Address%%", 
                                                   "%%Phone%%", 
                                                   "%%Gender%%",
                                                   "%%PurchaseId%%",
                                                   "%%PurchaseDate%%", 
                                                   "%%PurchaseDetails%%",
                                                   "%%MandateReference%%",
                                                   "%%TotalAmount%%",
                                                   "%%SignDate%%" );
                                    $real = array($transdetails['user_firstname'], 
                                                  $transdetails['user_lastname'],
                                                  $transdetails["user_email"],
                                                  $transdetails["user_company_name"],
                                                  $transdetails['user_company_number'],
                                                  $transdetails["user_company_address"],
                                                  $transdetails['user_phone'],
                                                  $transdetails['user_gender'],
                                                  $check,
                                                  $transdetails['opt_option_purchase_date'],
                                                  $purchasedoptions,
                                                  $mandateref,
                                                  $transdetails['pyt_amount'],
                                                  date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                                );
                                    $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                                    $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];                                    
                                    $x= $this->send_mail($transdetails["user_email"], $subject, $msg);
                                    $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                                    $this->session->set_userdata("slimpay_success", "Successfull");
                                    redirect(base_url("Users/account/messoptions"));
                                }else{
                                    $optiondetails = $this->User->getOptionDetails($check);
                                    if(!empty($optiondetails)){
                                        $purchasedoptions = "";
                                        foreach($optiondetails as $txnopt){
                                            $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                                            $opt_option_price = $txnopt['opt_option_price'];
                                            $opt_option_id = $txnopt['opt_option_id'];
                                            $checkduration = $txnopt['otp_option_duration'];
                                            $opt_type = $txnopt['opt_option_type'];                
                                            $mstopt = $this->User->getMasterOption($txnopt['opt_option_id']);                
                                            if($opt_type == 4){
                                                $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                            }else{
                                                if($txnopt['otp_option_duration'] == "1 Day" ){
                                                    if($opt_option_id != 7 && $opt_option_id != 8){
                                                        // $this->User->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                                                    }                                        
                                                    $date1 = date_create($txnopt['opt_option_active_date']);
                                                    $date2 = date_create($txnopt['opt_option_end_date']);        
                                                    $difference = date_diff($date1,$date2);
                                                    $diff = $difference->format('%a');
                                                    if($diff > 0)
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                                    else
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                                }
                                                if($txnopt['opt_option_id'] == 6){
                                                    $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                                }else{
                                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                }
                                            }
                                        }
                                    }
                                    $transdetails = $this->User->getSlimpayUserDetails($check);
                                    $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                                    $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>36));
                                    $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                                    $subject = $templatecenter['tmplate_subject'];
                                    $dummy = array("%%Firstname%%",
                                                   "%%LastName%%", 
                                                   "%%Email%%", 
                                                   "%%BusinessName%%", 
                                                   "%%CompanyNumber%%", 
                                                   "%%Address%%", 
                                                   "%%Phone%%", 
                                                   "%%Gender%%",
                                                   "%%PurchaseId%%",
                                                   "%%PurchaseDate%%", 
                                                   "%%PurchaseDetails%%",
                                                   "%%MandateReference%%",
                                                   "%%TotalAmount%%",
                                                   "%%SignDate%%" );
                                    $real = array($transdetails['user_firstname'], 
                                                  $transdetails['user_lastname'],
                                                  $transdetails["user_email"],
                                                  $transdetails["user_company_name"],
                                                  $transdetails['user_company_number'],
                                                  $transdetails["user_company_address"],
                                                  $transdetails['user_phone'],
                                                  $transdetails['user_gender'],
                                                  $check,
                                                  $transdetails['opt_option_purchase_date'],
                                                  $purchasedoptions,
                                                  $mandateref,
                                                  $transdetails['pyt_amount'],
                                                  date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                                );
                                    $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                                    $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];                                    
                                    $x= $this->send_mail($transdetails["user_email"], $subject, $msg);
                                    $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);

                                    $this->session->set_userdata("slimpay_error", "No");
                                    redirect(base_url("Users/completePaymentAction"));
                                }
                            }else{
                                $this->session->unset_userdata("purchase_option");
                                $this->slimpay_lib->signSlimpayMandate($jsonobj);
                            }
                        }else{
                            $this->session->unset_userdata("purchase_option");
                            $this->slimpay_lib->signSlimpayMandate($jsonobj);
                        }
                        // $this->slimpay_lib->signSlimpayMandate($jsonobj);                        
                    }
                //Slimpay Integration End
            }else{
                echo "<br>Now I am here at 1112";
                $optionbuy = array("opt_user_id" => $opt_user_id,
                                   "opt_option_id" => $opt_option_id,
                                   "opt_option_type" => $opt_type,
                                   "opt_option_purchase_date" => $opt_option_purchase_date,
                                   "otp_option_duration" => $otp_option_duration,
                                   "otp_option_qnty"=>$otp_option_qnty,
                                   "opt_option_price" => $opt_option_price,
                                   "otp_search_city"=>$otp_search_city,
                                   "opt_city_lat"=>$opt_city_lat,
                                   "opt_city_long"=>$opt_city_long,
                                   "opt_option_active_date" => $opt_option_active_date,
                                   "opt_option_end_date"=>$opt_option_end_date,
                                   "opt_option_inactive_validity"=> $opt_option_inactive_validity);            
                $arr = array("pyt_txn_status"=>"Pending", "pyt_amount"=>$opt_option_price, "py_type"=>2);
                $check = $this->User->insertTransaction($arr);
               
                if($check)
                {
                    if($opt_option_id == 7 || $opt_option_id == 8){
                        $arr = array("event_status"=>0,
                                     "event_is_free"=>0,
                                     "created_by_user"=>$opt_user_id
                                    );
                        if($otp_option_duration == "1 Day"){
                            $arr['event_publish_start'] = $opt_option_active_date;
                            $arr['event_publish_end'] = $opt_option_end_date;
                        }
                        if($opt_option_id == 8)
                            $arr['event_goodplace'] = 1;
                        $optionbuy['opt_event_id'] = $this->User->insertEventwithougNoti($arr);
                    }
                   // if($opt_option_id == 10){
                    // if($opt_type == 3){
                    //     $userdetails = $this->User->getRows(array("user_id"=>$opt_user_id));
                    //     $subject = "Mess Option Purchase";
                    //     $msginner = "<br><p>Purchase details are given below:</p>
                    //                  <p>Purchase option type: ".$package['opt_text']."</p>
                    //                  <p>Option Price: ".$package['opt_price']."</p>";

                    //     $msg = $this->sendMailFormat($subject, $msginner, 23);
                    //     $x= $this->send_mail($userdetails['user_email'], $subject, $msg);

                    //       //Admin
                    //     $emailadmin = ADMIN_EMAIL;
                    //     $msgadmin = $this->sendMailFormat($subject, $msginner, 23);
                    //     $x= $this->send_mail($emailadmin, $subject, $msgadmin);
                    // }
                    $optionbuy['opt_tran_id'] = $check;
                    $y = $this->User->insertUserOption($optionbuy);
echo "<br>Inside Check = $check value 1162 ";
                    //Slimpay Integration start
                        if($check > 0){
                            $this->load->library("Slimpay_lib");
                            $jsonobj = array("reference"=>"BUSINESSUSER_".$ses,
                                             "address1"=>$this->input->post("company_address"),
                                             "address2"=>$this->input->post("company_address"),
                                             "city"=>$bsdetails['bs_city'],
                                             "postalCode"=>$bsdetails['bs_zipcode'],
                                             "country"=>'FR',
                                             "honorificPrefix"=>$bsdetails['bs_honorific'],
                                             "familyName"=>$udetails['user_lastname'],
                                             "givenName"=>$udetails['user_firstname'],
                                             "email"=>$userdetails['user_email'],
                                             "telephone"=>$udetails['user_phone'],
                                             "amount"=>$opt_option_price,
                                             "orderid"=>$check,
                                             "label"=>"First payment for your subscription",
                                             "executionDate"=>date("Y-m-d")
                                            );
                            // $jsonobj['mdreference'] = "SLMP004102375";
                            // $this->slimpay_lib->signmandateonly($jsonobj);
                            // $payaments = $this->slimpay_lib->makeDirectPayment($jsonobj);
                            // echo "<pre>";
                            // print_r($payaments);
                            // exit;

                            $mandateexists = $this->User->getUserMandate(array("slmref_userid"=>$opt_user_id));
                            
                            echo "<br>Getting Mandate: ";

                            if(!empty($mandateexists)){
                                echo "<br>Yes MAndate is available";
                                $mandateref = $mandateexists['slmref_mandateref'];                                
                                $mandatestate = $this->slimpay_lib->getMandateState(array("reference"=>$mandateref));                                
                                if($mandatestate['state'] == "active"){
                                    $jsonobj['mdreference'] = $mandateref;
                                    echo "<br>Trying payment";
                                    echo "<pre>";
                                    print_r($jsonobj);
                                    echo "</pre>";
                                    $payments = $this->slimpay_lib->makeDirectPayment($jsonobj);
                                    if($payments){    
                                        echo "Ohh Payment is done";
                                        echo "<pre>";
                                        print_r($payments);
                                        echo "</pre>";                            
                                        $this->session->unset_userdata("purchase_option");
                                        $optiondetails = $this->User->getOptionDetails($check);
                                        if(!empty($optiondetails)){
                                            $purchasedoptions = "";
                                            foreach($optiondetails as $txnopt){
                                                $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                                                $opt_option_price = $txnopt['opt_option_price'];
                                                $opt_option_id = $txnopt['opt_option_id'];
                                                $checkduration = $txnopt['otp_option_duration'];
                                                $opt_type = $txnopt['opt_option_type'];                
                                                $mstopt = $this->User->getMasterOption($txnopt['opt_option_id']);                
                                                if($opt_type == 4){
                                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                }else{
                                                    if($txnopt['otp_option_duration'] == "1 Day" ){
                                                        if($opt_option_id != 7 && $opt_option_id != 8){
                                                            $this->User->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                                                        }
                                                        $date1 = date_create($txnopt['opt_option_active_date']);
                                                        $date2 = date_create($txnopt['opt_option_end_date']);        
                                                        $difference = date_diff($date1,$date2);
                                                        $diff = $difference->format('%a');
                                                        if($diff > 0)
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                                        else
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                                    }
                                                    if($txnopt['opt_option_id'] == 6){
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                                    }else{
                                                        $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                    }
                                                }
                                            }
                                        }
                                        $transdetails = $this->User->getSlimpayUserDetails($check);

                                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>34));
                                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                                        $subject = $templatecenter['tmplate_subject'];
                                        $dummy = array("%%Firstname%%",
                                                       "%%LastName%%", 
                                                       "%%Email%%", 
                                                       "%%BusinessName%%", 
                                                       "%%CompanyNumber%%", 
                                                       "%%Address%%", 
                                                       "%%Phone%%", 
                                                       "%%Gender%%",
                                                       "%%PurchaseId%%",
                                                       "%%PurchaseDate%%", 
                                                       "%%PurchaseDetails%%",
                                                       "%%MandateReference%%",
                                                       "%%TotalAmount%%",
                                                       "%%SignDate%%" );
                                        $real = array($transdetails['user_firstname'], 
                                                      $transdetails['user_lastname'],
                                                      $transdetails["user_email"],
                                                      $transdetails["user_company_name"],
                                                      $transdetails['user_company_number'],
                                                      $transdetails["user_company_address"],
                                                      $transdetails['user_phone'],
                                                      $transdetails['user_gender'],
                                                      $check,
                                                      $transdetails['opt_option_purchase_date'],
                                                      $purchasedoptions,
                                                      $mandateref,
                                                      $transdetails['pyt_amount'],
                                                      date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                                    );
                                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];                                        
                                        $x= $this->send_mail($transdetails["user_email"], $subject, $msg);
                                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                                        
                                        $this->session->set_userdata("slimpay_success", "Successfull");
                                        redirect(base_url("Users/account/messoptions"));
                                    }else{

                                        echo "Ohh Payment is not done. I am in else.";
                                         
                                        $optiondetails = $this->User->getOptionDetails($check);
                                        echo "<pre>";
                                        print_r($optiondetails );
                                        echo "</pre>";      

                                        if(!empty($optiondetails)){
                                            $purchasedoptions = "";
                                            foreach($optiondetails as $txnopt){
                                                $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                                                $opt_option_price = $txnopt['opt_option_price'];
                                                $opt_option_id = $txnopt['opt_option_id'];
                                                $checkduration = $txnopt['otp_option_duration'];
                                                $opt_type = $txnopt['opt_option_type'];                
                                                $mstopt = $this->User->getMasterOption($txnopt['opt_option_id']);                
                                                if($opt_type == 4){
                                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                }else{
                                                    if($txnopt['otp_option_duration'] == "1 Day" ){                                                                                            
                                                        $date1 = date_create($txnopt['opt_option_active_date']);
                                                        $date2 = date_create($txnopt['opt_option_end_date']);        
                                                        $difference = date_diff($date1,$date2);
                                                        $diff = $difference->format('%a');
                                                        if($diff > 0)
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                                        else
                                                            $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                                    }
                                                    if($txnopt['opt_option_id'] == 6){
                                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                                    }else{
                                                        $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                                    }
                                                }
                                            }
                                        }
                                        $transdetails = $this->User->getSlimpayUserDetails($check);
                                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>36));
                                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                                        $subject = $templatecenter['tmplate_subject'];
                                        $dummy = array("%%Firstname%%",
                                                       "%%LastName%%", 
                                                       "%%Email%%", 
                                                       "%%BusinessName%%", 
                                                       "%%CompanyNumber%%", 
                                                       "%%Address%%", 
                                                       "%%Phone%%", 
                                                       "%%Gender%%",
                                                       "%%PurchaseId%%",
                                                       "%%PurchaseDate%%", 
                                                       "%%PurchaseDetails%%",
                                                       "%%MandateReference%%",
                                                       "%%TotalAmount%%",
                                                       "%%SignDate%%" );
                                        $real = array($transdetails['user_firstname'], 
                                                      $transdetails['user_lastname'],
                                                      $transdetails["user_email"],
                                                      $transdetails["user_company_name"],
                                                      $transdetails['user_company_number'],
                                                      $transdetails["user_company_address"],
                                                      $transdetails['user_phone'],
                                                      $transdetails['user_gender'],
                                                      $check,
                                                      $transdetails['opt_option_purchase_date'],
                                                      $purchasedoptions,
                                                      $mandateref,
                                                      $transdetails['pyt_amount'],
                                                      date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                                    );
                                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];                                    
                                        $x= $this->send_mail($transdetails["user_email"], $subject, $msg);
                                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                                        $this->session->set_userdata("slimpay_error", "No");
                                        redirect(base_url("Users/completePaymentAction"));
                                    }
                                }else{                                    
                                    $this->session->unset_userdata("purchase_option");
                                    $this->slimpay_lib->signSlimpayMandate($jsonobj);
                                }
                            }else{
                                $this->session->unset_userdata("purchase_option");
                                $this->slimpay_lib->signSlimpayMandate($jsonobj);
                            }
                        }
                    //Slimpay Integration End
                }
                else
                {
                    $this->session->set_flashdata("error", "<label class='text-danger'>Option non achetée, veuillez réessayer.</label>");
                    redirect(base_url("Users/account"));
                }
            }
            
        }
        else
        {
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        } 
    }

    function checkforMandate(){
        $ses = $this->session->userdata("user_id");
            
        if(!empty($ses)){
            $this->load->library("Slimpay_lib");
            $mandateexists = $this->User->getUserMandate(array("slmref_userid"=>$ses));
         
            if(!empty($mandateexists)){
              
                $mandateref = $mandateexists['slmref_mandateref'];
          
                $mandatestate = $this->slimpay_lib->getMandateState(array("reference"=>$mandateref));
                if($mandatestate['state'] == "active"){
                    echo json_encode(array("status"=>200, "open"=>true));
                }else{
                    echo json_encode(array("status"=>200, "open"=>false));
                }
            }else{
                echo json_encode(array("status"=>200, "open"=>false));
            }
        }else{
            echo json_encode(array("status"=>1));
        }

      
    }



    function downloadMandateref(){
        $this->load->library("Slimpay_lib");
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $mandate = $this->User->getUserMandate(array("slmref_userid"=>$ses));
            if(!empty($mandate)){
                $content = $this->slimpay_lib->getMandateDocument(array('reference'=>$mandate['slmref_mandateref']));
                $mandatid = $mandate['slmref_mandateref'];
                file_put_contents("assets/temp/$mandatid.pdf",$content);
                $path = "assets/temp/$mandatid.pdf";
                $filename = $mandatid.".pdf";                
                header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly                
                header('Accept-Ranges: bytes');  // For download resume
                header('Content-Length: ' . filesize($path));  // File size
                header('Content-Encoding: none');
                header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
                header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog                
                readfile($path);  //this is nec
                unlink($path);
            }
        }else{
            redirect(base_url());
        }
    }

    public function UpdateMandateBankAccount(){
        $ses = $this->session->userdata("user_id");
        $mandate = $this->input->post("mandate");
        if(!empty($ses) && !empty($mandate)){
            $this->load->library("Slimpay_lib");
            $details = $this->User->getUserMandate(array("slmref_userid"=>$ses, "slmref_mandateref"=>$mandate));
            if(!empty($details)){
                $url = $this->slimpay_lib->updateBankAccount(array("subscriber"=>$details['slmref_subscriber'], "mandate"=>$details['slmref_mandateref']));
                if($url){
                    echo json_encode(array("status"=>200, "msg"=>$url));
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Something went wrong, Please try after sometime.</lable>"));
                }
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Accès refusé.</lable>"));
            }
        }        
    }

    function headOfListsOptions(){
        // echo "<pre>";
        // print_r($this->input->post());
        // exit;
        if(!empty($this->session->userdata("user_id"))){
            $opt_user_id = $this->session->userdata('user_id');
            $opt_option_id = $this->input->post('opt_id');   
            $opt_type = $this->input->post('opt_type');            
            $startfrom = $this->input->post("perdaysdate");
            $endfrom = $this->input->post("perdayedate");
            $opt_option_purchase_date = date("Y/m/d", time());            
            $optid = $this->input->post('price_type');
            $package = $this->User->get_single_options_price($optid);
            $otp_option_duration = $package['opt_price_type'];
            $opt_option_price = $package['opt_price'];
            $otp_option_qnty = $package['opt_qnty'];            
            $opt_option_active_date = null;
            $opt_option_end_date = null;
            $opt_option_inactive_validity=null;
            $otp_search_city = "";
            if(empty($opt_type))
            {
               $opt_type=1; 
            }

            if($opt_type == 4){
                $pkgoptions = $this->User->getPackageOptionRows(array("conditions"=>array("pkg_package_id"=>$opt_option_id)));                    
                $optionbuy = array("opt_user_id" => $opt_user_id,
                                   "opt_option_id" => $opt_option_id,
                                   "opt_option_type" => $opt_type,
                                   "opt_option_purchase_date" => $opt_option_purchase_date,
                                   "otp_option_duration" => $otp_option_duration,
                                   "otp_option_qnty"=>$otp_option_qnty,
                                   "opt_option_price" => $opt_option_price,
                                   "otp_search_city"=>$otp_search_city,
                                   "opt_option_active_date" => $opt_option_active_date,
                                   "opt_option_end_date"=>$opt_option_end_date,
                                   "opt_option_inactive_validity"=> $opt_option_inactive_validity);            
                $arr = array("pyt_txn_status"=>"Pending");
                $check = $this->User->insertTransaction($arr);
                $optionbuy['opt_tran_id'] = $check;
                $pkgid = $this->User->insertUserOption($optionbuy);
                if(!empty($pkgoptions)){
                    foreach($pkgoptions as $pkopt){                        
                        $optionbuy1 = array("opt_user_id" => $opt_user_id,
                                           "opt_option_id" => $pkopt['pkg_inc_optid'],
                                           "opt_option_type" => 5,
                                           "opt_pkg_id"=>$pkgid,
                                           "opt_option_purchase_date" => $opt_option_purchase_date,
                                           "otp_option_duration" => $pkopt['pkg_opt_duration'],
                                           "otp_option_qnty"=>$pkopt['pkg_opt_qty'],
                                           "opt_option_price" => 0,
                                           "otp_search_city"=>"",
                                           "opt_option_active_date" => "",
                                           "opt_option_end_date"=>"",
                                           "opt_option_inactive_validity"=> "",
                                           "opt_tran_id"=>0
                                       );
                        if($pkopt['opt_id'] == 7 || $pkopt['opt_id'] == 8){                        

                            $arr = array("event_status"=>0,
                                         "event_is_free"=>0,
                                         "created_by_user"=>$opt_user_id
                                        );                            
                            if($pkopt['opt_id'] == 8)
                                $arr['event_goodplace'] = 1;
                            $optionbuy1['opt_event_id'] = $this->User->insertEventwithougNoti($arr);
                        }
                        // echo "<pre>";
                        // print_r($optionbuy);                        
                        // echo "</pre>";
                        $y = $this->User->insertUserOption($optionbuy1);
                    }                    
                }
                //Paypal Code Integration start
                    $this->load->library("paypal_lib");
                    //Set variables for paypal form
                    $returnURL = base_url().'Users/completePayment'; //payment success url
                    $cancelURL = base_url().'Users/cancelPayment'; //payment cancel url
                    $notifyURL = base_url().'Pages/paymentIpn'; //ipn url
                    //get particular product data                    
                    $this->paypal_lib->add_field('return', $returnURL);
                    $this->paypal_lib->add_field('cancel_return', $cancelURL);
                    $this->paypal_lib->add_field('notify_url', $notifyURL);
                    $this->paypal_lib->add_field('item_name', "YellowPages");
                    $this->paypal_lib->add_field('custom', $check);
                    $this->paypal_lib->add_field('item_number',  "1");
                    $this->paypal_lib->add_field('amount', $opt_option_price );
                    $this->paypal_lib->paypal_auto_form();
                //Paypal Code Integration End    
            }else{

                if($otp_option_duration == "1 Day"){
                    if($opt_option_id == 2)
                        $otp_search_city = $this->input->post("city");
                    $opt_option_active_date = $startfrom;
                    $opt_option_end_date = $endfrom;
                    $date1 = date_create($opt_option_active_date);
                    $date2 = date_create($opt_option_end_date);        
                    $difference = date_diff($date1,$date2);
                    $diff = $difference->format('%a');
                    if($diff > 0 )
                        $opt_option_price = $diff*$opt_option_price;
                }
                
                $optionbuy = array("opt_user_id" => $opt_user_id,
                                   "opt_option_id" => $opt_option_id,
                                   "opt_option_type" => $opt_type,
                                   "opt_option_purchase_date" => $opt_option_purchase_date,
                                   "otp_option_duration" => $otp_option_duration,
                                   "otp_option_qnty"=>$otp_option_qnty,
                                   "opt_option_price" => $opt_option_price,
                                   "otp_search_city"=>$otp_search_city,
                                   "opt_option_active_date" => $opt_option_active_date,
                                   "opt_option_end_date"=>$opt_option_end_date,
                                   "opt_option_inactive_validity"=> $opt_option_inactive_validity);            
                $arr = array("pyt_txn_status"=>"Pending");
                $check = $this->User->insertTransaction($arr);            
                if($check)
                {
                    if($opt_option_id == 7 || $opt_option_id == 8){
                        $arr = array("event_status"=>0,
                                     "event_is_free"=>0,
                                     "created_by_user"=>$opt_user_id
                                    );
                        if($otp_option_duration == "1 Day"){
                            $arr['event_publish_start'] = $opt_option_active_date;
                            $arr['event_publish_end'] = $opt_option_end_date;
                        }
                        if($opt_option_id == 8)
                            $arr['event_goodplace'] = 1;
                        $optionbuy['opt_event_id'] = $this->User->insertEventwithougNoti($arr);
                    }
                   // if($opt_option_id == 10){
                    if($opt_type == 3){
                        $userdetails = $this->User->getRows(array("user_id"=>$opt_user_id));
                        $subject = "Mess Option Purchase";
                        $msginner = "<br><p>Purchase details are given below:</p>
                                     <p>Purchase option type: ".$package['opt_text']."</p>
                                     <p>Option Price: ".$package['opt_price']."</p>";

                        $msg = $this->sendMailFormat($subject, $msginner, 23);
                        $x= $this->send_mail($userdetails['user_email'], $subject, $msg);

                          //Admin
                        $emailadmin = ADMIN_EMAIL;
                        $msgadmin = $this->sendMailFormat($subject, $msginner, 23);
                        $x= $this->send_mail($emailadmin, $subject, $msgadmin);
                    }
                    $optionbuy['opt_tran_id'] = $check;
                    $y = $this->User->insertUserOption($optionbuy);

                    // $y = $this->User->insertUserOption($optionbuy);
                    //Paypal Code Integration start
                        $this->load->library("paypal_lib");
                        //Set variables for paypal form
                        $returnURL = base_url().'Users/completePayment'; //payment success url
                        $cancelURL = base_url().'Users/cancelPayment'; //payment cancel url
                        $notifyURL = base_url().'Pages/paymentIpn'; //ipn url
                        //get particular product data                    
                        $this->paypal_lib->add_field('return', $returnURL);
                        $this->paypal_lib->add_field('cancel_return', $cancelURL);
                        $this->paypal_lib->add_field('notify_url', $notifyURL);
                        $this->paypal_lib->add_field('item_name', "YellowPages");
                        $this->paypal_lib->add_field('custom', $check);
                        $this->paypal_lib->add_field('item_number',  "1");
                        $this->paypal_lib->add_field('amount', $opt_option_price );
                        $this->paypal_lib->paypal_auto_form();
                    //Paypal Code Integration End
                                
                }
                else
                {
                    $this->session->set_flashdata("error", "<label class='text-danger'>Option non achetée, veuillez réessayer.</label>");
                    redirect(base_url("Users/account"));
                }
            }
        }
        else
        {
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé</label>");
            redirect(base_url());
        }
    }

    function userOptions(){       
        $date = $this->input->post("stdate");
        $option = $this->input->post("useroptions");       
        if(!empty($date) && !empty($option)){
            $query = $this->User->getSingleOptiondetails($option);
            $checkactivation = $this->checkActivatedOptionDateForClassOnly($query['opt_option_id'], $date);
            if($checkactivation){
                $duration = $query['otp_option_duration'];
                $exp_date = date("Y-m-d", strtotime($date." + $duration"));
                $arr = array("opt_option_active_date"=>$date,
                             "opt_option_end_date"=>$exp_date,
                             "opt_option_status"=>1
                            );
                if($query['opt_option_id'] == 2){
                    $arr['otp_search_city'] = $this->input->post("city");
                    $latlong = $this->User->get_lat_long($this->input->post("city"));
                    $arr['opt_city_lat'] = $latlong['lat'];
                    $arr['opt_city_long'] = $latlong['lng'];
                }
                
                $check = $this->User->updateUserOption($arr, array("opt_user_option_id"=>$option));
                $userdetails = $this->User->getUserRowsFromOption($option);
                $email = $userdetails['user_email'];
                $opt_option_purchase_date = $userdetails['opt_option_purchase_date'];            
                $opt_option_price = $userdetails['opt_option_price'];
                $opt_option_id = $userdetails['opt_option_id'];            
                if($userdetails['opt_option_id'] == 6){
                    $otp_option_duration = $userdetails['otp_option_qnty']." Pictures For ".$userdetails['otp_option_duration'];
                }else{
                    $otp_option_duration = $userdetails['otp_option_duration'];
                }

                $opt_st_date = date("d F, Y", strtotime($userdetails['opt_option_active_date']));
                if($userdetails['opt_option_active_date'] == date("Y-m-d")){
                    if($userdetails['opt_option_id'] == 3){
                        $this->User->update(array("publishonhome"=>1), array("user_id"=>$userdetails['opt_user_id']));
                    }
                    $today = 1;
                }
                else{
                    $today = 0;
                }
                $opt_ed_date = date("d F, Y", strtotime($userdetails['opt_option_end_date']));

                //Fetch Email Templates
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>20));
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer

                $subject = $templatecenter['tmplate_subject'];
                $dummy = array("%%Firstname%%",
                               "%%LastName%%", 
                               "%%Email%%", 
                               "%%BusinessName%%", 
                               "%%CompanyNumber%%", 
                               "%%Address%%", 
                               "%%Phone%%", 
                               "%%Gender%%", 
                               "%%OptionName%%", 
                               "%%OptionsDescription%%", 
                               "%%PurchaseDate%%", 
                               "%%OptionDuration%%", 
                               "%%OptionPrice%%", 
                               "%%OptionsVisibleCity%%", 
                               "%%OptionActiveStartDate%%",
                               "%%OptionActiveEndDate%%" );
                $real = array($userdetails['user_firstname'], 
                              $userdetails['user_lastname'],
                              $userdetails["user_email"],
                              $userdetails["user_company_name"],
                              $userdetails['user_company_number'],
                              $userdetails["user_company_address"],
                              $userdetails['user_phone'],
                              $userdetails['user_gender'],
                              $userdetails['opt_name'],
                              $userdetails['opt_description'],
                              $userdetails['opt_option_purchase_date'],
                              $userdetails['otp_option_duration'],
                              $userdetails['opt_option_price'],
                              $userdetails['otp_search_city'],
                              $userdetails['opt_option_active_date'],
                              $userdetails['opt_option_end_date']
                            );
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                $x= $this->send_mail($userdetails["user_email"], $subject, $msg);        
                $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                $labels = $this->User->getTextContent(15);
                $cont = '<div class="optionaction"><button class="btn btn-xs btn-default tooltip"><i class="fa fa-thumbs-up"></i> <span class="tooltiptext">'.$labels['label87'].'</span></button><a href="'.base_url("Users/userPurchase/").$option.'" target="_blank" class="btn btn-xs btn-primary tooltip" details="'.$option.'"><i class="fa fa-file-text-o"></i> <span class="tooltiptext">'.$labels['label96'].'</span></a><button class="btn btn-xs btn-warning prtrecpt tooltip" atr-desc="'.$option.'"><i class="fa fa-print"></i> <span class="tooltiptext">'.$labels['label91'].'</span></button></div>';
                echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Activé avec succès.</label>" , "type"=>$userdetails['opt_id'], "activate"=>$today, "content"=>$cont));
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Vous avez déjà acheté cette option pour ces mêmes dates.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Veuillez saisir les champs requis.</label>"));
        }
    }

    function userOptionsPurchaseDetails(){
        $id = $this->input->post("detailsFor");


        if(!empty($id) && !empty($this->session->userdata("user_id"))){
            $query = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata("user_id"), "opt_user_option_id"=>$id));
            $msg = "";
            if(!empty($query)){
                foreach($query as $dt){ }
                //$vars= print_r($dt, true);
                $msg .= '<div class="form-group"><label>Purchase ID: </label> <span>'.$dt['opt_user_option_id'].'</span></div>
                         <div class="form-group"><label>Purchased Option: </label> <span>'.$dt['opt_name'].'</span></div>
                         <div class="form-group"><label>Status: </label> <span>'.$dt['pyt_txn_status'].'</span></div>
                         <div class="form-group"><label>Description: </label> <span>'.$dt['opt_discription'].'</span></div>';
                   
                    if($dt['otp_option_duration'] == "1 Day"){
                    $date1 = date_create($dt['opt_option_active_date']);
                    $date2 = date_create($dt['opt_option_end_date']);        
                    $difference = date_diff($date1,$date2);
                    $diff = $difference->format('%a');
                    if($diff > 0)
                        $duration = $diff." Jours";
                    else 
                        $duration = "1 Jour";
                    $msg .= '<div class="form-group"><label>Duration: </label> <span>'.$duration.'</span></div>';
                     }else{
                        $msg .= '<div class="form-group"><label>Duration: </label> <span>'.$dt['otp_option_duration'].'</span></div>';
                      }
                        $msg .= '<div class="form-group"><label>Price: </label> <span>$'.$dt['opt_option_price'].'</span></div>
                         <div class="form-group"><label>Purchase Date: </label> <span>'.$dt['opt_option_purchase_date'].'</span></div>';
                // if($dt['pyt_txn_status']  == "Completed"){
                //     $msg .= '<div class="form-group"><label>Payment Status: </label> <span class="text-success">Completed</span></div>
                //              <div class="form-group"><label>Transaction Id: </label> <span>'.$dt['pyt_txn_id'].'</span></div>';
                // }else{
                //     $msg .= '<div class="form-group"><label>Payment Status: </label> <span class="text-danger">'.$dt['pyt_txn_status'].'</span></div>';
                // }  
                if($dt['opt_option_status'] == 0)
                    $msg .= "<div class='form-group'><label>Option Status: </label><span> Not Active</span></div>";
                if($dt['opt_option_status'] == 1)
                    $msg .= "<div class='form-group'><label>Option Status: </label><span class='text-success'> Active</span></div>";
                if($dt['opt_option_status'] == 3)
                    $msg .= "<div class='form-group'><label>Option Status: </label><span class='text-danger'> Expired</span></div>";                

                if($dt['opt_option_status'] != 0 && $dt['pyt_txn_status']  == "Completed"){
                    $msg .= '<div class="form-group"><label>Activate Date: </label> <span>'.$dt['opt_option_active_date'].'</span></div>
                             <div class="form-group"><label>End Date: </label> <span>'.$dt['opt_option_end_date'].'</span></div>';
                    if($dt['opt_option_id'] == 2)
                        $msg .= '<div class="form-group"><label>Search City: </label> <span>'.$dt['otp_search_city'].'</span></div>';
                }

                echo json_encode(array("status"=>200, "msg"=>$msg));
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Details are not available.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Accès refusé.</label>"));
        }
    }


    function userOptionsPurchaseDetailsStatic()
    {
        $id = $this->input->post("detailsFor");
        if(!empty($id) && !empty($this->session->userdata("user_id")))
        {
           // $query = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata("user_id"), "opt_user_option_id"=>$id));
           
            $query = $this->User->getUserAdvOptionsDetails($id);

            $msg = "OKK";
            if(!empty($query))
            {
                
                // foreach($query as $rows){ 
                //         $msg .= '<tr><td width="30%">Option Name</td><td>'. $rows['opt_name'].'</td></tr>
                //          <tr><td>Option Description</td><td>'. $rows['Description'].'</td></tr>
                //          <tr><td>First Name</td><td>'. $rows['FirstName'].'</td></tr>
                //          <tr><td>Last Name</td><td>'. $rows['LastName'].'</td></tr>
                //          <tr><td>Gender</td><td>'. $rows['gender'].'</td></tr>
                //          <tr><td>Telephone</td><td>'. $rows['Telphone'].'</td></tr>
                //          <tr><td>Submission Date</td><td>'. $rows['StartDate'].'</td></tr>
                //          <tr><td>Budget</td><td>'. $rows['Budget'].'</td></tr>';
                         
                // }
                   
                                  
                
            echo json_encode(array("status"=>200, "msg"=>$msg));
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Details are not available.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Accès refusé.</label>"));
        }
    }


    function printPurchasedReceipt(){
        $id = $this->input->post("detailsFor");
        if(!empty($id) && !empty($this->session->userdata("user_id"))){
            $query = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata("user_id"), "opt_user_option_id"=>$id));
            $msg = "";
            if(!empty($query)){
                foreach($query as $dt){ }
                $msg .= '<div class="form-group"><label>Purchase ID: </label> <span>'.$dt['opt_user_option_id'].'</span></div>
                         <div class="form-group"><label>Purchased Option: </label> <span>'.$dt['opt_name'].'</span></div>';
                if($dt['otp_option_duration'] == "1 Day"){
                    $date1 = date_create($dt['opt_option_active_date']);
                    $date2 = date_create($dt['opt_option_end_date']);        
                    $difference = date_diff($date1,$date2);
                    $diff = $difference->format('%a');
                    if($diff > 0)
                        $duration = $diff." Days";
                    else 
                        $duration = "1 Day";
                    $msg .= '<div class="form-group"><label>Duration: </label> <span>'.$duration.'</span></div>';
                }else{
                    $msg .= '<div class="form-group"><label>Duration: </label> <span>'.$dt['otp_option_duration'].'</span></div>';
                }
                $msg .= '<div class="form-group"><label>Price: </label> <span>$'.$dt['opt_option_price'].'</span></div>
                         <div class="form-group"><label>Purchase Date: </label> <span>'.$dt['opt_option_purchase_date'].'</span></div>';
                if($dt['pyt_txn_status']  == "Completed"){
                    $msg .= '<div class="form-group"><label>Payment Status: </label> <span class="text-success">Completed</span></div>
                             <div class="form-group"><label>Transaction Id: </label> <span>'.$dt['pyt_txn_id'].'</span></div>';
                }else{
                    $msg .= '<div class="form-group"><label>Payment Status: </label> <span class="text-danger">'.$dt['pyt_txn_status'].'</span></div>';
                }
                if($dt['opt_option_status'] == 0)
                    $msg .= "<div class='form-group'><label>Option Status: </label><span> Not Active</span></div>";
                if($dt['opt_option_status'] == 1)
                    $msg .= "<div class='form-group'><label>Option Status: </label><span class='text-success'> Active</span></div>";
                if($dt['opt_option_status'] == 3)
                    $msg .= "<div class='form-group'><label>Option Status: </label><span class='text-danger'> Expired</span></div>";                

                if($dt['opt_option_status'] != 0 && $dt['pyt_txn_status']  == "Completed"){
                    $msg .= '<div class="form-group"><label>Activate Date: </label> <span>'.$dt['opt_option_active_date'].'</span></div>
                             <div class="form-group"><label>End Date: </label> <span>'.$dt['opt_option_end_date'].'</span></div>';
                    if($dt['opt_option_id'] == 2)
                        $msg .= '<div class="form-group"><label>Search City: </label> <span>'.$dt['otp_search_city'].'</span></div>';
                }
                echo json_encode(array("status"=>200, "msg"=>$msg));
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Details are not available.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, Accès refusé.</label>"));
        }
    }

    function completePayment(){
        $data = array();
        // if($opt_option_id == 10){
            // $subject = "Mess Option Purchase";
            // $msginner = "<br><p>Purchase details are given below:</p>
            //             <p>Purchase option type: ".$package['opt_text']."</p>
            //             <p>Option Price: ".$package['opt_price']."</p>";

            // $msg = $this->sendMailFormat($subject, $msginner, 23);
            // $fromemail = SENDER_EMAIL;
            // $fromname = SENDER_NAME;

            // $x= $this->send_mail($email, $subject, $msg);

            //   //Admin
            // $emailadmin = ADMIN_EMAIL;
            // $msgadmin = $this->sendMailFormat($subject, $msginner, 23);
            // $fromemail = SENDER_EMAIL;
            // $fromname = SENDER_NAME;
            // $x= $this->send_mail($emailadmin, $subject, $msgadmin);
        // }

        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'User Dashboard');
        $data["texts"] = $this->User->getTextContent(25);
        $this->template->load('home_layout', 'contents' , 'paymentcomplete', $data);

        //mail from here
    }

    function slimpayPaymentConfirmation(){
        if(!empty($this->session->userdata("user_id"))){
            $reference = $this->session->userdata("slimpay_orderstatus");
            if(!empty($reference)){
                $this->load->library("Slimpay_lib");
                $orderstatus = $this->slimpay_lib->getOrder(array("reference"=>$reference));

                //purchase order related information
                    $transdetails = $this->User->getSlimpayTransactionDetails($reference);
                    $optiondetails = $this->User->getOptionDetails($transdetails['pyt_id']);
                    if(!empty($optiondetails)){
                        $purchasedoptions = "";
                        foreach($optiondetails as $txnopt){
                            $opt_option_purchase_date = $txnopt['opt_option_purchase_date'];
                            $opt_option_price = $txnopt['opt_option_price'];
                            $opt_option_id = $txnopt['opt_option_id'];
                            $checkduration = $txnopt['otp_option_duration'];
                            $opt_type = $txnopt['opt_option_type'];                
                            $mstopt = $this->User->getMasterOption($txnopt['opt_option_id']);                
                            if($opt_type == 4){
                                $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                            }else{
                                if($txnopt['otp_option_duration'] == "1 Day" ){
                                    if($opt_option_id != 7 && $opt_option_id != 8){
                                        $this->User->updateUserOption(array("opt_option_status"=>1), array("opt_user_option_id"=>$txnopt['opt_user_option_id']));
                                    }                                        
                                    $date1 = date_create($txnopt['opt_option_active_date']);
                                    $date2 = date_create($txnopt['opt_option_end_date']);        
                                    $difference = date_diff($date1,$date2);
                                    $diff = $difference->format('%a');
                                    if($diff > 0)
                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> ".$diff." Days (".$txnopt['opt_option_active_date']." to ".$txnopt['opt_option_end_date'].")</p>";
                                    else
                                        $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b> 1 Day (".$txnopt['opt_option_active_date'].")";                            
                                }
                                if($txnopt['opt_option_id'] == 6){
                                    $purchasedoptions .= "<p<b>".$mstopt['opt_name'].": </b>".$txnopt['otp_option_qnty']." Pictures For ".$txnopt['otp_option_duration'];                        
                                }else{
                                    $purchasedoptions .= "<p><b>".$mstopt['opt_name'].":</b> ".$txnopt['otp_option_duration']."</p>";
                                }
                            }
                        }
                    }
                //purchase order related information
                if(strpos($orderstatus->getState()['state'], 'closed.aborted') === 0) {                    
                    $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                    $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>35));
                    $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                    $subject = $templatecenter['tmplate_subject'];
                    $dummy = array("%%Firstname%%",
                                   "%%LastName%%", 
                                   "%%Email%%", 
                                   "%%BusinessName%%", 
                                   "%%CompanyNumber%%", 
                                   "%%Address%%", 
                                   "%%Phone%%", 
                                   "%%Gender%%",
                                   "%%PurchaseId%%",
                                   "%%PurchaseDate%%", 
                                   "%%PurchaseDetails%%",
                                   "%%MandateReference%%",
                                   "%%TotalAmount%%",
                                   "%%SignDate%%" );
                    $real = array($transdetails['user_firstname'], 
                                  $transdetails['user_lastname'],
                                  $transdetails["user_email"],
                                  $transdetails["user_company_name"],
                                  $transdetails['user_company_number'],
                                  $transdetails["user_company_address"],
                                  $transdetails['user_phone'],
                                  $transdetails['user_gender'],
                                  $transdetails['pyt_id'],
                                  $transdetails['opt_option_purchase_date'],
                                  $purchasedoptions,
                                  $mandateref,
                                  $transdetails['pyt_amount'],
                                  date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                );
                    $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                    $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                    $x= $this->send_mail($transdetails["user_email"], $subject, $msg);        
                    $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                    $this->session->unset_userdata("slimpaymandate_Sign");
                    $this->User->updateTable("otd_user_option_payment", array("pyt_txn_status"=>"Cancelled", "py_slm_state"=>$orderstatus->getState()['state']), array("py_slmref_id"=>$reference));
                    $this->session->set_flashdata("slimpay_cancelorder", "<label class='text-success'>You have cancelled order.</label>");
                    redirect(base_url("Users/account/messoptions"));
                }elseif (strpos($orderstatus->getState()['state'], 'closed.completed') === 0) {                    
                    $mandateref = $this->slimpay_lib->getMandateReference($orderstatus);
                    $this->User->updateTable("otd_user_option_payment", array("pyt_txn_status"=>"Completed", "py_slm_state"=>$orderstatus->getState()['state'], "py_slmMandate_ref"=>$mandateref), array("py_slmref_id"=>$reference));
                    $this->User->insertMandate($mandateref);
                    $issignmandate = $this->session->userdata("slimpaymandate_Sign");
                    if($issignmandate == "Yes"){
                        $this->session->unset_userdata("slimpaymandate_Sign");
                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>33));
                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                        $subject = $templatecenter['tmplate_subject'];
                        $dummy = array("%%Firstname%%",
                                       "%%LastName%%", 
                                       "%%Email%%", 
                                       "%%BusinessName%%", 
                                       "%%CompanyNumber%%", 
                                       "%%Address%%", 
                                       "%%Phone%%", 
                                       "%%Gender%%",
                                       "%%PurchaseId%%",
                                       "%%PurchaseDate%%", 
                                       "%%PurchaseDetails%%",
                                       "%%MandateReference%%",
                                       "%%TotalAmount%%",
                                       "%%SignDate%%" );
                        $real = array($transdetails['user_firstname'], 
                                      $transdetails['user_lastname'],
                                      $transdetails["user_email"],
                                      $transdetails["user_company_name"],
                                      $transdetails['user_company_number'],
                                      $transdetails["user_company_address"],
                                      $transdetails['user_phone'],
                                      $transdetails['user_gender'],
                                      $transdetails['pyt_id'],
                                      $transdetails['opt_option_purchase_date'],
                                      $purchasedoptions,
                                      $mandateref,
                                      $transdetails['pyt_amount'],
                                      date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                    );
                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                        $x= $this->send_mail($transdetails["user_email"], $subject, $msg);        
                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);                        
                    }else{
                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>34));
                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                        $subject = $templatecenter['tmplate_subject'];
                        $dummy = array("%%Firstname%%",
                                       "%%LastName%%", 
                                       "%%Email%%", 
                                       "%%BusinessName%%", 
                                       "%%CompanyNumber%%", 
                                       "%%Address%%", 
                                       "%%Phone%%", 
                                       "%%Gender%%",
                                       "%%PurchaseId%%",
                                       "%%PurchaseDate%%", 
                                       "%%PurchaseDetails%%",
                                       "%%MandateReference%%",
                                       "%%TotalAmount%%",
                                       "%%SignDate%%" );
                        $real = array($transdetails['user_firstname'], 
                                      $transdetails['user_lastname'],
                                      $transdetails["user_email"],
                                      $transdetails["user_company_name"],
                                      $transdetails['user_company_number'],
                                      $transdetails["user_company_address"],
                                      $transdetails['user_phone'],
                                      $transdetails['user_gender'],
                                      $transdetails['pyt_id'],
                                      $transdetails['opt_option_purchase_date'],
                                      $purchasedoptions,
                                      $mandateref,
                                      $transdetails['pyt_amount'],
                                      date("d m, Y h:i A", strtotime($transdetails['py_slm_datecreated']))
                                    );
                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                        $x= $this->send_mail($userdetails["user_email"], $subject, $msg);        
                        $y= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                    }
                    header("Access-Control-Allow-Origin: *");
                    $this->template->set('title', 'User Dashboard');
                    $data["texts"] = $this->User->getTextContent(25);
                    $this->template->load('home_layout', 'contents' , 'paymentcomplete', $data);                    
                }
            }else{
                $this->session->set_flashdata("error", "<label class='text-danger'>Order Details expired.</label>");
                redirect(base_url("Users/account/messoptions"));
            }
        }else{
            $this->session->set_flashdata("error", "<label class='text-danger'>Accès refusé.</label>");
            redirect(base_url());
        }
        // if($opt_option_id == 10){
            // $subject = "Mess Option Purchase";
            // $msginner = "<br><p>Purchase details are given below:</p>
            //             <p>Purchase option type: ".$package['opt_text']."</p>
            //             <p>Option Price: ".$package['opt_price']."</p>";

            // $msg = $this->sendMailFormat($subject, $msginner, 23);
            // $fromemail = SENDER_EMAIL;
            // $fromname = SENDER_NAME;

            // $x= $this->send_mail($email, $subject, $msg);

            //   //Admin
            // $emailadmin = ADMIN_EMAIL;
            // $msgadmin = $this->sendMailFormat($subject, $msginner, 23);
            // $fromemail = SENDER_EMAIL;
            // $fromname = SENDER_NAME;
            // $x= $this->send_mail($emailadmin, $subject, $msgadmin);
        // }       

        //mail from here
    }

    function slimpayIPN(){
        $body = file_get_contents('php://input');
        // $order = \HapiClient\Hal\Resource::fromJson($body);
        $this->load->library("Slimpay_lib");
        $orderstatus = $this->slimpay_lib->getIPNOrder($body);
        $this->User->updateTable("otd_user_option_payment", array("py_slm_state"=>$orderstatus->getState()['state']), array("py_slmref_id"=>$reference));
        if (strpos($orderstatus->getState()['state'], 'closed.aborted') === 0) {
        }elseif (strpos($orderstatus->getState()['state'], 'closed.completed') === 0) {
            $mandateref = $this->slimpay_lib->getMandateReference($orderstatus);
            $this->User->updateTable("otd_user_option_payment", array("pyt_txn_status"=>"Completed", "py_slm_state"=>$orderstatus->getState()['state'], "py_slmMandate_ref"=>$mandateref), array("py_slmref_id"=>$reference));            
        }
    }

    function cancelPayment(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $ses = $this->session->set_flashdata("cancelled", "Your Last order has been cancelled");
            redirect(base_url("Users/account/messoptions"));
        }else{
            $ses = $this->session->set_flashdata("error", "Accès refusé");
            redirect(base_url("Users/account/messoptions"));
        }

    }

    function eventsView($args){
        $user = $this->session->userdata("user_id");
        // if(!empty($user)){
            if(!empty($args)){
                $data['evnt'] = $this->User->eventDetails($args);
                header("Access-Control-Allow-Origin: *");
                $this->template->set('title', 'Event Details');
                $this->template->load('home_layout', 'contents' , 'EventDetails', $data);                
            }else{
                redirect(base_url("Users/account"));
            }
        // }else{
        //     redirect(base_url());
        // }
    }


    function myFormOption ($args){
        $user = $this->session->userdata("user_id");
        if(!empty($user)){
            if(!empty($args)){                                
                $data['formoption'] = $this->User->getUserAdvOptionsDetails($args);
                header("Access-Control-Allow-Origin: *");
                $this->template->set('title', 'Option Details');
                $this->template->load('home_layout', 'contents' , 'myOptionDetails', $data);                
            }else{
                redirect(base_url("Users/account"));
            }
        }else{
            redirect(base_url());
        }
    }
    
    function myEvents($args){
        $user = $this->session->userdata("user_id");
        if(!empty($user)){
            if(!empty($args)){                                
                $data['evnt'] = $this->User->myEventDetails($args);
                header("Access-Control-Allow-Origin: *");
                $this->template->set('title', 'Event Details');
                $this->template->load('home_layout', 'contents' , 'EventDetails', $data);                
            }else{
                redirect(base_url("Users/account"));
            }
        }else{
            redirect(base_url());
        }
    }

    function eventDetails(){
        $id = $this->input->post("event");
        $ses = $this->session->userdata("user_id");
        if(!empty($ses) && !empty($id)){
            $check = $this->User->getEventsRows(array("event_id"=>$id));
            $suggestion = $this->User->getSuggestions();
            $labels = $this->User->getTextContent(15);
            if(!empty($check)){
                if($check['event_is_free'] && $check['event_status'] != 3 && $check['opt_option_status'] == 0){
                    $content = '<div class="col-md-6"><div class="form-group"><label class="form-label">'.$labels['label37'].':<span class="tooltip">?<span class="tooltiptext">'.$suggestion['label37'].'</span></span></label><input readonly type="text" name="pesdate" class="form-control edatepickerfuture" placeholder="'.$labels['label37'].'"></div></div><div class="col-md-6"><div class="form-group"><label class="form-label confm-pass">'.$labels['label38'].':<span class="tooltip">?<span class="tooltiptext">'.$suggestion['label38'].'</span></span></label><input readonly type="text" name="peedate" class="form-control edatepickerfuture" placeholder="'.$labels['label38'].'"></div></div>';
                    echo json_encode(array("status"=>200, "type"=>1, "content"=>$content));
                }else if($check['otp_option_duration'] == "1 Day" && $check['opt_option_status'] == 0){
                    $content = '';
                    echo json_encode(array("status"=>200, "type"=>2, "content"=>$content));
                }else{
                   $content = '<div class="col-md-12"><div class="form-group"><label class="form-label">'.$labels['label37'].':<span class="tooltip">?<span class="tooltiptext">'.$suggestion['label37'].'</span></span></label><input readonly type="text" name="pesdate" class="form-control edatepickerfuture" placeholder="'.$labels['label37'].'"></div></div>';
                   echo json_encode(array("status"=>200, "type"=>3, "content"=>$content));
                }
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Accès refusé.</label>"));    
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Accès refusé.</label>"));
        }
    }

    /*
     * full_map
     */
    public function full_map()
    {
        $data = array();
        $user_id = $this->uri->segment(3);
        $data['business'] = $this->User->getBusinessProfile(array('user_id'=>$user_id));
        header("Access-Control-Allow-Origin: *");                     
             $this->template->set('title', 'User Map');
             $this->template->load('home_layout', 'contents' , 'full_map', $data);

    }

    public function user_profile(){
         // die("Ok");
        $data = array();
        $is_claiming="";
        $data['is_claiming']="";
        $user_id = $this->uri->segment(3);
        // $ses = $this->session->userdata("user_id");
        // if(!empty($ses)){
        //     if($user_id == $ses)
        //         redirect(base_url("Users/account"));
        // }
        $is_claiming = $this->uri->segment(4);
        
        if($is_claiming == "claim_profile")
        {
            $data['is_claiming'] ="yes";
        }

        $data['user_phone'] = $this->User->getUserPhones($user_id);
        // $data['business'] = $this->User->getBusinessProfile(array('user_id'=>$user_id));
        $data['business'] = $this->User->getBusinessProfile(array("conditions"=>array('user_id'=>$user_id, "status!="=>3), "returnType"=>"single"));
        if(empty($data['business']))
            redirect(base_url());
         $data['similarprofile']  = $this->User->getSimilarBusinessProfile($user_id);
        $data['pics'] = $this->User->getProfilePics($user_id);

        $data['products'] = $this->User->getProducstServices($user_id, 2);
        $data['services'] = $this->User->getProducstServices($user_id, 3);
        $data['other'] = $this->User->getProducstServices($user_id, 4);

        $data['openhrs'] = $this->User->getOpeningHrs($user_id);
        $data["isopen"] = $this->is_open($data['openhrs']);
        $data["today_hrs"] = $this->today_hrs($data['openhrs']);

        $yes_online = $this->User->isBusinessOnline($user_id);
        $data['is_online'] = $yes_online;

        $data['prstable'] = $this->User->getBusinessPriceRows(array('user_id'=>$user_id));
        $data['reviewlist'] = $this->User->getReviewRows(array("conditions"=>array("voted_user_id"=>$user_id, "vote_status"=>1)));
        $data['TotalReview'] = $this->User->getTotalReview($user_id);
        $data['pagecontent'] = $this->User->getTextContent(10); // get page content        
        $data['rccontent'] = $this->User->getTextContent(16); // get Recommend content        
        $data['flcontent'] = $this->User->getTextContent(17); // get Follow content        
        $data['msgcontent'] = $this->User->getTextContent(18); // get messagepopup text content        
        $data['events'] =  $this->User->getEvents($user_id);
      
        $follower_id = $this->session->userdata('user_id') != "" ? $this->session->userdata('user_id') : 0;
       
        $fol= $this->User->isFollowing($user_id, $follower_id);
        if($fol == true)
        {
           // echo "Wow";
            $data['followed'] = "yes";
        }
        else
        {
         //echo "ohh";
            $data['followed'] = "no";   
        }

        $data['rec_count'] = $this->User->getrecommendUser($user_id);

             header("Access-Control-Allow-Origin: *");                     
             $this->template->set('title', 'Business Listing');
             $this->template->load('home_layout', 'contents' , 'profile', $data);
       
    }

    /*
    * claim_user_profile
    */
    public function claim_user_profile()
    {
        $data = array();
        $data["claimed"] = "";
        if($this->input->post('claimSubmit'))
        {        
            $user_gender = $this->input->post("user_gender");
            $first = $this->input->post("user_firstname");
            $last = $this->input->post("user_lastname");
            $email = $this->input->post("user_email");
            $user_phone = $this->input->post("user_phone");
            $user_company_number = $this->input->post("user_company_number");
            if(!empty($first) && !empty($last) && !empty($email))
            {             
                $array = array(
                               "claim_user_gender" => $user_gender,
                               "claim_user_firstname"=>$first,
                               "claim_user_lastname"=>$last,
                               "claim_user_phone"=>$user_phone,
                               "claim_user_email"=>$email,
                               "claim_user_company_number"=>$user_company_number
                                );
                $check = $this->User->claimBusinessProfile($array, array());
                $data["claimed"] = "yes";
                //echo $check;
                if($check)
                {
                    //Fetch Email Templates
                    $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                    $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>16));
                    $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                    $subject = $templatecenter['tmplate_subject'];
                    $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%BusinessName%%", "%%CompanyNumber%%", "%%Address%%", "%%Phone%%", "%%Gender%%");
                    $real = array($array['claim_user_firstname'], 
                                  $array['claim_user_lastname'],
                                  $array["claim_user_email"],
                                  $this->input->post("user_company_name"),
                                  $array['claim_user_company_number'],
                                  $this->input->post('user_company_address'),
                                  $array['claim_user_phone'],
                                  $array['claim_user_gender']);
                    $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                    $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                    $x= $this->send_mail($email, $subject, $msg);
                    $x= $this->send_mail(ADMIN_EMAIL, $subject, $msg);
                    $this->session->set_userdata("claim_success", "Yes");
                    redirect($this->input->post("url"));
                    //echo $x;
                    // $data['success_msg'] = 'Thanks for Claiming Your Business Profile. Our Administrative team is looking into your claim. We will process your request and infom you soon on your registered email ID.';
                }
                //Send email
            }            
        }
        // $data['texts'] = $this->User->getTextContent(24);
        // header("Access-Control-Allow-Origin: *");
        // $this->template->set('title', 'Thanks for Claim Request');
        // $this->template->load('inner_layout_reg', 'contents' , 'claim_request', $data);
    }



    /*
    * User account profile update
    */
    public function updateProfile(){
        $user = $this->session->userdata("isUserLoggedIn");
        if($user){
            $first = $this->input->post("first_name");
            $last = $this->input->post("last_name");
            $lang = $this->input->post("lang");
            $gender = $this->input->post("douhavekidsprice");
            $dob = $this->input->post("dob");
            if(!empty($first) && !empty($last) && !empty($lang) && !empty($gender) && !empty($dob)){
                $userid = $this->session->userdata("user_id");
                $lang = implode(",", $lang);
                $array = array("user_firstname"=>$first,
                               "user_lastname"=>$last,
                               "user_language"=>$lang,
                               "user_gender"=>$gender,
                               "user_dob"=>$dob,
                                );
                $check = $this->User->update($array, array("user_id"=>$userid));                
                $cont = '<div class="alert alert-success" role="alert">                      
                          <strong>Merci,</strong> les informations ont été mise à jour.
                        </div>';
                echo json_encode(array("status"=>200, "msg"=>$cont));                
            }
            else{
                $cont = '<div class="alert alert-danger" role="alert">                      
                          <strong></strong> Veuillez replir les champs requis.
                        </div>';
                echo json_encode(array("status"=>1, "msg"=>$cont));
            }
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert">                      
                      <strong></strong> Accès refusé
                    </div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }

    }

    public function follow_user()
    {
        $profile_id = $this->input->post("profile_id") != ""? $this->input->post("profile_id"): 0;
        $follow_action = $this->input->post("follow_action") != ""?$this->input->post("follow_action"): "follow";
        $follower_id = $this->session->userdata("user_id") != "" ? $this->session->userdata("user_id"): 0;

        if($profile_id == "")
        {
            $profile_id = 11;
        }
        if($follow_action == "follow")
        {
            $array = array("follower_user_id"=>$follower_id, 'followed_user_id'=>$profile_id);
            $check = $this->User->insertFollower($array);                
            $cont = 'You are now following the business.';
            echo json_encode(array("status"=>200, "msg"=>$cont));
        }
        else if($follow_action == "unfollow")
        {
            $array = array("follower_user_id"=>$follower_id, 'followed_user_id'=>$profile_id);
            $check = $this->User->deleteFollower($array);                
            if($check ==true)
            {
                $cont = 'You are now following the business.';
            }
            else
            {
                $cont = 'You have unfollowed the business.';    
            }
            echo json_encode(array("status"=>200, "msg"=>$cont));
        }


    }

    public function reviewcomplaint()
    {
        $review_id = $this->input->post("vote_id");
        $complaint_by_user_id = $this->session->userdata("user_id");
        if(!empty($review_id) && !empty($complaint_by_user_id)){
            $review = $this->User->getReviewRowSingle($review_id);
            if(!empty($review)){
                $review_text = $review['review_text'];
            }else{
                $review_text = "N/A";
            }
            $y =$this->User->insertUserReportedReview($review);
            $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
            $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>15)); 
            $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); //Email Footer
            $tempeventdetials = $this->User->getEventsRows(array("event_id"=>$eventid));
            $emailadmin = ADMIN_EMAIL;
            $subject = $templatecenter['tmplate_subject'];
            $reviewlink = "<a href='".base_url("Admin/reportReviewListing/$review_id")."'> Open Review</a>";
            $dummy = array("%%ComplainUserId%%", "%%ReviewId%%", "%%ReviewDetails%%", "%%ReviewLink%%");
            $real = array($complaint_by_user_id,
                          $review_id, 
                          $review_text, 
                          $reviewlink                          
                        );
            $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
            $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
            //Send mail to admin                
            $this->send_mail(ADMIN_EMAIL, $subject, $msg);
        }else{
            $this->session->set_flashdata("error", "Invalid User Access");
            $this->redirect(base_url());
        }        
    }

    /*
    * Update User Email
    */
    public function updateUserEmail(){
        $user = $this->session->userdata("isUserLoggedIn");
        if($user){
            $email = $this->input->post("email");
            $pass = $this->input->post("password");
            if(!empty($email) && !empty($pass)){
                $userid = $this->session->userdata("user_id");
                $con['conditions'] = array("user_id"=>$userid);
                $user = $this->User->getRows($con);                
                if($user[0]['user_password'] == md5($pass)){                    
                    $con = array();
                    $con['conditions'] = array("user_id !="=>$userid,
                                               "user_email"=>$email
                                              );
                    $con['returnType'] = "count";
                    $users = $this->User->getRows($con);
                    if($users > 0){
                        $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Cette email est déjà utilisée.</div>';
                        echo json_encode(array("status"=>1, "msg"=>$cont));                        
                    }
                    else{
                        $array = array("user_email"=>$email);                        
                        $check = $this->User->update($array, array("user_id"=>$userid));                
                        $cont = '<div class="alert alert-success" role="alert"><strong></strong> Adresse mail mise à jour.</div>';
                        echo json_encode(array("status"=>200, "msg"=>$cont));
                    }
                }
                else{
                    $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Les mots de passe ne correspondent pas.</div>';
                    echo json_encode(array("status"=>1, "msg"=>$cont));
                }
            }
            else{
                $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
                echo json_encode(array("status"=>1, "msg"=>$cont));
            }
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * Update User password
    */
    public function updateUserPassword(){
        $user = $this->session->userdata("isUserLoggedIn");
        if($user){            
            $old = $this->input->post("oldpass");
            $new = $this->input->post("newpass");            
            if(!empty($old) && !empty($new)){
                $userid = $this->session->userdata("user_id");
                $con['conditions'] = array("user_id"=>$userid);
                $user = $this->User->getRows($con);                
                if($user[0]['user_password'] == md5($old)){
                    $array = array("user_password"=>md5($new));
                    $check = $this->User->update($array, array("user_id"=>$userid));
                    $cont = '<div class="alert alert-success" role="alert"><strong></strong> Mot de passe mise à jour.</div>';
                    echo json_encode(array("status"=>200, "msg"=>$cont));
                }
                else{
                    $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Les mots de passe ne correspondent pas.</div>';
                    echo json_encode(array("status"=>1, "msg"=>$cont));
                }
            }
            else{
                $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
                echo json_encode(array("status"=>1, "msg"=>$cont));
            }
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * Deactivate user account
    */
    public function deactivateUserAccount(){
        $user = $this->session->userdata("isUserLoggedIn");
        if($user){
            $pass = $this->input->post("dpass");
            if(!empty($pass)){
                $userid = $this->session->userdata("user_id");
                $con['conditions'] = array("user_id"=>$userid);
                $user = $this->User->getRows($con);                
                if($user[0]['user_password'] == md5($pass)){
                    $array = array("status"=>3);
                    $check = $this->User->update($array, array("user_id"=>$userid));
                    $cont = '<div class="alert alert-success" role="alert"><strong></strong> Votre compte à bien été supprimé.</div>';
                    echo json_encode(array("status"=>200, "msg"=>$cont));
                }
                else{
                    $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Les mots de passe ne correspondent pas.</div>';
                    echo json_encode(array("status"=>1, "msg"=>$cont));
                }
            }
            else{
                $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
                echo json_encode(array("status"=>1, "msg"=>$cont));
            }
        }
        else{

            $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * Update User Business Details
    */
    function updateUserBusiness(){
        $cm_name = $this->input->post("cm_name");
        $cm_number = $this->input->post("cm_number");
        $cm_url = $this->input->post("cm_url");
        $cm_opening = $this->input->post("cm_opening");
        $cm_twitter = $this->input->post("cm_twitter");
        $cm_email = $this->input->post("cm_email");
        $cm_phnumber = $this->input->post("cm_phnumber");
        $cm_address = $this->input->post("cm_address");
        $cm_desc = $this->input->post("cm_desc");
        $business_category = $this->input->post("user_categories");
     
        // $newnumbers = $this->input->post("newphnumber");
        if(!empty($business_category)){
        //     $business_category = explode(",", $business_category);
        // }else{
            $this->User->removeExistingCategories(1);
        }
        
        //service filter
        $servicefilters = $this->input->post("servicefilters");
        if(empty($servicefilters)){
            // $servicefilters = explode(",", $servicefilters);
        // }else{
            $this->User->removeExistingCategories(2);
        }

        //product filter
        $productfilters = $this->input->post("productfilters");
        if(empty($productfilters)){
        //     $productfilters = explode(",", $productfilters);
        // }else{
            $this->User->removeExistingCategories(3);
        }

        //other filter
        $otherfilters = $this->input->post("otherfilters");
        if(empty($otherfilters)){
        //     $otherfilters = explode(",", $otherfilters);
        // }else{
            $this->User->removeExistingCategories(4);
        }

        if(!empty($cm_name) && !empty($cm_number) && !empty($cm_phnumber) && !empty($cm_address)){
            $userid = $this->session->userdata("user_id");
            $con['conditions'] = array("bs_user"=>$userid);
            $con['returnType'] = "count";            
            $rows = $this->User->getBusinessRows($con);
            $array = array("user_company_name"=>$cm_name,
                           "user_company_number"=>$cm_number,
                           "user_phone"=>$cm_number,                           
                           "user_address"=>$cm_address,
                           "user_company_address"=>$cm_address
                           );
            $latlong = $this->User->get_lat_long($cm_address);
            $array['user_lat'] = $latlong['lat'];
            $array['user_long'] = $latlong['lng'];
            $this->User->update($array, array("user_id"=>$userid));
            $params = array("bs_name"=>$cm_name,
                            "bs_comp_number"=>$cm_number,                            
                            "bs_phone"=>$cm_phnumber,
                            "bs_website"=>$cm_url,                            
                            "bs_address"=>$cm_address,
                            "bs_opening"=>$cm_opening,
                            "bs_twitter"=>$cm_twitter,
                            "bs_desc"=>$cm_desc,
                            "bs_banner_display"=>$this->input->post("cm_banner_desc")
                            );
            // $emailcheck = $this->User->getRows(array("conditions"=>array("user_email"=>$cm_email)));
            $valid = 1;
            if(!empty($cm_email)){
                $emailcheck = $this->User->checkBsEmail($cm_email);
                if($emailcheck){
                    $params['bs_email'] = $cm_email;
                }else{
                    $valid = 0;
                }
            }else{
                $params['bs_email'] = "";
            }
            // if(!empty($newnumbers)){
            //     foreach($newnumbers as $nb){
            //         $this->User->insertPhonenumbers(1, array("phn_user"=>$userid, "phn_number"=>$nb));
            //     }
            // }
            if(count($business_category) > 0){
                $this->User->insertUserCategories(1, $business_category, $userid);
            }
            if(count($servicefilters) > 0){
                $this->User->insertUserCategories(2, $servicefilters, $userid);
            }
            if(count($productfilters) > 0){
                $this->User->insertUserCategories(3, $productfilters, $userid);
            }            
            if(count($otherfilters) > 0){
                $this->User->insertUserCategories(4, $otherfilters, $userid);
            }            
            // if($check){
                if($valid == 1){
                    if($rows > 0 ){                
                        $check = $this->User->updateBusinessDetails($params, array("bs_user"=>$userid));
                    }
                    else{
                        $params['bs_user'] = $userid;
                        $check = $this->User->insertBusinessProfile($params);
                    }
                    $cont = '<div class="alert alert-success" role="alert"><strong></strong> Mise à jour réussie.</div>';
                    echo json_encode(array("status"=>200, "msg"=>$cont));
                }else{
                    $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Ce parrain ne semble pas faire parti de notre réseau. Les parrains doivent avoir un compte otourdemoi</div>';
                    echo json_encode(array("status"=>1, "msg"=>$cont));
                }
            // }
            // else{
            //     $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Information not updated, Please try again.</div>';
            //     echo json_encode(array("status"=>1, "msg"=>$cont));
            // }
            // print_r($rows);
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Veuillez saisir les champs requis.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * Upload business price list
    */
    function uploadBusinessPrice(){        
        $random = $this->generateRandomString(30);
        $ext = pathinfo($_FILES['bs_price_pdf']['name'], PATHINFO_EXTENSION);        
        $file_name = $random.".".$ext;
        $dirpath = './././assets/priceLists/'.$file_name;
        if($ext == "pdf" ){
            $where = array();
            if(move_uploaded_file($_FILES['bs_price_pdf']['tmp_name'], $dirpath)){                
                $userid = $this->session->userdata("user_id");                
                $params = array("pr_pdfpath"=>$file_name,
                                "pr_metaname"=>$_FILES['bs_price_pdf']['name']
                                );
                $user = $this->User->getBusinessPriceRows(array("user_id"=>$userid));                 
                $mode = 1; //if 1 insert and if 2 update will fire            
                if(count($user) > 0){
                    $filepath = $user['pr_pdfpath'];
                    if(!empty($user['pr_pdfpath']))
                        unlink("./././assets/priceLists/$filepath");
                    $mode = 2;
                    $where["pr_user"] = $userid;
                }else{
                    $params["pr_user"] = $userid;
                }
                $check = $this->User->insertBusinessPricePdf($mode, $params, $where);
                if($check){
                    $prstable = $this->User->getBusinessPriceRows(array("user_id"=>$userid));
                    $cont = '<div class="file-uploaded">
                              <div class="file-upl-name">
                                <p>'.$prstable['pr_metaname'].'</p>
                              </div>
                              <div class="file-upload-del"> <a class="delete-pricefile" modal-aria="'.$prstable['pr_id'].'" href="javascript:;"><i class="fa fa-close"></i></a> </div>
                            </div>';
                    echo json_encode(array("status"=>200, "msg"=>$cont));
                }
                else{
                    echo json_encode(array("status"=>1, "msg"=>"Oups! veuillez réessayer."));
                }
            }
            else{
                echo json_encode(array("status"=>1, "msg"=>"Oups! veuillez réessayer."));
            }
        }
        else{
            echo json_encode(array("status"=>1, "msg"=>"Nous acceptons que les PDF."));
        }
    }

    /*
    * Delete business price pdf
    */
    function deleteBusinessPrices(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $id = $this->input->post("id");
            $con['conditions'] = array("pr_id"=>$id, "pr_user"=>$userid);
            $user = $this->User->getBusinessPriceRows($con);            
            if(count($user) > 0){
                $filepath = $user[0]['pr_pdfpath'];
                if(!empty($user[0]['pr_pdfpath']))
                    unlink("./././assets/priceLists/$filepath");
                $this->db->delete("otd_business_pricetable", array("pr_id"=>$id));
                // if($this->db->affected_rows() > 0)
                    echo json_encode(array("status"=>200));
                // else{
                //     $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Not deleted, please try again.</div>';
                //     echo json_encode(array("status"=>1, "msg"=>$cont));
                // }
            }
            else{
                $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Accès refusé.</div>';
                echo json_encode(array("status"=>1, "msg"=>$cont));
            }
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Accès refusé.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * Store Business opening time
    */
    function storeBusinessOpening(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $con['conditions'] = array("op_user"=>$userid);
            $con['returnType]'] = "count";
            $array = array("mon1"=> $this->input->post("mon1"),
                           "mon2"=> $this->input->post("mon2"),
                           "mon3"=> $this->input->post("mon3"),
                           "mon4"=> $this->input->post("mon4"),
                           "tues1"=> $this->input->post("tues1"),
                           "tues2"=> $this->input->post("tues2"),
                           "tues3"=> $this->input->post("tues3"),
                           "tues4"=> $this->input->post("tues4"),
                           "wed1"=> $this->input->post("wed1"),
                           "wed2"=> $this->input->post("wed2"),
                           "wed3"=> $this->input->post("wed3"),
                           "wed4"=> $this->input->post("wed4"),
                           "thur1"=> $this->input->post("thur1"),
                           "thur2"=> $this->input->post("thur2"),
                           "thur3"=> $this->input->post("thur3"),
                           "thur4"=> $this->input->post("thur4"),
                           "fri1"=> $this->input->post("fri1"),
                           "fri2"=> $this->input->post("fri2"),
                           "fri3"=> $this->input->post("fri3"),
                           "fri4"=> $this->input->post("fri4"),
                           "sat1"=> $this->input->post("sat1"),
                           "sat2"=> $this->input->post("sat2"),
                           "sat3"=> $this->input->post("sat3"),
                           "sat4"=> $this->input->post("sat4"),
                           "sun1"=> $this->input->post("sun1"),
                           "sun2"=> $this->input->post("sun2"),
                           "sun3"=> $this->input->post("sun3"),
                           "sun4"=> $this->input->post("sun4"),
                          );
            $this->User->insertBusinessOpeningTime($array);
            $cont = '<div class="alert alert-success" role="alert"><strong></strong> Vos horaires d\'ouverture ont été mise à jour.</div>';                
            echo json_encode(array("status"=>200, "msg"=>$cont));
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert"><strong></strong> Accès refusé.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * store photo gallary
    */
    function uploadGallaryImage(){
        $userid = $this->session->userdata("user_id");
        $imagecounts = $this->User->getGalleryRows(array("conditions"=>array("ph_user"=>$userid), "returnType"=>"count"));
        $imglimit = $this->User->get_userPurchasedOptionActivatedByType(6);        
        if($imglimit->num_rows() > 0){
            $imgltd = "";
            foreach($imglimit->result() as $imgs){ 
                // if($imgs->opt_option_status == 1 && $imgs->opt_option_active_date <= date("Y-m-d")){      
                // if($imgs->opt_option_type == 5 ){
                //     $arr = array(1,2,3,4,5,6,7,8,9);
                //     $today = date("Y-m-d");
                //     if(in_array($imgs->opt_mnth_strt_day, $arr)){
                //       $udate = date("Y-m-0".$imgs->opt_mnth_strt_day);
                //     }else{
                //       $udate = date("Y-m-".$imgs->opt_mnth_strt_day);
                //     }
                //     $nextdate = date("Y-m-d", strtotime($udate."+ ".$imgs->otp_option_duration));
                //     if($udate <= $today && $nextdate >= $today){                                            
                //         $imgltd = $imgltd + $imgs->otp_option_qnty;
                //     }
                // }else 
                if($imgs->opt_option_active_date <= date("Y-m-d") && $imgs->opt_option_end_date >= date("Y-m-d")){
                    $imgltd = $imgltd + $imgs->otp_option_qnty;
                }
            }
            if($imgltd == 0){
                $imgltd = 4;
            }
        }else{
            $imgltd = 4;
        }
        if($imagecounts < $imgltd ){
            $random = $this->generateRandomString(30);
            $ext = pathinfo($_FILES['bs_photos']['name'], PATHINFO_EXTENSION);        
            $file_name = $random.".".$ext;
            $dirpath = './././assets/photogallery/'.$file_name;
            if($ext == "png" || $ext == "jpeg" || $ext == "jpg" || $ext == "gif" ){
				$image_info = getimagesize($_FILES["bs_photos"]["tmp_name"]);
				$image_width = $image_info[0];
				$image_height = $image_info[1];
                 if($image_width > 265  AND $image_height > 265){				
					  if(move_uploaded_file($_FILES['bs_photos']['tmp_name'], $dirpath)){
						$userid = $this->session->userdata("user_id");
						$params = array("ph_user"=>$userid,
										"ph_path"=>$file_name,                                
										);
						$check = $this->User->insertPhotoGallery($params);
						if($check){
							$con['conditions'] = array("ph_id"=>$check);                    
							$photo = $this->User->getGalleryRows($con);
							$path = $photo[0]['ph_path'];
							$cont = '<li><div class="up-img"><a href="'.base_url("assets/photogallery/$path").'" data-lightbox="example-set"><img src="'.base_url("assets/photogallery/$path").'"></a><div class="del-cross"><a href="javascript:;" modal-aria="'.$check.'" class="make-profile"><i class="fa fa-user"></i></a><br><a href="javascript:;" modal-aria="'.$check.'" class="delete-galphoto"><i class="fa fa-trash-o"></i></a></div></div></li>';
							echo json_encode(array("status"=>200, "msg"=>$cont));
						}
						else{
							echo json_encode(array("status"=>1, "msg"=>"Oups! veuillez réessayer."));
						}
					}
					else{
						echo json_encode(array("status"=>1, "msg"=>"Oups! veuillez réessayer."));
					}
				 }else{
					 
					 echo json_encode(array("status"=>1, "msg"=>"La taille limite de vos images est de 265*265px"));
				 }	
            }
            else{
                echo json_encode(array("status"=>1, "msg"=>"Type de fichier accepté png|jpeg|jpg|gif."));
            }
        }
        else{
            echo json_encode(array("status"=>1, "msg"=>"Oups! Vous êtes limité qu'à $imgltd images. L'option \"Pack Photos\" vous permet de télécharger plus d'images."));
        }
    }

    /*
    * Delete gallery photo
    */
    function deleteGallaryPhoto(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $id = $this->input->post("id");
            $con['conditions'] = array("ph_id"=>$id, "ph_user"=>$userid);
            $user = $this->User->getGalleryRows($con);            
            if(count($user) > 0){
                $userprofile = $this->User->getRows(array("user_id"=>$this->session->userdata("user_id")));
                $existingpic = $userprofile['user_profile_pic'];
                $filepath = $user[0]['ph_path'];
                if( $existingpic != "default/default.jpg" && $existingpic == $filepath){
                    if(file_exists("./././assets/profile_pics/$existingpic")){
                        unlink("./././assets/profile_pics/$existingpic");
                    }
                    $check = $this->User->update(array("user_profile_pic"=>"default/default.jpg"), array("user_id"=>$this->session->userdata("user_id")));
                }
                if(!empty($user[0]['ph_path']))
                    unlink("./././assets/photogallery/$filepath");
                $this->db->delete("otd_user_photogallery", array("ph_id"=>$id));
                // if($this->db->affected_rows() > 0)
                    echo json_encode(array("status"=>200));
                // else{
                //     $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Not deleted, please try again.</div>';
                //     echo json_encode(array("status"=>1, "msg"=>$cont));
                // }
            }
            else{
                $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Accès refusé.</div>';
                echo json_encode(array("status"=>1, "msg"=>$cont));
            }
        }
        else{
            $cont = '<div class="alert alert-danger" role="alert"><strong>Sorry!</strong> Accès refusé.</div>';
            echo json_encode(array("status"=>1, "msg"=>$cont));
        }
    }

    /*
    * set as default profile pic from photo gallery
    */
    function setProfilePicture(){
        $user = $this->User->getRows(array("user_id"=>$this->session->userdata("user_id")));
        if(!empty($user)){
            $existingpic = $user['user_profile_pic'];
            if($existingpic != "default/default.jpg"){
                if(file_exists("./././assets/profile_pics/$existingpic")){
                    unlink("./././assets/profile_pics/$existingpic");
                }                
            }
            $picid = $this->input->post("id");
            $photos = $this->User->getGalleryRows(array("ph_id"=>$picid));
            $phpath = $photos['ph_path'];
            $srcPath = base_url("assets/photogallery/$phpath");
            $destPath = './././assets/profile_pics/'.$photos['ph_path'];
            if(copy($srcPath, $destPath))
            {
                $check = $this->User->update(array("user_profile_pic"=>$phpath), array("user_id"=>$user['user_id']));
                if($check){
                    echo json_encode(array("status"=>200, "msg"=>"Profile set successfully."));
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"Profile not set, Please try again."));
                }
            }
            else
            {
                echo json_encode(array("status"=>1, "msg"=>"Profile not set, Please try again."));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé."));
        }        
    }
    
    /*
     * User login
     */
    public function login()

    {
        $user = $this->session->userdata("user_id");
        
        if(!empty($user)){
            redirect(base_url());
        }
        else{
            
            $this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
            $this->form_validation->set_rules("user_email", "email", "trim|required|valid_email", array("required"=>"Please enter %s", "valid_email"=>"Please enter valid %s"));
            $this->form_validation->set_rules("user_password", "password", "required", array("required"=>"Please enter password"));

            if($this->form_validation->run() == FALSE)
            {
                $data = array();
                $data['error_msg']="";
                $data['success_msg'] = "";
                $data['pagecontent'] = $this->User->getTextContent(8); // get page content        
                $this->template->set('title', 'User Login');
                $this->template->load('inner_layout_reg', 'contents' , 'login_page', $data);
            }
            else{
                $data = array();
                $con['conditions'] = array(
                        'user_email'=>$this->input->post('user_email'),
                        'user_password' => md5($this->input->post('user_password')),                    
                    );

                $checkLogin = $this->User->getRows($con);  
                        
                if($checkLogin){
                    if($checkLogin[0]['status'] == 0){
                        $data['error_msg'] = 'Votre compte n\'est pas encore activé, veuillez vérifier vos mails ou contacter nos administrateur.';
                    }
                    else if($checkLogin[0]['status'] == 3){
                        $data['error_msg'] = 'Votre compte est désactivé';
                    }
                    else{
                        $this->session->set_userdata('isUserLoggedIn',TRUE);
                        $this->session->set_userdata('user_id',$checkLogin[0]['user_id']); 
                        $this->session->set_userdata('user_type',$checkLogin[0]['user_type']); 
                        $this->session->set_userdata('timeout',time());
                        $array = array("is_online"=>1);
                        $upd = $this->User->update($array, array("user_id"=>$checkLogin[0]['user_id'])); 
                        
                         $uu_id = $checkLogin[0]['user_id'];						
                         $ci_session_check = $this->db->get_where("ci_sessions",array("user_id"=>$uu_id))->row();
						 $ci_session['timestamp'] = time();
						 $ci_session['status'] = 0;
						 
						 if(empty($ci_session_check)){
							 $ci_session['user_id'] = $uu_id;
							 $this->db->insert("ci_sessions",$ci_session);
						 }else{ 
						 
						      $this->db->where("user_id",$uu_id);
						      $this->db->update("ci_sessions",$ci_session);	
                         }						 


                        redirect(base_url().'Users/account');
                    }
                }else{
                    $data['error_msg'] = 'Oops, veuillez vérifier votre email ou mot de passe.';
                }
            
                $data['pagecontent'] = $this->User->getTextContent(8); // get page content
                header("Access-Control-Allow-Origin: *");
                $this->template->set('title', 'User Login');
                $this->template->load('inner_layout_reg', 'contents' , 'login_page', $data);
            }

                       

        }
    }
    
    /*
     * User registration
     */
    public function registration(){
       $user = $this->session->userdata("user_id");
        if(!empty($user)){
            redirect(base_url());
        }
        else{
            $data = array();
            $data['error_msg']=""; 
            $userData = array();

            $data['error_msg_cap'] = "";
           
            if($this->input->post('regisSubmit')){
                $this->form_validation->set_rules('user_firstname', 'First Name', 'required');
                $this->form_validation->set_rules('user_lastname', 'Last Name', 'required');
                $this->form_validation->set_rules('user_address', 'User Address', 'required');
                $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[users.user_email]',
                array(
                    'required'      => 'You have not provided %s.',
                    'is_unique'     => 'This %s already exists.',
                    'valid_email'     => 'This %s already exists.'
                ));

              
                $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',
                array(
                    'required'      => 'You have not provided %s.',
                    'numeric'     => 'This %s not numeric value.',
                    'is_natural'     => 'Only natural numbers can be accepted.'
                    ));

               
                $this->form_validation->set_rules('user_password', 'password', 'required|min_length[6]',
                array(
                    'required'      => 'You have not provided %s.',
                    'min_length'     => 'This %s can not be less than 6 digit.'
                    ));

              
                $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[user_password]');
                $this->form_validation->set_rules('g-recaptcha-response', 'Captcha', 'required');

               
                $act_link = $this->generateRandomString();

                $userData = array(
                    'user_firstname' => strip_tags($this->input->post('user_firstname')),
                    'user_lastname' => strip_tags($this->input->post('user_lastname')),
                    'user_email' => strip_tags($this->input->post('user_email')),
                    'user_phone' => strip_tags($this->input->post('user_phone')),
                    'user_address' => strip_tags($this->input->post('user_address')),
                    'user_company_address' => strip_tags($this->input->post('user_address')),
                    'user_password' => md5($this->input->post('user_password')),
                    'user_gender' => $this->input->post('user_gender'),
                    'user_type' => strip_tags($this->input->post('user_type')),
                    'auth_check' => strip_tags($this->input->post('auth_check')),
                    'sms_check' => strip_tags($this->input->post('sms_check')),
                    'act_link' => $act_link,
                    'created'=>date("Y-m-d H:i:s")
                );

                $userDatasubs = array(                   
                    'auth_check' => strip_tags($this->input->post('auth_check')),
                    'sms_check' => strip_tags($this->input->post('sms_check')),
                );

                $activelink = "<a href='".base_url()."/Users/activate/".$act_link."'> Activate Account</a>";
              
                $email = $userData['user_email'];
                //Send mail to user and admin
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>12));
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%Address%%", "%%Phone%%", "%%Gender%%", "%%ActivationLink%%");
                $real = array($userData['user_firstname'], $userData['user_lastname'], $userData["user_email"], $userData['user_address'], $userData['user_phone'], $userData['user_gender'], $activelink);
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];

                $emailadmin = ADMIN_EMAIL;
                $subject = $templatecenter['tmplate_subject'];
                $subjectadmin = $templatecenter['tmplate_subject'];
                if($this->form_validation->run() == true)
                {                
                    
                                        $latlong = $this->User->get_lat_long($userData['user_company_address']);
                                        $userData['user_lat'] = $latlong['lat'];
                                        $userData['user_long'] = $latlong['lng'];
                                        $insert = $this->User->insert($userData);
                                        // redirect(base_url()."Users/thanks");
                                           if($insert >= 0){
                                                $this->session->set_userdata('success_msg', 'Your registration was successfully. Please login to your account.');
                                                $this->session->set_userdata('reg_success', 'yes');
                                               
                                                $x = $this->send_mail($email, $subject, $msg);
                                                $x = $this->send_mail($emailadmin, $subjectadmin, $msg);  
                                               // redirect(base_url()."Users/thanks");
                           
                                                redirect(base_url());
                                                }else{                                                     
                                                    $data['error_msg'] = 'Some problems occured, please try again.';
                                                }

                               
                           
                }
                
            }


            $data['user'] = $userData;
            $data['pagecontent'] = $this->User->getTextContent(7); // get page content
            $data['newsletter1'] = $this->User->site_contents(7);
            $data['newsletter2'] = $this->User->site_contents(8);
            header("Access-Control-Allow-Origin: *");
                        
            $this->template->set('title', 'Personal Account Creation');
            $this->template->load('inner_layout_reg', 'contents' , 'login_signup', $data);
        }
    }


    public function generateRandomString($length = 12) {
        // $length = 12;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

 public function generateRandomPass() {
        $length = 8;
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&()}{][';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
    
   /* public function thanks()
    {
         header("Access-Control-Allow-Origin: *");
                     $data['success'] = "Your registration was successfully Completed.";
                     $this->template->set('title', 'Registration Completed');
                     $this->template->load('inner_layout', 'contents' , 'thanks_page', $data);
    }
*/
    /*
     * User registration
     */
    public function pro_registration(){
        $user = $this->session->userdata("user_id");
        if(!empty($user)){
            redirect(base_url());
        }
        else{
            $data = array();
            $data['error_msg']=""; 
            $userData = array();
            //|is_unique[users.user_company_number] 
            //'is_unique'     => 'This %s already exists.'

            if($this->input->post('regisSubmit')){
                $this->form_validation->set_rules('user_company_name', 'Company Name', 'required');
                $this->form_validation->set_rules('user_company_address', 'Company Address', 'required');
                $this->form_validation->set_rules('user_company_number', 'Company Number', 'required',
                array(
                        'required'      => 'You have not provided %s.'
                        
                ));
                $this->form_validation->set_rules('user_firstname', 'First Name', 'required');
                $this->form_validation->set_rules('user_lastname', 'Last Name', 'required');
                $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[users.user_email]',
                array(
                        'required'      => 'You have not provided %s.',
                        'is_unique'     => 'Cette adresse email est à déjà un compte.',
                        'valid_email'     => 'Cette adresse email est à déjà un compte.'
                ));
                $this->form_validation->set_rules('user_phone', 'Phone', 'required|numeric|is_natural',
                array(
                        'required'      => 'You have not provided %s.',
                        'numeric'     => 'This %s not numeric value.',
                        'is_natural'     => 'Only natural numbers can be accepted.'
                ));

                $this->form_validation->set_rules('user_password', 'password', 'required|min_length[6]',
                array(
                        'required'      => 'You have not provided %s.',
                        'min_length'     => 'This %s can not be less than 6 digit.'
                ));

                $this->form_validation->set_rules('g-recaptcha-response', 'Captcha','required', array("required"=>'Veuillez accepter le Captcha'));
                $this->form_validation->set_rules('conf_password', 'confirm password', 'required|matches[user_password]');
                $this->form_validation->set_rules("user_categories", "categories", "required", array("required"=>"Please select %s"));
                $this->form_validation->set_error_delimiters('', '');


                if(!null == $this->input->post('lang'))
                {
                    $langs = implode(",", $this->input->post('lang'));
                }
                else
                {
                    $langs="1";
                }

                $userData = array(
                    'user_firstname' => strip_tags($this->input->post('user_firstname')),
                    'user_lastname' => strip_tags($this->input->post('user_lastname')),
                    'user_email' => strip_tags($this->input->post('user_email')),
                    'user_phone' => strip_tags($this->input->post('user_phone')),
                    'user_address' => strip_tags($this->input->post('user_company_address')),
                    'user_password' => md5($this->input->post('user_password')),
                    'user_gender' => $this->input->post('user_gender'),
                    'user_type' => strip_tags($this->input->post('user_type')),
                    'auth_check' => strip_tags($this->input->post('auth_check')),
                    'sms_check' => strip_tags($this->input->post('sms_check')),
                    'user_language' => $langs,                    
                    'user_company_name' => strip_tags($this->input->post('user_company_name')),
                    'user_company_address' => strip_tags($this->input->post('user_company_address')),
                    'user_company_number' => strip_tags($this->input->post('user_company_number')),
                    'user_signup_method' => "Regular",
                    'created'=>date("Y-m-d H:i:s")
                );

                $activelink="";
                $email = $userData['user_email'];
                // $activelink = "<a href='".base_url()."/Users/activate/".$act_link."'> Activate Account</a>";
                // $msg = $this->sendMailFormat($subject, $msginner, 11);
                // $strdata="";
                // foreach($userData as $key=>$value)
                // {
                //    $strdata .= "<br>". $key . " : ". $value;
                // }
                // $msginner ="<p>A Professional User Get Registered from website</p><br>Details Are:<br>".$strdata;
                // $msgadmin = $this->sendMailFormat($subjectadmin, $msginner, 11);
                // $fromemail = SENDER_EMAIL;
                // $fromname = SENDER_NAME;
                if($this->form_validation->run() == true){
                    $latlong = $this->User->get_lat_long($userData['user_company_address']);
                    $userData['user_lat'] = $latlong['lat'];
                    $userData['user_long'] = $latlong['lng'];
                    $recaptcha = $this->input->post('g-recaptcha-response');
                    // if (!empty($recaptcha)) {
                     //   $response = $this->recaptcha->verifyResponse($recaptcha);
                       // if (isset($response['success']) and $response['success'] === true) {
                            
                    $insert = $this->User->insert($userData);

                    if($insert > 0){
                        $params = array("bs_name"=>strip_tags($this->input->post('user_company_name')),
                                        "bs_comp_number"=>strip_tags($this->input->post('user_company_number')),
                                        "bs_phone"=>strip_tags($this->input->post('user_phone')),
                                        "bs_address"=>strip_tags($this->input->post('user_company_address')),
                                        "bs_user"=>$insert
                                        );                    
                        $check = $this->User->insertBusinessProfile($params);
                        if(!empty(strip_tags($this->input->post('user_categories'))))
                            $business_category = explode(",", strip_tags($this->input->post('user_categories')));
                        if(count($business_category) > 0){
                            $this->User->insertUserCategories(1, $business_category, $insert);
                        }
                        $this->session->set_userdata('success_msg', 'Your registration was successfully. Please login to your account.');
                        $this->session->set_userdata('pro_reg_success', 'Yes');

                        //send email to user and admin
                        //Fetch Email Templates
                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>11));
                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                        
                        $emailadmin = ADMIN_EMAIL;
                        $subject = $templatecenter['tmplate_subject'];
                        $subjectadmin = "Admin-".$templatecenter['tmplate_subject'];
                        $msginner = "";
                        $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%BusinessName%%", "%%CompanyNumber%%", "%%Address%%", "%%Phone%%", "%%Gender%%");
                        $real = array($userData['user_firstname'], $userData['user_lastname'], $userData["user_email"], $params['bs_name'], $params['bs_comp_number'], $userData['user_company_address'], $userData['user_phone'], $userData['user_gender']);
                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                        $this->send_mail($email, $subject, $msg);
                        $this->send_mail($emailadmin, $subjectadmin, $msg);
						// $this->session->set_flashdata('res_suc', '1');
                        // redirect(base_url('Users/pro_registration'));
                        redirect(base_url());
                    }else{
                        $data['error_msg'] = 'Some problems occured, please try again.';
                       
                    }
                            
                        //}
                    //}
                    //else
                    //{
                     //$data['error_msg_cap'] = 'Captcha is Required.';
                     //die("sdfdsf");
                    //}
                }
                else
                {
                    //get claim details
                    $cmp_number = strip_tags($this->input->post('user_company_number'));
                    $data['claimdata'] = $this->User->getClaimData($cmp_number);

                }
            }
           /* else
            {
                
            }*/
                
            $data['user'] = $userData;
            $data['business_categories'] = $this->User->getBusinessCategories(1);
            $data['pagecontent'] = $this->User->getTextContent(6); // get page content        
            $data['newsletter1'] = $this->User->site_contents(9);
            $data['newsletter2'] = $this->User->site_contents(10);
            $data['languages'] = $this->User->getLanguageRows(array("conditions"=>array("lang_status"=>1)));

            header("Access-Control-Allow-Origin: *");
            
            $this->template->set('title', 'Professional Account Creation');
            $this->template->load('inner_layout_reg', 'contents' , 'pro_signup', $data);
        }
     }


    /*
     * User logout
     */
    public function logout(){
        $array = array("is_online"=>0);
        $uid = $this->session->userdata('user_id');
        $upd = $this->User->update($array, array("user_id"=>$uid));
        // $this->load->library("cart");
        // $this->cart->destroy();
		$ci_session['status'] = 1;
		$this->db->where("user_id",$uid);
		$this->db->update("ci_sessions",$ci_session);	
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('social_login');
        $this->session->sess_destroy();
        redirect(base_url());
    }
    
    /*
     * Existing email check during validation
     */
    public function email_check($str){
         $con['returnType'] = 'count';
        $con['conditions'] = array('user_email'=>$str);
        $checkEmail = $this->User->getRows($con);
        if($checkEmail > 0){
            $this->form_validation->set_message('email_check', 'The given email already exists.');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    function test($args){
        // $x = $this->send_mail("votive.toshik@gmail.com", "Test testing with site");
        // print_r($x);
        $this->load->library("Slimpay_lib");        
        if($args ==1){
            $token = $this->slimpay_lib->getAccessToken();
            echo "<pre>";
            print_r($token);            
        }else if($args ==2){
            $this->slimpay_lib->createOrder();
        }else if($args == 3){
            $this->slimpay_lib->signSlimpayMandate();
        }else if($args == 4){
            $this->slimpay_lib->updateBankAccount();
        }        
    }

    function send_mail($email, $subject="Test Mail", $msg="Testing mail", $cc=NULL, $bcc=NULL){
        $from = SENDER_EMAIL;
        $this->load->library('email');
        $config['protocol']    = 'ssl';
        $config['smtp_host']    = 'ssl://mail.gandi.net';
        $config['smtp_port']    = 587;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $from;
        $config['smtp_pass']    = 'puR1-7,XifsArlyV!5';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; 
        $config['validation'] = TRUE;      
        $this->email->initialize($config);
        $fromname = SENDER_NAME;
        $fromemail =SENDER_EMAIL;
        $to = $email;        
        $subject=$subject;
        $message=$msg;
        $cc=false;
        $this->email->from($fromemail, $fromname);
        $this->email->to($to);
        $this->email->subject($subject);       
        $this->email->message($msg);  
        if($cc){
            $this->email->cc($cc);
        }

        if(!$this->email->send()){
            return $this->email->print_debugger();
        }else{
            return true;
        }
    }
    // function send_mail($email, $subject="Test Mail", $msg="Testing mail", $cc=NULL, $bcc=NULL){
    //     $from = SENDER_EMAIL;
    //     $this->load->library('email');
    //     $config['protocol']    = 'smtp';
    //     $config['smtp_host']    = 'ssl://smtp.gmail.com';
    //     $config['smtp_port']    = '465';
    //     $config['smtp_timeout'] = '7';
    //     $config['smtp_user']    = $from;
    //     $config['smtp_pass']    = 'votive123456';
    //     $config['charset']    = 'utf-8';
    //     $config['newline']    = "\r\n";
    //     $config['mailtype'] = 'html'; 
    //     $config['validation'] = TRUE;      
    //     $this->email->initialize($config);
    //     $fromname = SENDER_NAME;
    //     $fromemail =SENDER_EMAIL;
    //     $to = $email;        
    //     $subject=$subject;
    //     $message=$msg;
    //     $cc=false;
    //     $this->email->from($fromemail, $fromname);
    //     $this->email->to($to);
    //     $this->email->subject($subject);       
    //     $this->email->message($msg);  
    //     if($cc){
    //         $this->email->cc($cc);
    //     }

    //     /*      if(!empty($bcc)){
    //     $this->email->bcc($bcc);
    //     }
    //     */

    //     if(!$this->email->send()){
    //         return $this->email->print_debugger();
    //     }else{
    //         return true;
    //     }
    // }

     /*
     * User registration
     */
    public function subscription(){
        $data = array();
        $data['error_msg']=""; 
        $userData = array();

        if(!empty($this->input->post())){            
            $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[subscribers.user_email]',array('required'   => 'Veuillez saisir votre adresse mail.', 'is_unique'     => 'Vous avez déjà souscrit à notre abonnement', 'valid_email' => 'Veuillez saisir votre adresse mail.' ));
            if($this->form_validation->run() == FALSE){
                echo json_encode(array("status"=>1, "msg"=>form_error("user_email",'<label class="error">','</label>')));
            }else{
                $userData = array(
                                    'user_email' => strip_tags($this->input->post('user_email')),
                                    'user_postcode' => strip_tags($this->input->post('user_postcode')),
                                    'auth_check' => 1,
                                    'sms_check' => 1,
                                );

                $email = $userData['user_email'];

                //Fetch Email Templates
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>22));
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer

                $emailadmin= ADMIN_EMAIL;
                $subject = $templatecenter['tmplate_subject'];
                $dummy = array("%%Email%%", "%%Postalcode%%");
                $real = array($email, $userData['user_postcode']);
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                //$this->send_mail($email, $subject, $msg);
                //$this->send_mail($emailadmin, $subject, $msg);


                
                // $msginner = "Subscriber Details are:";                
                // foreach($userData as $key=>$value)
                // {
                //    $msginner .= "<br>". $key . " : ". $value;
                // }
                // $msg = $this->sendMailFormat($subject, $msginner, 22);
                // $x= $this->send_mail($email, $subject, $msg);
                // $subject = "Subscription for Newsletters from website";
                // $msgadmin = $this->sendMailFormat($subject, $msginner, 22);
                // $x= $this->send_mail($email, $subject, $msg);
                // $fromemail = SENDER_EMAIL;
                // $fromname = SENDER_NAME;
                $insert = $this->User->subscribe_insert($userData);
                if($insert){
                    echo json_encode(array("status"=>200));
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='error'>Something went wrong, Please try after sometime.</label>"));
                }
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='error'>Veuillez saisir les champs requis.</label>"));
        }


        // if($this->input->post('subscribeSubmit')){              
           
        //     //  $this->form_validation->set_rules('user_postcode', 'User Postcode', 'required');
            
        //     $this->form_validation->set_rules('user_email', 'Email', 'required|valid_email|is_unique[subscribers.user_email]',array('required'   => 'You have not provided %s.', 'is_unique'     => 'This %s already exists.', 'valid_email' => 'Please enter valid email address.' ));

        //     $userData = array(
        //         'user_email' => strip_tags($this->input->post('user_email')),
        //         'user_postcode' => strip_tags($this->input->post('user_postcode')),
        //        'auth_check' => 1,
        //         'sms_check' => 1,
        //     );




        //     $email = $userData['user_email'];
        //     $emailadmin= ADMIN_EMAIL;
        //     $subject = "Subscription for Newsletters";
        //     $subjectadmin = "Subscription for Newsletters from website";
        //     $msginner = "Thank you, for subscribing for the newsletter.<br><br><br>Thanks";

        //     /*$msg = '
        //                 <html>
        //                 <head>
        //                 <title>Subscription for Newsletters</title>
        //                 </head>
        //                 <body>
        //                 <h3>'.$subject.'</h3>
        //                 <p>'.$msginner.'</p><br>
        //                 <p>With Regards</p>
        //                 <p>Sales Team</p>
        //                 </body>
        //                 </html>
        //             ';*/

        //              $msg = $this->sendMailFormat($subject, $msginner, 22);

        //     $strdata="";
        //     foreach($userData as $key=>$value)
        //     {
        //        $strdata .= "<br>". $key . " : ". $value;
        //     }

        //     $subject = "A User Subscribed";
        //     /*$msgadmin = '
        //                 <html>
        //                 <head>
        //                 <title>A User Subscribed</title>
        //                 </head>
        //                 <body>
                       
        //                 <p>An user has subscribed for newsletters.</p><br>Details Are:<br>
        //                 '.
        //                 $strdata

        //                 .'
        //                 <p>With Regards</p>
        //                 <p>Sales Team</p>
        //                 </body>
        //                 </html>
        //                 ';*/



        //     $msginner = "<p>An user has subscribed for newsletters.</p><br>Details Are:<br>". $strdata;
        //     $msgadmin = $this->sendMailFormat($subject, $msginner, 22);
        //     $fromemail = SENDER_EMAIL;
        //     $fromname = SENDER_NAME;
        //     if($this->form_validation->run() == true){

        //         $insert = $this->User->subscribe_insert($userData);

        //         if($insert>=1){
        //             $this->session->set_userdata('success_msg', 'Your Subscription was successfully.');

        //           redirect(base_url().'Users/thanks');
                                         
        //         }else{
        //             $data['error_msg'] = 'Some problems occured, please try again.';
                   
        //         }
        //     }
            
        // }        
        // $data['user'] = $userData;
        // $data['pagecontent'] = $this->User->getTextContent(14); // get page content
        // header("Access-Control-Allow-Origin: *");
        // $this->template->set('title', 'Newsletter Subscription');
        // $this->template->load('inner_layout_reg', 'contents' , 'newsletter_sbuscription', $data);
    }
    
    public function thanks()
    {
        header("Access-Control-Allow-Origin: *");
        $data['success'] = "Thank you for your registration. You should have recieved an account activation email.";
        $data["pro"] = "no";
        $this->template->set('title', 'Registration Completed');
        $this->template->load('inner_layout', 'contents' , 'thanks_page', $data);
    }

    public function thankspro()
    {
        header("Access-Control-Allow-Origin: *");
        $data['success'] = "Thank you for your registration, we will reach out to you to validate your inscription.";
        $data["pro"] = "yes";
        $this->template->set('title', 'Registration Completed');
        $this->template->load('inner_layout', 'contents' , 'thanks_page', $data);
    }

	
public function mailme($to,$from,$fromname,$subject,$message,$cc=false)
        {  
        
$this->load->library('email');
$config['protocol']    = 'smtp';
$config['smtp_host']    = 'ssl://smtp.gmail.com';
$config['smtp_port']    = '465';
$config['smtp_timeout'] = '7';
$config['smtp_user']    = $from;
$config['smtp_pass']    = 'votive123456';
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype'] = 'html'; // or html
$config['validation'] = TRUE; // bool whether to validate email or not      
$this->email->initialize($config);
//$this->email->set_mailtype("html");
            $this->email->from($from,$fromname);
            $this->email->to($to);
            if($cc){
            $this->email->cc($cc);
            }
            
            if(!empty($bcc)){
                $this->email->bcc($bcc);
            }
            
            $this->email->subject($subject);
            $this->email->message($message);
            
            if(!$this->email->send()){
              return $this->email->print_debugger();
            }else{
              return true;
            }
        }

    public function activate($str=""){
    
        header("Access-Control-Allow-Origin: *");
        $str = $this->uri->segment(3);
         //$con['conditions'] = array('act_key'=>$str);
        $active_key = $this->User->activateUser($str);
        if($active_key > 0){
              
           $data['act'] = 'yes';
                     $data['success'] = "Thanks for activating your account. Now you can login to your Dashboard";
                     $this->template->set('title', 'Activation Completed');

        }
        else  if($active_key == -101){
              
            $data['act'] = 'all';
                     $data['success'] = "You'r account is already Activated";
                     $this->template->set('title', 'Already Activated Account');

        } else {
             $data['act'] = 'no';
                     $data['success'] = "Activation not completed. Please Contact Support for Help.";
                    
                     $this->template->set('title', 'Activation Not Completed');
            
        }



                    // $this->template->set('title', 'Activation Completed');
                     $this->template->load('inner_layout', 'contents' , 'activation_page', $data);

    }


    /*
    * Auto suggest business categories
    */

    function categorySuggestion(){
        $query = $this->User->getBusinessCategories();
        $array = array();
        foreach($query->result() as $cat){
            $array = array("value"=>$cat->cat_id, "name"=>$cat->cat_name);
        }

        echo json_encode($array);
    }

     /*
    * Login with facebook
    */

    function fblogin(){
        $con = array(
            'user_email'=>$this->input->post('email'),
            // 'social_id' =>$this->input->post('id'),
            // 'status' => '1'
        );
       
        $checkLogin = $this->User->socialLogin($con);   

              
        if($checkLogin->num_rows() > 0){
            foreach($checkLogin->result() as $user){ 
                $userid = $user->user_id;
                $status = $user->status;
                $user_type = $user->user_type;
            }
        
            if($status == 1 && $user_type == "Personal"){
                $this->session->set_userdata('isUserLoggedIn',TRUE);
                $this->session->set_userdata('user_id', $userid);
                $this->session->set_userdata("social_login", array("is_social"=>true, "social_type"=>"facebook"));
    			$this->session->set_userdata('timeout',time());
    			$array = array("is_online"=>1);
    			$upd = $this->User->update($array, array($userid)); 
    			$uu_id = $userid;						
    			$ci_session_check = $this->db->get_where("ci_sessions",array("user_id"=>$uu_id))->row();
    			$ci_session['timestamp'] = time();
    			$ci_session['status'] = 0;    			
    			if(empty($ci_session_check)){
    			    $ci_session['user_id'] = $uu_id;
    			    $this->db->insert("ci_sessions",$ci_session);
    			}else{ 			
    			    $this->db->where("user_id",$uu_id);
    			    $this->db->update("ci_sessions",$ci_session);	
                }
          
                echo json_encode(array("flag"=>200));
            }else{
                echo json_encode(array("flag"=>1, "msg"=> "This Email already exists."));
            }
        }else{
            $data = array("social_id"=>$this->input->post("id"),
                          "user_firstname"=>$this->input->post("first_name"),
                          "user_lastname"=>$this->input->post("last_name"),
                          "user_email"=>$this->input->post("email"),
                          "user_gender"=>$this->input->post("gender"),
                          "user_signup_method"=>"Facebook",
                          'created'=>date("Y-m-d H:i:s"),
                          "status"=>1
                        );
            $this->db->insert("users", $data);
            $user_id = $this->db->insert_id();
            $this->createPasswordLink($user_id);
            $this->session->set_userdata('isUserLoggedIn',TRUE);
            $this->session->set_userdata('user_id',$user_id);
            $this->session->set_userdata("social_login", array("is_social"=>true, "social_type"=>"facebook"));
            $array = array("is_online"=>1);
            $upd = $this->User->update($array, array("user_id"=>$user_id));
			
			$this->session->set_userdata('timeout',time());
  
			$uu_id = $user_id;						
			$ci_session_check = $this->db->get_where("ci_sessions",array("user_id"=>$uu_id))->row();
			$ci_session['timestamp'] = time();
			$ci_session['status'] = 0;
			 
			if(empty($ci_session_check)){
			    $ci_session['user_id'] = $uu_id;
			    $this->db->insert("ci_sessions",$ci_session);
			}else{ 
			 
			    $this->db->where("user_id",$uu_id);
				$this->db->update("ci_sessions",$ci_session);	
            }			
           
            echo json_encode(array("flag"=>200, "name"=>"pradeep"));
        }
    }

    /*
    * Login with Google+
    */

    function googleLogin(){
        $login = $this->session->userdata("user_id");
        if(!empty($login)){
            echo json_encode(array("flag"=>2));
        }
        else{
            $con = array(
                'user_email'=>$this->input->post('email'),
                // 'social_id' =>$this->input->post('id'),
                // 'status' => '1'
            );
           
            $checkLogin = $this->User->socialLogin($con);        
            if($checkLogin->num_rows() > 0){
                foreach($checkLogin->result() as $user){ 
                    $userid = $user->user_id;
                    $status = $user->status;
                    $user_type = $user->user_type;
                }
                if($status == 1 && $user_type == "Personal"){
                    $this->session->set_userdata('isUserLoggedIn',TRUE);
                    $this->session->set_userdata('user_id', $userid);
                    $this->session->set_userdata("social_login", array("is_social"=>true, "social_type"=>"google"));           
                    echo json_encode(array("flag"=>200));
    				
    				$this->session->set_userdata('timeout',time());
    				$array = array("is_online"=>1);
    				$upd = $this->User->update($array, array($userid)); 
                    $uu_id = $userid;						
                    $ci_session_check = $this->db->get_where("ci_sessions",array("user_id"=>$uu_id))->row();
                    $ci_session['timestamp'] = time();
                    $ci_session['status'] = 0;

                    if(empty($ci_session_check)){
                        $ci_session['user_id'] = $uu_id;
                        $this->db->insert("ci_sessions",$ci_session);
                    }else{
                        $this->db->where("user_id",$uu_id);
                        $this->db->update("ci_sessions",$ci_session);	
                    }
                    echo json_encode(array("flag"=>200));
                }else{
                    echo json_encode(array("flag"=>1, "msg"=> "This Email already exists."));
                }
            }
            else{
                $data = array("social_id"=>$this->input->post("id"),
                              "user_firstname"=>$this->input->post("first_name"),
                              "user_lastname"=>$this->input->post("last_name"),
                              "user_email"=>$this->input->post("email"),
                              "user_signup_method"=>"Google",
                              'created'=>date("Y-m-d H:i:s"),
                              "status"=>1
                            );

                $this->db->insert("users", $data);
                $user_id = $this->db->insert_id();
                $this->createPasswordLink($user_id);
                $this->session->set_userdata('isUserLoggedIn',TRUE);
                $this->session->set_userdata('user_id',$user_id);
                $this->session->set_userdata("social_login", array("is_social"=>true, "social_type"=>"google"));
                $array = array("is_online"=>1);
                $upd = $this->User->update($array, array("user_id"=>$user_id));
				
				$this->session->set_userdata('timeout',time());
 
                $uu_id = $user_id;						
                $ci_session_check = $this->db->get_where("ci_sessions",array("user_id"=>$uu_id))->row();
                $ci_session['timestamp'] = time();
                $ci_session['status'] = 0;				 
                if(empty($ci_session_check)){
                    $ci_session['user_id'] = $uu_id;
                    $this->db->insert("ci_sessions",$ci_session);
                }else{
                    $this->db->where("user_id",$uu_id);
                    $this->db->update("ci_sessions",$ci_session);	
                }
                echo json_encode(array("flag"=>200));
            }
        }
    }

    /*
    * Send password to login with social media users
    */
    Function createPasswordLink($userid){
        $con['conditions'] = array(
                    'user_id'=> $userid,
                    'status' => '1'
                );
        $con['returnType'] = "single";
        $checkUser = $this->User->getRows($con);

        if($checkUser){
            $email = $checkUser['user_email'];
            $temppass = $this->generateRandomPass();
            $temppass_md5 = md5($temppass);
            $upd = $this->User->update_password($email, $temppass_md5);
            if($upd >=1){
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>14)); 
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); //Email Footer                
                $emailadmin = ADMIN_EMAIL;
                $subject = $templatecenter['tmplate_subject'];
                $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%Gender%%", "%%TempPassword%%");
                $real = array($checkUser['user_firstname'],
                              $checkUser['user_lastname'], 
                              $checkUser["user_email"], 
                              $checkUser['user_gender'],
                              $temppass
                            );
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                //Send mail to userr
                    $this->send_mail($email, $subject, $msg);
                    $this->send_mail(ADMIN_EMAIL, $subject, $msg);
            }
        }
    }
    
    /*
     * User Social logout
     */
    public function socialLogout(){
		$uu_id = $this->session->userdata('user_id');
        $this->session->unset_userdata('isUserLoggedIn');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('social_login');
        $this->session->sess_destroy();
		$ci_session['status'] = 1;
		$this->db->where("user_id",$uu_id);
		$this->db->update("ci_sessions",$ci_session);			
		
		
        echo json_encode(array("status"=>200));
    }

    /*
    * Get Terms & conditions
    */

    function termsAndConditions($args){
        if(!empty($args)){
            $pdfname = "";
            if($args == "personal"){
                $check = $this->db->get_where("site_contents", array("content_id"=>5));
                foreach($check->result() as $pdf){
                    $pdfname = $pdf->uploaded_file_path;
                }
            }
            else if($args == "professional"){
                $check = $this->db->get_where("site_contents", array("content_id"=>2));
                foreach($check->result() as $pdf){
                    $pdfname = $pdf->uploaded_file_path;
                }
            }else if($args == "slimpayterms"){
                $check = $this->db->get_where("site_contents", array("content_id"=>28));
                foreach($check->result() as $pdf){
                    $pdfname = $pdf->uploaded_file_path;
                }
            }            
            echo json_encode(array("pdf"=>$pdfname));
        }
    }

    /*
    * Send message to admin by user
    */
    function sendMessage(){
        $title = $this->input->post("title");
        $message = $this->input->post("message");
        if(!empty($title) && !empty($message)){
            $userid = $this->session->userdata("user_id");
            $param = array("bx_title"=>$title,
                           "bx_content"=>$message,
                           "bx_by_user"=>$userid,
                           "bx_status"=>1
                        );
            $query = $this->User->insertBoxIdea($param);
            if($query){
                $user = $this->User->getRows(array('user_id'=>$userid));
                $username = $user['user_firstname'].' '.$user['user_lastname'];
                 
                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>37));
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
                $dummy = array("%%username%%","%%title%%","%%msg%%");
                $real = array($username,$title,$message);
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                $subject = $templatecenter['tmplate_subject'];
                $emailadmin = ADMIN_EMAIL;
                //serviceclient@otourdemoi.fr
                $subject = $templatecenter['tmplate_subject'];
                $subjectadmin = $templatecenter['tmplate_subject'];
                $x = $this->send_mail($emailadmin, $subject, $msg);
                echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Votre message à bien été envoyé.</label>"));

            }
        }
        else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
        }
    }

    /*
    * Get Single Messages
    */
    function GetMessages($args){
        $id = $this->session->userdata("user_id");
        $messages = $this->User->singleUserMessages($id, $args);
        $sender = $this->User->getRows(array("user_id"=>$id));
        $username = "";
        $users = $this->User->getRows(array("user_id"=>$args));
        $message = "";
        foreach($messages->result() as $msg){
            if($msg->msg_reciever != $id){
                $message .= '<div class="msg-admin"><div class="row"><div class="col-md-12"><div class="msg-user-sec"><div class="msg-user-pro-pic"><img src="'.base_url("assets/profile_pics/").$sender['user_profile_pic'].'"/></div><div class="msg-user-main-msg"><p>'.$msg->msg_content.'</p><p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>'.date("m-d-Y", strtotime($msg->msg_date)).'</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>'.date("H:i A", strtotime($msg->msg_date)).'</span></p></div></div></div></div></div>';
            }
            else{
                $message .= '<div class="msg-user"><div class="row"><div class="col-md-12"><div class="msg-user-sec"><div class="msg-user-pro-pic"><img src="'.base_url("assets/profile_pics/").$users['user_profile_pic'].'"/></div><div class="msg-user-main-msg"><p>'.$msg->msg_content.'</p><p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>'.date("m-d-Y", strtotime($msg->msg_date)).'</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>'.date("H:i A", strtotime($msg->msg_date)).'</span></p></div></div></div></div></div>';
            }
        }
        if($users['user_type'] == "Professional"){
          if(!empty($users['bs_name']))
            if(!empty($users['bs_name']))                                              
              $username = ucwords($users['bs_name']);
            else
              $username = ucwords($users['user_firstname']." ".$users['user_lastname']);
          else
            $username = ucwords($users['user_firstname']." ".$users['user_lastname']);
        }
        else
          $username = ucwords($users['user_firstname']." ".$users['user_lastname']);
        echo json_encode(array("status"=>200, "content"=>$message, "name"=>$username, "profile"=>$users['user_profile_pic']));
    }

    /*
    * Send message to other users
    */
    function sendUserMessage(){
        $user = $this->session->userdata("user_id");
        if(!empty($user)){
            $id = $this->User->insertUserMessages(array("msg_sender"=>$user,
                                                        "msg_reciever"=>$this->input->post("tosend"),
                                                        "msg_content"=>$this->input->post("usermessage")));
            if($id){
                $msg = $this->User->getMessagesRows(array("msg_id"=>$id));
                $message = '<div class="msg-admin"><div class="row"><div class="col-md-12"><div class="msg-user-sec"><div class="msg-user-pro-pic"><img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg"/></div><div class="msg-user-main-msg"><p>'.$msg['msg_content'].'</p><p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>'.date("m-d-Y", strtotime($msg['msg_date'])).'</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>'.date("H:i A", strtotime($msg['msg_date'])).'</span></p></div></div></div></div></div>';
                echo json_encode(array("status"=>200, "content"=>$message));
            }
            else{
                echo json_encode(array("status"=>1));
            }
        }
        else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
        }
    }

    /*
    * add Business Events
    */
    public function addEvents(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $check = $this->User->getMyFreeEvents();
            if(empty($check) && count($check) == 0){
                $this->form_validation->set_rules("ename", "event name", "required");
                $this->form_validation->set_rules("esdate", "event start date", "required");
                $this->form_validation->set_rules("eedate", "event end date", "required");
                $this->form_validation->set_rules("noperson", "number of person", "required");
                $this->form_validation->set_rules("eventprice", "event price", "required");
                $this->form_validation->set_rules("eventtype", "event type", "required");
                $this->form_validation->set_rules("description", "event description", "required");
                if($this->form_validation->run() == TRUE){
                    $arr = array("event_name"=>$this->input->post("ename"),
                                 "event_start_date"=>$this->input->post("esdate"),
                                 "event_end_date"=>$this->input->post("eedate"),
                                 "event_per_ltd"=>$this->input->post("noperson"),
                                 "event_price"=>$this->input->post("eventprice"),
                                 "event_type_id"=>$this->input->post("eventtype"),
                                 "event_description"=>$this->input->post("description"),
                                 "event_status"=>1,
                                 "event_is_free"=>1,
                                 "created_by_user"=>$userid
                                );
                    $check1 = $this->User->insertEvents($arr);
                    $check = $check1['id'];
                    $bcc = $check1['followemails'];
                    $date1 = date_create($this->input->post("esdate"));
                    $date2 = date_create($this->input->post("eedate"));        
                    $difference = date_diff($date1,$date2);
                    $opt_option_active_date = $this->input->post("esdate");
                    $opt_option_end_date = $this->input->post("eedate");
                    $diff = $difference->format('%a');
                    if($diff > 0 ){
                        $opt_option_price = 0;
                        $otp_option_duration = "1 Event x $diff Days";
                    }else{                        
                        $opt_option_price = 0;
                        $otp_option_duration = "1 Event x 1 Day";
                    }
                    $arr1 = array("pyt_txn_status"=>"Completed",
                                  "pyt_amount"=>0,
                                  "pyt_fee"=>0,
                                  "pyt_date"=>date("Y-m-d H:i:s"));
                    $txnid = $this->User->insertTransaction($arr1);
                    $optionbuy = array("opt_user_id" => $userid,
                                       "opt_option_id" => 7,
                                       "opt_option_purchase_date" => date("Y/m/d", time()),
                                       "otp_option_duration" => $otp_option_duration,
                                       "otp_option_qnty"=>1,
                                       "opt_option_price" => $opt_option_price,
                                       "otp_search_city"=>"",
                                       "opt_option_active_date" => $opt_option_active_date,
                                       "opt_option_end_date"=>$opt_option_end_date,
                                       "opt_tran_id"=>$txnid,
                                       "opt_option_status"=>1,
                                       "opt_event_id"=>$check,
                                       "opt_option_inactive_validity"=>"");
                    $this->User->insertUserOption($optionbuy);
                    if($check){
                        // Notify admin by email
                            $eventDetails = $this->User->getEventsRows(array("event_id"=>$check));                            
                            $emailadmin = ADMIN_EMAIL;
                            $subject = "Event Alert";                            
                            $msginner = "<p>Event details are given below</p>
                                        <p><b>Event Name: </b>".$eventDetails['event_name']."</p>
                                        <p><b>Type: </b>".$eventDetails['evt_type']."</p>
                                        <p><b>Start Date: </b>".$eventDetails['event_start_date']."</p>
                                        <p><b>End Date: </b>".$eventDetails['event_end_date']."</p>                                        
                                        <p><b>People Strength: </b>".$eventDetails['event_per_ltd']."</p>
                                        <p><b>Price: </b>$".$eventDetails['event_price']."</p>
                                        <br/>
                                        <p>Please <a href=".base_url("Admin").">click here</a> to view event</p>
                                        <br/>
                                        <p>Thank You</p>";
                            
                            $msgadmin = $this->sendMailFormat($subject, $msginner, 17);
                            $fromemail = SENDER_EMAIL;
                            $fromname = SENDER_NAME;
                            $this->load->library("email");
                            $this->email->from($fromemail, $fromname);
                            $this->email->to($emailadmin);
                            $this->email->subject($subject);
                            $this->email->message($msgadmin);                            
                            $x = $this->email->send();
                            $this->email->clear(TRUE);

                            $msginner = "<p>One event is organized by ".ucwords($eventDetails['user_firstname']." ".$eventDetails['user_lastname']).", Event details are given below</p>
                                        <p><b>Event Name: </b>".$eventDetails['event_name']."</p>
                                        <p><b>Type: </b>".$eventDetails['evt_type']."</p>
                                        <p><b>Start Date: </b>".$eventDetails['event_start_date']."</p>
                                        <p><b>End Date: </b>".$eventDetails['event_end_date']."</p>                                        
                                        <p><b>People Strength: </b>".$eventDetails['event_per_ltd']."</p>
                                        <p><b>Price: </b>$".$eventDetails['event_price']."</p>
                                        <br/>
                                        <p>Please <a href=".base_url("Users/eventsView/").$eventDetails['event_id'].">click here</a> to view event.</p>
                                        <br/>
                                        <p>Thank You</p>";
                            
                            $msg = $this->sendMailFormat($subject, $msginner, 17);
                            $fromemail = SENDER_EMAIL;
                            $fromname = SENDER_NAME;
                            $this->load->library("email");
                            $this->email->from($fromemail, $fromname);
                            // $this->email->to($emailadmin);                            
                            $this->email->subject($subject);
                            $this->email->message($msg);
                            if(!empty($bcc)){
                                $this->email->bcc($bcc);
                            }
                            $x = $this->email->send();
                        // Notify admin by email
                        echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Event added successfully.</label>"));
                    }else{
                        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Sorry, event not created, Pleaes try again.</label>"));
                    }

                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Veuillez saisir les champs requis.</label>"));
                }
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>You Have already created your free event. Please use cart to create event.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Accès refusé.</label>"));
        }
    }

    /*
    * create user event and also user option
    */
    function userEvents(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $check = $this->User->getMyFreeEvents();
            if(empty($check) && count($check) == 0){
                $arr1 = array("pyt_txn_status"=>"Completed",
                              "pyt_amount"=>0,
                              "pyt_fee"=>0,
                              "pyt_date"=>date("Y-m-d H:i:s"));
                $txnid = $this->User->insertTransaction($arr1);
                if($txnid){
                    $arr = array("event_status"=>0,
                                 "event_is_free"=>1,
                                 "created_by_user"=>$userid
                                );
                    $check = $this->User->insertEventwithougNoti($arr);
                    $optionbuy = array("opt_user_id" => $userid,
                                       "opt_option_id" => 7,
                                       "opt_option_purchase_date" => date("Y/m/d", time()),
                                       "otp_option_qnty"=>1,
                                       "opt_tran_id"=>$txnid,
                                       "opt_option_status"=>0,
                                       "opt_event_id"=>$check);
                    $isoption = $this->User->insertUserOption($optionbuy);
                    if($isoption){
                        $prows = $this->User->getSingleOptiondetails($isoption);
                        $content = '<tr class="cs-cnt">
                                  <td>'.$prows['opt_name'].'</td><td>'.$prows['opt_option_purchase_date'].'</td>';
                                  
                        if($prows['opt_option_status'] == 1)
                          $content .= "<td>Yes</td>";
                        else if($prows['opt_option_status'] == 3){
                          $content .= "<td>Expired</td>";
                        }else{
                          $content .= "<td>No</td>";
                        }
                                  
                        $content .= '<td><span class="fevt">Free Event</span></td>                                  
                                  <td>
                                    <div class="optionaction">
                                      <button class="btn btn-success btn-xs tooltip event1" data-toggle="modal" data-target="#createEventmodal" details="'.$prows['opt_user_option_id'].'" list-id="'.$prows['opt_event_id'].'"><i class="fa fa-edit"></i> <span class="tooltiptext">Activate</span></button>                                      
                                    </div>                                                                         
                                  </td>
                                </tr>';
                        echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Votre bon plan gratuit à bien été publié.</label>", "content"=>$content));
                    }
                    else{
                        echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! une erreur est survenue lors de l'enregistrement de votre bon plan, veuillez réessayer.</label>"));
                    }
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! Veuillez réessayer.</label>"));
                }
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Votre bon plan gratuit est activé, veuillez le congirure dans le menu \"Gestion des options\", merci.</label>"));
            }
        }
    }

    /*
    * Activate User Event
    */
    function activateUserEvent(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $this->form_validation->set_rules("ename", "event name", "required");
            $this->form_validation->set_rules("eventtype", "event type", "required");
            $this->form_validation->set_rules("evtaddress", "Event address", "required");
            // $this->form_validation->set_rules("peedate", "Publishing End Date", "required");
            $this->form_validation->set_rules("esdate", "Event Start Date", "required");
            $this->form_validation->set_rules("eedate", "Event End Date", "required");
            $this->form_validation->set_rules("noperson", "Number Of Persons", "required");
            $this->form_validation->set_rules("eventprice", "Price Per Person", "required");
            $this->form_validation->set_rules("description", "Description", "required");
            $this->form_validation->set_rules("usoption", "", "required");
            if($this->form_validation->run() == TRUE){
                $latlong = $this->User->get_lat_long($this->input->post("evtaddress"));                
                $arr = array("event_name"=>$this->input->post("ename"),
                             "event_type_id"=>$this->input->post("eventtype"),
                             "event_start_date"=>$this->input->post("esdate"),
                             "event_end_date"=>$this->input->post("eedate"),
                             "event_per_ltd"=>$this->input->post("noperson"),
                             "event_price"=>$this->input->post("eventprice"),
                             "event_address"=>$this->input->post("evtaddress"),
                             "event_lat"=>$latlong['lat'],
                             "event_long"=>$latlong['lng'],
                             "event_description"=>$this->input->post("description"),
                             "event_status"=>1,
                            );                
                if(!empty($_FILES)){
                    $cnt = 1;
                    foreach($_FILES['evtfile']['name'] as $key=>$val){
                        //upload and stored images
                        $random = $this->generateRandomString(30);
                        $ext = pathinfo($_FILES['evtfile']['name'][$key], PATHINFO_EXTENSION);
                        $file_name = $random.".".$ext;
                        $dirpath = './././assets/img/events/'.$file_name;
                        
                        if(move_uploaded_file($_FILES['evtfile']['tmp_name'][$key],$dirpath)){
                            $arr["event_img".$cnt] = $file_name;
                        }
                        if($cnt == 4)
                            break;
                        $cnt++;
                    }
                }
                $opt_arr = array();
                $option = $this->input->post("usoption");
                $eventid = $this->input->post("uslist");
                $eventdetials = $this->User->getEventsRows(array("event_id"=>$eventid));
                $pagetext = $this->User->getTextContent(15);
                // $ademtext = '<p>One event is created by business user, Event details are given below</p>';
                
                if($eventdetials['event_is_free'] && $eventdetials['event_status'] != 3 && $eventdetials['opt_option_status'] == 0){
                    $arr["event_publish_start"] = $this->input->post("pesdate");
                    $arr["event_publish_end"] = $this->input->post("peedate");
                    $opt_arr['opt_option_active_date'] = $this->input->post("pesdate");
                    $opt_arr['opt_option_end_date'] = $this->input->post("peedate");                    
                    $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>17));
                }else if($eventdetials['otp_option_duration'] != "1 Day" && $eventdetials['opt_option_status'] == 0){
                    $arr["event_publish_start"] = $this->input->post("pesdate");
                    $duration = $eventdetials['otp_option_duration'];
                    $exp_date = date("Y-m-d", strtotime($this->input->post("pesdate")." + $duration"));
                    $arr["event_publish_end"] = $exp_date;
                    $opt_arr['opt_option_active_date'] = $this->input->post("pesdate");
                    $opt_arr['opt_option_end_date'] = $exp_date;
                    // $ademtext = '<p>One event is created by business user, Event details are given below</p>';                    
                }
                $check = $this->User->updateUserEventWithNoti($arr, array("created_by_user"=>$ses, "event_id"=>$eventid));
                if($check['status']){
                    $opt_arr["opt_option_status"] = 1;
                    $ispackage = $this->input->post("urlpackage");
                    if(!empty($ispackage) ){
                        $mnthstday = date("d", strtotime($this->input->post("pesdate")));
                        $opt_arr['opt_mnth_strt_day'] = $mnthstday;
                        $mnthendday = date("d", strtotime($this->input->post("pesdate")." + ".$eventdetials['otp_option_duration']));
                        $opt_arr['opt_mnth_end_day'] = $mnthendday;
                    }
                    $bcc = $check['emails'];
                    $this->User->updateUserOption($opt_arr, array("opt_user_option_id"=>$option));
                    // Notify email code
                        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                        $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>17)); // For User and admin
                        $templatecenter1 = $this->User->getEmailTemplateRows(array("tmplate_id"=>18)); // For Followers
                        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25));
                        $tempeventdetials = $this->User->getEventsRows(array("event_id"=>$eventid));
                        $emailadmin = ADMIN_EMAIL;
                        $subject = $templatecenter['tmplate_subject'];
                        $dummy = array("%%Firstname%%", "%%LastName%%", "%%Email%%", "%%BusinessName%%", "%%Address%%", "%%Phone%%", "%%Gender%%", "%%EventName%%", "%%EventType%%", "%%PublishStartDate%%", "%%PublishEndDate%%", "%%EventStartDate%%", "%%EventEndDate%%", "%%Price%%", "%%Persons%%", "%%Description%%", "%%EventAddress%%");
                        $real = array($eventdetials['user_firstname'],
                                      $eventdetials['user_lastname'], 
                                      $eventdetials["user_email"], 
                                      $eventdetials['bs_name'], 
                                      $eventdetials['user_company_address'], 
                                      $eventdetials['user_phone'], 
                                      $eventdetials['user_gender'],
                                      $tempeventdetials['event_name'],
                                      $tempeventdetials['evt_type'],
                                      date("d M, Y", strtotime($tempeventdetials['event_publish_start'])),
                                      date("d M, Y", strtotime($tempeventdetials['event_publish_end'])),
                                      date("d M, Y H:i", strtotime($tempeventdetials['event_start_date'])),
                                      date("d M, Y H:i", strtotime($tempeventdetials['event_end_date'])),
                                      $tempeventdetials['event_price'],
                                      $tempeventdetials['event_per_ltd'],
                                      substr($tempeventdetials['event_description'], 0, 200),
                                      $tempeventdetials['event_address'],
                                      );
                        $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                        //Send mail to userr
                        $email = $eventdetials["user_email"];
                        $this->send_mail($email, $subject, $msg);
                        $this->send_mail(ADMIN_EMAIL, $subject, $msg);                    
                        //Send mail to followers
                        $msg = str_replace($dummy, $real, $templatecenter1['tmplate_content'] );
                        $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                        
                        if(!empty($bcc)){
                            $this->send_mail("", $subject, $msg, "", $bcc);
                        }
                    // Notify admin by email
                    $content = '<button class="btn btn-xs btn-default tooltip"><i class="fa fa-thumbs-up"></i> <span class="tooltiptext">'.$pagetext['label94'].'</span></button>
                                <a href="'.base_url("Users/myEvents/").$eventid.'" target="_blank" class="btn btn-xs btn-info tooltip"><i class="fa fa-eye"></i> <span class="tooltiptext">'.$pagetext['label89'].'</span></a> ';
                    if($eventdetials['event_is_free'] && $eventdetials['event_status'] != 3)
                        $content .= ' <a href="'.base_url("Users/editEvent").'" target="_blank" class="btn btn-xs btn-success tooltip"><i class="fa fa-pencil"></i> <span class="tooltiptext">'.$pagetext['label88'].'</span></a>';

                    $content .= ' <button class="btn btn-xs btn-danger dtevent tooltip" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" details="'.$option.'"><i class="fa fa-trash-o"></i> <span class="tooltiptext">'.$pagetext['label90'].'</span></button>
                                 <button class="btn btn-xs btn-warning prtrecpt tooltip" atr-desc="'.$option.'"><i class="fa fa-print"></i> <span class="tooltiptext">'.$pagetext['label91'].'</span></button>';
                    echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Votre bon plan à bien été publié.</label>", "content"=>$content));
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! une erreur est survenue lors de l'enregistrement de votre bon plan, veuillez réessayer.</label>"));
                }
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Veuillez saisir les champs requis</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
        }
    }

    /*
    * edit Business Events
    */
    public function editEvent(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $this->form_validation->set_rules("ename", "event name", "required");
            $this->form_validation->set_rules("eventtype", "event type", "required");
            $this->form_validation->set_rules("pesdate", "Publishing Start Date", "required");
            $this->form_validation->set_rules("peedate", "Publishing End Date", "required");
            $this->form_validation->set_rules("esdate", "Start Date", "required");
            $this->form_validation->set_rules("eedate", "End Date", "required");
            $this->form_validation->set_rules("noperson", "Number Of Persons", "required");
            $this->form_validation->set_rules("eventprice", "Price Per Person", "required");
            $this->form_validation->set_rules("evtaddress", "Address", "required");
            $this->form_validation->set_rules("description", "Description", "required");
            if($this->form_validation->run() == TRUE){
                $eventDetails = $this->User->getMyFreeEvents();
                $latlong = $this->User->get_lat_long($this->input->post("evtaddress"));
                $arr = array("event_name"=>$this->input->post("ename"),
                             "event_type_id"=>$this->input->post("eventtype"),
                             "event_publish_start"=>$this->input->post("pesdate"),
                             "event_publish_end"=>$this->input->post("peedate"),
                             "event_start_date"=>$this->input->post("esdate"),
                             "event_end_date"=>$this->input->post("eedate"),
                             "event_per_ltd"=>$this->input->post("noperson"),
                             "event_price"=>$this->input->post("eventprice"),
                             "event_address"=>$this->input->post("evtaddress"),
                             "event_lat"=>$latlong['lat'],
                             "event_long"=>$latlong['lng'],
                             "event_description"=>$this->input->post("description"),
                             "event_status"=>1,                             
                            );
                if(!empty($_FILES)){
                    $cnt = 1;
                    foreach($_FILES['evtfile']['name'] as $key=>$val){
                        //upload and stored images
                        $random = $this->generateRandomString(30);
                        $ext = pathinfo($_FILES['evtfile']['name'][$key], PATHINFO_EXTENSION);
                        $file_name = $random.".".$ext;
                        $dirpath = './././assets/img/events/'.$file_name;
                        
                        if(move_uploaded_file($_FILES['evtfile']['tmp_name'][$key],$dirpath)){
                            $arr["event_img".$cnt] = $file_name;
                        }
                        if($cnt == 4)
                            break;
                        $cnt++;
                    }
                }
                $arr["event_publish_start"] = $this->input->post("pesdate");
                $arr["event_publish_end"] = $this->input->post("peedate");
                $opt_arr['opt_option_active_date'] = $this->input->post("pesdate");
                $opt_arr['opt_option_end_date'] = $this->input->post("peedate");
                $check = $this->User->updateUserEvent($arr, array("event_id"=> $eventDetails[0]['event_id'] ));
                if($check){
                    $this->User->updateUserOption($opt_arr, array("opt_event_id"=>$eventDetails[0]['event_id'] ));
                    echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Votre bon plan à bien été publié.</label>"));
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! une erreur est survenue lors de l'enregistrement de votre bon plan, veuillez réessayer.</label>"));
                }
            }else{
                $data = array();                
                $data['suggestion'] = $this->User->getSuggestions();
                $data['details'] = $this->User->getMyFreeEvents();
                $data['labels'] = $this->User->getTextContent(15);
                // $data['evnt'] = $this->User->getEventsRows(array("event_id"=>$args));
                $data['eventTypeList'] = $this->User->getEventTypeList();
                header("Access-Control-Allow-Origin: *");                     
                $this->template->set('title', 'Edit Event');
                $this->template->load('home_layout', 'contents' , 'editEvent', $data);
            }
        }else{
            redirect(base_url());
        }
    }

    /*
    * remove Events
    */
    function removeEvents(){
        $id = $this->input->post("details");
        $ses = $this->session->userdata("user_id");
        if(!empty($ses) && !empty($id)){
            $eventDetails = $this->User->getEventsRows(array("conditions"=>array("opt_user_option_id"=>$id, "created_by_user"=>$ses), "returnType"=>"single"));
            if(!empty($eventDetails)){
                $img1 = $eventDetails['event_img1'];                
                if(!empty($img1)){
                    if(file_exists("./././assets/img/events/$img1")){
                        unlink("./././assets/img/events/$img1");
                    }
                }
                $img2 = $eventDetails['event_img2'];
                if(!empty($img2)){
                    if(file_exists("./././assets/img/events/$img2")){
                        unlink("./././assets/img/events/$img2");
                    }
                }
                $img3 = $eventDetails['event_img3'];
                if(!empty($img3)){
                    if(file_exists("./././assets/img/events/$img3")){
                        unlink("./././assets/img/events/$img3");
                    }
                }
                $img4 = $eventDetails['event_img4'];
                if(!empty($img4)){
                    if(file_exists("./././assets/img/events/$img4")){
                        unlink("./././assets/img/events/$img4");
                    }
                }
                $check = $this->User->removeEvent($id, $ses);
                if($check){
                    $checkevents = $this->User->getMyFreeEvents();
                    if(empty($checkevents) || count($checkevents) == 0){
                        $type = 1;                        
                    }else{
                        $type = 0;
                    }
                    echo json_encode(array("status"=>200, "check"=>$type));
                }
                else{
                    echo json_encode(array("status"=>1));
                }
            }else{
                echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
        }
    }

    /*
    * Upload Event Images
    */
    function eventImageUpload(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $images_arr = array();
            $cnt = 1;
            foreach($_FILES['evtfile']['name'] as $key=>$val){
                $extra_info = getimagesize($_FILES['evtfile']['tmp_name'][$key]);
                $images_arr[] = "data:" . $extra_info["mime"] . ";base64," . base64_encode(file_get_contents($_FILES['evtfile']['tmp_name'][$key]));
                if($cnt == 4)
                    break;
                $cnt++;
            }
            $imgdiv = "<ul class='evtprw'>";
            if(!empty($images_arr)){ 
                foreach($images_arr as $image_src){ 
                    $imgdiv .= '<li><img src="'.$image_src.'" alt="Event images" height="100px" width="100px" /></li>';
                }
            }
            $imgdiv .= "</ul>";
            echo json_encode(array("status"=>200, "content"=>$imgdiv));
        }else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
        }
    }

    /*
    * insert user review
    */
    function submitReview(){
        $review = $this->input->post("userreview");
        $userid = $this->session->userdata("user_id");
        if(!empty($review) || !empty($userid)){
            $check = $this->User->insertUserReview(array("voted_user_id"=>$this->input->post("reviewto"),
                                                         "voter_user_id"=>$userid,
                                                         "review_text"=>$review,
                                                         "blog_vote"=>$this->input->post("rating"),
                                                         "review_date"=>date("Y-m-d H:i:s"),
                                                         "vote_status"=>0,
                                                         "status"=>0,
                                                        ));
            if($check){
                // $review = $this->User->getReviewRows(array("vote_id"=>$check));
                // $totalreview = $this->User->getTotalReview($review['voted_user_id']);
                // $content = '<div class="bsns-comnt-read">
                //                 <div class="bsns-comnt-img">
                //                     <img src='.base_url("assets/profile_pics/").$review['user_profile_pic'].'>
                //                 </div>
                //                 <div class="bsns-comnt-user">
                //                     <h4>'.$review['user_firstname'].'<span>'.$review['blog_vote'].'<i class="fa fa-star"></i></span></h4>
                //                     <span class="lr-revi-date">'.$review['review_date'].'</span>
                //                     <p>'.$review['review_text'].'</p> 
                //                 </div>
                //             </div>';
                
                // $overall_vote_rate = $totalreview['sum'] / $totalreview['total'];
                // $stars = '<span style="width:30px;">'.floor($overall_vote_rate).'</span>';
                // for ($i = 1; $i <= floor($overall_vote_rate); $i++) {
                //   $stars .= '<i class="fa fa-star" id="' . $i . ' aria-hidden="true"></i>';
                // } 
                $stars = "";
                $content = "";
                echo json_encode(array("status"=>200, "content"=>$content, "stars"=>$stars));
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! un soucis est survenu lors de votre publication, veuillez réessayer.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Veuillez saisir votre avis</label>"));
        }
    }

    public function recommend()
   {
        $rec_by = $this->session->userdata("user_id");
        if(empty($rec_by))
        {
           $rec_by=0;
        }
        $rec_to = !empty($this->input->post("rec_to"))?$this->input->post("rec_to"): 0;
        if($rec_to != -1 || $rec_by == -1)
        {
            $val=0; 
            $val =  $this->input->cookie('recommend_user', TRUE);
                

            if($val != $rec_to)
            {

                $query = $this->User->recommendUser($rec_by, $rec_to);
                if($query > 0){
                           
                    $cookie = array(
                               'name'   => 'recommend_user',
                               'value'  => $rec_to,
                               'expire' => time()+86500,
                               'path'   => '/',
                               );
                    $this->input->set_cookie($cookie); 

                    //print_r($this->input->cookie('recommend_user', false));  
                    echo $query;
                }
                else
                {
                    echo 0;
                }
            }
            else if($query == 0)
            {
               //already recomd
               echo 0;
            }
            else
            {
               echo "Something not correct happend";
            }
        } else{              
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé"));
        }
   }


    public function is_open($openhrs_arr = array())
    {

        date_default_timezone_set('Asia/Kolkata');

        $dt1="";
        $dt2="";
        $dt3="";
        $dt4="";

        $day = date("D");

        if($day == 'Mon')
        {
        $dt1=$openhrs_arr[0]['mon1'];
        $dt2=$openhrs_arr[0]['mon2'];
        $dt3=$openhrs_arr[0]['mon3'];
        $dt4=$openhrs_arr[0]['mon4'];
        }
        else if($day == 'Tue')
        {
        $dt1=$openhrs_arr[0]['tues1'];
        $dt2=$openhrs_arr[0]['tues2'];
        $dt3=$openhrs_arr[0]['tues3'];
        $dt4=$openhrs_arr[0]['tues4'];
        }
        else if($day == 'Wed')
        {
        $dt1=$openhrs_arr[0]['wed1'];
        $dt2=$openhrs_arr[0]['wed2'];
        $dt3=$openhrs_arr[0]['wed3'];
        $dt4=$openhrs_arr[0]['wed4'];
        }

        else if($day == 'Thu')
        {
        $dt1=$openhrs_arr[0]['thur1'];
        $dt2=$openhrs_arr[0]['thur2'];
        $dt3=$openhrs_arr[0]['thur3'];
        $dt4=$openhrs_arr[0]['thur4'];
        }

        else if($day == 'Fri')
        {
        $dt1=$openhrs_arr[0]['fri1'];
        $dt2=$openhrs_arr[0]['fri2'];
        $dt3=$openhrs_arr[0]['fri3'];
        $dt4=$openhrs_arr[0]['fri4'];
        }

        else if($day == 'Sat')
        {
        $dt1=$openhrs_arr[0]['sat1'];
        $dt2=$openhrs_arr[0]['sat2'];
        $dt3=$openhrs_arr[0]['sat3'];
        $dt4=$openhrs_arr[0]['sat4'];
        }

        else if($day == 'Sun')
        {
        $dt1=$openhrs_arr[0]['sun1'];
        $dt2=$openhrs_arr[0]['sun2'];
        $dt3=$openhrs_arr[0]['sun3'];
        $dt4=$openhrs_arr[0]['sun4'];
        }
        
        $daytime1 =  strtotime($dt1);
        $daytime2 =  strtotime($dt2);
        $daytime3 =  strtotime($dt3);
        $daytime4 =  strtotime($dt4);

        $currentTime = strtotime(date('h:i:s A'));

        if(empty($daytime1) && empty($daytime2)){
            return "";
        }else if( ((!empty($daytime1) && $currentTime > $daytime1) || (!empty($daytime2) && $currentTime > $daytime2)) && ((!empty($daytime3) && $currentTime < $daytime3) || (!empty($daytime4) && $currentTime < $daytime4)) ){  
            return "Yes we are open";
        }
        else
        {
            return "We are closed";
        }
        // if (($currentTime > $daytime1 && $currentTime < $daytime2) || $currentTime > $daytime3 && $currentTime < $daytime4)
        // {            
        //    return "Yes we are open";
        // }
        // else
        // {
        //     return "We are closed";
        // }
    }
 
    public function today_hrs($openhrs_arr = array())
    {

        date_default_timezone_set('Asia/Kolkata');

        $dt1="";
        $dt2="";
        $dt3="";
        $dt4="";

        $day = date("D");

        if($day == 'Mon')
        {
        $dt1=$openhrs_arr[0]['mon1'];
        $dt2=$openhrs_arr[0]['mon2'];
        $dt3=$openhrs_arr[0]['mon3'];
        $dt4=$openhrs_arr[0]['mon4'];
        }
        else if($day == 'Tue')
        {
        $dt1=$openhrs_arr[0]['tues1'];
        $dt2=$openhrs_arr[0]['tues2'];
        $dt3=$openhrs_arr[0]['tues3'];
        $dt4=$openhrs_arr[0]['tues4'];
        }
        else if($day == 'Wed')
        {
        $dt1=$openhrs_arr[0]['wed1'];
        $dt2=$openhrs_arr[0]['wed2'];
        $dt3=$openhrs_arr[0]['wed3'];
        $dt4=$openhrs_arr[0]['wed4'];
        }

        else if($day == 'Thu')
        {
        $dt1=$openhrs_arr[0]['thur1'];
        $dt2=$openhrs_arr[0]['thur2'];
        $dt3=$openhrs_arr[0]['thur3'];
        $dt4=$openhrs_arr[0]['thur4'];
        }

        else if($day == 'Fri')
        {
        $dt1=$openhrs_arr[0]['fri1'];
        $dt2=$openhrs_arr[0]['fri2'];
        $dt3=$openhrs_arr[0]['fri3'];
        $dt4=$openhrs_arr[0]['fri4'];
        }

        else if($day == 'Sat')
        {
        $dt1=$openhrs_arr[0]['sat1'];
        $dt2=$openhrs_arr[0]['sat2'];
        $dt3=$openhrs_arr[0]['sat3'];
        $dt4=$openhrs_arr[0]['sat4'];
        }

        else if($day == 'Sun')
        {
        $dt1=$openhrs_arr[0]['sun1'];
        $dt2=$openhrs_arr[0]['sun2'];
        $dt3=$openhrs_arr[0]['sun3'];
        $dt4=$openhrs_arr[0]['sun4'];
        }

        if(!empty($dt1) || !empty($dt2)){
            $str = "";
            if(!empty($dt1))
                $str .= $dt1;
            if(!empty($dt1) && !empty($dt2))
                $str .= "-".$dt2.", ";
            else if(empty($dt1) && !empty($dt2))
                $str .= $dt2.", ";
            else if(empty($dt2))
                $str .= ", ";

            if(!empty($dt3))
                $str .= $dt3;
            if(!empty($dt3) && !empty($dt4))
                $str .= "-".$dt4." ";
            else if(empty($dt3) && !empty($dt4))
                $str .= $dt4." ";
            else if(empty($dt4))
                $str .= "";
            // $str = $dt1. "-". $dt2. ", ". $dt3. "-". $dt4;
        }else{
            $str = "";
        }
        return $str;
    }
 
    function deleteExtraNumbers(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $check = $this->User->deleteExtraNumber(array("phn_id"=>$this->input->post("id"), "phn_user"=>$ses));
            if($check)
                echo json_encode(array("status"=>200));
            else
                echo json_encode(array("status"=>1));
        }else{
            echo json_encode(array("status"=>1));
        }
     }



     public function sendMailFormat($subject, $msginner, $template = 21)
     {
     
        $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
        $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); // Email Footer
        $templatedata = $this->User->getEmailTemplateRows(array("tmplate_id"=>$template));

       $format = '<html><head><meta charset="utf-8"><title>'.$subject.'</title><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"></head>
                    <body style="font-family: open Sans;font-size: 13px; line-height:20px;">
                    <div style="padding: 0 10px;">
                        <div style="max-width:550px;width:85%;padding:30px;margin:30px auto;border:1px solid #e2e2e2; overflow:hidden; background-position:center;background-size:cover;text-align: center;color: #fff;">
                            <div style="padding:30px;border:2px solid #fff;background:rgba(0, 0, 0, 0.63);">
                    ';
        
        $format .=  $templateheader['tmplate_content'].'
                    <div style="border-top: 1px dotted #fff; display: block; margin-top:5px; margin-bottom:5px;"></div>
                        <div class="mailbody" style="min-height: 250px;">
                            <div>'.$templatedata['tmplate_content'].' <br>'.$msginner.'</div>
                        </div>';

        $format .= $templatefooter['tmplate_content'].'
                </div>
            </div>
        </div>
        </body></html>';

        return $format;
     }


    function addNewPhone(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $phone = $this->input->post("phone");
            $label = $this->input->post("phlabel");
            $check = $this->User->checknumberavailable($phone);
            if(!$check){
                $insert_id = $this->User->insertPhonenumbers(1, array("phn_user"=>$ses, "phn_number"=>$phone, "phn_label"=>$label));
                if($insert_id){
                    $cont = '<div class="pardiv">
                                <span class="div1">
                                    <input type="text" readonly class="form-control" value="'.$label.'" />
                                </span>
                                <span class="div2">
                                    <input type="text" class="form-control" readonly value="'.$phone.'"><button type="button" class="removeextnumber" modal-aria="'.$insert_id.'"><i class="fa fa-times"></i></button>
                                </span>
                            </div>';
                    echo json_encode(array("status"=>200, "msg"=>"", "content"=>$cont));
                }else{
                    echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Oops! Veuillez réessayer.</label>"));
                }                
            }else{
                echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Ce numéro de téléphone est déjà utilisé.</label>"));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Accès refusé</label>"));
        }
    }

    function notiUpdate($args){
        $ses = $this->session->userdata("user_id");
        $noti = $args;
        if(!empty($ses)){
            if(!empty($noti) ){
                $query = $this->User->getNotificationsRows(array("conditions"=> array("nt_to"=>$ses, "nt_id"=>$noti), "returnType"=>"single"));
                if(!empty($query)){
                    $this->User->notiUpdate(array("nt_read"=>1, "nt_flag"=>1), array("nt_id"=>$noti));
                    $redurl = base_url();
                    $nttype = $query['nt_type'];
                    if($nttype == 1){
                        $redurl = base_url("Users/user_profile/").$query['user_id'];
                    }else if($nttype == 2){
                        $redurl = base_url("Users/eventsView/").$query['nt_rlt_id'];
                    }else if($nttype == 3){
                        $redurl = base_url("Users/reviewListing/");
                    }                    
                    redirect($redurl);
                }else{
                    redirect(base_url());
                }
            }else{
                redirect(base_url());
            }
        }else{
            redirect(base_url());
        }
    }

    function reviewListing(){
        $user = $this->session->userdata("user_id");
        if(!empty($user)){
            $data['details'] = $this->User->getReviewRows(array("conditions"=>array("voted_user_id"=>$user)));
            $data['TotalReview'] = $this->User->getTotalReview($user);
            header("Access-Control-Allow-Origin: *");                     
            $this->template->set('title', 'Request New Password');
            $this->template->load('home_layout', 'contents' , 'reviewListing', $data);
        }else{
            redirect(base_url());
        }
    }

    function purchaseReceipt($args){
        $data['details'] = $query = $this->User->getUserOptions(array("opt_user_id"=> $this->session->userdata("user_id"), "opt_user_option_id"=>$args));
        header("Access-Control-Allow-Origin: *");
        $this->template->set('title', 'Request New Password');
        $this->template->load('home_layout', 'contents' , 'purchaseReceipt', $data);
    }

    public function formOption()
    {
        // $this->form_validation->set_rules("txtFirstName", "First Name", "required");
        // $this->form_validation->set_rules("txtLastName", "Last Name", "required");
        // $this->form_validation->set_rules("douhavekidsprice", "Gender", "required");
        // $this->form_validation->set_rules("txtTelphone", "Telephone", "required");
        // $this->form_validation->set_rules("txtBudget", "Budget", "required");

       // if($this->form_validation->run() == TRUE)
        //{
            $arr = array("FirstName"=>$this->input->post("txtFirstName"),
                         "LastName"=>$this->input->post("txtLastName"),
                         "gender"=>$this->input->post("douhavekidsprice"),
                         "Telphone"=>$this->input->post("txtTelphone"),
                         "Budget"=>$this->input->post("txtBudget"),
                         "StartDate"=>$this->input->post("txtDate"),
                         "Description"=>$this->input->post("txtDescription"),
                         "emailTo"=>$this->input->post("emailTo"),
                         "user_id"=> $this->session->userdata("user_id"), 
                         "opt_id"=> $this->input->post("opt_id")
                        );
               
            $insert = $this->User->createFormOption($arr);
            
          
            if($insert)
            {
                $optdetails = $this->User->getUserAdvOptionsDetails($insert);

                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>28)); 
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); //Email Footer
                $tempeventdetials = $this->User->getEventsRows(array("event_id"=>$eventid));
                $emailadmin = ADMIN_EMAIL;
                $subject = $templatecenter['tmplate_subject'];
                $optionlink = "<a href='".base_url("Admin/formOptionView/$insert")."'>View Details</a>";
                $dummy = array("%%Firstname%%", "%%LastName%%", "%%Gender%%", "%%Telephone%%", "%%Budget%%", "%%StartDate%%", "%%Description%%", "%%FormOptionLink%%");
                $real = array($arr['FirstName'],
                              $arr['LastName'], 
                              $arr["gender"], 
                              $arr['Telphone'],
                              $arr['Budget'],
                              $arr['StartDate'],
                              $arr['Description'],
                              $optionlink
                            );
                $msg = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg = $templateheader['tmplate_content']." ".$msg." ".$templatefooter['tmplate_content'];
                //Send mail to userr
                    $this->send_mail($email, $subject, $msg);
                    $this->send_mail(ADMIN_EMAIL, $subject, $msg);



                //======================
                // $emailadmin = ADMIN_EMAIL;
                // $emailto= "votive.pradeep01@gmail.com";
                // $emailadmin= "votive.pradeep01@gmail.com";
                // $subject = "Form Option Selected By User";                            
                // $msginner = "<p>Hello Admin,</p>"
                //             ."A user has purchased <strong>Form Option</strong> <br>Below are details: <br>".
                //             "<p><b>User Name: </b>".$arr['FirstName']." ".$arr['LastName']. "</p>
                //             <p><b>Gender: </b>".$arr['gender']."</p>
                //             <p><b>Phone: </b>".$arr['Telphone']."</p>                                        
                //             <p><b>Budget: </b>".$arr['Budget']."</p>
                //             <p><b>Date: </b>$".$arr['StartDate']."</p>
                //             <p><b>Description: </b>".$arr['Description']."</p>
                //             <br/>
                //             <p>Thank You</p>";
                // $emailto = $arr['emailTo'];
                // $msgadmin = $this->sendMailFormat($subject, $msginner);
              
               
                // $fromemail = SENDER_EMAIL;
                // $fromname = SENDER_NAME;
               
                // $x= $this->send_mail($emailadmin, $subject, $msgadmin,  $fromemail, $fromname);
               
               
                // $msginner = "<p>Hello User,</p>"
                // ."You have purchased <strong>Form Option</strong> <br>Below are details: <br>".
                // "<p><b>User Name: </b>".$arr['FirstName']." ".$arr['LastName']. "</p>
                // <p><b>Gender: </b>".$arr['gender']."</p>
                // <p><b>Phone: </b>".$arr['Telephone']."</p>                                        
                // <p><b>Budget: </b>".$arr['Budget']."</p>
                // <p><b>Date: </b>$".$arr['StartDate']."</p>
                // <p><b>Description: </b>".$arr['Description'].
                // "</p><br/><p>Thank You</p>";
              
                // $msg = $this->sendMailFormat($subject, $msginner);
                // $fromemail = SENDER_EMAIL;
                // $fromname = SENDER_NAME;
                // $x= $this->send_mail($mailto, $subject, $msgadmin,  $fromemail, $fromname);
               
                $labels = $this->User->getTextContent(15);
                foreach($optdetails as $optd){
                    $content = '<tr>
                                  <td>'.$optd['opt_name'].'</td>
                                  <td>'.$optd['StartDate'].'</td>
                                  <td>'.$optd['Budget'].'</td>
                                  <td><a href="'.base_url("Users/myFormOption/").$optd['frm_option_id'].'" target="_blank" class="btn btn-xs btn-info tooltip"><i class="fa fa-eye"></i> <span class="tooltiptext">'.$labels['label89'].'</span></a></td>
                                </tr>';
                }
               $data['option_purchased'] = $arr;
               $data['option9_success'] = "yes";
              //  echo json_encode(array("status"=>200, "content"=>$content, "msg"=>"<label class='text-success'>Details sent successfully.</label>"));
            
            
            }else{
              //  echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Details not sent, Please try again.</label>"));
            }
            $this->session->set_userdata("option9_success", "yes");
            redirect(base_url()."Users/account");
          //  die("I am here");
            // header("Access-Control-Allow-Origin: *");                     
            // $this->template->set('title', 'Request New Password');
            // $this->template->load('home_layout', 'contents' , 'user_dashboard', $data);   
       // }
       // else {
          // echo "Required parameters missing";
      //  }        
    }


    /*
    * download uploaded documents
    */
    public function downloadUploadedDoc($args){
        if(!empty($this->session->userdata("user_id")) && !empty($args)){
            $details = $this->User->getUploadDocuments(array("conditions"=>array("dc_id"=>$args, "dc_userid"=>$this->session->userdata("user_id")), "returnType"=>"single"));
            if(!empty($details)){
                $filename = $details['dc_docpath'];
                $path = "./././assets/img/UserPurchaseDocuments/$filename";
                $filename = $details['dc_userid']."-".$details['ct_name'].".pdf";                
                header('Content-Transfer-Encoding: binary');  // For Gecko browsers mainly
                header('Accept-Ranges: bytes');  // For download resume
                header('Content-Length: ' . filesize($path));  // File size
                header('Content-Encoding: none');
                header('Content-Type: application/pdf');  // Change this mime type if the file is not PDF
                header('Content-Disposition: attachment; filename=' . $filename);  // Make the browser display the Save As dialog
                readfile($path);  //this is nec
            }else{
                $this->session->set_flashdata("error", "<label class='text-danger'>Invalid Access</label>");
                redirect(base_url("Users/account/messoptions"));
            }
        }
    }

    /*
    * Add Extra Filters
    */
    public function addExtraFilter(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){            
            $data = array("cat_name"=>$this->input->post('name'),
                          "type"=>$this->input->post('filtertype'),
                          "cat_description"=>"Manually added by user",
                          "cat_status"=>1,                         
                          "cat_by_user"=>$ses
                        );
            $check = $this->User->insertTable("otd_business_category", 1, $data);
            if($check){
                $sender = $this->User->getRows(array("user_id"=>$ses));

                $templateheader = $this->User->getEmailTemplateRows(array("tmplate_id"=>26)); //Email Header
                $templatecenter = $this->User->getEmailTemplateRows(array("tmplate_id"=>38)); 
                $templatefooter = $this->User->getEmailTemplateRows(array("tmplate_id"=>25)); //Email Footer
               
                $emailadmin = ADMIN_EMAIL;

                $subject = $templatecenter['tmplate_subject'];
                $usrname = $sender['user_firstname'].' '.$sender['user_lastname'];
               
                
                $dummy = array(
                             "%%filtername%%",
                             "%%category%%",
                             "%%username%%"
                 );
                
                $real = array(
                            $this->input->post('name'),
                              $this->input->post('filtertype'), 
                              $usrname
                            );
              
                $msg1 = str_replace($dummy, $real, $templatecenter['tmplate_content'] );
                $msg1 = $templateheader['tmplate_content']." ".$msg1." ".$templatefooter['tmplate_content'];
                //Send mail to userr
                    
                 $this->send_mail($emailadmin, $subject, $msg1);


                $msg = "<option value='".$check."' selected='selected'>".$this->input->post('name')."</option>";
                echo json_encode(array("status"=>200, "msg"=>$msg, "type"=>$this->input->post("filtertype")));
            }else{
                $msg = "<label class='text-danger'>Something went wrong, Please try after sometime.</label>";
                echo json_encode(array("status"=>1, "msg"=>$msg));
            }
        }else{
            echo json_encode(array("status"=>1, "msg"=>"Accès refusé."));
        }
    }


}
