<?php
$charlimit = 500;
$imgdefaultlimit = 3;
if($desclimit->num_rows() > 0){

  foreach($desclimit->result() as $dsc){ }
    if($dsc->opt_option_status == 1 && $dsc->opt_option_active_date <= date("Y-m-d")){
        $charlimit = 3000;
        ?>
        <input type="hidden" id="descltd" value="3000" />
      <?php
    }else{
      ?>
      <input type="hidden" id="descltd" value="500" />
      <?php
    }  
}else{  
?>
  <input type="hidden" id="descltd" value="500" />
  <?php
}
?>

<div class="main-dashboard">
  <section class="dash-board-sec">
    <div class="container">
      <div class="dash-board-head">
        <div class="row">
          <div class="col-md-4">
            <div class="header-link-dash"> <a href="<?php echo base_url("Pages/businessListing"); ?>" class="btn btn-default"><i class="fa fa-arrow-left"> </i>Page de recherche</a> <a href="<?php echo base_url("Users/user_profile"). "/".$this->session->userdata('user_id'); ?>" class="btn btn-default"><i class="fa fa-arrow-left"> </i>Mon profil</a> </div>
          </div>
        </div>
      </div>
      <div class="dash-board-inner">
        <div class="panel panel-primary">
          <div class="dash-link-main">
            <ul class="nav panel-tabs1">
              <li class="<?php if($this->uri->segment(3) != "inbox" && $this->uri->segment(3) != "notifications" && $this->uri->segment(3) != "messoptions") echo "active"; ?>"><a href="#tab1" data-toggle="tab">Mes Informations</a></li>
              <li ><a href="#tab2" data-toggle="tab">Mes commerçants</a></li>
              <li class="<?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "notifications") echo "active"; ?>"><a  id="messagetab" href="#tab3" data-toggle="tab">Mes Messages</a></li>
              <?php
              if($user['user_type'] == "Professional"){
              ?>
                <li class="<?php if($this->uri->segment(3) == "messoptions") echo "active"; ?>"><a href="#tab4" data-toggle="tab">Mes Options</a></li>
                <!-- <li><a href="#tab5" data-toggle="tab">Create Event</a></li> -->
              <?php
              }
              ?>              
            </ul>
          </div>
          <div class="panel-body">
            <div class="tab-content">
              <div class="tab-pane <?php if($this->uri->segment(3) != "inbox" && $this->uri->segment(3) != "notifications" && $this->uri->segment(3) != "messoptions") echo "active"; ?>" id="tab1">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs2">
                          <li class="active"><a href="#tab-sub1" data-toggle="tab">Informations personnelles</a></li>
                          <li><a href="#tab-sub2" data-toggle="tab">Account parameter</a></li>
                          <?php
                          if($user['user_type'] == "Professional"){
                          ?>
                          <li><a href="#tab-sub3" data-toggle="tab">Mon entreprise</a></li>
                          <?php
                          }
                          ?>
                          <li><a href="#tab-sub4" data-toggle="tab">Boite à idée</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab-sub1">
                            <form id="personalinfoupdate" method="post" action="">
                              <div class="mes-info-sec">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label" for="usr"><?php echo $labels['label1']; ?>: 
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label1']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" class="form-control" name="first_name" value="<?php echo $user['user_firstname']; ?>">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label" for="usr"><?php echo $labels['label2']; ?>: 
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label2']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" class="form-control" name="last_name" value="<?php echo $user['user_lastname']; ?>">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="language-main"><?php echo $labels['label3']; ?>
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label3']; ?></span>
                                        </span>
                                      </label>
                                      <?php
                                      $lang = $user['user_language'];
                                      if(!empty($lang))
                                        $lang = explode(",", $lang);                                    
                                      else
                                        $lang = array();
                                      ?>
                                      <div class="check-box-inline">
                                        <?php
                                        if(!empty($languages)){
                                          foreach($languages as $lang1){
                                            ?>
                                            <label class="checkbox-inline">
                                              <input type="checkbox" name="lang[]" value="<?php echo $lang1['lang_id']; ?>" <?php if(in_array($lang1['lang_id'], $lang)) echo "checked"; ?>>
                                              <?php echo ucwords($lang1['lang_name']); ?>
                                            </label>
                                            <?php
                                          }
                                        }
                                        ?>
                                        <!-- <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="2" <?php if(in_array(2, $lang)) echo "checked"; ?>>
                                          English 
                                        </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="3" <?php if(in_array(3, $lang)) echo "checked"; ?>>
                                          French </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="4" <?php if(in_array(4, $lang)) echo "checked"; ?>>
                                          Spanish </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="5" <?php if(in_array(5, $lang)) echo "checked"; ?>>
                                          Arab </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="6" <?php if(in_array(6, $lang)) echo "checked"; ?>>
                                          Italion </label>
                                        <label class="checkbox-inline">
                                          <input type="checkbox" name="lang[]" value="7" <?php if(in_array(7, $lang)) echo "checked"; ?>>
                                          Germen </label> -->
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="gender-main"><?php echo $labels['label4']; ?>
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label4']; ?></span>
                                        </span>
                                      </label>
                                      <div class="for-radio">
                                        <div class="btn-group" data-toggle="buttons">
                                          <label class="btn btn-default <?php if($user['user_gender'] == "Male" || $user['user_gender'] == "male") echo "active"; ?>">
                                            <input type="radio" name="douhavekidsprice" <?php if($user['user_gender'] == "Male" || $user['user_gender'] == "male") echo "checked"; ?> value="Male">
                                            Homme </label>
                                          <label class="btn btn-default <?php if($user['user_gender'] == "Female" || $user['user_gender'] == "female") echo "active"; ?>">
                                            <input type="radio" name="douhavekidsprice" <?php if($user['user_gender'] == "Female" || $user['user_gender'] == "female") echo "checked"; ?> value="Female">
                                            Femme </label>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label date" for="usr"><?php echo $labels['label5']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label5']; ?></span>
                                        </span>
                                      </label>
                                      <p>
                                        <input type="text" name="dob" class="form-control datepickerpast" placeholder="<?php echo $labels['label5']; ?>" value="<?php if($user['user_dob'] != "0000-00-00") echo $user['user_dob']; ?>">
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                      <div class="update-button">
                                          <button class="btn btn-default profsubmit" type="submit"><?php echo $labels['label45']; ?></button>
                                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span> 
                                          <div id="profilemsg"></div>
                                      </div>                                      
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="tab-pane" id="tab-sub2">
                            <div class="mes-info-sec">
                              <div class="row">
                                <form method="post" action="" id="changeemail" autocomplete="off" >
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label6']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label6']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" name="email" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label7']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label7']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="password" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <button type="submit" class="chnge-mail btn btn-default upmail"><?php echo $labels['label46']; ?></button>
                                    <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                    <div id="updateemailmsg"></div>
                                  </div>
                                </form>
                              </div>
                              <div class="row">
                                <form method="post" action="" id="updatepassword">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label8']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label8']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="oldpass" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label9']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label9']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="newpass" id="newpass" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label confm-pass"><?php echo $labels['label10']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label10']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="newcpass" class="form-control cnfm-pass">
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <button type="submit" class="chnge-mail btn btn-default upps"><?php echo $labels['label47']; ?></button>
                                    <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                    <div id="updatepsmsg"></div>
                                  </div>
                                </form>
                              </div>
                              <div class="row">
                                <form id="userdeactivate" method="post" action="">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label confm-pass"><?php echo $labels['label11']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label11']; ?></span>
                                        </span>
                                      </label>
                                      <input type="Password" name="dpass" class="form-control cnfm-pass">
                                    </div>
                                    <button type="submit" class="chnge-mail btn btn-default deactivateac"><?php echo $labels['label48']; ?></button>
                                    <span style="display:none;" class="dloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                    <div id="dacctmsg"></div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                          <?php
                          if($user['user_type'] == "Professional"){
                            ?>
                          <div class="tab-pane" id="tab-sub3">                            
                            <div class="mes-info-sec">
                              <form id="businessupdate" method="post">
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label12']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label12']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" name="cm_name" value="<?php echo $user['user_company_name']; ?>" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label13']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label13']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" name="cm_number" value="<?php echo $user['user_company_number']; ?>" class="form-control">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label14']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label14']; ?></span>
                                        </span>  
                                      </label>
                                      <input type="text" name="cm_url" value="<?php echo $business['bs_website']; ?>" class="form-control">
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label15']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label15']; ?></span>
                                        </span>
                                      </label>
                                      <p>
                                        <input type="text" name="cm_opening" value="<?php if($business['bs_opening'] != "0000-00-00") echo $business['bs_opening']; ?>" class="form-control datepickerpast">
                                      </p>
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label16']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label16']; ?></span>
                                        </span>
                                      </label>
                                      <div class="existingphone">
                                        <input type="text" name="cm_phnumber" value="<?php echo $user['user_phone']; ?>" class="form-control">                                          
                                      </div>
                                      <div class="extranumbers">
                                        <?php
                                        if(!empty($extraphone)){
                                          foreach($extraphone as $nb){
                                            ?>
                                            <div class="pardiv">
                                              <span class="div1">
                                                  <input type="text" readonly class="form-control" value="<?php echo $nb['phn_label']; ?>" />
                                              </span>
                                              <span class="div2">
                                                  <input type="text" class="form-control" readonly value="<?php echo $nb['phn_number']; ?>"><button type="button" class="removeextnumber" modal-aria="<?php echo $nb['phn_id']; ?>"><i class="fa fa-times"></i></button>
                                              </span>                                                
                                            </div>
                                            <?php
                                          }
                                        }
                                        ?>
                                      </div>
                                      <div class="addnewphone">                                          
                                      </div>
                                      <button type="button" id="addphone" data-toggle="modal" data-target="#addphonenumbers" class="btn btn-default"><i class="fa fa-plus"></i> <?php echo $labels['label50']; ?></button>
                                    </div>
                                  </div>
                                  <div class="col-md-6">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label17']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label17']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" value="<?php echo $business['bs_twitter']; ?>" name="cm_twitter" class="form-control">
                                    </div>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label18']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label18']; ?></span>
                                        </span>
                                      </label>
                                      <input type="email" name="cm_email" value="<?php echo $user['bs_email']; ?>" class="form-control">
                                    </div>
                                  </div>
                                </div>
                                <div class="catergory-containr">
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="form-label"><?php echo $labels['label19']; ?>:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label19']; ?></span>
                                          </span>
                                        </label>                                        
                                        <input class='form-control' multiple='multiple' list='categories' name='user_categories' id="business_category" type='text' placeholder="<?php echo $labels['label19']; ?>" value="<?php echo $this->User->getCategoriesAndFilters(1); ?>">
                                        <datalist id="categories">
                                            <?php
                                            foreach($business_categories->result() as $cate){
                                              ?>
                                              <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                              <?php
                                            }
                                            ?>                        
                                        </datalist>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="catergory-containr">                                   
                                  <div class="row">
                                    <div class="col-md-12">
                                      <div class="form-group">
                                        <label class="form-label"><?php echo $labels['label20']; ?>:
                                          <span class="tooltip">?
                                            <span class="tooltiptext"><?php echo $suggestion['label20']; ?></span>
                                          </span>
                                        </label>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="ano-cat-sub">
                                        <div class="anno-cat-head">
                                          <h3><?php echo $labels['label51']; ?></h3>
                                        </div>
                                        <div class="service_ul" data-mcs-theme="dark">
                                          <input class='form-control' multiple='multiple' list='servicefilterslist' name='servicefilters' id="servicefilters" type='text' placeholder="<?php echo $labels['label51']; ?>" value="<?php echo $this->User->getCategoriesAndFilters(2); ?>">

                                          <datalist id="servicefilterslist">
                                              <?php
                                              foreach($service_filters->result() as $cate){
                                                ?>
                                                <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                <?php
                                              }
                                              ?>                        
                                          </datalist>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="ano-cat-sub">
                                        <div class="anno-cat-head">
                                          <h3><?php echo $labels['label52']; ?></h3>
                                        </div>
                                        <div class="service_ul" data-mcs-theme="dark">
                                          <input class='form-control'                     
                                                 multiple='multiple'
                                                 list='productfilterslist'
                                                 name='productfilters'
                                                 id="productfilters"
                                                 type='text' placeholder="<?php echo $labels['label52']; ?>" value="<?php echo $this->User->getCategoriesAndFilters(3); ?>">

                                          <datalist id="productfilterslist">
                                              <?php
                                              foreach($product_filters->result() as $cate){
                                                ?>
                                                <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                <?php
                                              }
                                              ?>                        
                                          </datalist>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-md-4">
                                      <div class="ano-cat-sub">
                                        <div class="anno-cat-head">
                                          <h3><?php echo $labels['label53']; ?></h3>
                                        </div>
                                        <div class="service_ul" data-mcs-theme="dark">
                                          <input class='form-control'                     
                                                 multiple='multiple'
                                                 list='otherfilterslist'
                                                 name='otherfilters'
                                                 id="otherfilters"
                                                 type='text' placeholder="<?php echo $labels['label53']; ?>" value="<?php echo $this->User->getCategoriesAndFilters(4); ?>">

                                          <datalist id="otherfilterslist">
                                              <?php
                                              foreach($other_filters->result() as $cate){
                                                ?>
                                                <option value="<?php echo $cate->cat_id; ?>"><?php echo $cate->cat_name; ?></option>
                                                <?php
                                              }
                                              ?>                        
                                          </datalist>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label21']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label21']; ?></span>
                                        </span>
                                      </label>
                                      <input type="text" id="autocomplete" onFocus="geolocate()" value="<?php echo $user['user_company_address']; ?>" name="cm_address" class="form-control">
                                    </div>
                                  </div>
                                </div>
                                <div class="row text-area">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label"><?php echo $labels['label22']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label22']; ?></span>
                                        </span>
                                      </label>
                                      <textarea class="form-control text-main" name="cm_desc" id="cm_desc" rows="4"><?php echo $business['bs_desc']; ?></textarea>
                                      <span class="max-char"><?php echo $labels['label62']; ?>: <span id="desclength"><?php echo $charlimit - strlen($business['bs_desc']); ?></span></span> </div>
                                  </div>
                                </div>
                                <?php
                                if($searchbannerdisplay->num_rows() > 0 ){
                                  foreach($searchbannerdisplay->result() as $bnmsg){ }
                                ?>
                                <div class="row text-area bannrtxt" style="<?php if($bnmsg->opt_option_status == 1 && $bnmsg->opt_option_active_date <= date("Y-m-d")){}else { echo "display: none";} ?>">
                                  <div class="col-md-12">
                                    <div class="form-group">
                                      <label class="form-label">Banner Display:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label22']; ?></span>
                                        </span>
                                      </label>
                                      <textarea class="form-control text-main" name="cm_banner_desc" id="cm_banner_desc" rows="4"><?php echo $business['bs_banner_display']; ?></textarea>
                                      <!-- <span class="max-char">Nombre de caractères: <span id="desclength"><?php echo $charlimit - strlen($business['bs_desc']); ?></span></span>  -->
                                      </div>
                                  </div>
                                </div>
                                <?php
                                }
                                ?>
                                <div class="row">
                                  <div class="col-md-12">
                                      <div class="update-button">
                                        <button class="btn btn-default bsbutton"><?php echo $labels['label49']; ?></button>
                                        <span style="display:none;" class="bsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                        <div id="bsmsg"></div>
                                      </div>
                                  </div>
                                </div>
                              </form>
                              <hr>
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="form-label"><?php echo $labels['label23']; ?>:
                                      <span class="tooltip">?
                                        <span class="tooltiptext"><?php echo $suggestion['label23']; ?></span>
                                      </span>
                                    </label>
                                    <form id="uploadbsprice" enctype="multipart/form-data" method="post" action="">
                                      <div class="col-md-9">
                                        <input type="file" name="bs_price_pdf" class="form-control form-file">
                                        <label style="color:#F00;" id="prcmsg"></label>
                                      </div>
                                      <div class="col-md-3 update-button">
                                        <button class="btn btn-default prbutton"><?php echo $labels['label54']; ?></button>
                                        <span style="display:none;" class="prloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                      </div>                                      
                                    </form>
                                  </div>
                                  <div class="price-listing">
                                    <?php
                                    if(!empty($prstable)){
                                      foreach($prstable as $prs){
                                        ?>
                                        <div class="file-uploaded">
                                          <div class="file-upl-name">
                                            <p><?php echo $prs['pr_metaname']; ?></p>
                                          </div>
                                          <div class="file-upload-del"> <a href="javascript:;" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left" class="delete-pricefile" modal-aria="<?php echo $prs['pr_id']; ?>"><i class="fa fa-close"></i></a> </div>
                                        </div>
                                        <?php
                                      }
                                    }
                                    ?>
                                  </div>

                                </div>
                              </div>
                              <hr>
                              <div class="row time-intervl">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="form-label"><?php echo $labels['label24']; ?>:
                                      <span class="tooltip">?
                                        <span class="tooltiptext"><?php echo $suggestion['label24']; ?></span>
                                      </span>
                                    </label>
                                  </div>
                                </div>
                                <form method="post" action="" id="openingtime">
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label25']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label25']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input"> 
                                        <input type="text" name="mon1" value="<?php echo $opentiming['mon1']; ?>"  class="timepicker form-control">
                                        <input type="text" name="mon2" value="<?php echo $opentiming['mon2']; ?>"  class="timepicker form-control">
                                        <label class="or-label"><?php echo $labels['label55']; ?></label>
                                         <input type="text" name="mon3" value="<?php echo $opentiming['mon3']; ?>" class="timepicker form-control">
                                         <input type="text" name="mon4" value="<?php echo $opentiming['mon4']; ?>" class="timepicker form-control">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label26']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label26']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input">
                                        <input type="text" name="tues1" value="<?php echo $opentiming['tues1']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="tues2" value="<?php echo $opentiming['tues2']; ?>" class="timepicker form-control"/>
                                        <label class="or-label"><?php echo $labels['label55']; ?></label>
                                        <input type="text" name="tues3" value="<?php echo $opentiming['tues3']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="tues4" value="<?php echo $opentiming['tues4']; ?>" class="timepicker form-control"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label27']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label27']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input">
                                        <input type="text" name="wed1" value="<?php echo $opentiming['wed1']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="wed2" value="<?php echo $opentiming['wed2']; ?>" class="timepicker form-control"/>
                                        <label class="or-label">a<?php echo $labels['label55']; ?>nd</label>
                                        <input type="text" name="wed3" value="<?php echo $opentiming['wed3']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="wed4" value="<?php echo $opentiming['wed4']; ?>" class="timepicker form-control"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label28']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label28']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input">
                                        <input type="text" name="thur1" value="<?php echo $opentiming['thur1']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="thur2" value="<?php echo $opentiming['thur2']; ?>" class="timepicker form-control"/>
                                        <label class="or-label"><?php echo $labels['label55']; ?></label>
                                        <input type="text" name="thur3" value="<?php echo $opentiming['thur3']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="thur4" value="<?php echo $opentiming['thur4']; ?>" class="timepicker form-control"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label29']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label29']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input">
                                        <input type="text" name="fri1" value="<?php echo $opentiming['fri1']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="fri2" value="<?php echo $opentiming['fri2']; ?>" class="timepicker form-control"/>
                                        <label class="or-label"><?php echo $labels['label55']; ?></label>
                                        <input type="text" name="fri3" value="<?php echo $opentiming['fri3']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="fri4" value="<?php echo $opentiming['fri4']; ?>" class="timepicker form-control"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label30']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label30']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input">
                                        <input type="text" name="sat1" value="<?php echo $opentiming['sat1']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="sat2" value="<?php echo $opentiming['sat2']; ?>" class="timepicker form-control"/>
                                        <label class="or-label"><?php echo $labels['label55']; ?></label>
                                        <input type="text" name="sat3" value="<?php echo $opentiming['sat3']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="sat4" value="<?php echo $opentiming['sat4']; ?>" class="timepicker form-control"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="form-group time-shift">
                                      <label class="form-label"><?php echo $labels['label31']; ?>:
                                        <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label31']; ?></span>
                                        </span>
                                      </label>
                                      <div class="time-input">
                                        <input type="text" name="sun1" value="<?php echo $opentiming['sun1']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="sun2" value="<?php echo $opentiming['sun2']; ?>" class="timepicker form-control"/>
                                        <label class="or-label"><?php echo $labels['label55']; ?></label>
                                        <input type="text" name="sun3" value="<?php echo $opentiming['sun3']; ?>" class="timepicker form-control"/>
                                        <input type="text" name="sun4" value="<?php echo $opentiming['sun4']; ?>" class="timepicker form-control"/>
                                      </div>
                                    </div>
                                  </div>
                                  <div class="col-md-12">
                                    <div class="update-button">
                                      <button class="btn btn-default bsopbutton"><?php echo $labels['label56']; ?></button>
                                      <span style="display:none;" class="bsoploading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                      <div id="bsopmsg"></div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                              <hr>
                              <div class="row add-photos">
                                <div class="col-md-12">                                  
                                  <div class="form-group">
                                    <label class="form-label"><?php echo $labels['label32']; ?>:
                                      <span class="tooltip">?
                                          <span class="tooltiptext"><?php echo $suggestion['label32']; ?></span>
                                        </span>
                                    </label>
                                    <form id="uploadbsphoto" enctype="multipart/form-data" method="post" action="">
                                      <div class="row">
                                        <div class="col-md-9">
                                          <input type="file" name="bs_photos" class="form-control form-file">  
                                          <label style="color:#F00;" id="ptgmsg"></label>
                                        </div>
                                        <div class="col-md-3 update-button">
                                          <button class="btn btn-default ptgbutton"><?php echo $labels['label57']; ?></button>
                                          <span style="display:none;" class="ptgloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                                        </div>
                                      </div>

                                      <div class="row">
                                        <div class="col-sm-12">
                                          <div class="light-list-gallery">
                                            <ul id="img-gallry-box">
                                              <?php
                                              if(!empty($gallery)){
                                                foreach($gallery as $photo){
                                                  $path = $photo['ph_path'];
                                              ?>
                                                <li>
                                                  <div class="up-img">
                                                    <a href="<?php echo base_url("assets/photogallery/$path"); ?>" data-lightbox="example-set">
                                                      <img src="<?php echo base_url("assets/photogallery/$path"); ?>">
                                                    </a>
                                                     <div class="del-cross">
                                                        <a href="javascript:;" modal-aria="<?php echo $photo['ph_id']; ?>" class="make-profile"><i class="fa fa-user"></i></a><br>
                                                        <a href="javascript:;" modal-aria="<?php echo $photo['ph_id']; ?>" class="delete-galphoto"><i class="fa fa-trash-o"></i></a>
                                                      </div>
                                                  </div>
                                                </li>
                                              <?php
                                                }
                                              }
                                              ?>
                                            </ul>
                                          </div>
                                        </div>
                                      </div>

                                    </form>
                                  </div>
                                </div>
                              </div>                                
                            </div>                            
                          </div>
                          <?php
                          }
                          ?>
                          <div class="tab-pane" id="tab-sub4">
                            <div class="mes-info-sec mesg-send">
                              <form id="boxideamessages">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="form-label"><?php echo $labels['label33']; ?>:
                                      <span class="tooltip">?
                                        <span class="tooltiptext"><?php echo $suggestion['label33']; ?></span>
                                      </span>
                                    </label>
                                    <input type="text" name="title" class="form-control">
                                  </div>
                                </div>
                              </div>
                              <div class="row text-area">
                                <div class="col-md-12">
                                  <div class="form-group">
                                    <label class="form-label"><?php echo $labels['label34']; ?>:
                                      <span class="tooltip">?
                                        <span class="tooltiptext"><?php echo $suggestion['label34']; ?></span>
                                      </span>
                                    </label>
                                    <textarea class="form-control text-main" name="message" rows="4"></textarea>
                                  </div>
                                </div>
                                <div class="col-md-12">
                                  <button class="btn btn-default" id="msgsubmit" type="submit"><?php echo $labels['label58']; ?></button>
                                  <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span> 
                                  <div id="msgmessag"></div>
                                </div>
                              </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="tab2">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs3">
                          <li class="active"><a href="#tab2-sub1" data-toggle="tab">Mes favoris</a></li>
                          <li><a href="#tab2-sub2" data-toggle="tab">Bon plan de mes favoris</a></li>
                          <li><a href="#tab2-sub3" data-toggle="tab">Mes Bon Plan</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab2-sub1">
                            <div class="mes-commercant">
                                <?php
                                if(!empty($following)){
                                  foreach($following as $flw){
                                  ?>
                                    <div class="listing-sub-mes">
                                      <div class="listing-sub">
                                        <div class="image-lis-sub">
                                          <?php $picpath = $flw['user_profile_pic'];
                                            $flid = $flw['usersid'];
                                          ?>
                                          <img src="<?php echo base_url("assets/profile_pics/$picpath"); ?>">
                                        </div>
                                        <div class="image-txt-sub">
                                          <div class="row">
                                            <div class="col-md-8">
                                              <div class="its-main">
                                                <div class="img-txt-subhead">
                                                  <h3>
                                                  <?php //echo ucwords($flw['user_company_name']); ?>
                                                  <?php
                                                    if($flw['user_type'] == "Professional"){
                                                      if(!empty($flw['bs_name']))
                                                        echo ucwords($flw['bs_name']);
                                                      else
                                                        echo ucwords($flw['user_firstname']." ".$flw['user_lastname']);
                                                    }
                                                    else{
                                                      echo ucwords($flw['user_firstname']." ".$flw['user_lastname']);
                                                    }
                                                    ?>
                                                  </h3>
                                                  <?php
                                                  $totalreview = $this->User->getTotalReview($flid);
                                                  if(!empty($totalreview['total']))
                                                    $overall_vote_rate = round($totalreview['sum'] / $totalreview['total']);                                                
                                                  else
                                                    $overall_vote_rate = 0;
                                                  $stars = '';
                                                  $stars1 = '';
                                                                                                                                                          
                                                  ?>
                                                  <span class="rating-main">
                                                    <ul>
                                                      <?php 
                                                      for ($i = 1; $i <= $overall_vote_rate; $i++) {
                                                        $stars .= '<li><i class="fa fa-star" id="' . $i . ' aria-hidden="true"></i></li>';

                                                      }
                                                       // echo $i;
                                                      for ($ix = $overall_vote_rate+1; $ix <= 5; $ix++) {
                                                        $stars1 .= '<li><i class="fa fa-star-o" id="' . $ix . ' aria-hidden="false"></i></li>';
                                                      }
                                                      echo $stars. $stars1;
                                                      ?>
                                                      <!-- <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li>
                                                      <li><i class="fa fa-star"></i></li> -->
                                                    </ul>                                                    
                                                  </span>
                                                  &nbsp;
                                                  <?php
                                                    if($flw['is_online']){
                                                      echo '<i class="fa fa-circle online"></i>';
                                                    }
                                                  ?>
                                                </div>
                                                <p class="add-l"><i class="fa fa-map" aria-hidden="true"></i> <?php echo $flw['user_company_address'] != ""? $flw['user_company_address']: "NA"; ?></p>
                                                <p class="catgry-l"><span class="f-cat"><i class="fa fa-bars" aria-hidden="true"></i> Categorie(s): </span><span><?php if(!empty($flw['categories'])) echo $flw['categories']; echo "NA"; ?></span></p>
                                                <p class="distance-l"><span class="f-cat"><i class="fa fa-map-marker"></i>Distance: </span><span><?php if(!empty($flw['distance'])) echo $flw['distance']." KM"; else echo "NA"; ?></span></p>
                                              </div>
                                            </div>
                                            <div class="col-md-4">
                                              <div class="list-status">
                                                <ul>
                                                  <li><a href="<?php echo base_url("Users/user_profile/$flid");?>" target="_blank"><i class="fa fa-phone" aria-hidden="true"></i><?php echo $labels['label65']; ?></a></li>
                                                  <li><a href="javascript:;" class="unfollow-user" modal-aria="<?php echo $flid; ?>" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="left"><i class="fa fa-thumbs-o-down" aria-hidden="true"></i><?php echo $labels['label66']; ?></a></li>
                                                  
                                                  <?php
                                                  if($flw['is_online'] == 1){
                                                    ?>
                                                    <li>
                                                      <a href="javascript:;" class="online-chat" modal-aria="<?php echo $flid; ?>"><i class="fa fa-envelope" aria-hidden="true"></i><?php echo $labels['label68']; ?></a>                                                    
                                                    </li>
                                                    <?php
                                                  }else{
                                                    ?>
                                                    <li>
                                                      <a href="javascript:;" data-toggle="modal" data-target="#chat-modal" class="usermessges" modal-aria="<?php echo $flid; ?>"><i class="fa fa-envelope" aria-hidden="true"></i><?php if($flw['is_online']) $labels['label68']; else echo $labels['label67'] ?></a>                                                    
                                                    </li>
                                                    <?php
                                                  }
                                                  ?>

                                                  
                                                </ul>
                                              </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  <?php
                                  }
                                }else{
                                  ?>
                                  <div class="listing-sub-mes">
                                    <h4 class="noti-sub"><?php echo $labels['label69']; ?></h4>                                    
                                  </div>
                                  <?php
                                }
                                ?>                                
                            </div>
                          </div>
                          <div class="tab-pane" id="tab2-sub2">
                            <div class="mes-commercant">
                              <?php
                              if(!empty($followingEvents)){
                                foreach($followingEvents as $evnt){
                                  $picpath = $evnt['user_profile_pic'];                                  
                                  ?>
                                  <div class="listing-bon-plan">
                                    <div class="evnt-listing">
                                      <a href="<?php echo base_url("Users/eventsView/").$evnt['event_id']; ?>" target="_blank">
                                        <ul class="event-list">
                                          <li>
                                            <div class="mang-envt-time">
                                              <span class="day"><?php echo date("d", strtotime($evnt['event_start_date'])); ?></span>
                                              <span class="month"><?php echo date("M", strtotime($evnt['event_start_date'])); ?></span>
                                              <span class="year"><?php echo date("Y", strtotime($evnt['event_start_date'])); ?></span>                                            
                                            </div>
                                            <div class="mang-evnt-dicrp">
                                              <h2 class="title"><?php echo substr(ucwords($evnt['event_name']), 0, 50); ?></h2>
                                              <p class="desc"><?php echo substr($evnt['event_description'], 0, 100); ?></p>
                                            </div>
                                            <div class="mang-evnt-usr-pro">
                                              <div class="profile-image">
                                                <img src="<?php echo base_url("assets/profile_pics/$picpath"); ?>"/>
                                              </div>
                                              <div class="mang-evnt-usr-profile ">
                                                <strong>
                                                <?php 
                                                if($evnt['user_type'] == "Professional"){
                                                  if(!empty($evnt['bs_name'])) 
                                                    echo ucwords($evnt['bs_name']);
                                                  else
                                                    echo ucwords($evnt['user_firstname']." ".$evnt['user_lastname']);
                                                }
                                                else
                                                  echo ucwords($evnt['user_firstname']." ".$evnt['user_lastname']);                                                
                                                ?> 
                                                <?php //echo ucwords($evnt['user_company_name']); ?>
                                                  
                                                </strong>
                                                <article>Event leader</article>
                                              </div>
                                            </div>
                                          </li>
                                        </ul>
                                      </a>
                                    </div>
                                  </div>
                                  <?php
                                }
                              }else{
                                ?>
                                <div class="listing-bon-plan">
                                  <div class="evnt-listing">
                                    <a href="javascript:;">
                                      <ul class="event-list">
                                        <li>
                                          <div class="mang-envt-time">
                                            <span class="day"><?php echo date("d"); ?></span>
                                            <span class="month"><?php echo date("M"); ?></span>
                                            <span class="year"><?php echo date("Y"); ?></span>                                            
                                          </div>
                                          <div class="mang-evnt-dicrp">
                                            <h2 class="title"></h2>
                                            <p class="desc"><?php echo $labels['label70']; ?></p>
                                          </div>
                                        </li>
                                      </ul>
                                    </a>
                                  </div>
                                </div>
                                <?php
                              }
                              ?>
                              
                            </div>
                          </div>

                          <!--user Event-->
                          <div class="tab-pane" id="tab2-sub3">
                            <div class="mes-commercant">
                              <?php
                              if(!empty($myEvents)){
                                foreach($myEvents as $evnt){                                  
                                  ?>
                                  <div class="listing-bon-plan">
                                    <div class="evnt-listing">
                                      <a href="<?php echo base_url("Users/myEvents/").$evnt['event_id']; ?>" target="_blank">
                                        <ul class="event-list">
                                          <li>
                                            <div class="mang-envt-time">
                                              <span class="day"><?php echo date("d", strtotime($evnt['event_start_date'])); ?></span>
                                              <span class="month"><?php echo date("M", strtotime($evnt['event_start_date'])); ?></span>
                                              <span class="year"><?php echo date("Y", strtotime($evnt['event_start_date'])); ?></span>                                            
                                            </div>
                                            <div class="mang-evnt-dicrp">
                                              <h2 class="title"><?php echo ucwords($evnt['event_name']);
                                                if($evnt['event_is_free']){
                                                  echo '&nbsp;<i class="fa fa-star-o"></i>';
                                                }
                                            ?></h2>
                                              <p class="desc"><?php echo substr($evnt['event_description'], 0, 100); ?></p>
                                            </div>
                                            <!-- <div class="mang-evnt-usr-pro">
                                              <button>View</button>
                                              <button>Delete</button>
                                            </div> -->
                                          </li>
                                        </ul>
                                      </a>
                                    </div>
                                  </div>
                                  <?php
                                }
                              }else{
                                ?>
                                <div class="listing-bon-plan">
                                  <div class="evnt-listing">
                                    <a href="javascript:;">
                                      <ul class="event-list">
                                        <li>
                                          <div class="mang-envt-time">
                                            <span class="day"><?php echo date("d"); ?></span>
                                            <span class="month"><?php echo date("M"); ?></span>
                                            <span class="year"><?php echo date("Y"); ?></span>                                            
                                          </div>
                                          <div class="mang-evnt-dicrp">
                                            <h2 class="title"></h2>
                                            <p class="desc">No Events Available.</p>
                                          </div>
                                        </li>
                                      </ul>
                                    </a>
                                  </div>
                                </div>
                                <?php
                              }
                              ?>
                              
                            </div>
                          </div>
                          <!--user Event-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="tab-pane <?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "notifications") echo "active"; ?>" id="tab3">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs4">
                          <li class="<?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "messoptions" || $this->uri->segment(3) == "") echo "active"; ?>"><a id="messagetab1" href="#tab3-sub1" data-toggle="tab">Message</a></li>
                          <li class="<?php if($this->uri->segment(3) == "notifications") echo "active"; ?>"><a id="notiftab1" href="#tab3-sub2" data-toggle="tab">Notification</a></li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-md-9 col-sm-8 bg-col">
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane <?php if($this->uri->segment(3) == "inbox" || $this->uri->segment(3) == "messoptions" || $this->uri->segment(3) == "") echo "active"; ?>" id="tab3-sub1">
                            <div class="mes-messages">
                            <?php
                            if($messagelisting->num_rows() > 0){
                              
                              foreach($messagelisting->result() as $msg){
                                $query = $this->db->select("msg_content, msg_date, msg_reciever")->get_where("otd_user_messaging", array("msg_id"=>$msg->mx_id));
                                foreach($query->result() as $lstmsg){ }
                                $user_pic = $msg->user_profile_pic;
                              ?>
                                <div class="msg-sub-list">
                                    <div class="msg-pro-pic">
                                        <!-- <i class="fa fa-user"></i> -->
                                        <img src="<?php echo base_url("assets/profile_pics/$user_pic"); ?>"/>
                                    </div>
                                    <div class="msg-text">
                                        <h4 class="usr-name">
                                        <?php 
                                        if($msg->user_type == "Professional"){                                          
                                          if(!empty($msg->bs_name))
                                            echo ucwords($msg->bs_name);
                                          else
                                            echo ucwords($msg->user_firstname." ".$msg->user_lastname);                                          
                                        }
                                        else
                                          echo ucwords($msg->user_firstname." ".$msg->user_lastname);
                                        ?> 
                                        <?php if($msg->unread > 0) echo "<span class='unread-msg'>".$msg->unread."</span>"; ?> &nbsp; <?php if($msg->is_online) echo '<i class="fa fa-circle online"></i>'; ?></h4>
                                        <p class="msg-show">
                                          <?php 
                                          //echo $msg->msg_content;
                                          if($lstmsg->msg_reciever == $this->session->userdata("user_id"))
                                            echo '<i class="fa fa-arrow-left"></i> ';
                                          else
                                            echo '<i class="fa fa-arrow-right"></i> ';                                          
                                          ?>
                                          <?php
                                          echo $lstmsg->msg_content;
                                          ?>
                                        </p>
                                    </div>
                                    <div class="msg-button-date">
                                        <span class="msg-date">
                                            <?php echo date("m/d/Y H:i a", strtotime($lstmsg->msg_date)); ?>
                                        </span>
                                        <span class="btn-msg">
                                            <?php
                                            // if($msg->is_online){
                                            //   ?>
                                              <!-- <button data-toggle="modal" class="btn btn-default online-chat" modal-aria="<?php echo $msg->user_id; ?>">View</button> -->
                                              <?php
                                            // }else{
                                              ?>
                                              <!-- <button data-toggle="modal" data-target="#chat-modal" class="btn btn-default usermessges" modal-aria="<?php echo $msg->user_id; ?>">View</button> -->
                                              <?php
                                            // }
                                            ?>
                                            <button data-toggle="modal" data-target="#chat-modal" class="btn btn-default usermessges" modal-aria="<?php echo $msg->user_id; ?>"><?php echo $labels['label71']; ?></button>                                            
                                        </span>
                                    </div>
                                </div>
                              <?php
                              }
                            }else{
                              ?>
                              <div class="msg-sub-list">
                                <h4 class="noti-sub"><?php echo $labels['label72']; ?></h4>
                              </div>
                              <?php
                            }
                            ?>                                
                            </div>
                          </div>
                          <div class="tab-pane <?php if($this->uri->segment(3) == "notifications") echo "active"; ?>" id="tab3-sub2">
                            <div class="mes-notification">
                              <?php
                              if(!empty($notificationslisting)){
                                foreach($notificationslisting as $nt){
                                  ?>
                                  <a href="<?php echo base_url('Users/notiUpdate/').$nt['nt_id']; ?>" target="_blank" class="notilink <?php if($nt['nt_read']== 1) echo "read-this"; ?>">
                                    <div class="noti-sub">
                                      <span class="user-noti-link">
                                        <?php 
                                        if($nt['user_type'] == "Professional"){                                          
                                          if(!empty($nt['bs_name']))
                                            echo ucwords($nt['bs_name']);
                                          else
                                            echo ucwords($nt['user_firstname']." ".$nt['user_lastname']);                                          
                                        }
                                        else
                                          echo ucwords($nt['user_firstname']." ".$nt['user_lastname']);
                                        ?></span> 
                                      <?php
                                      if($nt['nt_type'] == 1)
                                        echo $labels['label73'];
                                      if($nt['nt_type'] == 2)
                                        echo $labels['label74'];
                                      if($nt['nt_type'] == 3)
                                        echo $labels['label75'];
                                      ?>
                                      <span class="date-time">
                                          <span class="calender-not"><i class="fa fa-calendar"></i><?php echo date("d/m/Y", strtotime($nt['nt_date'])); ?></span>
                                          <span class="clock-not"><i class="fa fa-clock-o"></i><?php echo date("H:i", strtotime($nt['nt_date'])); ?></span>
                                      </span>
                                    </div>
                                  </a>
                                  <?php
                                }
                              }else{
                                ?>
                                <h4 class="noti-sub">
                                  <?php echo $labels['label76']; ?>
                                </h4>
                                <?php
                              }
                              ?>                                
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              if($user['user_type'] == "Professional"){
              ?>
              <div class="tab-pane <?php if($this->uri->segment(3) == "messoptions") echo "active"; ?>" id="tab4">
                <div class="panel panel-primary">
                  <div class="row row-table">
                    <div class="col-md-3 col-sm-4 bg-col">
                      <div class="dash-link-sub">
                        <ul class="nav panel-tabs5">
                          <li class="active"><a href="#tab4-sub1" data-toggle="tab">Acheter une options</a></li>
                          <li><a href="#tab4-sub2" data-toggle="tab">Reciept and Document</a></li>
                          <li><a id="optionredirect" href="#tab4-sub3" data-toggle="tab">Gestions des options</a></li> 
                        </ul>
                        <div class="side-txt-tab">
                          <div class="logo-paymnt">
                            <div class="main-pay">
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-visa.jpg"); ?>"></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-ca_e.jpg"); ?>"></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-cb.jpg"); ?>"></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-3dscan.jpg"); ?>"></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-master.jpg"); ?>"></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-sli-pay.jpg"); ?>"></a>
                                </div>
                              </div>
                              <div class="col-md-6">
                                <div class="logo-paymnt-sub">
                                  <a href="#"><img src="<?php echo base_url("assets/img/card-b.jpg"); ?>"></a>
                                </div>
                              </div>
                            </div>
                          </div>
                            <p>Otourdemoi.Pro s'appuie sur les dernières normes de sécurité et ne dispose à aucun moment de vos informations bancaires.</p>
                             

                            <p>Otourdemoi.Pro ne collecte pas vos données bancaires et a choisi des partenaires, établissements bancaires et de paiement, reconnus et assurant un niveau de sécurité fort en conformité avec la réglementation Française.</p>
                             
                            <p>La transaction s'effectue par paiement sécurisé standard SSL de nos partenaires, CREDIT AGRICOLE et Slimpay, établissements agréés par la banque de France.</p>
                        </div>
                      </div>
                    </div>

                    <div class="col-md-9 col-sm-8 bg-col">
                      <?php
                      $cartavailable = array();
                      if($cartcnt = $this->cart->total_items() > 0){
                        foreach($this->cart->contents() as $items){
                          $cartavailable[] = $items['id'];
                        }
                      }                      
                      ?>
                      <div class="panel-body panel-bodyy">
                        <div class="tab-content">
                          <div class="tab-pane active" id="tab4-sub1">
                            <div class="mes-opt-one">
                              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">                                
                                <div class="panel panel-default">
                                  <?php
                                  $purchasedopt = $this->User->get_userPurchasedOptionByType(1);
                                  if($purchasedopt->num_rows() > 0 ){
                                  $purchs = 1;
                                  foreach($purchasedopt->result() as $opt){
                                  $pur_duration = $opt->otp_option_duration;
                                  $purstatus = $opt->opt_option_status;
                                  $pystatus = $opt->pyt_txn_status;
                                  }
                                  }else{
                                  $purchs = 0;
                                  }
                                  ?>
                                  <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <input type="checkbox" disabled="true" value="None" id="accept-terms1" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?>>
                                      <label for="accept-terms1"></label>
                                      <!-- <label><input type="checkbox" disabled="true" value="" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?> /></label> -->
                                    </div>
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                      <?php echo $mesoption1['opt_name']; ?>
                                    </a>
                                    </h4>
                                  </div>
                                  <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <div class="abo-pre-txt">
                                          <p><?php echo $mesoption1['opt_description']; ?></p>
                                          <h4>Option Pricing</h4>
                                          <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                            <input type="hidden" name="opt_id" value="<?php echo $mesoption1['opt_id']; ?>">
                                            <?php
                                            $price_options = $this->User->getMesOptionsPriceList($mesoption1['opt_id']);
                                            foreach ($price_options as $prows) {
                                            ?>
                                            <input type="radio" class="" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" <?php if($purchs && $pystatus == "Completed"){ if($prows['opt_price_type'] == $pur_duration) echo "checked"; } ?>> <?php echo $prows['opt_text']."(Price: $".$prows['opt_price']." )"; ?><br>
                                            <?php
                                            if($prows['opt_price_type'] == "1 Day"){
                                            ?>
                                            <div class="singleoption"></div>
                                            <div class="totalprice"></div>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            }
                                            ?>
                                            <p>
                                              <div class="errorplacements"></div>
                                              <?php
                                              if($purchs){
                                              if($pystatus == "Completed"){
                                              if($purstatus == 0){
                                              ?>
                                              <a href="javascript:;" class="optionredirect">Your option is not active. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a>
                                              <?php
                                              }else if($purstatus == 1){
                                              ?>
                                              <button type="button" class="btn btn-act btn-default btn-sm optionredirect"><i class="fa fa-thumbs-up"></i> Activated</button>
                                              <?php
                                              }
                                              }else{
                                              if($this->cart->total_items() == 0){
                                              ?>
                                              <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                              <?php
                                              }
                                              ?>
                                              <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption1['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                              <?php
                                              }
                                              ?>
                                              <?php
                                              }else{
                                              if($this->cart->total_items() == 0){
                                              ?>
                                              <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                              <?php
                                              }
                                              ?>
                                              <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption1['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                              <?php
                                              }
                                              ?>
                                            </p>
                                          </form>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <?php
                                  $purchasedopt = $this->User->get_userPurchasedOptionByType(2);
                                  if($purchasedopt->num_rows() > 0 ){
                                  $purchs = 1;
                                  foreach($purchasedopt->result() as $opt){
                                  $pur_duration = $opt->otp_option_duration;
                                  $purstatus = $opt->opt_option_status;
                                  $pystatus = $opt->pyt_txn_status;
                                  }
                                  }else{
                                  $purchs = 0;
                                  }
                                  ?>
                                  <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <input type="checkbox" disabled="true" value="None" id="accept-terms2" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?>>
                                      <label for="accept-terms2"></label>
                                      <!-- <label><input type="checkbox" disabled="true" value="" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?> /></label> -->
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"><?php echo $mesoption2['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <?php echo $mesoption2['opt_description']; ?>
                                        <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <input type="hidden" name="opt_id" value="<?php echo $mesoption2['opt_id']; ?>">
                                          <?php
                                          $price_options = $this->User->getMesOptionsPriceList($mesoption2['opt_id']);
                                          foreach ($price_options as $prows) {
                                          ?>
                                          <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" <?php if($purchs && $pystatus == "Completed"){ if($prows['opt_price_type'] == $pur_duration) echo "checked"; } ?>> <?php echo $prows['opt_text']."(Price: $".$prows['opt_price']." )"; ?><br>
                                          <?php
                                          if($prows['opt_price_type'] == "1 Day"){
                                          ?>
                                          <div class="singleoption"></div>
                                          <div class="totalprice"></div>
                                          <?php
                                          }
                                          ?>
                                          <?php
                                          }
                                          ?>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <?php
                                            if($purchs){
                                            if($pystatus == "Completed"){
                                            if($purstatus == 0){
                                            ?>
                                            <a href="javascript:;"  class="optionredirect">Your option is not active. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a>
                                            <?php
                                            }else if($purstatus == 1){
                                            ?>
                                            <button type="button" class="btn btn-act btn-default btn-sm optionredirect"><i class="fa fa-thumbs-up"></i> Activated</button>
                                            <?php
                                            }
                                            }else{
                                            
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption2['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <!-- <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> Buy This Option</button>
                                            <button type="button" class="addcart">  <i class="fa fa-cart-plus"></i> Add To Cart</button> -->
                                            <?php
                                            }
                                            
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption2['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <!-- <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> Buy This Option</button>
                                            <button type="button" class="addcart">  <i class="fa fa-cart-plus"></i> Add To Cart</button> -->
                                            <?php
                                            }
                                            ?>
                                          </p>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <?php
                                  $purchasedopt = $this->User->get_userPurchasedOptionByType(3);
                                  if($purchasedopt->num_rows() > 0 ){
                                  $purchs = 1;
                                  foreach($purchasedopt->result() as $opt){
                                  $pur_duration = $opt->otp_option_duration;
                                  $purstatus = $opt->opt_option_status;
                                  $pystatus = $opt->pyt_txn_status;
                                  }
                                  }else{
                                  $purchs = 0;
                                  }
                                  ?>
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <input type="checkbox" disabled="true" value="None" id="accept-terms3" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?>>
                                      <label for="accept-terms3"></label>
                                      <!-- <label><input type="checkbox" disabled="true" value="" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?> /></label> -->
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"><?php echo $mesoption3['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <?php echo $mesoption3['opt_description']; ?>
                                        <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <input type="hidden" name="opt_id" value="<?php echo $mesoption3['opt_id']; ?>">
                                          <?php
                                          $price_options = $this->User->getMesOptionsPriceList($mesoption3['opt_id']);
                                          foreach ($price_options as $prows) {
                                          ?>
                                          <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" <?php if($purchs && $pystatus == "Completed"){ if($prows['opt_price_type'] == $pur_duration) echo "checked"; } ?>> <?php echo $prows['opt_text']."(Price: $".$prows['opt_price']." )"; ?><br>
                                          <?php
                                          if($prows['opt_price_type'] == "1 Day"){
                                          ?>
                                          <div class="singleoption"></div>
                                          <div class="totalprice"></div>
                                          <?php
                                          }
                                          ?>
                                          <?php
                                          }
                                          ?>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <?php
                                            if($purchs){
                                            if($pystatus == "Completed"){
                                            if($purstatus == 0){
                                            ?>
                                            <a href="javascript:;"  class="optionredirect">Your option is not active. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a>
                                            <?php
                                            }else if($purstatus == 1){
                                            ?>
                                            <button type="button" class="btn btn-act btn-default btn-sm optionredirect"><i class="fa fa-thumbs-up"></i> Activated</button>
                                            <?php
                                            }
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption3['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption3['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                          </p>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <?php
                                  $purchasedopt = $this->User->get_userPurchasedOptionByType(4);
                                  if($purchasedopt->num_rows() > 0 ){
                                  $purchs = 1;
                                  foreach($purchasedopt->result() as $opt){
                                  $pur_duration = $opt->otp_option_duration;
                                  $purstatus = $opt->opt_option_status;
                                  $pystatus = $opt->pyt_txn_status;
                                  }
                                  }else{
                                  $purchs = 0;
                                  }
                                  ?>
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <input type="checkbox" disabled="true" value="None" id="accept-terms4" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?>>
                                      <label for="accept-terms4"></label>
                                      <!-- <label><input type="checkbox" disabled="true" value="" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?> /></label> -->
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseThree"><?php echo $mesoption4['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <?php echo $mesoption4['opt_description']; ?>
                                        <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <input type="hidden" name="opt_id" value="<?php echo $mesoption4['opt_id']; ?>">
                                          <?php
                                          $price_options = $this->User->getMesOptionsPriceList($mesoption4['opt_id']);
                                          foreach ($price_options as $prows) {
                                          ?>
                                          <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" <?php if($purchs && $pystatus == "Completed"){ if($prows['opt_price_type'] == $pur_duration) echo "checked"; } ?>> <?php echo $prows['opt_text']."(Price: $".$prows['opt_price']." )"; ?><br>
                                          <?php
                                          }
                                          ?>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <?php
                                            if($purchs){
                                            if($pystatus == "Completed"){
                                            if($purstatus == 0){
                                            ?>
                                            <a href="javascript:;"  class="optionredirect">Your option is not active. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a>
                                            <?php
                                            }else if($purstatus == 1){
                                            ?>
                                            <button type="button" class="btn btn-act btn-default btn-sm optionredirect"><i class="fa fa-thumbs-up"></i> Activated</button>
                                            <?php
                                            }
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption4['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption4['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                          </p>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <?php
                                  $purchasedopt = $this->User->get_userPurchasedOptionByType(5);
                                  if($purchasedopt->num_rows() > 0 ){
                                  $purchs = 1;
                                  foreach($purchasedopt->result() as $opt){
                                  $pur_duration = $opt->otp_option_duration;
                                  $purstatus = $opt->opt_option_status;
                                  $pystatus = $opt->pyt_txn_status;
                                  }
                                  }else{
                                  $purchs = 0;
                                  }
                                  ?>
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <input type="checkbox" disabled="true" value="None" id="accept-terms5" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?>>
                                      <label for="accept-terms5"></label>
                                      <!-- <label><input type="checkbox" disabled="true" value="" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?> /></label> -->
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive"><?php echo $mesoption5['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <?php echo $mesoption5['opt_description']; ?>
                                        <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <input type="hidden" name="opt_id" value="<?php echo $mesoption5['opt_id']; ?>">
                                          <?php
                                          $price_options = $this->User->getMesOptionsPriceList($mesoption5['opt_id']);
                                          foreach ($price_options as $prows) {
                                          ?>
                                          <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" <?php if($purchs && $pystatus == "Completed"){ if($prows['opt_price_type'] == $pur_duration) echo "checked"; } ?>> <?php echo $prows['opt_text']."(Price: $".$prows['opt_price']." )"; ?><br>
                                          <?php
                                          if($prows['opt_price_type'] == "1 Day"){
                                          ?>
                                          <div class="singleoption"></div>
                                          <div class="totalprice"></div>
                                          <?php
                                          }
                                          ?>
                                          <?php
                                          }
                                          ?>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <?php
                                            if($purchs){
                                            if($pystatus == "Completed"){
                                            if($purstatus == 0){
                                            ?>
                                            <a href="javascript:;"  class="optionredirect">Your option is not active. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a>
                                            <?php
                                            }else if($purstatus == 1){
                                            ?>
                                            <button type="button" class="btn btn-act btn-default btn-sm optionredirect"><i class="fa fa-thumbs-up"></i> Activated</button>
                                            <?php
                                            }
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption5['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption5['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                          </p>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <?php
                                  $purchasedopt = $this->User->get_userPurchasedOptionByType(6);
                                  if($purchasedopt->num_rows() > 0 ){
                                  $purchs = 1;
                                  foreach($purchasedopt->result() as $opt){
                                  $pur_duration = $opt->otp_option_duration;
                                  $purstatus = $opt->opt_option_status;
                                  $pystatus = $opt->pyt_txn_status;
                                  $qty = $opt->otp_option_qnty;
                                  }
                                  }else{
                                  $purchs = 0;
                                  }
                                  ?>
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <input type="checkbox" disabled="true" value="None" id="accept-terms6" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?>>
                                      <label for="accept-terms6"></label>
                                      <!-- <label><input type="checkbox" disabled="true" value="" <?php if($purchs){ if($purstatus == 1) echo "checked"; } ?> /></label> -->
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix"><?php echo $mesoption6['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <?php echo $mesoption6['opt_description']; ?>
                                        <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <input type="hidden" name="opt_id" value="<?php echo $mesoption6['opt_id']; ?>">
                                          <?php
                                          $price_options = $this->User->getMesOptionsPriceList($mesoption6['opt_id']);
                                          foreach ($price_options as $prows) {
                                          ?>
                                          <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" <?php if($purchs && $pystatus == "Completed"){ if($prows['opt_qnty'] == $qty) echo "checked"; } ?>> <?php echo $prows['opt_text']."( Price: $".$prows['opt_price']." )"; ?><br>
                                          <?php
                                          }
                                          ?>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <?php
                                            if($purchs){
                                            if($pystatus == "Completed"){
                                            if($purstatus == 0){
                                            ?>
                                            <a href="javascript:;"  class="optionredirect">Your option is not active. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a>
                                            <?php
                                            }else if($purstatus == 1){
                                            ?>
                                            <button type="button" class="btn btn-act btn-default btn-sm optionredirect"><i class="fa fa-thumbs-up"></i> Activated</button>
                                            <?php
                                            }
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption6['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <?php
                                            }else{
                                            if($this->cart->total_items() == 0){
                                            ?>
                                            <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                            <?php
                                            }
                                            ?>
                                            <button type="button" class="addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption6['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                            <?php
                                            }
                                            ?>
                                          </p>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <label><input type="checkbox" value=""></label>
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEight" aria-expanded="false" aria-controls="collapseEight"><?php echo $mesoption7['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseEight" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEight">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <p><?php echo $mesoption7['opt_description']; ?></p>
                                        <div class="sub-txt">
                                          <?php
                                          $checkevents = $this->User->getMyFreeEvents();
                                          ?>
                                          <button class="acti-valid btn btn-default" id="av-btn" <?php if(!empty($checkevents) || count($checkevents) > 0){ echo "disabled='true'"; } ?> ><?php echo $labels['label80']; ?></button>
                                          <button class="acti-valid1 btn btn-default" id="av-btn1" <?php if(empty($checkevents) || count($checkevents) == 0){ echo "style=display:none;"; } ?>><?php echo $labels['label81']; ?></button>
                                          <div class="optionplacemsg"></div>
                                          <div class="mes-info-sec acti-valid-div" style="display: none;">
                                            <h4>Option Pricing</h4>
                                            <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                              <div class="choice-radio">
                                                <?php
                                                $price_options = $this->User->getMesOptionsPriceList($mesoption7['opt_id']);
                                                foreach ($price_options as $prows) {
                                                ?>
                                                <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" > <?php echo $prows['opt_text']."( Price: $".$prows['opt_price']." )"; ?><br>
                                                <?php
                                                if($prows['opt_price_type'] == "1 Day"){
                                                ?>
                                                <div class="singleoption"></div>
                                                <div class="totalprice"></div>
                                                <?php
                                                }
                                                }
                                                ?>
                                              </div>
                                              <p>
                                                <div class="errorplacements"></div>
                                                <div class="row">
                                                  <div class="col-md-12">
                                                    <input type="hidden" name="opt_id" value="<?php echo $mesoption7['opt_id']; ?>">
                                                    <?php
                                                    if($this->cart->total_items() == 0){
                                                    ?>
                                                    <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                                    <?php
                                                    }
                                                    ?>
                                                    <button type="button" class="btn btn-default addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption7['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                                    <button type="button" class="btn btn-default cancelevent"> <i class="fa fa-times"></i> Close</button>
                                                  </div>
                                                </div>
                                              </p>
                                            </form>
                                          </div>                                         
                                          
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <label><input type="checkbox" value=""></label>
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine"><?php echo $mesoption8['opt_name']; ?></a> </h4>
                                  </div>
                                  <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <?php echo $mesoption8['opt_description']; ?>
                                        <?php
                                        // if($purchs){
                                        //   if($pystatus == "Completed"){
                                        //     if($purstatus == 0){
                                        //       ?>
                                        <!-- <div><a href="javascript:;"  class="optionredirect">Created event not active for the good place. Please check you email and activate the option OR Activate Your Option Plane (Click here)</a></div> -->
                                        //       <?php
                                        //     }
                                        //   }
                                        // }
                                        ?>
                                        <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <div class="choice-radio">
                                            <?php
                                            $price_options = $this->User->getMesOptionsPriceList($mesoption8['opt_id']);
                                            foreach ($price_options as $prows) {
                                            ?>
                                            <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" > <?php echo $prows['opt_text']."( Price: $".$prows['opt_price']." )"; ?><br>
                                            <?php
                                            if($prows['opt_price_type'] == "1 Day"){
                                            ?>
                                            <div class="singleoption"></div>
                                            <div class="totalprice"></div>
                                            <?php
                                            }
                                            }
                                            ?>
                                          </div>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <input type="hidden" name="opt_id" value="<?php echo $mesoption8['opt_id']; ?>">
                                                <?php
                                                if($this->cart->total_items() == 0){
                                                ?>
                                                <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                                <?php
                                                }
                                                ?>
                                                <button type="button" class="btn btn-default addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption8['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                              </div>
                                            </div>
                                          </p>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                </div>
      <!-- Start Here -->
      <?php
      $tabindx=0;
          foreach ($mesoption9 as $mesoption9row) {
            $tabindx++;
           ?>
                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <label><input type="checkbox" value=""></label>
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen<?php echo $tabindx; ?>" aria-expanded="false" aria-controls="collapseTen<?php echo $tabindx; ?>"><?php echo $mesoption9row['opt_name']; ?></a>
                                    </h4>
                                  </div>
                                  <div id="collapseTen<?php echo $tabindx; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <p><?php echo $mesoption9row['opt_description']; ?></p>
                                        <div class="sub-txt">
                                          
                                          <!-- Option Form will appear here.-->
                                          <form name="option9form<?php echo $tabindx; ?>" method="POST" action="<?php echo base_url("Users/formOption");?>">
                                            <div class="row bottom-form">
                                              <div class="col-sm-6">
                                                <div class="form-group">
                                                  <label>First Name:</label>
                                                  <input type="text" name="txtFirstName" id="txtFirstName" value="<?php echo $user['user_firstname']; ?>">
                                                  <input type="hidden" name="opt_id" value="<?php echo $mesoption9row['opt_id']; ?>">
                                                  <input type="hidden" name="opt_type" value="2">
                                                </div>
                                              </div>

                                              <div class="col-sm-6">
                                                <div class="form-group">
                                                  <label>Last Name:</label>
                                                  <input type="text" name="txtLastName" id="txtLastName" value="<?php echo   $user['user_lastname']; ?>">
                                                  <input type="hidden" name="emailTo" id="emailTo" value="<?php echo   $user['user_email']; ?>">
                                                </div>
                                              </div>
                                            </div>                                              
                                              
                                            <div class="row bottom-form">
                                              <div class="col-md-6">
                                                <div class="form-group">
                                                  <label>Gender</label>
                                                  <div class="for-radio">
                                                    <div class="btn-group" data-toggle="buttons">                                                      
                                                      <label class="btn btn-default <?php if($user['user_gender'] == "Male" || $user['user_gender'] == "male") echo "active"; ?>">
                                                        <input type="radio" name="douhavekidsprice" <?php if($user['user_gender'] == "Male" || $user['user_gender'] == "male") echo "checked"; ?> value="Male">
                                                        Homme </label>
                                                      <label class="btn btn-default <?php if($user['user_gender'] == "Female" || $user['user_gender'] == "female") echo "active"; ?>">
                                                        <input type="radio" name="douhavekidsprice" <?php if($user['user_gender'] == "Female" || $user['user_gender'] == "female") echo "checked"; ?> value="Female">
                                                        Femme </label>
                                        
                                                    </div>
                                                  </div>
                                                </div>
                                              </div>

                                              <div class="col-sm-6">
                                                <div class="form-group">
                                                  <label>Telephone:</label>
                                                  <input type="text" name="txtTelphone" id="txtTelphone" value="<?php echo $user['user_phone']; ?>">
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row bottom-form">
                                              <div class="col-sm-6">
                                                <div class="form-group">
                                                  <label>Budget:</label>
                                                  <input type="text" name="txtBudget" required id="txtBudget" value="">
                                                </div>
                                              </div>
                                            
                                              <div class="col-sm-6">
                                                <div class="form-group">
                                                  <label>Date:</label>
                                                  <!-- <input type="text" name="txtDate" id="txtDate" value=""> -->
                                                  <input type="text" required="" name="txtDate" class="form-control datepickerfuture" placeholder="Start Date" value="<?php if($user['StartDate'] != "0000-00-00") echo $user['StartDate']; ?>">
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row bottom-form">
                                              <div class="col-sm-12">
                                                <div class="form-group">
                                                  <label>Description:</label>
                                                  <textarea name="txtDescription" id="txtDescription"></textarea>
                                                </div>
                                              </div>
                                            </div>

                                            <div class="row bottom-form">
                                              <div class="col-sm-6">
                                                <div class="form-group">
                                                  <input type="submit" class="sub-btn" name="btnformsubmit" value="Save"><span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>
                                                  <div id="option9msg"></div>
                                                </div>
                                              </div>
                                            </div>
                                          </form>                                          
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                <?php
   # code...
  }
                                ?>
<!-- end here -->

                                <div class="panel panel-default">
                                  <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <div class="checkbox">
                                      <label><input type="checkbox" value=""></label>
                                    </div>
                                    <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseEleven" aria-expanded="false" aria-controls="collapseEleven">
                                    <?php echo $mesoption10['opt_name']; ?></a> </h4>
                                  </div>

                                  <div id="collapseEleven" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingEleven">
                                    <div class="panel-body">
                                      <div class="abo-pre-txt">
                                        <p><?php echo $mesoption10['opt_description']; ?></p>
                                        <div class="sub-txt">          
                                          <h4>Option Pricing</h4>
                                        <form name="optionPurchaseForm" method="post" action="<?php echo base_url("Users/headOfListsOptions");?>">
                                          <div class="choice-radio">
                                            <?php
                                            $price_options = $this->User->getMesOptionsPriceList($mesoption10['opt_id']);
                                            foreach ($price_options as $prows) {
                                            ?>
                                            <input type="radio" class="" atr-val="<?php if($prows['opt_price_type'] == "1 Day") echo "single"; ?>" name="price_type" value="<?php echo $prows['opt_price_id']; ?>" > <?php echo $prows['opt_text']." = $".$prows['opt_price']; ?><br>
                                            <?php
                                            if($prows['opt_price_type'] == "1 Day"){
                                            ?>
                                            <div class="singleoption"></div>
                                            <div class="totalprice"></div>
                                            <?php
                                            }
                                            }
                                            ?>
                                          </div>
                                          <p>
                                            <div class="errorplacements"></div>
                                            <div class="row">
                                              <div class="col-md-12">
                                                <input type="hidden" name="opt_id" value="<?php echo $mesoption10['opt_id']; ?>">
                                                <?php
                                                if($this->cart->total_items() == 0){
                                                ?>
                                                <button type="submit" name="optionPurchaseSubmit" class="buyopt"> <i class="fa fa-money"></i> <?php echo $labels['label77']; ?></button>
                                                <?php
                                                }
                                                ?>
                                                <button type="button" class="btn btn-default addcart"> <?php if($this->cart->total_items() > 0  && in_array("sku_".$mesoption10['opt_id'], $cartavailable)) echo '<i class="fa fa-check"></i> Added To Cart'; else echo '<i class="fa fa-cart-plus"></i> Add To Cart'; ?></button>
                                              </div>
                                            </div>
                                          </p>
                                        </form>     
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                              </div>
                            </div>
                          </div>
                        

                          <div class="tab-pane" id="tab4-sub2">
                            <div class="mes-opt-two">
                              <div class="select-form">
                                <select id="optionlist" name="optionlist" class="form-control">
                                  <option value="0">Select Option</option>
                                  <option value="1">Profil plus</option>
                                  <option value="2" selected="selected">Visibilité</option>
                                  <option value="3">Tête de liste</option>
                                  <option value="4">A la Une</option>
                                  <option value="5">Pack Photos</option>
                                  <option value="6">Events</option>
                                  <option value="8">Encart publicitaire dans votre ville (Page d'acceuil)</option>
                                  <option value="9">Bon Plan</option> <option value="10">Bon Plan Plus</option>
                                  <option value="11">Exclusivité - Touch &amp; Go – Publicité sur mobiles géolocalisés en temps réel </option>
                                  <option value="12">Envoi de SMS publicitaires à des clients géolocalisés à 200M de votre point de vente (en partenariat avec SFR)</option>
                                  <option value="13">Pack Sms Géociblé</option>
                                  <option value="14">Pack Email B2B</option>
                                  <option value="16">Pack Email B2C</option>
                                  <option value="18">Pack de création graphique</option>
                                  <option value="19">A la Une Plus</option>
                                  <option value="20">Publicité sur internet</option>
                                  <option value="21">Publicité sur Facebook</option>
                                </select>
                              </div>
                              <div class="content-select">
                                
                                <table>
                                  <tr>
                                    <th class="head-cs">Option</th>
                                    <th class="head-cs">Date of purchase</th>
                                    <th class="head-cs">Facture</th>
                                  </tr>
                                  <tr class="cs-cnt">
                                    <td>Dummy Content</td>
                                    <td>Dummy Content</td>
                                    <td>Dummy Content</td>
                                  </tr>
                                </table>
                              </div>
                            </div>
                          </div>

                          <div class="tab-pane" id="tab4-sub3">
                            <div class="mes-opt-thre">
                              <div class="facture-sec">                             
                                <div class="table-responsive">
                                  <table class="table">
                                    <tr>
                                      <th class="head-cs"><?php echo $labels['label82']; ?></th>
                                      <th class="head-cs"><?php echo $labels['label83']; ?></th>
                                      <th class="head-cs"><?php echo $labels['label84']; ?></th>
                                      <th class="head-cs"><?php echo $labels['label85']; ?></th>
                                      <!-- <th class="head-cs">Payment</th> -->
                                      <th class="head-cs"><?php echo $labels['label86']; ?></th>
                                    </tr>
                                    <tbody class="option-content">
                                      <?php 
                                      if(!empty($user_purchasd_options)){
                                        $freeevent = $this->User->getMyFreeEvents();
                                        if(!empty($freeevent) && count($freeevent) > 0 ){
                                          foreach($freeevent as $evt){
                                            $evt_id = $evt['event_id'];
                                          }
                                          $evtcheck = 1;
                                        }else{
                                          $evtcheck = 0;
                                        }
                                        foreach ($user_purchasd_options as $prows) 
                                        {                                  
                                        ?>
                                          <tr class="cs-cnt">
                                            <td><?php echo $prows['opt_name']; ?></td><td><?php echo $prows['opt_option_purchase_date']; ?></td>
                                            <td class='optstatus'>
                                            <?php 
                                              //echo $prows['opt_option_status'] == 1? 'Yes': 'No';
                                              if($prows['opt_option_status'] == 1)
                                                echo "Yes";
                                              else if($prows['opt_option_status'] == 3){
                                                echo "Expired";
                                              }else{
                                                echo "No";
                                              }
                                            ?>
                                            </td>
                                            <td><?php 
                                              if($prows['otp_option_duration'] == "1 Day"){
                                                $date1 = date_create($prows['opt_option_active_date']);
                                                $date2 = date_create($prows['opt_option_end_date']);
                                                $difference = date_diff($date1,$date2);
                                                $diff = $difference->format('%a');
                                                if($diff > 0 )
                                                  echo $diff." Days";
                                                else
                                                  echo "1 Day";
                                              }else{                                                                                             
                                                if($prows['opt_option_id'] == 7 && $evtcheck == 1){
                                                  if($prows['opt_event_id'] == $evt_id){
                                                    ?>
                                                    <span class="fevt">Free Event</span>
                                                    <?php
                                                  }else{
                                                    echo $prows['otp_option_duration'];
                                                  }
                                                }else{
                                                  echo $prows['otp_option_duration'];
                                                }
                                              } ?></td>
                                            
                                            <td>
                                              <?php
                                              if($prows['pyt_txn_status'] == "Completed"){
                                                if($prows['opt_option_status'] == 1){
                                                  ?>
                                                  <div class="optionaction">
                                                    <button class="btn btn-xs btn-default tooltip"><i class="fa fa-thumbs-up"></i> <span class="tooltiptext">Activated</span></button>
                                                    <?php
                                                    if($prows['opt_option_id'] == 7 || $prows['opt_option_id'] == 8){
                                                      if($prows['opt_option_id'] == 7 && $evtcheck == 1){
                                                        if($prows['opt_event_id'] == $evt_id){
                                                          ?>
                                                          <a href="<?php echo base_url("Users/editEvent/").$prows['opt_event_id']; ?>" target="_blank" class="btn btn-xs btn-success tooltip"><i class="fa fa-pencil"></i> <span class="tooltiptext"><?php echo $labels['label88']; ?></span></a>
                                                          <?php
                                                        }
                                                      }
                                                      ?>
                                                      <a href="<?php echo base_url("Users/myEvents/").$prows['opt_event_id']; ?>" target="_blank" class="btn btn-xs btn-info tooltip"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></a>
                                                      <button class="btn btn-xs btn-danger dtevent tooltip" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-trash-o"></i> <span class="tooltiptext"><?php echo $labels['label90']; ?></span></button>
                                                      <?php
                                                    }else{
                                                    ?>
                                                    <button class="btn btn-xs btn-info purcdetails tooltip" data-toggle="modal" data-target="#purchasedetails" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></button>
                                                    <?php
                                                    }
                                                    ?>
                                                    <button class="btn btn-xs btn-warning prtrecpt tooltip" atr-desc="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-print"></i> <span class="tooltiptext"><?php echo $labels['label91']; ?></span></button>
                                                  </div>
                                                  <?php
                                                }else if($prows['opt_option_status'] == 3){
                                                  ?>
                                                  <div class="optionaction">
                                                    <button class="btn btn-xs btn-danger tooltip"><i class="fa fa-times"></i> <span class="tooltiptext"><?php echo $labels['label92']; ?></span></button>
                                                    <?php
                                                    if($prows['opt_option_id'] == 7 || $prows['opt_option_id'] == 8){
                                                      ?>
                                                      <a href="<?php echo base_url("Users/myEvents/").$prows['opt_event_id']; ?>" target="_blank" class="btn btn-xs btn-info tooltip"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></a>
                                                      <button class="btn btn-xs btn-danger dtevent tooltip" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-trash-o"></i> <span class="tooltiptext"><?php echo $labels['label90']; ?></span></button>
                                                      <?php
                                                    }else{
                                                    ?>
                                                    <button class="btn btn-xs btn-info purcdetails tooltip" data-toggle="modal" data-target="#purchasedetails" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></button>
                                                    <?php
                                                    }
                                                    ?>
                                                    <button class="btn btn-xs btn-warning prtrecpt tooltip" atr-desc="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-print"></i> <span class="tooltiptext"><?php echo $labels['label91']; ?></span></button>
                                                  </div>
                                                  <?php
                                                }else{
                                                ?>
                                                  <div class="optionaction">
                                                    <?php
                                                    if($prows['opt_option_id'] == 7 || $prows['opt_option_id'] == 8){
                                                      ?>
                                                      <button class="btn btn-success btn-xs tooltip event1" data-toggle="modal" data-target="#createEventmodal" details="<?php echo $prows['opt_user_option_id']; ?>" list-id="<?php echo $prows['opt_event_id']; ?>" ><i class="fa fa-edit"></i> <span class="tooltiptext">Create Event</span></button>                       
                                                      <?php
                                                    }else{
                                                    ?>
                                                    <button class="btn btn-success btn-xs activationoption tooltip"><i class="fa fa-edit"></i> <span class="tooltiptext">Activate</span></button>
                                                    <button class="btn btn-xs btn-info purcdetails tooltip" data-toggle="modal" data-target="#purchasedetails" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></button>
                                                    <button class="btn btn-xs btn-warning prtrecpt tooltip" atr-desc="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-print"></i> <span class="tooltiptext"><?php echo $labels['label91']; ?></span></button>
                                                    <?php
                                                    }
                                                    ?>                                                  
                                                  </div>
                                                  <?php
                                                  if($prows['opt_option_id'] != 7 && $prows['opt_option_id'] != 8){
                                                  ?>
                                                  <div class="actform" style="display:none">
                                                    <form name="activaform" method="post" action="<?php echo base_url("Users/account"); ?>">
                                                      <div class="form-group">
                                                        <label class="form-label">Start From:                                                
                                                        </label>                                                    
                                                        <input type="text" readonly name="stdate" class="form-control datepickerfuture" placeholder="Select Start Date">                                                                                                  
                                                        <span class="ermsg"></span>                                                    
                                                      </div>
                                                      <?php
                                                      if($prows['opt_option_id'] == 2){
                                                      ?>
                                                        <div class="form-group">
                                                          <label class="form-label">Visibile For Categories:</label>
                                                          <?php echo $this->User->getCategoriesAndFiltersByName(1); ?>
                                                        </div>
                                                        <div class="form-group">
                                                          <label class="form-label">Visibile For City:</label>
                                                          <?php echo $user['user_company_address']; ?>
                                                        </div>
                                                        <div class="form-group">
                                                          <label class="form-label">City:                                                
                                                          </label>                                                    
                                                          <input type="text" name="city" class="form-control citycomplete" onfocus="geolocate()" placeholder="Enter city">                                                      
                                                        </div>
                                                      <?php
                                                      }
                                                      ?>
                                                      <div class="sbbutton">
                                                        <input type="hidden" value="<?php echo $prows['opt_user_option_id']; ?>" name="useroptions" />
                                                        <button type="submit" name="submit" class="btn btn-success btn-xs">Activate</button>
                                                        <button type="reset" class="btn btn-xs btn-default close-actform"> Cancel</button>
                                                      </div>
                                                    </form>
                                                  </div>
                                                  <?php
                                                }
                                                  ?>
                                                <?php
                                                }
                                              }else{
                                                ?>
                                                <!-- <button class="btn btn-xs btn-warning"><i class="fa fa-spinner fa-pulse"></i> Payment Pending</button> -->
                                                <div class="optionaction">
                                                  <?php
                                                  if($prows['opt_option_id'] == 7 || $prows['opt_option_id'] == 8){
                                                    ?>
                                                    <button class="btn btn-xs btn-info purcdetails tooltip" data-toggle="modal" data-target="#purchasedetails" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-eye"></i> <span class="tooltiptext">View</span></button>
                                                    <!-- <a href="<?php echo base_url("Users/myEvents/").$prows['opt_event_id']; ?>" target="_blank" class="btn btn-xs btn-info tooltip"><i class="fa fa-eye"></i> <span class="tooltiptext">View</span></a> -->
                                                    <button class="btn btn-xs btn-danger dtevent tooltip" data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-trash-o"></i> <span class="tooltiptext">Delete</span></button>
                                                    <?php
                                                  }else{
                                                  ?>
                                                  <button class="btn btn-xs btn-info purcdetails tooltip" data-toggle="modal" data-target="#purchasedetails" details="<?php echo $prows['opt_user_option_id']; ?>"><i class="fa fa-eye"></i> <span class="tooltiptext">View</span></button>
                                                  <?php
                                                  }
                                                  ?>
                                                </div>
                                                <?php
                                              }
                                              ?>                                            
                                            </td>
                                          </tr>
                                        <?php
                                        }
                                      }else{
                                        ?>
                                        <tr class="cs-cnt"><td colspan="6">No option purchased yet.</td></tr>
                                        <?php
                                      }
                                      ?>
                                    </tbody>
                                  </table>
                                  <hr/>
                                  <h3>Static Options</h3>
                                  <table class="table">
                                    <thead>
                                      <tr>
                                        <th>Option Name</th>
                                        <th>Date</th>
                                        <th>Price</th>
                                        <th>View</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(!empty($user_purchased_static_options)){
                                      foreach ($user_purchased_static_options as $stopt){
                                       ?>
                                        <tr>
                                          <td><?php echo $stopt['opt_name']; ?></td>
                                          <td><?php echo $stopt['opt_option_purchase_date']; ?></td>
                                          <td><?php echo $stopt['opt_option_price']; ?></td>
                                          <td><a href="javascript:;" class="btn btn-xs btn-info purcdetails tooltip" data-toggle="modal" data-target="#purchasedetails" details="<?php echo $stopt['opt_user_option_id']; ?>"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></a></td>
                                        </tr>
                                      <?php
                                      }
                                    }else{
                                      ?>
                                      <tr>
                                        <td colspan="4">Sorry, No records available.</td>                                        
                                      </tr>
                                      <?php
                                    }
                                    ?>
                                    </tbody>
                                  </table>
                                  <hr/>
                                  <h3>Form Options</h3>                                  
                                
                                  <table class="table">
                                    <thead>
                                      <tr>
                                        <th>Option Name</th>
                                        <th>Date</th>
                                        <th>Budget</th>
                                        <th>View</th>
                                      </tr>
                                    </thead>
                                    <tbody id="option9list">
                                    <?php
                                    if(!empty($user_purchasd_adv_options)){
                                      foreach ($user_purchasd_adv_options as $rows){
                                       ?>
                                        <tr>
                                          <td><?php echo $rows['opt_name']; ?></td>
                                          <td><?php echo $rows['StartDate']; ?></td>
                                          <td><?php echo $rows['Budget']; ?></td>
                                          <td><a href="<?php echo base_url("Users/myFormOption/").$rows['frm_option_id']; ?>" target="_blank" class="btn btn-xs btn-info tooltip"><i class="fa fa-eye"></i> <span class="tooltiptext"><?php echo $labels['label89']; ?></span></a></td>
                                        </tr>
                                      <?php
                                      }
                                    }else{
                                      ?>
                                      <tr>
                                        <td colspan="4">Sorry, No records available.</td>                                        
                                      </tr>
                                      <?php
                                    }
                                    ?>
                                    </tbody>
                                  </table>
                                </div>
                              </div>
                            </div>
                          </div><!-- Pradeep -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>


<!-- User Chat model -->
<div id="chat-modal" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">        
        <div class="prof-msg-details">
          <div class="row">
            <div class="col-xs-12">
              <div class="msg-pro-pic-sec">
                <div class="msg-profile-pic">
                  <!-- <i class="fa fa-user"></i> -->
                  <img src="http://votivephp.in/VotiveYellowPages/assets/profile_pics/default/default.jpg"/>
                </div>
                <div class="msh-prof-details">
                  <a href="#"><h3 id="msgusername"></h3></a>
                </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body messagebody" data-mcs-theme="dark">
        <div class="usermessageslisting">
        </div>
      </div>
      <div class="modal-footer">
        <div class="row">
          <div class="col-md-12">
            <div class="reply-sec-msg">
              <form method="post" action="" id="sendusermessage">
                <div class="form-group">
                  <!-- <textarea class="form-control mCustomScrollbar" wrap="off" rows="2" id="comment" placeholder="Enter Your Message..." name="usermessage"></textarea> -->
                  <input type="text" class="form-control" id="comment" placeholder="Enter Your Message..." name="usermessage"/>
                </div>
                <input type="hidden" id="tosend" name="tosend" value="" />
                <button type="submit" class="btn btn-default msgsubmit">Reply</button><span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-2x"></i></span>
              </form>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- User Chat Model end -->

<!-- User Chat model -->  
<div id="addphonenumbers" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="prof-msg-details">
          <div class="row">
            <div class="col-xs-12">
              <div class="msg-pro-pic-sec">
                <div class="msh-prof-details">
                  <h3>Add New Number</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <form method="post" action="<?php echo base_url("Users/addNewPhone"); ?>" id="checkavailablenumbers">
        <div class="modal-body review-body messagebody" data-mcs-theme="dark">
          <div class="col-md-12">
            <div class="reply-sec-msg">
              <div class="form-group">
                <label>Number Type</label>
                <input type="text" class="form-control" placeholder="Enter Type" name="phlabel" />
              </div>
              <div class="form-group">
                <label>Phone Number</label>
                <input type="text" class="form-control" placeholder="Enter Numbers" name="phone" />
                <div id="nummsg"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <button type="submit" class="btn btn-default addnm">Add</button>
            <button type="button" class="btn btn-default closeaddnum" data-dismiss="modal">Close</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- User Chat Model end -->

<!-- User Purchase Details model -->  
<div id="purchasedetails" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="prof-msg-details">
          <div class="row">
            <div class="col-xs-12">
              <div class="msg-pro-pic-sec">
                <div class="msh-prof-details">
                  <h3>Purchase Details</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body review-body messagebody" data-mcs-theme="dark">
        <div class="col-md-12">
          <div class="reply-sec-msg purchase-body">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button type="button" class="btn btn-default closeaddnum" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- User Purchase Details Model end -->

<!-- Create Event model-->  
<div id="createEventmodal" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <form id="createevent" method="POST" action="" autocomplete="off">
        <div class="modal-header">
          <div class="prof-msg-details">
            <div class="row">
              <div class="col-xs-12">
                <div class="msg-pro-pic-sec">
                  <div class="msh-prof-details">
                    <h3>Create Event</h3>
                  </div>
                  <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-body review-body" data-mcs-theme="dark">
          <div class="col-sm-12">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label"><?php echo $labels['label35']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label35']; ?></span>
                    </span>
                  </label>
                  <input type="text" name="ename" class="form-control" placeholder="<?php echo $labels['label35']; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label36']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label36']; ?></span>
                    </span>
                  </label>
                  <select name="eventtype" class="form-control">
                    <option value="">Select Type</option>
                    <?php
                    if(!empty($eventTypeList)){
                    foreach($eventTypeList as $evttype){
                    ?>
                    <option value="<?php echo $evttype['evt_type_id']; ?>"><?php echo $evttype['evt_type']; ?></option>
                    <?php
                    }
                    }
                    ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="row publicationdiv"></div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label"><?php echo $labels['label39']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label39']; ?></span>
                    </span>
                  </label>
                  <input type="text" readonly name="esdate" class="form-control datetimepickerfuture" placeholder="<?php echo $labels['label39']; ?>e">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label40']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label40']; ?></span>
                    </span>
                  </label>
                  <input type="text" readonly name="eedate" class="form-control datetimepickerfuture" placeholder="<?php echo $labels['label40']; ?>">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label41']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label41']; ?></span>
                    </span>
                  </label>
                  <input type="number" min="1" name="noperson" class="form-control" placeholder="<?php echo $labels['label41']; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label42']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label42']; ?></span>
                    </span>
                  </label>
                  <input type="text" name="eventprice" class="form-control" placeholder="<?php echo $labels['label42']; ?>" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label43']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label43']; ?></span>
                    </span>
                  </label>
                  <input type="text" name="evtaddress" class="form-control citycomplete" onfocus="geolocate()" placeholder="<?php echo $labels['label43']; ?>" />
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label confm-pass"><?php echo $labels['label59']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label45']; ?></span>
                    </span>
                  </label>
                  <input type="file" multiple="true" name="evtfile[]" id="evtfile" class="form-control" />
                  <div id="evtimgmsg"></div>
                  <div id="evtimgpreview"></div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="form-label"><?php echo $labels['label44']; ?>:
                    <span class="tooltip">?
                      <span class="tooltiptext"><?php echo $suggestion['label44']; ?></span>
                    </span>
                  </label>
                  <textarea class="textarea-main" name="description" placeholder="<?php echo $labels['label44']; ?>"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <div class="form-group">
            <input type="hidden" value="" id="usoption" name="usoption"/>
            <input type="hidden" value="" id="uslist" name="uslist" />
            <button type="submit" id="evtsubmit" class="btn" name="optionPurchaseSubmit"> <?php echo $labels['label60']; ?></button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $labels['label61']; ?></button>
            <span style="display:none;" class="updpsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
            <div class="eventmsg"></div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<!-- Create event model end -->

<!-- User Purchase Details model -->  
<div id="formoptiondetails" class="modal chat-modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <div class="prof-msg-details">
          <div class="row">
            <div class="col-xs-12">
              <div class="msg-pro-pic-sec">
                <div class="msh-prof-details">
                  <h3>Option Details</h3>
                </div>
                <button type="button" class="close" data-dismiss="modal">
                <span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-body review-body messagebody" data-mcs-theme="dark">
        <div class="col-md-12">
          <div class="reply-sec-msg purchase-body">
          <h1>Thank You</h1>
<br>
          <h2>Thank You for Purchasing the Option. Please check your registered email. Our representative will contact you soon.</h2>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <div class="form-group">
          <button type="button" class="btn btn-default closeaddnum" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- User Purchase Details Model end -->





<!-- User Purchase Details model -->  


<div class="modal fade" id="formoption9details" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Thanks</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
        <h1>Thank You</h1>
<br>
          <h3>Thank You for Purchasing the Option. Please check your registered email. Our representative will contact you soon.</h3>
         
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<!-- User Purchase Details Model end -->


