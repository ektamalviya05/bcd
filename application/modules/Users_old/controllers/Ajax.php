<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MX_Controller {
	
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://localhost/mytofah/index.php/user
	        
	 *	- or -
	 * 		http://localhost/mytofah/index.php/user/index
	      
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://localhost/mytofah/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/login/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()// no spaces around parenthesis in function declarations 
	{
		
	    parent::__construct();
		$this->load->library('session');
		$info=$this->session->all_userdata();
		$this->load->model('User');
	    $this->load->library('email');
	    $this->load->library('form_validation');
	    $this->load->helper(array('form', 'url'));
	    
	}
	public function logout(){
    error_reporting(E_ALL);    
      $info = $this->session->all_userdata();

	  $this->session->sess_destroy($info);
	  redirect(base_url());
	  //echo 1;
	}


/////////////////////////////////facebook login/////////////////////////////////////////////

	function fblogin(){
		print_r($this->input->post());

		$con['conditions'] = array(
            'user_email'=>$this->input->post('email'),
            'social_id' =>$this->input->post('id'),
            'status' => '1'
        );
       
        $checkLogin = $this->User->getRows($con);

        if($checkLogin){
            $this->session->set_userdata('isUserLoggedIn',TRUE);
            $this->session->set_userdata('user_id',$checkLogin[0]['user_id']);
           // echo base_url().'Users/account';
          // redirect(base_url().'Users/account');
            echo json_encode(array("flag"=>1));
        }
        else{
        	$data = array("social_id"=>$this->input->post("id"),
        				  "user_firstname"=>$this->input->post("first_name"),
        				  "user_lastname"=>$this->input->post("last_name"),
        				  "user_email"=>$this->input->post("email"),
        				  "user_gender"=>$this->input->post("gender"),
        				)
        	$this->db->insert("users", $data);
        	$user_id = $this->db->insert_id();
        	$this->session->set_userdata('isUserLoggedIn',TRUE);
            $this->session->set_userdata('user_id',$checkLogin[0]['user_id']);
            echo json_encode(array("flag"=>1));
        }
	}


 ////////////////////////////////google Login//////////////////////////////////////////////

     function googlelogin() {

    $id= $this->input->post('id');
    $email= $this->input->post('email');
    $name= $this->input->post('name');

    $data=array('social_id'=>$id);
	$check_email=array('email'=>$email);
	$verfiy = $this->model->get_data('*','tbl_user_master',$data);

   
      if(count($verfiy)>0)
      {
        $session_id =session_id();
       

        $logininfo= array(
          'session_id'=>session_id(),
          'email'=>$email,
          'user_id'=>$verfiy[0]->user_id,
          'siterole'=>2,
          'social_id'=>$verfiy[0]->social_id, 
          'login_type'=>'GP',
          'status'=>$verfiy[0]->status          
          );

        $this->session->set_userdata($logininfo); 
        $info = $this->session->all_userdata();

        // echo $this->db->last_query();
        echo json_encode(array('flag'=>$verfiy[0]->status,'usertype'=>'old user','user_id'=>$verfiy[0]->user_id));
        die;
      }
      else
      {
          $photo= $this->input->post('picture');
          $post_data=array('social_id'=>$id,'email'=>$email,'status'=>1,'login_type'=>'GP','profile_picture'=>$photo,'fname'=>$name);

          $this->db->insert('tbl_user_master', $post_data); 
		  $userid = $this->db->insert_id();
        
         ////for set role in session
          $getuserdate = $this->model->get_data('*','tbl_user_master',array('user_id' => $userid));

          $session_id= session_id();
          $logininfo= array(
            'session_id'=>session_id(),
            'email'=>$email,
            'user_id'=>$userid,
            'social_id'=>$id,
            'siterole'=>2,             
            'login_type'=>'GP',
            'verification_status'=>$getuserdate[0]->status 
            );
          $info = $this->session->set_userdata($logininfo); 

          echo json_encode(array('flag'=>$getuserdate[0]->status,'usertype'=>'new user','user_id'=>$userid));
          die;

      }
  }



    //////////////////////////////////////////////////////////////////////////////





}

	?>