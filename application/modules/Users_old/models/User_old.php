<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User extends CI_Model{
    function __construct() {
        $this->userTbl = 'users';
        $this->userTblSubs = 'subscribers';
        $this->businessTbl = 'otd_business_details';
        $this->priceTbl = "otd_business_pricetable";
        $this->bsopenTbl = "otd_business_openingtime";
        $this->photogal = "otd_user_photogallery";
        $this->boxIdea = "otd_idea_box";
        $this->followTbl = "otd_business_follower";
        $this->langTbl = "otd_languages";
        $this->userbscatTbl = "otd_user_business_category";
        $this->msgTbl = "otd_user_messaging";
        $this->revewTbl = "profile_review";
        $this->notiTbl = "otd_notifications";
        $this->eventTbl = "otd_events";
        $this->reportedRevewTbl = "reported_profile_review";
        $this->phTbl = "otd_user_phone";
        $this->labelTbl = "otd_label_suggestion_list";
        $this->ftTbl = "otd_footer_content";
        $this->pytTbl = "otd_user_option_payment";
        $this->slmrefTbl = "otd_user_slimpayreference";
        $this->usoptMaster = "otd_option_master";
        $this->usoptprice = "otd_option_price";
        $this->usOptTbl = "otd_user_option";
        $this->eventTypeTbl = "otd_event_type";
        $this->emailTpTbl = "otd_email_templates";
        $this->pkgTbl = "otd_package_options";
		$this->userCatTbl = "otd_user_business_category";
        $this->upldDocTbl = "otd_user_purchasedoc";
        $this->docTbl = "otd_contractdoc";
    }

    function getMesOptionDetails($id){
        return $this->db->get_where($this->usoptMaster, array("opt_id"=>$id))->row_array();
        //return $this->db->get_where($this->usoptMaster, array("opt_type"=>2, "opt_status"=>1))->result_array();
    }

    function getMesOptionDetailsStatic(){
       // echo "Doo";
        $qry = $this->db->get_where($this->usoptMaster, array("opt_type"=>3, "opt_status" =>1))->result_array();
        //echo "I am here". $this->db->last_query();
        //echo $this->db->_error_message();
        return $qry;
        //return $this->db->get_where($this->usoptMaster, array("opt_type"=>2, "opt_status"=>1))->result_array();
  

    }
    function getMesOptionFormDetails(){
      
        $qry =$this->db->get_where($this->usoptMaster, array("opt_type"=>2, "opt_status"=>1))->result_array();
       //echo $this->db->last_query();

      
        return $qry;
    }
    

    function getMesOptionsPriceList($type){
       $qry =$this->db->get_where($this->usoptprice, array("opt_type_id"=>$type, "opt_pr_status"=>1))->result_array();
      // echo $this->db->last_query();

       return $qry;
    }

    function get_userPurchasedOptionByType($type, $opt_type=1){
        $ses = $this->session->userdata("user_id");
        return $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id")
                        ->where_in("opt_option_status", array(0,1))
                        ->get_where($this->usOptTbl, array("opt_user_id"=>$ses, "opt_option_id"=>$type, "opt_option_type"=>$opt_type));
    }

    function get_userPurchasedOptionActivatedByType($type, $opt_type=1){
        $ses = $this->session->userdata("user_id");
        return $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id", "left")
                        ->get_where($this->usOptTbl, array("opt_user_id"=>$ses, "opt_option_status"=>1, "opt_option_id"=>$type));
    }

    function get_userPurchasedOptionActivatedDateByType($type, $date, $opt_type=1){
        $ses = $this->session->userdata("user_id");
        return $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id", "left")
                        ->get_where($this->usOptTbl, array("opt_user_id"=>$ses, "opt_option_status"=>1, "opt_option_id"=>$type, "opt_option_active_date <="=>$date, "opt_option_end_date >="=>$date));
    }
    
    //functions for slimpay coding start
        /*
        * retrive user and order detials by 34 digits slimpay mandate creation id reference
        */
        function getSlimpayTransactionDetails($reference){
            $this->db->select("*");
            $this->db->from($this->pytTbl);
            $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_tran_id = $this->pytTbl.pyt_id");
            $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
            $this->db->where("py_slmref_id", $reference);
            return $this->db->get()->row_array();
        }

        function getSlimpayUserDetails($reference){
            $this->db->select("*");
            $this->db->from($this->pytTbl);
            $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_tran_id = $this->pytTbl.pyt_id");
            $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id");
            $this->db->where("pyt_id", $reference);
            return $this->db->get()->row_array();
        }

        function getOptionDetails($args){
            return $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usOptTbl.opt_option_id")
                            ->get_where($this->usOptTbl, array("opt_tran_id"=>$args))->result_array();
        }

        function getMasterOption($args = 1){
            return $this->db->get_where($this->usoptMaster, array("opt_id"=>$args))->row_array();
        }
    //Functions form slimpay coding end
    

    function get_options($opt_name)
    {
     $sql = "SELECT * from otd_option_master WHERE opt_name = '". $opt_name. "'";
        $query = $this->db->query($sql);
        $REC = $query->num_rows();

        if($query->num_rows()>0)
        {
            
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }


    function getUserOptions($params = array(), $wherein = array())
    {

        $this->db->select("*");
        $this->db->from("otd_user_option");
        $this->db->join("otd_option_master", "otd_option_master.opt_id = otd_user_option.opt_option_id", "left");
        $this->db->join("otd_user_option_payment", "otd_user_option_payment.pyt_id =  otd_user_option.opt_tran_id", "left");
        $this->db->order_by("opt_user_option_id", "DESC");
        $this->db->where($params);
        if(!empty($wherein)){
            foreach($wherein as $k=>$v){
                $this->db->where_in($k, $v);
            }
        }
        $query = $this->db->get();


        //  echo $this->db->last_query();
        //die();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function getUserMandate($where = array()){
      
        $str = implode(",", $where);
       // echo("<script>console.log('in user modal 169".$str ."');</script>");
        
        $query = $this->db->get_where($this->slmrefTbl, $where)->row_array();
        $strtxt = $this->db->last_query();
        $str = implode(",", $query);
      //  echo "in user modal 174 ===> ".$str . " <===" ;

        return $query;
    }

    function getUploadDocuments($params = array()){
        $this->db->select("*");
        $this->db->from($this->upldDocTbl);        
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->upldDocTbl.dc_userid");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        $this->db->join($this->docTbl, "$this->docTbl.ct_id = $this->upldDocTbl.dc_docid");
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("dc_id",$params)){
            $this->db->where('dc_id',$params['dc_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function insertMandate($reference){
        $ses = $this->session->userdata("user_id");
        $mandate = $this->getUserMandate(array("slmref_userid"=>$ses));
        if(!empty($mandate)){
            $this->db->update($this->slmrefTbl, array("slmref_mandateref"=>$reference), array("slmref_userid"=>$ses));
            return true;
        }else{
            $array = array("slmref_userid"=>$ses,
                           "slmref_mandateref"=>$reference,
                           "slmref_subscriber"=>"BUSINESSUSER_".$ses
                          );
            $this->db->insert($this->slmrefTbl, $array);
            return $this->db->insert_id();
        }
    }


    function getUserOptionsStatic($params = array())
    {
       // print_r($params);
       // die();
       $query = $this->db->join("otd_option_master", "otd_option_master.opt_id = otd_user_option.opt_option_id", "left")
                          ->join("otd_user_option_payment", "otd_user_option_payment.pyt_id =  otd_user_option.opt_tran_id", "left")
                          ->order_by("opt_user_option_id", "DESC")
                          ->get_where("otd_user_option", $params);

       //                   $sql = "SELECT * FROM `otd_user_option` LEFT JOIN `otd_option_master` ON `otd_option_master`.`opt_id` = `otd_user_option`.`opt_option_id` LEFT JOIN `otd_user_option_payment` ON `otd_user_option_payment`.`pyt_id` = `otd_user_option`.`opt_tran_id` WHERE `opt_user_id` = '77' AND `opt_option_type` = 3 AND `opt_option_status` != 2 ORDER BY `opt_user_option_id` DESC "

       // echo $this->db->last_query();

        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function getUserAdvOptions($params = array())
    {
        $sql = "SELECT fo.frm_option_id, fo.opt_id, fo.Budget, fo.StartDate, fo.Description, fo.CreateDate, om.opt_name, om.opt_description FROM `otd_formoption` `fo` LEFT JOIN `otd_option_master` `om` ON om.`opt_id` = fo.`opt_id` where fo.`user_id` = ". $params['user_id']." and om.opt_status != 2 ";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function getUserAdvOptionsDetails($frm_option_id)
    {
        $sql = "SELECT fo.*, om.opt_name, om.opt_description FROM `otd_formoption` `fo` LEFT JOIN `otd_option_master` `om` ON om.`opt_id` = fo.`opt_id` where fo.`frm_option_id` = ". $frm_option_id." and om.opt_status != 2 ";
        $query = $this->db->query($sql);
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function get_user_options_price($user_id)
    {
        $sql = "SELECT * from otd_user_option WHERE opt_user_id = ". $user_id;
     

            $query = $this->db->query($sql);

            $REC = $query->num_rows();

                                                                                                                                                                                            

            if($query->num_rows()>0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }


    }

    function getSingleOptiondetails($id = 1){
        return $this->db->join("otd_option_master", "otd_option_master.opt_id = otd_user_option.opt_option_id", "left")
                        ->get_where("otd_user_option", array("opt_user_option_id"=>$id))->row_array();
    }

    function get_options_price($opt_id = 0)
    {
        if($opt_id == "")
           { $opt_id = 0;}

            $sql = "SELECT * from otd_option_price WHERE opt_type_id = ". $opt_id;

           
            $query = $this->db->query($sql);
            $REC = $query->num_rows();

            if($query->num_rows()>0)
            {
                return $query->result_array();
            }
            else
            {
                return false;
            }
    }

    function get_single_options_pricebytype($where = array())
    {
        return $this->db->get_where("otd_option_price", $where)->row_array();
    }

    function get_single_options_price($opt_id = 0)
    {
        if($opt_id == "")
           { $opt_id = 0;}

            $sql = "SELECT * from otd_option_price WHERE opt_price_id = ". $opt_id;

           
            $query = $this->db->query($sql);
            $REC = $query->num_rows();

            if($query->num_rows()>0)
            {
                return $query->row_array();
            }
            else
            {
                return false;
            }
    }

    function getUserPhones($user_id = 0)
    {
        if($user_id == ""){ $user_id = 0;}
        $sql = "SELECT * from otd_user_phone WHERE phn_user = ". $user_id;
        $query = $this->db->query($sql);
        $REC = $query->num_rows();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }

    function getEvents($user_id){
        $sql = "SELECT e.*, u.user_company_name as `created_by_user_name` from otd_events e left join users u on u.user_id = e.created_by_user WHERE e.created_by_user = ". $user_id." and e.event_status = 1";
        $query = $this->db->query($sql);
        $REC = $query->num_rows();
        if($query->num_rows()>0)
        {
            return $query->result_array();
        }
        else
        {
            return false;
        }
    }
    function isBusinessOnline($user_id)
    {

        $result=0; 
        $query =$this->db->query("select is_online from users where user_id = ".$user_id. " and is_online = '1'");
        $result = $query->num_rows(); 
        /*echo $this->db->last_query();
        echo $result;
        die();*/
        if($result > 0)
        {
            return 1;
        }
        else
        {
            return 0;
        }

    }


    /*
     * get rows from the users table
     */
    function getRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->userTbl);
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }



        return $result;
    }
    
    function checkBsEmail($email){
        $ses = $this->session->userdata("user_id");
        $this->db->select('*');
        $this->db->from($this->userTbl);
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        $this->db->where(array("bs_email"=>$email, "bs_user !="=>$ses));
        $query = $this->db->get();
        if($query->num_rows() > 0){
            return true;
        }else{
            return false;
        }
    }


    /*
     * Insert user information
     */
    public function insert($data = array()) {

        $auth_check_subs = 0;
        $sms_check_subs = 0;
        $user_email_subs =  null;
        $user_postcode_subs =  null;

         //add created and modified data if not included
        if(!array_key_exists("created", $data)){
            $data['created'] = date("Y-m-d H:i:s");
        }
        if(!array_key_exists("modified", $data)){
            $data['modified'] = date("Y-m-d H:i:s");
        }

        if(array_key_exists("auth_check", $data)){
            $auth_check_subs = 1;
        }
         if(array_key_exists("sms_check", $data)){
            $sms_check_subs = 1;
        }

         if(array_key_exists("user_email", $data)){
            $user_email_subs =  $data['user_email'];
        }

         if(array_key_exists("user_postcode", $data)){
            $user_postcode_subs =  $data['user_postcode'];
        }


        //insert user data to users table
        $this->db->insert($this->userTbl, $data);
        $insert = $this->db->insert_id();
        
        //return the status
        if($insert){
            $subsData = array();
            if($auth_check_subs == 1 || $sms_check_subs == 1)
            {
                $subsData = array('user_id' => $this->db->insert_id(),
                                  'user_email'=>$user_email_subs,
                                  'user_postcode' => $user_postcode_subs,
                                  'promo_mail' => $auth_check_subs,
                                  'promo_sms' => $sms_check_subs,
                                );
              $insert1 = $this->db->insert($this->userTblSubs, $subsData);
            }
            return $insert;
        }else{
            return false;
        }
    }


    public function createFormOption($data = array()) 
    {
         $this->db->insert("otd_formoption", $data);
         $insert = $this->db->insert_id();
           if($insert){
                return $insert;
            }else{
                return false;
           }
    }

    public function update_password($email, $pass)
    {

        $data = array(
                   'user_password' => $pass              
                );


        $this->db->where("user_email", $email);

        $val =  $this->db->update($this->userTbl, $data);


        return $val;

    } 
    public function activateUser($str) {
        $this->db->select('*');
        $this->db->from($this->userTbl);
        $this->db->where('act_link', $str);
        $this->db->where('status', 1);
        $query = $this->db->get();

        $rowcount = $query->num_rows();
        if($rowcount >= 1)
        {
            return -101;
        }
        else
        {
            $Data = array('status'=> 1);

            $this->db->where('act_link', $str);
            $this->db->where('status', 0);
            $val =  $this->db->update($this->userTbl, $Data);

            $this->db->select('*');
            $this->db->from($this->userTbl);
            $this->db->where('act_link', $str);
            $this->db->where('status', 1);
            $query = $this->db->get();

            $rowcount = $query->num_rows();
            return $rowcount;
        }
        
    }


    /*
     * Insert user information
     */
    public function subscribe_insert($data = array()) {
        $subsData = array();
        $auth_check_subs = 0;
        $sms_check_subs = 0;
        $user_email_subs =  null;
        $user_postcode_subs =  null;
        if(array_key_exists("auth_check", $data)){
            $auth_check_subs = 1;
        }
         if(array_key_exists("sms_check", $data)){
            $sms_check_subs = 1;
        }

         if(array_key_exists("user_email", $data)){
            $user_email_subs =  $data['user_email'];
        }

         if(array_key_exists("user_postcode", $data)){
            $user_postcode_subs =  $data['user_postcode'];
        }


        $subsData = array(
               
                'user_email'=> $user_email_subs,
                'user_postcode' => $user_postcode_subs,
                'promo_mail' => $auth_check_subs,
                'promo_sms' => $sms_check_subs,
                

            );
        $insert = $this->db->insert($this->userTblSubs, $subsData);
        return $this->db->insert_id();        
    }

    public function update($data = array(), $where = array()) {        
        $this->db->where($where);                        
        $x = $this->db->update($this->userTbl, $data);
        //echo  $this->db->last_query();
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;        
    }

    public function updateTable($table="", $data = array(), $where = array()) {        
        $this->db->where($where);                        
        $x = $this->db->update($table, $data);
        //echo  $this->db->last_query();
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;        
    }



    public function claimBusinessProfile($data = array()) {        
        $x = $this->db->insert("otd_claimed_requests", $data);
       
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;        
    }



    function getOpeningHrs($user_id){
        $query = $this->db->get_where("otd_business_openingtime", array("op_user"=>$user_id));
         $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
       
    }



    function getBusinessCategories($type=1){
        $query = $this->db->order_by("cat_name", "ASC")->group_by("cat_name")->get_where("otd_business_category", array("cat_status"=>1, "type"=>$type));
        return $query;
    }

function getClaimData($clmnum)
{
    $sql = "select user_id, user_company_name, user_company_address, user_company_number from users where user_company_number = '".$clmnum. "'";
                  
    $query = $this->db->query($sql);
        return $query->row_array();

}
    function socialLogin($info, $type){        
        $query = $this->db->get_where("users", $info);
        return $query;
    }

    function site_contents($args){
        $query = $this->db->get_where("site_contents", array("content_id"=>$args));
        foreach($query->result() as $q){
            $data = $q->content;
        }
        return $data;
    }

    /*
     * get rows from the business profile table
     */
    function getBusinessRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->businessTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("user_id",$params)){
            $this->db->where('bs_user',$params['user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }


        return $result;
    }

    function getSimilarBusinessProfile_with_view($user_id){
     //$sql = "select * from vv_users_list where categories in (select categories from vv_categories_list where user_id =". $user_id. ")";
       
       $sql = "select * from vv_users_list where user_id in(select user_id from otd_user_business_category where category_id in (select category_id from otd_user_business_category where user_id = $user_id) and user_id != $user_id group by user_id)";

        $query = $this->db->query($sql);
         $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

         return $result;

    }

    function getSimilarBusinessProfile($user_id){
            
           $this->db->select("
                             users.user_id AS user_id,
                             users.user_firstname AS user_firstname,
                             users.user_lastname AS user_lastname,
                             users.user_phone AS user_phone,
                             users.user_company_name AS user_company_name,
                             users.user_company_number AS user_company_number,
                             users.user_password AS user_password,                          
                             users.user_type AS user_type,
                             users.user_email AS user_email,
                             users.user_profile_pic AS user_profile_pic,
                             otd_business_details.bs_address AS user_company_address,
                             users.user_lat AS user_lat,
                             users.user_long AS user_long,                         
                             (select count(*) from profile_review where voted_user_id = users.user_id AND vote_status= 1 AND status = 1) AS Comments,
                             (select count(*) from otd_recommend_merchant where recommended_user_id = users.user_id) AS Recommaned,
                             group_concat(category_name separator ',') AS CategoryName,
    						 group_concat(category_id separator ',') AS CategoryId
    						 ", FALSE);
     
           $this->db->from("$this->userTbl");
           $this->db->join("$this->businessTbl", "$this->businessTbl.bs_user = $this->userTbl.user_id");
           $this->db->join("$this->userCatTbl", "$this->userCatTbl.user_id = $this->userTbl.user_id", "left");
    	   
           $this->db->where(" users.user_id in ( select user_id from  otd_user_business_category where category_id in ( select category_id from otd_user_business_category where user_id = $user_id)  and users.user_id != $user_id ) ");
           
           $this->db->group_by("users.user_id");
           $this->db->order_by("users.user_id","RANDOM");
    	   $this->db->limit(6);
           $query = $this->db->get();
           $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           $return['result'] = $result;
      
           return $result;

    }

    function getUserLanguages($lng)
    {
        if($lng==""){$lng=2;}
        $sql = "select lang_name from otd_languages where lang_id in (". $lng.")";
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

    function getProfilePics($user_id){
        $imglimit = $this->get_userPurchasedOptionActivatedByType(6);
        if($imglimit->num_rows() > 0){
            $imgltd = "";
            foreach($imglimit->result() as $imgs){ 
                if($imgs->opt_option_active_date <= date("Y-m-d") && $imgs->opt_option_end_date >= date("Y-m-d")){
                    $imgltd = $imgltd + $imgs->otp_option_qnty;
                }
            }
            if($imgltd == 0){
                $imgltd = 4;
            }
        }else{
            $imgltd = 4;
        }
        $sql = "select * from otd_user_photogallery where ph_user =". $user_id." LIMIT $imgltd";
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

    function getProducstServices($user_id, $type)
    {
        $sql = "select * from otd_business_category where cat_id in (select category_id from otd_user_business_category where user_id = ". $user_id. " and type = ".$type.")";
        $query = $this->db->query($sql);
        $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
        return $result;
    }

    function getBusinessProfile($params = array()){
        $this->db->select('*');
        $this->db->from("business_profile");        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("user_id",$params)){
            $this->db->where('user_id',$params['user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }

        return $result;
    }

    /*
    * Insert Business profile details
    */
    function insertBusinessProfile($params = array()){
        $this->db->insert($this->businessTbl, $params);
        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    /*
    * Update Business profile details
    */
    function updateBusinessDetails($params = array(), $where = array()){
        $this->db->update($this->businessTbl, $params, $where);
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;  
    }

    /*
    * get rows for business price pdf
    */
    function getBusinessPriceRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->priceTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("user_id",$params)){
            $this->db->where('pr_user',$params['user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert & Upload Business price details
    */
    function insertBusinessPricePdf($mode = 1, $params = array(), $where = array()){
        if($mode == 1){
            $this->db->insert($this->priceTbl, $params);            
        }
        else{
            $this->db->update($this->priceTbl, $params, $where);
        }
        if($this->db->affected_rows() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    /*
    * get rows for business opening time table
    */
    function getBusinessOpeningRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->bsopenTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("user_id",$params)){
            $this->db->where('op_user',$params['user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert/ Update business opening timing    
    */
    function insertBusinessOpeningTime($params = array()){
        $userid = $this->session->userdata("user_id");
        $user = $this->db->get_where($this->bsopenTbl, array("op_user"=>$userid));
        if($user->num_rows() > 0){
            $this->db->update($this->bsopenTbl, $params, array("op_user"=>$userid));
        }
        else{
            $params['op_user'] = $userid;
            $this->db->insert($this->bsopenTbl, $params);
        }
        return true;        
    }

    /*
    * get rows for business opening time table
    */
    function getGalleryRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->photogal);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("ph_id",$params)){
            $this->db->where('ph_id',$params['ph_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Insert Photo Gallery
    */
    function insertPhotoGallery($params = array()){
        $this->db->insert($this->photogal, $params);
        $id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            return $id;
        }
        else{
            return false;
        }
    }

    /*
    * Send box idea message
    */
    function insertBoxIdea($params = array()){
        $this->db->insert($this->boxIdea, $params);
        $id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            return $id;
        }
        else{
            return false;
        }
    }

    /*
    * Get User Following details
    */
    function getFollowingRows($params = array()){
        // $lat="48.8566140";
        // $lng = "2.3522220";
        $user = $this->getRows(array('user_id'=>$this->session->userdata('user_id')));
        if(!empty($user['user_lat']))
            $lat = $user['user_lat'];
        else
            $lat = "48.8566140";
        if(!empty($user['user_long']))
            $lng = $user['user_long'];
        else
            $lng = "2.3522220";
        $this->db->select("*, $this->userTbl.user_id AS usersid,
                           GROUP_CONCAT(DISTINCT CONCAT(category_name) SEPARATOR ', ') as categories,
						   GROUP_CONCAT(DISTINCT CONCAT(category_id) SEPARATOR ', ') as category_id,
                           round(( 3959 * acos( cos( radians($lat) ) * cos( radians( user_lat ) ) 
                           * cos( radians(user_long) - radians($lng)) + sin(radians($lat)) 
                           * sin( radians(user_lat)))), 2) AS distance");
        $this->db->from($this->followTbl);
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->followTbl.followed_user_id");
        $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
        $this->db->join($this->userbscatTbl, "$this->userbscatTbl.user_id = $this->userTbl.user_id", "left");
        $this->db->group_by("$this->followTbl.followed_user_id");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("followed_user_id",$params)){
            $this->db->where('followed_user_id',$params['follower_user_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get rows from the langauges table
    */
    function getLanguageRows($params = array()){
       $this->db->select('*');
       $this->db->from($this->langTbl);        
       //fetch data by conditions
       if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
       }
       
       if(array_key_exists("lang_id",$params)){
           $this->db->where('lang_id',$params['lang_id']);
           $query = $this->db->get();
           $result = $query->row_array();
       }else{            
           //set start and limit
           if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
           }
           $query = $this->db->get();


           if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
           }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
           }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           }
       }
       return $result;
    }

    function getCategoriesAndFilters($type= 1){
       $user = $this->session->userdata("user_id");
       $query = $this->db->select("GROUP_CONCAT(DISTINCT CONCAT(category_id)) as category")
                         ->group_by("user_id",$user)
                         ->get_where($this->userbscatTbl, array("user_id"=>$user, "type"=>$type));
       if($query->num_rows() > 0 ){
           foreach($query->result() as $res){ }
           return $res->category;
       }
       else{
           return "";
       }
    }

    function getCategoriesAndFiltersByName($type= 1){
       $user = $this->session->userdata("user_id");
       $query = $this->db->select("GROUP_CONCAT(DISTINCT CONCAT(category_name) SEPARATOR ',') as category")
                         ->group_by("user_id",$user)
                         ->get_where($this->userbscatTbl, array("user_id"=>$user, "type"=>$type));
       if($query->num_rows() > 0 ){
           foreach($query->result() as $res){ }
           return $res->category;
       }
       else{
           return "";
       }
    }

    function insertUserCategories($type=1, $ids = array(), $foruser = NULL){
       if(!empty($ids)){
           $this->db->delete($this->userbscatTbl, array("user_id"=>$foruser, "type"=>$type));
           foreach($ids as $id){
               $category = $this->db->get_where("otd_business_category", array("cat_id"=>$id));
               foreach($category->result() as $cate){ }
               $this->db->insert($this->userbscatTbl, array("user_id"=>$foruser, "category_id"=>$id, "category_name"=>$cate->cat_name, "type"=>$type));                
           }
       }
    }

    /*
    * remove user categories when user not select any category
    */
    function removeExistingCategories($type=1){       
        $ses = $this->session->userdata("user_id");
        $this->db->delete($this->userbscatTbl, array("user_id"=>$ses, "type"=>$type));
        if($this->db->affected_rows() > 0){
            return true;
        }else{
            return false;
        }
    }

    /*
    *  GET LAT & LONG FROM ADDRESS
    */
    function get_lat_long($address){

        $address = str_replace(" ", "+", $address);
        $region = "fr";
        $json = file_get_contents("https://maps.google.com/maps/api/geocode/json?key=AIzaSyCFV0MgMCdDw4wuDxgS7_ymejhyA7d2h7A&address=$address&sensor=false&region=$region");
        $json = json_decode($json);              
        if(!empty($json->results)){
            $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
            $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};            
            $this->session->set_userdata("emptyaddress", 0);
        }else{
            $emptyaddress = $this->session->userdata("emptyaddress");
            $emptyaddress++;
            if($emptyaddress < 3){
                $this->session->set_userdata("emptyaddress",$emptyaddress);
                $this->get_lat_long($address);
            }else{
                $lat="48.8566140";
                $long = "2.3522220";                
            }
        }
        $response = array('lat'=>$lat,'lng'=>$long);
        return $response;

       // $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?key=AIzaSyCFV0MgMCdDw4wuDxgS7_ymejhyA7d2h7A&address=$address&sensor=false");
       // $json = json_decode($json);

       // if((isset($json)) && ($json->{'status'} == "OK")){
       //      $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}; 
       //      $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};        
       // }else{
       //      $lat = -25.274398; 
       //      $long = 133.775136; 
       // }
       
       // $response = array('lat'=>$lat,'lng'=>$long);
       // return $response;

    }

    /*
    * get rows from the langauges table
    */
   function getMessagesRows($params = array()){
       $this->db->select('*');
       $this->db->from($this->msgTbl);        
       //fetch data by conditions
       if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
       }
       
       if(array_key_exists("msg_id",$params)){
           $this->db->where('msg_id',$params['msg_id']);
           $query = $this->db->get();
           $result = $query->row_array();
       }else{            
           //set start and limit
           if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
           }
           $query = $this->db->get();


           if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
           }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
           }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           }
       }
       return $result;
   }

    function userMessages(){
        $id = $this->session->userdata("user_id");
        return $this->db->select("user_id, 
                                  MAX(msg_id) as mx_id,                                  
                                  user_firstname,
                                  user_lastname,
                                  user_profile_pic,
                                  is_online,
                                  bs_name,
                                  user_type,
                                  SUM(if(msg_reciever = $id,
                                  (CASE WHEN msg_flag = 0 THEN 1 ELSE 0 END), 0)) AS unread")
                         ->where("(msg_sender = $id OR msg_reciever = $id ) AND msg_sender != msg_reciever")
                         ->order_by("mx_id", "DESC")
                         ->join("users", "user_id = IF(msg_sender = $id, msg_reciever, msg_sender)")
                         ->join($this->businessTbl, "$this->businessTbl.bs_user = users.user_id", "left")
                         ->group_by("user_id")
                         ->get($this->msgTbl);
    }

    function singleUserMessages($id, $args){
        $this->updatemessage(array("msg_flag"=>1, "us_notify"=>1), array("msg_sender"=>$args, "msg_reciever"=>$id));
        return $messages = $this->db->select("msg_sender, msg_reciever, user_profile_pic, msg_content, msg_date, msg_flag, IF(msg_sender = $id, msg_reciever, msg_sender) AS user_by")
                                    ->join("users", "user_id = $args")
                                    ->where("((msg_sender = $id AND msg_reciever = $args) OR (msg_sender = $args AND msg_reciever = $id))")
                                    ->get($this->msgTbl);
    }

    /*
    * Insert Business profile details
    */
    function insertUserMessages($params = array()){
        $this->db->insert($this->msgTbl, $params);
        if($this->db->affected_rows() > 0){
            return $this->db->insert_id();
        }
        else{
            return false;
        }
    }

    /*
    * update messages flag
    */
    private function updatemessage($params = array(), $where=array()){
        $this->db->update($this->msgTbl, $params, $where);
        return true;
    }
    
    /*
    * Insert Follower
    */
    function insertFollower($params = array()){
        $this->db->insert($this->followTbl, $params);
        $id = $this->db->insert_id();
        if($this->db->affected_rows() > 0){
            $this->db->insert($this->notiTbl, array("nt_by"=>$params['follower_user_id'], "nt_to"=>$params['followed_user_id'], "nt_type"=>1, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s")));
            return $id;
        }
        else{
           return false;
        }
    }

    /*
    * Delete Follower
    */
    function deleteFollower($params = array()){
        $this->db->delete($this->followTbl, $params);     
        if($this->db->affected_rows() > 0){
           return true;
        }
        else{
           return false;
        }
    }

    function isFollowing($profile_id, $follower_id=0)
    {
       if(!isset($follower_id) || $follower_id ==0)
       {
           $follower_id=0;
       }
       $sql = "select followed_user_id from otd_business_follower where followed_user_id = ". $profile_id. " and follower_user_id =". $follower_id;
      // echo $sql;
    
       $query = $this->db->query($sql);
       $result = $query->num_rows();

       if($result > 0)
       {
          return true;
       }
       else
       {
          return false;
       }
    }

    function getReviewRowSingle($vote_id=0)
    {
        $this->db->select('*');
        $this->db->from($this->revewTbl);
        $this->db->where('vote_id',$vote_id);
        $query = $this->db->get();
        $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
        return $result;
    }

    /*
    * get rows from the review table
    */
   
    function getReviewRows($params = array()){
       $this->db->select('*');
       $this->db->from($this->revewTbl);
       $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->revewTbl.voter_user_id");
       //fetch data by conditions
       if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
       }
       if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
       
       if(array_key_exists("vote_id",$params)){
           $this->db->where('vote_id',$params['vote_id']);
           $query = $this->db->get();
           $result = $query->row_array();
       }else{            
           //set start and limit
           if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
           }



           $query = $this->db->get();


           if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
           }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
           }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           }
       }

       return $result;
   }

/*   function getReviewRows($user_id)
   {
           $sql = "a.*, b.user_firstname as user_firstname, b.user_lastname as user_lastname from profile_review a join users b where voted_user_id = ". $user_id. " order by vote_id DESC";


$query = $this->db->query($sql);
            $result = ($query->num_rows() > 0)?$query->result_array():FALSE;

            return $result;

   }*/

   /*
   * Get Total Review
   */
   function getTotalReview($args){
        if(!empty($args)){
            return $this->db->select("COUNT(*) AS total, SUM(blog_vote) AS sum")
                            ->get_where($this->revewTbl, array("voted_user_id"=>$args, "vote_status"=>1, "status"=>1))->row_array();
        }else{
            return false;
        }
   }

    /*
    * Insert user review
    */
    function insertUserReview($params = array()){
        $this->db->insert($this->revewTbl, $params);
        if($this->db->affected_rows() > 0){
         $id = $this->db->insert_id();   
            $this->db->insert($this->notiTbl, array("nt_by"=>$params['voter_user_id'], "nt_to"=>$params['voted_user_id'], "nt_type"=>3, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s")));
            return $id; 
        }
        else{
            return false;
        }
    }

    function insertUserOption($params = array())
    {
        $this->db->insert("otd_user_option", $params);     
        $insertid =  $this->db->insert_id();
        if( $insertid > 0){
            return $insertid;
        }
        else{           
            return false;
        }
    }

    function updateUserOption($params = array(), $where){
        $this->db->update("otd_user_option", $params, $where);
        if($this->db->affected_rows() > 0)
            return true;
        else
            return false;
    }

    /*
    * get user details from user option table
    */
    function getUserRowsFromOption($args = ""){
        return $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->usOptTbl.opt_user_id")
                        ->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->usOptTbl.opt_option_id")
                        ->get_where($this->usOptTbl, array("opt_user_option_id"=>$args))->row_array();
    }

    function insertUserReportedReview($params = array()){
        $this->db->insert($this->reportedRevewTbl, $params);
        if($this->db->affected_rows() > 0){
            $id = $this->db->insert_id();   
            //  $this->db->insert($this->, array("nt_by"=>$params['voter_user_id'], "nt_to"=>$params['voted_user_id'], "nt_type"=>3, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s")));
            return $id; 
        }
        else{
            return false;
        }
    }


    function recommendUser($rec_by, $rec_to)
    {
       $recommend_by_user_id = $rec_by;
       $recommended_user_id =  $rec_to;

       if($rec_by !=0)
       {
       $sql2 = "select * from `otd_recommend_merchant` where recommended_user_id =".$rec_to. " and recommend_by_user_id =" . $rec_by;
       $query = $this->db->query($sql2);
             $x =$query->num_rows(); 
       }
       else
       {
           $x=0;
       }    

       if($x > 0)
       {
       return $x;
       }
       else
       {
       $sql = "INSERT INTO `otd_recommend_merchant` (`recommend_by_user_id`, `recommended_user_id`) VALUES (". $rec_by .", ".$rec_to . ")";

       $x = $this->db->query($sql);

       $sql2 = "select * from `otd_recommend_merchant` where recommended_user_id =".$rec_to;

       $query = $this->db->query($sql2);
       return $query->num_rows(); 
       }

    }


    function getrecommendUser($rec_to)
    {
        $recommended_user_id =  $rec_to;
        $sql2 = "select * from `otd_recommend_merchant` where recommended_user_id =".$rec_to;
        $query = $this->db->query($sql2);
        return $query->num_rows(); 
    }

    /*
    * Get rows for notifications
    */
    /*
    * get rows from the review table
    */
   function getNotificationsRows($params = array()){
       $this->db->select('*');
       $this->db->from($this->notiTbl);
       $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->notiTbl.nt_by");
       $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
       //fetch data by conditions
       if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
       }
       if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
       
       if(array_key_exists("nt_id",$params)){
           $this->db->where('nt_id',$params['nt_id']);
           $query = $this->db->get();
           $result = $query->row_array();
       }else{            
           //set start and limit
           if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
           }
           $query = $this->db->get();


           if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
           }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
           }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           }
       }
       return $result;
   }

    /*
    * update noti table
    */
    function notiUpdate($params = array(), $where = array()){
        $this->db->update($this->notiTbl, $params, $where);
        return true;
    }

    /*
    * Insert Business events 
    */
    public function insertEvents($params = array()){
        $this->db->insert($this->eventTbl, $params);
        if($this->db->affected_rows() > 0){            
            $id = $this->db->insert_id();
            $ses = $this->session->userdata("user_id");
            $query = $this->db->select("user_id, user_email")
                              ->join($this->userTbl, "$this->userTbl.user_id = $this->followTbl.follower_user_id")
                              ->get_where($this->followTbl,array('followed_user_id'=>$ses));
            $emails = array();
            foreach($query->result() as $userid){
                $this->db->insert($this->notiTbl, array("nt_by"=>$ses, "nt_to"=>$userid->user_id, "nt_type"=>2, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s"), "nt_rlt_id"=>$id));
                $emails[] = $userid->user_email;
            }
            return array("id"=>$id, "followemails"=>$emails);
        }else{
            return false;
        }
    }

    public function insertEventwithougNoti($params = array()){
        $this->db->insert($this->eventTbl, $params);
        if($this->db->affected_rows() > 0){            
            return $this->db->insert_id();

        }else{
            return false;
        }
    }

    /*
    * update event
    */
    function updateUserEvent($param = array(), $where){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $this->db->update($this->eventTbl, $param, $where);
            return true;
        }else{
            return false;
        }
    }

    /*
    * update event and notify
    */
    function updateUserEventWithNoti($param = array(), $where){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            $query = $this->db->select("user_id, user_email")
                              ->join($this->userTbl, "$this->userTbl.user_id = $this->followTbl.follower_user_id")
                              ->get_where($this->followTbl,array('followed_user_id'=>$ses));
            $emails = array();
            foreach($query->result() as $userid){
                $this->db->insert($this->notiTbl, array("nt_by"=>$ses, "nt_to"=>$userid->user_id, "nt_type"=>2, "nt_read"=>0, "nt_flag"=>0, "nt_date"=>date("Y-m-d H:i:s"), "nt_rlt_id"=>$where['event_id']));
                $emails[] = $userid->user_email;
            }
            $this->db->update($this->eventTbl, $param, $where);
            return array("status"=>true, "emails"=>$emails);
        }else{
            return false;
        }
    }


    /*
    * Remove Events from table
    */
    function removeEvent($where= NULL, $ses = NULL){
        if(!empty($where) && !empty($ses)){
            $query = $this->db->get_where($this->usOptTbl, array("opt_user_option_id"=>$where))->row_array();
            if(!empty($query)){
                $this->db->update($this->eventTbl, array("event_status"=>3), array("event_id"=>$query['opt_event_id']));
                $this->db->update($this->usOptTbl, array("opt_option_status"=>2), array("opt_user_option_id"=>$query['opt_user_option_id']));
            }
            return true;
        }
    }

    /*
    * Get rows from following users events
    */
   public function getFollowingEvents(){
        $userid = $this->session->userdata("user_id");
        if(!empty($userid)){
            $query = $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->eventTbl.created_by_user")
                              ->join($this->followTbl, "$this->followTbl.followed_user_id = $this->eventTbl.created_by_user")
                              ->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left")
                              ->join($this->usOptTbl, "$this->usOptTbl.opt_event_id = $this->eventTbl.event_id")
                              ->get_where($this->eventTbl, array("$this->followTbl.follower_user_id"=>$userid, "event_status"=>1, "opt_option_status"=>1));
            return $query->result_array();
        }else{
            return false;
        }
    }

    /*
    * Get row of event with Full Details
    */
    function getEventsRows($params = array()){
       $this->db->select('*');
       $this->db->from($this->eventTbl);
       $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->eventTbl.created_by_user");
       $this->db->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left");
       $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_event_id = $this->eventTbl.event_id");
       $this->db->join($this->eventTypeTbl, "$this->eventTypeTbl.evt_type_id = $this->eventTbl.event_type_id", "left");
       //fetch data by conditions
       if(array_key_exists("conditions",$params)){
           foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
           }
       }
       if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
       
       if(array_key_exists("event_id",$params)){
           $this->db->where('event_id',$params['event_id']);
           $query = $this->db->get();
           $result = $query->row_array();
       }else{            
           //set start and limit
           if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
           }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
           }
           $query = $this->db->get();


           if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
           }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
           }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
           }
       }
       return $result;
   }

    /*
    * Get user own Events
    */
    public function getMyEvents(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            return $this->db->order_by("created_on", "DESC")->get_where($this->eventTbl, array("created_by_user"=>$ses, "event_status !="=>3))->result_array();
        }else{
            return false;
        }
    }

    /*
    * Get Free Event
    */
    public function getMyFreeEvents(){
        $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            return $this->db->order_by("created_on", "DESC")->get_where($this->eventTbl, array("created_by_user"=>$ses, "event_is_free"=>1, "event_status !="=>3))->result_array();
        }else{
            return false;
        }
    }

    /*
    * Get events type list
    */
    function getEventTypeList(){
         $ses = $this->session->userdata("user_id");
        if(!empty($ses)){
            return $this->db->order_by("evt_type", "ASC")->get_where($this->eventTypeTbl, array("evt_status"=>1))->result_array();
        }else{
            return false;
        }
    }

     /*
    * Get single row details from event table
    */
    public function myEventDetails($args){        
        $user = $this->session->userdata("user_id");
        $query = $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->eventTbl.created_by_user")
                          ->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left")
                          ->join($this->usOptTbl, "$this->usOptTbl.opt_event_id = $this->eventTbl.event_id")
                          ->join($this->eventTypeTbl, "$this->eventTypeTbl.evt_type_id = $this->eventTbl.event_type_id")
                          ->where_not_in("event_status", array(0,3))
                          ->get_where($this->eventTbl, array("event_id"=>$args, "created_by_user"=>$user));
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
    }

    /*
    * Get single row details from event table
    */
    public function eventDetails($args){        
        $user = $this->session->userdata("user_id");
        $query = $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->eventTbl.created_by_user")
                          ->join($this->businessTbl, "$this->businessTbl.bs_user = $this->userTbl.user_id", "left")
                          ->join($this->usOptTbl, "$this->usOptTbl.opt_event_id = $this->eventTbl.event_id")
                          ->join($this->eventTypeTbl, "$this->eventTypeTbl.evt_type_id = $this->eventTbl.event_type_id")
                          ->where_not_in("event_status", array(0,3))
                          ->get_where($this->eventTbl, array("event_id"=>$args));
        // $check = $this->db->get_where($this->followTbl, array("followed_user_id"=>$query['created_by_user'], "follower_user_id"=>$user));
        // if($check->num_rows() > 0){
        //     return $query;
        // }else{
        //     return false;
        // }
        if($query->num_rows() > 0){
            return $query->row_array();
        }else{
            return false;
        }
    }

    /*
     * get rows from the user number table
     */
    function getExtraNumberRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->phTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        
        if(array_key_exists("phn_id",$params)){
            $this->db->where('phn_id',$params['phn_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }


        return $result;
    }

    /*
    * Insert user extra numbers
    */
    public function insertPhonenumbers($mode = 1, $numb = array(), $where = array()){
        if($mode == 1){
            $this->db->insert($this->phTbl, $numb);
            return $this->db->insert_id();
        }else{
            $this->db->update($this->phTbl, $numb, $where);
            return $this->db->affected_rows();
        }
    }

    /*
    * delete records from extra number table
    */
    function deleteExtraNumber($params = array()){
        $this->db->delete($this->phTbl, $params);     
        if($this->db->affected_rows() > 0){
           return true;
        }
        else{
           return false;
        }
    }

    /*
    * Get all records from label suggestion tble
    */
    function getSuggestions(){
        $this->db->select('*');
        $this->db->from($this->labelTbl);        
        $this->db->where('label_id',1);
        $query = $this->db->get();
        return $query->row_array();        
    }

    /*
    * get rows from text content table
    */
    function getTextContent($args){
        if(!empty($args)){
            return $this->db->get_where($this->ftTbl, array("ft_id"=>$args))->row_array();
        }else{
            return false;
        }
    }

    function checknumberavailable($args){
        $query1 = $this->getRows(array("conditions"=>array("user_phone"=>$args), "returnType"=>"count"));
        $query2 = $this->getExtraNumberRows(array("conditions"=>array("phn_number"=>$args), "returnType"=>"count"));

        if($query1 > 0 || $query2 > 0){
            return true;
        }else{
            return false;
        }
    }

    function insertTransaction($params = array()){
        $this->db->insert($this->pytTbl, $params);
        return $this->db->insert_id();
    }

    /*
    * get rows for business opening time table
    */
    function getEmailTemplateRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->emailTpTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("tmplate_id",$params)){
            $this->db->where('tmplate_id',$params['tmplate_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get packages list
    */
    function getActiavatedPackageRows(){
        $this->db->select("*");        
        return $this->db->get_where($this->usoptMaster, array("opt_type"=>4, "opt_status"=>1))->result_array();
    }

    /*
    * get Purchased package lists
    */
    function getPackageRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usoptMaster);
        $this->db->join($this->usoptprice, "$this->usoptprice.opt_type_id = $this->usoptMaster.opt_id", "left");        
        $this->db->join($this->usOptTbl, "$this->usOptTbl.opt_option_id = $this->usoptMaster.opt_id", "left");
        $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->usOptTbl.opt_tran_id", "left");
        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * get packages option list
    */
    function getPackageOptionRows($params = array()){
        $this->db->select("*");        
        $this->db->from($this->pkgTbl);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = pkg_inc_optid");
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

     /*
    * get user purchased packages option list
    */
    function getUserPurchasedPackageOptionRows($params = array()){
        $this->db->select("*");
        $this->db->from($this->usOptTbl);
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = opt_option_id");        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
               $this->db->where($key,$value);
            }
        }

        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }

        if(array_key_exists("opt_id",$params)){
            $this->db->where('opt_id',$params['opt_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
               $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
               $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
               $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
               $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    function getDebitCardRows(){
        $query = $this->db->order_by("dbt_display_order", "ASC")->get_where("otd_debitcard_images", array("dbt_status"=>1));
        if($query->num_rows() > 0 ){
            return $query->result_array();
        }else{
            return false;
        }
    }

    function insertTable($table, $mode =1, $params = array(), $where = array()){
        if($mode == 1){ // insert testimonial
            $this->db->insert($table, $params);
            $insert_id = $this->db->insert_id();
            if($this->db->affected_rows() > 0)
                return $insert_id;
            else
                return false;
        }
        else{ // update existing testimonial
            $this->db->update($table, $params, $where);
            return true;
        }
    }
}
