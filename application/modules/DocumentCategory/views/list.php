      <div class="import-create dash-counter">          
        <div class="row">
          <div class="col-md-3">
              <div class="dc-sub">
                  <a href="<?php echo base_url('DocumentCategory/Add'); ?>">
                      <div class="dc-sub-ico">
                          <i class="fa fa-file-text-o" aria-hidden="true"></i>
                      </div>
                      <div class="dc-sub-txt pu-txt">
                          <h3>Add</h3>
                          <p>New Document</p>
                      </div>
                  </a>
              </div>
          </div> 
        </div>
      </div>

    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <?php
          if(!empty($this->session->flashdata("success"))){
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-success">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Success!</strong> <?php echo $this->session->flashdata("success"); ?>
              </div>
            </div>
          </div>
          <?php
          }
          if(!empty($this->session->flashdata("error"))){
          ?>
          <div class="row">
            <div class="col-md-12">
              <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <strong>Error!</strong> <?php echo $this->session->flashdata("error"); ?>
              </div>
            </div>
          </div>
          <?php
          }
          ?>
          <h2> Document List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th>No.</th>
                  <th>Name</th>
                  <th>Status</th> 
                  <th>Action</th>
                </tr>
                <?php                
                if(!empty($documents)){
                  $count = 1;
                  foreach($documents as $dc){
                  ?>
                  <tr>
                    <td><?php echo $count; ?></td>
                    <td ><?php echo $dc['ct_name'] ?></td>
                    <td ><?php echo ($dc['ct_status']==1)? 'active':'inactive'; ?></td> 
                    <td>
                      <div class="link-del-view">
                        <div class="tooltip-2"><a href="<?php echo base_url("DocumentCategory/edit/").$dc['ct_id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                          <span class="tooltiptext">Edit</span>
                        </div>
            						<!-- <div class="tooltip-2">
        						      <a href="javascript:;"  data-toggle="confirmation" data-singleton="true" data-popout="true" data-placement="top" class="delete_language" modal-aria="<?php echo $dc['ct_id'];?>" data-original-title="" title=""><i class="fa fa-trash-o" aria-hidden="true"></i></a>
        						      <span class="tooltiptext">Delete</span>
            						</div> -->                        
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
             <div class="read-more">
                      
                        </div>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>