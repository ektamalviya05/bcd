<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class Doc_model extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->docTbl = "otd_contractdoc";        
	}

	/*
     * get rows from the heading content table
     */
    function getRows($params = array()){

        $this->db->select($params['select']);
        $this->db->from($this->docTbl);
  
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("grouping",$params)){            
            $this->db->group_by($params['grouping']);            
        }
                
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);                
            }
        }

        if(array_key_exists("ct_id",$params)){
            $this->db->where('ct_id',$params['ct_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);            
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();

            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        
        return $result;
    }

    /*
    * Insert / Update into heading content table
    */

    function insertTable($opr = 1, $para = array(), $where = array()){
    	if($opr == 1){ // insert testimonial
    		$this->db->insert($this->docTbl, $para);
    		$insert_id = $this->db->insert_id();
    		if($this->db->affected_rows() > 0)
    			return $insert_id;
    		else
    			return false;
    	}
    	else{ // update existing document
    		$this->db->update($this->docTbl, $para, $where);
    		return true;
    	}
    }

	function deleteTable($id){ 
	       $this->db->where("lang_id",$id);
	       $this->db->delete($this->pageHeadTbl);
		   return true;
	
	}
}
?>