<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class DocumentCategory extends MX_Controller{
	function __construct(){
		parent::__construct();
		$userid = $this->session->userdata("user_id");
		if(empty($userid))
			redirect("Admin");
		$this->load->model("Doc_model");
		$this->load->library("session");		
		$this->load->library("form_validation");
	}

	function index(){
		$data = array();
		header("Access-Control-Allow-Origin: *");
		$data['documents'] = $this->Doc_model->getRows(array("select"=>"*", "sorting"=>array("ct_name"=>"ASC")));
		$data['heading'] = "User Documents";		
		$this->template->set('title', 'Language Management');
	    $this->template->load('admin_layout', 'contents' , 'list', $data);
	}
	
	function Add($args = "Add"){
		$this->load->library("form_validation");
		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
		$this->form_validation->set_rules("name", "document name", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("status", "status", "required", array("required"=>"Please select %s"));
		if($this->form_validation->run() == FALSE){
			$data = array();
			header("Access-Control-Allow-Origin: *");
			$args = str_replace("-", " ", $args);
			$data['heading'] = 'Add User Document';
			$this->template->set('title', 'Add User Document');
			$this->template->load('admin_layout', 'contents' , 'add', $data);
		}else{
			$arr = array("ct_name"=>$this->input->post("name"),
						 "ct_status"=>$this->input->post("status")
						);
			$check = $this->Doc_model->insertTable(1, $arr);
			if($check){
				$this->session->set_flashdata("success", "<label class='text-success'>Document details added successfully.");
				redirect(base_url("DocumentCategory"));
			}else{
				$this->session->set_flashdata("error", "<label class='text-danger'>Document details not added, Please try again.");
				redirect(base_url("DocumentCategory"));
			}
		}
	}
	function edit($id){
		$this->load->library("form_validation");
		$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
		$this->form_validation->set_rules("name", "document name", "required", array("required"=>"Please enter %s"));
		$this->form_validation->set_rules("status", "status", "required", array("required"=>"Please select %s"));
		if($this->form_validation->run() == FALSE){
			$data = array();
			header("Access-Control-Allow-Origin: *");
			$data['details'] = $this->Doc_model->getRows(array("select"=>"*", "conditions"=>array("ct_id"=>$id),"returnType"=>"single"));          
			$data['heading'] = 'Edit Document';
			$data['action'] = base_url('Languages/Edit/'.$id);
			$this->template->set('title', 'Edit Document');
			$this->template->load('admin_layout', 'contents' , 'edit', $data);
		}else{
			$arr = array("ct_name"=>$this->input->post("name"),
						 "ct_status"=>$this->input->post("status")
						);
			$check = $this->Doc_model->insertTable(2, $arr, array("ct_id"=>$id));
			if($check){
				$this->session->set_flashdata("success", "<label class='text-success'>Document details updated successfully.");
				redirect(base_url("DocumentCategory"));
			}else{
				$this->session->set_flashdata("error", "<label class='text-danger'>Document details not updated, Please try again.");
				redirect(base_url("DocumentCategory"));
			}
		}
	}

    function update_language(){
		$data['lang_name'] = $this->input->post('name');
		$data['lang_status'] = $this->input->post('status');
		  
		if($this->input->post('lang_id')){
		  
		  $lang_id = $this->input->post('lang_id');
		   
		  $where = array("lang_id"=>$lang_id);
		  $result = $this->Lang_model->insertTable("0",$data,$where);
		  $array = array();
		  if($result){
			   $array['status'] = 200;
			   $array['msg'] = 'Language Update successfully';
			  
		  }else{
			   $array['status'] = 300;
			   $array['msg'] = 'Language not update';
		  }			  
		  
		}else{
			$result = $this->Lang_model->insertTable("1",$data);
			$array = array();
			if($result){
				$array['status'] = 200;
				$array['msg'] = 'Language inserted successfully';
			  
			}else{
				$array['status'] = 300;
				$array['msg'] = 'Language not inserted';
			}
		} 
		echo json_encode($array);		  
	}

    function delete_language(){
		$id = $this->input->post('id');
		$this->Lang_model->deleteTable($id);		 
		$array['status'] = 200;
		echo json_encode($array);
	}	

 
}