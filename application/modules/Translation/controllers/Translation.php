<?php
defined("BASEPATH") OR exit("Direct access not allowed");

class Translation extends MX_Controller{
	function __construct(){
		parent::__construct();
		$userid = $this->session->userdata("user_id");
		if(empty($userid))
			redirect("Admin");
		$this->load->model("Trans_model");
		$this->load->library("session");		
		$this->load->library("form_validation");
	}

	function index($args = "Home"){
		$data = array();
		header("Access-Control-Allow-Origin: *");
		$args = str_replace("-", " ", $args);
		$data['languages'] = $this->Trans_model->getRows(array("select"=>"*", "grouping"=>"ct_language", "conditions"=>array("ct_language"=>"English")));
		$data['heading'] = $args;
		// echo "<pre>";
		// print_r($data['listing']);
		// exit;
		$this->template->set('title', 'Content Management');
	    $this->template->load('admin_layout', 'contents' , 'form', $data);
	}

	function getHeadingdetails(){
		$id = $this->input->post("id");
		if(!empty($id)){
			$query = $this->Trans_model->getRows(array("select"=>"*", "ct_id"=>$id));
			$data = array("id"=>$query['ct_id'],
						  "heading"=>$query['ct_heading'],
						  "subheading"=>$query['ct_subheading'],
						  "section"=>$query['ct_section']
							);
			echo json_encode(array("status"=>200, "content"=>$data));
		}else{
			echo json_encode(array("status"=>1));
		}
	}

	function updateHeadings(){
		$heading = $this->input->post("heading");
		$subheading = $this->input->post("subheading");
		$sectionid = $this->input->post("sectionid");
		if(!empty($sectionid)){
			$para = array("ct_heading"=>$heading,
						  "ct_subheading"=>$subheading						  
						 );
			$check = $this->Trans_model->insertTable(2, $para, array("ct_id"=>$sectionid));
			if($check){
				echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Heading updated successfully.</label>"));
			}else{
				echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Heading not updated, Please try again.</label>"));
			}
		}else{
			echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Invalid Access</label>"));
		}
	}
}