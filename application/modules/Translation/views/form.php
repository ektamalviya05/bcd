<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3><?php echo $heading; ?></h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <ul class="nav nav-tabs">
                    <?php
                    $cnt = 1;
                    foreach($languages as $lang){                      
                    ?>
                      <li class="<?php if($cnt == 1) echo "active"; ?>"><a data-toggle="tab" href="#tab<?php echo $cnt ?>"><?php echo $lang['ct_language']; ?></a></li>                    
                    <?php
                      $cnt++;
                    }                    
                    ?>
                    </ul>
                    <div class="tab-content">
                      <?php
                      $cnt = 1;
                      foreach($languages as $lang){                      
                        ?>
                        <div id="tab<?php echo $cnt ?>" class="tab-pane fade <?php if($cnt == 1) echo "in active"; ?>">
                          <ul class="change-l">
                          <?php 
                          $query = $this->Trans_model->getRows(array("select"=>"ct_id,ct_section", "conditions"=>array("ct_page_title"=>$heading, "ct_language"=>$lang['ct_language'])));
                          foreach($query as $lst){
                            ?>
                              <li>
                                <a href="javascript:;" modal-aria="<?php echo $lst['ct_id'] ?>" class="updateheadingcontent" data-toggle="modal" data-target="#updateheadings"><?php echo $lst['ct_section']; ?></a>
                              </li>
                            <?php
                          }
                          ?>
                          </ul>
                        </div>
                        <?php
                        $cnt++;
                      }
                      ?>
                    </div>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->       