<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
    <!--  Second Top Row   -->
      <!-- <div class="import-create dash-counter">
          <h2>Create</h2>
          <div class="row">
            <div class="col-md-3">
                <div class="dc-sub">
                    <a href="<?php echo base_url("ETemplate/add"); ?>">
                        <div class="dc-sub-ico">
                            <i class="fa fa-file-text-o" aria-hidden="true"></i>
                        </div>
                        <div class="dc-sub-txt pu-txt">
                            <h3>Add</h3>
                            <p>New Template</p>
                        </div>
                    </a>
                </div>
            </div>            
          </div>
      </div> -->


    <!--  Main Containt  -->
      <div class="dash-counter users-main">
          <h2>Email Template List</h2>
          <div class="user-table table-responsive">
            <table class="table table-striped table-bordered">
              <tbody>                              
                <tr >
                  <th>No.</th>
                  <th >Name</th>
                  <!-- <th>Short Description</th> -->
                  <th >Action</th>
                </tr>
                <?php                
                if(!empty($details)){
                  $count = 1;
                  foreach($details as $testi){
                  ?>
                  <tr>
                    <td><?php echo $count; ?></td>
                    <td ><?php echo $testi['tmplate_name'] ?></td>
                    <!-- <td><?php //echo substr($testi['tmplate_content'], 0, 100); ?></td> -->
                    <td>
                      <div class="link-del-view">
                        <div class="tooltip-2"><a href="<?php echo base_url("ETemplate/edit/").$testi['tmplate_id']; ?>"><i class="fa fa-edit" aria-hidden="true"></i></a>
                          <span class="tooltiptext">Edit</span>
                        </div>                          
                      </div>
                    </td>
                  </tr>
                  <?php
                    $count++;
                  }
                }
                ?>
              </tbody>
            </table>
             <div class="read-more">
                       <?php //echo $links ;  ?>
                        </div>
          </div>
          <!-- <div class="read-more">
            <a href="<?php echo base_url().'Admin/user_list'; ?>">
                View more
            </a>
          </div> -->
      </div>                    
  </div>