<script src="<?php echo base_url("assets/ckeditor/ckeditor.js"); ?>"></script>
<!-- page content -->
        <div class="right_col" role="main">          
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Edit Email Template</h3>
              </div>
              <div class="title_right">
                <div class="col-st-5 col-sm-2 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                    <a href="javascript:window.history.back()" class="btn btn-default"> <i class="fa fa-chevron-left"></i> Back </a>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <!-- <div class="x_title">
                    <h2> <small></small></h2>                   
                    <div class="clearfix"></div>
                  </div> -->
                  <div class="x_content">
                    <br />
                    <form id="saveTemplate" enctype="multipart/form-data" method="post" action='<?php echo base_url("ETemplate/edit/").$details['tmplate_id']; ?>' data-parsley-validate class="form-horizontal form-label-left">                                            
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Label:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input class="form-control" readonly value="<?php echo $details['tmplate_name']; ?>">                          
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Email Subject:</label>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <input name="subject" class="form-control" value="<?php echo $details['tmplate_subject']; ?>">                          
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id="ersbmsg">
                        </div>
                      </div>                      
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Description:</label>
                        </div>                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <textarea class="form-control summernote" name="template" id="template"><?php echo $details['tmplate_content']; ?></textarea>
                          <?php echo form_error('template'); ?>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id="ermsg">
                        </div>
                      </div> 
                      <div class="form-group">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                          <label>Variable names:</label>
                        </div>                        
                        <div class="col-md-12 col-sm-12 col-xs-12">
                           <b><?php echo $details['tmplate_variable']; ?></b>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12" id="ermsg">
                        </div>
                      </div> 
					  
                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 ">
                          <button type="submit" id="catsub" class="btn btn-success">Save</button>
                          <span style="display:none;" class="loading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>                          
                          <div id="catupmsg">
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>           
          </div>
        </div>
        <!-- /page content -->
        <script type="text/javascript">
          CKEDITOR.replace( 'template', {
          language: 'en',
          uiColor: '#9AB8F3',
          allowedContent:true,
      });
        </script>