<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ETemplate extends MX_Controller {

  public function __construct(){
  	$userid = $this->session->userdata("user_id");
  	if(empty($userid)) {redirect("Admin");}
  	$this->load->model("Template_model");
  }

  function index(){
  	$data = array();
  	$data['details'] = $this->Template_model->getTemplateRows(array("sorting"=>array("tmplate_name"=>"ASC")));
  	header("Access-Control-Allow-Origin: *");
  	$this->template->set('title', 'Category Management');
    $this->template->load('admin_layout', 'contents' , 'list', $data);
  }

  function Edit($args = 1){
  	$this->load->library("form_validation");
  	$this->form_validation->set_error_delimiters("<label class='error'>", "</label>");
  	$this->form_validation->set_rules("template", "Description", "required", array("required"=>"Please enter %s"));
    $this->form_validation->set_rules("subject", "Email Subject", "required", array("required"=>"Please enter %s"));
  	if($this->form_validation->run() == FALSE){
	  	$data = array();
	  	$data['details'] = $this->Template_model->getTemplateRows(array("tmplate_id"=>$args));
		header("Access-Control-Allow-Origin: *");
		$this->template->set('title', 'Category Management');
	    $this->template->load('admin_layout', 'contents' , 'edit', $data);
	}else{
		$check = $this->Template_model->insertTable(2, array("tmplate_content"=>$this->input->post("template"), "tmplate_subject"=>$this->input->post("subject")), array("tmplate_id"=>$args));
		if($check){
			echo json_encode(array("status"=>200, "msg"=>"<label class='text-success'>Template updated successfully.</label>"));
		}else{
			echo json_encode(array("status"=>1, "msg"=>"<label class='text-danger'>Template not updated, Please try again.</label>"));
		}
	}
  }
}