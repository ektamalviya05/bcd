<?php

function is_user_login() {
    $CI = & get_instance();
    $is_logged_in = $CI->session->userdata('isUserLoggedIn');
    if (!isset($is_logged_in)) {
        redirect(base_url());
    }
}

function is_user_not_login() {
    $CI = & get_instance();
    $is_logged_in = $CI->session->userdata('isUserLoggedIn');
    if (isset($is_logged_in)) {
        redirect(base_url());
    }
}

function text_wrap($test,$text_count) {
    $string = strip_tags($test);
    if (strlen($string) > $text_count) {

        $stringCut = substr($string, 0, $text_count);
        $endPoint = strrpos($stringCut, ' ');
        $string = $endPoint ? substr($stringCut, 0, $endPoint) : substr($stringCut, 0);
        $string .= '...';
    }
    echo $string; 
}

function is_active($main, $sub) {
    $active = '';
    $url = explode('/', $_SERVER['REQUEST_URI']);
    if (!empty($main) && !empty($sub)) {
        if (in_array($main, $url)) {
            if (in_array($sub, $url)) {
                $active = "active";
            } else {
                $active = "";
            }
        } else {
            $active = "";
        }
    } elseif (!empty($main) && empty($sub)) {
        if (in_array($main, $url)) {
            $active = "active";
        } else {
            $active = "";
        }
    } else {
        $active = "";
    }
    echo $active;
}
