<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/*__________________________ Common Query _____________________*/
    //Update
    if(!function_exists('UR'))
    {
        function UR($t, $pd, $wc)
        {
            $ci =&get_instance();
            $ci->db->where($wc);
            $q = $ci->db->update($t,$pd);
            return $q;
        }
    }

    //select single by id
    if(!function_exists('SR'))
    {
        function SR($t,$c)
        {
            $ci =&get_instance();
            $q = $ci->db->get_where($t,$c)->row_array();
            return $q;
        }
    }


    if(!function_exists('AR'))
    {
        function AR($t,$c,$v,$ob='ASC')
        {
            $ci =&get_instance();

            if($ob) {
                $ci->db->order_by($v,$ob);
            }
            $q = $ci->db->get_where($t,$c)->result_array();
            //echo $ci->db->last_query();die;
            return $q;
        }
        
    }

    


    //Insert record 
    if(!function_exists('IR'))
    {
        function IR($t,$pd)
        {
            $ci =&get_instance();
            $ci->db->insert($t,$pd);
            return $ci->db->insert_id();
        }
    }
    //Count Record
    if(!function_exists('CR'))
    {
        function CR($t,$c)
        {
            $ci =&get_instance();
            $q = $ci->db->get_where($t,$c)->num_rows();
            return $q;
        }
    }

    //select single by id
    if(!function_exists('TR'))
    {
        function TR($t)
        {
            $ci =&get_instance();
            $q = $ci->db->get($t)->result_array();
            return $q;
        }
    }

    if(!function_exists('p')) {
        function p($array) {
            echo '<pre>';
            print_r($array);
            echo '</pre>';die;
        }
    }
    //Time
    if ( ! function_exists('ftime'))
    {
        function ftime($time,$f) {
                  if (gettype($time)=='string') 
                  $time = strtotime($time);  
                
                  return ($f==24) ? date("G:i", $time) : date("g:i a", $time);  
                }
    }
/*-------------------------- Common Query ---------------------*/

/*__________________________ Languge __________________________*/
    if(!function_exists('getlanguge'))
    {
        function getlanguge($uid)
        {
           $ci =&get_instance();
           $data = $ci->db->get_where(USERS,array("user_id" => $uid))->row_array();         
            $uid = $data["user_language"];
            return $uid;
        }
    }
    if(!function_exists('message'))
    {
        function message($word)
        {
            global $user_language;        
            $ci =&get_instance();            
            $data = $ci->db->get_where(MESSAGE,array("id" => $word))->row_array();            
            if ($user_language == 'ar') {
                $word = $data["arabic"];   
            }else{
                $word = $data["english"];    
            }
            return $word;
        }
    }
/*-------------------------- Languge --------------------------*/

/*__________________________ Send Mail _________________________*/
    if ( ! function_exists('sendEmail'))
    {
        function sendEmail($email,$subject,$message)
        {
            // print_r($email);print_r($subject);print_r($message);die;
            $ci =&get_instance();
            $from = "votivephp.sumit@gmail.com";
            $ci->load->library('email');
            $config['protocol']    = 'smtp';
            $config['smtp_host']    = 'ssl://smtp.gmail.com';
            $config['smtp_port']    = '465';
            $config['smtp_timeout'] = '7';
            $config['smtp_user']    = $from;
            $config['smtp_pass']    = 'votive123456';
            $config['charset']    = 'utf-8';
            $config['newline']    = "\r\n";
            $config['mailtype'] = 'html'; 
            $config['validation'] = 1;      
            $ci->email->initialize($config);
            //$fromname = "Sales Team";
            $fromemail = $from;
            $mpass = "votive123456";
            $to = $email;
            $from = $from;
            $subject=$subject;
            $message=$message;

            $cc=0;

              $ci->email->from($fromemail);
              $ci->email->to($to);
              $ci->email->subject($subject);       
              $ci->email->message($message);
              //$ci->email->attach('C:\Users\xyz\Desktop\images\abc.png');  
                if($cc) {
                    $ci->email->cc($cc);
                }
                    
              /*      if(!empty($bcc)){
                        $this->email->bcc($bcc);
                    }
                      */

            if(!$ci->email->send()){
              return $ci->email->print_debugger();
            }else{
              return 1;
            }
            }
    }
    if ( ! function_exists('send_mail'))
    {
        function send_mail($msg, $subject, $useremail)
        {          
            $ci =&get_instance();
            $ci->load->library('email');
            $config['mailtype']='html';
            $ci->email->initialize($config);    
            $ci->email->from(ADMIN_EMAIL);
            $ci->email->to($useremail);
            $ci->email->subject($subject);
            $ci->email->message($msg);
            
            if($ci->email->send()) {    
                return true;
            } else {
                return false;
            }
        }
    }
/*-------------------------- Send Mail -------------------------*/


/*__________________________ Notification ______________________*/

    if(!function_exists('send_Notification')) {
        function send_Notification($input) { 

            //print_r($input);die;
            $device_token = $input['noti_deviceid'];
            $user_id = $input['noti_userid'];
            $order_id = $input['noti_orderid'];
            $target = $input['noti_deviceid'];//$_POST['target'];
            $title = $input['noti_title'];
            $description = $input['noti_desc'];
            //$supervisor_id = $input['supervisor_id'];
            $active_methods = array();

            $error = '';
            $error_sess = '';

            //$user_token = get_user_meta( $userid, '_token_id', true );die;

                if(empty($device_token) ) {
                   $error = 'Require Field Missing';
                // } elseif ($user_token_id != $user_token) {
                //     $error_sess = 'Your account is currently logged onto another device.Do you want to continue with this device ?';
                } else {

                    //FCM API end-point
                    $url = 'https://fcm.googleapis.com/fcm/send';
                    //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
                    $server_key = 'AIzaSyDjI1nq1_3w0TIdtV3F8ub4Erxib85byrk';
                    $data = array("via"=>"EPCO", "title"=>$title, "description"=>$description, "order_id" => $order_id);
                    $fields = array();
                    $fields['data'] = $data;    
                        if(is_array($target)){
                            $fields['registration_ids'] = $target;
                        }else{
                            $fields['to'] = $target;
                        }
                        // print_r($input);die;

                        //header with content_type api key
                        $headers = array(
                        'Content-Type:application/json',
                          'Authorization:key='.$server_key
                        );
                        //CURL request to route notification to FCM connection server (provided by Google)  
                        $ch = curl_init();
                        curl_setopt($ch, CURLOPT_URL, $url);
                        curl_setopt($ch, CURLOPT_POST, true);
                        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                        $result = curl_exec($ch);

                        if ($result === FALSE) {
                            return false;
                        }

                        curl_close($ch);
                        $jsn =json_decode($result);                
                        if($jsn->success){
                            return true;
                            
                        }else{
                            return true;
                           
                        }
                    }
        }
    }

    //Test notification

    if(!function_exists('testNotification'))
    {
        function testNotification()
        { 
            $device_token = $_REQUEST['device_token'];
            $userid = $_REQUEST['userid'];
            $user_token_id = $_REQUEST['token_id'];
            $target = $_REQUEST['target'];
            $title = $_REQUEST['title'];
            $description = $_REQUEST['description'];
            $active_methods = array();

            $error = '';
            $error_sess = '';

            $user_token = get_user_meta( $userid, '_token_id', true );

            if( empty($device_token) ) {
               $error = 'Require Field Missing';
            } 
            elseif ($user_token_id != $user_token) {
            $error_sess = 'Your account is currently logged onto another device.Do you want to continue with this device ?';
            }else {

                //FCM API end-point
                $url = 'https://fcm.googleapis.com/fcm/send';
                //api_key available in Firebase Console -> Project Settings -> CLOUD MESSAGING -> Server key
                $server_key = 'AAAAmGIOx8Y:APA91bHgsRHP-jGk7o82oRV_uuGMEfYRnT9r3eDmVTMHKlKnemKkE_0TmygkOR_fKP_KDFj7mFvK5aa6iRfwLMQBEo3pTZw5sw9xV_PHX5aFRy8jORRawbQAycVqxKPewa1l1JJM_WEF';
                $data = array("via"=>"Borex Poissons", "title"=>$title, "description"=>$description);
                $fields = array();
                $fields['data'] = $data;    
                if(is_array($target)){
                    $fields['registration_ids'] = $target;
                }else{
                    $fields['to'] = $target;
                }
                //header with content_type api key
                $headers = array(
                'Content-Type:application/json',
                  'Authorization:key='.$server_key
                );
                //CURL request to route notification to FCM connection server (provided by Google)  
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
                $result = curl_exec($ch);
                if ($result === FALSE) {
                return false;
                }
                curl_close($ch);
                $jsn =json_decode($result);
                if($jsn->success){
                return true;
                }
                else{
                return true;
                }
            }
      
            $ar = array ("result" => 1, "message" => "success");
             echo json_encode($ar);
             exit();
        }
    }
/*-------------------------- Notification ----------------------*/


/*__________________________ Images Uploads ____________________*/
    if(!function_exists('imageUploads')) 
    {
        function imageUploads($image, $name, $folder){
            $imagename = array();
             $ci =&get_instance();
            // echo $folder;
            //  print_r($image);
            //   print_r($name);die;
            $config['upload_path'] = './uploads/'.$folder.'';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['encrypt_name'] = true;         
            $ci->load->library('upload', $config);
            if (!$ci->upload->do_upload($name))
            {
                
               $error = array('error' => $ci->upload->display_errors());
               //print_r($error); die;
            }
            else
            { 
                // if(!empty($image[$param])){
                // //unlink('./uploads/'.$folder.'/'.$image[$param]['name']);
                // }
                $image_name = array('upload_data' => $ci->upload->data());
                
                $imagename = $image_name['upload_data']['file_name'];  
                //$ci->output->clear_all_cache();                      
            }
            //print_r($imagename);die;
            return $imagename;
        }
    }
    if(!function_exists('multipulImage')) 
    {
        function multipulImage($image, $name, $folder){
            //echo $folder; die;

            //;print_r($name);//die;
            $imagename = array();
            $ci =&get_instance();
            foreach ($name as $param) {                
                if (array_key_exists($param, $image) && ($image[$param] != '')) {
                    //$b = $image[$param]; print_r($b);die;
                    $config['upload_path'] = './uploads/'.$folder.'';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['encrypt_name'] = true;         
                    $ci->load->library('upload', $config);
                    if (!$ci->upload->do_upload($param))
                    {
                        $error = array('error' => $ci->upload->display_errors());
                    }
                    else
                    { 
                        // if(!empty($image[$param])){
                        // //unlink('./uploads/'.$folder.'/'.$image[$param]['name']);
                        // }
                        $image_name = array('upload_data' => $ci->upload->data());
                        
                        $imagename[$param] = $image_name['upload_data']['file_name'];   
                        //$ci->output->clear_all_cache();                     
                    }                                       
                }
            }
            //print_r($imagename);die;
            return $imagename;           
        }
    }
/*-------------------------- Images Uploads --------------------*/

/*__________________________ Cheack Required Field ______________*/

    /* For valid Login and valid jeson formate or cheack reqiuired paremeter*/
    if(!function_exists('check_required_and_valid')) {
        function check_required_and_valid($chk_params, $converted_array) {
            if (empty($converted_array)) {
               $Error = array('status' => '201', 'message' => message('1'), 'response' => new stdclass);
            }else{
                foreach ($chk_params as $param) {
                    if (array_key_exists($param, $converted_array) && ($converted_array[$param] != '')) {
                        $Error = 0;
                    } else {
                        $Error = array('status' => '203', 'message' => message('3').strtoupper($param), 'response' => new stdclass);
                        break;
                    }
                }                
                if ($Error == 0) {
                    $ci =&get_instance();
                    $userdata = $ci->db->get_where(USERS, array("user_id" => $converted_array['user_id'], "is_online" => 1))->row_array(); 
                    if (!empty($userdata)) {
                        if ($userdata['status'] == "1") {                        
                            if ($userdata['device_id'] != $converted_array['device_id'] && $userdata['is_online'] == "1") {
                                $Error = array('status' => '202', 'message' => message('2'), 'response' => array('user_id' => $converted_array['user_id'])); 
                            }else{
                                $Error = 0;
                            }
                        }else{
                            $Error = array('status' => '206', 'message' =>  message('8'), 'response' => new stdclass);

                        }                       
                    }else{
                        $Error = array('status' => '100', 'message' =>  message('5'), 'response' => new stdclass);
               
                    }
                   
                }
            }

            return $Error;
        }
    }
    /* For only cheack reqiuired paremeter*/

    if(!function_exists('check_required_value')) {
        function check_required_value($chk_params, $converted_array) {
            if (!empty($converted_array)) {
                foreach ($chk_params as $param) {
                    if (array_key_exists($param, $converted_array) && ($converted_array[$param] != '')) {
                        $Error = 0;
                    } else {
                        $Error = array('status' => '203', 'message' => message('3').ucfirst($param).'!', 'response' => new stdclass);
                        break;
                    }
                }               
            }else{
                $Error = array('status' => '201', 'message' => message('1'), 'response' => new stdclass);
            }
            return $Error;
        }
    }
/*-------------------------- Cheack Required Field --------------*/

/*__________________________ SMS API ____________________________*/

    if(!function_exists('smsapi')) 
    {
      function smsapi($otp,$mobile){

        $sid = 'ACf3c15917b08e13ca3ae4c5bb91839065';
        $token = '57bd920870e7a35862931fcf3333cd86';
        $client = new Client($sid, $token);
        // Use the client to do fun stuff like send text messages!
        $client->messages->create(
            // the number you'd like to send the message to
            //'+91'.$mobile,
            '+972'.$mobile,
            array(
                // A Twilio phone number you purchased at twilio.com/console
                'from' => '+972526285726',
                // the body of the text message you'd like to send
                'body' => $otp. "Please verify this otp in v12mobile"
            )
        );
      }
    }
/*-------------------------- SMS API ----------------------------*/

class Paypal {
    /**
     * Last error message(s)
     * @var array
     */
    protected $_errors = array();
    /**
     * API Credentials
     * Use the correct credentials for the environment in use (Live / Sandbox)
     * @var array
     */
    protected $_credentials = array(
        'USER' => PAYPAL_MERCHANT_USER,
        'PWD' => PAYPAL_MERCHANT_PWD,
        'SIGNATURE' => PAYPAL_MERCHANT_SIGNATURE,
    );
    /**
     * API endpoint
     * Live - https://api-3t.paypal.com/nvp
     * Sandbox - https://api-3t.sandbox.paypal.com/nvp
     * @var string
     */
    protected $_endPoint = PAYPAL_API_URL;
    /**
     * API Version
     * @var string
     */
    protected $_version = '86.0';
    /**
     * Make API request
     *
     * @param string $method string API method to request
     * @param array $params Additional request parameters
     * @return array / boolean Response array / boolean false on failure
     */
    public function request($method,$params = array()) {
        $this -> _errors = array();
        if( empty($method) ) { //Check if API method is not empty
            $this -> _errors = array('API method is missing');
            return false;
        }
        //Our request parameters
        $requestParams = array(
            'METHOD' => $method,
            'VERSION' => $this -> _version
        ) + $this -> _credentials;
        //Building our NVP string
        $request = http_build_query($requestParams + $params);
        //cURL settings
        $curlOptions = array (
            CURLOPT_URL => $this -> _endPoint,
            CURLOPT_VERBOSE => 1,
            /*
             * If you are using API Signature rather then certificates, leave the code below commented out
             */
          //  CURLOPT_SSL_VERIFYPEER => true,
          //  CURLOPT_SSL_VERIFYHOST => 2,
           // CURLOPT_CAINFO => dirname(__FILE__) . '/cacert.pem', //CA cert file
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => $request
        );
        $ch = curl_init();
        curl_setopt_array($ch,$curlOptions);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);        //  Skip peer certificate verification   - Comment this if you are using Certificates instead of API Signature
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);        // Skip host certificate verification    - Comment this as well if you are using Certificates instead of API Signature
        //Sending our request - $response will hold the API response
        $response = curl_exec($ch);
        //Checking for cURL errors
        if (curl_errno($ch)) {
            $this -> _errors = curl_error($ch);
            curl_close($ch);
            return false;
            //Handle errors
        } else  {
            curl_close($ch);
            $responseArray = array();
            parse_str($response,$responseArray); // Break the NVP string to an array
            return $responseArray;
        }
    }

    /*public function test() {
        echo "checking....";
    }*/

}

if(!function_exists('getLocationCity')) 
{
    function getLocationCity($address){

        $address = str_replace (" ", "+", urlencode($address));
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"/*,
            "postman-token: fcd9d910-5ce7-97d6-e949-12c5aa2fe7b6"*/
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          echo "cURL Error #:" . $err;
          //echo false;
        } else {
          $arr = json_decode($response);
          $city = false;
          foreach ($arr->results[0]->address_components as $key => $val) 
          {
              if($val->types[0] == 'locality')
              {
                $city = $val->long_name;
              }
          }
          echo $city;
        }
    
    }
}

if(!function_exists('getLocationLatLng')) 
{
    function getLocationLatLng($address){

        $address = str_replace (" ", "+", urlencode($address));
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://maps.googleapis.com/maps/api/geocode/json?address=".$address."&key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache"/*,
            "postman-token: fcd9d910-5ce7-97d6-e949-12c5aa2fe7b6"*/
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
          return false;
          //echo false;
        } else {
          $arr = json_decode($response);
          
          return array('lat' => $arr->results[0]->geometry->location->lat,'lng' => $arr->results[0]->geometry->location->lng);
        }
    
    }
}

if(!function_exists('get_client_ip')) 
{
    function get_client_ip() {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }
}

if(!function_exists('getIpLatLng')) 
{
    function getIpLatLng(){
        $ipAddress = get_client_ip();
        //$ipAddress = "IP_ADDRESS";
        $ip_key = "02b8721d03be69ddd4abb2cde4338b51960bdead0efb4f80dd3ba12816232240";
        $query = "http://api.ipinfodb.com/v3/ip-city/?key=" . $ip_key . "&ip=" . $ipAddress . "&format=json";
        $json = file_get_contents($query);
        $data = json_decode($json, true);
        if ($data['statusCode'] == "OK") {
            return $data;
        } else {
            return false;
        }
    }
}

if(!function_exists('check_cuntry_city')) {
        function check_country_city($cities, $country) {
            //print_r($cities); print_r($country);//die;
            //$country = AR(COUNTRIES, array('add_status !=' => 3));
            $city = AR(CITIES, array('add_status !=' => 3));
            foreach ($city as $cityname) {
              $people[] = strtolower($cityname['name']);

             
            }        //print_r($people);die;   
            if (!empty($people)) {

                if (in_array($cities, $people))
                {    
           
                    $countree = AR(COUNTRIES, array('add_status !=' => 3));  

                    foreach ($countree as $countryname) {
                      $countrypeople[] = strtolower($countryname['name']);


                    }//print_r($countrypeople);die;  
                    if ($countrypeople) {

                        if (in_array($country, $countrypeople)) {
                            $Error ="0";
                            //return $Error;
                        }else {                         
                            $Error = array('status' => '201', 'message' =>'This Country not Activated', 'response' => new stdclass);
                        }
                    }
                }
                else
                {
                    $Error = array('status' => '201', 'message' =>'This City not Activated', 'response' => new stdclass);
                }                         
            }else{
                $Error = array('status' => '201', 'message' =>'1', 'response' => new stdclass);
            }
            return $Error;
        }
    }

    if(!function_exists('idToName')) {
        function idToName($t, $id, $name) {
            $ci =&get_instance();
            $q = $ci->db->get_where($t, array('cat_id' => $id))->row_array();
            if (!empty($q)) {
            return $q[$name];
            }else{
                return "0";
            }
            
        }
    }