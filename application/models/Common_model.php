<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model 
{

/*________________________NEW FUNCTION MAKE HEAR________________________________________*/



/*________________________NEW FUNCTION MAKE HEAR________________________________________*/
    
    public function showUserPlane($user_id){

        $this->db->select('*');
        $this->db->Where(array('userid'=> $user_id, 'plan_valid'=> 1, 'package_type !='=> 3, 'st' => "success"));    
        $this->db->from(TRANSACTION);
        $this->db->JOIN(PACKAGE, ''.PACKAGE.'.opt_id = '.TRANSACTION.'.packageid' , 'left');
        $query = $this->db->get();
        $marg =$query->row_array();

        if (!empty($marg)) {  

            $results['package'] = array('user_id' => $marg['userid'],
                            'packageid' => $marg['opt_id'],
                            'packegname' => $marg['opt_name'],            
                            'price' => $marg['price'],      
                            'Plane_start' => $marg['free_start'],
                            'Plane_end' => $marg['free_end'],
                            'status' => $marg['st'],
                            'description' => $marg['opt_description']
                            );
        }

        $this->db->select('*');
        $this->db->Where(array('userid'=> $user_id, 'plan_valid'=> 1, 'package_type'=> 3, 'st' => "success"));    
        $this->db->from(TRANSACTION);
        $this->db->JOIN(FEATURES, ''.FEATURES.'.id = '.TRANSACTION.'.packageid' , 'left');
        $query = $this->db->get();
        $margs =$query->row_array();
        if (!empty($margs)) {       

            $results['features'] = array('user_id' => $margs['userid'],
                            'packageid' => $margs['id'],
                            'packegname' => $margs['package_title'],            
                            'price' => $margs['package_price'],      
                            'features_start' => $margs['free_start'],
                            'features_end' => $margs['free_end'],
                            'status' => $margs['st'],
                            'description' => $margs['package_desc'],
                            'business_id' => $margs['business_id']
                            );
        }
        return $results;
    }



    public function getvalidity($user_id){

        $this->db->order_by('tid','DESC');
        $this->db->select('*');
        $this->db->Where(array('userid' => $user_id, 'free_end >' => date("Y-m-d"), 'st' => "success", 'business_id' => 0));    
        $this->db->from(TRANSACTION);
        $query = $this->db->get();
        $results =$query->result_array();
        if (!empty($results) && $results > "0") {
            for ($i=0; $i < count($results); $i++) {            
                //$data = $results[$i]['packageid'];
                $data = $results[$i]['free_end'];

            }             
        }
        return $data;

    }






    public function transactionHistory($user_id,$limit,$offset){

        $offset = $limit*$offset;
        $this->db->order_by('tid','DESC');
        $this->db->select('*');
        $this->db->Where(array('userid' => $user_id));    
        $this->db->from(TRANSACTION);      
        $this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $results =$query->result_array();
        return $results;
    }



    public function getAllRatinges($business_id,$limit,$offset){

        $offset = $limit*$offset;
        $this->db->order_by('id','DESC');
        $this->db->select(RATE.'.*,'.BUSINESS.'.business_title,'.USERS.'.user_profile_pic,'.USERS.'.user_firstname');
        $this->db->Where(array(''.BUSINESS.'.status' => 1,''.RATE.'.status' => 1,''.BUSINESS.'.business_id' => $business_id));    
        $this->db->from(RATE);      
        $this->db->JOIN(BUSINESS, ''.BUSINESS.'.business_id = '.RATE.'.business_id' , 'left');
        $this->db->JOIN(USERS, ''.USERS.'.user_id = '.RATE.'.user_id' , 'left');
        $this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $results =$query->result_array();
        //print_r($results);die;
        return $results;
    }
    public function countAllRatinges($business_id,$limit,$offset){

        //$offset = $limit*$offset;
        //$this->db->order_by('id','DESC');
        $this->db->select('*');
        $this->db->Where(array(''.BUSINESS.'.status' => 1,''.RATE.'.status' => 1,''.BUSINESS.'.business_id' => $business_id));  
        $this->db->from(RATE);      
        $this->db->JOIN(BUSINESS, ''.BUSINESS.'.business_id = '.RATE.'.business_id' , 'left');
        //$this->db->JOIN(USERS, ''.USERS.'.user_id = '.RATE.'.user_id' , 'left');
        //$this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $results =$query->num_rows();
        //print_r($results);die;
        return $results;
    }


    function getSearchField($search, $lat, $lon, $limit, $offset, $name) {


        $offset = $limit*$offset;
        $strQuery1 = "SELECT
                     `bdc_business`.*,`otd_business_category`.`cat_id`,`otd_business_category`.`cat_name`,
                     ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) )
                      AS distance 
                      FROM bdc_business 
                      RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` 
                      WHERE `cat_status` = 1 AND `status` = 1 AND (
                      business_title LIKE '%$name%' AND cat_name LIKE '%$search%')
                      HAVING distance < 100 ORDER BY `features_ads` = 'DESC' 
                      LIMIT $limit OFFSET $offset";
        $query = $this->db->query($strQuery1);
        $result = $query->result_array();
        //print_r($result);die;

        if ($result) {
            for ($i=0; $i < count($result); $i++) {

                $results[$i]['user_id'] = $result[$i]['user_id'];
                $results[$i]['business_id'] = $result[$i]['business_id'];
                $results[$i]['cat_id'] = $result[$i]['cat_id'];
                $results[$i]['cat_name'] = $result[$i]['cat_name'];
                $results[$i]['business_title'] = $result[$i]['business_title'];
                $results[$i]['business_desc'] = $result[$i]['business_desc'];
                $results[$i]['business_country'] = $result[$i]['business_country'];
                if (!empty($result[$i]['business_city'])) {
                    $results[$i]['business_city'] = $result[$i]['business_city'];
                }else{
                    $results[$i]['business_city'] = "";
                }                
                $results[$i]['lat'] = $result[$i]['lat'];
                $results[$i]['lon'] = $result[$i]['lon'];
                $results[$i]['distance'] = number_format($result[$i]['distance'],5)." ".Km;
                $results[$i]['business_price'] = $result[$i]['business_price'];
                $results[$i]['business_email'] = $result[$i]['business_email'];
                $results[$i]['business_mobile'] = $result[$i]['business_mobile'];
                $results[$i]['business_address'] = $result[$i]['business_address'];
                $results[$i]['validate'] = $result[$i]['validate'];
                $results[$i]['status'] = $result[$i]['status'];
                $results[$i]['features_ads'] = $result[$i]['features_ads'];
                $results[$i]['business_image1'] = BUSINESSPATH . $result[$i]['business_image1']; 
                $chk = $this->common_model->getratshows();
            if ($chk == 1) {

                $rat = $this->common_model->getAvgByid($results[$i]['business_id']);
                if (!empty($rat)) {
                    $results[$i]['ratshow'] = "1"; 
                    $results[$i]['rating'] = $rat; 
                }else{
                    $results[$i]['ratshow'] = "1"; 
                    $results[$i]['rating'] = "0"; 
                }
            }else{
                $results[$i]['ratshow'] = "0";
                $results[$i]['rating'] = "0"; 
            }
            if (!empty($user_id)) {

                $favourite = $this->common_model->getfavByid($result[$i]->business_id,  $user_id);

                if (!empty($favourite)) {
                    $results[$i]['favourite'] = $favourite;     
                }else{
                    $results[$i]['favourite'] = "0";
                } 
            }else{
                 $results[$i]['favourite'] = "0";
            } 
            }
        }
        //print_r($result);die;
        // $query = $this->db->select('*')->from($table)
        //->where("cat_id LIKE '%$search%'  OR business_title LIKE '%$search%' ")->get();
        return $results;
    }

    function countSearchField($search, $lat, $lon, $name) {

        $strQuery1 = "SELECT
                     `bdc_business`.*,`otd_business_category`.`cat_id`,
                     ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) )
                      AS distance 
                      FROM bdc_business 
                      RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` 
                      WHERE `cat_status` = 1 AND `status` = 1 AND (
                      business_title LIKE '%$name%' AND cat_name LIKE '%$search%')
                      HAVING distance < 100 ORDER BY `features_ads` = 'DESC' 
                      ";
        $query = $this->db->query($strQuery1);
        $result = $query->num_rows();        
        return $result;
    }




    public function getEnquiryByid($user_id,$limit,$offset){

        $offset = $limit*$offset;
        $this->db->order_by('id','DESC');
        $this->db->select(ENQUIRY. '.*,'.BUSINESS.'.business_title'); 
                  
        //$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
        $this->db->Where(array(''.BUSINESS.'.status' => 1,''.ENQUIRY.'.user_id' => $user_id));    
        $this->db->from(ENQUIRY);      
        $this->db->JOIN(BUSINESS, ''.BUSINESS.'.business_id = '.ENQUIRY.'.business_id' , 'left');
        $this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $results =$query->result_array();
        return $results;
    }
    public function countEnquiryByid($user_id,$limit,$offset){

        //$offset = $limit*$offset;
        //$this->db->order_by('id','DESC');
        $this->db->select('*'); 
                  
        //$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
        $this->db->Where(array(''.BUSINESS.'.status' => 1));    
        $this->db->from(ENQUIRY);      
        $this->db->JOIN(BUSINESS, ''.BUSINESS.'.business_id = '.ENQUIRY.'.business_id' , 'left');
        //$this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $results =$query->num_rows();
        return $results;
    }



    public function getBussinessList($user_id,$cat_id,$limit,$offset){

		$offset = $limit*$offset;
    	$this->db->order_by('features_ads','DESC');
		$this->db->select('*');	
		if (!empty($cat_id)) {
	    	$this->db->Where(array(''.BUSINESS.'.cat_id'=> $cat_id));
	    }			
		$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
		$this->db->Where(array(''.BUSINESS.'.status' => 1));	
	    $this->db->from(BUSINESS);	    
	    $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');
        //$this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.sub_cat_id' , 'left');        
	    $this->db->limit($limit, $offset);		
		$query = $this->db->get();
        $result =$query->result_array();

        	for ($i=0; $i <count($result); $i++) {

            $results[$i]['business_id'] = $result[$i]['business_id'];
            $results[$i]['user_id'] = $result[$i]['user_id']; 
            $results[$i]['cat_id'] = $result[$i]['cat_id'];             
            $results[$i]['business_title'] = $result[$i]['business_title'];             
            $results[$i]['business_desc'] = $result[$i]['business_desc'];             
            $results[$i]['business_address'] = $result[$i]['business_address'];           
            $results[$i]['business_country'] = $result[$i]['business_country'];
            $results[$i]['business_city'] = $result[$i]['business_city'];                       
            $results[$i]['lat'] = $result[$i]['lat'];           
            $results[$i]['long'] = $result[$i]['lon'];          
            $results[$i]['distance'] = "";          
            $results[$i]['business_image1'] = BUSINESSPATH . $result[$i]['business_image1'];            
            $results[$i]['features_ads'] = $result[$i]['features_ads'];

            $chk = $this->common_model->getratshows();
            if ($chk == 1) {

	            $rat = $this->common_model->getAvgByid($results[$i]['business_id']);
	            if (!empty($rat)) {
	            	$results[$i]['ratshow'] = "1"; 
	                $results[$i]['rating'] = $rat; 
	            }else{
	            	$results[$i]['ratshow'] = "1"; 
	                $results[$i]['rating'] = "0"; 
	            }
	        }else{
	        	$results[$i]['ratshow'] = "0";
	        	$results[$i]['rating'] = "0"; 
	        }
            if (!empty($user_id)) {

                $favourite = $this->common_model->getfavByid($result[$i]['business_id'],  $user_id);

                if (!empty($favourite)) {
                    $results[$i]['favourite'] = $favourite;     
                }else{
                    $results[$i]['favourite'] = "0";
                } 
            }else{
                 $results[$i]['favourite'] = "0";
            }
        }
        return $results;
	}



    public function bussListLatLong($lat,$lon,$cat_id,$user_id,$limit,$offset){

        $offset = $limit*$offset;
        if ($cat_id == "0") {
            $strQuery1 = "SELECT `bdc_business`.*,`otd_business_category`.*,( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) AS distance FROM bdc_business RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` WHERE `cat_status` = 1 AND `status` = 1 HAVING distance < 100 ORDER BY `features_ads` = 'DESC' LIMIT $limit OFFSET $offset";
        }else{
            $strQuery1 = "SELECT `bdc_business`.*,`otd_business_category`.*,( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) AS distance FROM bdc_business RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` WHERE (`bdc_business`.`cat_id`= $cat_id OR `bdc_business`.`sub_cat_id`= $cat_id) AND `cat_status` = 1 AND `status` = 1 HAVING distance < 100 ORDER BY `features_ads` = 'DESC' LIMIT $limit OFFSET $offset";

        }
        
        
        $query = $this->db->query($strQuery1);
        $result = $query->result();
        //echo count($result);//die;
        //print_r($result);die;
        
        for ($i=0; $i <count($result); $i++) {

            $results[$i]['business_id'] = $result[$i]->business_id;
            $results[$i]['user_id'] = $result[$i]->user_id;
            $results[$i]['cat_id'] = $result[$i]->cat_id;
            $results[$i]['sub_cat_id'] = $result[$i]->sub_cat_id;
            $results[$i]['business_title'] = $result[$i]->business_title;
            $results[$i]['business_desc'] = $result[$i]->business_desc;
            $results[$i]['business_address'] = $result[$i]->business_address;           
            $results[$i]['business_country'] = $result[$i]->business_country;
            if (!empty($result[$i]->business_city)) {
                $results[$i]['business_city'] = $result[$i]->business_city;
            }else{
                $results[$i]['business_city'] = "";
            }
            $results[$i]['lat'] = $result[$i]->lat;           
            $results[$i]['long'] = $result[$i]->lon;            
            $results[$i]['distance'] = number_format($result[$i]->distance,5)." ".Km;       
            $results[$i]['business_image1'] = BUSINESSPATH . $result[$i]->business_image1;            
            $results[$i]['features_ads'] = $result[$i]->features_ads;

            $chk = $this->common_model->getratshows();
            if ($chk == 1) {

                $rat = $this->common_model->getAvgByid($results[$i]['business_id']);
                if (!empty($rat)) {
                    $results[$i]['ratshow'] = "1"; 
                    $results[$i]['rating'] = $rat; 
                }else{
                    $results[$i]['ratshow'] = "1"; 
                    $results[$i]['rating'] = "0"; 
                }
            }else{
                $results[$i]['ratshow'] = "0";
                $results[$i]['rating'] = "0"; 
            }
            if (!empty($user_id)) {

                $favourite = $this->common_model->getfavByid($result[$i]->business_id,  $user_id);

                if (!empty($favourite)) {
                    $results[$i]['favourite'] = $favourite;     
                }else{
                    $results[$i]['favourite'] = "0";
                } 
            }else{
                 $results[$i]['favourite'] = "0";
            }
        }
        return $results;
    }
    public function countbussListLatLong($lat,$lon,$cat_id){

        if ($cat_id == "0") {
            $strQuery1 = "SELECT `bdc_business`.*,`otd_business_category`.*,( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) AS distance FROM bdc_business RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` WHERE `cat_status` = 1 AND `status` = 1 HAVING distance < 100";
        }else{
            $strQuery1 = "SELECT `bdc_business`.*,`otd_business_category`.*,( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) ) AS distance FROM bdc_business RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` WHERE (`bdc_business`.`cat_id`= $cat_id OR `bdc_business`.`sub_cat_id`= $cat_id) AND `cat_status` = 1 AND `status` = 1 HAVING distance < 100";

        }
        
        $query = $this->db->query($strQuery1);
        $results = $query->num_rows();

        return $results;
    }    


	public function myAllList($whr, $limit, $offset){

		$date = date("Y-m-d");
        $whr=array(array('user_id' => $user_id, 'validate >=' =>$date, 'features_ads' =>0, 'status' =>"1" ,"cat_status" => 1),
            array('user_id' =>$user_id, 'validate >=' =>$date, 'features_ads' =>1, 'status' =>"1", "cat_status" => 1),
            array('user_id' => $user_id, 'validate <' => $date, 'status' => "0"));                
        $offset = array($offset1, $offset2, $offset3);
        //print_r($offset);die;
            for ($i=0; $i < 3; $i++) {

                $results[$i]['total'] = (string)$this->countallMyListbuss($whr[$i]);
                $count = $this->common_model->totalOffset($results[$i]['total'], $limit);
                $results[$i]['maxoffset'] = (string)$count['numberOffset'];
                $result[$i]['data'] = $this->allMyListbuss($whr[$i], $limit, $offset[$i]);
                //print_r($result[$i]['data']);//die;
                if (!empty($result[$i]['data'])) {
                    //print_r($result[$i]['data']);die;
                    for ($j=0; $j < count($result[$i]['data']); $j++) {
                        $results[$i]['data'][$j]['business_id'] = $result[$i]['data'][$j]['business_id'];
                        $results[$i]['data'][$j]['user_id'] = $result[$i]['data'][$j]['user_id']; 
                        $results[$i]['data'][$j]['cat_id'] = $result[$i]['data'][$j]['cat_id'];             
                        $results[$i]['data'][$j]['business_title'] = $result[$i]['data'][$j]['business_title'];             
                        $results[$i]['data'][$j]['business_desc'] = $result[$i]['data'][$j]['business_desc'];             
                        $results[$i]['data'][$j]['business_address'] = $result[$i]['data'][$j]['business_address'];           
                        $results[$i]['data'][$j]['business_country'] = $result[$i]['data'][$j]['business_country']; 
                        $results[$i]['data'][$j]['business_city'] = $result[$i]['data'][$j]['business_city']; 

                        $results[$i]['data'][$j]['lat'] = $result[$i]['data'][$j]['lat'];           
                        $results[$i]['data'][$j]['long'] = $result[$i]['data'][$j]['lon'];   
                        $results[$i]['data'][$j]['distance'] = "";   
                        $results[$i]['data'][$j]['business_image1'] = BUSINESSPATH . $result[$i]['data'][$j]['business_image1'];
                        $results[$i]['data'][$j]['business_image2'] = BUSINESSPATH . $result[$i]['data'][$j]['business_image2'];
                        $results[$i]['data'][$j]['business_image3'] = BUSINESSPATH . $result[$i]['data'][$j]['business_image3']; 
                        $results[$i]['data'][$j]['business_image4'] = BUSINESSPATH . $result[$i]['data'][$j]['business_image4'];
                        //$results[$i][$j]['favourite'] = $this->common_model->getfavByid($results[$i][$j]['business_id'],  $input['user_id']);
                        $chk = $this->common_model->getratshows();
                        
                        if ($chk == 1) {                  
                        
                            $rat = $this->common_model->getAvgByid($results[$i][$j]['business_id']);
                            if (!empty($rat)) {
                                $results[$i]['data'][$j]['rating'] = $rat;
                                $results[$i]['data'][$j]['ratshow'] = "1";
                            }else{
                                $results[$i]['data'][$j]['rating'] = "0";
                                $results[$i]['data'][$j]['ratshow'] = "1"; 
                            }
                        }else{
                            $results[$i]['data'][$j]['rating'] = "0";
                            $results[$i][$j]['data']['ratshow'] = "0";
                        }
                        // if (!empty($input['user_id'])) {
                        //     $favourite = $this->common_model->getfavByid($results[$i][$j]['business_id'],  $input['user_id']);
                        //     if (!empty($favourite)) {
                        //         $results[$i]['data'][$j]['favourite'] = $favourite;     
                        //     }else{
                        //         $results[$i]['data'][$j]['favourite'] = "0";
                        //     } 
                        // }
                    }
                }else{
                   $results[$i]['data'] = array(); 
                }                    
            }//die;
        return $results;
	}


    public function getFeaturedList($whr, $limit, $offset){

        $this->db->select('*');
        $this->db->Where($whr);
        $this->db->from(BUSINESS);      
        $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');
        $offset = $limit*$offset;
        $this->db->limit($limit, $offset);
        $query = $this->db->get();
        $result = $query->result_array();
        //return $result;
        for ($i=0; $i <count($result); $i++) {
            $results[$i]['business_id'] = $result[$i]['business_id'];
            $results[$i]['user_id'] = $result[$i]['user_id']; 
            $results[$i]['cat_id'] = $result[$i]['cat_id'];             
            $results[$i]['business_title'] = $result[$i]['business_title'];             
            $results[$i]['business_desc'] = $result[$i]['business_desc'];             
            $results[$i]['business_address'] = $result[$i]['business_address'];           
            $results[$i]['business_country'] = $result[$i]['business_country'];
            $results[$i]['business_city'] = $result[$i]['business_city'];                       
            $results[$i]['lat'] = $result[$i]['lat'];           
            $results[$i]['long'] = $result[$i]['lon'];           
            $results[$i]['distance'] = "";           
            $results[$i]['business_image1'] = BUSINESSPATH . $result[$i]['business_image1'];            
            $results[$i]['features_ads'] = $result[$i]['features_ads'];
            $results[$i]['status'] = $result[$i]['status'];

            $chk = $this->common_model->getratshows();
            if ($chk == 1) {

                $rat = $this->common_model->getAvgByid($results[$i]['business_id']);
                if (!empty($rat)) {
                    $results[$i]['ratshow'] = "1"; 
                    $results[$i]['rating'] = $rat; 
                }else{
                    $results[$i]['ratshow'] = "1"; 
                    $results[$i]['rating'] = "0"; 
                }
            }else{
                $results[$i]['ratshow'] = "0";
                $results[$i]['rating'] = "0"; 
            }
            if (!empty($user_id)) {

                $favourite = $this->common_model->getfavByid($result[$i]['business_id'],  $user_id);

                if (!empty($favourite)) {
                    $results[$i]['favourite'] = $favourite;     
                }else{
                    $results[$i]['favourite'] = "0";
                } 
            }else{
                 $results[$i]['favourite'] = "0";
            }
        }
        return $results;
    }


    public function allMyListbuss($whr, $limit, $offset){
        //print_r($whr);die;
        $offset = $limit*$offset;
        //$this->db->order_by('features_ads','DESC');
        $this->db->select('*');                   
        //$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
        //$this->db->Where(array(''.BUSINESS.'.status' => 1));   
        $this->db->Where($whr);   
        $this->db->from(BUSINESS);      
        $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');
        $this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $result =$query->result_array();
        return $result;
    }


    public function countallMyListbuss($whr){

        //$offset = $limit*$offset;
        //$this->db->order_by('features_ads','DESC');
        $this->db->select('*');                   
        //$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
        //$this->db->Where(array(''.BUSINESS.'.status' => 1));   
        $this->db->Where($whr);   
        $this->db->from(BUSINESS);      
        $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');
        //$this->db->limit($limit, $offset);      
        $query = $this->db->get();     
        $result =$query->num_rows();
        return $result;
    }
    	

    public function allFavBusinessList($user_id, $limit, $offset){

    	$offset = $limit*$offset;
    	$this->db->order_by('features_ads','DESC');
		$this->db->select('*');		
		$this->db->Where(array(''.FAVOURITE.'.user_id' => $user_id,  ''.FAVOURITE.'.favourite' => 1));	
		$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
		$this->db->Where(array(''.BUSINESS.'.status' => 1));	
	    $this->db->from(FAVOURITE);
	    $this->db->JOIN(BUSINESS, ''.FAVOURITE.'.business_id = '.BUSINESS.'.business_id' , 'left');
	    $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');
	    $this->db->limit($limit, $offset);
		
		$query = $this->db->get();
	    $busslist = $query->result_array();
	    //print_r($busslist);die;
	    //if (!empty($busslist)) {

	     	for ($i=0; $i < count($busslist); $i++) {
	     		if (!empty($busslist[$i]['business_id'])) {
	     		
                $results[$i]['business_id'] = $busslist[$i]['business_id'];
                $results[$i]['user_id'] = $busslist[$i]['user_id']; 
                $results[$i]['cat_id'] = $busslist[$i]['cat_id'];             
                $results[$i]['business_title'] = $busslist[$i]['business_title'];             
                $results[$i]['business_desc'] = $busslist[$i]['business_desc'];             
                $results[$i]['business_address'] = $busslist[$i]['business_address'];           
                $results[$i]['business_country'] = $busslist[$i]['business_country'];           
                $results[$i]['business_city'] = $busslist[$i]['business_city'];           
                $results[$i]['business_image1'] = BUSINESSPATH.$busslist[$i]['business_image1'];
                $results[$i]['features_ads'] = $busslist[$i]['features_ads'];
                $results[$i]['lat'] = $busslist[$i]['lat'];
                $results[$i]['lon'] = $busslist[$i]['lon'];
                $results[$i]['distance'] = "";
                $results[$i]['favourite'] = "1";

                $chk = $this->getratshows();
                if ($chk == 1) {

	                $rat = $this->common_model->getAvgByid($busslist[$i]['business_id']); 
                    //print_r($rat);die;
	                if (!empty($rat)) {
	                	$results[$i]['ratshow'] = "1";
	                    $results[$i]['rating'] = $rat; 
	                }else{
	                	$results[$i]['ratshow'] = "1";
	                    $results[$i]['rating'] = "0";
	                }
	            }else{
	            	$results[$i]['ratshow'] = "0";
	                $results[$i]['rating'] = "0";

	            }

            }
        }
        return $results;
    }


    public function countFavBusinessList($user_id){

        //$offset = $limit*$offset;
        $this->db->order_by('features_ads','DESC');
        $this->db->select('*');     
        $this->db->Where(array(''.FAVOURITE.'.user_id' => $user_id,  ''.FAVOURITE.'.favourite' => 1));  
        $this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
        $this->db->Where(array(''.BUSINESS.'.status' => 1));    
        $this->db->from(FAVOURITE);
        $this->db->JOIN(BUSINESS, ''.FAVOURITE.'.business_id = '.BUSINESS.'.business_id' , 'left');
        $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');
        //$this->db->limit($limit, $offset);
        
        $query = $this->db->get();
        $results = $query->num_rows();
        
        return $results;
    }



    public function getDetailes($bid,$uid){

      //echo $bid; echo $uid; die;

        $this->db->select('*');
        $this->db->from(BUSINESS);
        //$this->db->JOIN('show_module');

        $this->db->where($bid);
        //$this->db->Where(array(''.show_module.'.module_id' => 1));
        $query = $this->db->get();
        $result =$query->row_array();

        $results['user_id'] = $result['user_id'];
        $results['business_id'] = $result['business_id'];
        $results['cat_id'] = $result['cat_id'];
        $results['sub_cat_id'] = $result['sub_cat_id'];
        $results['business_title'] = $result['business_title'];
        $results['business_desc'] = $result['business_desc'];
        $results['business_price'] = $result['business_price'];
        $results['business_email'] = $result['business_email'];
        $results['business_country'] = $result['business_country'];
        $results['business_city'] = $result['business_city'];
        $results['business_mobile'] = $result['business_mobile'];
        $results['business_address'] = $result['business_address'];
        $results['validate'] = $result['validate'];
        $results['status'] = $result['status'];
        $results['features_ads'] = $result['features_ads'];
        $results['lat'] = $result['lat'];
        $results['lon'] = $result['lon'];
        $results['distance'] = "";
        $results['review'] = (string)$this->countReview($results['business_id']);
        $chk = $this->common_model->getratshows();
        if ($chk == 1) {
        	$rat = $this->getAvgByid($result['business_id']);
        	$results['rating'] = $rat;
        	$results['ratshow'] = "1";
    	}else{
    		$results['rating'] = "0";
        	$results['ratshow'] = "0";
    	}
        if (!empty($uid)) {
        	$favourite = $this->getfavByid($result['business_id'],$uid);
        	if (!empty($favourite)) {
        		$results['favourite'] = $favourite;	
        	}else{
        		$results['favourite'] = "0";
        	}	
        }

        $services = $this->getservises($bid);
        if (!empty($services)) {
            $results['services'] = $services;
        }else{
        	$results['services'] = array();
        }       
        

        $offer = $this->getoffers($bid);
        if (!empty($offer)) {
            for ($i=0; $i < count($offer); $i++) { 

            $offers[$i]['offer_id'] = $offer[$i]['offer_id'];            
            $offers[$i]['offer_name'] = $offer[$i]['offer_name'];
            $offers[$i]['validate'] = $offer[$i]['validate'];            
            $offers[$i]['offer_image'] = BUSINESSPATH.$offer[$i]['offer_image'];
            }
        }else{
            $offers = array();
        }
        $results['offers'] =  $offers;
        $results['image'] = array(
                        array('business_image' => BUSINESSPATH . $result['business_image1']),
                        array('business_image' => BUSINESSPATH . $result['business_image2']),
                        array('business_image' => BUSINESSPATH . $result['business_image3']),
                        array('business_image' => BUSINESSPATH . $result['business_image4']),
                        array('business_image' => BUSINESSPATH . $result['business_image5']),
                        array('business_image' => BUSINESSPATH . $result['business_image6']),
                        array('business_image' => BUSINESSPATH . $result['business_image7']),
                        array('business_image' => BUSINESSPATH . $result['business_image8']),
                        array('business_image' => BUSINESSPATH . $result['business_image9']),
                        array('business_image' => BUSINESSPATH . $result['business_image10']),                        
                        );
        if (!empty($result['business_logo'])) {
            $results['business_logo'] = BUSINESSPATH . $result['business_logo'];
        }else{
            $Image = SR(CATEGORY, array('cat_status' => 1,'cat_parent' => 0));

            $results['business_logo'] = CATEGORYPATH. $Image['cat_img_path'];
        }
        $results['category_logo'] = CATEGORYPATH. $Image['cat_img_path'];
        
        
        return $results;
	}



	public function getfavByid($bid,$uid ){

        $this->db->select('favourite');
		$query = $this->db->get_where(FAVOURITE, array("business_id" => $bid, 'user_id' => $uid));       
		$results = $query->row_array();
		if (!empty($results)) {
			//print_r($results);//die;
			return $results['favourite'];
		}        
	}



	public function getAvgByid($bid){

		$this->db->select_avg('rating');
		$query = $this->db->get_where(RATE, array("business_id" => $bid));       
		$results = $query->row_array();

		if (!empty($results['rating'])) {
			return $results['rating'];

		}else{
			return $results="0";
		}       
	}



	public function getratshows(){
		
        $this->db->select('show_front');
		$query = $this->db->get_where('show_module', array("module_id" => 1));       
		$results = $query->row_array();
		if (!empty($results)) {
			//print_r($results);//die;
			return $results['show_front'];
		}        
	}
    public function countReview($business_id){
        
        $this->db->select('description');
        $query = $this->db->get_where('business_review', array("business_id" => $business_id,"status" => 1,));       
        $results = $query->num_rows();
        if (!empty($results)) {

            return $results;
        }else{
            return "0";
        }        
    }



	public function cheackBussId($bid){
		//echo $bid;die;
		
    	$this->db->order_by('features_ads','DESC');
		$this->db->select('*');				
		$this->db->Where(array(''.CATEGORY.'.cat_status' => 1));
		$this->db->Where(array(''.BUSINESS.'.status' => 1));
		$this->db->Where(array(''.BUSINESS.'.business_id' => $bid));
	    $this->db->from(BUSINESS);	    
	    $this->db->JOIN(CATEGORY, ''.CATEGORY.'.cat_id = '.BUSINESS.'.cat_id' , 'left');	    
		$query = $this->db->get();	   
        $results =$query->result_array();
        //print_r($results[0]['business_id']);die;
        return $results[0];
	}



	public function getoffers($bid){

		$this->db->select('*');
		$query = $this->db->get_where(OFFERS, $bid);       
		$results = $query->result_array();
		return $results;       
	}



	public function getservises($bid){

		$this->db->select('*');
		$query = $this->db->get_where(SERVICES, $bid);       
		$results = $query->result_array();
		return $results;       
	}



    public function getOption($opt_type_id){

      //echo $opt_type_id;die;

        $this->db->select('*');
        $this->db->from('otd_package_options');
        $this->db->join( 'otd_option_master', 'otd_package_options.pkg_inc_optid = otd_option_master.opt_id', 'left' );
        $this->db->where(array("pkg_package_id"=>$opt_type_id));
        $query = $this->db->get();
        $results =$query->result_array();
        //echo $a = count($results);
        for ($i=0; $i < count($results); $i++) { 
            
            $result[$i]['pkg_id'] = $results[$i]['pkg_id'];
            $result[$i]['pkg_opt_qty'] = $results[$i]['pkg_opt_qty'];
            $result[$i]['pkg_price_optid'] = $results[$i]['pkg_price_optid'];            
            $result[$i]['opt_name'] = $results[$i]['opt_name'];
            $result[$i]['pkg_status'] = $results[$i]['pkg_status'];
        }
        return $result;
        //die;
	}


	public function getPackegslist()
	{


		$this->db->select('*');
        $this->db->from('otd_option_master');
        $this->db->join( 'otd_option_price', 'otd_option_master.opt_id = otd_option_price.opt_type_id', 'left' );
        $this->db->where(array("opt_type"=>4, "opt_status"=>1));
        $query = $this->db->get();
        $results =$query->result_array();

        // print_r("<pre/>");
        // print_r($results);
        // die;
        $result = array();

        foreach($results as $result) {
          //$package_option = $this->getOption($result['opt_type_id']);

          $result1[]  = array(
                            'opt_id'=>$result['opt_id'],
                            'opt_name'=>$result['opt_name'],
                            'opt_price'=>$result['opt_price'],
                            'opt_text'=>$result['opt_text'],
                            'package_option'=>$this->getOption($result['opt_type_id'])
                          );

        }

        return $result1;

    } 


	public function allBusinessList($user_id,$cat_id)
	{

		$this->db->select('*');
	    $this->db->from(BUSINESS);
	    if (!empty($cat_id)) {
	    	$this->db->Where(array('cat_id' => $cat_id));
	    }
	    $this->db->Where(array('status' => 1));
	    $this->db->order_by('features_ads','DESC');
		$query = $this->db->get();


	    return $query->result_array();
    }
		



	function getDataInPagination($table, $limit, $offset, $condition){

		$this->db->order_by('noti_id','desc');
		$this->db->where($condition);
        $query = $this->db->get($table,$limit,$offset,$condition);
        return $query->result_array();
	}


	


	function getcatuseridbybusinesslist($table,$where_condition)
    {

	   	$this->db->select('*');
	    $this->db->from($table);
	    $this->db->where($where_condition);
		$this->db->order_by('features_ads','desc');
		$query = $this->db->get();
		return $query->result_array(); 
    }



    function totalOffset($count,$limit)
    {        
        $div = $count/$limit;
        $number = intval($div,0);
        if(fmod($div, 1) !== 0.00){
           $results['numberOffset'] = $number;
        }elseif ($div == 0) {
            $results['numberOffset'] = 0;
        }else{
            $results['numberOffset'] = $number-1;
        }
        return $results;
    }




    function countmaxoffset($table,$condition,$limit)
    {       
        $results['count'] = CR($table,$condition);
        $d = $results['count']/$limit;

        $number = intval($d,0);
        if(fmod($d, 1) !== 0.00){
           $results['numberOffset'] = $number;
        }elseif ($d == 0) {
            $results['numberOffset'] = 0;
        }else{
            $results['numberOffset'] = $number-1;
        }

        return $results;
    }   
/*________________________NEW FUNCTION END HEAR________________________________________*/




/*_________________COMMON MODEL FUNCTION HEAR PLEASE DON'T CHANGE THIS _____________________*/

    function getAllRecords($table)
	{

		$query = $this->db->get($table);

		return $query->result_array();

	}


	function randomTotalRecord($table, $colums)
	{
		$this->db->order_by($colums, 'RANDOM');

		$query = $this->db->get($table);

		return $query->result_array();
	}
    function randomTotalOffers($table, $colums, $limit, $offset)
    {
        $offset = $limit*$offset;

        $this->db->order_by($colums, 'RANDOM');
        
        $this->db->limit($limit, $offset);   

        $query = $this->db->get_where($table, array('status' =>1));

        return $query->result_array();
    }
    function countTotalOffers($table)
    {
        //$this->db->order_by($colums, 'RANDOM');

        $query = $this->db->get_where($table, array('status' =>1));

        return $query->num_rows();
    }


	

    function getSingleRecordById($table,$conditions)
	{
	   $query = $this->db->get_where($table,$conditions);

	   return $query->row_array();
	}

	

	function getAllRecordsById($table,$conditions)
	{
	   $query = $this->db->get_where($table,$conditions);

		return $query->result_array();
	}



	function addRecords($table,$post_data)
	{

		$this->db->insert($table,$post_data); 

		return $this->db->insert_id();
	}



	function addRecordsReturnId($table,$post_data)
	{
		$this->db->insert($table,$post_data);

		return $this->db->insert_id();
	}

	

	function updateRecords($table, $post_data, $where_condition)
	{
		$this->db->where($where_condition);

		$query = $this->db->update($table, $post_data); 

		return $query;
	}

	

	function deleteRecords($table,$where_condition)
	{
	    $this->db->where($where_condition);

		$this->db->delete($table);
	}	

	

	function getPaginateRecords($table, $result, $offset = 0)
	{
		$query = $this->db->get($table,$result,$offset);

	    return $query->result_array();
	}



	function getPaginateRecordsByConditions($table, $result, $offset=0, $condition)
	{
		$query = $this->db->get_where($table, $condition, $result, $offset);

	    return $query->result_array();
	}



	function getPaginateRecordsByLikeConditions($table, $result, $offset=0, $condition, $like_field, $like_value)
	{
		$this->db->like($like_field, $like_value);

		$query = $this->db->get_where($table, $condition, $result, $offset);

	    return $query->result_array();
	}



	function getTotalRecords($table)
	{
		$query = $this->db->get($table);

		return $query->num_rows();
	}

	

	function getTotalRecordsByIdLike($table, $condition, $like_field, $like_value)
	{
	    $this->db->like($like_field, $like_value);

	    $query = $this->db->get_where($table, $condition);

		return $query->num_rows();
	}

	

	function getPaginateRecordsByCondition($table,$result,$offset=0,$where_condition,$condition)
	{
	    $this->db->where($where_condition,$condition);

		$query = $this->db->get($table,$result,$offset);

	    return $query->result_array();
	}



	function getPaginateRecordsByOrderByCondition($table, $field, $short, $result, $offset=0, $condition)
	{
	    $this->db->where($condition);

	    $this->db->order_by($field, $short);

		$query = $this->db->get($table,$result,$offset);

	    return $query->result_array();
	}



	function getTotalRecordsByCondition($table, $condition)
	{
	    $this->db->where($condition);

		$query = $this->db->get($table);

		return $query->num_rows();
	}

	public function getratshow(){

		$this->db->select('*');
        $this->db->from('show_module');
        $this->db->where("module_id =", 1);
        $query = $this->db->get();
        $results = $query->row_array();
        return $results;     
	}
	

	function fetchMaxRecord($table,$field)
	{
		$this->db->select_max($field,'max');

        $query = $this->db->get($table);

		return $query->row_array();
	}



	function fetchRecordsByOrder($table,$field,$sort)
	{
	    $this->db->order_by($field,$sort);

		$query = $this->db->get($table);

		return $query->result_array();
	}			

			

	function getAllRecordsByLimitId($table,$conditions,$limit)
	{
	    $this->db->limit($limit);

		$query = $this->db->get_where($table,$conditions);

		return $query->result_array();
	}

	

	function getLatestRecords($table,$date,$limit)
	{
	    $this->db->order_by($date,'desc');

	    $this->db->limit($limit);

		$query = $this->db->get($table);

		return $query->result_array();
	}

	

	function getRelatedRecords($table,$date,$conditions)
	{
	    $this->db->order_by($date,'desc');

	    $this->db->limit(4);

		$query = $this->db->get_where($table,$conditions);

		return $query->result_array();
	}

	

	function getAscLatestRecords($table,$date,$limit)
	{

	    $this->db->order_by($date,'asc');

	    $this->db->limit($limit);

		$query = $this->db->get($table);

		return $query->result_array();
	}
	

	function getLimitedRecords($table,$limit)
	{
	    $this->db->limit($limit);

		$query = $this->db->get($table);

		return $query->result_array();
	}


	function getRecordCount($table, $where_condition)
	{

	    $this->db->where($where_condition);

		$query = $this->db->get($table);

		return $query->num_rows();
	}
	

	function getallservicesdetail()
	{

         $this->db->select('*');

	     $this->db->from('services');

	     $query = $this->db->get();

         return $query->result_array();
	}
	

    function getallusersdetail()
    {

	   	$this->db->select('*');

	    $this->db->from('users');

		$query = $this->db->get();

		return $query->result_array();
    }


	function generateRandomString($length = 12) {

	    

	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';

		    for ($i = 0; $i < $length; $i++) {
		        $randomString .= $characters[rand(0, $charactersLength - 1)];
		    }

	    return $randomString;
	}

	function cheackDevice($user_id,$token)
	{

        $this->db->select('*');

        $this->db->from('users');

        $this->db->where(array('user_id' => $user_id,'token' => $token));

        $query = $this->db->get();

        return $query->row_array();
    }

    function count_Search($search, $lat, $lon, $name,$distance="10") {

        $strQuery1 = $strQuery = "SELECT
                     `bdc_business`.*,`otd_business_category`.`cat_id`,`otd_business_category`.`cat_name`,
                     ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) )
                      AS distance 
                      FROM bdc_business

                      RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` 
                      WHERE `cat_status` = 1 AND `status` = 1 AND `features_ads` = 1 AND (
                      business_title LIKE '%$name%' AND cat_name LIKE '%$search%')
                      HAVING distance < $distance ORDER BY distance";

        $query = $this->db->query($strQuery1);
        $result['features'] = $query->num_rows();        
      
        $strQuery2 = "SELECT
                     `bdc_business`.*,`otd_business_category`.`cat_id`,`otd_business_category`.`cat_name`,
                     ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) )
                      AS distance 
                      FROM bdc_business 
                      RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` 
                      WHERE `cat_status` = 1 AND `status` = 1 AND `features_ads` = 0 AND (
                      business_title LIKE '%$name%' AND cat_name LIKE '%$search%')
                      HAVING distance < $distance ORDER BY distance";
        $query2 = $this->db->query($strQuery2);
        $result['simple'] = $query2->num_rows();

        $strQuery3 = "SELECT
                     `bdc_business`.*,`otd_business_category`.`cat_id`,`otd_business_category`.`cat_name`,
                     ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) )
                      AS distance 
                      FROM bdc_business 
                      RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` 
                      WHERE `cat_status` = 1 AND `status` = 1 AND (
                      business_title LIKE '%$name%' AND cat_name LIKE '%$search%')
                      HAVING distance < $distance ORDER BY distance";
        $query3 = $this->db->query($strQuery3);
        $result['total'] = $query3->num_rows(); 
        return $result;
    }

    function new_GetSearch($lat, $lon, $f, $limit, $offset, $search="", $name="",$distance="10") {


        
        $strQuery = "SELECT
                     `bdc_business`.*,`otd_business_category`.`cat_id`,`otd_business_category`.`cat_name`,
                     ( 3959 * acos( cos( radians($lat) ) * cos( radians( lat ) ) * cos( radians(lon) - radians($lon) ) + sin( radians($lat) ) * sin(radians(lat)) ) )
                      AS distance 
                      FROM bdc_business

                      RIGHT JOIN `otd_business_category` ON `bdc_business`.`cat_id`=`otd_business_category`.`cat_id` 
                      WHERE `cat_status` = 1 AND `status` = 1 AND `features_ads` = $f AND (
                      business_title LIKE '%$name%' AND cat_name LIKE '%$search%')
                      HAVING distance < $distance ORDER BY distance
                      LIMIT $limit OFFSET $offset";
                     
        $query = $this->db->query($strQuery);
        $result = $query->result_array();       
        
        if ($result) {
            for ($i=0; $i < count($result); $i++) {

                $results[$i]['user_id'] = $result[$i]['user_id'];
                $results[$i]['business_id'] = $result[$i]['business_id'];
                $results[$i]['cat_id'] = $result[$i]['cat_id'];
                $results[$i]['cat_name'] = $result[$i]['cat_name'];
                $results[$i]['business_title'] = $result[$i]['business_title'];
                $results[$i]['business_desc'] = $result[$i]['business_desc'];
                $results[$i]['business_country'] = $result[$i]['business_country'];
                if (!empty($result[$i]['business_city'])) {
                    $results[$i]['business_city'] = $result[$i]['business_city'];
                }else{
                    $results[$i]['business_city'] = "";
                }                
                $results[$i]['lat'] = $result[$i]['lat'];
                $results[$i]['lon'] = $result[$i]['lon'];
                $results[$i]['distance'] = number_format($result[$i]['distance'],5)/*." ".Km*/;
                $results[$i]['business_price'] = $result[$i]['business_price'];
                $results[$i]['business_email'] = $result[$i]['business_email'];
                $results[$i]['business_mobile'] = $result[$i]['business_mobile'];
                $results[$i]['business_address'] = $result[$i]['business_address'];
                $results[$i]['validate'] = $result[$i]['validate'];
                $results[$i]['status'] = $result[$i]['status'];
                $results[$i]['features_ads'] = $result[$i]['features_ads'];
                $results[$i]['business_image1'] = BUSINESSPATH . $result[$i]['business_image1']; 
                $chk = $this->common_model->getratshows();
                if ($chk == 1) {

                    $rat = $this->common_model->getAvgByid($results[$i]['business_id']);
                    if (!empty($rat)) {
                        $results[$i]['ratshow'] = "1"; 
                        $results[$i]['rating'] = $rat; 
                    }else{
                        $results[$i]['ratshow'] = "1"; 
                        $results[$i]['rating'] = "0"; 
                    }
                }else{
                    $results[$i]['ratshow'] = "0";
                    $results[$i]['rating'] = "0"; 
                }
                if (!empty($user_id)) {

                    $favourite = $this->common_model->getfavByid($result[$i]->business_id,  $user_id);

                    if (!empty($favourite)) {
                        $results[$i]['favourite'] = $favourite;     
                    }else{
                        $results[$i]['favourite'] = "0";
                    } 
                }else{
                     $results[$i]['favourite'] = "0";
                } 
                }
        }        
        if(!empty($results))
        {
            
            return $results;
        }        
    }







    
/*______________________________COMMON FUNCTION END HEAR____________________________________*/

}















