<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cronjobs extends CI_Model{
	function __construct(){
		parent::__construct();
		$this->OptionTbl = "otd_user_option";
		$this->emailTpTbl = "otd_email_templates";
		$this->userTbl = 'users';
		$this->usoptMaster = "otd_option_master";
		$this->pytTbl = "otd_user_option_payment";
	}

	/*
	* Get rows from user option table
	*/
	function getUserOptionRows($params = array()){
		$this->db->select('*');
        $this->db->from($this->OptionTbl);
        $this->db->join($this->pytTbl, "$this->pytTbl.pyt_id = $this->OptionTbl.opt_tran_id", "left");
        $this->db->join($this->userTbl, "$this->userTbl.user_id = $this->OptionTbl.opt_user_id");
        $this->db->join($this->usoptMaster, "$this->usoptMaster.opt_id = $this->OptionTbl.opt_option_id");
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }

        if(array_key_exists("wherein",$params)){
            foreach ($params['wherein'] as $key => $value) {
                $this->db->where_in($key,$value);
            }
        }        
        // $order_mode = "ASC";
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
                // $this->session->set_userdata('order_by',$value);
            }
        }
        if(array_key_exists("opt_user_option_id",$params)){
            $this->db->where('opt_user_option_id',$params['opt_user_option_id']);
            $query = $this->db->get();            
            $result = $query->row_array();
        }else{
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();
            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }        
        return $result;
	}

	/*
    * get rows for business opening time table
    */
    function getEmailTemplateRows($params = array()){
        $this->db->select('*');
        $this->db->from($this->emailTpTbl);        
        //fetch data by conditions
        if(array_key_exists("conditions",$params)){
            foreach ($params['conditions'] as $key => $value) {
                $this->db->where($key,$value);
            }
        }
        if(array_key_exists("sorting",$params)){
            foreach($params['sorting'] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        if(array_key_exists("tmplate_id",$params)){
            $this->db->where('tmplate_id',$params['tmplate_id']);
            $query = $this->db->get();
            $result = $query->row_array();
        }else{            
            //set start and limit
            if(array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit'],$params['start']);                
            }elseif(!array_key_exists("start",$params) && array_key_exists("limit",$params)){
                $this->db->limit($params['limit']);
            }
            $query = $this->db->get();


            if(array_key_exists("returnType",$params) && $params['returnType'] == 'count'){
                $result = $query->num_rows();                
            }elseif(array_key_exists("returnType",$params) && $params['returnType'] == 'single'){
                $result = ($query->num_rows() > 0)?$query->row_array():FALSE;
            }else{
                $result = ($query->num_rows() > 0)?$query->result_array():FALSE;
            }
        }
        return $result;
    }

    /*
    * Send Mail
    */
    function send_mail($email, $subject="Test Mail", $msg="Testing mail", $cc=NULL, $bcc=NULL){
        $from = SENDER_EMAIL;
        $this->load->library('email');
        $config['protocol']    = 'ssl';
        $config['smtp_host']    = 'ssl://mail.gandi.net';
        $config['smtp_port']    = 587;
        $config['smtp_timeout'] = '7';
        $config['smtp_user']    = $from;
        $config['smtp_pass']    = 'puR1-7,XifsArlyV!5';
        $config['charset']    = 'utf-8';
        $config['newline']    = "\r\n";
        $config['mailtype'] = 'html'; 
        $config['validation'] = TRUE;      
        $this->email->initialize($config);
        $fromname = SENDER_NAME;
        $fromemail =SENDER_EMAIL;
        $to = $email;        
        $subject=$subject;
        $message=$msg;
        $cc=false;
        $this->email->from($fromemail, $fromname);
        $this->email->to($to);
        $this->email->subject($subject);       
        $this->email->message($msg);  
        if($cc){
            $this->email->cc($cc);
        }

        if(!$this->email->send()){
            return false;//$this->email->print_debugger();
        }else{
            return true;
        }

    }


    /*
    * update option table
    */
    public function updateUserOption($params = array(), $where = array()){
    	$this->db->update($this->OptionTbl, $params, $where);
    	if($this->db->affected_rows() > 0){
    		return true;
    	}else{
    		return false;
    	}
    }
}

?>