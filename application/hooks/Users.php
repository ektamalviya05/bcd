<?php
class Users extends CI_Hooks {
	function setConstants(){
		$ci=& get_instance();
		$ci->load->model("Users/User");
		$constants = $ci->User->getTextContent(22);
        define('MASTER_EMAIL', "votive.pradeep01@gmail.com");
        define('ADMIN_EMAIL', $constants['label1']);
        define('SENDER_EMAIL', $constants['label2']);
        define('SENDER_NAME', $constants['label3']);
	}
}
?>