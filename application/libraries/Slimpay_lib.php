<?php 
if( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH.'third_party/Slimpay/vendor/autoload.php';
use \HapiClient\Http;
use \HapiClient\Hal;

class Slimpay_lib{
	var $CI;
	var $hapiClient;
	var $appid;
	var $appsecret;
	var $appcreditor;
	var $relation_namespace;
	var $token_url;

    function __construct(){
    	$this->CI =& get_instance();
		$this->CI->load->helper('url');
		$this->CI->load->helper('form');
		$this->CI->load->config('slimpay_config');
		$this->appid = $this->CI->config->item('sm_appid');
    	$this->appsecret = $this->CI->config->item('sm_appsecret');
    	$this->appcreditor = $this->CI->config->item('sm_creditor');    	
		//$this->relation_namespace = $this->CI->config->item('sm_relation_namespace');

		$this->relation_namespace = 'https://api.slimpay.net/alps#';
		$this->token_url = 'https://api.preprod.slimpay.com/oauth/token';
		$this->hapiClient = new Http\HapiClient('https://api.preprod.slimpay.com','/','https://api.slimpay.net/alps/v1', new Http\Auth\Oauth2BasicAuthentication('/oauth/token', $this->appid, $this->appsecret ));
    }


    /*
    * Create User Mandate
    */
    public function createUserMandate($params = array()){


		$relNs = 'https://api.slimpay.net/alps#';
		$rel = new Hal\CustomRel($relNs . 'create-orders');
		echo "</br>". $rel;
	
		    // The HAPI Client
		$hapiClient = new Http\HapiClient('https://api.preprod.slimpay.com','/','https://api.slimpay.net/alps/v1', new Http\Auth\Oauth2BasicAuthentication('/oauth/token','moovsit01', 'FtdBvWrfzxPkkKw7O53Mudupe7RFyxR0einjLb6a'));

		$follow = new Http\Follow($rel, 'POST', null, new Http\JsonBody([
																	    'started' => true,
																	    'creditor' => [
																	        'reference' => $this->appcreditor
																	    ],
																	    'subscriber' => [
																	        'reference' => $params['reference']
																	    ],
																	    'items' => [
																	        [
																	        	'type' => 'signMandate',
																	            'autoGenReference' => true,
																	            'mandate' => [
																	                'signatory' => [
																	                    'billingAddress' => [
																	                        'street1' => $params['address1'],
																	                        'street2' => $params['address2'],
																	                        'city' => $params['city'],
																	                        'postalCode' => $params['postalCode'],
																	                        'country' => $params['country']
																	                    ],
																	                    'honorificPrefix' => $params['honorificPrefix'],
																	                    'familyName' => $params['familyName'],
																	                    'givenName' => $params['givenName'],
																	                    'email' => $params['email'],
																	                    'telephone' => $params['telephone']
																	                ],
																	                'standard' => 'SEPA',
																	                'paymentScheme' => 'SEPA.DIRECT_DEBIT.CORE'
																	            ]
																	        ]
																	    ]
																	]
												
																));

													
		$res = $this->hapiClient->sendFollow($follow);

		if(!empty($res) && $res != FALSE){			
			$this->CI->session->set_userdata("slimpay_orderstatus", $res->getState()['reference']);
			$check = $this->updateOrderDetails($params['orderid'], $res->getState());
			if($check){
				$url = $res->getLink($this->relation_namespace.'user-approval')->getHref();		
				header('Location:' . $url);
			}
			exit;
		}else{
			redirect(base_url("Users/account/messoption"));
		}
    }

    public function signSlimpayMandate($params = array()){

		$relNs = 'https://api.slimpay.net/alps#';
		$rel = new Hal\CustomRel($relNs . 'create-orders');
		    // The HAPI Client
		$hapiClient = new Http\HapiClient('https://api.preprod.slimpay.com', '/', 'https://api.slimpay.net/alps/v1', new Http\Auth\Oauth2BasicAuthentication('/oauth/token', 'moovsit01', 'FtdBvWrfzxPkkKw7O53Mudupe7RFyxR0einjLb6a'));
		$follow = new Http\Follow($rel, 'POST', null, new Http\JsonBody(
			[
				'paymentScheme' => 'SEPA.DIRECT_DEBIT.CORE',
				'started' => true,
				'creditor' => [
					'reference' => 'moovsit'
				],
				'subscriber' => [
					'reference' => $params['reference']
				],
				'items' => [
					[
						'type' => 'signMandate',
						'autoGenReference' => true,
						'mandate' => [
							'signatory' => [
								'billingAddress' => [
									'street1' => $params['address1'],
									'street2' => $params['address2'],
									'city' => $params['city'],
									'postalCode' => $params['postalCode'],
								    'country' => 'FR'
								],
							
								'honorificPrefix' => $params['honorificPrefix'],
								'familyName' => $params['familyName'],
								'givenName' => $params['givenName'],
								'email' => $params['email'],
								'telephone' => $params['telephone']
							],
							'standard' => 'SEPA',
							'paymentScheme' => 'SEPA.DIRECT_DEBIT.CORE'
						]
					],
					[
						'type' => 'payment',
						'action' => 'create',
						'payin' => [
							'reference' => null,
							'scheme' => 'SEPA.DIRECT_DEBIT.CORE',
							'amount' =>  $params['amount'],
							'currency' => 'EUR',
							'label' => $params['label'],
							'capture' => 'false',
							'confirmed' => 'false',
							'executionDate' => null
						]
					],
				
				]
			]
			));
//echo "<pre>";
			//print_r($follow);
//echo "</pre>";		
			$res = $hapiClient->sendFollow($follow);
		//print_r($res);
		if(!empty($res) && $res != FALSE){
			//echo "I am here m";
			$this->CI->session->set_userdata("slimpay_orderstatus", $res->getState()['reference']);
			$this->CI->session->set_userdata("slimpaymandate_Sign", "Yes");
			$check = $this->updateOrderDetails($params['orderid'], $res->getState());
			if($check){
				//echo "I am here m 2";
				$url = $res->getLink($relNs.'user-approval')->getHref();		
				echo "URL: " . $url;
				//die(OK);
				header('Location:' . $url);
			}
			exit;
		}else{
		//echo "I am here in else";
		//	die();
			redirect(base_url("Users/account/messoption"));
		}
    }

    public function getOrder($params =array()){
    	$rel = new Hal\CustomRel($this->relation_namespace.'get-orders');
		$follow = new Http\Follow($rel, 'GET', [
		    'creditorReference' => $this->appcreditor,
		    'reference' => $params['reference']
		]);
		$order = $this->hapiClient->sendFollow($follow);
		return $order;
    }

    public function getIPNOrder($obj){
    	$rel = new Hal\CustomRel($this->relation_namespace.'get-orders');
    	$orderarray = \HapiClient\Hal\Resource::fromJson($obj);
		$follow = new Http\Follow($rel, 'GET', [
		    'creditorReference' => $this->appcreditor,
		    'reference' => $orderarray['reference']
		]);
		$order = $this->hapiClient->sendFollow($follow);
		return $order;
    }

    public function getMandateReference($order){
    	// The mandate reference (needed to create payments)
	    $rel = new Hal\CustomRel($this->relation_namespace.'get-mandate');
	    $follow = new Http\Follow($rel);
	    $mandate = $this->hapiClient->sendFollow($follow, $order);
	    return $mandateReference = $mandate->getState()['reference'];

	    // The mandate PDF file download
		 //    $rel = new Hal\CustomRel($this->relation_namespace.'get-binary-content');
		 //    $follow = new Http\Follow($rel);
		 //    $binaryContent = $this->hapiClient->sendFollow($follow, $mandate);

		 //    $pdfContent = base64_decode($binaryContent->getState()['content']);
		 //    $file = "mandate-$mandateReference.pdf";
			// file_put_contents($file, $pdfContent);			
		// The mandate PDF file download End

	    // The subscriber reference that you provided
	    
	    // return $file;
	    // ... trigger some action(s) depending on your need
    }    

    /*
    * get existing mandate state
    */
    public function getMandateState($params = array()){
	
		//(DO NOT DELETE)
		// The HAPI Client
		$hapiClient = new Http\HapiClient('https://api.preprod.slimpay.com', '/', 'https://api.slimpay.net/alps/v1', new Http\Auth\Oauth2BasicAuthentication('/oauth/token', 'moovsit01', 'FtdBvWrfzxPkkKw7O53Mudupe7RFyxR0einjLb6a' ));
		// The Relations Namespace
		$relNs = 'https://api.slimpay.net/alps#';
		// Follow get-mandates
		$rel = new Hal\CustomRel($relNs . 'get-mandates');
		$follow = new Http\Follow($rel, 'GET', ['creditorReference' => 'moovsit','reference' => 'SLMP004114729', 'rum' => null, 'id' => null, 'paymentScheme' => null]);
		$res = $hapiClient->sendFollow($follow);
		// The Resource's state
		$state = $res->getState();
		
		if(!empty($res)){
			return $state = $res->getState(); // return an array
		}else{
			return false;
		}
    }

    /*
    * get mandate document
    */
    function getMandateDocument($params = array()){
    	$rel = new Hal\CustomRel($this->relation_namespace.'get-mandates');
		$follow = new Http\Follow($rel, 'GET', [
		    'creditorReference' => $this->appcreditor,
		    'reference' => $params['reference']	
		]);
		$mandate = $this->hapiClient->sendFollow($follow);
	      	// The mandate PDF file download
		    $rel = new Hal\CustomRel($this->relation_namespace.'get-binary-content');
		    $follow = new Http\Follow($rel);
		    $binaryContent = $this->hapiClient->sendFollow($follow, $mandate);
		    $pdfContent = base64_decode($binaryContent->getState()['content']);	    
		    return $pdfContent;
		// The mandate PDF file download End
    }

    /*
    * get Bank acount number using details
    */
    function getBankAccount($params = array()){
    	// Follow get-mandates
		$rel = new Hal\CustomRel($this->relation_namespace.'get-mandates');
		$follow = new Http\Follow($rel, 'GET', [
		    'creditorReference' => $this->appcreditor,
		    'reference' => $params['reference']	
		]);
		$res = $this->hapiClient->sendFollow($follow);

		// Follow get-bank-account
		$rel = new Hal\CustomRel($this->relation_namespace.'get-bank-account');
		$follow = new Http\Follow($rel, 'GET');
		$res = $this->hapiClient->sendFollow($follow, $res);

		// The Resource's state
		$state = $res->getState();
		return $state;
    }

    /*
    * Custom method to update order details
    */
    public function updateOrderDetails($order, $details){
    	$aray = array("py_slmref_id"=>$details['reference'],
    				  "py_slm_state"=>$details['state'],
    				  "py_slm_started"=>$details['started'],
    				  "py_slm_datecreated"=>$details['dateCreated'],
    				  "py_slm_datemodified"=>$details['dateModified'],
    				  "py_slm_datestarted"=>$details['dateStarted'],
    				  "py_slm_payscheme"=>$details['paymentScheme'],
    				  "py_slm_senduserapproval"=>$details['sendUserApproval'],
    				  "py_slm_checkoutactour"=>$details['checkoutActor'],
    				  "pyt_date"=>date("Y-m-d H:i:s")
    				 );
    	$this->CI->db->update("otd_user_option_payment", $aray, array("pyt_id"=>$order));
    	if($this->CI->db->affected_rows() > 0){
    		return true;
    	}else{
    		return false;
    	}
    }


    /*
    * direct payment when user has active mandate reference
    */
    public function makeDirectPayment($params =array()){
	
		$rel = new Hal\CustomRel('https://api.slimpay.net/alps#create-payins');
		$follow = new Http\Follow($rel, 'POST', null, new Http\JsonBody([
			'creditor' => ['reference' => $this->appcreditor],																		    
				'mandate' => ['reference' => $params['mdreference']],
							'reference' => null,
							'amount' => $params['amount'],
							'currency' => 'EUR',
							'scheme' => 'SEPA.DIRECT_DEBIT.CORE'																		    
							]
						));

	$payment = $this->hapiClient->sendFollow($follow);
	
		if(!empty($payment)){			
			$arr = array("py_slmMandate_ref"=>$params['mdreference'],
						 "py_slm_state"=>$payment->getState()['executionStatus'],
						 "py_slm_datecreated"=>$payment->getState()['dateCreated'],
						 "py_slm_datemodified"=>$payment->getState()['executionDate'],
						 "py_slmref_id"=>$payment->getState()['reference'],
						 "py_slm_payscheme"=>$payment->getState()['scheme'],
						 "pyt_date"=>date("Y-m-d H:i:s")
						);
			if($payment->getState()['executionStatus'] == "toprocess")
				$arr['pyt_txn_status'] = "Completed";
			$this->CI->db->update("otd_user_option_payment", $arr, array("pyt_id"=>$params['orderid']));
			return $payment;
		}else{
			return false;
		}
    }

    /*
    * only sign mandate
    */
    public function signmandateonly($params = array()){
    	$rel = new Hal\CustomRel($this->relation_namespace.'create-orders');
		$follow = new Http\Follow($rel, 'POST', null, new Http\JsonBody([
																		    'started' => true,
																		    'creditor' => [
																		        'reference' => $this->appcreditor
																		    ],
																		    'subscriber' => [
																		        'reference' => $params['reference']
																		    ],
																		    'items' => [
																					        [
																				            'type' => 'signMandate',
																				            'autoGenReference' => true,
																				            'mandate' => [
																				                'signatory' => [
																				                    'billingAddress' => [
																				                        'street1' => '27 rue des fleurs',
																				                        'street2' => 'Bat 2',
																				                        'city' => 'Paris',
																				                        'postalCode' => '75008',
																				                        'country' => 'FR'
																				                    ],
																				                    'honorificPrefix' => 'Mr',
																				                    'familyName' => 'Toshik',
																				                    'givenName' => 'Parihar',
																				                    'email' => 'votive.toshi@gmail.com',
																				                    'telephone' => '+33612345678'
																				                ],
																				                'standard' => 'SEPA'
																				            ]																				            
																				        ]
																				    ]
																	]
																	));
		$res = $this->hapiClient->sendFollow($follow);
		if(!empty($res) && $res != FALSE){
			$this->CI->session->set_userdata("slimpay_orderstatus", $res->getState()['reference']);
			$check = $this->updateOrderDetails($params['orderid'], $res->getState());
			if($check){
				$url = $res->getLink($this->relation_namespace.'user-approval')->getHref();		
				header('Location:' . $url);
			}
			exit;
		}else{
			redirect(base_url("Users/account/messoption"));
		}
    }

    /*
    * update bank acount number
    */
    public function updateBankAccount($params = array()){
    	$rel = new Hal\CustomRel($this->relation_namespace.'create-orders');
		$follow = new Http\Follow($rel, 'POST', null, new Http\JsonBody(
		[
		    'started' => true,
		    'locale' => null,
		    'creditor' => [
		        'reference' => $this->appcreditor
		    ],
		    'subscriber' => [
		        'reference' => $params['subscriber']
		    ],
		    'items' => [
		        [
		            'type' => 'signMandate',
		            'action' => 'amendBankAccount',
		            'mandate' => [
		                'reference' => $params['mandate']
		            ]
		        ]
		    ]
		]
		));
		$res = $this->hapiClient->sendFollow($follow);
		if(!empty($res) && $res != FALSE){
			$url = $res->getLink($this->relation_namespace.'user-approval')->getHref();
			return $url;
		}else{
			return false;
		}
		
    }
}

?>