<?php
$footer = $this->db->get_where("otd_footer_content", array("ft_id"=>1))->row_array();
?>
<footer class="main_footer">
  <div class="footer_new_letter">
    <div class="news-letter">
      <div class="container">
        <div class="new_letter_main">
          <div class="row">
            <div class="col-sm-3">
              <div class="news_letter_heading">
                <h2><?php echo $footer['label1']; ?></h2>
              </div>
            </div>
            <div class="col-sm-9">
              <div class="new_letter_input">
                <form id="new_letter_input" method="post" action="<?php echo base_url("Users/subscription"); ?>">
                  <div class="input-group">
                    <input required="" type="text" class="form-control" name="user_email" placeholder="Enter Email">
                    <input type="text" class="form-control" placeholder="Enter Postal Code" name="user_postcode">
                    <span class="input-group-btn">
                      <button name="subscribeSubmit" class="btn btn-default" type="submit" value="Subscribe Submit"><?php echo $footer['label2']; ?></button>                      
                    </span>
                    <div id="emlermsg1"></div>
                  </div>                  
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="f_middle">
        <div class="col-md-4 col-sm-4">
          <div class="footer-logo"> <img src="<?php echo base_url();?>assets/img/logo.png"> </div>
          <div class="f_heading">
            <p><?php echo $footer['label3']; ?></p>
            <?php
            // $footerart = $this->db->get_where("site_contents", array("content_id"=>11));
            // foreach($footerart->result() as $ft){
            //   echo $ft->content;
            // }
            ?>
          </div>
        </div>
        <div class="col-md-8 col-sm-8">
          <div class="working_hours">
            <div class="col-sm-4">
              <div class="f_heading">
                <h3><?php echo $footer['label4']; ?></h3>
              </div>
              <ul>                
                <?php
                            $Information = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>1, "pg_status"=>1));
                           
                            foreach($Information->result() as $info){
                            
                              ?>
                <li>
                <?php
                if($info->page_url != "")
                {
                 
                  ?>
               <!-- <a href="<?php echo base_url("Pages/content/$info->page_url"); ?>"> -->
               <a href="<?php echo base_url("articles/$info->page_url"); ?>">
                <?php
                }
                else {
                  ?>
                  <!-- <a href="<?php echo base_url("Pages/content/$info->pg_meta_tag"); ?>"> -->
                  <a href="<?php echo base_url("articles/$info->pg_meta_tag"); ?>">
               <?php
                }
                ?>

                <?php echo $info->pg_title; ?></a></li>

                <?php
                            }
                          ?>
                <li><a href="<?php echo base_url("assets/doc/CGU_MOOVSIT_OTOURDEMOI_V4.2.pdf"); ?>" target="_blank">Conditions générales d'utilisations</a></li>
              </ul>
            </div>
            <div class="col-sm-4">
              <div class="f_heading">
                <h3><?php echo $footer['label5']; ?></h3>
              </div>
              <ul>
                <?php
                              $discover = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>2, "pg_status"=>1));
                              foreach($discover->result() as $disc){
                                ?>
                <li>
                <?php
                if($disc->page_url != "")
                {
                
                  ?>
               <!-- <a href="<?php echo base_url("Pages/content/$disc->page_url"); ?>"> -->
               <a href="<?php echo base_url("articles/$disc->page_url"); ?>">
                <?php
                }
                else {
                  ?>
                  <!-- <a href="<?php echo base_url("Pages/content/$disc->pg_meta_tag"); ?>"> -->
                  <a href="<?php echo base_url("articles/$disc->pg_meta_tag"); ?>">
               <?php
                }
                ?>
                
                <?php echo $disc->pg_title; ?></a></li>
                <?php
                              }
                            ?>
              </ul>
            </div>
            <div class="col-sm-4">
              <div class="f_heading">
                <h3><?php echo $footer['label6']; ?></h3>
              </div>
              <ul>
                <?php
                            $professional = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>3, "pg_status"=>1));
                            foreach($professional->result() as $prof){
                              ?>
                <li>
                <?php
                if($prof->page_url != "")
                {
                 
                  ?>
               <!-- <a href="<?php echo base_url("Pages/content/$prof->page_url"); ?>"> -->
               <a href="<?php echo base_url("articles/$prof->page_url"); ?>">
                <?php
                }
                else {
                  ?>
                  <!-- <a href="<?php echo base_url("Pages/content/$prof->pg_meta_tag"); ?>"> -->
                  <a href="<?php echo base_url("articles/$prof->pg_meta_tag"); ?>">
               <?php
                }
                ?>
                
                
                <?php echo $prof->pg_title; ?></a></li>
                <?php
                            }
                          ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="copyright">
          <div class="col-sm-6">
            <div class="footer-link-social">
              <ul>
                <li><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"></i></a></li>
                <li><a href="https://in.pinterest.com/" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                <li><a href="https://plus.google.com/" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook"></i></a></li>
                <li><a href="https://www.instagram.com/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="f_follow">Copyright © 2017</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<div class="side-footer-link">
  <?php
  $facebook = $this->db->get_where("otd_social_links", array("sc_id"=>1))->row_array();
  $twitter = $this->db->get_where("otd_social_links", array("sc_id"=>2))->row_array();
  $instagram = $this->db->get_where("otd_social_links", array("sc_id"=>3))->row_array();
?>
  <ul>
    <li><i class="fa fa-facebook"></i>
      <div class="icon-detail"> <a href="<?php echo $facebook['sc_links']; ?>">Facebook</a> </div>
    </li>
    <li><i class="fa fa-twitter"></i>
      <div class="icon-detail"> <a href="<?php echo $twitter['sc_links']; ?>">Twitter</a> </div>
    </li>
    <li><i class="fa fa-instagram"></i>
      <div class="icon-detail"> <a href="<?php echo $instagram['sc_links']; ?>">Instagram</a> </div>
    </li>
  </ul>
</div>

<div id="chat-rooms">
  
</div>

<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url("assets/css/multiselect/bootstrap-multiselect.js") ?>"></script> -->
<script type="text/javascript" src="<?php echo base_url("assets/css/multiselect/bootstrap-select.js") ?>"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/imagelightbox.js"></script>
<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/js/imagelightbox.css" />
<!-- <script src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script>  -->

<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script> 
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script> 
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script> 
<script src="<?php echo base_url("assets/js/jquery.flexdatalist.js");?>"></script> 
<script src="<?php echo base_url('assets/js/jquery.mCustomScrollbar.min.js'); ?>"></script> 
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script> 
<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script> 
<script src="<?php echo base_url();?>assets/js/lightbox.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script>
<script type="text/javascript" src="<?php echo base_url("assets/datetimepicker/js/bootstrap-datetimepicker.js"); ?>" charset="UTF-8"></script> 
<script type="text/javascript" src="<?php echo base_url("assets/datetimepicker/js/locales/bootstrap-datetimepicker.fr.js"); ?>" charset="UTF-8"></script>
<script src="<?php echo base_url();?>assets/js/jspdf.min.js"></script>
<script src="<?php echo base_url("assets/js/custom.js");?>"></script> 
<script type= 'text/javascript' src="<?php echo base_url(); ?>assets/js/blog_rating.js"></script>
<script type= 'text/javascript' src="<?php echo base_url(); ?>assets/js/latinize.js"></script>


<!-- <script>
           $(document).ready(function () {
               $("#usercategories").selectr({
                  //  title: 'What would you like to drink?',
                  //  placeholder: 'Search beverages'
               });
           });
       </script> -->



<script type="text/javascript">
    // $(document).ready(function() {

    //     $('#userservicefilters').multiselect({
    //     nonSelectedText: 'Services',
    //     // includeSelectAllOption: true,
    //     enableCaseInsensitiveFiltering: true,
    //     // filterBehavior: 'value',
    //     buttonWidth: 250,
    //     enableFiltering: true,
    //     maxHeight:200,

    //     onChange: function(option, checked) {
    //             // Get selected options.
    //             var selectedOptions = $('#userservicefilters option:selected');
 
    //             if (selectedOptions.length >= 3) {
    //                 // Disable all other checkboxes.
    //                 var nonSelectedOptions = $('#userservicefilters option').filter(function() {
    //                     return !$(this).is(':selected');
    //                 });
 
    //                 nonSelectedOptions.each(function() {
    //                     var input = $('input[value="' + $(this).val() + '"]');
    //                     input.prop('disabled', true);
    //                     input.parent('li').addClass('disabled');
    //                 });

    //                 selectedOptions.each(function() {
    //                     var input = $('input[value="' + $(this).val() + '"]');
    //                     input.prop('disabled', false);
    //                     input.parent('li').removeClass('disabled');
    //                 });

    //             }
    //             else {
    //                 // Enable all checkboxes.
    //                 $('#userservicefilters option').each(function() {
    //                     var input = $('input[value="' + $(this).val() + '"]');
    //                     input.prop('disabled', false);
    //                     input.parent('li').removeClass('disabled');
    //                 });
    //             }
    //         }

    //   });
      // $('#userproductfilters').multiselect({
      //   nonSelectedText: 'Produits',
      //   // includeSelectAllOption: true,
      //   enableCaseInsensitiveFiltering: true,
      //   // filterBehavior: 'value',
      //   buttonWidth: 250,
      //   enableFiltering: true,
      //   maxHeight:200,
      //   onChange: function(option, checked) {
      //           // Get selected options.
      //           var selectedOptions = $('#userproductfilters option:selected');
 
      //           if (selectedOptions.length >= 3) {
      //               // Disable all other checkboxes.
      //               var nonSelectedOptions = $('#userproductfilters option').filter(function() {
      //                   return !$(this).is(':selected');
      //               });
 
      //               nonSelectedOptions.each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', true);
      //                   input.parent('li').addClass('disabled');
      //               });

      //               selectedOptions.each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', false);
      //                   input.parent('li').removeClass('disabled');
      //               });

      //           }
      //           else {
      //               // Enable all checkboxes.
      //               $('#userproductfilters option').each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', false);
      //                   input.parent('li').removeClass('disabled');
      //               });
      //           }
      //       }
      // });
      // $('#userotherfilters').multiselect({
      //   nonSelectedText: 'Autres',
      //   // includeSelectAllOption: true,
      //   enableCaseInsensitiveFiltering: true,
      //   // filterBehavior: 'value',
      //   buttonWidth: 250,
      //   enableFiltering: true,
      //   maxHeight:200,
      //   onChange: function(option, checked) {
      //           // Get selected options.
      //           var selectedOptions = $('#userotherfilters option:selected');
 
      //           if (selectedOptions.length >= 3) {
      //               // Disable all other checkboxes.
      //               var nonSelectedOptions = $('#userotherfilters option').filter(function() {
      //                   return !$(this).is(':selected');
      //               });
 
      //               nonSelectedOptions.each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', true);
      //                   input.parent('li').addClass('disabled');
      //               });

      //               selectedOptions.each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', false);
      //                   input.parent('li').removeClass('disabled');
      //               });

      //           }
      //           else {
      //               // Enable all checkboxes.
      //               $('#userotherfilters option').each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', false);
      //                   input.parent('li').removeClass('disabled');
      //               });
      //           }
      //       }
      // });

      // $('#usercategories').multiselect({

      //   templates: {
               
      //           filter: '<li class="multiselect-item filter"><div class="input-group"><span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span><input class="form-control multiselect-search" type="text"></div></li>',
      //           ul: '<ul class="multiselect-container dropdown-menu"></ul>',
      //           filterClearBtn: '<span class="input-group-btn"><button class="btn btn-default multiselect-clear-filter" type="button"><i class="glyphicon glyphicon-remove-circle"></i></button></span>',
      //           li: '<li><a href="javascript:void(0);"><label></label></a></li>',
      //           divider: '<li class="multiselect-item divider"></li>',
      //           liGroup: '<li class="multiselect-item group"><label class="multiselect-group"></label></li>'
      //       },

      //   nonSelectedText: '',
      //   // includeSelectAllOption: true,
      //   enableCaseInsensitiveFiltering: true,
      //   // filterBehavior: 'value',
      //   buttonWidth: 250,
      //   enableFiltering: true,
      //   maxHeight:200,


      //   onChange: function(option, checked) {
      //           // Get selected options.
      //           var selectedOptions = $('#usercategories option:selected');
 
      //           if (selectedOptions.length >= 3) {
      //               // Disable all other checkboxes.
      //               var nonSelectedOptions = $('#usercategories option').filter(function() {
      //                   return !$(this).is(':selected');
      //               });
 
      //               nonSelectedOptions.each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', true);
      //                   input.parent('li').addClass('disabled');
      //               });

      //               selectedOptions.each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', false);
      //                   input.parent('li').removeClass('disabled');
      //               });

      //           }
      //           else {
      //               // Enable all checkboxes.
      //               $('#usercategories option').each(function() {
      //                   var input = $('input[value="' + $(this).val() + '"]');
      //                   input.prop('disabled', false);
      //                   input.parent('li').removeClass('disabled');
      //               });
      //           }
      //       }
      // });
     

      // setTimeout(
      //   function(){
      //     $(".multiselect").trigger("click");
      //   }, 1000
      // );
      
      

      // $("#newservicefilter").on("click", function(){
      //   $("<option value='add1' selected='selected'>Addition 1</option>").appendTo("#userservicefilters");
      //   $('#userservicefilters').multiselect('rebuild');
      // })

  

    // });
</script>
<script>
var limit = 3;
$('selectpicker option').on('change', function(evt) {
   if($(this).siblings(':checked').length >= limit) {
       this.checked = false;
   }
});
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example-rebuild').multiselect();
 
        $('#example-rebuild-button').on('click', function() {
            $('#example-rebuild').multiselect('rebuild');
        });
 
        $('#example-rebuild-add').on('click', function() {
            $('#example-rebuild').append('<option value="add1">Addition 1</option>
<option value="add2">Addition 2</option><option value="add3">Addition 3</option>');
        });
      });
        $('#example-rebuild-delete').on('click', function() {
            $('option[value="add1"]', $('#example-rebuild')).remove();
            $('option[value="add2"]', $('#example-rebuild')).remove();
            $('option[value="add3"]', $('#example-rebuild')).remove();
        });
    });
</script>


<script>

<?php
  $ses = $this->session->userdata("user_id");
  if(!empty($ses)){
    $msgcnt = $this->db->get_where("otd_footer_content", array("ft_id"=>18))->row_array();
    ?>
    var plhold = '<?php echo $msgcnt['label1']; ?>';
    var rpbutton = '<?php echo $msgcnt['label2']; ?>';
    setInterval(function(){
	var cookie = readCookie('popup_states');
	var all_state = JSON.parse(cookie);

	$.each(all_state, function(index, value) {
		if($('#chat_window_'+value.id).length > 0 ) 
		 {
             openAllState_state(value.id,value.state);
		 }else{
		      openAllState(value.id,value.state);
		 }
	});

	 
        $.ajax({
            url:"<?php echo base_url('Chat/getMessages'); ?>",
            dataType:"json",
            success:function(data){
         
              if(data.status == 200){
                $.each(data.details, function(key,value){
                  var msgdiv = "";
                  if($("#chat-rooms .chat-window").is("#chat_window_"+value.roomId)){
                    $.each(value.chat, function(key1, value1){
                      if( $("#chat-rooms #chat_window_"+value.roomId+" .msg_container_base").find("#msg"+value1.msgid).length == 0){
                        if(value1.msg_type == 1){
                          msgdiv = '<div id="msg'+value1.msgid+'" class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img src="'+value1.img+'" class=" img-responsive "></div><div class="col-md-10 col-xs-10"><div class="messages msg_receive"><p>'+value1.msg+'</p><time datetime="2009-11-13T20:00">'+value1.mdate+'</time></div></div></div>';
                        }else{
                          msgdiv = '<div id="msg'+value1.msgid+'" class="row msg_container base_sent"><div class="col-md-10 col-xs-10"><div class="messages msg_sent"><p>'+value1.msg+'</p><time datetime="2009-11-13T20:00">'+value1.mdate+'</time></div></div><div class="col-md-2 col-xs-2 avatar"><img src="'+value1.img+'" class=" img-responsive "></div></div>';
                        }
                        //$(msgdiv).appendTo("#chat-rooms #chat_window_"+value.roomId+" .panel-body");
                        $(msgdiv).appendTo("#chat-rooms #chat_window_"+value.roomId+" .panel-body .panel-container");

                        if($("#chat-rooms #chat_window_"+value.roomId).find('.minim_chat_window').hasClass('panel-collapsed')){
                          $("#chat-rooms #chat_window_"+value.roomId+" .panel-title span").addClass("pingbounce");
                        }

                        // $("#chat-rooms #chat_window_"+value.roomId+" .msg_container_base").animate({ scrollTop: $("#chat-rooms #chat_window_"+value.roomId+" .msg_container_base")[0].scrollHeight});
                        // $("#chat-rooms #chat_window_"+value.roomId+" .msg_container_base").mCustomScrollbar("update");
                        $("#chat-rooms #chat_window_"+value.roomId+" .msg_container_base").mCustomScrollbar("scrollTo", "bottom", { scrollInertia:100 });
                      }
                    });                    
                  }else{                    
                    msgdiv += '<div class="row chat-window hidden-xs col-sm-4 col-md-3 col-lg-3" id="chat_window_'+value.roomId+'" style="margin-left:10px;"><div class="subchatwindow"><div class="panel panel-default"><div class="panel-heading top-bar"><div class="col-md-8 col-xs-8"><h3 class="panel-title"><span class="glyphicon glyphicon-comment"></span> '+value.user+'</h3></div><div class="col-md-4 col-xs-4" style="text-align: right;"><a href="javascript:;"><span class="minim_chat_window glyphicon icon_minim glyphicon-minus"></span></a><a href="javascript:;"><span class="glyphicon glyphicon-remove icon_close" modal-aria="'+value.roomId+'" data-id="chat_window_'+value.roomId+'"></span></a></div></div><div class="panel-body msg_container_base"><span class="loading" style="display:none"><input type="hidden" class="scrto" value="'+value.timestmp.id+'" /><input type="hidden" class="tmpstmp" value="'+value.timestmp.dt+'"/><img src="http://localhost/VotiveYellowPages/assets/img/load.gif" /></span><div class="panel-container">';
                    $.each(value.chat, function(key1, value1){
                      if(value1.msg_type == 1){
                        msgdiv += '<div id="msg'+value1.msgid+'" class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img src="'+value1.img+'" class=" img-responsive "></div><div class="col-md-10 col-xs-10"><div class="messages msg_receive"><p>'+value1.msg+'</p><time datetime="2009-11-13T20:00">'+value1.mdate+'</time></div></div></div>';
                      }else{
                        msgdiv += '<div id="msg'+value1.msgid+'" class="row msg_container base_sent"><div class="col-md-10 col-xs-10"><div class="messages msg_sent"><p>'+value1.msg+'</p><time datetime="2009-11-13T20:00">'+value1.mdate+'</time></div></div><div class="col-md-2 col-xs-2 avatar"><img src="'+value1.img+'" class=" img-responsive "></div></div>';                        
                      }
                    });
                    msgdiv += '</div></div><div class="panel-footer"><form autocomplete="off" action="'+value.roomId+'"><div class="input-group"><input name="btn-input" type="text" class="form-control input-sm chat_input" placeholder="'+plhold+'" /><span class="input-group-btn"><button type="submit" class="btn btn-primary btn-sm sendusermsg">'+rpbutton+'</button></span></div></form></div></div></div></div>';
                    // var divlength = $("#chat-rooms .chat-window").length;
                    $(msgdiv).appendTo("#chat-rooms");
                    $("#chat-rooms .msg_container_base").mCustomScrollbar({
                      callbacks:{
                        onScroll:function(){
                          if(this.mcs.topPct == 0){
                            
                            var tmpstamp = $("#chat-rooms #chat_window_"+value.roomId+" .loading .tmpstmp").val();
                            if(tmpstamp != ""){
                              $("#chat-rooms #chat_window_"+value.roomId+" .loading").show();
                              $.ajax({
                                url: '<?php echo base_url("Chat/timestmpMessages"); ?>',
                                type:"POST",
                                data:{ "roomId":value.roomId, "tmpstmp":tmpstamp},
                                dataType:"json",
                                success:function(prev){                                
                                  msgdiv1 = "";
                                  $.each(prev.details.chat, function(key2, value2){
                                    if( $("#chat-rooms #chat_window_"+prev.details.roomId+" .msg_container_base").find("#msg"+value2.msgid).length == 0){
                                      if(value2.msg_type == 1){
                                        msgdiv1 += '<div id="msg'+value2.msgid+'" class="row msg_container base_receive"><div class="col-md-2 col-xs-2 avatar"><img src="'+value2.img+'" class=" img-responsive "></div><div class="col-md-10 col-xs-10"><div class="messages msg_receive"><p>'+value2.msg+'</p><time datetime="2009-11-13T20:00">'+value2.mdate+'</time></div></div></div>';
                                      }else{
                                        msgdiv1 += '<div id="msg'+value2.msgid+'" class="row msg_container base_sent"><div class="col-md-10 col-xs-10"><div class="messages msg_sent"><p>'+value2.msg+'</p><time datetime="2009-11-13T20:00">'+value2.mdate+'</time></div></div><div class="col-md-2 col-xs-2 avatar"><img src="'+value2.img+'" class=" img-responsive "></div></div>';
                                      }
                                    }
                                  });                                
                                  var scrolid = "#"+$("#chat-rooms #chat_window_"+prev.details.roomId+" .loading .scrto").val();
                                  $("#chat-rooms #chat_window_"+prev.details.roomId+" .loading .tmpstmp").val(prev.details.tmstmp.dt);
                                  $("#chat-rooms #chat_window_"+prev.details.roomId+" .loading .scrto").val(prev.details.tmstmp.id);
                                  $("#chat-rooms #chat_window_"+prev.details.roomId+" .loading").hide();
                                  $(msgdiv1).prependTo("#chat-rooms #chat_window_"+prev.details.roomId+" .panel-body .panel-container");                                
                                  $("#chat-rooms #chat_window_"+prev.details.roomId+" .panel-body .msg_container_base").mCustomScrollbar("scrollTo", scrolid, { scrollInertia:100 });
                                }
                              });
                            }
                          }
                        }
                      }
                    });
                    $("#chat-rooms .msg_container_base").mCustomScrollbar("scrollTo", "bottom", { scrollInertia:100 });
                    // $("#chat-rooms .msg_container_base").animate({ scrollTop: $("#chat-rooms .msg_container_base")[0].scrollHeight});                    
                  }
                });
              } 
            },
			complete: function(){
			        
			}
			
        });
    }, 3000);
    <?php
  }
?>
$("#chat-rooms").on('click', '.panel-heading span.icon_minim', function (e) {
    var $this = $(this);
	 var roomId = $this.parent('a').next("a").find('span').attr("modal-aria");
	 
    if (!$this.hasClass('panel-collapsed')) {
	     chat_state(roomId,'min');
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
	    chat_state(roomId,'open');
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$("#chat-rooms").on('focus', '.panel-footer input.chat_input', function (e) {
    var $this = $(this);
    if($this.parents('.chat-window').find('.minim_chat_window').hasClass('panel-collapsed')) {
        
        // $this.parents(".chat-window").find(".msg_container_base").animate({ scrollTop: $this.parents(".chat-window").find(".msg_container_base")[0].scrollHeight});        
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.parents('.chat-window').find('.minim_chat_window').removeClass('panel-collapsed');
        $this.parents('.chat-window').find('.minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        $this.parents('.chat-window').find('.panel-title').children("span").removeClass("pingbounce");
        setTimeout(
          function(){
            roomId = $this.parents('form').attr("action");
            $("#chat-rooms #chat_window_"+roomId+" .msg_container_base").mCustomScrollbar("scrollTo", "bottom", { scrollInertia:100 });
          }, 500);
    }
    setTimeout(
      function(){
        $.ajax({
          url: '<?php echo base_url("Chat/status"); ?>',
          type:"POST",
          data:{ "roomId": $this.parents('form').attr("action"), "rdstatus":1, "ntstatus":1},
          dataType:"json",
          success:function(data){}
        });
      }, 2000);
    
});
$("#chat-rooms").on('click', '#new_chat', function (e) {
    var size = $( ".chat-window:last-child" ).css("margin-left");
     size_total = parseInt(size) + 400;
    //alert(size_total);
    var clone = $( "#chat_window_1" ).clone().appendTo( ".container" );
    clone.css("margin-left", size_total);
});
$("#chat-rooms").on('click', '.icon_close', function (e) {
    var row = $(this);
    var roomId = row.attr("modal-aria");
	chat_state(roomId,'close');
    $.ajax({
      url: '<?php echo base_url("Chat/status"); ?>',
      type:"POST",
      data:{ "roomId": roomId, "ntstatus":1, "open":1},        
      dataType:"json",
      success:function(data){
        row.parents(".chat-window").remove();
      }
    });    
});
$("#chat-rooms").on("submit", "form", function(){
  var form = $(this);
  var roomId = form.attr("action");
  var msg = $("input[name='btn-input']",form).val();
  if(msg != ""){
    $.ajax({
      url:"<?php echo base_url('Chat/sendUserMessage'); ?>",
      type: "POST",
      data:{"roomId":roomId, "msg":msg},    
      dataType:"json",
      success:function(data){
        $(data.content).appendTo( form.parents('.panel').find('.panel-container'));
        $("input[name='btn-input']",form).val("");
        // form.parents('.panel').find('.panel-body').animate({ scrollTop: form.parents('.panel').find('.panel-body')[0].scrollHeight}, 1000);
        $("#chat-rooms #chat_window_"+roomId+" .msg_container_base").mCustomScrollbar("scrollTo", "bottom", { scrollInertia:100 });
      }
    });
  }
  return false;
});


<?php
    if($this->uri->segment(2) == "goodPlace"){
      ?>
      var map;
      var iconicon;
      var mapOptions;
      var infoWindow
      var allMyMarkers1 = [];

      markers1 = [
           <?php 
            if(isset($listing))
            {
              $c = count($listing) - 1;
              $i = 0;
              foreach ($listing as $businessL) { ?>
              {      
                "title": "<?php if(isset($businessL['event_name'])){echo $businessL['event_name'];}?>",
                "lat": '<?php if(isset($businessL['event_lat'])){echo $businessL['event_lat'];}?>',
                "lng": '<?php if(isset($businessL['event_long'])){echo $businessL['event_long'];}?>',
                "etype": '<?php if(isset($businessL['evt_type'])){echo $businessL['evt_type'];}?>',
                "edate": '<?php if(isset($businessL['event_start_date'])){echo date("d, M Y H:i", strtotime($businessL['event_start_date']));}?>',
              }
          <?php if($i < $c)
         {echo ",";}?>
         <?php $i++;
          }}?> 
          ];          
    function LoadMap1() {
      
      var paris = { lat: 48.8566, lng: 2.3522 };
      mapOptions = {
            center: paris,
            zoom:11,
            minZoom: 0, 
            maxZoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("eventmap"), mapOptions);
      var image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
      infoWindow = new google.maps.InfoWindow();
      for (var i = 0; i < markers1.length; i++) {
        var data = markers1[i];
        var myLatlng = new google.maps.LatLng(data.lat, data.lng);
        var marker = new google.maps.Marker({
            position: myLatlng,
            map: map,
            icon: image,
            id: i,
            title: data.title
        });
        allMyMarkers1.push(marker);

        (function (marker, data) {
            google.maps.event.addListener(marker, "click", function (e) {
                infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/><b>Event Type: </b>'+data.etype+'<br><b>Event Date: </b>'+data.edate+"</div>");
                infoWindow.open(map, marker);
            });
        })(marker, data);
      }


      google.maps.event.addDomListenerOnce(map, 'idle', function () 
      {
            google.maps.event.addDomListener(window, 'resize', function () {
            map.setCenter(data.lat, data.lng);
           
            });
      });


      $('.event-class').hover(function(){
        var selectedID = $(this).attr('id');
        toggleBounce1(selectedID);               
      });
	  toggleBounce1('pin-0');
    }

    function toggleBounce1(selectedID) {
      var pinID = selectedID.split('-');
      for(var j=0;j<allMyMarkers1.length;j++){

              if(allMyMarkers1[j].id == pinID[1]){
                      if (allMyMarkers1[j].getAnimation() != null) {
                              allMyMarkers1[j].setAnimation(null);
                      } else {
                              var mapOptions = {
                              center: new google.maps.LatLng(markers1[0].lat, markers1[0].lng),
                              minZoom: 0, 
                              maxZoom: 15,
                              //zoom:4,
                              //mapTypeId: google.maps.MapTypeId.ROADMAP
                              };
                             // map.setOptions({ minZoom: 5, maxZoom: 15 });
                             // map = new google.maps.Map(document.getElementById("map"), mapOptions);
                      map.set(mapOptions);
            iconicon = {
            url: "<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png", // url
            scaledSize: new google.maps.Size(30, 30), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(15, 15) // anchor
            };
                           //   var image = 'http://localhost/hmvc_site/assets/img/map-marker.png';
                           var image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";          

                              //Create and open InfoWindow.
            infoWindow = new google.maps.InfoWindow();
                              for (var i = 0; i < markers1.length; i++) {

                                 if(pinID[1] == i)
                                  {

                            iconicon = {
            url: "<?php echo base_url(); ?>assets/img/map-marker.png", // url
            scaledSize: new google.maps.Size(30, 30), // scaled size
            origin: new google.maps.Point(0,0), // origin
            anchor: new google.maps.Point(15, 15) // anchor
            };
                    }
                                  else
                                  {
                                    var image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
                                  }

                                  var data = markers1[i];
                                  var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                                  var marker = new google.maps.Marker({
                                      position: myLatlng,
                                      map: map,
                                      icon: image,
                                      id: i,
                                      title: data.title
                                  });
                                  //marker.setIcon(iconicon);
                                  allMyMarkers1.push(marker);

                                  (function (marker, data) {
                                      google.maps.event.addListener(marker, "click", function (e) {
                                      infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/><b>Event Type: </b>'+data.etype+'<br><b>Event Date: </b>'+data.edate+"</div>");
                                      infoWindow.open(map, marker);
                                      });
                                  })(marker, data);
                                
                              }

                              map.setCenter(allMyMarkers1[j].getPosition());                              
                      }
                      break; // stop continue looping
              }
      }
    }
    $(document).ready(function() {
      LoadMap1();
    });
      <?php
    }
 

 ?>
</script>


<?php
if($this->uri->segment(2) == "user_profile"){
  ?>
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
    //SET THESE VARS
    var $transitionLength = 400;
    var $timeBetweenTransitions = 4000;

    //STORAGE
    var imageCount = 0;
    var currentImageIndex = 0;
    var currentScrollIndex = 1;
    var $imageBank = [];
    var $thumbBank = [];
    var $mainContainer = $("#gallery-main");
    var $thumbContainer = $("#thumbcon");
    var $progressBar = $("#progressbar");
    var currentElement;

    //CONTROLS
    var $go = true;

    $(document).ready(function(){

      $("#gallery-hidden img").each(function() {
        $imageBank.push($(this).attr("id", imageCount));
        imageCount++;
      });

      generateThumbs();

      setTimeout(function () {
        imageScroll(0);
      }, $timeBetweenTransitions);

      $('#left-arrow').click(function () {
        thumbScroll("left");
        toggleScroll(true);
        });

      $('#right-arrow').click(function () {
        thumbScroll("right");
        toggleScroll(true);
        });

      $('#thumbcon img').on('click',function () {

        imageFocus(this);
      });

      $('#playtoggle').click(function () {
        toggleScroll(false);
      });
    });

    function progress(imageIndex){
      var parts = 960/imageCount-1;
      var pxProgress = parts*(imageIndex+1);

      $progressBar.css({ width: pxProgress , transition: "all 0.7s ease"});
    }

    function imageFocus(focus){
      for(var i = 0; i < imageCount; i++){
        if($imageBank[i].attr('src') == $(focus).attr('src')){
          $mainContainer.fadeOut($transitionLength);
          $thumbBank[currentImageIndex].removeClass("selected");
          setTimeout(function () {
            $mainContainer.html($imageBank[i]);
            $thumbBank[i].addClass("selected");
            $mainContainer.fadeIn($transitionLength);
          }, $transitionLength);
          currentScrollIndex = i+1;
          currentImageIndex = i;
          progress(currentImageIndex);
          toggleScroll(true);

          return false;
        }
      }
    }

    function toggleScroll(bool){
      if($go){
        $go = false;
        $('#playtoggle').children().removeClass('icon-pause').addClass('icon-play');
      }else{
        $go = true;
        $('#playtoggle').children().removeClass('icon-play').addClass('icon-pause');
      }

      if(bool){
        $go = false;
        $('#playtoggle').children().removeClass('icon-pause').addClass('icon-play');
      }
    }

    function autoScroll(){
      if(currentScrollIndex >= 0 || currentScrollIndex < imageCount){
        if(currentScrollIndex+1 > imageCount){
          $thumbBank[0].css({ marginLeft: "0" , transition: "all 1.0s ease"});
          currentScrollIndex = 1;
        }else if(currentScrollIndex+1 >= 3){
          if(currentScrollIndex+2 >= imageCount){

          }else
            $thumbBank[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});

          currentScrollIndex++;
        }else{
          currentScrollIndex++;
        }
      }
    }

    function thumbScroll(direction){
      if(currentScrollIndex >= 0 || currentScrollIndex < imageCount){
        var marginTemp = currentScrollIndex;
        if(direction == "left"){
          if(currentScrollIndex-3 <= 0){
            var k = ((imageCount-4)*200)-5;
            $thumbBank[0].css({ marginLeft: -k , transition: "all 1.0s ease"});
            currentScrollIndex = imageCount-1;
          }else{
            $thumbBank[0].css({ marginLeft: "+=200" , transition: "all 1.0s ease"});
            currentScrollIndex--;
          }
        }else if(direction == "right"){
          if(currentScrollIndex+3 >= imageCount){
            $thumbBank[0].css({ marginLeft: "5px" , transition: "all 1.0s ease"});
            currentScrollIndex = 1;
          }else{
            $thumbBank[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});
            currentScrollIndex++;
          }
        }
      }
    }

    function generateThumbs(){
      progress(currentImageIndex);
      for(var i = 0; i < imageCount; i++){

        var $tempObj = $('<img id="'+i+'t" class="thumb" src="'+$imageBank[i].attr('src')+'" />');

        if(i == 0)
          $tempObj.addClass("selected");

        $thumbContainer.append($tempObj);
        $thumbBank.push($tempObj);

      }
    }

    function imageScroll(c){
      if($go){

        $thumbBank[c].removeClass("selected");

        c++

        if(c == $imageBank.length)
          c = 0;

        $mainContainer.fadeOut($transitionLength);
        setTimeout(function () {
          $mainContainer.html($imageBank[c]);
          $thumbBank[c].addClass("selected");
          autoScroll("left");
          $mainContainer.fadeIn($transitionLength);
        }, $transitionLength);

      }

      progress(c);

      setTimeout(function () {
        imageScroll(currentImageIndex);
      }, $timeBetweenTransitions);

      currentImageIndex = c;
    }


  </script>

  <?php
    if($is_claiming == "yes")
    {
    ?>
    <script type="text/javascript">
      $(document).ready(function() {
       $("#shownumberexits").trigger("click");
      });
    </script>
    <?php
    }

    if($openphone == "openphonemodal")
    {
    ?>
      <script type="text/javascript">
      $(document).ready(function() {
       $("#shownumberexits_log").trigger("click");
      });
      </script>
    <?php
    }
    ?>




<script type="text/javascript">


    $(function() {
      // Initialize form validation on the registration form.
      // It has the name attribute "registration"
      $("form[name='myForm']").validate({
        rules: {
            user_phone:"required",
            user_firstname: "required",
            user_lastname: "required",
            user_email: {
              required: true,
              email: true
            },
            user_company_address:"required",
            confidential:"required"      
          },
        messages: {
          user_phone:"Please enter phone number",
          user_firstname: "Please enter your firstname",
          user_lastname: "Please enter your lastname",
          user_categories: "Please select business category",
          user_company_address:"Please enter company address",      
          confidential:"Please select terms & conditions",
          user_email: {
            required:"Please enter a valid email address",
            email:"Please enter valid email address"
          }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
          var recaptcha = $("#g-recaptcha-response").val();
          if (recaptcha === "") {
            event.preventDefault();
            $("#captchaerror").text("Please check the recaptcha");
            setTimeout(function(){
              $("#captchaerror").text("");
            }, 2000);
          }else{
            form.submit();
          }
        }
      });
    });



   /* function initMap() {
      var uluru = {lat: <?php echo $business['user_lat']; ?>, lng: <?php echo $business['user_long']; ?>};
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: uluru
      });
      var marker = new google.maps.Marker({
        position: uluru,
        map: map
      });
    }


    $(document).ready(function() {
      initMap();
    });
*/  
    function follow(pro_id, act)
    {
            var profile_id = pro_id;
            var data = "";
            var but="";
            <?php
            $flcontent = $this->db->get_where("otd_footer_content", array("ft_id"=>10))->row_array();
            ?>
            var flw = '<?php echo $flcontent['label4']; ?>';
            var unflw = '<?php echo $flcontent['label31']; ?>';
                                                    
            var msg="";
            if(act ==1)
            {
              msg = "Thanks for following";
              but = '<span><i class="fa fa-share"></i></span><a href="javascript:;" id="follow" onclick="return follow('+ profile_id + ', 2);">'+unflw+'</a>';
              data = 'profile_id=' + profile_id + '&follow_action=follow';
            }
            else
            {
              msg = "You have Unfollowed the business";
              data = 'profile_id=' + profile_id + '&follow_action=unfollow';
              but = '<span><i class="fa fa-share"></i></span><a href="javascript:;" id="follow" onclick="return follow('+ profile_id + ', 1);">'+flw+'</a>';
            }
            //alert(data);
            $.ajax({
                type: "POST",
                data: data,
                //url: "http://localhost/hmvc_site/Users/follow_user",
                url: "<?php echo base_url("Users/follow_user"); ?>", //CALLBACK FILE
                success: function (e) {
                    if(act==1)
                    {
                      $("#thanksModal").modal();
                    }
                    else
                    {
                       $("#unthanksModal").modal();
                    }                   
                    document.getElementById("follow_li").innerHTML = but;
                }
                // ,
                // error: function (e) {
                //     alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
                // }
            });
    }
    function showphone()
    {
      //alert("Hello ");
      $('#dsply-text').toggle();
      $('#dsply-num').toggle();
    }
</script>
<script>
  function recommend(rec_to_id)
  {
  
   data = 'rec_to=' + rec_to_id;
       $.ajax({
         type: "POST",
         data: data,
        // url: "http://localhost/hmvc_site/Users/recommend",
        url: '<?php echo base_url("Users/recommend"); ?>', //CALLBACK FILE
        success: function (e) {
         
          if(e>0)
          {
             $("#showthanksrecom").trigger("click");
             //alert("Thanks for recommendation ");
             //location.reload();
             document.getElementById("rec_count").innerHTML = e;
           }
           else  if(e==0)
          {
            
              //alert("You haev already recommend this business ");
              $("#showthanksallreadyrecom").trigger("click");
           }
           else 
           {
            alert(JSON.stringify(e));
           }
         },
         error: function (e) {
          //alert(JSON.stringify(e));
             alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
         }
     });
  }

</script>
<?php
}
 
?>

  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete" async defer></script>
  
 
  
  <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<?php
  $reg_success = $this->session->userdata('reg_success');
  if( trim($reg_success) == "yes")
  {
   ?>
    <script type="text/javascript">
      $(document).ready(function() {
      openModalreg();
      });
      function openModalreg(){
        $('#myModalregsucc').modal();
      } 
    </script>
  <?php
  }
  $this->session->set_userdata('reg_success', '');
  //$this->session->set_userdata('success_msg', '');
?>
<?php
  $reg_success = $this->session->userdata('pro_reg_success');
  if( trim($reg_success) == "Yes")
  {
  ?>
    <script type="text/javascript">
      $(document).ready(function() {
      openModalreg1();
      });
      function openModalreg1(){
        $('#myModalproregsucc').modal();
      } 
    </script>
<?php
  }
  $this->session->set_userdata('pro_reg_success', '');


    //slimpay transaction thank you
      $slimpay_success = $this->session->userdata('slimpay_success');
      if(!empty($slimpay_success))
      {
      ?>
        <script type="text/javascript">
          $(document).ready(function() {
            slimpaytransationcthankyou();
          });
          function slimpaytransationcthankyou(){
            $('#slimpaythankyou').modal();
          }
        </script>
        <?php
      }
      $this->session->set_userdata('slimpay_success', '');

      $slimpay_error = $this->session->userdata('slimpay_error');
      if(!empty($slimpay_error))
      {
      ?>
        <script type="text/javascript">
          $(document).ready(function() {
            slimpaytransationcthankyou();
          });
          function slimpaytransationcthankyou(){
            $('#slimpaytransactionfailed').modal();
          }
        </script>
        <?php
      }
      $this->session->set_userdata('slimpay_error', '');


  //claimed profile popup modal call  
  echo $claim_success = $this->session->userdata('claim_success');

  if( trim($claim_success) == "Yes")
  {
   ?>
    <script type="text/javascript">
      $(document).ready(function() {

        openModalreg2();
      });
      function openModalreg2(){
        $('#clmbsmodalbox').modal("show");
      } 
    </script>
  <?php
  }
  $this->session->set_userdata('claim_success', '');
  ?>  

  
<script>

  $('a[data-imagelightbox="demo"]').imageLightbox({
    selector:       'a[data-imagelightbox]',
    id:             'imagelightbox',
    allowedTypes:   'png|jpg|jpeg|gif',
    animationSpeed: 250,
    activity:       true,
    arrows:         true,
    button:         true,
    caption:        true,
    enableKeyboard: true,
    lockBody:       true,
    navigation:     true,
    overlay:        true,
    preloadNext:    true,
    quitOnEnd:      true,
    quitOnImgClick: true,
    quitOnDocClick: true,
    quitOnEscKey:   true
  });

</script>

<script type="text/javascript">
$(window).scroll(function() {   
       if ($(window).scrollTop() >=50) { 
          if($('#nav-top').hasClass( "navi" ))
{
  //jQuery('#header').removeClass("fixed-theme");
  }
  else
  {
  $('#nav-top').addClass("navi");
  }
}
else{
$('#nav-top').removeClass("navi");
}
});
</script> 
<script type="text/javascript">
  $(window).scroll(function() {   
        if ($(window).scrollTop() >=50) { 
          if($('.filter-sec').hasClass( "fs-top" ))
        {
      //jQuery('#header').removeClass("fixed-theme");
      }
      else
      {
      $('.filter-sec').addClass("fs-top");
      }
        }
    else{
    $('.filter-sec').removeClass("fs-top");
    }
   });
</script> 
<script type="text/javascript">
  $(window).scroll(function() {   
    if ($(window).scrollTop() >=50) { 
      if($('.map-main').hasClass( "map-top" ))
      {
        //jQuery('#header').removeClass("fixed-theme");
      }
      else
      {
        $('.map-main').addClass("map-top");
      }
    }
    else{
      $('.map-main').removeClass("map-top");
    }
  });
</script> 
<script type="text/javascript">
    // $(document).ready(function()
    // {
    //   $('.date').bootstrapMaterialDatePicker
    //   ({
    //     time: false,
    //     clearButton: true
    //   });     
    //   $('.time').bootstrapMaterialDatePicker
    //   ({
    //     date: false,
    //     shortTime: true,
    //     format: 'hh:mm A'
    //   });
    // });
</script> 
<script> 
  $(document).ready(function(){
      $(".extra-filter").click(function(){
          $(".extra-filter-sec").slideToggle("slow");
      });
  });
</script> 
  <script>
    $(document).ready(function() {

      var owl = $("#owl-demo");

      owl.owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        singleItem : true,
        items : 1, 
        itemsDesktop : [1000,1], 
        itemsDesktopSmall : [900,1], 
        itemsTablet: [600,1], 
        itemsMobile : false 
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });

    $(document).ready(function() {
      var owl = $("#owl-demo-picture");
      owl.owlCarousel({
        autoPlay : 3000,
        items : 3,
        itemsDesktop : [1000,3], 
        itemsDesktopSmall : [900,3], 
        itemsTablet: [600,2]
      });
    });

    $(document).ready(function() {
      var owl = $("#Event-slider");
      owl.owlCarousel({
        autoPlay : 3000,
        singleItem : true,
        items : 1
      });
    });
    </script>

<script>
$(document).ready(function() {   
  initAutocomplete();
  <?php
   $userloggedin = $this->session->userdata("user_id");
  if(empty($userloggedin)){
  ?>
  setTimeout(function(){    
   
    if(!$("#myModal2").hasClass("in") && !$("#myModal").hasClass("in") && !$("#myModal3").hasClass("in") && !$("#myModalregsucc").hasClass("in") && !$("#personalaccountreadmore").hasClass("in") && !$("#professionalaccountreadmore").hasClass("in")){            
    
 

      //checkCookie();
       if(!myfun()){
        $("#registrationmodal").trigger("click");
        var d = new Date();
        d.setTime(d.getTime() + (1*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        
        document.cookie = "UserSeeSignup=Yes;" + expires + ";path=/";
      }
      // else
      // {
      //   alert("I am out");
      // }
    }
  },6000); // 10000 to load it after 10 seconds from page load
  <?php
  }
  ?>

  $(".owl-demo").owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      pagination : true,
      singleItem:true
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
  });


});
</script> 

<script>
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
    }
    return "";
}
function myfun()
{
  
var user = getCookie("UserSeeSignup");
   if (user != "") {
      return true;
    } else {
      return false;
 }


}
function checkCookie() {
   var user = getCookie("UserSeeSignup");
    alert(user);
    if (user != "") {
      return true;
    } else {
      return false;
    }
}
</script> 
<script>
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'],  componentRestrictions: {country: "fr"}});

        var address = document.getElementsByClassName('citycomplete');
        for(var i=0; i< address.length; i++){
            new google.maps.places.Autocomplete(address[i], {types: ['geocode'],  componentRestrictions: {country: "fr"}});
        }
        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', cityAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          //alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
        cityAddress();
      }




      function cityAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

       // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
         
          if (componentForm[addressType] && addressType == "locality") {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById("searchedcity").value = val;
           // alert(addressType + " " + val);
            //alert(val);
          }
        }
      }



    </script> 
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script> 
 -->




<!-- Social Login --> 
<script type="text/javascript">
      var flag=false;
      // This is called with the results from from FB.getLoginStatus().
      
      function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
 
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          getUserInfo();
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          /*document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';*/
        }
      }

      // This function is called when someone finishes with the Login
      // Button.  See the onlogin handler attached to it in the sample
      // code below.
      function checkLoginState() {
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
      }

    
      // Load the SDK asynchronously

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1546847162054270', //1068492099877422 App ID (1776946472525281)
          // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true,  // parse XFBML
          version    : 'v2.8'
        });
        
        
        FB.Event.subscribe('auth.authResponseChange', function(response) 
        {
          if (response.status === 'connected') 
          {
            console.log("Connected to Facebook");
            //SUCCESS
             
          }  
          else if (response.status === 'not_authorized') 
          {
            console.log("Failed to Connect");

          //FAILED
          } else 
          {
          console.log("Logged Out");
            

            //UNKNOWN ERROR
          }
        }); 
      
      };
      
      function Login(){
      
        FB.login(function(response) {
          if (response.authResponse) 
          {
           
            getUserInfo();
             
          } else 
          {
             console.log('User cancelled login or did not fully authorize.');
          }
        }, {scope: 'email,user_birthday,user_location'});  
      }

      function getUserInfo() {
        
         
          FB.api('/me',{fields: 'id,email, first_name, last_name, picture.width(800).height(800),gender'}, function(response)
          {      
              $.ajax({
              method: "POST",
              url: "<?php echo base_url();?>Users/fblogin",
              dataType: 'json',
              data: {'id':response.id, 'first_name':response.first_name, 'last_name':response.last_name, 'email': response.email, 'picture': response.picture.data.url, 'gender': response.gender},
              success:function(data){              
                if(data.flag==200){
                  setTimeout(function(){ window.location.href = '<?php echo base_url('Users/account');?>';   }, 100);               
                  return false;  
                }else if(data.flag == 1){
                  alert("Votre compte pro est déjà créé, merci de bien vouloir vous connecter");
                  return false;
                }else{
                  console.log("PPPP"+data);
                }
              },
              error:function(data){   
                alert("Votre compte pro est déjà créé, merci de bien vouloir vous connecter ERRORR");
                
              }
            }); 

          });
      }

      function Logout()
      {
        FB.logout(function(){ window.location.replace("<?php echo base_url(); ?>Users/logout");});
      }

      // Load the SDK asynchronously
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));


      ////////////////////////////////////////Gmail Login////////////////////////////////////////////////

      /*Google signup*/
      window.onbeforeunload = function(e){
        gapi.auth2.getAuthInstance().signOut();
      };
      function onSignIn(googleUser){        
        BasicProfile = googleUser.getBasicProfile();
        var social_id = BasicProfile.getId();      
        var first_name = BasicProfile.getGivenName();
        var last_name = BasicProfile.getFamilyName();
        var image_path = BasicProfile.getImageUrl();
        var email = BasicProfile.getEmail();

        // var userId = profile.getId(); // Do not send to your backend! Use an ID token instead.
        // var userName = profile.getName();
        // var userEmail =  profile.getEmail();
        // var picture = profile.getImageUrl();
        //var DOB = profile.getBirthday()

        if(flag==false){           
          $.ajax({
              method:"POST",
              url:'<?php echo base_url();?>Users/googleLogin',
              dataType: 'json',
              data: {'id':social_id ,'email': email, 'first_name':first_name, 'last_name':last_name, 'picture':image_path},
              success:function(data){
                if(data.flag == 200){
                  window.location.href = '<?php echo base_url('Users/account');?>';
                  flag=true;
                  return false;
                }else if(data.flag == 1){
                  alert("Votre compte pro est déjà créé, merci de bien vouloir vous connecter");
                  return false;
                }else{
                  console.log(data);
                }
              }
          });
        } 
      }

      $("glog").click(function(){
          gp_signOut();
      });

      function gp_signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
              console.log('User signed out.');
          });
      }
  </script> 

<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script> 
<script>
    $("glog").click(function(){
        gp_signOut();
    });

    function gp_signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

    function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
        disassociate();
             $.ajax({
              method: "POST",
              dataType:"json",
              url: "<?php echo base_url();?>Users/socialLogout",
              success:function(data){
                 if(data.status == 200){
                   document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>";                     
                 }
              }
              }); 
      
        // document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>userlogin";
      });
    }
    
    function onLoad() {
      gapi.load('auth2', function() {
        gapi.auth2.init();
      });
    }
    function disassociate() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect().then(function () {
            console.log('User disconnected from association with app.');
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////// 
  </script> 
  

<script>    
    $(document).ready(function () {
    
        $('.dropdown-toggle').dropdown();

    });
</script> 
<!-- Social Login end -->

<?php

if(!empty($option10_success) && $option10_success=="yes")
{
 
  ?>
  <script type="text/javascript">
  $(document).ready(function() {
    $('#myOptionSuccess').modal();
  });
  
  </script>
  
  <?php

}
?>

<?php
$option9_success = $this->session->userdata("option9_success");
if($option9_success=="yes")
{
 
  ?>
  <script type="text/javascript">
  $(document).ready(function() {
  //  alert("OKK" + formoption9details);
    $('#formoption9details').modal();
  });
  
  </script>
  
  <?php
$this->session->set_userdata("option9_success", "");
}
?>

<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<script>

/*Code for Search Result Page---Pradeep */


/*
var map;
var iconicon;
var mapOptions;
var infoWindow
var allMyMarkers = [];
var marker;
var markers = [
     <?php 
   if(isset($businessMarker))
   {
    $c = count($businessMarker) - 1;
    $i = 0;
    foreach ($businessMarker as $businessL) {?>
    {
      
        "title": "<?php if(isset($businessL['user_company_address'])){echo $businessL['user_company_address'];}?>",
        "lat": '<?php if(isset($businessL['user_lat'])){echo $businessL['user_lat'];}?>',
        "lng": '<?php if(isset($businessL['user_long'])){echo $businessL['user_long'];}?>',
        "description": '<?php if(isset($businessL['user_company_name'])){echo $businessL['user_company_name'];}?>'
    }
   <?php if($i < $c)
   {echo ",";}?>
   <?php $i++;
    }}?> 
    ]; */


 

 /*
  function LoadMap() {
  
    var paris = { lat: 48.8566, lng: 2.3522 };
      mapOptions = {
            center: paris,
            zoom:5,
            minZoom: 0, 
            maxZoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("map"), mapOptions);
     var image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
    infoWindow = new google.maps.InfoWindow();
 
      for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
             marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: image,
                id: i,
                title: data.title
            });
            allMyMarkers.push(marker);
 
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/>'+ data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);

        }


  google.maps.event.addDomListenerOnce(map, 'idle', function () 
      {
            google.maps.event.addDomListener(window, 'resize', function () {
            map.setCenter(LatLng);
           
            });
    });
  

       $('.listing-sub').hover(function(){
                var selectedID = $(this).attr('id');
                toggleBounce(selectedID);
                  				
				
				});
     } */

function toggleBounce(selectedID) {
	 var image ="<?php echo base_url(); ?>assets/img/map-marker.png";
	var pinID = selectedID.split('-');
	var ppif = pinID[1];
	  marker.setIcone(image);
}
function toggleBounce_old(selectedID) {
  var image="";
   
  var pinID = selectedID.split('-');
        
        for(var j=0;j<allMyMarkers.length;j++){
         // image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
                if(allMyMarkers[j].id == pinID[1]){
                        if (allMyMarkers[j].getAnimation() != null) {
                                allMyMarkers[j].setAnimation(null);
                        } else {
                                var mapOptions = {
                                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                                zoom: 5,
                                minZoom: 0, 
                                maxZoom: 15,
                                };
                          map.set(mapOptions);
                          //image = 'http://localhost/hmvc_site/assets/img/map-marker.png';
                          infoWindow = new google.maps.InfoWindow();
                            for (var i = 0; i < markers.length; i++) {
                                   if(pinID[1] == i)
                                    {
                                    image ="<?php echo base_url(); ?>assets/img/map-marker.png";
                                    }
                                    else
                                    {
                                    image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
                                    }

                                   // alert(image);
								   
                                    var data = markers[i];
                                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                                    var marker = new google.maps.Marker({
                                        position: myLatlng,
                                        map: map,
                                        icon: image,
                                        id: i,
                                        title: data.title
                                    });
                                    allMyMarkers.push(marker);

                                    (function (marker, data) {
                                        google.maps.event.addListener(marker, "click", function (e) {
                                        infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/>'+ data.description + "</div>");
                                        infoWindow.open(map, marker);
                                        });
                                    })(marker, data);  
                                  
                                }
								 
                                 
                                map.setCenter(allMyMarkers[j].getPosition());
								 marker.setIcone(image); 
                                
                        }
                        break; // stop continue looping
                }
        }
} // end toggleBounce


 $(document).ready(function() {

  getLocation();
  /*LoadMap(); */

 $( ".map-wrapper-name" ).click(function() {
  google.maps.event.trigger(map, "resize");
});


 });


/*function showphone(id)
{
$('#'+id).toggle();
$('#txt'+id).toggle();
}
*/


function getLocation() {

   var options = {timeout:60000};
   var geolocation = navigator.geolocation;
   geolocation.getCurrentPosition(showLocation, errorHandler, options);
 

}
function showLocation(position) {
  GetAddress(position.coords.latitude, position.coords.longitude);

}

function errorHandler(err) {
            if(err.code == 1) {
              // alert("Error: Access is denied!" + err.message);
            }
            
            else if( err.code == 2) {
              // alert("Error: Position is unavailable!" + err.message);
            }

             //history.pushState({},"Results for `Cats`",'?your_loc=no_location');
                     
         }



  function GetAddress(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    var geocoder = geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'latLng': latlng }, function (results, status) {          
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[1]) {
          if(document.getElementById('autocomplete').value == "")
          {
            document.getElementById('autocomplete').value = results[1].formatted_address;
            $("a.categorylistingdev").each(function() {
              var $this = $(this);       
              var _href = $this.attr("href"); 
              $this.attr("href", _href + '&address='+results[1].formatted_address);
            });
          }
          //var uris ="?your_loc="+results[1].formatted_address;
          // history.pushState({},"Results for `Cats`",uris);
        }
      }
    });
  }


$(window).scroll(function() {   
       if ($(window).scrollTop() >=51) { 
          if($('#nav-top').hasClass( "navbar-fixed-top" ))
{
  //jQuery('#header').removeClass("fixed-theme");
  }
  else
  {
  $('#nav-top').removeClass("navi");
  }
}
else{
$('#nav-top').addClass("navi");
}
});


/* END Code for Search Result Page---Pradeep */

 
</script>
<script>
  

  function proLoadMap() {
    var paris = "";
    var lt = <?php echo empty($business['user_lat'])?46.2276:$business['user_lat']; ?>;
    var ln = <?php echo empty($business['user_long'])?2.2137:$business['user_long']; ?>;

    if(lt == "" || ln == ""){
      paris = { lat: 46.2276, lng: 2.2137 };
    }else{
      paris = { lat: lt, lng: ln };
    }
  
    mapOptions = {
          center: paris,
          zoom:15,
          minZoom: 0,
          mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("promap"), mapOptions);
  
    var image ="<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
    infoWindow = new google.maps.InfoWindow();
 
    var marker = new google.maps.Marker({
        position: paris,
        map: map,
        icon: image,
                     
    });
           
  }
  $(document).ready(function() {
    proLoadMap();
  });

  function showemail(review_id){
     // $("#showreviewemail").trigger("click");
       var data = "";
       data = 'vote_id=' + review_id ;
     $.ajax({

                type: "POST",
                data: data,
               //url: "http://localhost/hmvc_site/Users/reviewcomplaint",
               url: "<?php echo base_url("Users/reviewcomplaint"); ?>", //CALLBACK FILE
                success: function (e) {

                  
                    $("#showreviewemail").trigger("click");
                   
                    
                },
                error: function (e) {
                    alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
                    //$("#showreviewemail").trigger("click");
                }
            });

  }
</script>

  <script type="text/javascript">
    <?php
      $logincontent = $this->db->get_where("otd_footer_content", array("ft_id"=>2))->row_array();
    ?>
    $(document).ready(function () {
      var gbtn = '<?php  echo $logincontent['label3']; ?>';      
      setTimeout(function(){
        $("span.abcRioButtonContents").text(gbtn);
      }, 1000);
    });
  </script>
  <?php
     $user = $this->session->userdata("isUserLoggedIn");
	  
      if($user > 0){
        $inactive = 300;
        $session_life = (time() - $this->session->userdata('timeout')) / 60;  
         ?>

		 <?php 
       if($session_life > $inactive){
		   
        $social_login = $this->session->userdata("social_login");
        if(!empty($social_login)){
		   ?>
 		 	<script>
  
		      $(document).ready(function(){
					gapi.load('auth2', function() {
					  gapi.auth2.init();
					});
					
				  setTimeout(function(){ 
				   
                        $("#logoutbutton").trigger("click");
				  }, 9000);
				  
				  
			  });
			   
		 
		   </script>
             <?php 		   
			
			
			}else{
				
				  redirect(base_url("Users/logout"));
				
			}		   
          //        
        }else{
             $this->session->set_userdata('timeout', time());  
             $ci_session['timestamp'] = time();
			 $this->db->where("user_id",$this->session->userdata('user_id'));
             $this->db->update("ci_sessions",$ci_session); 
		 
        }      
     }  ?> 
<script>
  <?php
  if($this->uri->segment(2) == "businessListing"){
  ?>
    var map;
    var icon1 = "<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png";
    var icon2 = "<?php echo base_url(); ?>assets/img/map-marker.png";
    var curlocicon = "<?php echo base_url(); ?>assets/img/map-cur-marker.png";
    var allMarkers = [];
     
    function LoadMap() {
    	
    	console.log('LoadMap_1');
     
       map = new google.maps.Map(document.getElementById("map"), 
       {
          center: new google.maps.LatLng(48.8566, -2.3522),
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      infoWindow = new google.maps.InfoWindow();    
      //Create the marker 1
      <?php 
      
        if(isset($businessMarker))
        {
          $c = count($businessMarker) - 1;
          $i = 0;

          //show user search location marker
          $searchlatlong = $this->session->userdata("search_latlong");
          if(!empty($searchlatlong)){
            $cur_latlng = explode(",", $searchlatlong);
            ?>        
            var marker<?php echo $i;?> = new google.maps.Marker({
              position: new google.maps.LatLng(<?php echo $cur_latlng[0]; ?>, <?php echo $cur_latlng['1']; ?>),
              map: map,
              id: <?php echo $i; ?>,
              icon: curlocicon,
              title: '<?php echo str_replace("'", "\'", $this->session->userdata('search_location')); ?>',
              description: '<?php echo str_replace("'", "\'", $this->session->userdata('search_location')); ?>'
            });
            allMarkers.push(marker<?php echo $i;?>); //Add it to allMarkers
            google.maps.event.addListener(marker<?php echo $i; ?>, "click", function (e) {
              //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
              infoWindow.setContent("<div style = 'width:300px;min-height:50px'><h4><?php echo str_replace("'", "\'", $this->session->userdata('search_location')); ?></h4></div>");
              infoWindow.open(map, marker<?php echo $i; ?>);
            });
            <?php
            $i++;
          }
          //show user search location marker end
          foreach ($businessMarker as $businessL) {?> 
            var marker<?php echo $i;?> = new google.maps.Marker({
                position: new google.maps.LatLng(<?php if(isset($businessL['user_lat'])){echo $businessL['user_lat'];}?>, <?php if(isset($businessL['user_long'])){echo $businessL['user_long'];}?>),
                map: map,
                id: <?php echo $i; ?>,
                icon: icon1,
                title: "<?php if(isset($businessL['user_company_name'])){echo str_replace("'", "\'",$businessL['user_company_name']); } ?>",
                description: '<?php if(isset($businessL['user_company_address'])){echo str_replace("'", "\'", $businessL['user_company_address']);
                        str_replace("'", "\'", $businessL['user_company_address']);
                        }?>'
            });
            allMarkers.push(marker<?php echo $i;?>); //Add it to allMarkers
            google.maps.event.addListener(marker<?php echo $i; ?>, "click", function (e) {
              //Wrap the content inside an HTML DIV in order to set height and width of InfoWindow.
              infoWindow.setContent("<div style = 'width:300px;min-height:50px'><h4><?php if(isset($businessL['user_company_name'])){echo str_replace("'", "\'", $businessL['user_company_name']);}?></h4><br/><?php if(isset($businessL['user_company_address'])){echo str_replace("'", "\'", $businessL['user_company_address']);}?></div>");
              infoWindow.open(map, marker<?php echo $i; ?>);
            });
            <?php
            $i++;
          }

        } ?>
        google.maps.event.addDomListenerOnce(map, 'idle', function () 
        {
          map.setCenter(allMarkers[0].getPosition());
          google.maps.event.addDomListener(window, 'resize', function () {
            map.setCenter(<?php if(isset($businessL['user_lat'])){echo $businessL['user_lat'];}?>, <?php if(isset($businessL['user_long'])){echo $businessL['user_long'];}?>);           
          });
        });
     
    }


    //Function called when hover the div
    function hover(id) {
      for ( var i = 0; i< allMarkers.length; i++) {
        if (id === allMarkers[i].id) {
       	  map.setCenter(allMarkers[i].getPosition());      
          allMarkers[i].setIcon(icon2);
          infoWindow.setContent("<div style = 'width:300px;min-height:50px'><h4>"+allMarkers[i].getTitle()+"</h4><br/>"+allMarkers[i].description+"</div>");
          infoWindow.open(map, allMarkers[i]);
          break;
        }
      }
    }

    //Function called when out the div
    function out(id) {  
      for ( var i = 0; i< allMarkers.length; i++) {
        if (id === allMarkers[i].id) {
          allMarkers[i].setIcon(icon1);
          infoWindow.close();
          break;
        }
     }
    }
  <?php
  }
  ?>

</script>


<script>
$(document).ready(function(e) {
	$(".fs-option").click(function(e) {
      if ($(this).siblings(".fs-option.selected").length > 3) {        
        alert('You can select upto 3 options only');
		$(this).removeClass("selected");
    	}  
    });
	
});
</script>

<script>
$(document).ready(function () {
    $('.close-filter').on('click', function(e) { 
      //idd = $(this).parent("button").attr('id');
      idd = $(this).parent("div").attr('id');
      $("#"+idd).prop('checked', false); 
      // $(this).parent("button").remove(); 
       $(this).parent(".btn_close").remove(); 
      
        $('#searchForm').submit();
    });
});  
</script>
 

<script>
 // Creare's 'Implied Consent' EU Cookie Law Banner v:2.4
// Conceived by Robert Kent, James Bavington & Tom Foyster
 
var dropCookie = true;                      // false disables the Cookie, allowing you to style the banner
var cookieDuration = 14;                    // Number of days before the cookie expires, and the banner reappears
var cookieName = 'complianceCookie1';        // Name of our cookie
var cookieValue = 'on';                     // Value of cookie
 
function createDiv(){
    var bodytag = document.getElementsByTagName('body')[0];
    var div = document.createElement('div');
    div.setAttribute('id','cookie-law');
    div.innerHTML = "<div class='cok-contain'><h3>Utilisation des cookies</h3><p>En poursuivant votre navigation sans modifier vos paramètres, vous acceptez l'utilisation des cookies ou technologies similaires pour disposer de services et d'offres adaptés à vos centres d'intérêts ainsi que pour la sécurisation des transactions sur notre site. Pour plus d’informations, <a class='' target='_blank' href='<?php echo base_url("assets/doc/CGU_MOOVSIT_OTOURDEMOI_V4.2.pdf"); ?>' rel='nofollow' title='Privacy &amp; Cookies Policy'>cliquez ici</a> <a class='btn btn-danger' href='javascript:void(0);' onclick='removeMe();'><i class='fa fa-times'></i></a></p>";    
 // Be advised the Close Banner 'X' link requires jQuery
     
    // bodytag.appendChild(div); // Adds the Cookie Law Banner just before the closing </body> tag
    // or
    bodytag.insertBefore(div,bodytag.firstChild); // Adds the Cookie Law Banner just after the opening <body> tag
     
    document.getElementsByTagName('body')[0].className+=' cookiebanner'; //Adds a class tothe <body> tag when the banner is visible
     
    createCookie(window.cookieName,window.cookieValue, window.cookieDuration); // Create the cookie
}
 
 
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000)); 
        var expires = "; expires="+date.toGMTString(); 
    }
    else var expires = "";
    if(window.dropCookie) { 
        document.cookie = name+"="+value+expires+"; path=/"; 
    }
}
 
function checkCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}
 
function eraseCookie(name) {
    createCookie(name,"",-1);
}
 
// window.onload = function(){
//     if(checkCookie(window.cookieName) != window.cookieValue){
//       createDiv(); 
//     }
// }

function removeMe(){
	var element = document.getElementById('cookie-law');
	element.parentNode.removeChild(element);
}

$(document).ready(function(){
   if(checkCookie(window.cookieName) != window.cookieValue){
  //  eraseCookie(window.cookieName);
  createDiv(); 
   }
});

</script>

  <?php
  if($this->uri->segment(2) == "full_map"){
    ?>
    <script>
      function initMap() {
        var uluru = {lat: <?php echo $business['user_lat']; ?>, lng: <?php echo $business['user_long']; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }


      $(document).ready(function() {
        initMap();
      });
    </script>
    <?php
  }
  ?>
</body>
</html>