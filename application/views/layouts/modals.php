<?php
$logincontent = $this->db->get_where("otd_footer_content", array("ft_id"=>2))->row_array();
$registercontent = $this->db->get_where("otd_footer_content", array("ft_id"=>3))->row_array();
$forgetcontent = $this->db->get_where("otd_footer_content", array("ft_id"=>4))->row_array();
?>
<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title"><?php echo $logincontent['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <h2>User Login</h2>
          <form action="<?php echo base_url();?>Users/login" method="post">
            <div class="row">
              <div class="social-media-login">
                <a href="javascript:;" class="btn btn-default fb-so" onClick="Login()"><i class="fa fa-facebook"></i><?php echo $logincontent['label2']; ?></a>                
                <div class="g-signin2" data-onsuccess="onSignIn"><?php echo $logincontent['label3']; ?></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="email" class="form-control" name="user_email" placeholder="<?php echo $logincontent['label8']; ?>"  value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="password" class="form-control" name="user_password" placeholder="<?php echo $logincontent['label9']; ?>" required>
                  <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="submit" name="loginSubmit" class="btn-primary" value="<?php echo $logincontent['label4']; ?>"/>
                </div>
              </div>
              <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal3"><?php echo $logincontent['label5']; ?></button>
              <button type="button" class="btn btn-default new-acc-m" data-dismiss="modal" data-toggle="modal" data-target="#myModal"><?php echo $logincontent['label6']; ?></button>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $logincontent['label7']; ?></button>
      </div>
    </div>
  </div>
</div>


 <!-- Modal -->

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title"><?php echo $registercontent['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="select-acount">
          <div class="select-sub">
            <img src="<?php echo base_url('assets/img/loginicon.png'); ?>">
            <h2><?php echo $registercontent['label2']; ?></h2>
            <div class="overly-sub-select">
              <a class="btn btn-default" href="<?php echo base_url();?>Users/registration"><?php echo $registercontent['label3']; ?></a>
              <a href="<?php echo base_url("Pages/readMore/personal"); ?>" class="learn-mre"><?php echo $registercontent['label4']; ?></a>
            </div>
          </div>
          <div class="select-sub">
            <img src="<?php echo base_url('assets/img/rightimgdiv.png'); ?>">
            <h2><?php echo $registercontent['label5']; ?></h2>
            <div class="overly-sub-select">
              <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration"><?php echo $registercontent['label6']; ?></a>
              <a href="<?php echo base_url("Pages/readMore/professional"); ?>" class="learn-mre"><?php echo $registercontent['label7']; ?></a>
            </div>
          </div>
        </div>

      <div class="subscription">
          <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="mySubs">
            <div class="newsletter-main-head">
              <div class="news-letter-head">
                <h4><?php echo $registercontent['label8']; ?></h4>
                <p><?php echo $registercontent['label9']; ?></p>
            
              <div class="row news-letter-form">
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" required="" name="user_email" class="inputbox">
                </div>
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                </div>
                <div class="col-md-2">
                  <input type="submit" name="subscribeSubmit" onClick="" value="<?php echo $registercontent['label10']; ?>" class="submit-main">
                </div>
              </div>
              <div class="row agree-sec">
                <div class="col-md-12">
                  <input type="checkbox" title="Terms and Conditions" required="" name="terms">
                  <span><?php echo $registercontent['label11']; ?></span>
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $registercontent['label12']; ?></button>
      </div>
    </div>
  </div>
</div>


<!--     Forget Password Model -->
<div class="modal fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title"><?php echo $forgetcontent['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <form action="<?php echo base_url();?>Users/forgetpassword" method="post">
            <div class="row">
              <div class="col-sm-8">
                <div class="form-group">
                  <input type="text" class="form-control" name="user_email" placeholder="<?php echo $forgetcontent['label4']; ?>" required>
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="form-group sub-m">
                  <input type="submit" name="passwordSubmit" class="btn-primary" value="<?php echo $forgetcontent['label2']; ?>"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $forgetcontent['label3']; ?></button>
      </div>
    </div>
  </div>
</div>

