<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Pradeep Verma">
    <meta name="google-signin-scope" content="profile email">
    <meta name="google-signin-client_id" content="185961027889-o82v14cenj8uvcnptkq2vd4ksim1kkth.apps.googleusercontent.com">
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>Listing - <?php echo $title;?></title>
    <!-- Bootstrap Core CSS -->

  <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
  <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/master.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.theme.css">
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/blog_rating.css"/>

 <!-- Custom Core CSS -->

  <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> 
<!-- <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597c1b975d21460011308163&product=inline-share-buttons"></script> -->
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597c1b975d21460011308163&product=inline-share-buttons"></script>

</head>
<body>

<header id="main_header_id" >
    <?php
    $headercontent = $this->db->get_where("otd_footer_content", array("ft_id"=>5))->row_array();
    ?>
   <div class="header-main">
       <nav class="navbar navbar-default navbar-fixed-top back-color" id="nav-top">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right main-nav">
               <?php 
                    if($this->session->userdata('user_id') && $this->session->userdata('user_id') >0)
                    {
                        echo $this->session->userdata("user_id");
                        $userdetails = $this->db->select("user_firstname")->get_where("users", array("user_id"=>$this->session->userdata("user_id")));
                        ?>
                        <li class="user-intro dropdown">
                          <a href="<?php echo base_url("Users/account"); ?>" class="dropdown-toggle" data-toggle="dropdown">Hello <?php echo $userdetails->result()[0]->user_firstname; ?>
                          <span class="caret"></span></a>
                          <ul class="dropdown-menu">
      <li><a href="<?php echo base_url("Users/account/inbox"); ?>">
          <i class="fa fa-envelope"></i>          
          <?php 
            echo $headercontent['label4'];
            $sid= $this->session->userdata('user_id');
            $unreadmsg = $this->db->select("msg_id")                
                ->where("msg_reciever = $sid AND us_notify = 0")
                ->group_by("msg_sender")
                ->get("otd_user_messaging");  
            if($unreadmsg->num_rows()) {
              ?>
              <span class="counter-msg">
                <?php
                  echo $unreadmsg->num_rows();
                ?>
              </span>
              <?php
            }
          ?>
        </a></li>
      <li>
        <a href="<?php echo base_url("Users/account/notifications"); ?>">
          <i class="fa fa-bell" aria-hidden="true"></i>          
          <?php 
            echo $headercontent['label5'];
            $sid= $this->session->userdata('user_id');
            $unreadnoti = $this->db->where("nt_to = $sid AND nt_flag = 0")
                                  ->count_all_results("otd_notifications");  
            if($unreadnoti > 0) {
              ?>
              <span class="counter-not">
                <?php
                  echo $unreadnoti;
                ?>
              </span>
              <?php
            }
          ?>
          <!-- <span class="counter-not">10</span> -->
        </a>
      </li>
      <li><a href="<?php echo base_url("Users/account"); ?>"><i class="fa fa-user" aria-hidden="true"></i><?php echo $headercontent['label6']; ?></a></li>
      <?php  
        $social_login = $this->session->userdata("social_login");
        if(!empty($social_login)){
          if($social_login['is_social'] && $social_login['social_type'] == "facebook"){
          ?>
          <li><a id="logoutbutton" href="javascript:;" onclick="Logout();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
          <?php
          }
          else if($social_login['is_social'] && $social_login['social_type'] == "google"){
            ?>
            <li><a id="logoutbutton" href="javascript:void(0);" onClick="signOut();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
            <?php
          }
        }else{
        ?>
        <li><a id="logoutbutton" href="<?php echo base_url();?>Users/logout"><i class="fa fa-power-off" aria-hidden="true"></i><?php echo $headercontent['label7']; ?></a></li>
        <?php
        }
        ?>
    </ul>
                        </li>
                        <?php
                      }
                      else{              ?>
                    <li><a id="loginmodal" href="javascript:;" data-toggle="modal" data-target="#myModal2"><?php echo $headercontent['label1']; ?></a></li>
                    <li><a href="javascript:;" data-toggle="modal" data-target="#myModal"><?php echo $headercontent['label2']; ?></a></li>
                    <?php
}
      ?>
        <li class="good-place"><a href="#"><?php echo $headercontent['label3']; ?> <i class="fa fa-plus"></i></a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </div>
</header>
<div class="main-content">
  <section class="main-search-sec inner-page-heading <?php if($this->uri->segment(2) == "thanks") echo "thk-section"; ?>">
    <div class="container">
      <div class="header-srch">
        <div class="row">
          <div class="col-md-12">
            <div class="main-srch-heading inner-page-heading">
              <h2><?php echo $title;?></h2>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
</div>
<div id="page-wrapper" class="top-pad center-div <?php if($this->uri->segment(2) == "thanks") echo "thk-page"; ?>">            
  <?php echo $contents;?>
</div>
     
<!-- Modals -->
<?php
include "modals.php";
?>
<!-- end modals -->   
<!-- footer  -->
<?php
include "footer.php";
?>
<!--  End Footer -->
