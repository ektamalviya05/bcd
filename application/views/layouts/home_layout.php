
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <base href="<?php echo base_url();?>"></base>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ilbait</title>
<link rel="icon" href="assets_new/images/favicon.ico" type="image/ico" sizes="16x16">
<link rel="stylesheet" type="text/css" href="assets_new/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/master.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="assets_new/css/animate.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="assets_new/css/component.css" />
<link rel="stylesheet" type="text/css" href="assets_new/css/lightslider.css" />
<link rel="stylesheet" type="text/css" href="assets_new/css/asRange.css" />

 <?php
              if($this->session->userdata('lang_id') == 23 ) {
                               

                               
              ?>
              <link rel="stylesheet" type="text/css" href="assets/css/arabic.css" />
               
              <?php 
                }

               
              ?>  


<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ&libraries=places&callback=initAutocomplete"></script>
<!--<script src="assets_new/js/modernizr.custom.js"></script>-->
</head>

<body class="home_page">
  
<article class="slider-arti">
  <header id="main_header_id">
    <nav class="navbar navbar-default  navbar-static-top menu-header wow fadeInDown animated" id="nav-top">
      <div class="container">
        <div class="navbar-header">

          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          <a class="navbar-brand" href="home"><img src="assets_new/images/logo.png"></a> </div>
        <div class="collapse navbar-collapse text-right" id="bs-example-navbar-collapse-1">
          
          <ul class="nav navbar-nav main-nav">
            <li><a href="home" class="<?php echo (isset($active_tab_home)? $active_tab_home :"") ; ?>"><?php echo $top_content[0]['home']; ?></a></li>
            <!-- <li><a href="<?php echo base_url();?>listings">Explore</a></li> -->
            <li><a class="<?php echo (isset($active_tab_listings)? $active_tab_listings :"") ; ?>" href="<?php echo base_url();?>listings"><?php echo $top_content[0]['listings']; ?></a></li>
            <li><a class="<?php echo (isset($active_tab_category)? $active_tab_category :"") ; ?>" href="<?php echo base_url();?>categories"><?php echo $top_content[0]['Category']; ?></a></li>
            <li><a class="<?php echo (isset($active_tab_offer)? $active_tab_offer :"") ; ?>" href="<?php echo base_url();?>offers"><?php echo $top_content[0]['Offer']; ?></a></li>
          </ul>
          <div class="box_headeRight">
            <div class="box_logSign">
              <?php
              if(!($this->session->userdata('lang_id')) || $this->session->userdata('lang_id') == 20 ) {
                                 $email  = "Email";
                                 $password = "Password";
                                 //$forget_password = "?";

                                 //$category_desc  = $categories['cat_description'];
                               } else {
                                 $email  = "البريد الإلكتروني";
                                 $password  = "كلمه السر";
                                 //$forget_password = "؟";
                                 //$category_desc  = $categories['cat_descriptiona'];
                               }
              ?>                 
              <ul>
                   <?php if(!$this->session->userdata('isUserLoggedIn')){?> 
                <li class="login"><a class="log-op <?php echo (isset($active_tab_login)? $active_tab_login :"") ; ?>" href="javascript:void(0)"><img src="assets_new/images/icon_login.png"> <?php echo $top_content[0]['Login']; ?> <b class="caret"></b></a>
                    <div class="login-drop" style="display: none;">
                        <form id="loginform" name="loginform" action="do_login" method="post">
                        <div class="f-login">
                            <input type="eamil" name="email" id="email" placeholder="<?php echo $email;?>"><?php echo form_error('email', '<span class="error">', '</span>'); ?>
                        </div>
                        <div class="f-login">
                            <input type="Password" name="password" id="password" placeholder="<?php echo $password;?>"><?php echo form_error('password', '<span class="error">', '</span>'); ?>
                        </div>
                        <div class="submit-login">
                              <input type="submit" name="" value="<?php echo $top_content['0']['Login']?>">
                        </div>
                        <div class="f-login">
                          <ul class="social">
                            <li>
                              <a href="<?php echo base_url();?>Users/google_login"><i class="fa fa-google-plus"></i></a>
                            </li>
                            <li>
                              <a href="<?php echo $authUrl ; ?>"><i class="fa fa-facebook"></i></a>
                            </li>
                          </ul>
                          <ul>
                            <li><a href="signup"><?php echo $top_content[0]['Sign_up']; ?></a></li>
                            <li><a href="forgetpassword"><?php echo $top_content[0]['forget_password'];?></a></li>
                          </ul>
                        </div>
                      </form>
                    </div>                  
                </li>
                <li><a class="<?php echo (isset($active_tab_signup)? $active_tab_signup :"") ; ?>" href="signup"><img src="assets_new/images/icon_signUp.png"> <?php echo $top_content[0]['Sign_up']; ?></a></li>
                  <?php } ?>
                <li class="dropdown "><a href="javascript:;" id="drop1" data-toggle="dropdown" class="dropdown-toggle" role="button"><img src="assets_new/images/icon_map.png">
                  <?php if($this->session->userdata('lang_id')){
                      if($this->session->userdata('lang_id')==20){
                        //echo $top_content[0]['lang_name'];
                        echo "English";
                      }else{
                        //echo $top_content[0]['lang_name'];
                        echo "Arabic";
                      }
                  }else{ ?>
                  Language
                  <?php } ?>

                   <b class="caret"></b></a>
                  <ul role="menu" class="dropdown-menu lang-menu" aria-labelledby="drop1">

                   

                   <!--  <li role="presentation"><a href="<?php echo base_url();?>details/2/en" role="menuitem">English</a></li>
                    <li role="presentation"><a href="<?php echo base_url();?>details/ar" role="menuitem">Arabic</a></li> -->

                   
                        <!-- <li role="presentation"><a href="<?php echo base_url();?><?php echo $segment;?>/en" role="menuitem">English</a></li>
                    <li role="presentation"><a href="<?php echo base_url();?><?php echo $segment?>/ar" role="menuitem">Arabic</a></li> -->
                     
                    <li onclick="lang_data('en');">English</li>
                    <li onclick="lang_data('ar');">Arabic</li> 



                    
                  </ul>
                </li>
              </ul>
            </div>
              <?php if($this->session->userdata('isUserLoggedIn')){?> 
              <div class="add_listing"> <a href="dashboard" class="btn_addListing"> <?php echo $top_content[0]['my_profile']; ?></a> </div> 
              <?php } ?>
              <?php if($this->session->userdata('isUserLoggedIn')){?>

              <!-- <div class="add_listing"> <a href="<?php echo base_url();?>addlisting " class="btn_addListing"> <i class="fa fa-plus" aria-hidden="true"></i> <?php echo $top_content[0]['add_listings']; ?></a> </div>  -->



                <?php if($current_plan){?> 
              <div class="add_listing"> <a href="<?php echo base_url();?>addlisting " class="btn_addListing"> <i class="fa fa-plus" aria-hidden="true"></i> <?php echo $top_content[0]['add_listings']; ?></a> </div> 
              <?php } else { ?>
              <div class="add_listing"> <a href="<?php echo base_url();?>buy-package-plan " class="btn_addListing"> <i class="fa fa-plus" aria-hidden="true"></i> <?php echo $top_content[0]['add_listings']; ?></a> </div>
              <?php } ?>
              
              <?php }else if(!$this->session->userdata('isUserLoggedIn')){ ?>
                <div class="add_listing disable"> 
                  <a href="<?php echo site_url().'signup'; ?>" class="btn_addListing"> 
				              <i class="fa fa-plus" aria-hidden="true"></i> <?php echo $top_content[0]['add_listings']; ?>
                  </a> 
                </div> 
              <?php }else{} ?>
          </div>
        </div>
      </div>
    </nav>
  </header>
</article>
<script>

  function lang_data(id){

    //alert('hello');
    $.ajax({
        url: "<?php echo base_url();?>Pages/lang/",
        type: "post",
        data: ({id:id}),
        dataType: "html" ,
        success: function (data) {

          //alert(data);
            
        window.location.reload(true);

        },
      
    });
  }
  </script>
 <?php echo $contents;?>
<?php include 'footer.php';?>

