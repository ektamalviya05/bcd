<footer class="main_footer">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="ft_logo"> <a href="home"><img src="assets_new/images/logo.png" /> </a></div>
                </div>
                <div class="col-md-6 col-sm-6">                    
                    <section class="email_sub">                        
                        <div class="email_su_search">
                            <div class="search-container">
                                <form action="email_subscription" method="post" name="email_subscription" id="email_subscriptionss" enctype="multipart/form-data">
                                    <span class="email_su_icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </span>
                                    <input type="text" placeholder="Subscribe to Our News" name="news_email" id="news_email">                                    
                                    <div id="success_msg"></div>
                                    <button type="submit" id="emailnews"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                    </section>                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="f_middle wow fadeInDown animated">
                <div class="">
                    <div class="working_hours">
                        <div class="row">
                            <div class="col-sm-9">
                                <ul>
                                    <li><a href="home">Home</a></li>
                                    <li><a href="javascript:;">Explore</a></li>
                                    <li><a href="javascript:;">Listings</a></li>
                                    <li><a href="javascript:;">More</a></li>
                                    <li><a href="javascript:;">About us </a></li>
                                    <li><a href="<?php echo base_url();?>contact">Contact us</a></li>
                                </ul>
                            </div>

                            <div class="col-sm-3">
                                <div class="f_heading footer-add">
                                    <div class="social-demo">
                                        <ul>
                                            <li><a href="javascript:;"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="javascript:;"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a href="javascript:;"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="javascript:;"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="javascript:;"><i class="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="f_follow text-center">© (ilbait ) 2018. All rights reserved.</div>
                </div>
                <div class="col-sm-6">
                    <div class="copy_left">
                        <ul>
                            <li><a href="javascript:;"><img src="assets_new/images/img_app.png"/></a></li>
                            <li><a href="javascript:;"><img src="assets_new/images/img_app.png"/></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<style>
    .email_su_search input[type="text"] {
        width: 100%;
        border: 0;
        padding: 15px 45px;
        letter-spacing: 1px;
        background: #fff;
        box-shadow: 0 0px 3px rgba(0, 0, 0, 0.33);
        border-radius: 40px;
        margin: 3px 0;
    }
    .email_su_icon {
        position: absolute;
        left: 10px;
        padding: 0px;
        margin: 0;
        top: 12px;
    }
    .search-container {
        position: relative;
        padding-bottom: 20px;
    }
    div#success_msg {
        position: absolute;
        bottom: 0;
        color: #f10;
        font-size: 12px;
        left: 20px;
        width: 100%;
    }
    section.email_sub {
        width:  100%;
        padding:  00;
        border-radius:  0;
        box-shadow:  none;
    }
    .email_su_search button {
        background: transparent;
        border: 0px solid #fff;
        padding: 15px;
        letter-spacing: 1px;
        text-transform: uppercase;
        position: absolute;
        right: 10px;
        top: 6px;
    }
    .search-container label.error {
        position:  absolute;
        left: 19px;
        bottom: -3px;
        width:  100%;
        font-size:  12px;
        font-weight: 400;
    }
</style>
<script type="text/javascript" src="assets_new/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets_new/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets_new/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets_new/js/wow.min.js"></script>
<script src="assets_new/js/lightslider.js"></script> 
<script src="assets_new/js/jquery-asRange.js"></script>
<script src="assets_new/js/cleave.js"></script>
<script src="assets_new/js/cleave.min.js"></script>
<script src="assets_new/js/cleave-phone.i18n.js"></script>
<script src="assets_new/js/jquery.validate.js"></script>
<script src="assets_new/js/pages/addlisting.js"></script>
<script src="assets/js/custom.js"></script>
<script>
    wow = new WOW(
            {
                animateClass: 'animated',
                offset: 100
            }
    );
    wow.init();
</script>
<script>
    jQuery(document).ready(function () {
        jQuery("#content-slider").lightSlider({
            loop: true,
            keyPress: true
        });
        jQuery('#image-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 4,
            slideMargin: 0,
            speed: 500,
            auto: true,
            loop: true,
            responsive: [
                {
                    breakpoint: 800,
                    settings: {// settings for width 480px to 800px
                        thumbItem: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {// settings for width 0px to 480px
                        thumbItem: 2
                    }
                }
            ],
            onSliderLoad: function () {
                jQuery('#image-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#add-slid").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        var one = $(".range-example-2").asRange({
            range: true,
            limit: true,
            tip: {
                active: 'onMove'
            }
        });
        console.log(one.asRange('set', [30, 60]));
    });
    $(document).ready(function () {
        $(".log-op").click(function () {
            $(".login-drop").fadeToggle();
        });
    });
</script>

<script type="text/javascript">
    jQuery(window).scroll(function () {
        if (jQuery(window).scrollTop() >= 50) {
            if (jQuery('#nav-top').hasClass("navi"))
            {
                //jQuery('#header').removeClass("fixed-theme");
            } else
            {
                jQuery('#nav-top').addClass("navi");
            }
        } else {
            jQuery('#nav-top').removeClass("navi");
        }
    });
</script>
<script>
    $(function () {

        $("#loginforms").validate({
            rules: {
                email: {
                    required: true,
                    email: true,

                },
                password: {required: true}
            },
            messages: {

                email: {
                    required: "Email is required",
                    email: "Please enter valid email",
                    // remote: "The Email Already Exists." 		  
                },
                password: {required: "Password is required"}
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
        $("#loginform").validate({
            rules: {
                email: {
                    required: true,
                    email: true,

                },
                password: {required: true}
            },
            messages: {

                email: {
                    required: "Email is required",
                    email: "Please enter valid email",
                    // remote: "The Email Already Exists." 		  
                },
                password: {required: "Password is required"}
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#editprofiles").validate({
            debug: true,
            rules: {
                user_firstname: {required: true},
                user_lastname: {required: true},
                user_address: {required: true},
                website: {required: true,
                            url: true
                    },
                user_phone: {required: true},
                user_zipcode: {required: true},
                about: {required: true},
            },
            messages: {

                user_firstname: {required: "First name is required"},
                user_lastname: {required: "Last name is required"},
                user_address: {required: "Address is required"},
                website: {required: "Website url is required"},
                user_phone: {required: "Phone number is required"},
                user_zipcode: {required: "Zip code is required"},
                about: {required: "About is required"},
            },
            submitHandler: function (form) {
                var formDatas = new FormData($("#editprofiles")[0]);
//                 
//                    var allowedFiles = [".jpeg", ".jpg", ".png", ".gif"];
//                    var fileUpload = $("#pucture");
//                   var lblError = $("#lblError");
//                    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
//                    if (fileUpload.val() != '') {
//                        if (!regex.test(fileUpload.val().toLowerCase())) {
//                            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
//                          return false;
//                       }
//                    }
//                  lblError.html('');
                $.ajax({
                    url: "<?php echo base_url('Dashboards/editprofile'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    //async: false,
                    //cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#myModal').modal('toggle');
                         setTimeout(function() {
                         window.location = "<?php echo base_url('dashboard'); ?>";
                        }, 2000);
                       

                    }
                });
            }
        });



        $("#email_subscriptionss").validate({
            debug: true,
            rules: {
                news_email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                news_email: {required: "Email is required"},
            },
            submitHandler: function () {
                var formDatas = new FormData($("#email_subscriptionss")[0]);
                $.ajax({
                    url: "<?php echo base_url('Pages/news_subscription'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                      $('#emailnews').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        $('#news_email').val('');
                        $('#success_msg').html(data);
                        setTimeout(function() {
                        $('.removess').fadeOut('fast');
                        }, 1000);
                        $('#emailnews').removeAttr("disabled");
                    }
                });
            }
        });
    });
</script>
<script>
    var autocomplete;

    function initAutocomplete() {

        var input = document.getElementById('autocomplete');
        var options = {
            types: ['address']
        };
        autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            var place = autocomplete.getPlace();

            document.getElementById('main_lat').value = place.geometry.location.lat();
            document.getElementById('main_lng').value = place.geometry.location.lng();

        });

    }

    new Cleave('#user_zipcode', {
        numericOnly: true,
        blocks: [6],
        delimiterLazyShow: true

    });

    new Cleave('#user_phone', {
        numericOnly: true,
        delimiter: '-',
        blocks: [12],
        delimiterLazyShow: true
    });

</script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ&libraries=places&callback=initAutocomplete"></script>
</body>
</html>
