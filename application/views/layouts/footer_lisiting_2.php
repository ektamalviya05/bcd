<footer class="main_footer">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <div class="ft_logo"> <a href="home"><img src="assets_new/images/logo.png" /> </a></div>
                </div>
                <div class="col-md-6 col-sm-6">                    
                    <section class="email_sub">                        
                        <div class="email_su_search">
                            <div class="search-container">
                                <form action="email_subscription" method="post" name="email_subscription" id="email_subscriptionss" enctype="multipart/form-data">
                                    <span class="email_su_icon"> <i class="fa fa-envelope-o" aria-hidden="true"></i> </span>
                                    <input type="text" placeholder="<?php echo $top_content[0]['subscribe']; ?>" name="news_email" id="news_email">                                    
                                    <div id="success_msg"></div>
                                    <button type="submit" id="emailnews"><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>
                    </section>                    
                </div>
            </div>
        </div>
        <div class="row">
            <div class="f_middle wow fadeInDown animated">
                <div class="">
                    <div class="working_hours">
                        <div class="row">
                            <div class="col-sm-9">
                                <ul>
                                    <li><a href="home"><?php echo $top_content[0]['home']; ?></a></li>
                                    <li><a href="<?php echo base_url();?>categories"><?php echo $top_content[0]['Category']; ?></a></li>
                                    <li><a href="<?php echo base_url();?>offers"><?php echo $top_content[0]['Offer']; ?></a></li>
                                    <li><a href="<?php echo base_url();?>listings"><?php echo $top_content[0]['listings']; ?></a></li>
                                    <!-- <li><a href="javascript:;">More</a></li> -->
                                    <li><a href="<?php echo base_url();?>about-us"><?php echo $top_content[0]['about_us']; ?> </a></li>

                                    <li><a href="<?php echo base_url();?>term-condition"><?php echo $top_content[0]['terms_and_conditions']; ?> </a></li>
                                    <li><a href="<?php echo base_url();?>privacy-policy"><?php echo $top_content[0]['privacy_policy']; ?> </a></li>

                                    <li><a href="<?php echo base_url();?>contact-us"><?php echo $top_content[0]['contact_us']; ?></a></li>
                                </ul>
                            </div>

                            <div class="col-sm-3">
                                <div class="f_heading footer-add">
                                    <div class="social-demo">
                                         <?php
                                             $facebook = $this->db->get_where("otd_social_links", array("sc_id"=>1))->row_array();
                                             $twitter = $this->db->get_where("otd_social_links", array("sc_id"=>2))->row_array();
                                             $instagram = $this->db->get_where("otd_social_links", array("sc_id"=>3))->row_array();
                                             $google_plus = $this->db->get_where("otd_social_links", array("sc_id"=>4))->row_array();
                                             $pinterest = $this->db->get_where("otd_social_links", array("sc_id"=>5))->row_array();
                                          ?>

                                        <ul>
                                            <li><a href="<?php echo $twitter['sc_links']; ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                            <li><a href="<?php echo $pinterest['sc_links']; ?>" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
                                            <li><a href="<?php echo $google_plus['sc_links']; ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                            <li><a href="<?php echo $facebook['sc_links']; ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                            <li><a href="<?php echo $instagram['sc_links']; ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="f_follow text-center"><?php echo $top_content[0]['copy_right']; ?></div>
                </div>
                <div class="col-sm-6">
                    <div class="copy_left">
                        <ul>
                            <li><a href="javascript:;"><img src="assets_new/images/img_app.png"/></a></li>
                            <li><a href="javascript:;"><img src="assets_new/images/img_app.png"/></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript" src="assets_new/js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets_new/js/bootstrap.min.js"></script>
<script type="text/javascript" src="assets_new/js/owl.carousel.min.js"></script>
<script type="text/javascript" src="assets_new/js/wow.min.js"></script>
<script src="assets_new/js/lightslider.js"></script> 
<script src="assets_new/js/jquery-asRange.js"></script>
<script src="assets_new/js/cleave.js"></script>
<script src="assets_new/js/cleave.min.js"></script>
<script src="assets_new/js/cleave-phone.i18n.js"></script>
<script src="assets_new/js/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/jquery.validate.js"></script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.13.1/additional-methods.js"></script>
<script src="assets_new/js/pages/addlisting.js"></script>
<script src="assets/js/custom.js"></script>
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>
<script>
    wow = new WOW(
            {
                animateClass: 'animated',
                offset: 100
            }
    );
    wow.init();
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".main-grid").click(function(){
            $(this).addClass("act");
            $(".listing-sub").addClass("grid-system");
            $(".main-list").removeClass("act");
        });
    });
    $(document).ready(function(){
        $(".main-list").click(function(){
            $(this).addClass("act");
            $(".listing-sub").removeClass("grid-system");
            $(".main-grid").removeClass("act");
        });
    });
</script>
<script>
    $("#cat_id").change(function() {
        var cat_id = $(this).val();
        //alert(cat_id);
        //alert(siteurl+'Listing/category');
        if(cat_id) {
              $.ajax({
                url: "<?php echo base_url(); ?>Dashboards/category",
                data:{cat_id:cat_id},
                type:'POST',
                success:function(response) {
                        console.log(response);
                        var resp = $.trim(response);
                        $("#sub_cat_id").html(resp);
                },

                error :function(response) {
                        console.log(response);
                        
                }
              });
            } 
       else
       {
          $("#sub_cat_id").html("<option value=''>Select Business Category First</option>");
       }
  });

</script>
<script>

    $(".fav").click(function() {
        var b_id = $(this).attr('id');
        //alert(b_id);
        if(b_id) {
              $.ajax({
                url: "<?php echo base_url(); ?>Dashboards/addfav",
                data:{b_id:b_id},
                type:'POST',
                success:function(response) {
                        console.log(response);
                        $(".favlist").removeClass("fa-heart-o");
                        $(".enquiryone").removeClass("fav");
                         $(".favlist").addClass("fa-heart");
                        
                },

                error :function(response) {
                        console.log(response);
                        
                }
              });
            } 

    });
    
</script>
<script>
    $(document).ready(function(){
    $(".deleteuserlisting").click(function(){
        if(confirm("Are you sure?")){
         var id =  $(this).attr('id');
         //alert(id);

         $.ajax({
                url:"<?php echo base_url(); ?>Dashboards/deletelisting",
                data:{"id":id, "mode":3},
                type:"POST",
                dataType:"html",
                async: false,
                success: function(data){
                    //console.log(data);
                    location.reload();
                   
                },
                error: function(data){
                    console.log(data);
                   
                }
            });
        }
    });
    });
    
</script>
<script>
    $(document).ready(function() {
    $("input[name$='cars']").click(function() {
        var test = $(this).val();
        var checkval = $(this).val();

            if(checkval ==3)
          {
              $("#business_address").val('');
              $("#business_country").val('');
              $("#business_city").val('');
          }
          else
          {
              $("#location").val('');
          }

        $("div.desc").hide();
        $("#Cars" + test).show();
    });
 
     $("#business_country").change(function() {

        var level = $(this).val();
        //alert(level);
         if(level){
            $.ajax ({
                type: 'POST',
                url: '<?php echo base_url(); ?>Users/viewcity',
                data: { hps_level: '' + level + '' },
                success : function(htmlresponse) {
                    $('#business_city').html(htmlresponse);
                    //console.log(htmlresponse);
                }
            });
        }
     });

});
</script>
<script>
    jQuery(document).ready(function () {
        jQuery("#content-slider").lightSlider({
            loop: true,
            keyPress: true
        });
        jQuery('#image-gallery').lightSlider({
            gallery: true,
            item: 1,
            thumbItem: 4,
            slideMargin: 0,
            speed: 500,
            auto: true,
            loop: true,
            responsive: [
                {
                    breakpoint: 800,
                    settings: {// settings for width 480px to 800px
                        thumbItem: 4
                    }
                },
                {
                    breakpoint: 480,
                    settings: {// settings for width 0px to 480px
                        thumbItem: 2
                    }
                }
            ],
            onSliderLoad: function () {
                jQuery('#image-gallery').removeClass('cS-hidden');
            }
        });
    });
</script>
<script>
    $(document).ready(function () {
        $("#add-slid").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true
        });
    });
</script>
<script>
    $(document).ready(function () {
        var one = $(".range-example-2").asRange({
            range: true,
            limit: true,
            tip: {
                active: 'onMove'
            }
        });
        //console.log(one.asRange('set', [30, 60]));
    });
    $(document).ready(function () {
        $(".log-op").click(function () {
            $(".login-drop").fadeToggle();
        });
    });
</script>

<script type="text/javascript">
    jQuery(window).scroll(function () {
        if (jQuery(window).scrollTop() >= 50) {
            if (jQuery('#nav-top').hasClass("navi"))
            {
                //jQuery('#header').removeClass("fixed-theme");
            } else
            {
                jQuery('#nav-top').addClass("navi");
                jQuery('body').addClass("bfix");
            }
        } else {
            jQuery('#nav-top').removeClass("navi");
            jQuery('body').removeClass("bfix");
        }
    });
</script>
<script>
    $(function () {

        $("#loginforms").validate({
            rules: {
                email: {
                    required: true,
                    email: true,

                },
                password: {required: true}
            },
            messages: {

                email: {
                    required: "Email is required",
                    email: "Please enter valid email",
                    // remote: "The Email Already Exists." 		  
                },
                password: {required: "Password is required"}
            },
            submitHandler: function (form) {
                form.submit();
            }
        });
        $("#loginform").validate({
            rules: {
                email: {
                    required: true,
                    email: true,

                },
                password: {required: true}
            },
            messages: {

                email: {
                    required: "Email is required",
                    email: "Please enter valid email",
                    // remote: "The Email Already Exists." 		  
                },
                password: {required: "Password is required"}
            },
            submitHandler: function (form) {
                form.submit();
            }
        });

        $("#editprofiles").validate({
            debug: true,
            rules: {
                user_firstname: {required: true},
                user_lastname: {required: true},
                user_address: {required: true},
                website: {required: true,
                            url: true
                    },
                user_phone: {required: true,
                            number: true,
                            minlength:8,
                            maxlength:8
                            },
                user_city:{required: true},
                user_country : {required: true},
                //user_zipcode: {required: true},
                about: {required: true},
            },
            messages: {

                user_firstname: {required: "First name is required"},
                user_lastname: {required: "Last name is required"},
                user_address: {required: "Address is required"},
                website: {required: "Website url is required"},
                user_phone: {required: "Phone number is required"},
                //user_zipcode: {required: "Zip code is required"},
                about: {required: "About is required"},
            },
            submitHandler: function (form) {
                var formDatas = new FormData($("#editprofiles")[0]);
//                 
//                    var allowedFiles = [".jpeg", ".jpg", ".png", ".gif"];
//                    var fileUpload = $("#pucture");
//                   var lblError = $("#lblError");
//                    var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
//                    if (fileUpload.val() != '') {
//                        if (!regex.test(fileUpload.val().toLowerCase())) {
//                            lblError.html("Please upload files having extensions: <b>" + allowedFiles.join(', ') + "</b> only.");
//                          return false;
//                       }
//                    }
//                  lblError.html('');
                $.ajax({
                    url: "<?php echo base_url('Dashboards/editprofile'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    //async: false,
                    //cache: false,
                    contentType: false,
                    processData: false,
                    success: function (data) {
                        $('#myModal').modal('toggle');
                        setTimeout(function() {
                         window.location = "<?php echo base_url('dashboard'); ?>";
                        }, 3000);
                       

                    }
                });
            }
        });



        $("#email_subscriptionss").validate({
            debug: true,
            rules: {
                news_email: {
                    required: true,
                    email: true,
                },
            },
            messages: {
                news_email: {required: "Email is required"},
            },
            submitHandler: function () {
                var formDatas = new FormData($("#email_subscriptionss")[0]);
                $.ajax({
                    url: "<?php echo base_url('Pages/news_subscription'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                      $('#emailnews').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        $('#news_email').val('');
                        $('#success_msg').html(data);
                        setTimeout(function() {
                        $('.removess').fadeOut('fast');
                        }, 1000);
                        $('#emailnews').removeAttr("disabled");
                    }
                });
            }
        });
    });
</script>
<script>
    $('.detail_enquiry').click(function(){
        
        var id = $(this).attr('id');
        $.ajax({
                url: "<?php echo base_url(); ?>Dashboards/enquirydetail", // Controller URL
                type: 'POST',
                data: {id:id},
                success: function (data) {
                    //console.log(data);
                    $('#enquiry_show').html(data);  
                     $('#popUpshow').modal("show")
                },
                error: function (error) {
                    console.log(error);
                }
            });

        
    });
    
  </script>

  <script>
    $('.edituserlisting').click(function(){
        
        var id = $(this).attr('id');
        //alert(id);
        $.ajax({
                url: "<?php echo base_url(); ?>Dashboards/listingdetail", // Controller URL
                type: 'POST',
                data: {id:id},
                success: function (data) {
                    console.log(data);
                    $('#listingdetail_show').html(data);  
                     $('#popUplisting').modal("show")
                },
                error: function (error) {
                    console.log(error);
                }
            });

        
    });
    
  </script>


<script>
    var autocomplete;

    //var autocomplete2;

    function initAutocomplete() {

        var input = document.getElementById('autocomplete');


        var options = {
            //types: ['geocode']

            types: ['(cities)'],
             componentRestrictions: {country: "KW"}

             
        };




        autocomplete = new google.maps.places.Autocomplete(input, options);
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            var place = autocomplete.getPlace();

            document.getElementById('main_lat').value = place.geometry.location.lat();
            document.getElementById('main_lng').value = place.geometry.location.lng();

        });

    }
     

    new Cleave('#user_zipcode', {
        numericOnly: true,
        blocks: [6],
        delimiterLazyShow: true

    });

    new Cleave('#user_phone', {
        numericOnly: true,
        delimiter: '-',
        blocks: [12],
        delimiterLazyShow: true
    });

</script>

<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ&libraries=places&callback=initAutocomplete"></script>

<!-- <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCrVGd7PYWdG6NMU-zXRvowyPY1qXr2mjQ&libraries=places&callback=initAutocomplete2"></script> -->



<!-- Google map auto fill code start --> 
    <script type="text/javascript">
         $(function(){   
            
            //var lat = -33.8688,
               // lng = 151.2195,
               $.get("http://freegeoip.net/json/", function (data) {
               var lat = data.latitude;
               var lng = data.longitude;
         
                latlng = new google.maps.LatLng(lat, lng),
                image = 'https://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'; 
                 
            var mapOptions = {           
                    center: new google.maps.LatLng(lat, lng),           
                    zoom: 5,           
                    mapTypeId: google.maps.MapTypeId.ROADMAP         
                },
                map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions),
                marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: image
                 });
             
            var input = document.getElementById('location');         
            var autocomplete = new google.maps.places.Autocomplete(input, {
                types: ["geocode"]
            }); 

             autocomplete.bindTo('bounds', map); 
            var infowindow = new google.maps.InfoWindow(); 
         
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                infowindow.close();
                var place = autocomplete.getPlace();
                if (place.geometry.viewport) {
                    map.fitBounds(place.geometry.viewport);
                } else {
                    map.setCenter(place.geometry.location);
                    map.setZoom(5);
                }
                
                moveMarker(place.name, place.geometry.location);
            });



            }, "jsonp"); 


            function moveMarker(placeName, latlng){
                marker.setIcon(image);
                marker.setPosition(latlng);
                infowindow.setContent(placeName);
                infowindow.open(map, marker);
             }


             }); 

             

            
            $("input").focusin(function () {
                $(document).keypress(function (e) {
                    if (e.which == 13) {
                        infowindow.close();
                        var firstResult = $(".pac-container .pac-item:first").text();
                        
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({"address":firstResult }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                var lat = results[0].geometry.location.lat(),
                                    lng = results[0].geometry.location.lng(),
                                    placeName = results[0].address_components[0].long_name,
                                    latlng = new google.maps.LatLng(lat, lng);
                                
                                moveMarker(placeName, latlng);
                                $("input").val(firstResult);
                            }
                        });
                    }
                });
            });
             
             
        /*});*/


    </script>
    <!-- Google map code close -->

    <script type="text/javascript">
        /*function submitReview() 
        {
            var formDatas = new FormData($("#ratingForm")[0]);
            $.ajax({
                url: "<php echo base_url('submit_review'); ?>", // Controller URL
                type: 'POST',
                data: formDatas,
                contentType: false,
                processData: false,
                beforeSend: function () {
                  $('#btnReview').attr("disabled", "disabled");
                },
                success: function (data) {
                    $('#visitor_name').val('');
                    $('#visitor_email').val('');
                    $('#description').val('');
                    $('#success_msg').html(data);
                    $('#btnReview').removeAttr("disabled");
                },
                error: function (error) {
                    console.log(error);
                }
            });
            return false;
        }*/

        $("#ratingForm").validate({
            debug: true,
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                visitor_name: {
                    required: true
                },
                description: {
                    required: true
                }
            },
            messages: {
                email: {required: "Email is required"},
            },
            submitHandler: function () {
                var formDatas = new FormData($("#ratingForm")[0]);
                $.ajax({
                    url: "<?php echo base_url('submit_review'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                      $('#btnReview').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        console.log(data);
                        $('#visitor_name').val('');
                        $('#visitor_email').val('');
                        $('#description').val('');
                        $('#success_msg').html(data);
                        /*setTimeout(function() {
                         location.reload();
                        }, 2000);*/
                        $('#btnReview').removeAttr("disabled");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>


    <script type="text/javascript">
        $("#contactform").validate({
            debug: true,
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                name: {
                    required: true
                },
                phone: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                email: {required: "Email is required"},
            },
            submitHandler: function () {
                var formDatas = new FormData($("#contactform")[0]);
                $.ajax({
                    url: "<?php echo base_url('send_contactInfo'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                      $('#btnContactSubmit').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        $('#email').val('');
                        $('#name').val('');
                        $('#phone').val('');
                        $('#comment').val('');
                        $('#success_msg').html(data);
                        setTimeout(function() {
                         location.reload();
                        }, 2000);
                        $('#btnRbtnContactSubmiteview').removeAttr("disabled");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
    </script>

    <script>
 ///// Contact form JS
    $("#contact").validate({
            debug: true,
            rules: {
                email: {
                    required: true,
                    email: true,
                },
                name: {
                    required: true
                },
                phone: {
                    required: true
                },
                comment: {
                    required: true
                }
            },
            messages: {
                email: {required: "Email is required"},
            },
            submitHandler: function () {
                var formDatas = new FormData($("#contact")[0]);
                $.ajax({
                    url: "<?php echo base_url(); ?>Pages/contactformsubmitbyus", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    dataType : 'html',
                    beforeSend: function () {
                      $('#contactSubmit').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        $('#email').val('');
                        $('#name').val('');
                        $('#phone').val('');
                        $('#comment').val('');
                        $('#success_msg').html(data);
                        //$('#myModal').modal('toggle');
                        //setTimeout(function() {
                         //location.reload();
                        //}, 2000);
                        console.log(data);
                        $('#btnRbtnContactSubmiteview').removeAttr("disabled");
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
  
</script>

<!-- <script>
function submitContactForm(){
    var reg = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
    var name = $('#inputName').val();
    var email = $('#inputEmail').val();
    var message = $('#inputMessage').val();
    var bid =$('#bid').val();

    if(name.trim() == '' ){
        //alert('Please enter your name.');
        $('#nameerror').text('Please enter your name.');
        $('#inputName').focus();
        return false;
    }else if(email.trim() == '' ){
        //alert('Please enter your email.');
        $('#emailerror').text('Please enter your email.');
        $('#inputEmail').focus();
        return false;
    }else if(email.trim() != '' && !reg.test(email)){
        //alert('Please enter valid email.');
         $('#emailerror').text('Please enter valid email.');
        $('#inputEmail').focus();
        return false;
    }else if(message.trim() == '' ){
        //alert('Please enter your message.');
        $('#messageerror').text('Please enter your message.');
        $('#inputMessage').focus();
        return false;
    }else{
        $.ajax({
            type:'POST',
            url:'<?php //echo base_url(); ?>Pages/euaformsubmitbyus',
            data:'contactFrmSubmit=1&name='+name+'&email='+email+'&message='+message+'&bid='+bid,
            dataType: 'html',
            beforeSend: function () {
                $('.submitBtn').attr("disabled","disabled");
                $('.modal-body').css('opacity', '.5');
            },
            success:function(msg){
                console.log(msg)
                if(msg == 'ok'){
                    $('#inputName').val('');
                    $('#inputEmail').val('');
                    $('#inputMessage').val('');
                    $('.statusMsg').html('<span style="color:green;">Thanks for contacting us, we will get back to you soon.</p>');
                    setTimeout(function(){// wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                     }, 2000);
                }else{
                    $('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                }
                $('.submitBtn').removeAttr("disabled");
                $('.modal-body').css('opacity', '');
            }
        });
    }
}
</script> -->

<script>
 ///// Enquiry form JS

    $("#enquiryForm").validate({
            debug: true,
            rules: {
                inputEmail: {
                    required: true,
                    email: true,
                },
                inputName: {
                    required: true
                },
                
                inputMessage: {
                    required: true
                }
            },
            messages: {
                inputEmail: {required: "Email is required"},
            },
            submitHandler: function () {
                var formDatas = new FormData($("#enquiryForm")[0]);
                $.ajax({
                    url: "<?php echo base_url(); ?>Pages/euaformsubmitbyus", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    dataType : 'html',
                    beforeSend: function () {
                        $('.submitBtn').attr("disabled","disabled");
                        $('.modal-body').css('opacity', '.5');
                    },
                    success: function (msg) {
                        // console.log(msg)
                            if(msg == 'ok'){
                                $('#inputName').val('');
                                $('#inputEmail').val('');
                                $('#inputMessage').val('');
                                $('.statusMsg').html('<span style="color:green;">Thanks for contacting us, we will get back to you soon.</p>');
                                setTimeout(function(){// wait for 5 secs(2)
                                    location.reload(); // then reload the page.(3)
                                 }, 2000);
                            }else{
                                $('.statusMsg').html('<span style="color:red;">Some problem occurred, please try again.</span>');
                            }
                            $('.submitBtn').removeAttr("disabled");
                            $('.modal-body').css('opacity', '');
                                
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });
   
  
</script>


    
    <script type="text/javascript">

        $("#searching_form").validate({
            rules: {
                address: {
                    required: true,
                    //email: true,

                },
                //category_id: {
                    //required: true,
                    //email: true,

               // },
                //password: {required: true}
            },
            messages: {

                address: {
                    required: "Address is required",
                    //email: "Please enter valid email",
                    // remote: "The Email Already Exists."        
                },
                //category_id: {
                    //required: "Category is required",
                    //email: "Please enter valid email",
                    // remote: "The Email Already Exists."        
                //},
                //password: {required: "Password is required"}
            },
            submitHandler: function (form) {
                form.submit();
            }
        });


        $("#paymentProcessForm").validate({
            debug: true,
            rules: {
                creditCardType: {
                    required: true
                },
                creditCardNumber: {
                    required: true
                },
                expDateMonth: {
                    required: true
                },
                expDateYear: {
                    required: true
                },
                cvv2Number: {
                    required: true
                },
                first_name: {
                    required: true
                },
                last_name: {
                    required: true
                },
                city: {
                    required: true
                },
                state: {
                    required: true
                },
                street: {
                    required: true
                },
                zip: {
                    required: true
                }
            },
            /*messages: {
                //name: {required: "Email is required"},
            },*/
            submitHandler: function () {
                var formDatas = new FormData($("#paymentProcessForm")[0]);
                $.ajax({
                    url: "<?php echo base_url('buy_package'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                      $('#btnPurchase').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        //alert(data);
                        console.log(data);
                        /*
                        $('#visitor_name').val('');
                        $('#visitor_email').val('');
                        $('#description').val('');
                        $('#success_msg').html(data);
                        //setTimeout(function() {location.reload();}, 2000);
                        $('#btnReview').removeAttr("disabled");
                        */
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            }
        });

        /*function purchase_package()
        {
            var formDatas = new FormData($("#paymentProcessForm")[0]);
            $.ajax({
                    url: "<php echo base_url('buy_package'); ?>", // Controller URL
                    type: 'POST',
                    data: formDatas,
                    //contentType: false,
                    //processData: false,
                    beforeSend: function () {
                      $('#btnReview').attr("disabled", "disabled");
                    },
                    success: function (data) {
                        //alert(data);
                        console.log(data);
                        
                        //$('#visitor_name').val('');
                        //$('#visitor_email').val('');
                        //$('#description').val('');
                        //$('#success_msg').html(data);
                        //setTimeout(function() {location.reload();}, 2000);
                        //$('#btnReview').removeAttr("disabled");
                        
                    },
                    error: function (error) {
                        console.log(error);
                    }
                });
            return false;
        }*/

        function getListingData() {
            var formDatas = new FormData($("#searching_form")[0]);
            $.ajax({
                url: "<?php echo base_url('listing_data'); ?>", // Controller URL
                type: 'POST',
                data: formDatas,
                contentType: false,
                processData: false,
                beforeSend: function () {
                  $('#btnListingData').attr("disabled", "disabled");
                },
                success: function (data) {
                    
                    console.log(data);
                    
                    
                    // $('#visitor_name').val('');
                    // $('#visitor_email').val('');
                    // $('#description').val('');
                    
                    $('#listing_block').html(data);
                    $('#btnListingData').removeAttr("disabled");

                    //$(".map-main").load();

                    
                    
                },
                error: function (error) {
                    console.log(error);
                }
            });
            return false;
        }

    </script>

    <script type="text/javascript">
        function getListing(page) {
            alert(page);
            /*
            $.ajax({
                url: "<php echo base_url('Dashboards/editprofile'); ?>", // Controller URL
                type: 'POST',
                data: formDatas,
                //async: false,
                //cache: false,
                contentType: false,
                processData: false,
                success: function (data) {
                   
                }
            });
            */
        }
    </script>

    <!-- Get address using ip address start -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
    <script type="text/javascript">

          $.get("http://freegeoip.net/json/", function (data) 
          {
        var country = data.country_name;
         var ip = data.ip;
       
        //  var loc_adr = "<?php echo $this->session->userdata("search_location");  ?>";
        //  if(loc_adr == '')
        //  {
        //  $("#autocomplete").val(data.city + ", " + data.region_name + ", " + country);
        //  }
        //  else
        //  {
        //     $("#autocomplete").val(loc_adr);
        //  }
        }, "jsonp");
        // $.get("https://ipinfo.io/json", function (response) {
            
        //     $("#ip").html("IP: " + response.ip);
        //     $("#address").html("Location: " + response.city + ", " + response.region);
        //     $("#details").html(JSON.stringify(response, null, 4));

        //     $("#autocomplete").val(response.city + ", " + response.region);

        // }, "jsonp");
    </script>
    <!-- Get address using ip address close -->

    <!-- auto fill address code start -->
    <script>
      // This example displays an address form, using the autocomplete feature
      // of the Google Places API to help users fill in the information.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
    <!--<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places&callback=initAutocomplete"
        async defer></script>-->
    <!-- auto fill address code close -->   
    <script type="text/javascript">
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        var labelIndex = 0;
        
         var loc_adr = "<?php echo $this->session->userdata("search_location");  ?>";
         if(loc_adr == '')
         {
         $("#autocomplete").val(data.city + ", " + data.region_name + ", " + country);
         }
         else
         {
            $("#autocomplete").val(loc_adr);
         }


        var locations = [
            <?php
            $ar = getIpLatLng();
            //echo "['User', ".@$ar['latitude'].", ".@$ar['longitude'].", 1],";
            $i = 2;
            if ($featureCategoryListings) {
                foreach ($featureCategoryListings as $featureListing) {
                    if(!is_null($featureListing['latitude']) && !is_null($featureListing['longitude']))
                    {
                        //echo "['".$featureListing['business_title']."', ".$featureListing['lat'].", ".$featureListing['longitude'].", ".$i."],";
                        echo '["'.$featureListing['business_title'].'", '.$featureListing['latitude'].', '.$featureListing['longitude'].', '.$i.'],';
                        if(is_null($lat) && is_null($long))
                        {
                            $lat = $featureListing['latitude'];
                            $long = $featureListing['longitude'];
                        }
                        $i++;
                    }
                }
            }

            if ($categoryListing) {
                foreach ($categoryListing as $listing) {
                    if(!is_null($listing['lat']) && !is_null($listing['longitude']))
                    {
                        //echo "['".$listing['business_title']."', ".$listing['lat'].", ".$listing['longitude'].", ".$i."],";
                        echo '["'.$listing['business_title'].'", '.$listing['latitude'].', '.$listing['longitude'].', '.$i.'],';
                        if(is_null($lat) && is_null($long))
                        {
                            $lat = $listing['latitude'];
                            $long = $listing['longitude'];
                        }
                        $i++;
                    }
                }
            }
            
            ?>
          /*['Bondi Beach', -33.890542, 151.274856, 4],
          ['Coogee Beach', -33.923036, 151.259052, 5],
          ['Cronulla Beach', -34.028249, 151.157507, 3],
          ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
          ['Maroubra Beach', -33.950198, 151.259302, 1]*/
        ];

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 5,
          //center: new google.maps.LatLng(-33.92, 151.25),

            <?php
            $lat = '';
            $long = '';
            if ($featureCategoryListings) {
                foreach ($featureCategoryListings as $featureListing) {
                    if(!is_null($featureListing['latitude']) && !is_null($featureListing['longitude']))
                    {
                        if(empty($lat) && empty($long))
                        {
                            $lat = $featureListing['latitude'];
                            $long = $featureListing['longitude'];
                        }
                    }
                }
            }

            if ($categoryListing) {
                    foreach ($categoryListing as $listing) {
                        if(!is_null($listing['latitude']) && !is_null($listing['longitude']))
                        {
                            if(empty($lat) && empty($long))
                            {
                                $lat = $listing['latitude'];
                                $long = $listing['longitude'];
                            }
                        }
                    }
                
            }
            ?>

          center: new google.maps.LatLng("<?php echo $ar['latitude']; ?>", "<?php echo $ar['longitude']; ?>"),

          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
     
    var lt = <?php echo  $this->session->userdata("search_lat") ?>;
    var lg = <?php echo  $this->session->userdata("search_long") ?>;
     map.setCenter(lt,lg);
     map.panTo(new google.maps.LatLng(lt,lg));
       //
        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {  
            if(i === 0)
            {
                var lab = 1;
                //var lab = 20;
            }
            else
            {
                var lab = 1;
            }
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            //label: labels[labelIndex++ % labels.length],
            label: labels[lab % labels.length],
            map: map
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(locations[i][0]);
              infowindow.open(map, marker);
              map.setCenter(locations[i][1], locations[i][2]);
              map.panTo(new google.maps.LatLng(locations[i][1], locations[i][2]));
            
            }
          })(marker, i));
        }
  </script>
  <script>
             $("#editpasswords").validate({
            rules: {
            user_password: {required: true},
            old_password : {required: true},
            confirm_password:{
                            required: true,
                            equalTo : "#user_password"
                          },
            },
            messages: {
            user_password: {required: "New Password is required"},
            old_password: {required: "Old Password Must Enter"},
            confirm_password: {required: "Enter Confirm Password Same as Password"},
             },
            submitHandler: function (form) 
            {
                var formDatas = new FormData($("#editpasswords")[0]);
                    $.ajax({
                        url: "<?php echo base_url('Dashboards/editpassword'); ?>", // Controller URL
                        type: 'POST',
                        data: formDatas,
                        //async: false,
                        //cache: false,
                        contentType: false,
                        processData: false,
                        dataType:"json",
                        success: function (data) {
                             console.log(data);
                            if(data.typefive === 1) {
                            $("#erorrpass").html(data.msg);
                            }

                            if(data.status === 200) {
                            $("#erorrpass").html(data.msg);
                            setTimeout(function() {
                                    window.location = "<?php echo base_url('dashboard'); ?>";
                            }, 5000);
                            }

                    

                        }
                    });
            }
       });
  </script>

  

</body>
</html>
