<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Pradeep Verma">
    <title><?php if(!empty($details) && !empty($details[0]['meta_title'])) echo $details[0]['meta_title']; else echo "Otourdemoi"; ?></title>
    <?php
    //echo "His is my";
//print_r($details[0]['meta_title']);
    ?>
    <meta name="description" content="<?php if(!empty($details) && !empty($details[0]['meta_description'])) echo $details[0]['meta_description'];?>" />
    
    <meta name="keywords" content="<?php if(!empty($details) && !empty($details[0]['meta_keyword'])) echo $details[0]['meta_keyword'];?>">
    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <?php
    if($this->uri->segment(2) == "eventsView"){
      
      if(!empty($evnt)){
        if(!empty($evnt['event_img1']) && file_exists("./././assets/img/events/".$evnt['event_img1'])){
          $picpath = base_url("assets/img/events/").$evnt['event_img1'];
        }else{
          $picpath = $evnt['user_profile_pic'];
        }
      }
        ?>
        <!-- code for facebook share -->
        <meta property="og:title" content="<?php echo ucwords($evnt['event_name']); ?>" />
        <meta property="og:type" content="Event" />
        <meta property="og:image" content="<?php echo $picpath;?>" />
        <meta property="og:url" content="<?php echo base_url(uri_string()); ?>" />
        <meta property="og:description" content="<?php echo substr($evnt['event_description'], 0, 100); ?>" />
        <meta property="og:image:width"  />
        <meta property="og:image:width" content="400" />
        <meta property="og:image:height" content="300" />

        <!-- code for google share -->
          <meta property="og:title" content="<?php echo ucwords($evnt['event_name']); ?>" />
          <meta property="og:url" content="<?php echo base_url(uri_string()); ?>" />
          <meta property="og:image" content="<?php echo $picpath;?>" />
          <meta property="og:description" content="<?php echo substr($evnt['event_description'], 0, 100); ?>" />

        <!-- code for twitter share -->
          <meta name="twitter:card" content="summary" />
          <meta property="og:type" content="Event" />
          <meta name="twitter:title" content="<?php echo ucwords($evnt['event_name']); ?>" />
          <meta property="og:url" content="<?php echo base_url(uri_string()); ?>" />
          <meta name="twitter:description" content="<?php echo substr($evnt['event_description'], 0, 100); ?>" />
          <meta name="twitter:image" content="<?php echo $picpath;?>" />
        <?php
    }
    ?>
   <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <!-- <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"> -->
    <script src="https://use.fontawesome.com/045adc6a37.js"></script>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   <!--
    <link href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/creative.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/animate.min.css">    
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/multiselect/bootstrap-multiselect.css"); ?>" /> -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/multiselect/bootstrap-select.css"); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/master.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.theme.css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/jquery.flexdatalist.css");?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.mCustomScrollbar.css'); ?>">
    <!-- Custom Core CSS -->
    <link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
   
    <link href="<?php echo base_url("assets/datetimepicker/css/bootstrap-datetimepicker.min.css"); ?>" rel="stylesheet" media="screen">

<!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" /> -->
  <!-- <?php
    //if($this->uri->segment(2) == "account"){
      ?>
      <link rel="stylesheet" href="<?php //echo base_url("assets/timepicker/include/ui-1.10.0/ui-lightness/jquery-ui-1.10.0.custom.min.css"); ?>" type="text/css" />
      <link rel="stylesheet" href="<?php //echo base_url("assets/timepicker/jquery.ui.timepicker.css?v=0.3.3"); ?>" type="text/css" />      
      <?php
   // }
  ?> -->




<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lightbox.min.css" />
<?php /*?><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><?php */?>
  <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/blog_rating.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
<!-- <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> -->
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="185961027889-o82v14cenj8uvcnptkq2vd4ksim1kkth.apps.googleusercontent.com">

<!-- <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597c1b975d21460011308163&product=inline-share-buttons"></script> -->
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597c1b975d21460011308163&product=inline-share-buttons"></script>
</head>
<body>  
<header id="main_header_id" class="<?php if($this->uri->segment(2) == "businessListing" || $this->uri->segment(2) == "slimpayPaymentConfirmation" || $this->uri->segment(2) == "completePaymentAction" || $this->uri->segment(2) == "purchasedPackage" || $this->uri->segment(1) == "Carting" || $this->uri->segment(2) == "myFormOption" || $this->uri->segment(2) == "userPurchase" || $this->uri->segment(2) == "account" || $this->uri->segment(2) == "user_profile" || $this->uri->segment(2) == "completePayment" || $this->uri->segment(2) == "reviewListing" || $this->uri->segment(2) == "purchaseReceipt" || $this->uri->segment(2) == "myEvents" || $this->uri->segment(2) == "eventsView" || $this->uri->segment(2) == "editEvent" || $this->uri->segment(2) == "goodPlace" || $this->uri->segment(2) == "full_map" ) echo "in-page"; ?>">
  <?php 
  $headercontent = $this->db->get_where("otd_footer_content", array("ft_id"=>5))->row_array();
  ?>
  <div class="header-main">
       <nav class="navbar navbar-default navbar-fixed-top" id="nav-top">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            </div>
           <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right main-nav">
               <?php 
//print_r($user);
if($this->session->userdata('user_id') && $this->session->userdata('user_id') >0)
{
  $userdetails = $this->db->select("user_firstname")->get_where("users", array("user_id"=>$this->session->userdata("user_id")));
  ?>

  <li class="user-intro dropdown">
    <a href="<?php echo base_url("Users/account"); ?>" class="dropdown-toggle" data-toggle="dropdown">Hello <?php echo $userdetails->result()[0]->user_firstname; ?>
    <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo base_url("Users/account/inbox"); ?>">
          <i class="fa fa-envelope"></i>          
          <?php 
            echo $headercontent['label4'];
            $sid= $this->session->userdata('user_id');
            $unreadmsg = $this->db->select("msg_id")                
                ->where("msg_reciever = $sid AND us_notify = 0")
                ->group_by("msg_sender")
                ->get("otd_user_messaging");  
            if($unreadmsg->num_rows()) {
              ?>
              <span class="counter-msg">
                <?php
                  echo $unreadmsg->num_rows();
                ?>
              </span>
              <?php
            }
          ?>
        </a></li>
      <li>
        <a href="<?php echo base_url("Users/account/notifications"); ?>">
          <i class="fa fa-bell" aria-hidden="true"></i>          
          <?php 
            echo $headercontent['label5'];
            $sid= $this->session->userdata('user_id');
            $unreadnoti = $this->db->where("nt_to = $sid AND nt_flag = 0")
                                  ->count_all_results("otd_notifications");  
            if($unreadnoti > 0) {
              ?>
              <span class="counter-not">
                <?php
                  echo $unreadnoti;
                ?>
              </span>
              <?php
            }
          ?>
          <!-- <span class="counter-not">10</span> -->
        </a>
      </li>
      <li><a href="<?php echo base_url("Users/account"); ?>"><i class="fa fa-user" aria-hidden="true"></i><?php echo $headercontent['label6']; ?></a></li>
      <?php  
        $social_login = $this->session->userdata("social_login");
        if(!empty($social_login)){
          if($social_login['is_social'] && $social_login['social_type'] == "facebook"){
          ?>
          <li><a id="logoutbutton" href="javascript:;" onclick="Logout();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
          <?php
          }
          else if($social_login['is_social'] && $social_login['social_type'] == "google"){
            ?>
            <li><a id="logoutbutton" href="javascript:void(0);" onClick="signOut();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
            <?php
          }
        }else{
        ?>
        <li><a id="logoutbutton" href="<?php echo base_url();?>Users/logout"><i class="fa fa-power-off" aria-hidden="true"></i><?php echo $headercontent['label7']; ?></a></li>
        <?php
        }
        ?>
    </ul>
  </li>
<?php  
}
  else{              
      ?>
                    <li><a id="loginmodal" href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#myModal2"><?php echo $headercontent['label1']; ?></a>
                                </li>
                    <li><a id="registrationmodal" href="javascript:;" data-toggle="modal" data-target="#myModal"><?php echo $headercontent['label2']; ?></a>
                    </li>
                    <?php
                  }
              if(!empty($this->session->userdata('user_id')) && $this->session->userdata("user_type") == "Professional"){
              ?>
              <li class="cart-msg"><a href="<?php echo base_url("Carting"); ?>"><i class="fa fa-shopping-cart"></i><span class="cart-it"><?php
                  $this->load->library("cart");              
                  $cartcnt = $this->cart->total_items();
                  if($cartcnt > 0)
                    echo $cartcnt;
                  ?></span></a>
              </li>
              <?php
              }
              ?>

              <li class="good-place dropdown">
                <a href="<?php echo base_url("Pages/goodPlace"); ?>"><?php echo $headercontent['label3']; ?> <i class="fa fa-plus"></i></a>                
              </li>
              
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </div>
</header>     
<?php echo $contents;?>                    
<!-- footer  -->
<?php
$logincontent = $this->db->get_where("otd_footer_content", array("ft_id"=>2))->row_array();
$registercontent = $this->db->get_where("otd_footer_content", array("ft_id"=>3))->row_array();
$forgetcontent = $this->db->get_where("otd_footer_content", array("ft_id"=>4))->row_array();
$rgconfirm = $this->db->get_where("otd_footer_content", array("ft_id"=>21))->row_array();
$prorgconfirm = $this->db->get_where("otd_footer_content", array("ft_id"=>28))->row_array();
$nwsthankyou = $this->db->get_where("otd_footer_content", array("ft_id"=>23))->row_array();
$passthankyou = $this->db->get_where("otd_footer_content", array("ft_id"=>29))->row_array();
$clmbsthankyou = $this->db->get_where("otd_footer_content", array("ft_id"=>24))->row_array();
?>
<!--  End Footer -->
<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title"><?php echo $logincontent['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <h2>User Login</h2>
          <form id="userlogin" action="<?php echo base_url();?>Users/login" method="post">
            <div class="row">
              <div class="social-media-login">
                <a href="javascript:;" class="btn btn-default fb-so" onClick="Login()"><i class="fa fa-facebook"></i><?php echo $logincontent['label2']; ?></a>                
                <div class="g-signin2" data-onsuccess="onSignIn"><?php echo $logincontent['label3']; ?></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="email" class="form-control" name="user_email" placeholder="<?php echo $logincontent['label8']; ?>" value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="password" class="form-control" name="user_password" placeholder="<?php echo $logincontent['label9']; ?>" required>
                  <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="submit" name="loginSubmit" class="btn-primary" value="<?php echo $logincontent['label4']; ?>"/>
                </div>
              </div>
              <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal3"><?php echo $logincontent['label5']; ?></button>
              <button type="button" class="btn btn-default new-acc-m" data-dismiss="modal" data-toggle="modal" data-target="#myModal"><?php echo $logincontent['label6']; ?></button>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">       
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $logincontent['label7']; ?></button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">     
    <!-- Modal content-->    
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->      
        <h4 class="modal-title"><?php echo $registercontent['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="select-acount">
          <div class="select-sub">
            <img src="<?php echo base_url("/assets/img/loginicon.png"); ?>">
            <h2><?php echo $registercontent['label2']; ?></h2>
            <div class="overly-sub-select">
              <a class="btn btn-default" href="<?php echo base_url();?>Users/registration"><?php echo $registercontent['label3']; ?></a>
              <a href="<?php echo base_url("Pages/readMore/personal"); ?>" class="learn-mre"><?php echo $registercontent['label4']; ?></a>
            </div>
          </div>
          <div class="select-sub">
            <img src="<?php echo base_url("assets/img/rightimgdiv.png"); ?>">
            <h2><?php echo $registercontent['label5']; ?></h2>
            <div class="overly-sub-select">
              <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration"><?php echo $registercontent['label6']; ?></a>
              <a href="<?php echo base_url("Pages/readMore/professional"); ?>" class="learn-mre"><?php echo $registercontent['label7']; ?></a>
            </div>
          </div>
        </div>
        <div class="subscription">
          <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="mySubs">
            <div class="newsletter-main-head">
              <div class="news-letter-head">
                <h4><?php echo $registercontent['label8']; ?></h4>
                <p><?php echo $registercontent['label9']; ?></p>            
                <div class="row news-letter-form">
                  <div class="col-md-5">
                    <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" name="user_email" required class="inputbox">
                    <div id="emlermsg"></div>
                  </div>
                  <div class="col-md-5">
                    <input type="text" title="Postal Code" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                  </div>
                  <div class="col-md-2">
                    <input type="submit" name="subscribeSubmit" value="<?php echo $registercontent['label10']; ?>" class="submit-main">
                    <span style="display:none;" class="nwsloading"><i class="fa fa-spinner fa-pulse fa-3x"></i></span>
                  </div>
                </div>
                <div class="row agree-sec">
                  <div class="col-md-12">
                    <input type="checkbox" title="Terms and Conditions" name="terms">
                    <span><?php echo $registercontent['label11']; ?></span>
                  </div>
                  <div class="col-md-12" id="nssubmsg1"></div>
                </div>
              </div>
            </div>
          </form>
        </div>        
        <!-- -->         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $registercontent['label12']; ?></button>
      </div>
    </div>
  </div>
</div>
<!--     Forget Password Model -->
<div class="modal fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">         
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title"><?php echo $forgetcontent['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">         
          <form id="forgetpassform" action="<?php echo base_url();?>Users/forgetpassword" method="post">
            <div class="row">
              <div class="col-sm-8">		            
                <div class="form-group">
                  <input type="text" class="form-control" name="user_email" placeholder="<?php echo $forgetcontent['label4']; ?>" required>
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
                <div class="sho_ferr"></div>
              </div>
              <div class="col-sm-4">
                <div class="form-group sub-m">
                  <input type="submit" name="passwordSubmit" class="btn-primary" value="<?php echo $forgetcontent['label2']; ?>"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $forgetcontent['label3']; ?></button>
      </div>
    </div>
  </div>
</div>

<!-- Modal for registration-->
  <!-- <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title"><?php echo $registercontent['label1']; ?></h4>
          <div class="title-imge">
            <img src="<?php echo base_url();?>assets/img/logo.png">
          </div>
        </div>
        <div class="modal-body">
          <div class="select-acount">
            <div class="select-sub">
              <img src="<?php echo base_url("assets/img/loginicon.png"); ?>">
              <h2><?php echo $registercontent['label2']; ?></h2>
              <div class="overly-sub-select">
                <a class="btn btn-default" href="<?php echo base_url();?>Users/registration"><?php echo $registercontent['label3']; ?></a>
                <a href="#" class="learn-mre"><?php echo $registercontent['label4']; ?></a>
              </div>
            </div>
            <div class="select-sub">
              <img src="<?php echo base_url("assets/img/rightimgdiv.png"); ?>">
              <h2><?php echo $registercontent['label5']; ?></h2>
              <div class="overly-sub-select">
                <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration"><?php echo $registercontent['label6']; ?></a>
                <a href="#" class="learn-mre"><?php echo $registercontent['label7']; ?></a>
              </div>
            </div>
          </div>
          <div class="subscription">
            <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="mySubs">
              <div class="newsletter-main-head">
                <div class="news-letter-head">
                  <h4><?php echo $registercontent['label8']; ?></h4>
                  <p><?php echo $registercontent['label9']; ?></p>
              
                <div class="row news-letter-form">
                  <div class="col-md-5">
                    <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" name="user_email" required class="inputbox">
                  </div>
                  <div class="col-md-5">
                    <input type="text" title="Postal Code" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                  </div>
                  <div class="col-md-2">
                    <input type="submit" name="subscribeSubmit" onClick="" value="<?php echo $registercontent['label10']; ?>" class="submit-main">
                  </div>
                </div>
                <div class="row agree-sec">
                  <div class="col-md-12">
                    <input type="checkbox" required title="Terms and Conditions" name="terms">
                    <span><?php echo $registercontent['label11']; ?></span>
                  </div>
                  <div class="col-md-12" id="nssubmsg"></div>
                </div>
                </div>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $registercontent['label12']; ?></button>
        </div>
      </div>
    </div>
  </div> -->
<!-- Modal for regiatration end-->

<!--   Personal Registration confirmation Model -->
  <div class="modal fade" id="myModalregsucc" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">         
          <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
          <h4 class="modal-title"><?php echo $rgconfirm['label1']; ?></h4>
          <div class="title-imge">
            <img src="<?php echo base_url();?>assets/img/logo.png">
          </div>
        </div>
        <div class="modal-body">
          <div class="modal-login-txt">
           <span>
            <?php echo $rgconfirm['label2']; ?>
           </span>          
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal2"><?php echo $rgconfirm['label3']; ?></button> -->
           <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $rgconfirm['label4']; ?></button>
        </div>
      </div>
    </div>
  </div>
<!--   Personal Registration confirmation Model End -->

<!--   Professional Registration confirmation Model -->
  <div class="modal fade" id="myModalproregsucc" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">         
          <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
          <h4 class="modal-title"><?php echo $prorgconfirm['label1']; ?></h4>
          <div class="title-imge">
            <img src="<?php echo base_url();?>assets/img/logo.png">
          </div>
        </div>
        <div class="modal-body">
          <div class="modal-login-txt">
           <span>
            <?php echo $prorgconfirm['label2']; ?>
           </span>          
          </div>
        </div>
        <div class="modal-footer">
          <!-- <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal2"><?php echo $rgconfirm['label3']; ?></button> -->
           <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $prorgconfirm['label4']; ?></button>
        </div>
      </div>
    </div>
  </div>
<!--   Professional Registration confirmation Model End -->
<!-- thank pop up for subscription -->
<div class="modal fade" id="subscriptpopup" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">         
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title"><?php echo $nwsthankyou['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         <span>
          <?php echo $nwsthankyou['label2']; ?>
         </span>          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $nwsthankyou['label3']; ?></button>
      </div>
    </div>
  </div>
</div>
<!-- thank pop up for subscription end -->

<!-- thank pop up for Claimed business profile -->
  <div class="modal fade" id="clmbsmodalbox" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">         
          <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
          <h4 class="modal-title"><?php echo $clmbsthankyou['label1']; ?></h4>
          <div class="title-imge">
            <img src="<?php echo base_url();?>assets/img/logo.png">
          </div>
        </div>
        <div class="modal-body">
          <div class="modal-login-txt">
           <span>
            <?php echo $clmbsthankyou['label2']; ?>
           </span>          
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo $clmbsthankyou['label3']; ?></button>
        </div>
      </div>
    </div>
  </div>
<!-- thank pop up for Claimed business profile end -->




<!-- thank pop up for reset password -->
<div class="modal fade" id="reset_password_thankx" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">         
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title"><?php echo $passthankyou['label1']; ?></h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         <span>
              <?php echo $passthankyou['label2']; ?> 
         </span>          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"> <?php echo $passthankyou['label4']; ?></button>
      </div>
    </div>
  </div>
</div>
<!-- thank pop up for subscription end -->

<?php
include "footer.php";
?>

<script type="text/javascript">
  $(document).ready(function() {
    getLocation();
    <?php
    if($this->uri->segment(2) == "businessListing"){
    ?>
      setTimeout(function(){
        LoadMap();    
      }, 1500);
    <?php
    }
    ?>
    $("form[name='searchForm']").validate({
      rules: {
        user_address: "required",
      },
      // Specify validation error messages
      messages: {
        user_address: "Please enter address",
       
      },
      submitHandler: function(form) {
        form.submit();
      }
    });
  });
</script>


<!-- for Home -->


<script>
    $(document).ready(function() {

      var owl = $("#owl-demo-pro-head");

      owl.owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        items : 4,
        itemsDesktop : [1000,4],
        itemsDesktopSmall : [900,3],
        itemsTablet: [600,2],
        itemsMobile : [450,1]
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });
    </script>


<script>
    $(document).ready(function() {
      var owl = $("#owl-demo-pro");

      owl.owlCarousel({
      autoPlay : 3000,
        stopOnHover : true,
        items : 4,
        itemsDesktop : [1000,4],
        itemsDesktopSmall : [900,3],
        itemsTablet: [600,2],
        itemsMobile : [450,1]
      });

      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });

        $(document).ready(function() {
      var owl = $("#owl-demo-p_page");

      owl.owlCarousel({
      autoPlay : 3000,
        stopOnHover : true,
        items : 4,
        itemsDesktop : [1180,3],
        itemsDesktopSmall : [900,3],
        itemsTablet: [600,2],
        itemsMobile : [450,1]     
      });
    });
    </script>
   <script>
    $(document).ready(function() {

      var owl = $("#owl-demo-down");

      owl.owlCarousel({
      autoPlay : 3000,
        stopOnHover : true,
        items : 2,
        itemsDesktop : [1000,2],
        itemsDesktopSmall : [900,2],
        itemsTablet: [600,1],
        itemsMobile : false      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });
 </script>
