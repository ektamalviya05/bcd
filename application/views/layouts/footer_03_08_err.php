
<footer class="main_footer">
  <div class="footer_new_letter">
    <div class="news-letter">
      <div class="container">
        <div class="new_letter_main">
          <div class="row">
            <div class="col-sm-3">
              <div class="news_letter_heading">
                <h2>SIGN UP OUR NEWSLETTER</h2>
              </div>
            </div>
            <div class="col-sm-9">
              <div class="new_letter_input">
                <form method="post" action="<?php echo base_url("Users/subscription"); ?>">
                  <div class="input-group">
                    <input required="" type="text" class="form-control" name="user_email" placeholder="Enter Email">
                    <input type="text" class="form-control" placeholder="Enter Postal Code" name="user_postcode">
                    <span class="input-group-btn">
                      <button name="subscribeSubmit" class="btn btn-default" type="submit" value="Subscribe Submit">SUBSCRIBE</button>
                    </span> 
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="f_middle">
        <div class="col-md-4 col-sm-4">
          <div class="footer-logo"> <img src="<?php echo base_url();?>assets/img/logo.png"> </div>
          <div class="f_heading">
            <?php
                  $footerart = $this->db->get_where("site_contents", array("content_id"=>11));
                  foreach($footerart->result() as $ft){
                    echo $ft->content;
                  }
                  ?>
          </div>
        </div>
        <div class="col-md-8 col-sm-8">
          <div class="working_hours">
            <div class="col-sm-4">
              <div class="f_heading">
                <h3>Information</h3>
              </div>
              <ul>
                <?php
                            $Information = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>1, "pg_status"=>1));
                            foreach($Information->result() as $info){
                              ?>
                <li><a href="<?php echo base_url("Pages/content/$info->pg_meta_tag"); ?>"><?php echo $info->pg_title; ?></a></li>
                <?php
                            }
                          ?>
              </ul>
            </div>
            <div class="col-sm-4">
              <div class="f_heading">
                <h3>Discover</h3>
              </div>
              <ul>
                <?php
                              $discover = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>2, "pg_status"=>1));
                              foreach($discover->result() as $disc){
                                ?>
                <li><a href="<?php echo base_url("Pages/content/$disc->pg_meta_tag"); ?>"><?php echo $disc->pg_title; ?></a></li>
                <?php
                              }
                            ?>
              </ul>
            </div>
            <div class="col-sm-4">
              <div class="f_heading">
                <h3>Professional Accounts</h3>
              </div>
              <ul>
                <?php
                            $professional = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>3, "pg_status"=>1));
                            foreach($professional->result() as $prof){
                              ?>
                <li><a href="<?php echo base_url("Pages/content/$prof->pg_meta_tag"); ?>"><?php echo $prof->pg_title; ?></a></li>
                <?php
                            }
                          ?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="copyright">
          <div class="col-sm-6">
            <div class="footer-link-social">
              <ul>
                <li><a href=""><i class="fa fa-twitter"></i></a></li>
                <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                <li><a href=""><i class="fa fa-facebook"></i></a></li>
                <li><a href=""><i class="fa fa-instagram"></i></a></li>
              </ul>
            </div>
          </div>
          <div class="col-sm-6">
            <div class="f_follow">Copyright © 2017</div>
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>
<div class="side-footer-link">
  <?php
  $facebook = $this->db->get_where("otd_social_links", array("sc_id"=>1))->row_array();
  $twitter = $this->db->get_where("otd_social_links", array("sc_id"=>2))->row_array();
  $instagram = $this->db->get_where("otd_social_links", array("sc_id"=>3))->row_array();
?>
  <ul>
    <li><i class="fa fa-facebook"></i>
      <div class="icon-detail"> <a href="<?php echo $facebook['sc_links']; ?>">Facebook</a> </div>
    </li>
    <li><i class="fa fa-twitter"></i>
      <div class="icon-detail"> <a href="<?php echo $twitter['sc_links']; ?>">Twitter</a> </div>
    </li>
    <li><i class="fa fa-instagram"></i>
      <div class="icon-detail"> <a href="<?php echo $instagram['sc_links']; ?>">Instagram</a> </div>
    </li>
  </ul>
</div>

<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> 


<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/imagelightbox.js"></script>
<script src="<?php echo base_url();?>assets/js/moment-with-locales.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap-material-datetimepicker.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script> 
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script> 
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script> 
<script src="<?php echo base_url("assets/js/jquery.flexdatalist.js");?>"></script> 
<script src="<?php echo base_url('assets/js/jquery.mCustomScrollbar.min.js'); ?>"></script> 
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script> 
<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script> 
<script src="<?php echo base_url();?>assets/js/lightbox.min.js"></script> 
<script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script> 
<script src="<?php echo base_url("assets/js/custom.js");?>"></script> 
<script type= 'text/javascript' src="<?php echo base_url(); ?>assets/js/blog_rating.js"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<link rel="stylesheet" href="<?php echo base_url();?>assets/vendor/bootstrap/js/imagelightbox.css" />
<!-- moved script files to end -->


<?php
if($this->uri->segment(2) == "user_profile"){
  //open for profile
  ?>
  <script src='https://www.google.com/recaptcha/api.js'></script>

    <script>
    //SET THESE VARS
    var $transitionLength = 400;
    var $timeBetweenTransitions = 4000;

    //STORAGE
    var imageCount = 0;
    var currentImageIndex = 0;
    var currentScrollIndex = 1;
    var $imageBank = [];
    var $thumbBank = [];
    var $mainContainer = $("#gallery-main");
    var $thumbContainer = $("#thumbcon");
    var $progressBar = $("#progressbar");
    var currentElement;

    //CONTROLS
    var $go = true;

    $(document).ready(function(){

      $("#gallery-hidden img").each(function() {
        $imageBank.push($(this).attr("id", imageCount));
        imageCount++;
      });

      generateThumbs();

      setTimeout(function () {
        imageScroll(0);
      }, $timeBetweenTransitions);

      $('#left-arrow').click(function () {
        thumbScroll("left");
        toggleScroll(true);
        });

      $('#right-arrow').click(function () {
        thumbScroll("right");
        toggleScroll(true);
        });

      $('#thumbcon img').on('click',function () {

        imageFocus(this);
      });

      $('#playtoggle').click(function () {
        toggleScroll(false);
      });
    });

    function progress(imageIndex){
      var parts = 960/imageCount-1;
      var pxProgress = parts*(imageIndex+1);

      $progressBar.css({ width: pxProgress , transition: "all 0.7s ease"});
    }

    function imageFocus(focus){
      for(var i = 0; i < imageCount; i++){
        if($imageBank[i].attr('src') == $(focus).attr('src')){
          $mainContainer.fadeOut($transitionLength);
          $thumbBank[currentImageIndex].removeClass("selected");
          setTimeout(function () {
            $mainContainer.html($imageBank[i]);
            $thumbBank[i].addClass("selected");
            $mainContainer.fadeIn($transitionLength);
          }, $transitionLength);
          currentScrollIndex = i+1;
          currentImageIndex = i;
          progress(currentImageIndex);
          toggleScroll(true);

          return false;
        }
      }
    }

    function toggleScroll(bool){
      if($go){
        $go = false;
        $('#playtoggle').children().removeClass('icon-pause').addClass('icon-play');
      }else{
        $go = true;
        $('#playtoggle').children().removeClass('icon-play').addClass('icon-pause');
      }

      if(bool){
        $go = false;
        $('#playtoggle').children().removeClass('icon-pause').addClass('icon-play');
      }
    }

    function autoScroll(){
      if(currentScrollIndex >= 0 || currentScrollIndex < imageCount){
        if(currentScrollIndex+1 > imageCount){
          $thumbBank[0].css({ marginLeft: "0" , transition: "all 1.0s ease"});
          currentScrollIndex = 1;
        }else if(currentScrollIndex+1 >= 3){
          if(currentScrollIndex+2 >= imageCount){

          }else
            $thumbBank[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});

          currentScrollIndex++;
        }else{
          currentScrollIndex++;
        }
      }
    }

    function thumbScroll(direction){
      if(currentScrollIndex >= 0 || currentScrollIndex < imageCount){
        var marginTemp = currentScrollIndex;
        if(direction == "left"){
          if(currentScrollIndex-3 <= 0){
            var k = ((imageCount-4)*200)-5;
            $thumbBank[0].css({ marginLeft: -k , transition: "all 1.0s ease"});
            currentScrollIndex = imageCount-1;
          }else{
            $thumbBank[0].css({ marginLeft: "+=200" , transition: "all 1.0s ease"});
            currentScrollIndex--;
          }
        }else if(direction == "right"){
          if(currentScrollIndex+3 >= imageCount){
            $thumbBank[0].css({ marginLeft: "5px" , transition: "all 1.0s ease"});
            currentScrollIndex = 1;
          }else{
            $thumbBank[0].css({ marginLeft: "-=200" , transition: "all 1.0s ease"});
            currentScrollIndex++;
          }
        }
      }
    }

    function generateThumbs(){
      progress(currentImageIndex);
      for(var i = 0; i < imageCount; i++){

        var $tempObj = $('<img id="'+i+'t" class="thumb" src="'+$imageBank[i].attr('src')+'" />');

        if(i == 0)
          $tempObj.addClass("selected");

        $thumbContainer.append($tempObj);
        $thumbBank.push($tempObj);

      }
    }

    function imageScroll(c){
      if($go){

        $thumbBank[c].removeClass("selected");

        c++

        if(c == $imageBank.length)
          c = 0;

        $mainContainer.fadeOut($transitionLength);
        setTimeout(function () {
          $mainContainer.html($imageBank[c]);
          $thumbBank[c].addClass("selected");
          autoScroll("left");
          $mainContainer.fadeIn($transitionLength);
        }, $transitionLength);

      }

      progress(c);

      setTimeout(function () {
        imageScroll(currentImageIndex);
      }, $timeBetweenTransitions);

      currentImageIndex = c;
    }


  </script>

  <?php
    if($is_claiming == "yes")
    {
    ?>
    <script type="text/javascript">
      $(document).ready(function() {
       $("#shownumberexits").trigger("click");
      });
    </script>
    <?php
    }

    if($openphone == "openphonemodal")
    {
    ?>
      <script type="text/javascript">
      $(document).ready(function() {
       $("#shownumberexits_log").trigger("click");
      });
      </script>
  

    <?php
    //close for profile
    }
    ?>



<script type="text/javascript">
    $(function() {
      // Initialize form validation on the registration form.
      // It has the name attribute "registration"
      $("form[name='myForm']").validate({
        rules: {
            user_phone:"required",
            user_firstname: "required",
            user_lastname: "required",
            user_email: {
              required: true,
              email: true
            },
            user_company_address:"required",
            confidential:"required"      
          },
        messages: {
          user_phone:"Please enter phone number",
          user_firstname: "Please enter your firstname",
          user_lastname: "Please enter your lastname",
          user_categories: "Please select business category",
          user_company_address:"Please enter company address",      
          confidential:"Please select terms & conditions",
          user_email: {
            required:"Please enter a valid email address",
            email:"Please enter valid email address"
          }
        },
        // Make sure the form is submitted to the destination defined
        // in the "action" attribute of the form when valid
        submitHandler: function(form) {
          form.submit();
        }
      });
    });



    function follow(pro_id, act)
    {
            var profile_id = pro_id;
            var data = "";
            var but="";
                                                    
            var msg="";
            if(act ==1)
            {
              msg = "Thanks for following";
              but = '<span><i class="fa fa-share"></i></span><a href="javascript:;" id="follow" onclick="return follow('+ profile_id + ', 2);">Unfollow the Merchant</a>';
              data = 'profile_id=' + profile_id + '&follow_action=follow';
            }
            else
            {
              msg = "You have Unfollowed the business";
              data = 'profile_id=' + profile_id + '&follow_action=unfollow';
              but = '<span><i class="fa fa-share"></i></span><a href="javascript:;" id="follow" onclick="return follow('+ profile_id + ', 1);">Follow the Merchant</a>';
            }
            //alert(data);
            $.ajax({
                type: "POST",
                data: data,
               //url: "http://localhost/hmvc_site/Users/follow_user",
               url: "<?php echo base_url("Users/follow_user"); ?>", //CALLBACK FILE
                success: function (e) {

                    // alert(msg);
                    //location.reload();
                    document.getElementById("follow_li").innerHTML = but;
                },
                error: function (e) {
                    alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
                }
            });
    }
    function showphone()
    {
      //alert("Hello ");
      $('#dsply-text').toggle();
      $('#dsply-num').toggle();
    }


  function recommend(rec_to_id)
  {
  
   data = 'rec_to=' + rec_to_id;
       $.ajax({
         type: "POST",
         data: data,
        //url: "http://localhost/hmvc_site/Users/recommend",
        url: "http://votivephp.in/VotiveYellowPages/Users/recommend", //CALLBACK FILE
        success: function (e) {
             alert("Thanks for recommendation ");
             //location.reload();
             document.getElementById("rec_count").innerHTML = e;
         },
         error: function (e) {
             alert("Somethig Went Wrong! Please try again after sometime" +  JSON.stringify(e));
         }
     });
  }

</script>   

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script>
    <?php

}
?>



<?php
  $reg_success = $this->session->userdata('reg_success');
  if( trim($reg_success) == "yes")
  {
   ?>
    <script type="text/javascript">
      $(document).ready(function() {
      openModalreg();
      });
      function openModalreg(){
        $('#myModalregsucc').modal();
      } 
    </script>
  <?php
  }
  $this->session->set_userdata('reg_success', '');
  //$this->session->set_userdata('success_msg', '');
?>
<!--
inner layout
<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script>
<script src="<?php echo base_url("assets/js/jquery.flexdatalist.js");?>"></script>
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
<script src="<?php echo base_url("assets/js/custom.js");?>"></script>

--> 



<?php
  $reg_success = $this->session->userdata('reg_success');
  if( trim($reg_success) == "yes")
  {
  ?>
    <script type="text/javascript">
      $(document).ready(function() {
      openModalreg();
      });
      function openModalreg(){
        $('#myModalregsucc').modal();
      } 
    </script>
<?php
  }
  $this->session->set_userdata('reg_success', '');
?>
<script>

$('a[data-imagelightbox="demo"]').imageLightbox({
  selector:       'a[data-imagelightbox]',
  id:             'imagelightbox',
  allowedTypes:   'png|jpg|jpeg|gif',
  animationSpeed: 250,
  activity:       true,
  arrows:         true,
  button:         true,
  caption:        true,
  enableKeyboard: true,
  lockBody:       true,
  navigation:     true,
  overlay:        true,
  preloadNext:    true,
  quitOnEnd:      true,
  quitOnImgClick: true,
  quitOnDocClick: true,
  quitOnEscKey:   true
});



$(window).scroll(function() {   
       if ($(window).scrollTop() >=50) { 
          if($('#nav-top').hasClass( "navi" ))
{
  //jQuery('#header').removeClass("fixed-theme");
  }
  else
  {
  $('#nav-top').addClass("navi");
  }
}
else{
$('#nav-top').removeClass("navi");
}
});



  $(window).scroll(function() {   
        
        if ($(window).scrollTop() >=50) { 
          if($('.filter-sec').hasClass( "fs-top" ))
        {
      //jQuery('#header').removeClass("fixed-theme");
      }
      else
      {
      $('.filter-sec').addClass("fs-top");
      }
        }
    else{
    $('.filter-sec').removeClass("fs-top");
    }



      if ($(window).scrollTop() >=50) { 
      if($('.map-main').hasClass( "map-top" ))
      {
        //jQuery('#header').removeClass("fixed-theme");
      }
      else
      {
        $('.map-main').addClass("map-top");
      }
    }
    else{
      $('.map-main').removeClass("map-top");
    }


 $('.date').bootstrapMaterialDatePicker
      ({
        time: false,
        clearButton: true
      });     
     
      $('.time').bootstrapMaterialDatePicker
      ({
        date: false,
        shortTime: true,
        format: 'hh:mm A'
      });

 


 $(".extra-filter").click(function(){
          $(".extra-filter-sec").slideToggle("slow");
      });



 var owl = $("#owl-demo");

      owl.owlCarousel({
        autoPlay : 3000,
        stopOnHover : true,
        singleItem : true,
        items : 1, 
        itemsDesktop : [1000,1], 
        itemsDesktopSmall : [900,1], 
        itemsTablet: [600,1], 
        itemsMobile : false 
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      });
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      });



 var owl = $("#owl-demo-picture");
      owl.owlCarousel({
        autoPlay : 3000,
        items : 3,
        itemsDesktop : [1000,3], 
        itemsDesktopSmall : [900,3], 
        itemsTablet: [600,2]
      });


$(".owl-demo").owlCarousel({
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      pagination : true,
      singleItem:true
      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false
  });




   });

  </script> 


 <script>

$(document).ready(function() {   
  <?php
   $userloggedin = $this->session->userdata("user_id");
  if(empty($userloggedin)){
  ?>
  setTimeout(function(){    
    if(!$("#myModal2").hasClass("in") && !$("#myModal").hasClass("in") && !$("#myModal3").hasClass("in") && !$("#myModalregsucc").hasClass("in") && !$("#personalaccountreadmore").hasClass("in") && !$("#professionalaccountreadmore").hasClass("in")){            
      if(!checkCookie()){
        $("#registrationmodal").trigger("click");
        var d = new Date();
        d.setTime(d.getTime() + (1*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = "UserSeeSignup=Yes;" + expires + ";path=/";
      }
    }
  },6000); // 10000 to load it after 10 seconds from page load
 
  <?php
  }
  ?>
 
});


function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("UserSeeSignup");
    if (user != "") {
      return true;
    } else {
      return false;
    }
}


</script> 


<script>
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
       
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'],  componentRestrictions: {country: "fr"}});

           //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

       
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          //alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script> 
<!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script> 
 -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1p4mveOj8d4fxKTtfx4sJooTul47ffm0&libraries=places&callback=initAutocomplete"
        async defer></script>


<!-- Social Login --> 
<script type="text/javascript">
      var flag=false;
      // This is called with the results from from FB.getLoginStatus().
      
      function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
 
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          getUserInfo();
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          /*document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';*/
        }
      }

      // This function is called when someone finishes with the Login
      // Button.  See the onlogin handler attached to it in the sample
      // code below.
      function checkLoginState() {
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
      }

    
      // Load the SDK asynchronously

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1546847162054270', //1068492099877422 App ID (1776946472525281)
          // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true,  // parse XFBML
          version    : 'v2.8'
        });
        
        
        FB.Event.subscribe('auth.authResponseChange', function(response) 
        {
          if (response.status === 'connected') 
          {
            console.log("Connected to Facebook");
            //SUCCESS
            
          }  
          else if (response.status === 'not_authorized') 
          {
            console.log("Failed to Connect");

          //FAILED
          } else 
          {
          console.log("Logged Out");
            

            //UNKNOWN ERROR
          }
        }); 
      
      };
      
      function Login(){
        //alert(1)
        FB.login(function(response) {
          if (response.authResponse) 
          {
            getUserInfo();
          } else 
          {
             console.log('User cancelled login or did not fully authorize.');
          }
        }, {scope: 'email,user_birthday,user_location'});  
      }

      function getUserInfo() {//alert(1)      
          FB.api('/me',{fields: 'id,email, first_name, last_name, picture.width(800).height(800),gender'}, function(response) {
      
            console.log(response);
            $.ajax({
              method: "POST",
              url: "<?php echo base_url();?>Users/fblogin",
              dataType: 'json',
              data: {'id':response.id, 'first_name':response.first_name, 'last_name':response.last_name, 'email': response.email, 'picture': response.picture.data.url, 'gender': response.gender},
              success:function(data){              
                if(data.flag==1){
                   window.location.href = '<?php echo base_url('Users/account');?>';                 
                   return false;  
                }              
              }
            }); 
          });
      }

      function Logout()
      {
        FB.logout(function(){ window.location.replace("<?php echo base_url(); ?>Users/logout");});
      }

      // Load the SDK asynchronously
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));


      ////////////////////////////////////////Gmail Login////////////////////////////////////////////////

      /*Google signup*/

      function onSignIn(googleUser) {
        
        BasicProfile = googleUser.getBasicProfile();

        var social_id = BasicProfile.getId();      
        var first_name = BasicProfile.getGivenName();
        var last_name = BasicProfile.getFamilyName();
        var image_path = BasicProfile.getImageUrl();
        var email = BasicProfile.getEmail();

        // var userId = profile.getId(); // Do not send to your backend! Use an ID token instead.
        // var userName = profile.getName();
        // var userEmail =  profile.getEmail();
        // var picture = profile.getImageUrl();
        //var DOB = profile.getBirthday()

        if(flag==false){
           
          $.ajax({
              method:"POST",
              url:'<?php echo base_url();?>Users/googleLogin',
              dataType: 'json',
              data: {'id':social_id ,'email': email, 'first_name':first_name, 'last_name':last_name, 'picture':image_path},
              success:function(data){
                if(data.flag == 1){                
                  window.location.href = '<?php echo base_url('Users/account');?>';                
                  flag=true;
                  return false;                        
                }else{
                      console.log(data); 
                }
              }
          });
        } 
      }

      $("glog").click(function(){
          gp_signOut();
      });

      function gp_signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
              console.log('User signed out.');
          });
      }
  </script> 

<script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script> 

<script>
    $("glog").click(function(){
        gp_signOut();
    });

    function gp_signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

    function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
        disassociate();
             $.ajax({
              method: "POST",
              dataType:"json",
              url: "<?php echo base_url();?>Users/socialLogout",
              success:function(data){
                 if(data.status == 200){
                   document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>";                     
                 }
              }
              }); 
      
        // document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>userlogin";
      });
    }
    
    function onLoad() {
      gapi.load('auth2', function() {
        gapi.auth2.init();
      });
    }
    function disassociate() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect().then(function () {
            console.log('User disconnected from association with app.');
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////// 
  </script> 
  

<script>
    $(document).ready(function () {
    
        $('.dropdown-toggle').dropdown();

    });
</script> 
<!-- Social Login end -->




<script>
/*Code for Search Result Page---Pradeep */
var map;
var iconicon;
var mapOptions;
var infoWindow
var allMyMarkers = [];

var markers = [
     <?php 
   if(isset($businessMarker))
   {
    $c = count($businessMarker) - 1;
    $i = 0;
    foreach ($businessMarker as $businessL) {?>
    {
        "title": "<?php if(isset($businessL['user_company_address'])){echo $businessL['user_company_address'];}?>",
        "lat": '<?php if(isset($businessL['user_lat'])){echo $businessL['user_lat'];}?>',
        "lng": '<?php if(isset($businessL['user_long'])){echo $businessL['user_long'];}?>',
        "description": '<?php if(isset($businessL['user_company_name'])){echo $businessL['user_company_name'];}?>'
    }
   <?php if($i < $c)
   {echo ",";}?>
   <?php $i++;
    }}?> 
    ];





  function LoadMap() {
      mapOptions = {
            center: new google.maps.LatLng(markers[1].lat, markers[1].lng),
            zoom:4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById("map"), mapOptions);
 
      iconicon = {
              url: "http://localhost/hmvc_site/assets/img/map-marker-wyootwo.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
    infoWindow = new google.maps.InfoWindow();
 
      for (var i = 0; i < markers.length; i++) {
            var data = markers[i];
            var myLatlng = new google.maps.LatLng(data.lat, data.lng);
            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                icon: iconicon,
                id: i,
                title: data.title
            });
            allMyMarkers.push(marker);
 
            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/>'+ data.description + "</div>");
                    infoWindow.open(map, marker);
                });
            })(marker, data);

        }


  google.maps.event.addDomListenerOnce(map, 'idle', function () 
      {
            google.maps.event.addDomListener(window, 'resize', function () {
            map.setCenter(LatLng);
            });
    });
  

       $('.listing-sub').hover(function(){
                var selectedID = $(this).attr('id');
                toggleBounce(selectedID);

               
        });
    }


function toggleBounce(selectedID) {
        var pinID = selectedID.split('-');
        for(var j=0;j<allMyMarkers.length;j++){
     
                if(allMyMarkers[j].id == pinID[1]){
                        if (allMyMarkers[j].getAnimation() != null) {
                                allMyMarkers[j].setAnimation(null);
                        } else {
                                var mapOptions = {
                                center: new google.maps.LatLng(markers[0].lat, markers[0].lng),
                                //zoom:4,
                                //mapTypeId: google.maps.MapTypeId.ROADMAP
                                };
                               // map = new google.maps.Map(document.getElementById("map"), mapOptions);
                        map.set(mapOptions);

              iconicon = {
              url: "<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
                             //   var image = 'http://localhost/hmvc_site/assets/img/map-marker.png';
                                    

                                //Create and open InfoWindow.
              infoWindow = new google.maps.InfoWindow();
                                for (var i = 0; i < markers.length; i++) {

                                   if(pinID[1] == i)
                                    {

                              iconicon = {
              url: "<?php echo base_url(); ?>assets/img/map-marker.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
                      }
                                    else
                                    {
                                       iconicon = {
              url: "<?php echo base_url(); ?>assets/img/map-marker-wyootwo.png", // url
              scaledSize: new google.maps.Size(30, 30), // scaled size
              origin: new google.maps.Point(0,0), // origin
              anchor: new google.maps.Point(0, 0) // anchor
              };
                                    }

                                    var data = markers[i];
                                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);

                                    var marker = new google.maps.Marker({
                                        position: myLatlng,
                                        map: map,
                                        icon: iconicon,
                                        id: i,
                                        title: data.title
                                    });
                                    marker.setIcon(iconicon);
                                    allMyMarkers.push(marker);

                                    (function (marker, data) {
                                        google.maps.event.addListener(marker, "click", function (e) {
                                        infoWindow.setContent("<div style = 'width:300px;min-height:50px'>" +'<h4>'+ data.title +'</h4>'+' <br/>'+ data.description + "</div>");
                                        infoWindow.open(map, marker);
                                        });
                                    })(marker, data);
                                  
                                }

                                map.setCenter(allMyMarkers[j].getPosition());
                                
                        }
                        break; // stop continue looping
                }
        }
} // end toggleBounce


 $(document).ready(function() {

getLocation();
 LoadMap(); 


 });


function showphone(id)
{
//alert("Hello " + id);
$('#'+id).toggle();
$('#txt'+id).toggle();
}



function getLocation() {


   var options = {timeout:60000};
   
   var geolocation = navigator.geolocation;
   geolocation.getCurrentPosition(showLocation, errorHandler, options);
   geolocation.getCurrentPosition(showLocation);

}
function showLocation(position) {
  GetAddress(position.coords.latitude, position.coords.longitude);
  }

function errorHandler(err) {
            if(err.code == 1) {
               alert("Error: Access is denied!" + err.message);

               
            }
            
            else if( err.code == 2) {
               //alert("Error: Position is unavailable!" + err.message);

    var latLong;
    $.getJSON("http://ipinfo.io", function(ipinfo){
    console.log("Found location ["+ipinfo.loc+"] by ipinfo.io");
    latLong = ipinfo.loc.split(",");
    //alert(latLong[1]);
     GetAddress(latLong[0], latLong[1]);
});
            }



 history.pushState({},"Results for `Cats`",'?s=no_location');
                     
         }

    
function GetAddress(lat, lng) {
           // var lat = parseFloat(document.getElementById("txtLatitude").value);
            //var lng = parseFloat(document.getElementById("txtLongitude").value);
          //  alert(lat);
            // document.getElementById('currentlatlng').value = lat + "," + lng;
           var latlng = new google.maps.LatLng(lat, lng);
            
            var geocoder = geocoder = new google.maps.Geocoder();

            geocoder.geocode({ 'latLng': latlng }, function (results, status) {


                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[1]) {

                     // alert(document.getElementById('autocomplete').value );
                      if(document.getElementById('autocomplete').value == "")
                      {
                      document.getElementById('autocomplete').value = results[1].formatted_address;
                     
                       }
                       else  
                    {
                     // alert("Location: " + results[1].formatted_address);
                    }

                    var uris ="?"+results[1].formatted_address;
                     history.pushState({},"Results for `Cats`",uris);

                    }
                }
            });
        }


$(window).scroll(function() {   
       if ($(window).scrollTop() >=51) { 
          if($('#nav-top').hasClass( "navbar-fixed-top" ))
{
  //jQuery('#header').removeClass("fixed-theme");
  }
  else
  {
  $('#nav-top').removeClass("navi");
  }
}
else{
$('#nav-top').addClass("navi");
}
});


/* END Code for Search Result Page---Pradeep */




  
</script>



    


</body>
</html>