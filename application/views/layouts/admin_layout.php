<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="icon" href="<?php echo base_url(); ?>assets_new/images/favicon.ico" type="image/ico" sizes="16x16">
        <title><?php echo!empty($title) ? $title : "Admin Panel"; ?></title>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-toggle.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
        <!-- <link href="<?php echo base_url(); ?>assets/dist/css/sb-admin-2.css" rel="stylesheet">-->
        <link href="<?php echo base_url("assets/datetimepicker/css/bootstrap-datetimepicker.min.css"); ?>" rel="stylesheet" media="screen">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/admin-dash.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.css">
        <link href="<?php echo base_url(); ?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/jquery.flexdatalist.css"); ?>">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
    </head>

    <body>
        <?php
        if (empty($this->session->userdata('isUserLoggedIn')) || $this->session->userdata('user_type') != "Admin") {
            redirect(base_url());
        }
        ?>
        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top curs-admin" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo base_url(); ?>Admin"><img src="<?php echo base_url(); ?>assets/img/logo.png"></a>
                </div>
                <!-- Top Menu Items -->
                <ul class="nav navbar-right top-nav adm_sid_bar">

                    <?php if ($this->session->userdata('isUserLoggedIn')) {
                        ?>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> ilbait Admin <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <!-- <li>
                                    <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                                </li>
                                <li class="divider"></li> -->
                                <li>
                                    <a href="<?php echo base_url(); ?>Admin/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                                </li>

                            </ul>
                        </li>
                        <li class="dropdown">
                            <a  class="dropdown-toggle notification" data-toggle="dropdown"><i class="fa fa-envelope"></i> Notification Review   <b class="caret"></b></a>
                            <ul class="dropdown-menu notificationdata" >
                               

                            </ul>
                        </li>


                        <?php
                    }
                    ?>
                </ul>
                <div class="collapse navbar-collapse navbar-ex1-collapse admin-nav">
                    <div class="ad-menu side-nav mCustomScrollbar">
                    <ul class="nav navbar-nav">
                        <li class="<?php if ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'dashboard') {
                        echo 'active';
                    } ?>" >
                            <a href="<?php echo base_url(); ?>Admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                        </li>                    
                        <li class="dropdown <?php if ($this->uri->segment(2) == 'user_list_pro') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> User Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">


                                <li class="<?php
                                    if ($this->uri->segment(2) == 'user_list_pro') {
                                        echo 'active';
                                    }
                                    ?>">
                                    <a href="<?php echo base_url(); ?>Admin/user_list_pro"><i class="fa fa-fw fa-user-circle-o"></i>Users</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown <?php if ($this->uri->segment(1) == 'Category') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> Category Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(1) == 'Category') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url("Category"); ?>"><i class="fa fa-fw fa-puzzle-piece"></i> Category</a>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown <?php if ($this->uri->segment(1) == 'Listing') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> Listing Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(1) == 'Listing') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url("Listing"); ?>"><i class="fa fa-fw fa-puzzle-piece"></i> Listing</a>
                                </li>
                            </ul>
                        </li>
                    <li class="dropdown-submenu <?php if ($this->uri->segment(2) == 'option_list' || $this->uri->segment(2) == 'linkOptionUserSearch' || $this->uri->segment(2) == 'createOption' || $this->uri->segment(2) == 'createStaticOption' || $this->uri->segment(2) == 'createPackage' || $this->uri->segment(1) == 'transactionRecord') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-star-half-o"></i> Package Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu <?php if ($this->uri->segment(2) == 'option_list' || $this->uri->segment(2) == 'createOption') echo "active"; ?>">                            
                                <li class="<?php if ($this->uri->segment(2) == 'option_list') {
                                        echo 'active';
                                    } ?>"><a href="<?php echo base_url('Admin/option_list'); ?>"><i class="fa fa-fw fa-download"></i>Packages</a></li>
                                <li class="<?php if ($this->uri->segment(2) == 'createPackage') echo 'active'; ?>"><a href="<?php echo base_url('Admin/createPackage'); ?>"><i class="fa fa-tasks"></i> Create Package</a></li>
                                <!-- <li class="<?php if ($this->uri->segment(2) == 'createOption') echo 'active'; ?>"><a href="<?php echo base_url('Admin/createOption'); ?>"><i class="fa fa-dot-circle-o"></i> Create Form Option</a></li> --> 
                                <!-- <li class="<?php if ($this->uri->segment(2) == 'createStaticOption') echo 'active'; ?>"><a href="<?php echo base_url('Admin/createStaticOption'); ?>"><i class="fa fa-dot-circle-o"></i> Create Static Option</a></li> -->

                                 <li class="<?php if ($this->uri->segment(1) == 'transactionRecord') echo 'active'; ?>"><a href="<?php echo base_url('transactionRecord'); ?>"><i class="fa fa-tasks"></i> Transaction History</a></li>
                                
                            </ul>
                        </li>                        <li class="dropdown-submenu <?php if ($this->uri->segment(2) == 'googleAdScript' || $this->uri->segment(2) == 'manualAdvertisement' || $this->uri->segment(2) == 'addAdervertisement') {
    echo 'active open';
} ?>">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-television"></i> Advertisements
                                        <span class="caret"></span></a>                          
                                    <ul class="dropdown-menu">
                                        <li class='<?php if ($this->uri->segment(2) == 'manualAdvertisement') {
    echo 'active';
} ?>'>
                                            <a href="<?php echo base_url("Content/manualAdvertisement"); ?>"><i class="fa fa-dot-circle-o"></i>  Manual Advertisements</a>
                                        </li>
                                       <!--  <li class="<?php if ($this->uri->segment(2) == 'googleAdScript') {
    echo 'active';
} ?>">
                                            <a href="<?php echo base_url("Content/googleAdScript"); ?>"><i class="fa fa-dot-circle-o"></i>  Google Script</a>
                                        </li> -->
                                        
                                    </ul>
                                </li>
                        <!-- <li class="dropdown-submenu <?php if ($this->uri->segment(2) == 'reviewListing' || $this->uri->segment(2) == 'viewreview' || $this->uri->segment(2) == 'reportReviewListing') {
                                        echo 'active open';
                                    } ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-star-half-o"></i> Reviews
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu <?php if ($this->uri->segment(2) == 'reviewListing' || $this->uri->segment(2) == 'reportReviewListing' || $this->uri->segment(2) == 'viewreview') echo "active"; ?>">
                                <li class='<?php if ($this->uri->segment(2) == 'reviewListing' || $this->uri->segment(2) == 'viewreview') echo "active"; ?>'><a href="<?php echo base_url(); ?>Admin/reviewListing"><i class="fa fa-star-half-o"></i></i>  Review Listing</a></li>
                                <li class='<?php if ($this->uri->segment(2) == 'reportReviewListing') echo "active"; ?>'><a href="<?php echo base_url("Admin/reportReviewListing"); ?>"><i class="fa fa-dot-circle-o"></i> Reported Review</a></li>                            
                            </ul>
                        </li> -->


                        <li class="dropdown <?php if ($this->uri->segment(1) == 'Country' || $this->uri->segment(1) == 'City') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> Country Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(1) == 'Country') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url("Country"); ?>"><i class="fa fa-fw fa-puzzle-piece"></i> Country</a>
                                </li>
                               <!--  <li class="<?php if ($this->uri->segment(1) == 'City') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url("City"); ?>"><i class="fa fa-fw fa-puzzle-piece"></i> City</a>
                                </li> -->
                            </ul>
                        </li>
                         <li class="dropdown <?php if ($this->uri->segment(2) == 'pacakge_list') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> Featured Package Management
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(1) == 'pacakge_list') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url("Admin/pacakge_list"); ?>"><i class="fa fa-fw fa-puzzle-piece"></i> Packages</a>
                                </li>
                                <li class="<?php if ($this->uri->segment(1) == 'addPackage')  echo 'active'; ?>"><a href="<?php echo base_url('Admin/addPackage'); ?>"><i class="fa fa-tasks"></i> Create Package</a></li>

                            </ul>
                        </li>


                       <li class="<?php if ($this->uri->segment(1) == 'Languages') {
                                        echo 'active';
                                    } ?>">
                            <a href="<?php echo base_url("Languages"); ?>"><i class="fa fa-fw fa-language"></i> Languages</a>
                        </li>


                     <li class="dropdown <?php if ($this->uri->segment(2) == 'subs_list' || $this->uri->segment(1) == 'Boxidea') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-comments"></i> Communication
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(2) == 'subs_list') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url(); ?>Admin/subs_list"><i class="fa fa-fw fa-envelope-o"></i> Newsletter</a>
                                </li>

                               <!--  <li class="<?php if ($this->uri->segment(1) == 'Boxidea') {
                                        echo 'active';
                                    } ?>">
<?php
$msgcount = $this->db->where("bx_flag", 0)->count_all_results("otd_idea_box");
?>
                                    <a href="<?php echo base_url("Boxidea"); ?>"><i class="fa fa-fw fa-th-large"></i> Inquiry <span class="msg-count" style="<?php if (empty($msgcount)) echo "display:none;"; ?>"><?php if ($msgcount > 0) echo $msgcount; ?></span></a>
                                </li>  --> 
                            </ul>
                        </li>


                         <li class="dropdown <?php if ($this->uri->segment(2) == 'section' || $this->uri->segment(2) == 'reviewlist') echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i> Review Section
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(2) == 'section') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url(); ?>Review/section"><i class="fa fa-hand-o-right"></i>Show Review on Front</a>
                                </li>
                                <li class="<?php if ($this->uri->segment(2) == 'reviewlist') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url(); ?>Review/reviewlist"><i class="fa fa-hand-o-right"></i>Review List</a>
                                </li>

                               <!--  <li class="<?php if ($this->uri->segment(1) == 'Boxidea') {
                                        echo 'active';
                                    } ?>">
<?php
$msgcount = $this->db->where("bx_flag", 0)->count_all_results("otd_idea_box");
?>
                                    <a href="<?php echo base_url("Boxidea"); ?>"><i class="fa fa-fw fa-th-large"></i> Inquiry <span class="msg-count" style="<?php if (empty($msgcount)) echo "display:none;"; ?>"><?php if ($msgcount > 0) echo $msgcount; ?></span></a>
                                </li>  --> 
                            </ul>
                        </li>


                         <li class="dropdown <?php if ($this->uri->segment(1) == 'enquiry' ) echo "active open"; ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i>  Listing Enquiry
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(1) == 'enquiry') {
                                        echo 'active';
                                    } ?>">
                                    <a href="<?php echo base_url(); ?>enquiry"><i class="fa fa-hand-o-right"></i>Show Enquiry</a>
                                </li>
                               
                            </ul>
                        </li>


                        <li class="dropdown <?php if (($this->uri->segment(3) == 'SocialLinks' || $this->uri->segment(1) == 'Banner' || $this->uri->segment(1) == 'Testimonial' || $this->uri->segment(2) == 'pagesList' || $this->uri->segment(2) == 'editReadMore' || $this->uri->segment(1) == 'LabelSuggestion') && $this->uri->segment(2) != 'setSearchDistanceLimit' && $this->uri->segment(2) != 'emailSettings') {
    echo 'active open';
} ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-pencil-square-o"></i> CMS <span class="caret"></span></a>
                            <ul class="dropdown-menu">

                                <li class="<?php if ($this->uri->segment(3) == 'SocialLinks') {
    echo 'active';
} ?>"><a href="<?php echo base_url("Content/homeSection/SocialLinks"); ?>"><i class="fa fa-link"></i>  Social Links</a></li>
<li class="<?php if( $this->uri->segment(2) == 'pagesList'){echo 'active';} ?>"><a href="<?php echo base_url("Content/pagesList"); ?>"><i class="fa fa-file-text-o"></i> Static Pages</a></li>

                            </ul>
                        </li>
<!--                        <li class="dropdown <?php if ($this->uri->segment(1) == 'Admin' && ($this->uri->segment(2) == 'event_list' || $this->uri->segment(2) == 'eventTypeList' || $this->uri->segment(2) == 'addEventType')) {
    echo 'active open';
} ?>" >
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-calendar"></i> Event
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class="<?php if ($this->uri->segment(2) == 'event_list') {
    echo 'active';
} ?>">
                                    <a href="<?php echo base_url("Admin/event_list"); ?>"><i class="fa fa-fw fa-calendar"></i> Events</a>
                                </li>
                                <li class="dropdown-submenu <?php if ($this->uri->segment(2) == 'eventTypeList') echo 'active open'; ?>">
                                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-calendar"></i> Event Type
                                        <span class="caret"></span></a>
                                    <ul class="dropdown-menu">                            
                                        <li class='<?php if ($this->uri->segment(2) == 'eventTypeList') echo "active"; ?>'><a href="<?php echo base_url("Admin/eventTypeList"); ?>"><i class="fa fa-dot-circle-o"></i> Event Type</a></li>
                                        <li class='<?php if ($this->uri->segment(2) == 'addEventType') echo "active"; ?>'><a href="<?php echo base_url("Admin/addEventType"); ?>"><i class="fa fa-dot-circle-o"></i> Add Event Type</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>-->
                       <!--  <li class="<?php if ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'ETemplate') {
    echo 'active';
} ?>" >
                            <a href="<?php echo base_url('ETemplate'); ?>"><i class="fa fa-fw fa-envelope"></i> Email Templates</a>
                        </li>  -->
                        <!-- <li class="dropdown <?php if ($this->uri->segment(2) == 'setSearchDistanceLimit' || $this->uri->segment(2) == 'emailSettings') {
    echo 'active open';
} ?>">
                            <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cogs"></i> Configuration
                                <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li class='<?php if ($this->uri->segment(2) == 'setSearchDistanceLimit') echo "active"; ?>'>
                                    <a href="<?php echo base_url("Content/setSearchDistanceLimit"); ?>"><i class="fa fa-road"></i>  Search Distance Limit</a>
                                </li>
                                <li class='<?php if ($this->uri->segment(2) == 'emailSettings') echo "active"; ?>'>
                                    <a href="<?php echo base_url("Content/emailSettings"); ?>"><i class="fa fa-cog"></i>  Email Setting</a>
                                </li>
                            </ul>
                        </li> -->
                    </ul>
                    </div>
                </div>
                <!-- /.navbar-collapse -->
            </nav>



            <div id="page-wrapper">

                <div class="container-fluid">
                    <div class="content-div">
<?php echo $contents; ?>
                    </div>
                    <!-- #container fluid  -->
                </div>
                <!-- /#page-wrapper -->

            </div>
            <!-- /#wrapper -->




            <div class="modal fade" id="myModaDel" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Selected Record Deleted</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            Record Successfuly Deleted.
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/user_list">Close</a>

                        </div>
                    </div>
                </div>
            </div>




            <div class="modal fade" id="myModaDelSubs" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Selected Record Deleted</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            Record Successfuly Deleted.
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/subs_list">Close</a>

                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModaActOpt" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Selected Record Activated</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            You have just activated option.  . <?php echo $this->uri->segment(2); ?>

                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">

                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/purchase_list">Close</a> 

                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModaDActOpt" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Selected Option Dectivated</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            You have just Dectivated an Option. 
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/purchase_list">Close</a>
                        </div>
                    </div>
                </div>
            </div>





            <div class="modal fade" id="myModaAct" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title"></h4>
                            <div class="title-imge">
                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>
                                            You have just Activated a User.
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(uri_string()); ?>">Close</a>
                        </div>
                    </div>
                </div>
            </div>



            <div class="modal fade" id="myModaDAct" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title"></h4>
                            <div class="title-imge">
                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>
                                            You have just Dectivated a User. 
                                        </strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(uri_string()); ?>">Close</a>
                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModaResentMail" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Change Passwrod Link Sent</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            An Email with link to change password has been sent to user. 
                                        </strong>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModaPubs" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Published on Home Page</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            Selected Profile is Published on Home Page 
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/user_list_pro">Close</a> 

                        </div>
                    </div>
                </div>
            </div>



            <div class="modal fade" id="myModaUPubs" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Published on Home Page</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            Selected Profile is unPublished on Home Page 
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/user_list_pro">Close</a> 

                        </div>
                    </div>
                </div>
            </div>


            <div class="modal fade" id="myModaTops" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Published on Top Rated List</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            Selected Profile is Published on Top Rated Section of Home Page 
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/user_list_pro">Close</a> 

                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" id="myModauTops" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">UnPublished from Top Rated List</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            Selected Profile is UnPublished from Top Rated Section of Home Page 
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" href="<?php echo base_url(); ?>Admin/user_list_pro">Close</a> 

                        </div>
                    </div>
                </div>
            </div>



            <div class="modal fade" id="myModalDistance" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header"> 
                            <h4 class="modal-title">Distance Limit</h4>
                            <div class="title-imge">

                                <img src="<?php echo base_url(); ?>assets/img/logo.png">
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="modal-login-txt">


                                <div class="row">
                                    <div class="col-md-12">

                                        <strong>
                                            You have just Changed Distance Limit for Search Profiles. 
                                        </strong>


                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="modal-footer">
                            <a class="btn btn-default" data-dismiss="modal">Close</a>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Heading model -->
            <div class="modal fade" id="updateheadings" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="updateconheading">
                            <div class="modal-header"> 
                                <h4 class="modal-title" id="ctheading"></h4>                
                            </div>
                            <div class="modal-body">
                                <div class="modal-login-txt">
                                    <div class="row">
                                        <div class="col-md-12">                     
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Heading:</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" name="heading" id="heading" class="form-control" placeholder="Heading" value=""/>
<?php echo form_error('heading'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Sub-Heading:</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" name="subheading" id="subheading" class="form-control" placeholder="Sub Heading" value=""/>
<?php echo form_error('subheading'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="sectionid" value="" name="sectionid"/>
                                <div id="transmsg"></div>
                                <button type="submit" class="btn btn-success transsubmit">Update</button>
                                <a class="btn btn-default transclose" data-dismiss="modal">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Heading model end -->


            <!-- Create event model -->
            <div class="modal fade" id="creteevent" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form id="createnewevent" action="<?php echo base_url("Admin/createEvent"); ?>" method="post">
                            <div class="modal-header"> 
                                <h4 class="modal-title">Create Event</h4>                
                            </div>
                            <div class="modal-body">
                                <div class="modal-login-txt">
                                    <div class="row">
                                        <div class="col-md-12">                     
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Event Name:</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" name="name" id="" class="form-control" placeholder="Event Name" value=""/>
<?php echo form_error('name'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Event Date</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" name="eventdate" id="" class="form-control datepickerfuture" placeholder="Event Date" value=""/>
<?php echo form_error('eventdate'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Event Start Time</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" name="starttime" id="sttime" class="form-control timepicker" placeholder="Start Time" value=""/>
<?php echo form_error('eventdate'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Event End Time</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <input type="text" name="endtime" id="enddate" class="form-control timepicker" placeholder="End Time" value=""/>
<?php echo form_error('eventdate'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Event Details:</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <textarea name="evtdescription" class="form-control" placeholder="Event Details"></textarea>
<?php echo form_error('eventdate'); ?>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <label>Event Status:</label>
                                                </div>
                                                <div class="col-md-12 col-sm-12 col-xs-12">
                                                    <select class="form-control" name="status">
                                                        <option value="">Select Status</option>
                                                        <option value="1">Publish</option>
                                                        <option value="0">Unpublish</option>
                                                    </select>
<?php echo form_error('eventdate'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">            
                                <div id="evntsmsg"></div>
                                <button type="submit" class="btn btn-success evtsubmit">Create</button>
                                <a class="btn btn-default evtclose" data-dismiss="modal">Cancel</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- Create event model end -->
             <!--<script type="text/javascript" src="<php echo base_url("assets/js/jquery-3.1.1.min.js") ?>"></script>-->



            <script src="<?php echo base_url(); ?>assets/vendor/jquery/jquery.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url("assets/js/jquery.validate.js"); ?>"></script>
            <script type="text/javascript" src="<?php echo base_url("assets/js/sb-admin-custom.js") ?>"></script>

            <script type="text/javascript" src="<?php echo base_url("assets/datetimepicker/js/bootstrap-datetimepicker.js"); ?>" charset="UTF-8"></script> 
            <script src="<?php echo base_url(); ?>assets/js/bootstrap-toggle.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/bootstrap-confirmation.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendor/metisMenu/metisMenu.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendor/raphael/raphael.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/vendor/morrisjs/morris.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/data/morris-data.js"></script>
            <script src="<?php echo base_url(); ?>assets/dist/js/sb-admin-2.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/jquery.mCustomScrollbar.min.js"></script>
            <script src="<?php echo base_url(); ?>assets/js/bootstrap-confirmation.min.js"></script> 
            <script src="<?php echo base_url("assets/js/jquery.flexdatalist.js"); ?>"></script>
            
            <script src="<?php echo base_url("assets/js/custom.js"); ?>"></script>

            <script>
                var placeSearch, autocomplete;
                var componentForm = {
                    street_number: 'short_name',
                    route: 'long_name',
                    locality: 'long_name',
                    administrative_area_level_1: 'short_name',
                    country: 'long_name',
                    postal_code: 'short_name'
                };

                function initAutocomplete() {
                    autocomplete = new google.maps.places.Autocomplete(
                            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                            {types: ['geocode']});
                    //autocomplete.addListener('place_changed', fillInAddress);
                }

                function fillInAddress() {
                    var place = autocomplete.getPlace();
                    for (var component in componentForm) {
                        document.getElementById(component).value = '';
                        document.getElementById(component).disabled = false;
                    }
                    for (var i = 0; i < place.address_components.length; i++) {
                        var addressType = place.address_components[i].types[0];
                        //alert(addressType);
                        if (componentForm[addressType]) {
                            var val = place.address_components[i][componentForm[addressType]];
                            document.getElementById(addressType).value = val;
                        }
                    }
                }
                function geolocate() {

                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function (position) {
                            var geolocation = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            };
                            var circle = new google.maps.Circle({
                                center: geolocation,
                                radius: position.coords.accuracy
                            });
                            autocomplete.setBounds(circle.getBounds());
                        });
                    }
                }
            </script>

            <script type="text/javascript" src="<?php echo base_url("assets/js/autosugges/jquery.mockjax.js") ?>"></script>
            <script type="text/javascript" src="<?php echo base_url("assets/js/autosugges/bootstrap-typeahead.js") ?>"></script>
             <!--  <script type="text/javascript" src="<php echo base_url("assets/js/sb-admin-custom.js") ?>"></script>-->

            <script type="text/javascript">
                $('#user_email').typeahead({
                    displayField: 'name',
                    scrollBar: true,
                    ajax: {
                        url: '<?php echo base_url("Admin/userEmails"); ?>',
                        triggerLength: 1
                    }
                });
                $('#user_peremail').typeahead({
                    displayField: 'name',
                    scrollBar: true,
                    ajax: {
                        url: '<?php echo base_url("Admin/userPerEmails"); ?>',
                        triggerLength: 1
                    }
                });
                $('#subcribe_user_email').typeahead({
                    displayField: 'name',
                    scrollBar: true,
                    ajax: {
                        url: '<?php echo base_url("Admin/subscribeUserEmails"); ?>',
                        triggerLength: 1
                    }
                });
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
            async defer></script>
            <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
            async defer></script>
            

            <script  type="text/javascript">
                $(document).ready(function () {
                    jQuery('#export-to-csv').bind("click", function () {
                        var target = $(this).attr('id');
                        switch (target) {
                            case 'export-to-csv' :
                                $('#hidden-type').val(target);
                                // alert($('#signup_method').val());
                                $('#hidden_user_email').val($('#user_email').val());
                                $('#hidden_signup_method').val($('#signup_method').val());
                                $('#export-form').submit();
                                $('#hidden-type').val('');
                                break
                        }
                    });
                });

                $('[data-toggle=confirmation]').confirmation({
                    rootSelector: '[data-toggle=confirmation]',
                    container: 'body'
                });


                $("#clearfilterSubmit").click(function ()
                {
                    $('#hidden_user_email').val("");
                    $('#user_email').val("");
                    $('#hidden_signup_method').val("");
                    $('#signup_method').val("");
                    $('#clearfilter').val("yes");
                    $('#filterForm').submit();
                });

            </script>

            <script>
                /*$(document).ready(function() {
                 
                 // For the Second level Dropdown menu, highlight the parent	
                 $( ".dropdown-menu" )
                 .mouseenter(function() {
                 $(this).parent('li').addClass('active');
                 })
                 .mouseleave(function() {
                 $(this).parent('li').removeClass('active');
                 });
                 
                 });*/
            </script>
            <script type="text/javascript">
                $(document).ready(function () {
                    $('.navbar a.dropdown-toggle').on('click', function (e) {
                        var elmnt = $(this).parent().parent();
                        if (!elmnt.hasClass('nav')) {
                            var li = $(this).parent();
                            var heightParent = parseInt(elmnt.css('height').replace('px', '')) / 2;
                            var widthParent = parseInt(elmnt.css('width').replace('px', '')) - 10;

                            if (!li.hasClass('open'))
                                li.addClass('open')
                            else
                                li.removeClass('open');
                            //$(this).next().css('top', heightParent + 'px');
                            //$(this).next().css('left', widthParent + 'px');

                            return false;
                        }
                    });
                });
            </script>

             <script>
                $(document).ready(function () {
                    $('.notification').on('click', function (e) {
                
                
                    //alert("dsad");
                        $.ajax({
                        url: "<?php echo base_url(); ?>Review/fetch_review", // Controller URL
                        dataType: 'html',
                        success: function (data) {
                           // console.log(data);
                            $('.notificationdata').html(data);
                            
                        },
                        error: function (error) {
                           console.log(error);
                        } 
                    });
                });
                });


            </script>

            <script>
    $(document).ready(function() {
    $("input[name$='cars']").click(function() {
        var test = $(this).val();
        var checkval = $(this).val();
          //alert(checkval);
          if(checkval ==3)
          {
              $("#business_address").val('');
              $("#business_country").val('');
              $("#business_city").val('');
          }
          else
          {
              $("#autocomplete").val('');
          }

        $("div.desc").hide();
        $("#Cars" + test).show();
    });
 
     $("#business_country").change(function() {

        var level = $(this).val();
        //alert(level);
         if(level){
            $.ajax ({
                type: 'POST',
                url: '<?php echo base_url(); ?>Users/viewcity',
                data: { hps_level: '' + level + '' },
                success : function(htmlresponse) {
                    $('#business_city').html(htmlresponse);
                    //console.log(htmlresponse);
                }
            });
        }
     });

});
</script>

    </body>
</html>