<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Home</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/master.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.carousel.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap-datepicker.css'); ?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/owl.theme.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.mCustomScrollbar.css'); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/wickedpicker.min.css'); ?>">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
<style type="text/css">
nav#nav-top {
	background: #000 !important;
}
</style>
</head>
<body>
<!-- header start-->
  <header id="main_header_id">
    <div class="header-main">
      <nav class="navbar navbar-default navbar-fixed-top" id="nav-top">
        <div class="container"> 
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
            <a class="navbar-brand" href="#"><img src="<?php echo base_url('assets/img/logo.png'); ?>"></a> </div>
          
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right main-nav">
              <li><a href="#">Login</a></li>
              <li><a href="#">Register</a></li>
              <li class="good-place"><a href="#">Good Place <i class="fa fa-plus"></i></a></li>
            </ul>
          </div>
          <!-- /.navbar-collapse --> 
        </div>
        <!-- /.container-fluid --> 
      </nav>
    </div>
  </header>
<!-- header end-->

<?php echo $contents; ?>

<!-- Footer start -->
  <footer class="main_footer">
    <div class="footer_new_letter">
      <div class="news-letter">
        <div class="container">
          <div class="new_letter_main">
            <div class="row">
              <div class="col-sm-3">
                <div class="news_letter_heading">
                  <h2>SIGN UP OUR NEWSLETTER</h2>
                </div>
              </div>
              <div class="col-sm-9">
                <div class="new_letter_input">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Email">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">SUBSCRIBE</button>
                    </span> </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="f_middle">
          <div class="col-md-4 col-sm-4">
            <div class="footer-logo"> <img src="img/logo.png"> </div>
            <div class="f_heading">
              <p> Otourdemoi.Pro is not a directory of professionals as it already exists ...
                It is a unique community aimed at bringing together professionals from the private sector by promoting exchanges and strengthening the ties of trust and proximity.
                Together, let us act to preserve quality products and services, at the right price and a stone's throw from our home.
                Choosing crafts means choosing quality and trust! </p>
            </div>
          </div>
          <div class="col-md-8 col-sm-8">
            <div class="working_hours">
              <div class="col-sm-4">
                <div class="f_heading">
                  <h3>Information</h3>
                </div>
                <ul>
                  <li><a href="#">About</a></li>
                  <li><a href="#">Terms of Use</a></li>
                  <li><a href="#">Legal Notice</a></li>
                  <li><a href="#">Earn money with sponsorship</a></li>
                </ul>
              </div>
              <div class="col-sm-4">
                <div class="f_heading">
                  <h3>Discover</h3>
                </div>
                <ul>
                  <li><a href="#">Frequently Asked Questions</a></li>
                  <li><a href="#">Why Create a User Account</a></li>
                  <li><a href="#">Support the Fondation Hopitaux de France</a></li>
                  <li><a href="#">Contact Us</a></li>
                </ul>
              </div>
              <div class="col-sm-4">
                <div class="f_heading">
                  <h3>Professional Accounts</h3>
                </div>
                <ul>
                  <li><a href="#">AWhy Create a PRO Account</a></li>
                  <li><a href="#">Claim your business page</a></li>
                  <li><a href="#">Advice to attract more customers</a></li>
                  <li><a href="#">Boost your profile by advertising</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
          <div class="copyright">
            <div class="col-sm-6">
              <div class="footer-link-social">
                <ul>
                  <li><a href=""><i class="fa fa-twitter"></i></a></li>
                  <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                  <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                  <li><a href=""><i class="fa fa-facebook"></i></a></li>
                  <li><a href=""><i class="fa fa-instagram"></i></a></li>
                </ul>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="f_follow">Copyright © 2017</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </footer>
<!-- Footer end -->
  <div id="chat-modal" class="modal chat-modal fade" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
          <div class="modal-header">
              
              <div class="prof-msg-details">
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="msg-pro-pic-sec">
                              <div class="msg-profile-pic">
                                  <i class="fa fa-user"></i>
                              </div>
                              <div class="msh-prof-details">
                                  <a href="#"><h3>Lloyd Hamlet</h3></a>
                              </div>
                              <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="modal-body mCustomScrollbar" data-mcs-theme="dark">
              
              <div class="msg-user">   
                  <div class="row">
                      <div class="col-md-12">
                          <div class="msg-user-sec">
                              <div class="msg-user-pro-pic">
                                <i class="fa fa-user"></i>
                              </div>
                              <div class="msg-user-main-msg">
                                  <p>Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>
                                  <p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>22-11-17</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>11:00 PM</span></p>
                              </div>
                          </div>
                      </div>
                  </div>    
              </div>

              <div class="msg-admin">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="msg-user-sec">
                              <div class="msg-user-pro-pic">
                                  <i class="fa fa-user"></i>
                              </div>
                              <div class="msg-user-main-msg">
                                  <p>industry's standard dummy  scrambled it to make a type specimen</p>
                                  <p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>22-11-17</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>11:00 PM</span></p>
                              </div>
                             
                          </div>
                      </div>
                  </div>
              </div>
              <div class="msg-user">   
                  <div class="row">
                      <div class="col-md-12">
                          <div class="msg-user-sec">
                              <div class="msg-user-pro-pic">
                                  <i class="fa fa-user"></i>
                              </div>
                              <div class="msg-user-main-msg">
                                  <p>search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                                  <p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>22-11-17</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>11:00 PM</span></p>
                              </div>
                          </div>
                      </div>
                  </div>    
              </div>
              <div class="msg-user">   
                  <div class="row">
                      <div class="col-md-12">
                          <div class="msg-user-sec">
                              <div class="msg-user-pro-pic">
                                  <i class="fa fa-user"></i>
                              </div>
                              <div class="msg-user-main-msg">
                                  <p>industry's standard dummy  scrambled it to make a type specimen</p>
                                  <p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>22-11-17</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>11:00 PM</span></p>
                              </div>
                          </div>
                      </div>
                  </div>    
              </div>
              <div class="msg-user">   
                  <div class="row">
                      <div class="col-md-12">
                          <div class="msg-user-sec">
                              <div class="msg-user-pro-pic">
                                  <i class="fa fa-user"></i>
                              </div>
                              <div class="msg-user-main-msg">
                                  <p>search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like)</p>
                                  <p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>22-11-17</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>11:00 PM</span></p>
                              </div>
                          </div>
                      </div>
                  </div>    
              </div>
              <div class="msg-admin">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="msg-user-sec">
                              <div class="msg-user-pro-pic">
                                  <i class="fa fa-user"></i>
                              </div>
                              <div class="msg-user-main-msg">
                                  <p>industry's standard dummy  scrambled it to make a type specimen</p>
                                  <p class="msg-date-m"><i class="fa fa-calendar" aria-hidden="true"></i> <span>22-11-17</span></p><p class="msg-time-m"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>11:00 PM</span></p>
                              </div>
                              
                          </div>
                      </div>
                  </div>
              </div>

          </div>
          <div class="modal-footer">
              <div class="row">
                  <div class="col-md-12">
                      <div class="reply-sec-msg">
                          <form>
                              <div class="form-group">
                                <textarea class="form-control mCustomScrollbar" wrap="off" rows="2" id="comment" placeholder="Enter Your Message..."></textarea>
                              </div>
                          </form>
                          <button type="submit" class="btn btn-default">Reply</button>
                      </div>
                  
                  </div>
              </div>
          </div>
      </div>

    </div>
  </div>

  <div class="side-footer-link">
    <ul>
      <li><i class="fa fa-facebook"></i>
        <div class="icon-detail"> <a href="#">Facebook</a> </div>
      </li>
      <li><i class="fa fa-twitter"></i>
        <div class="icon-detail"> <a href="#">Twitter</a> </div>
      </li>
      <li><i class="fa fa-instagram"></i>
        <div class="icon-detail"> <a href="#">Instagram</a> </div>
      </li>
    </ul>
  </div>
  <div class="modal fade" id="myModal-pic" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body"> </div>
      </div>
      <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
  </div>
  <!-- /.modal --> 

  <script src="<?php echo base_url('assets/js/jquery-3.1.1.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/bootstrap.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/owl.carousel.min.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/photo-gallery.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/jquery.mCustomScrollbar.min.js'); ?>"></script> 
  <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap-datepicker.js'); ?>"></script> 
  <script src="<?php echo base_url('assets/js/wickedpicker.min.js'); ?>"></script> 
  <script>
      // initialize input widgets first
      

      $('#datepairExample .date').datepicker({
          'format': 'yyyy-m-d',
          'autoclose': true
      });

      
  </script> 
  <script type="text/javascript">
    $('.timepicker').wickedpicker();
  </script> 
  <script type="text/javascript">
  jQuery(window).scroll(function() {   
         if (jQuery(window).scrollTop() >=50) { 
            if(jQuery('#nav-top').hasClass( "navi" ))
    {
    //jQuery('#header').removeClass("fixed-theme");
    }
    else
    {
    jQuery('#nav-top').addClass("navi");
    }
         }
  else{
  jQuery('#nav-top').removeClass("navi");
  }
     });

  </script> 
  <script>
  $(document).ready(function() {

    $(".owl-demo").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 300,
        paginationSpeed : 400,
        pagination : true,
        singleItem:true

        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });


  });
  </script>
  <script type="text/javascript">
    $("button#av-btn").click(function(){
      $(".act-valid-sec").slideToggle();
  });
  </script>
<script type="text/javascript">
<?php /*?>    $('.dash-link-main>ul.panel-tabs li a').click(function (e) {
    $('.dash-link-main>ul.panel-tabs li.active').removeClass('active')
    $(this).parent('li').addClass('active')
})
  </script>
  <script type="text/javascript">
    $('.dash-link-sub>ul.panel-tabs li a').click(function (e) {
    $('.dash-link-sub>ul.panel-tabs li.active').removeClass('active')
    $(this).parent('li').addClass('active')
})<?php */?>



  </script>
  
  <script>
$(document).ready(function() {
    $("ul.tabs-menu.dash-link-main a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $("div#tabs-container>.tab>.tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
});
  </script>
 
   <script>
$(document).ready(function() {
    $(".dash-link-sub .tabs-menu a").click(function(event) {
        event.preventDefault();
        $(this).parent().addClass("current");
        $(this).parent().siblings().removeClass("current");
        var tab = $(this).attr("href");
        $("div#tabs-container-sub .tab .tab-content").not(tab).css("display", "none");
        $(tab).fadeIn();
    });
});
  </script>
  
  <body>
  </body>
  </html>

