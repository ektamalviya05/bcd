<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="185961027889-o82v14cenj8uvcnptkq2vd4ksim1kkth.apps.googleusercontent.com">
<title>Home</title>
<!--<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/custom.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/master.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/font-awesome.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.theme.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
-->

  <!-- Bootstrap Core CSS -->

    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/master.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.theme.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/jquery.flexdatalist.css");?>">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">

<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> 

 <script type="text/javascript">
     /* var onloadCallback = function() {
        grecaptcha.render('html_element', {
          'sitekey' : '6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M'
        });
      }; */   </script>

<script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>

<header id="main_header_id">
  
    
    <div class="header-main">
       <nav class="navbar navbar-default navbar-fixed-top" id="nav-top">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
               <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            </div>
        
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              
              
              <ul class="nav navbar-nav navbar-right main-nav">
                <?php 

                      //print_r($user);

                      if($this->session->userdata('user_id') && $this->session->userdata('user_id') >0)

                      {
                        $userdetails = $this->db->select("user_firstname")->get_where("users", array("user_id"=>$this->session->userdata("user_id")));
                        ?>
                        <li class="user-intro dropdown">
                          <a href="<?php echo base_url("Users/account"); ?>" class="dropdown-toggle" data-toggle="dropdown">Hello <?php echo $userdetails->result()[0]->user_firstname; ?>
                          <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#"><i class="fa fa-envelope"></i>Messages<span class="counter-msg">10</span></a></li>
                            <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications<span class="counter-not">10</span></a></li>
                            <?php  
                              $social_login = $this->session->userdata("social_login");
                              if(!empty($social_login)){
                                if($social_login['is_social'] && $social_login['social_type'] == "facebook"){
                                ?>
                                <li><a href="javascript:;" onclick="Logout();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
                                <?php
                                }
                                else if($social_login['is_social'] && $social_login['social_type'] == "google"){
                                  ?>
                                  <li><a href="javascript:void(0);" onClick="signOut();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
                                  <?php
                                }
                              }else{
                              ?>
                              <li><a href="<?php echo base_url();?>Users/logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
                              <?php
                              }
                              ?>
                          </ul>
                        </li>
                        <?php
                      }
                        else{              ?>

                    <li><a href="javascript:;" data-toggle="modal" data-target="#myModal2">Login</a>

                                </li>

                    <li><a href="javascript:;" data-toggle="modal" data-target="#myModal">Register</a>



                    </li>

                    <?php



}

                    ?>
              <li class="good-place"><a href="#">Good Place <i class="fa fa-plus"></i></a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </div>
</header>




<div class="main-reg-content">
  <section class="reg-main-sec">
    <div class="container">
          <?php echo $contents;?>
    </div>
  </section>
</div>





<footer class="main_footer">
  <div class="footer_new_letter">
      <div class="news-letter">
        <div class="container">
        <div class="new_letter_main">
        <form action="<?php echo base_url("Users/subscription"); ?>" method="Post" name="mySubs">

                <div class="row">
                    <div class="col-sm-3">
                        <div class="news_letter_heading">
                            <h2>SIGN UP OUR NEWSLETTER</h2>
                        </div>
                    </div>
                    <div class="col-sm-9">
                        <div class="new_letter_input">
                          <div class="input-group">
                       <input required="" type="text" class="form-control" name="user_email" placeholder="Enter Email">
                       <input type="text" class="form-control" placeholder="Enter Postal Code" name="user_postcode">

                      <span class="input-group-btn">
                        <button name="subscribeSubmit" class="btn btn-default" type="submit" value="Subscribe Submit">SUBSCRIBE</button>
                      </span>
                  </div>
                  </div>
                    </div>
                </div>

                </form>
            </div>
        </div>
      </div>
    </div>    
    <div class="container">
        
            
        <div class="row">
            <div class="f_middle">
              <div class="col-md-4 col-sm-4">
                <div class="footer-logo">
                <img src="<?php echo base_url();?>assets/img/logo.png">
              </div>
                  <div class="f_heading">
                      <?php
                      $footerart = $this->db->get_where("site_contents", array("content_id"=>11));
                      foreach($footerart->result() as $ft){
                        echo $ft->content;
                      }
                      ?>
                    </div>
                </div>
               
                
                <div class="col-md-8 col-sm-8">

                  

                    <div class="working_hours">

                      <div class="col-sm-4">

                        <div class="f_heading">

                          <h3>Information</h3>

                        </div>

                        <ul>
                          <?php
                            $Information = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>1, "pg_status"=>1));
                            foreach($Information->result() as $info){
                              ?>
                              <li><a href="<?php echo base_url("Pages/content/$info->pg_meta_tag"); ?>"><?php echo $info->pg_title; ?></a></li>
                              <?php
                            }
                          ?>
                        </ul>

                      </div>

                        <div class="col-sm-4">

                          <div class="f_heading">

                          <h3>Discover</h3>

                        </div> 

                          <ul>

                            <?php
                              $discover = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>2, "pg_status"=>1));
                              foreach($discover->result() as $disc){
                                ?>
                                <li><a href="<?php echo base_url("Pages/content/$disc->pg_meta_tag"); ?>"><?php echo $disc->pg_title; ?></a></li>
                                <?php
                              }
                            ?>

                            </ul>

                        </div>

                        <div class="col-sm-4">

                          <div class="f_heading">

                          <h3>Professional Accounts</h3>

                        </div> 

                        <ul>
                          <?php
                            $professional = $this->db->order_by("pg_display_number", "ASC")->get_where("otd_page_contents", array("pg_cat"=>3, "pg_status"=>1));
                            foreach($professional->result() as $prof){
                              ?>
                              <li><a href="<?php echo base_url("Pages/content/$prof->pg_meta_tag"); ?>"><?php echo $prof->pg_title; ?></a></li>
                              <?php
                            }
                          ?>
                        </ul>
                      </div>

                    </div>

                </div>
            </div>
        </div>
            
                
    </div>
    <div class="footer-bottom">
      <div class="container">
        <div class="row">
              <div class="copyright">
                <div class="col-sm-6">
                    <div class="footer-link-social">
                      <ul>
                        <li><a href=""><i class="fa fa-twitter"></i></a></li>
                        <li><a href=""><i class="fa fa-pinterest"></i></a></li>
                        <li><a href=""><i class="fa fa-google-plus"></i></a></li>
                        <li><a href=""><i class="fa fa-facebook"></i></a></li>
                        <li><a href=""><i class="fa fa-instagram"></i></a></li>
                      </ul>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="f_follow">Copyright © 2017</div>
                  </div>
              </div>
          </div>
      </div>
    </div>
</footer>
<div class="side-footer-link">
  <?php
  $facebook = $this->db->get_where("otd_social_links", array("sc_id"=>1))->row_array();
  $twitter = $this->db->get_where("otd_social_links", array("sc_id"=>2))->row_array();
  $instagram = $this->db->get_where("otd_social_links", array("sc_id"=>3))->row_array();
?>
  <ul>

    <li><i class="fa fa-facebook"></i>

    <div class="icon-detail">

      <a href="<?php echo $facebook['sc_links']; ?>">Facebook</a>

    </div>

    </li>

    <li><i class="fa fa-twitter"></i>

    <div class="icon-detail">

      <a href="<?php echo $twitter['sc_links']; ?>">Twitter</a>

    </div>

    </li>

    <li><i class="fa fa-instagram"></i>

    <div class="icon-detail">

      <a href="<?php echo $instagram['sc_links']; ?>">Instagram</a>

    </div>

    </li>

  </ul>
</div>



<!-- Modal -->

<div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        
        <h4 class="modal-title">Login To My Account</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <h2>User Login</h2>
          <form action="<?php echo base_url();?>Users/login" method="post" name="myLogin">
            <div class="row">
              <div class="social-media-login">
                <a href="javascript:;" class="btn btn-default fb-so" onClick="Login()"><i class="fa fa-facebook"></i>Login with facebook</a>                
                <div class="g-signin2" data-onsuccess="onSignIn">Gmail Login</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="email" required="" class="form-control" name="user_email" placeholder="Email"  value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="password" class="form-control" name="user_password" placeholder="Password" required>
                  <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="submit" name="loginSubmit" class="btn-primary" value="Login"/>
                </div>
              </div>
              <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Forgot Password</button>
              <button type="button" class="btn btn-default new-acc-m" data-dismiss="modal" data-toggle="modal" data-target="#myModal">New Account</button>
            </div>


          </form>
        </div>
      </div>
      <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>










 <!-- Modal -->

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    
    <!-- Modal content-->
    
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      
        <h4 class="modal-title">Create My Account</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="select-acount">
          <div class="select-sub">
            <img src="<?php echo base_url("assets/img/loginicon.png"); ?>">
            <h2>Create Personal Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-default" href="<?php echo base_url();?>Users/registration">Select</a>
              <a href="<?php echo base_url("Pages/readMore/personal"); ?>" class="learn-mre">Learn more</a>
            </div>
          </div>
          <div class="select-sub">
            <img src="<?php echo base_url("assets/img/rightimgdiv.png"); ?>">
            <h2>Create Professional Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration">Select</a>
              <a href="<?php echo base_url("Pages/readMore/professional"); ?>" class="learn-mre">Learn more</a>
            </div>
          </div>
        </div>
        <div class="subscription">

          <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="subscribe_form">
            <div class="newsletter-main-head">
              <div class="news-letter-head">
                <h4>To receive the local newsletter of your new Otourdemoi newsletter</h4>
                <p>Thank you for informing:</p>
            
              <div class="row news-letter-form">
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" required="" name="user_email" class="inputbox">
                </div>
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                </div>
                <div class="col-md-2">
                  <input type="submit" name="subscribeSubmit" onClick="" value="Subscribe" class="submit-main">
                </div>
              </div>
              <div class="row agree-sec">
                <div class="col-md-12">
                  <input type="checkbox" title="Terms and Conditions" required="" name="terms">
                  <span>I agree to receive communications from the companies and partners of Otourdemoi</span>
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>
        
        <!-- --> 
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<!--     Forget Password Model -->



<div class="modal fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
        
        <h4 class="modal-title">Forgot Password</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          <form action="<?php echo base_url();?>Users/forgetpassword" method="post">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" class="form-control" name="user_email" placeholder="Enter Your Email" required>
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <input type="submit" name="passwordSubmit" class="btn-primary" value="Send Password Request"/>
                </div>
              </div>
            </div>


          </form>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--     Forget Password Model -->



<div class="modal fade" id="myModaComp" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Business Already Registered</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
              Your business is already available on our site, click 
              <a style="color: #000!important;" href="<?php echo base_url();?>Users/login">HERE</a> to view the profile and claim your business.
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>






<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script>
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/1.12.4/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/js/owl.carousel.min.js"></script>
<script src="<?php echo base_url("assets/js/jquery.flexdatalist.js");?>"></script>
<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
<script src="<?php echo base_url("assets/js/custom.js");?>"></script>

<script type="text/javascript">
jQuery(window).scroll(function() {   
       if (jQuery(window).scrollTop() >=50) { 
          if(jQuery('#nav-top').hasClass( "navi" ))
  {
  //jQuery('#header').removeClass("fixed-theme");
  }
  else
  {
  jQuery('#nav-top').addClass("navi");
  }
       }
else{
jQuery('#nav-top').removeClass("navi");
}
   });

</script>

<script>
$(document).ready(function() {

  $(".owl-demo").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      pagination : true,
      singleItem:true

      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

  });


});


</script>

<script>
     var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        // Create the autocomplete object, restricting the search to geographical
        // location types.
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode'], componentRestrictions: {country: "fr"}});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        // Get the place details from the autocomplete object.
        var place = autocomplete.getPlace();

        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        // Get each component of the address from the place details
        // and fill the corresponding field on the form.
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }

      // Bias the autocomplete object to the user's geographical location,
      // as supplied by the browser's 'navigator.geolocation' object.
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script>

<!-- Social Login -->
  <script type="text/javascript">
      var flag=false;
      // This is called with the results from from FB.getLoginStatus().
      
      function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          getUserInfo();
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else {
          // The person is not logged into Facebook, so we're not sure if
          // they are logged into this app or not.
          /*document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';*/
        }
      }

      // This function is called when someone finishes with the Login
      // Button.  See the onlogin handler attached to it in the sample
      // code below.
      function checkLoginState() {
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
      }

    
      // Load the SDK asynchronously

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1546847162054270', //1068492099877422 App ID (1776946472525281)
          // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true,  // parse XFBML
          version    : 'v2.8'
        });
        
        
        FB.Event.subscribe('auth.authResponseChange', function(response) 
        {
          if (response.status === 'connected') 
          {
            console.log("Connected to Facebook");
            //SUCCESS
            
          }  
          else if (response.status === 'not_authorized') 
          {
            console.log("Failed to Connect");

          //FAILED
          } else 
          {
          console.log("Logged Out");
            

            //UNKNOWN ERROR
          }
        }); 
      
      };
      
      function Login(){
        //alert(1)
        FB.login(function(response) {
          if (response.authResponse) 
          {
            getUserInfo();
          } else 
          {
             console.log('User cancelled login or did not fully authorize.');
          }
        }, {scope: 'email,user_birthday,user_location'});  
      }

      function getUserInfo() {//alert(1)      
          FB.api('/me',{fields: 'id,email, first_name, last_name, picture.width(800).height(800),gender'}, function(response) {
      
            console.log(response);
            $.ajax({
              method: "POST",
              url: "<?php echo base_url();?>Users/fblogin",
              dataType: 'json',
              data: {'id':response.id, 'first_name':response.first_name, 'last_name':response.last_name, 'email': response.email, 'picture': response.picture.data.url, 'gender': response.gender},
              success:function(data){              
                if(data.flag==1){
                   window.location.href = '<?php echo base_url('Users/account');?>';                 
                   return false;  
                }              
              }
            }); 
          });
      }

      function Logout()
      {
        FB.logout(function(){ window.location.replace("<?php echo base_url(); ?>Users/logout");});
      }

      // Load the SDK asynchronously
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));


      ////////////////////////////////////////Gmail Login////////////////////////////////////////////////

      /*Google signup*/

      function onSignIn(googleUser) {
        
        BasicProfile = googleUser.getBasicProfile();

        var social_id = BasicProfile.getId();      
        var first_name = BasicProfile.getGivenName();
        var last_name = BasicProfile.getFamilyName();
        var image_path = BasicProfile.getImageUrl();
        var email = BasicProfile.getEmail();

        // var userId = profile.getId(); // Do not send to your backend! Use an ID token instead.
        // var userName = profile.getName();
        // var userEmail =  profile.getEmail();
        // var picture = profile.getImageUrl();
        //var DOB = profile.getBirthday()

        if(flag==false){
           
          $.ajax({
              method:"POST",
              url:'<?php echo base_url();?>Users/googleLogin',
              dataType: 'json',
              data: {'id':social_id ,'email': email, 'first_name':first_name, 'last_name':last_name, 'picture':image_path},
              success:function(data){
                if(data.flag == 1){                
                  window.location.href = '<?php echo base_url('Users/account');?>';                
                  flag=true;
                  return false;                        
                }else{
                      console.log(data); 
                }
              }
          });
        } 
      }

      $("glog").click(function(){
          gp_signOut();
      });

      function gp_signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
              console.log('User signed out.');
          });
      }
  </script>


  <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script> 
  <script>
    $("glog").click(function(){
        gp_signOut();
    });

    function gp_signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

    function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
        disassociate();
             $.ajax({
              method: "POST",
              dataType:"json",
              url: "<?php echo base_url();?>Users/socialLogout",
              success:function(data){
                 if(data.status == 200){
                   document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>";                     
                 }
              }
              }); 
      
        // document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>userlogin";
      });
    }
    
    function onLoad() {
      gapi.load('auth2', function() {
        gapi.auth2.init();
      });
    }
    function disassociate() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect().then(function () {
            console.log('User disconnected from association with app.');
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////// 
  </script> 
<!-- Social Login end -->
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script> 


<script type="text/javascript">
    $(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='myLogin']").validate({

    // Specify validation rules
    rules: {
      user_email: {
        required: true,
        email: true
      },
      user_password: {
        required: true,
        minlength: 6
      }
    },
    // Specify validation error messages
    messages: {
      user_password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
      user_email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    


<script type="text/javascript">
    $(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "registration"
  $("form[name='mySubs']").validate({

    // Specify validation rules
    rules: {
      user_email: {
        required: true,
        email: true
      }
     
    },
    // Specify validation error messages
    messages: {
      user_password: {
        required: "Please provide a password",
        minlength: "Your password must be at least 6 characters long"
      },
      user_email: "Please enter a valid email address"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});

</script>    

</body>
</html>
