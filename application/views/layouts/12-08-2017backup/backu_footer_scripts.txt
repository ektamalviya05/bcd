//inner layout


<script>
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
   //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }

        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }
      function geolocate() {
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }

    </script>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script>

<!-- Social Login -->
  <script type="text/javascript">
      var flag=false;
      // This is called with the results from from FB.getLoginStatus().
      
      function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
          // Logged into your app and Facebook.
          getUserInfo();
        } else if (response.status === 'not_authorized') {
          // The person is logged into Facebook, but not your app.
          document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
        } else {
          /*document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';*/
        }
      }

      function checkLoginState() {
        FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
        });
      }

    
      // Load the SDK asynchronously

      window.fbAsyncInit = function() {
        FB.init({
          appId      : '1546847162054270', //1068492099877422 App ID (1776946472525281)
          // Channel File
          status     : true, // check login status
          cookie     : true, // enable cookies to allow the server to access the session
          xfbml      : true,  // parse XFBML
          version    : 'v2.8'
        });
        
        
        FB.Event.subscribe('auth.authResponseChange', function(response) 
        {
          if (response.status === 'connected') 
          {
            console.log("Connected to Facebook");
            //SUCCESS
            
          }  
          else if (response.status === 'not_authorized') 
          {
            console.log("Failed to Connect");

          //FAILED
          } else 
          {
          console.log("Logged Out");
            

            //UNKNOWN ERROR
          }
        }); 
      
      };
      
      function Login(){
        //alert(1)
        FB.login(function(response) {
          if (response.authResponse) 
          {
            getUserInfo();
          } else 
          {
             console.log('User cancelled login or did not fully authorize.');
          }
        }, {scope: 'email,user_birthday,user_location'});  
      }

      function getUserInfo() {//alert(1)      
          FB.api('/me',{fields: 'id,email, first_name, last_name, picture.width(800).height(800),gender'}, function(response) {
      
            console.log(response);
            $.ajax({
              method: "POST",
              url: "<?php echo base_url();?>Users/fblogin",
              dataType: 'json',
              data: {'id':response.id, 'first_name':response.first_name, 'last_name':response.last_name, 'email': response.email, 'picture': response.picture.data.url, 'gender': response.gender},
              success:function(data){              
                if(data.flag==1){
                   window.location.href = '<?php echo base_url('Users/account');?>';                 
                   return false;  
                }              
              }
            }); 
          });
      }

      function Logout()
      {
        FB.logout(function(){ window.location.replace("<?php echo base_url(); ?>Users/logout");});
      }

      // Load the SDK asynchronously
      (function(d){
         var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
         if (d.getElementById(id)) {return;}
         js = d.createElement('script'); js.id = id; js.async = true;
         js.src = "//connect.facebook.net/en_US/all.js";
         ref.parentNode.insertBefore(js, ref);
       }(document));


      ////////////////////////////////////////Gmail Login////////////////////////////////////////////////

      /*Google signup*/

      function onSignIn(googleUser) {
        
        BasicProfile = googleUser.getBasicProfile();

        var social_id = BasicProfile.getId();      
        var first_name = BasicProfile.getGivenName();
        var last_name = BasicProfile.getFamilyName();
        var image_path = BasicProfile.getImageUrl();
        var email = BasicProfile.getEmail();

        // var userId = profile.getId(); // Do not send to your backend! Use an ID token instead.
        // var userName = profile.getName();
        // var userEmail =  profile.getEmail();
        // var picture = profile.getImageUrl();
        //var DOB = profile.getBirthday()

        if(flag==false){
           
          $.ajax({
              method:"POST",
              url:'<?php echo base_url();?>Users/googleLogin',
              dataType: 'json',
              data: {'id':social_id ,'email': email, 'first_name':first_name, 'last_name':last_name, 'picture':image_path},
              success:function(data){
                if(data.flag == 1){                
                  window.location.href = '<?php echo base_url('Users/account');?>';                
                  flag=true;
                  return false;                        
                }else{
                      console.log(data); 
                }
              }
          });
        } 
      }

      $("glog").click(function(){
          gp_signOut();
      });

      function gp_signOut() {
          var auth2 = gapi.auth2.getAuthInstance();
          auth2.signOut().then(function () {
              console.log('User signed out.');
          });
      }
  </script>


  <script src="https://apis.google.com/js/platform.js?onload=onLoad" async defer></script> 
  <script>
    $("glog").click(function(){
        gp_signOut();
    });

    function gp_signOut() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });
    }

    function signOut() {
      var auth2 = gapi.auth2.getAuthInstance();
      auth2.signOut().then(function () {
        console.log('User signed out.');
        disassociate();
             $.ajax({
              method: "POST",
              dataType:"json",
              url: "<?php echo base_url();?>Users/socialLogout",
              success:function(data){
                 if(data.status == 200){
                   document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>";                     
                 }
              }
              }); 
      
        // document.location.href = "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=<?php echo base_url();?>userlogin";
      });
    }
    
    function onLoad() {
      gapi.load('auth2', function() {
        gapi.auth2.init();
      });
    }
    function disassociate() {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.disconnect().then(function () {
            console.log('User disconnected from association with app.');
        });
    }
    ////////////////////////////////////////////////////////////////////////////////////////////// 
  </script> 
<!-- Social Login end -->
<script>
    $(document).ready(function () {
        $('.dropdown-toggle').dropdown();
    });
</script> 
</body>
</html>


