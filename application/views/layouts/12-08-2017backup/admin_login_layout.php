<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Pradeep Verma">
    <title>Listing - <?php echo $title;?></title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
<style>
    section.admin-log {
        height: 100vh;
        padding: 25vh 0;
        font-family: 'Source Sans Pro', sans-serif !important
    }
    .login {
        background: #fff;
        padding: 30px;
        max-width: 450px;
        margin: 0 auto;
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.17);
    }
    .login h2 {
        margin: 0 0 22px;
        font-size: 25px;
        position: relative;
        text-transform: uppercase;
        font-weight: 600;
        letter-spacing: 1.2px;
        padding: 0 0 10px;
    }
    .login input, .login input:focus {
        height: 40px;
        border-radius: 0;
        padding: 8px 0;
        border: none !important;
        margin-bottom: 20px;
        color: #333;
        border-bottom: 1px solid rgba(0, 0, 0, 0.15) !important;
        box-shadow: none;
        outline: none;
    }
    .login input.btn-primary {
        color: #fff;
        padding: 5px 20px;
        text-transform: uppercase;
        text-align: center;
        background: #ff8b00 !important;
        border: none !important;
        margin: 0;
    }
    .login .form-group {
        margin: 0 !important;
    }
    .login h2:after {
        background: #ff8b00;
        height: 2px;
        width: 50px;
        position: absolute;
        left: 0;
        bottom: -6px;
        content: '';
    }
    .login input:-webkit-autofill {
        border: 1px solid #ccc;
        -webkit-box-shadow: inset 0 0 0px 9999px white;
        outline: none;
    }
    .login input:focus,
    .login input:-webkit-autofill:focus {
        border-color: #66afe9;
        -webkit-box-shadow: inset 0 0 0px 9999px white,
                            0 0 8px rgba(102, 175, 233, 0);
                            outline: none;
    }
</style>
</head>
<body>
    <section class="admin-log">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="login"">
                         <?php echo $contents;?>
                    </div>                    
                </div>
            </div>
        </div>
    </section>

<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script>
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script>

</body>
</html>