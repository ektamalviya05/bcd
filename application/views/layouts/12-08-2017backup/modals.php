<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Login To My Account</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <h2>User Login</h2>
          <form action="<?php echo base_url();?>Users/login" method="post">
            <div class="row">
              <div class="social-media-login">
                <a href="javascript:;" class="btn btn-default fb-so" onClick="Login()"><i class="fa fa-facebook"></i>Login with facebook</a>                
                <div class="g-signin2" data-onsuccess="onSignIn">Gmail Login</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="email" class="form-control" name="user_email" placeholder="Email"  value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="password" class="form-control" name="user_password" placeholder="Password" required>
                  <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="submit" name="loginSubmit" class="btn-primary" value="Login"/>
                </div>
              </div>
              <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Forgot Password</button>
              <button type="button" class="btn btn-default new-acc-m" data-dismiss="modal" data-toggle="modal" data-target="#myModal">New Account</button>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
       
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


 <!-- Modal -->

<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog"> 
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Create My Account</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="select-acount">
          <div class="select-sub">
            <img src="<?php echo base_url('assets/img/loginicon.png'); ?>">
            <h2>Create Personal Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-default" href="<?php echo base_url();?>Users/registration">Select</a>
              <a href="<?php echo base_url("Pages/readMore/personal"); ?>" class="learn-mre">Learn more</a>
            </div>
          </div>
          <div class="select-sub">
            <img src="<?php echo base_url('assets/img/rightimgdiv.png'); ?>">
            <h2>Create Professional Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration">Select</a>
              <a href="<?php echo base_url("Pages/readMore/professional"); ?>" class="learn-mre">Learn more</a>
            </div>
          </div>
        </div>

      <div class="subscription">
          <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="subscribe_form">
            <div class="newsletter-main-head">
              <div class="news-letter-head">
                <h4>To receive the local newsletter of your new Otourdemoi newsletter</h4>
                <p>Thank you for informing:</p>
            
              <div class="row news-letter-form">
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" required="" name="user_email" class="inputbox">
                </div>
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                </div>
                <div class="col-md-2">
                  <input type="submit" name="subscribeSubmit" onClick="" value="Subscribe" class="submit-main">
                </div>
              </div>
              <div class="row agree-sec">
                <div class="col-md-12">
                  <input type="checkbox" title="Terms and Conditions" required="" name="terms">
                  <span>I agree to receive communications from the companies and partners of Otourdemoi</span>
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<!--     Forget Password Model -->
<div class="modal fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Forgot Password</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <form action="<?php echo base_url();?>Users/forgetpassword" method="post">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" class="form-control" name="user_email" placeholder="Enter Your Email" required>
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <input type="submit" name="passwordSubmit" class="btn-primary" value="Send Password Request"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

