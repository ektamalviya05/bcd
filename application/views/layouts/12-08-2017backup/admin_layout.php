<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Pradeep Verma">
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>Listing - <?php echo $title;?></title>
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-toggle.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- <link href="<?php echo base_url();?>assets/dist/css/sb-admin-2.css" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/admin-dash.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/jquery.mCustomScrollbar.css">
    <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/jquery.flexdatalist.css");?>">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
</head>
 
<body>

<div id="wrapper">

     <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top curs-admin" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>Admin"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                
                 <?php  if($this->session->userdata('isUserLoggedIn')){
                            ?>
                       

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> John Smith <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-envelope"></i> Inbox</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url();?>Admin/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>

                    </ul>
                </li>


                <?php
            }
            ?>
            </ul>
            <div class="collapse navbar-collapse navbar-ex1-collapse admin-nav">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?php if( $this->uri->segment(1) == '' || $this->uri->segment(1) == 'dashboard' ){echo 'active';} ?>" >
                      <a href="<?php echo base_url();?>Admin"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>                    

                    <li class="dropdown">
                      <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-user"></i> User Managment
                        <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li class="<?php if( $this->uri->segment(2) == 'user_list'){echo 'active';} ?>">
                                <a href="<?php echo base_url();?>Admin/user_list/All"><i class="fa fa-fw fa-users"></i> Users</a>
                            </li>

                            <li class="<?php if( $this->uri->segment(2) == 'user_list_pro'){echo 'active';} ?>">
                                <a href="<?php echo base_url();?>Admin/user_list_pro"><i class="fa fa-fw fa-user-circle-o"></i>  Profiles</a>
                            </li>

                            <li class="<?php if( $this->uri->segment(2) == 'reviewListing'){echo 'active';} ?>">
                                <a href="<?php echo base_url();?>Admin/reviewListing"><i class="fa fa-star-half-o"></i></i>  Reviews</a>
                            </li>

                            <li class="<?php if( $this->uri->segment(1) == 'Category'){echo 'active';} ?>">
                                <a href="<?php echo base_url("Category");?>"><i class="fa fa-fw fa-puzzle-piece"></i> Category</a>
                            </li>
                            
                            <li class="<?php if( $this->uri->segment(1) == 'ExtraFilters'){echo 'active';} ?>">
                                <a href="<?php echo base_url("ExtraFilters");?>"><i class="fa fa-fw fa-list"></i> Extra Filters</a>
                            </li>

                            <li class="<?php if( $this->uri->segment(1) == 'Boxidea'){echo 'active';} ?>">
                              <a href="javascript:void(0)"><i class="fa fa-fw fa-download"></i> Import Profiles</a>
                            </li>

                          </ul>
                        </li>

                        <li class="dropdown">
                      <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-fw fa-comments"></i> Messaging
                        <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li class="<?php if( $this->uri->segment(2) == 'subs_list'){echo 'active';} ?>">
                              <a href="<?php echo base_url();?>Admin/subs_list"><i class="fa fa-fw fa-envelope-o"></i> Newsletter</a>
                            </li>

                            <li class="<?php if( $this->uri->segment(1) == 'Boxidea'){echo 'active';} ?>">
                                <?php
                                $msgcount = $this->db->where("bx_flag", 0)->count_all_results("otd_idea_box");                        
                                ?>
                                <a href="<?php echo base_url("Boxidea"); ?>"><i class="fa fa-fw fa-th-large"></i> Box Idea <span class="msg-count" style="<?php if(empty($msgcount)) echo "display:none;"; ?>"><?php if($msgcount > 0) echo $msgcount; ?></span></a>
                            </li>  
                          </ul>
                        </li>
                   

                    <li class="dropdown <?php if( $this->uri->segment(1) == 'Content' || $this->uri->segment(2) == 'pagesList' || $this->uri->segment(2) == 'editReadMore' || $this->uri->segment(2) ==  'LabelSuggestion'){ echo 'active';} ?>">
                      <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-pencil-square-o"></i> CMS <span class="caret"></span></a>
                      <ul class="dropdown-menu">
                        <li class="dropdown-submenu">
                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-home"></i> Home
                          <span class="caret"></span></a>
                        	<ul class="dropdown-menu">
                            <li><a href="<?php echo base_url("Banner"); ?>"><i class="fa fa-dot-circle-o"></i> Home Banner</a></li>
                            <li><a href="<?php echo base_url("Content/homeSection/2"); ?>"><i class="fa fa-dot-circle-o"></i>
                            Category</a></li>
                            <li><a href="<?php echo base_url("Content/homeSection/3"); ?>"><i class="fa fa-dot-circle-o"></i>
                            Promotion</a></li>
                          	<li><a href="<?php echo base_url("Content/homeSection/5"); ?>"><i class="fa fa-dot-circle-o"></i>
                             About</a></li>
                            <li><a href="<?php echo base_url("Content/homeSection/6"); ?>"><i class="fa fa-dot-circle-o"></i>
                            Our Services</a></li>
                          	<li><a href="<?php echo base_url("Content/homeSection/7"); ?>"><i class="fa fa-dot-circle-o"></i>
                             Business</a></li>
                          	<li><a href="<?php echo base_url("Content/homeSection/9"); ?>"><i class="fa fa-dot-circle-o"></i>
                             Support</a></li>
                          </ul>
                        </li>

                        <li class="dropdown-submenu">
                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-television"></i> Advertisements
                          <span class="caret"></span></a>                          
                          <ul class="dropdown-menu">
                            <li>
                              <a href="<?php echo base_url("Content/googleAdScript"); ?>"><i class="fa fa-dot-circle-o"></i>  Google Script</a>
                            </li>
                            <li>
                              <a href="<?php echo base_url("Content/manualAdvertisement"); ?>"><i class="fa fa-dot-circle-o"></i>  Manual Advertisements</a>
                            </li>
                          </ul>
                        </li>

                        <li class="dropdown-submenu">
                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-book"></i> Articles
                          <span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <!-- <li><a href="<?php echo base_url("Content/editReadMore/footer");?>">Footer Article</a></li> -->                      
                            <li><a href="<?php echo base_url("Content/editReadMore/personal");?>"><i class="fa fa-dot-circle-o"></i>  Personal Account Article</a></li>
                          
                            <li><a href="<?php echo base_url("Content/editReadMore/professional");?>"><i class="fa fa-dot-circle-o"></i>  Professional Account Article</a></li>
                          </ul>
                        </li>

                        <li class="dropdown-submenu">
                          <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-fw fa-globe"></i> Translation
                          <span class="caret"></span></a>                          
                          <ul class="dropdown-menu"> 
                            <li><a href="<?php echo base_url("Translation"); ?>"><i class="fa fa-dot-circle-o"></i>  Home Elements Content</a></li>
                            <li><a href="<?php echo base_url("LabelSuggestion"); ?>"><i class="fa fa-dot-circle-o"></i>  User Dashbord</a></li>
                            <li><a href="<?php echo base_url("Content/newsLetterRegistrations"); ?>"><i class="fa fa-dot-circle-o"></i>  Newsletter Box Content</a></li>
                            <li><a href="http://votivephp.in/VotiveYellowPages/Content/contentManagement/1"><i class="fa fa-dot-circle-o"></i>  Footer Content</a></li>
                            <li><a href="http://votivephp.in/VotiveYellowPages/Content/contentManagement/2"><i class="fa fa-dot-circle-o"></i>  Login Content</a></li>
                            <li><a href="http://votivephp.in/VotiveYellowPages/Content/contentManagement/3"><i class="fa fa-dot-circle-o"></i>  Ragitration Content</a></li>
                          </ul>
                        </li>
                      
                        <li><a href="<?php echo base_url("Content/pagesList"); ?>"><i class="fa fa-file-text-o"></i> Static Pages</a></li>
                        <li><a href="<?php echo base_url("Testimonial"); ?>"><i class="fa fa-newspaper-o"></i> Testimonial</a></li>
                        <li><a href="<?php echo base_url("Content/homeSection/SocialLinks"); ?>"><i class="fa fa-link"></i>  Social Links</a></li>
                      </ul>
                    </li>


                    <li class="<?php if( $this->uri->segment(1) == 'events'){echo 'active';} ?>">
                      <a href="<?php echo base_url("Admin/event_list");?>"><i class="fa fa-fw fa-calendar"></i> Events</a>
                    </li>

                    <li>
                      <a href="<?php echo base_url("Content/setSearchDistanceLimit");?>"><i class="fa fa-cogs"></i> Configuration</a>
                    </li> 


                   
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>


        <div id="page-wrapper">
            
        <div class="container-fluid">
                <div class="content-div">
               <?php echo $contents;?>
          </div>
          <!-- #container fluid  -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->




<div class="modal fade" id="myModaDel" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Selected Record Deleted</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Record Successfuly Deleted.
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
         <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
         <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list">Close</a>

      </div>
    </div>
  </div>
</div>




<div class="modal fade" id="myModaDelSubs" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Selected Record Deleted</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Record Successfuly Deleted.
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
         <!--<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>-->
         <a class="btn btn-default" href="<?php echo base_url();?>Admin/subs_list">Close</a>

      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModaAct" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Selected Record Activated</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             You have just Activated a User. <?php echo  $this->uri->segment(2); ?>

</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
                   <?php
if( $this->uri->segment(2) == "user_list")
{
             ?>
          <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list">Close</a> 
<?php
}
else
{
?>
 <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list_pro">Close</a> 

<?php
}
?>
      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModaDAct" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Selected Record Dectivated</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             You have just Dectivated a User. 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
       <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list">Close</a>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="myModaResentMail" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Change Passwrod Link Sent</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             An Email with link to change password has been sent to user. 
</strong>
             
              
              </div>
            </div>
        </div>
      </div>
      
    </div>
  </div>
</div>


<div class="modal fade" id="myModaPubs" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Published on Home Page</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Selected Profile is Published on Home Page 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
                   <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list_pro">Close</a> 

      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModaUPubs" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Published on Home Page</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Selected Profile is unPublished on Home Page 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
                   <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list_pro">Close</a> 

      </div>
    </div>
  </div>
</div>

  
<div class="modal fade" id="myModaTops" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Published on Top Rated List</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Selected Profile is Published on Top Rated Section of Home Page 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
                   <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list_pro">Close</a> 

      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModauTops" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">UnPublished from Top Rated List</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             Selected Profile is UnPublished from Top Rated Section of Home Page 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
                   <a class="btn btn-default" href="<?php echo base_url();?>Admin/user_list_pro">Close</a> 

      </div>
    </div>
  </div>
</div>



<div class="modal fade" id="myModalDistance" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"> 
        <h4 class="modal-title">Distance Limit</h4>
        <div class="title-imge">

          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         
          
            <div class="row">
              <div class="col-md-12">
             
<strong>
             You have just Changed Distance Limit for Search Profiles. 
</strong>
             
              
              </div>
            </div>


        </div>
      </div>
      <div class="modal-footer">
       <a class="btn btn-default" data-dismiss="modal">Close</a>
      </div>
    </div>
  </div>
</div>

<!-- Heading model -->
  <div class="modal fade" id="updateheadings" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <form id="updateconheading">
          <div class="modal-header"> 
            <h4 class="modal-title" id="ctheading"></h4>                
          </div>
          <div class="modal-body">
            <div class="modal-login-txt">
                <div class="row">
                  <div class="col-md-12">                     
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Heading:</label>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="text" name="heading" id="heading" class="form-control" placeholder="Heading" value=""/>
                        <?php echo form_error('heading'); ?>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Sub-Heading:</label>
                      </div>
                      <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="text" name="subheading" id="subheading" class="form-control" placeholder="Sub Heading" value=""/>
                        <?php echo form_error('subheading'); ?>
                      </div>
                    </div>
                  </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <input type="hidden" id="sectionid" value="" name="sectionid"/>
            <div id="transmsg"></div>
            <button type="submit" class="btn btn-success transsubmit">Update</button>
            <a class="btn btn-default transclose" data-dismiss="modal">Cancel</a>
          </div>
        </form>
      </div>
    </div>
  </div>
<!-- Heading model end -->

 <!--<script type="text/javascript" src="<?php echo base_url("assets/js/jquery-3.1.1.min.js") ?>"></script>-->



<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-toggle.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/metisMenu/metisMenu.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url();?>assets/vendor/morrisjs/morris.min.js"></script>
<script src="<?php echo base_url();?>assets/data/morris-data.js"></script>
<script src="<?php echo base_url();?>assets/dist/js/sb-admin-2.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mCustomScrollbar.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap-confirmation.min.js"></script> 
<script src="<?php echo base_url("assets/js/jquery.flexdatalist.js");?>"></script>
<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
<script src="<?php echo base_url("assets/js/custom.js");?>"></script>
 
<script>
      var placeSearch, autocomplete;
      var componentForm = {
        street_number: 'short_name',
        route: 'long_name',
        locality: 'long_name',
        administrative_area_level_1: 'short_name',
        country: 'long_name',
        postal_code: 'short_name'
      };

      function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
            {types: ['geocode']});
        //autocomplete.addListener('place_changed', fillInAddress);
      }

      function fillInAddress() {
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
          document.getElementById(component).value = '';
          document.getElementById(component).disabled = false;
        }
        for (var i = 0; i < place.address_components.length; i++) {
          var addressType = place.address_components[i].types[0];
          //alert(addressType);
          if (componentForm[addressType]) {
            var val = place.address_components[i][componentForm[addressType]];
            document.getElementById(addressType).value = val;
          }
        }
      }
      function geolocate() {
          
        if (navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            var geolocation = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
            var circle = new google.maps.Circle({
              center: geolocation,
              radius: position.coords.accuracy
            });
            autocomplete.setBounds(circle.getBounds());
          });
        }
      }
    </script>

<script type="text/javascript" src="<?php echo base_url("assets/js/autosugges/jquery.mockjax.js") ?>"></script>
<script type="text/javascript" src="<?php echo base_url("assets/js/autosugges/bootstrap-typeahead.js") ?>"></script>
 <!--  <script type="text/javascript" src="<?php echo base_url("assets/js/sb-admin-custom.js") ?>"></script>-->
 
 <script type="text/javascript">
   $('#user_email').typeahead({
       displayField: 'name',
       scrollBar:true,
       ajax: { 
             url: '<?php echo base_url("Admin/userEmails"); ?>',
             triggerLength: 1 
           }
   });
   $('#subcribe_user_email').typeahead({
       displayField: 'name',
       scrollBar:true,
       ajax: { 
             url: '<?php echo base_url("Admin/subscribeUserEmails"); ?>',
             triggerLength: 1 
           }
   });
 </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDiEciqRYS_WiyGcfg1QR8lTWwyBHIcR_c&libraries=places&callback=initAutocomplete"
        async defer></script>
    <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer></script>
 <script type="text/javascript" src="<?php echo base_url("assets/js/sb-admin-custom.js") ?>"></script>
    
<script  type="text/javascript">
$(document).ready(function() {
jQuery('#export-to-csv').bind("click", function() {
var target = $(this).attr('id');
switch(target) {
    case 'export-to-csv' :
    $('#hidden-type').val(target);
   // alert($('#signup_method').val());
    $('#hidden_user_email').val($('#user_email').val());
    $('#hidden_signup_method').val($('#signup_method').val());
    $('#export-form').submit();
    $('#hidden-type').val('');
    break
}
});
    });

$('[data-toggle=confirmation]').confirmation({
    rootSelector: '[data-toggle=confirmation]',
    container: 'body'
  });


$("#clearfilterSubmit").click(function()
  {
  $('#hidden_user_email').val("");
  $('#user_email').val("");
  $('#hidden_signup_method').val("");
  $('#signup_method').val("");
  $('#clearfilter').val("yes");
  $('#filterForm').submit();
  });

</script>

<script>
/*$(document).ready(function() {
 
	// For the Second level Dropdown menu, highlight the parent	
	$( ".dropdown-menu" )
	.mouseenter(function() {
		$(this).parent('li').addClass('active');
	})
	.mouseleave(function() {
		$(this).parent('li').removeClass('active');
	});
 
});*/
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.navbar a.dropdown-toggle').on('click', function(e) {
            var elmnt = $(this).parent().parent();
            if (!elmnt.hasClass('nav')) {
                var li = $(this).parent();
                var heightParent = parseInt(elmnt.css('height').replace('px', '')) / 2;
                var widthParent = parseInt(elmnt.css('width').replace('px', '')) - 10;
                
                if(!li.hasClass('open')) li.addClass('open')
                else li.removeClass('open');
                //$(this).next().css('top', heightParent + 'px');
                //$(this).next().css('left', widthParent + 'px');
                
                return false;
            }
        });
    });
</script>

</body>
</html>