<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Pradeep Verma">
    <title>Listing - <?php echo $title;?></title>
    <meta name="description" content="overview &amp; stats" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
   <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="<?php echo base_url();?>assets/vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
   <!--
    <link href="<?php echo base_url();?>assets/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/creative.min.css" rel="stylesheet"> -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/master.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/owl.theme.css">
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url("assets/css/jquery.flexdatalist.css");?>">
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/jquery.mCustomScrollbar.css'); ?>">
<!-- Custom Core CSS -->
<link href="<?php echo base_url();?>assets/css/custom.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap-material-datetimepicker.css" />
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/lightbox.min.css" />
<?php /*?><link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"><?php */?>
  <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/blog_rating.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<link rel="icon" href="<?php echo base_url(); ?>/favicon.ico" type="image/gif">
<!-- <script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script> 
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script> -->
<meta name="google-signin-scope" content="profile email">
<meta name="google-signin-client_id" content="185961027889-o82v14cenj8uvcnptkq2vd4ksim1kkth.apps.googleusercontent.com">

<!-- <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597c1b975d21460011308163&product=inline-share-buttons"></script> -->
<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=597c1b975d21460011308163&product=inline-share-buttons"></script>
</head>
<body>  
<header id="main_header_id" class="<?php if($this->uri->segment(2) == "businessListing" || $this->uri->segment(2) == "account" || $this->uri->segment(2) == "user_profile") echo "in-page"; ?>">
  <div class="header-main">
       <nav class="navbar navbar-default navbar-fixed-top" id="nav-top">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/img/logo.png"></a>
            </div>
           <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav navbar-right main-nav">
               <?php 
//print_r($user);
if($this->session->userdata('user_id') && $this->session->userdata('user_id') >0)
{
  $userdetails = $this->db->select("user_firstname")->get_where("users", array("user_id"=>$this->session->userdata("user_id")));
  ?>
  <li class="user-intro dropdown">
    <a href="<?php echo base_url("Users/account"); ?>" class="dropdown-toggle" data-toggle="dropdown">Hello <?php echo $userdetails->result()[0]->user_firstname; ?>
    <span class="caret"></span></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo base_url("Users/account/inbox"); ?>">
          <i class="fa fa-envelope"></i>
          Messages
          <?php 
            $sid= $this->session->userdata('user_id');
            $unreadmsg = $this->db->select("msg_id")                
                ->where("msg_reciever = $sid AND us_notify = 0")
                ->group_by("msg_sender")
                ->get("otd_user_messaging");  
            if($unreadmsg->num_rows()) {
              ?>
              <span class="counter-msg">
                <?php
                  echo $unreadmsg->num_rows();
                ?>
              </span>
              <?php
            }
          ?>
        </a></li>
      <li>
        <a href="<?php echo base_url("Users/account/notifications"); ?>">
          <i class="fa fa-bell" aria-hidden="true"></i>
          Notifications
          <?php 
            $sid= $this->session->userdata('user_id');
            $unreadnoti = $this->db->where("nt_to = $sid AND nt_flag = 0")
                                  ->count_all_results("otd_notifications");  
            if($unreadnoti > 0) {
              ?>
              <span class="counter-not">
                <?php
                  echo $unreadnoti;
                ?>
              </span>
              <?php
            }
          ?>
          <!-- <span class="counter-not">10</span> -->
        </a>
      </li>
      <li><a href="<?php echo base_url("Users/account"); ?>"><i class="fa fa-user" aria-hidden="true"></i>My Profile</a></li>
      <?php  
        $social_login = $this->session->userdata("social_login");
        if(!empty($social_login)){
          if($social_login['is_social'] && $social_login['social_type'] == "facebook"){
          ?>
          <li><a id="logoutbutton" href="javascript:;" onclick="Logout();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
          <?php
          }
          else if($social_login['is_social'] && $social_login['social_type'] == "google"){
            ?>
            <li><a id="logoutbutton" href="javascript:void(0);" onClick="signOut();"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
            <?php
          }
        }else{
        ?>
        <li><a id="logoutbutton" href="<?php echo base_url();?>Users/logout"><i class="fa fa-power-off" aria-hidden="true"></i>Logout</a></li>
        <?php
        }
        ?>
    </ul>
  </li>
<?php  
}
  else{              
      ?>
                    <li><a id="loginmodal" href="javascript:;" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Login</a>
                                </li>
                    <li><a id="registrationmodal" href="javascript:;" data-toggle="modal" data-target="#myModal">Register</a>
                    </li>
                    <?php
}                    ?>
              <li class="good-place"><a href="#">Good Place <i class="fa fa-plus"></i></a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
    </div>
</header>     
<?php echo $contents;?>                    
<!-- footer  -->
<?php
include "footer.php";
?>      
<!--  End Footer -->
<!-- Modal -->
<div class="modal fade" id="myModal2" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">        
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title">Login To My Account</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
          <h2>User Login</h2>
          <form id="userlogin" action="<?php echo base_url();?>Users/login" method="post">
            <div class="row">
              <div class="social-media-login">
                <a href="javascript:;" class="btn btn-default fb-so" onClick="Login()"><i class="fa fa-facebook"></i>Login with facebook</a>                
                <div class="g-signin2" data-onsuccess="onSignIn">Gmail Login</div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <input type="email" class="form-control" name="user_email" placeholder="Email"  value="<?php echo !empty($user['user_email'])?$user['user_email']:''; ?>">
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="password" class="form-control" name="user_password" placeholder="Password" required>
                  <?php echo form_error('user_password','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <input type="submit" name="loginSubmit" class="btn-primary" value="Login"/>
                </div>
              </div>
              <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal3">Forgot Password</button>
              <button type="button" class="btn btn-default new-acc-m" data-dismiss="modal" data-toggle="modal" data-target="#myModal">New Account</button>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">       
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">     
    <!-- Modal content-->    
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->      
        <h4 class="modal-title">Create My Account</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="select-acount">
          <div class="select-sub">
            <img src="<?php echo base_url("/assets/img/loginicon.png"); ?>">
            <h2>Create Personal Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-default" href="<?php echo base_url();?>Users/registration">Select</a>
              <a href="<?php echo base_url("Pages/readMore/personal"); ?>" class="learn-mre">Learn more</a>
            </div>
          </div>
          <div class="select-sub">
            <img src="<?php echo base_url("assets/img/rightimgdiv.png"); ?>">
            <h2>Create Professional Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration">Select</a>
              <a href="<?php echo base_url("Pages/readMore/professional"); ?>" class="learn-mre">Learn more</a>
            </div>
          </div>
        </div>
        <div class="subscription">
          <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="subscribe_form">
            <div class="newsletter-main-head">
              <div class="news-letter-head">
                <h4>To receive the local newsletter of your new Otourdemoi newsletter</h4>
                <p>Thank you for informing:</p>
            
              <div class="row news-letter-form">
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" name="user_email" required class="inputbox">
                </div>
                <div class="col-md-5">
                  <input type="text" title="Postal Code" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                </div>
                <div class="col-md-2">
                  <input type="submit" name="subscribeSubmit" onClick="" value="Subscribe" class="submit-main">
                </div>
              </div>
              <div class="row agree-sec">
                <div class="col-md-12">
                  <input type="checkbox" required title="Terms and Conditions" name="terms">
                  <span>I agree to receive communications from the companies and partners of Otourdemoi</span>
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>        
        <!-- -->         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--     Forget Password Model -->
<div class="modal fade" id="myModal3" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">         
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title">Forgot Password</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">         
          <form action="<?php echo base_url();?>Users/forgetpassword" method="post">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <input type="text" class="form-control" name="user_email" placeholder="Enter Your Email" required>
                  <?php echo form_error('user_email','<span class="help-block">','</span>'); ?>
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group">
                  <input type="submit" name="passwordSubmit" class="btn-primary" value="Send Password Request"/>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
 <!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">     
    <!-- Modal content-->    
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->      
        <h4 class="modal-title">Create My Account</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="select-acount">
          <div class="select-sub">
            <img src="<?php echo base_url("assets/img/loginicon.png"); ?>">
            <h2>Create Personal Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-default" href="<?php echo base_url();?>Users/registration">Select</a>
              <a href="#" class="learn-mre">Learn more</a>
            </div>
          </div>
          <div class="select-sub">
            <img src="<?php echo base_url("assets/img/rightimgdiv.png"); ?>">
            <h2>Create Professional Account</h2>
            <div class="overly-sub-select">
              <a class="btn btn-primary" href="<?php echo base_url();?>Users/pro_registration">Select</a>
              <a href="#" class="learn-mre">Learn more</a>
            </div>
          </div>
        </div>
        <div class="subscription">
          <form action="<?php echo base_url().'Users/subscription'; ?>" method="post" name="subscribe_form">
            <div class="newsletter-main-head">
              <div class="news-letter-head">
                <h4>To receive the local newsletter of your new Otourdemoi newsletter</h4>
                <p>Thank you for informing:</p>
            
              <div class="row news-letter-form">
                <div class="col-md-5">
                  <input type="text" title="E-mail" placeholder="Votre email*" value="" style="width:34%" name="user_email" required class="inputbox">
                </div>
                <div class="col-md-5">
                  <input type="text" title="Postal Code" placeholder="Votre code postal*" value="" style="width:34%" name="user_postcode" class="inputbox">
                </div>
                <div class="col-md-2">
                  <input type="submit" name="subscribeSubmit" onClick="" value="Subscribe" class="submit-main">
                </div>
              </div>
              <div class="row agree-sec">
                <div class="col-md-12">
                  <input type="checkbox" required title="Terms and Conditions" name="terms">
                  <span>I agree to receive communications from the companies and partners of Otourdemoi</span>
                </div>
              </div>
              </div>
            </div>
          </form>
        </div>        
        <!-- -->         
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!--     Forget Password Model -->
<div class="modal fade" id="myModalregsucc" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">         
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->        
        <h4 class="modal-title">Registration Completed</h4>
        <div class="title-imge">
          <img src="<?php echo base_url();?>assets/img/logo.png">
        </div>
      </div>
      <div class="modal-body">
        <div class="modal-login-txt">
         <span>
          Your registration was successfully. Please login to your account.
         </span>          
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal" data-toggle="modal" data-target="#myModal2">Login To Your Account</button>
         <button type="button" class="btn btn-default frgt-pass" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
 $(document).ready(function() {

getLocation();
 LoadMap(); 


   $("form[name='searchForm']").validate({
    rules: {
      user_address: "required",
    },
    // Specify validation error messages
    messages: {
      user_address: "Please enter address",
     
    },
    submitHandler: function(form) {
      form.submit();
    }
  });

   
 });
</script>


<!-- for Home -->

<script>
    $(document).ready(function() {

      var owl = $("#owl-demo-pro");

      owl.owlCarousel({
    autoPlay : 3000,
        stopOnHover : true,
      items : 4, //10 items above 1000px browser width
      itemsDesktop : [1000,4], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // 3 items betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0;
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });
    </script>
       <script>
    $(document).ready(function() {

      var owl = $("#owl-demo-down");

      owl.owlCarousel({
    autoPlay : 3000,
        stopOnHover : true,
      items : 2, //10 items above 1000px browser width
      itemsDesktop : [1000,2], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,2], // 3 items betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0;
      itemsMobile : false // itemsMobile disabled - inherit from itemsTablet option
      
      });

      // Custom Navigation Events
      $(".next").click(function(){
        owl.trigger('owl.next');
      })
      $(".prev").click(function(){
        owl.trigger('owl.prev');
      })
    });




    </script>
