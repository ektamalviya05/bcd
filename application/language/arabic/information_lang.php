<?php
/**
 * System messages translation for CodeIgniter(tm)
 *
 * @author	CodeIgniter community
 * @copyright	Copyright (c) 2014-2018, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$ci =& get_instance();
$ci->load->database();

/*---------- Top Header ----------*/
$lang['topheader_news'] = "أخبار";
$lang['topheader_careers'] = "وظائف";
$lang['topheader_contactus'] = "اتصل بنا";
$lang['topheader_searchkeyword'] = "كلمة البحث";
$lang['topheader_mydashboard'] = "لوحة المعلومات الخاصة بي";
$lang['topheader_myaccount'] = "حسابي";
$lang['topheader_signin'] = "تسجيل الدخول";
$lang['topheader_signup'] = "سجل";
$lang['topheader_searchresult'] = "نتيجة البحث";

/*---------- Top Menu ----------*/
$lang['topheader_home'] = "الصفحة الرئيسية";
$lang['topheader_aboutus'] = "معلومات عنا";
$lang['topheader_service'] = "خدمات";
$lang['topheader_goinggreen'] = "يصبح أخضرا";
$lang['topheader_company'] = "لماذا الشركة";
$lang['topheader_ourpeople'] = "شعبنا";

/*---------- Home Page ----------*/
$lang['slider_text'] = "حماية البيئة";

$lang['readmore'] = "اقرأ أكثر";

/*---------- Contact Page ----------*/
$lang['getintouch'] = "ابق على تواصل معنا";
$lang['contactDetails'] = "بيانات المتصل";
$lang['or'] = "أو";
$lang['fullname'] = "الاسم الكامل";
$lang['emailaddress'] = "عنوان البريد الإلكتروني";
$lang['comment'] = "تعليق";
$lang['submit'] = "خضع";

$sql = "SELECT * FROM `site_contents` WHERE `language_slug` = 'ar'";
	$relt = $ci->db->query($sql);     
	$relts = $relt->result();
//echo "<pre>"; print_r($relts);die();
foreach ($relts as $key => $value) {
	$lang['homesections'.$key] = $value->section;
	$lang['homecontents'.$key] = $value->content;
	$lang['homesubsections'.$key] = $value->sub_section;
}

/*---------- Footer Section ----------*/
$lang['information'] = "معلومات";
$lang['services'] = "خدمات";

$lang['ourservices'] = "خدماتنا";
$lang['ournews'] = "أخبارنا";
$lang['recentnews'] = "أخبار حديثة";

/*---------- Career page ----------*/
$lang['applyjob'] = "طلب للحصول على وظيفة";
$lang['postapplied'] = "آخر التطبيقية";
$lang['firstname'] = "الاسم الاول";
$lang['lastname'] = "الكنية";
$lang['email'] = "البريد الإلكتروني";
$lang['contact'] = "رقم الاتصال";
$lang['resume'] = "أرفق السيرة الذاتية";

$sql = "SELECT * FROM `otd_social_links` WHERE `language_slug` = 'ar'";
	$relt = $ci->db->query($sql);     
	$relts = $relt->result();
//echo "<pre>"; print_r($relts);
foreach ($relts as $key => $value) {
	$lang['sclinks'.$key] = $value->sc_links;
}

/*---------- Customer Signup Page ----------*/
$lang['customer_signup'] = "تسجيل العميل";
$lang['Company_Information'] = "معلومات الشركة";
$lang['Company_Name'] = "اسم الشركة";
$lang['Company_No'] = "رقم الشركة";
$lang['Company_Type'] = "نوع الشركة";
$lang['Select_Company_Type'] = "اختر نوع الشركة";
$lang['Company_Commercial_Registration_Number'] = "رقم السجل التجاري للشركة";
$lang['Authorized_Person'] = "الشخص المرخص";
$lang['Mobile_No'] = "رقم المحمول";
$lang['Designation'] = "تعيين";
$lang['Department'] = " قسم، أقسام";
$lang['National_ID_No'] = "رقم الهوية الوطني";
$lang['Extension_No'] = "ملحق رقم";
$lang['Telephone_No'] = "الهاتف لا";
$lang['Password'] = "كلمه السر";
$lang['Confirm_Password'] = "تأكيد كلمة المرور";
$lang['Other_Information'] = "معلومات أخرى";
$lang['Country'] = "بلد";
$lang['Select_Country'] = "حدد الدولة";
$lang['State'] = "حالة";
$lang['Select_State'] = "اختر ولايه";
$lang['City'] = "مدينة";
$lang['Select_City'] = "اختر مدينة";
$lang['Address'] = "عنوان";
$lang['All_ready_have_a_Account'] = "كل مستعد لديك حساب";
$lang['Hospital'] = "مستشفى";
$lang['Medical_Center'] = "مركز طبي";
$lang['Pharmacy'] = "مقابل";
$lang['Pharmaceutical_Company'] = "شركة الأدوية";
$lang['Factory'] = "مصنع";
$lang['Government_Entity'] = "جهة حكومية";

/*---------- Customer Signin Page ----------*/
$lang['FORGOT_PASSWORD'] = "هل نسيت كلمة المرور ؟";
$lang['have_an_account'] = "ليس لديك حساب!";
$lang['Sign_up_here'] = "سجل هنا";

/*---------- Customer Forgot Page ----------*/
$lang['Reset_your_password'] = "اعد ضبط كلمه السر";
$lang['RESET'] = "إعادة تعيين";

/*---------- Customer My account Page ----------*/
$lang['Edit_Profile'] = "تعديل الملف الشخصي";
$lang['Change_Password'] = "تغيير كلمة السر";
$lang['All_Complaints'] = "جميع الشكاوي";
$lang['Orders'] = "أوامر";
$lang['View_Statements'] = "عرض البيانات";
$lang['Logout'] = "الخروج";

$lang['Profile_Information'] = "معلومات الملف الشخصي";
$lang['Gender'] = "جنس";
$lang['Gender_m'] = "الذكر";
$lang['Gender_f'] = "إناثا";
$lang['Date_of_Birth'] = "تاريخ الولادة";
$lang['Profile_Picture'] = "الصوره الشخصيه";
$lang['update'] = "تحديث";
$lang['cancel'] = "إلغاء";

$lang['Old_Password'] = "كلمة المرور القديمة";
$lang['New_Password'] = "كلمة السر الجديدة";
$lang['Confirm_New_Password'] = "تأكيد كلمة المرور الجديدة";

$lang['Add_Complaint'] = "أضف شكوى";
$lang['Ticket_No'] = "تذكرة رقم.";
$lang['Complaint_Subject'] = "موضوع الشكوى";
$lang['Complaint_Description'] = "وصف الشكوى";
$lang['Created'] = "خلقت";
$lang['Status'] = "الحالة";
$lang['Action'] = "عمل";
$lang['Submit_Complaint'] = "تقديم شكوى";

$lang['Statement'] = "بيان";
$lang['Sno'] = "سنو.";
$lang['Date'] = "تاريخ";
$lang['Invoice'] = "فاتورة #";
$lang['Description'] = "وصف";
$lang['Invoice_Amount'] = "قيمة الفاتورة";
$lang['Amount_Paid'] = "المبلغ المدفوع";
$lang['Total_Paid'] = "مجموع المبالغ المدفوعة";
$lang['Total_Unpaid'] = "مجموع غير مدفوع";
$lang['No_statement_found'] = "لم يتم العثور على بيان";

$lang['Orders_List'] = "قائمة الطلبات";
$lang['Place_Order'] = "مكان الامر";
$lang['Order_ID'] = "رقم التعريف الخاص بالطلب";
$lang['Service_Type'] = "نوع الخدمة";
$lang['Order_Date'] = "تاريخ الطلب";
$lang['Preferred_Date'] = "موعد مفضل";
$lang['Alternate_Date'] = "التاريخ البديل";
$lang['Preferred_Time'] = "الوقت المفضل";
$lang['Rate'] = "معدل";
$lang['Us'] = "لنا";
$lang['Title'] = "عنوان";
$lang['Feedback'] = "ردود الفعل";
$lang['No_Record_Found'] = "لا يوجد سجلات";
$lang['Manifest_No'] = "مانيفست رقم";
$lang['Select_Type_of_service'] = "اختر نوع الخدمة";
$lang['Select_Preferred_Time'] = "اختر الوقت المفضل";
$lang['Morning'] = "صباح (08 إلى 12)";
$lang['Afternoon'] = "بعد الظهر (12 إلى 03)";
$lang['Late_Afternoon'] = "بعد الظهر (03 إلى 06)";
$lang['Recurring'] = "تناوبي";
$lang['None'] = "لا شيء";
$lang['Weekly'] = "أسبوعي";
$lang['Monthly'] = "شهريا";
$lang['SelectDay'] = "اختر اليوم";
$lang['Monday'] = "الإثنين";
$lang['Tuesday'] = "الثلاثاء";
$lang['Wednesday'] = "الأربعاء";
$lang['Thursday'] = "الخميس";
$lang['Friday'] = "يوم الجمعة";
$lang['Saturday'] = "يوم السبت";
$lang['Sunday'] = "الأحد";

/*---------- Customer Change password Page ----------*/
$lang['please_enter_your_password'] = "من فضلك أدخل رقمك السري";
$lang['please_provide_new_password'] = "يرجى تقديم كلمة مرور جديدة";
$lang['your_password_must_be_least_characters_long'] = "يجب أن تتكون كلمة المرور الخاصة بك من 6 أحرف على الأقل";
$lang['please_provide_new_password_again'] = "يرجى تقديم كلمة مرور جديدة مرة أخرى";
$lang['password_must_be_same'] = "كلمة المرور يجب أن تكون هي نفسها";

/*---------- Customer edit profile Page ----------*/
$lang['The_firstname_is_required'] = "الاسم الأول مطلوب";
$lang['The_lastname_is_required'] = "اسم العائلة مطلوب";
$lang['The_email_address_is_required'] = "عنوان البريد الإلكتروني مطلوب";
$lang['The_date_of_birth_is_required'] = "تاريخ الميلاد مطلوب";
$lang['The_phone_number_is_required'] = "رقم الهاتف مطلوب";
$lang['The_country_is_required'] = "البلد مطلوب";
$lang['The_state_is_required'] = "الدولة مطلوبة";
$lang['The_city_is_required'] = "المدينة مطلوبة";
$lang['The_addres_is_requireds'] = "الاضافات مطلوبة";
$lang['The_desgination_is_required'] = "مطلوب desgination";
$lang['The_department_is_required'] = "مطلوب الدائرة";
$lang['The_national_ID_number_is_required'] = "رقم الهوية الوطنية مطلوب";
$lang['The_extension_number_is_requireds'] = "رقم الملحق مطلوب";

/*---------- Customer feedback Page ----------*/
$lang['Please_enter_title'] = "يرجى إدخال العنوان";
$lang['Please_enter_review'] = "يرجى إدخال مراجعة";
$lang['Please_select_stars'] = "يرجى اختيار النجوم";

/*---------- Contact us Page ----------*/
$lang['Please_enter_your_full_name'] = "من فضلك ادخل اسمك الكامل";
$lang['The_email_is_required'] = "البريد الإلكتروني مطلوب";
$lang['The_comments_is_required'] = "التعليقات مطلوبة";