<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

require APPPATH . '/libraries/lib/Braintree.php';


class Users extends REST_Controller 
{   
    public function __construct() {  

    parent:: __construct();

	    //error_reporting(E_ALL);
	    //ni_set('display_errors', 1);
	    /*For Language*/ 
	 	$this->load->library("braintree_lib");
	        $uid= "";
	    if ($_POST){ $this->param = $_POST;
            $res = file_put_contents('/home/votivephp/public_html/businessdirectoryclassified/results.json', $this->param);
	    }else{ $this->param = json_decode(file_get_contents("php://input"),true);}
	        
	    $user_language = $this->param['user_language'];

	    if (!empty($this->param['user_id'])){$uid = $this->param['user_id'];}

	    if (!empty($user_language))
	    	{$userData = $this->param['user_language']; }else{ $userData = getlanguge($uid);}
	    

	        /*Globals Language Set*/
	        $GLOBALS['user_language'] = $userData;        
    }
/*_______________________________________-END-______________________________________*/
    /*___________________________________-NEW FUNCTION-_____________________________*/

        public function new_Function_post(){

            /* Check for required parameter */
            $input = $this->param;
            $required = array('user_id','user_language');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){
                echo "For New Fenction";       
            } else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
    /*___________________________________-NEW FUNCTION-_____________________________*/
    /*___________________________________-PANDING-__________________________________*/
        
    	/**
        * Get Package List Process for simple Listing
        * @param user_id 
        */
        public function package_post() {
            
            //check for required parameter 

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                $plane = $this->common_model->showUserPlane($input['user_id']);
                if (!empty($plane)) {
                    $current = $plane['package']['packageid'];
                }

                $package = AR(PACKAGE, array('opt_status' => 1),"","");
                if (!empty($package)) {
                  for ($i=0; $i < count($package); $i++) {

                    if (!empty($current) &&$current == $package[$i]['opt_id']) {
                       $packages[$i]['current'] = "1";
                    }else{
                       $packages[$i]['current'] = "0"; 
                    }
                    $packages[$i]['opt_id'] = $package[$i]['opt_id'];
                    $packages[$i]['opt_name'] = $package[$i]['opt_name'];
                    $packages[$i]['opt_description'] = $package[$i]['opt_description'];
                    $packages[$i]['opt_type'] = $package[$i]['opt_type'];
                    $packages[$i]['duration'] = $package[$i]['duration'];
                    $packages[$i]['price'] = $package[$i]['price'];
                    $packages[$i]['opt_status'] = $package[$i]['opt_status'];
                    $packages[$i]['opt_create_date'] = $package[$i]['opt_create_date'];
                    $packages[$i]['opt_last_update'] = $package[$i]['opt_last_update'];
                  }
                }
                
                //print_r(expression)

                if ($package) {
                    $resp = array('status' => SUCCESS, 'message' => "show", 'response' => array('data' => $packages/*, 'plan' => $plane?$plane:new stdClass*/));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        * show Packeg plan for listing process
        * @param user_id 
        */

        public function showPlane_post() {

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                
                $plane = $this->common_model->showUserPlane($input['user_id']);
                if (!empty($plane)) {
                    $resp = array('status' => SUCCESS, 'message' => "success!", 'response' => array('data' => $plane)); 
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => "You have no plan active, select plan", 'response' => new stdClass);    
                }
                            
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        * show Features plan for listing process
        * @param user_id 
        */


        public function getFeaturesPackeg_post() {

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){


                $plane = $this->common_model->showUserPlane($input['user_id']);                
                if (!empty($plane)) {
                   $current = $plane['features']['packageid'];
                }
                
                $Features = AR(FEATURES, array('status' => 1),"","");
                //print_r($Features);die;
                if (!empty($Features)) {
                  for ($i=0; $i <count($Features); $i++) {

                    if (!empty($current) &&$current == $Features[$i]['id']) {
                       $Feature[$i]['current'] = "1";
                    }else{
                        $Feature[$i]['current'] = "0";
                    }
                    $Feature[$i]['id'] = $Features[$i]['id'];
                    $Feature[$i]['package_title'] = $Features[$i]['package_title'];
                    $Feature[$i]['package_desc'] = $Features[$i]['package_desc'];
                    $Feature[$i]['package_validity'] = $Features[$i]['package_validity'];
                    $Feature[$i]['package_price'] = $Features[$i]['package_price'];
                    $Feature[$i]['created'] = $Features[$i]['created'];
                    $Feature[$i]['status'] = $Features[$i]['status'];
                  }
                }
                $resp = array('status' => SUCCESS, 'message' => "success!", 'response' => array('data' => $Feature));          
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        *  Get Packages  List
        * @param user_language
        */
        // public function getpackagelist_post() {
            
        //     //check for required parameter 

        //     $input = $this->param;
        //     $required = array('user_id','user_language','device_id');
        //     $Error = check_required_value($required, $input);        
        //     if ($Error == "0"){
               
        //         $package = AR(PACKAGE, array('status' => 1), "", "");
        //         if ($package) {
        //             $resp = array('status' => SUCCESS, 'message' => "show", 'response' => array('data' => $package));
        //         }else{
        //             $resp = array('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
        //         }
        //     } else{//YOU_HAVE_MISSED_A_PARAMETER_
        //         $resp = $Error;
        //     }

        //     $this->response($resp);
        // }

        


        
    /*___________________________________-PANDING-__________________________________*/
    /*___________________________________-COMPLITED-________________________________*/


    	/**
        * Check New Business Process for Listing
        * @param user_id 
        */

        public function checkNewBusiness_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                $where= "( status = 1 || status = 0 ) && user_id = '".$input['user_id']."' && validate > '".date("Y-m-d")."'  "; 
                $simple = CR(BUSINESS, $where);
                $tran_count_simple = CR(TRANSACTION, array('userid' => $input['user_id'], 'st' => "success", 'free_end >'=> date("Y-m-d")));


                $features = CR(BUSINESS, array('user_id' => $input['user_id'], 'status' => 1, 'features_ads' => 1));     
                $tran_count = CR(TRANSACTION, array('userid' => $input['user_id'], 'st' => "success", 'features_end >'=> date("Y-m-d") ));

                $buss = SR(BUSINESS, array('user_id' => $input['user_id'], 'status' => "1"));
                if (!empty($buss)) {
                    $business = $buss['business_id'];
                }else{
                    $business = "0";
                }

                $results = array('pay' => (string)$tran_count_simple,
                                 'simple' => (string)$simple,
                                 'payment' => (string)$tran_count,                    
                                 'features' => (string)$features,                    
                                 'business_id' => $business
                                );

                $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('data' => $results));                                
                
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }


        /**
        * Add New Business Process for Listing
        * @param $required 
        */
                
        public function addNewBusiness_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id','user_language','device_id','cat_id', 'sub_cat_id','business_title','business_email','business_mobile','business_address','business_country', 'business_city', 'business_desc','lat', 'lon', 'address_type');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                $buss_count = CR(BUSINESS, array('user_id' => $input['user_id'], 'status' => "1"));
                //print_r($buss_count);die;
                if ($buss_count == "0" ) {
                    //$check = check_country_city(strtolower($input['business_city']), strtolower($input['business_country']));
                    //if ($check == "0") {

                        $tran_rec = $this->common_model->getvalidity($input['user_id']);
                        
                        if (!empty($_FILES)) {
                            $names = array('business_image1','business_image2','business_image3','business_image4','business_image5','business_image6','business_image7','business_image8','business_image9','business_image10','business_logo');    
                            $inputs = multipulImage($_FILES, $names,'business');                   
                        }
                        $inputs['user_id'] =                    $input['user_id'];
                        $inputs['cat_id'] =                     $input['cat_id'];
                        $inputs['sub_cat_id'] =                 $input['sub_cat_id'];
                        $inputs['business_title'] =             $input['business_title'];
                        $inputs['business_desc'] =              $input['business_desc'];
                        $inputs['business_address'] =           $input['business_address'];
                        $inputs['address_type'] =               $input['address_type'];
                        $inputs['business_email'] =             $input['business_email'];
                        $inputs['business_mobile'] =            $input['business_mobile'];           
                        $inputs['business_country'] =           $input['business_country'];
                        $inputs['business_city'] =              $input['business_city'];
                        $inputs['lat'] =                        $input['lat'];
                        $inputs['lon'] =                        $input['lon'];
                        $inputs['validate'] =                   $tran_rec;
                        $inputs['created'] =                    date("Y-m-d H:i:s");
                        $inputs['status'] =                     0;

                        $insert_id = IR(BUSINESS, $inputs);
                        if (!empty($insert_id)) {

                            $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('id' => (string)$insert_id));                                
                        }
                    // }else{
                    //     $resp = $check;
                    // }
                }else{
                    $resp = array ('status' =>ERROR, 'message' => "Business has Already been Added", 'response' => new stdClass);
                }
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Get Business Listing for update
        * @param $required 
        */

        public function getBusiness_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id','business_id', 'user_language');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){

                $buss = SR(BUSINESS, array('business_id' => $input['business_id'], 'status !='=> 3));
                if (!empty($buss)) {
                
                $buss['cat_name'] = idToName(CATEGORY,$buss['cat_id'],"cat_name");
                $buss['sub_cat_name'] = idToName(CATEGORY,$buss['sub_cat_id'],"cat_name");
                $buss['business_image1'] = BUSINESSPATH . $buss['business_image1'];
                $buss['business_image2'] = BUSINESSPATH . $buss['business_image2'];
                $buss['business_image3'] = BUSINESSPATH . $buss['business_image3'];
                $buss['business_image4'] = BUSINESSPATH . $buss['business_image4'];
                $buss['business_image5'] = BUSINESSPATH . $buss['business_image5'];
                $buss['business_image6'] = BUSINESSPATH . $buss['business_image6'];
                $buss['business_image7'] = BUSINESSPATH . $buss['business_image7'];
                $buss['business_image8'] = BUSINESSPATH . $buss['business_image8'];
                $buss['business_image9'] = BUSINESSPATH . $buss['business_image9'];
                $buss['business_image10'] = BUSINESSPATH . $buss['business_image10'];
                $buss['business_logo'] = BUSINESSPATH . $buss['business_logo'];


                $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('data' => $buss));     
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }   

            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Edit Business Listing by Users
        * @param $required 
        */
        public function editBusiness_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id','business_id','user_language','device_id','cat_id','business_title','business_email','business_mobile','business_address','business_country', 'business_city', 'business_desc','lat', 'lon', 'address_type');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){

                if (!empty($_FILES)) {
                    $names = array('business_image1','business_image2','business_image3','business_image4','business_image5','business_image6','business_image7','business_image8','business_image9','business_image10','business_logo');
                    $update = multipulImage($_FILES, $names,'business');                   
                }
                $update['user_id'] =                    $input['user_id'];
                $update['cat_id'] =                     $input['cat_id'];
                $update['business_title'] =             $input['business_title'];
                $update['business_desc'] =              $input['business_desc'];
                $update['business_address'] =           $input['business_address'];
                $update['address_type'] =               $input['address_type'];
                $update['business_email'] =             $input['business_email'];
                $update['business_mobile'] =            $input['business_mobile'];            
                $update['business_country'] =           $input['business_country'];
                $update['business_city'] =              $input['business_city'];
                $update['features_ads'] =               $input['features_ads'];
                $update['lat'] =                        $input['lat'];
                $update['lon'] =                        $input['lon'];
                $update['status'] =                     0;

                $updated = UR(BUSINESS, $update, array('business_id' => $input['business_id']));
                if (!empty($updated)) {

                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('data' => (string)$updated));
                }                    
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        *Add Service After Listing add by Users
        * @param $required 
        */

        public function addNewService_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id','business_id', 'services_name', 'user_language');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){

                $inputs['user_id'] = $input['user_id'];
                $inputs['business_id'] = $input['business_id'];
                $inputs['services_name'] = $input['services_name'];                           
                $inputs['status'] = 1;

                $insert_id = IR(SERVICES, $inputs);
                if (!empty($insert_id)) {

                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('id' => (string)$insert_id));                                
                }                    
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        public function getServices_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id', 'user_language');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                $services = AR(SERVICES, array('user_id' => $input['user_id'],'status !=' => 3));
                if (!empty($services)) {
                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => $services);
                }else{
                    $resp = array ('status' =>ERROR, 'message' => message('5'), 'response' => New stdClass);
                }                    
            }else{
                //YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }


        public function deleteService_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id', 'services_id', 'user_language');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                $services = UR(SERVICES, array('status' => 3), array('services_id' => $input['services_id']));
                if (!empty($services)) {
                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => $services);
                }else{
                    $resp = array ('status' =>ERROR, 'message' => message('4'), 'response' => New stdClass);
                }                    
            }else{
                //YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }
        

        /**
        *Add All business listing name and in by Users
        * @param $required 
        */

        public function getBusinessNameId_post() {
        
            // Check for required parameter
            $input = $this->param;
            $required = array('user_id', 'user_language');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){

                $buss = AR(BUSINESS, array('user_id' => $input['user_id']),"","");
                foreach ($buss as $nameid ) {
                    
                    if ($input['user_language'] == "ar") {
                        $results[] = array('business_id' => $nameid['business_id'], 'business_title' => $nameid['business_title_arabic']);                       
                    }else{
                        $results[] = array('business_id' => $nameid['business_id'], 'business_title' => $nameid['business_title']);
                    } 
                }
                if (!empty($results)) {
                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('data' => $results)); 
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
                
                                 
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Get List Process  
        * @param user_id 
        */
        // public function getlist_post() { 

        //         /* Check for required parameter */
        //         $input = $this->param;           
        //         $required = array('user_id','user_language', 'device_id');
        //         $Error = check_required_value($required, $input);  
        //         //print_r($Error);die;
        //         if ($Error == "0"){
        //             $date = date("Y-m-d");
        //             $whr =  array(array('user_id' => $input['user_id'], 'validate >=' => $date, 'features_ads' => 0, 'status' => "1" ),
        //                     array('user_id' => $input['user_id'], 'validate >=' => $date, 'features_ads' => 1, 'status' => "1" ),
        //                     array('user_id' => $input['user_id'],'status' => "0"));

        //             for ($i=0; $i < 3; $i++) { 
        //                 $results[$i] = AR(BUSINESS, $whr[$i], "features_ads", "DESC");
        //                 $results[$i] = $results[$i];
                        
        //                 for ($j=0; $j < count($results[$i]); $j++) {
        //                     $results[$i][$j]['business_image1'] = BUSINESSPATH . $results[$i][$j]['business_image1'];
        //                     $results[$i][$j]['business_image2'] = BUSINESSPATH . $results[$i][$j]['business_image2'];
        //                     $results[$i][$j]['business_image3'] = BUSINESSPATH . $results[$i][$j]['business_image3']; 
        //                     $results[$i][$j]['business_image4'] = BUSINESSPATH . $results[$i][$j]['business_image4'];
        //                     //$results[$i][$j]['favourite'] = $this->common_model->getfavByid($results[$i][$j]['business_id'],  $input['user_id']);
        //                     $chk = $this->common_model->getratshows();
                            
        //                     if ($chk == 1) {                  
                            
        //                         $rat = $this->common_model->getAvgByid($results[$i][$j]['business_id']);
        //                         if (!empty($rat)) {
        //                             $results[$i][$j]['rating'] = $rat;
        //                             $results[$i][$j]['ratshow'] = "1";
        //                         }else{
        //                             $results[$i][$j]['rating'] = "0";
        //                             $results[$i][$j]['ratshow'] = "1"; 
        //                         }
        //                     }else{
        //                         $results[$i][$j]['rating'] = "0";
        //                         $results[$i][$j]['ratshow'] = "0";
        //                     }
        //                     if (!empty($input['user_id'])) {
        //                         $favourite = $this->common_model->getfavByid($results[$i][$j]['business_id'],  $input['user_id']);
        //                         if (!empty($favourite)) {
        //                             $results[$i][$j]['favourite'] = $favourite;     
        //                         }else{
        //                             $results[$i][$j]['favourite'] = "0";
        //                         } 
        //                     }
        //                 }                    
        //             }                
                  
        //             if (!empty($results)) {         
        //                     $resp = array('status' => SUCCESS, 'message' => "1", 'response' => array('simple' => $results[0], 'featured' => $results[1], 'inactive' => $results[2]));
        //             }else{//    Data not found, please try again.
        //                 $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
        //             }                
        //         }else{
        //             $resp = $Error;
        //         }
                
        //         $this->response($resp);
        // }



        // public function newgetlist_post() { 

        //     /* Check for required parameter */
        //     $input = $this->param;           
        //     $required = array('user_id','user_language', 'device_id');
        //     $Error = check_required_value($required, $input);  
        //     //print_r($Error);die;
        //     if ($Error == "0"){
        //         $results = $this->common_model->myAllList($input['user_id'], $input['limit'], $input['offset1'],$input['offset2'],$input['offset3']);    
        //         //print_r($results);die;       
        //         if (!empty($results)) {         
        //                 $resp = array('status' => SUCCESS, 'message' => "1", 'response' => array('simple' => $results[0], 'featured' => $results[1], 'inactive' => $results[2]));
        //         }else{//    Data not found, please try again.
        //             $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
        //         }                
        //     }else{
        //         $resp = $Error;
        //     }
            
        //     $this->response($resp);
        // }

        /**
        *Get Business Feature listing by User_id
        * @param $required 
        */

        public function getlistfeatured_post() {

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id');
            $Error = check_required_value($required, $input);  
            //print_r($Error);die;
            if ($Error == "0"){
                $date = date("Y-m-d");
                $whr = array('user_id' => $input['user_id'],'status' => "1" ,'features_ads' => "1", /*'validate >=' => $date, */  );
                $Featured = $this->common_model->getFeaturedList($whr ,$input['limit'], $input['offset']);              
                if (!empty($Featured)) {
                    $count = CR(BUSINESS,$whr);
                    $results = $this->common_model->totalOffset($count, $input['limit']);
                    
                        $resp = array('status' => SUCCESS, 'message' => "1", 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $Featured));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }                
            }else{
                $resp = $Error;
            }
            
            $this->response($resp);
        }

        /**
        *Get Business Simple listing by User_id
        * @param $required 
        */

        public function getlistActive_post() {

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id');
            $Error = check_required_value($required, $input);  
 
            if ($Error == "0"){
                $date = date("Y-m-d");
                $whr = array('user_id' => $input['user_id'], /*'validate >=' => $date,*/ 'features_ads' => "0", 'status' => "1" );
                $Active = $this->common_model->getFeaturedList($whr ,$input['limit'], $input['offset']);             
              
                if (!empty($Active)) {

                    $count = CR(BUSINESS,$whr);
                    $results = $this->common_model->totalOffset($count, $input['limit']);

                    $resp = array('status' => SUCCESS, 'message' => "1", 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $Active));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }                
            }else{
                $resp = $Error;
            }
            
            $this->response($resp);
        }

        /**
        *Get Business DeActive listing by User_id
        * @param $required 
        */

        public function getlistDeActive_post() {

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id');
            $Error = check_required_value($required, $input);  

            if ($Error == "0"){
                $date = date("Y-m-d");
                $whr = "user_id = '".$input['user_id']."' && ( status = 0 ||  status = 2 ) "; 
                //$whr = array('user_id' => $input['user_id'], 'status' => "0"/*, 'validate >=' => $date, 'features_ads' => "1"*/  );
                $Deactive = $this->common_model->getFeaturedList($whr ,$input['limit'], $input['offset']);
       
                if (!empty($Deactive)) {    
                    $count = CR(BUSINESS,$whr);
                    $results = $this->common_model->totalOffset($count, $input['limit']);     
                    $resp = array('status' => SUCCESS, 'message' => "1", 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $Deactive));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }                
            }else{
                $resp = $Error;
            }
            
            $this->response($resp);
        }
        /**
        *  Get  business list
        * @param user_id,user_language,device_id,cat_id
        * cat_id offesnal
        */

        public function getbusinesslist_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_language','device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){         
                $busslist = $this->common_model->getBussinessList($input['user_id'],$input['cat_id'],$input['limit'],$input['offset']); 

                if (!empty($busslist)) { 
                    $results = $this->common_model->countmaxoffset(BUSINESS, array('status' => 1), $input['limit']);
                    //print_r($results['numberOffset']);die;
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$results['count'], 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $busslist));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        *  Get business list By lat lon
        * @param user_id,user_language,device_id,cat_id
        * cat_id offesnal
        */
        public function bussListbyLatLong_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_language','device_id', 'lat', 'lon');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){         
                $busslist = $this->common_model->bussListLatLong($input['lat'],$input['lon'],$input['cat_id'],$input['user_id'],$input['limit'],$input['offset']);



                if (!empty($busslist)) {
                    $count = $this->common_model->countbussListLatLong($input['lat'],$input['lon'],$input['cat_id']);
                    $results = $this->common_model->totalOffset($count, $input['limit']);
                    
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $busslist));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        * Get Favrate listing Process  
        * @param user_id 
        */
        public function getfavlisting_post() {

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id');
            $Error = check_required_value($required, $input);  

            if ($Error == "0"){ 

                $fdata = $this->common_model->allFavBusinessList($input['user_id'], $input['limit'],$input['offset']);
                if (!empty($fdata)) {

                    $count = $this->common_model->countFavBusinessList($input['user_id']);
                    $results = $this->common_model->totalOffset($count, $input['limit']);

                    $resp = array('status' => SUCCESS, 'message' => message('37'),  'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $fdata));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
            }else{
                $resp = $Error;
            }            
            $this->response($resp);
        }

        /**
        *  Get Favrate business list
        * @param user_id,user_language,device_id
        */  
        
        public function myfavbusslist_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){               
                
                
                $busslist = $this->common_model->allFavBusinessList($input['user_id'], $input['limit'],$input['offset']);
                if (!empty($busslist)) {

                    $count = $this->common_model->countFavBusinessList($input['user_id']);
                    $results = $this->common_model->totalOffset($count, $input['limit']);
                    

                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $busslist));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }

            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        *  Get  business Detailes with services listing and offers listing
        * @param user_id,user_language,device_id,
        */
        public function getbusinessdetails_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_language','device_id');            
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                if (!empty($input['user_id'])) {
                    $uid = $input['user_id'];
                }else{
                    $uid ="";
                }               

                $business = $this->common_model->getDetailes(array('business_id'=> $input['business_id']), $uid );
    

                if (!empty($business['business_id'] != "")) {             

                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $business));
                }else{
                    $resp = array('status' => ERROR, 'message' => message('5'), 'response' => new stdClass);
                }

            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        *  Get Enquiry of business 
        * @param user_id,user_language,device_id
        */ 

        public function getEnquiry_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){
                //$enquiry = AR(ENQUIRY,array('user_id' => $input['user_id']),'id', 'DESC');
                $enquiry = $this->common_model->getEnquiryByid($input['user_id'], $input['limit'],$input['offset']);

                
                if (!empty($enquiry)) {
                	$count = $this->common_model->countEnquiryByid($input['user_id']);
                	$results = $this->common_model->totalOffset($count, $input['limit']);
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $enquiry)); 
                }else{
                    $resp = array('status' => ERROR, 'message' => message('5'), 'response' => new stdClass);
                }
                
            
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        * Add Rating & Review Process  
        * @param user_id 
        */
        public function addratingreview_post() {

            /* Check for required parameter */
            $input = $this->param;         
            $required = array('user_id','user_language', 'device_id', 'business_id', 'rating' );
            $Error = check_required_value($required, $input); //print_r($Error);die;              
            if ($Error == "0"){
                $inputs = array(                           
                                'name' =>         $input['name'],
                                'email' =>        $input['email'],
                                'business_id' =>  $input['business_id'], 
                                'rating' =>       $input['rating'],
                                'user_id' =>      $input['user_id'],     
                                'description' =>  $input['review'],
                                'create_date' =>  date('Y-m-d H:i:s'),
                                'status' =>       0

                                );

                
                $cheack = SR(RATE, array('user_id' => $input['user_id'],'business_id' => $input['business_id'] ));
                if (empty($cheack)) {
                    $rate = IR(RATE, $inputs);
                }else{
                    $rate = UR(RATE, $inputs, array('user_id' => $input['user_id'],'business_id' => $input['business_id'] ));
                }
                if (!empty($rate)) {
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $rate));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
            }else{
                $resp = $Error;
            }            
            $this->response($resp);
        }

        /**
        * Get Rating Process  
        * @param user_id 
        */
        // public function oldgetrating_post() { 

        //     /* Check for required parameter */
        //     $input = $this->param;           
        //     $required = array('user_language', 'device_id');
        //     $Error = check_required_value($required, $input);              
        //     if ($Error == "0"){ 
        //         //$rate = AR(RATE, array('business_id' => $input['business_id']));

        //         //print_r($rate);die;
        //         $busstitle = SR(BUSINESS,array('business_id' => $input['business_id']));
                
        //         for ($i=0; $i < count($rate); $i++) {
        //             $rating[$i]['business_title'] = $busstitle['business_title'];
        //             $rating[$i]['id'] = $rate[$i]['id'];
        //             $rating[$i]['business_id'] = $rate[$i]['business_id'];
        //             $rating[$i]['user_id'] = $rate[$i]['user_id'];                    
        //             $rating[$i]['rate'] = $rate[$i]['rating'];
        //             $rating[$i]['review'] = $rate[$i]['description'];
        //             $rating[$i]['cd'] = $rate[$i]['create_date'];
        //             $rating[$i]['name'] = $rate[$i]['name'];
        //             $profile = SR(USERS,array('user_id' => $rate[$i]['user_id']));
        //             $rating[$i]['image'] = UPLOADPATH.$profile['user_profile_pic']; 
        //         }

                
        //         //$rate['business_title'] = $ratei['business_title'];
        //         if (!empty($rating)) {         
        //             $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $rating));
        //         }else{//    Data not found, please try again.
        //             $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
        //         }
        //     }else{
        //         $resp = $Error;
        //     }            
        //     $this->response($resp);
        // }


        public function getrating_post() { 

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_language', 'device_id', 'business_id');
            $Error = check_required_value($required, $input);              
            if ($Error == "0"){ 
                
                $busstitle = $this->common_model->getAllRatinges( $input['business_id'],$input['limit'],$input['offset']);
                               
                for ($i=0; $i < count($busstitle); $i++) {
                    $rating[$i]['business_title'] = $busstitle[$i]['business_title'];
                    $rating[$i]['id'] = $busstitle[$i]['id'];
                    $rating[$i]['business_id'] = $busstitle[$i]['business_id'];
                    $rating[$i]['user_id'] = $busstitle[$i]['user_id'];                    
                    $rating[$i]['rate'] = $busstitle[$i]['rating'];
                    $rating[$i]['review'] = $busstitle[$i]['description'];
                    $rating[$i]['cd'] = $busstitle[$i]['create_date'];
                    if (!empty($busstitle[$i]['user_firstname'])) {
                       $rating[$i]['name'] = $busstitle[$i]['user_firstname'];
                    }else{
                        $rating[$i]['name'] = $busstitle[$i]['name'];
                    }
                    
                    $rating[$i]['image'] = UPLOADPATH.$busstitle[$i]['user_profile_pic']; 
                }

                $count = $this->common_model->countAllRatinges( $input['business_id']);
                $results = $this->common_model->totalOffset($count, $input['limit']);
                
                if (!empty($rating)) {         
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'],  'response' => array('data' => $rating));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
            }else{
                $resp = $Error;
            }            
            $this->response($resp);
        }
        /**
        * Get Advertise image Process  
        * @param user_id 
        */
        public function getadvertiseimages_post() { 

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_language');
            $Error = check_required_value($required, $input);              
            if ($Error == "0"){ 

                $advertisement = $this->common_model->randomTotalRecord(ADVERTISEMENT,'add_id');  
                if (!empty($advertisement)) {              
                    for ($i=0; $i < 2; $i++) {
                       $advertisement_data[$i]['add_id'] = $advertisement[$i]['add_id'];
                       $advertisement_data[$i]['add_img_path'] = ADVERTISEMENTPATH . $advertisement[$i]['add_img_path'];

                    }  
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $advertisement_data));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
            }else{
                $resp = $Error;
            }            
            $this->response($resp);
        }
        
        /**
        * Add fav business Process  
        * @param user_id 
        */

        public function addfavbusiness_post() { 

            /* Check for required parameter */
            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id', 'business_id');
            $Error = check_required_value($required, $input);  
            if ($Error == "0"){  


                $cheack = SR(FAVOURITE, array('user_id' => $input['user_id'], 'business_id' => $input['business_id']));
                if (!empty($cheack)) {
                    if ($cheack['favourite'] == 1) {
                      $add = UR(FAVOURITE, array('favourite' => 0), array('fid' => $cheack['fid']));
                    }else{
                        $add = UR(FAVOURITE, array('favourite' => 1), array('fid' => $cheack['fid']));
                    
                    }
                    $results = SR(FAVOURITE, array('fid' => $cheack['fid']));
                            
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $results));
                }else{
                    $add = IR(FAVOURITE, array('favourite' => 1, 'user_id' => $input['user_id'], 'business_id' => $input['business_id'],));
                    $results = SR(FAVOURITE, array('fid' => $add));
                            
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $results));
                }                    
            }else{
                $resp = $Error;
            }
            
            $this->response($resp);
        }  


        /**
        *Add Offers After Listing add by Users
        * @param $required 
        */

        public function addNewOffers_post() {
        
            // Check for required parameter
            $input = $this->param;
            
            $required = array('user_id','business_id','user_language', 'start_date', 'end_date');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){

                if (!empty($_FILES)) {
                    $inputs['offer_image'] =imageUploads($_FILES['offer_image'],"offer_image",'business');
                    $inputs['user_id'] = $input['user_id'];
                    $inputs['business_id'] = $input['business_id'];
                    $inputs['start_date'] = $input['start_date'];             
                    $inputs['end_date'] = $input['end_date'];          
                    $inputs['status'] = 0;
                    
                    $insert_id = IR(OFFERS, $inputs);
                    if (!empty($insert_id)) {

                        $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('id' => (string)$insert_id));                                
                    }
                }else{
                    $resp = array ('status' =>ERROR, 'message' => "Please select offer image", 'response' => new stdClass);    
                }
                                    
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }     

        /**
        * Get offers Process  
        * @param user_id 
        */
        // public function oldoffers_post() {
        
            //     /* Check for required parameter */

            //     $input = $this->param;
            //     $offers = $this->common_model->randomTotalRecord(OFFERS, 'offer_id');
            //     //print_r($offers);die;
            //     $count = count($offers);
            //     for ($i=0; $i < $count; $i++) {
            //         $results[$i]['offer_id'] = $offers[$i]['offer_id'];
            //         //$results[$i]['offer_name'] = $offers[$i]['offer_name'];
            //         $results[$i]['business_id'] = $offers[$i]['business_id'];
            //         $results[$i]['validate'] = $offers[$i]['validate'];
            //         $results[$i]['offer_image'] = BUSINESSPATH.$offers[$i]['offer_image'];
                   
            //     }

            //     $count = $this->common_model->countAllRatinges( $input['business_id']);
            //     $results = $this->common_model->totalOffset($count, $input['limit']);


            //     $resp = array('status' => SUCCESS, 'message' => "1", 'response' => array('data' => $results));
            //     $this->response($resp);
        // }
        public function offers_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $offers = $this->common_model->randomTotalOffers(OFFERS, 'offer_id', $input['limit'], $input['offset']);
            $count = count($offers);
            for ($i=0; $i < $count; $i++) {
                $results[$i]['offer_id'] = $offers[$i]['offer_id'];
                $results[$i]['business_id'] = $offers[$i]['business_id'];
                $results[$i]['start_date'] = $offers[$i]['start_date'];
                $results[$i]['end_date'] = $offers[$i]['end_date'];
                $results[$i]['offer_image'] = BUSINESSPATH.$offers[$i]['offer_image'];
                //$results[$i]['validate'] = $offers[$i]['validate'];
                //$results[$i]['offer_name'] = $offers[$i]['offer_name'];               
            }
            if (!empty($results)) {    

                $count = $this->common_model->countTotalOffers(OFFERS);
                $result = $this->common_model->totalOffset($count, $input['limit']);

                $resp = array('status' => SUCCESS, 'message' => "1", 'total' => (string)$count, 'maxoffset' => (string)$result['numberOffset'], 'response' => array('data' => $results));
            }else{
                $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
            }
            $this->response($resp);
        }

        public function getMyOffersList_post() {
        
            /* Check for required parameter */

            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $offers = AR(OFFERS, array('user_id' => $input['user_id'],'status !=' => 3));

                for ($i=0; $i < count($offers); $i++) {
                $results[$i]['offer_id'] = $offers[$i]['offer_id'];
                $results[$i]['business_id'] = $offers[$i]['business_id'];
                $results[$i]['start_date'] = $offers[$i]['start_date'];
                $results[$i]['end_date'] = $offers[$i]['end_date'];
                $results[$i]['offer_image'] = BUSINESSPATH.$offers[$i]['offer_image'];                              
                }
                // print_r($results);die;
                if (!empty($results)) {

                    $resp = array('status' => SUCCESS, 'message' => "1", 'response' => array('data' => $results));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            
            $this->response($resp);
        }


        public function deleteOffers_post() {
        
            /* Check for required parameter */

            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id', 'offer_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $results = UR(OFFERS, array('status' => 3), array('offer_id' => $input['offer_id']));

                if (!empty($results)) {         
                    $resp = array('status' => SUCCESS, 'message' => "SUCCESS", 'response' => array('data' => $results));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            
            $this->response($resp);
        }

        /**
        * Get Slider Home screen Process  
        * @param user_id 
        */

        public function getslider_post() {
        
            /* Check for required parameter */

            $input = $this->param;           
            $required = array('user_id','user_language', 'device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
            	$offers = $this->common_model->randomTotalRecord(OFFERS, 'offer_id');
                $count = count($offers);
            
                for ($i=0; $i< 4; $i++) {
                    $results[$i]['offer_id'] = $offers[$i]['offer_id'];
                    $results[$i]['business_id'] = $offers[$i]['business_id'];
                    $results[$i]['user_id'] = $offers[$i]['user_id'];
                    $results[$i]['offer_name'] = $offers[$i]['offer_name'];
                    $results[$i]['offer_image'] = BUSINESSPATH . $offers[$i]['offer_image'];              
                    $results[$i]['created'] = $offers[$i]['created'];
                    $results[$i]['status'] = $offers[$i]['status'];
                }
                if (!empty($results)) {         
                    $resp = array('status' => SUCCESS, 'message' => "1", 'response' => array('data' => $results));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            
            $this->response($resp);
        }
                
        /**
        *   Languages List
        * @param user_id
        */
        public function languages_post() {

            $input = $this->param;
            $lang = TR(LANGUAGES);
            if (!empty($lang)) {
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $lang));
            }else{
                $resp = array('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
            }
            $this->response($resp);
        }
        /**
        *   Countries List
        * @param 
        */
        public function countries_post() {

            $input = $this->param;
            if ($input['user_language'] == "ar") {
               $name = "arabic";
            }else{
                $name = "name";  
            }
            
            $countries = AR(COUNTRIES, array('add_status !=' => 3),$name,"ASC");            
            if (!empty($countries)) {
                for ($i=0; $i < count($countries); $i++) { 
                        
                    $results[$i]['id'] = $countries[$i]['id'];
                    $results[$i]['phonecode'] = $countries[$i]['phonecode'];
                    $results[$i]['sortname'] = $countries[$i]['sortname'];                  
                    if ($input['user_language'] == "ar") {
                    $results[$i]['name'] = $countries[$i]['arabic'];
                    }else{
                    $results[$i]['name'] = $countries[$i]['name']; 
                    }
                }
                
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $results));
            }else{
                $resp = array('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
            }

            
            $this->response($resp);
        }

        /**
        *   City List
        * @param 
        */

        public function cities_post() {

            $input = $this->param;
            $cities = AR(CITIES, array('country_id ' => $input['country_id'],'add_status !=' => 1),"","");
            if (!empty($cities)) {
                for ($i=0; $i < count($cities); $i++) { 
                        
                    $results[$i]['id'] = $cities[$i]['id'];
                    $results[$i]['country_id'] = $cities[$i]['country_id'];                  
                    if ($input['user_language'] == "ar") {
                    $results[$i]['name'] = $cities[$i]['name_arabic'];
                    }else{
                    $results[$i]['name'] = $cities[$i]['name']; 
                    }
                }
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $results));
            }else{
                $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
            }

            
            $this->response($resp);
        }
        /**
        *  notification on/off services
        * @param user_id
        */
        public function notification_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                if ($userData['notification'] == 0) {
                    $update = array('notification' => 1);
                    $message = "32";
                }else{
                    $update = array('notification' => 0);
                    $message = "33";
                }
                $update = UR(USERS, $update, $condition);
                $resp = array('status' => SUCCESS, 'message' =>  message($message), 'response' => $update);
        
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * Update Profile Data Process  
        * @param user_id 
        */
        public function updateprofile_post() {
        
            /* Check for required parameter */

            $input = $this->param;//
            //print_r($input);print_r($_FILES);die;
            $required = array('user_id','user_language',  'user_lastname', 'device_id', 'user_firstname','user_address', 'user_city', 'user_lat','user_long', 'device_token', /*'user_country', 'user_state',*/ );
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $inputs = array('user_firstname' =>   $input['user_firstname'],
                                'user_lastname' =>   $input['user_lastname'],
                                'user_phone' =>      $input['user_phone'],
                                'user_email' =>      $input['user_email'],  
                                'user_address' =>    $input['user_address'],
                                'device_id' =>       $input['device_id'],
                                'device_type' =>     $input['device_type'],
                                'device_token' =>    $input['device_token'],
                                'user_country' =>    $input['user_country'],
                                'user_language' =>   $input['user_language'],
                                'user_city' =>       $input['user_city'],
                                'about' =>           $input['about'],
                                'user_lat' =>        $input['user_lat'],
                                'user_long' =>       $input['user_long'],

                                );
                                if (!empty($_FILES)) {
                                    $name= "user_profile_pic";
                                    $inputs[$name] = imageUploads($_FILES, $name, 'profile');
                                }

                $update = UR(USERS, $inputs, $condition);
                if ($update == 1) {
                	//User profile details has been updated sucessfully.
                    $results = SR( USERS, $condition);
                    $resp = array ('status' => SUCCESS, 'message' => message('31'), 'response' => array('data' => $results));
                } else{
                	//Some error occured, please try again.
                    $resp = array ('status' => ERROR, 'message' => message('4'), 'response' => new stdClass);    
                }
            } else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * Get Profile Data Process  
        * @param user_id 
        */
        public function getprofile_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id',/*'user_language',*/ 'device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                if ($userData) {

                   $userData['user_profile_pic'] = UPLOADPATH . $userData['user_profile_pic'];
                    //Profile data has been shown sucessfully.
                    $resp = array('status' => SUCCESS, 'message' => message('30'), 'response' => array('data' => $userData));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        * After login UPDATE PASSWORD Process  
        * @param user_id 
        */
        public function updatePassword_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language', 'device_id', 'old_password', 'new_password', 'confirm_password');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                    if($userData['user_password'] == md5($input['old_password'])) {

                        if($input['new_password'] == $input['confirm_password']) {
                            
                            if($userData['user_password'] != md5($input['new_password'])) {

                                $updateArr = array('user_password' => md5($input['new_password']));
                                $update = UR(USERS, $updateArr, $condition);
                                if ($update == 1) {//Password has been changed sucessfully.
                                    $resp = array('status' => SUCCESS, 'message' => message('27'), 'response' => new stdClass);
                                }else{//Some error occured, please try again.
                                    $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);    
                                }
                            }else{//This password has already been used. Please select another password.
                             $resp = array('status' => ERROR, 'message' => message('28'), 'response' => new stdClass);
                            }        
                        }else{//Confirm password field did not match.
                             $resp = array('status' => ERROR, 'message' => message('26'), 'response' => new stdClass);
                        }
                    }else{//Old password did not match.
                             $resp = array('status' => ERROR, 'message' => message('29'), 'response' => new stdClass);
                    }                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * RESET PASSWORD Process  
        * @param user_id 
        */
        public function changePassword_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language', 'device_id', 'new_password', 'confirm_password');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                if (!empty($userData)) {

                    if($input['new_password'] == $input['confirm_password']) {
                        
                        if($userData['user_password'] != md5($input['new_password'])) {

                            $updateArr = array('user_password' => md5($input['new_password']));
                            $update = UR(USERS, $updateArr, $condition);
                            if ($update == 1) {//Password has been changed sucessfully.
                                $resp = array('status' => SUCCESS, 'message' => message('27'), 'response' => new stdClass);
                            }else{//Some error occured, please try again.
                                $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);    
                            }
                        }else{//Confirm password field did not match.
                         $resp = array('status' => ERROR, 'message' => message('28'), 'response' => new stdClass);
                        }        
                    }else{//Confirm password field did not match.
                         $resp = array('status' => ERROR, 'message' => message('26'), 'response' => new stdClass);
                    }
                }else{//Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
               // $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => new stdClass);
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * Logout Process  
        * @param user_id 
        */
        public function logOut_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id', 'user_language', 'device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                
                if (!empty($userData)) {

                    $upd = array('is_online' => 0, 'device_id' => 0);           
                    $update = UR(USERS, $upd, $condition);
                    $resp = array('status' => SUCCESS, 'message' => message('35'), 'response' => new stdClass);
                }else{
                    //Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }               
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * Verify Otp  
        * @param user Email Address
        */
        public function verifyOtp_post() {

            $input = $this->param;
            $required = array('user_id', 'otp', 'user_language', 'device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                $checkotp = SR(OTP, array('user_id' => $input['user_id'], 'status' => 0));
                if(!empty($checkotp)){
                    if ($checkotp['otp'] == $input['otp']) {                 
                        
                        
                        $package = SR(PACKAGE, array('price' => 0, 'opt_status' => 1));
                        //print_r($package);die;

                        $free_end = date("Y-m-d", strtotime("+".$package['duration']." day"));
                        $detailes = array(  //'transaction_id' => ,
                                        //'responce_token' => $input['responce_token'],
                                        'userid' => $input['user_id'],
                                        'packageid' => $package['opt_id'],
                                        'item_name' => $package['opt_name'],
                                        'amt' => $package['opt_name'],                      
                                        'free_start'=> date("Y-m-d"),
                                        'free_end' => $free_end,
                                        'cc'=> "USD",
                                        'plan_valid'=> "1",
                                        'package_type'=> "1",
                                        'st'=> "success",
                                        'create_date'=> date("Y-m-d")
                                    );
     
                        $insert = IR(TRANSACTION, $detailes);


                        $updatestatus = UR(USERS,array('status' => 1) ,array('user_id' => $input['user_id']));
                        $upd = UR(OTP, array('status' => 1), array('user_id' => $input['user_id']));

                        $email = strip_tags($userData['user_email']);
                        $name = strip_tags($userData['user_firstname']);
                        $from = 'ilbait@gmail.com';
                        $subject = "Activate Account";
                        $message = "<p>Dear " . $name . ",</p>";
                        $message .= "<p>Your account activated successfully. Please login.  </p>";
                        $message .= "<p><br></p>";
                        $message .= "<p>Thank You,</p>";
                        $message .= "<p>Team Ilbait</p>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                        $headers .= 'From: <"' . $from . '">' . "\r\n";
                        mail($email, $subject, $message, $headers);

                        //$upd = SR(OTP, array('user_id' => $input['user_id'], 'status' => 1));
                        //One time password matched successfully.
                        $resp = array('status' => SUCCESS, 'message' => message('24'), 'response' => new stdClass);                   

                    }else{//One time password did not matched.please try again.
                        $resp = array('status' => ERROR, 'message' => message('25'), 'response' => new stdClass);
                    }
                }else{//Some error occured, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                //$resp = $Error;
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * Verify Otp for forgot password  
        * @param user Email Address
        */
        public function verifyOtpForgot_post() {

            $input = $this->param;
            $required = array('user_id', 'otp', 'user_language', 'device_id');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){
                $checkotp = SR(OTP, array('user_id' => $input['user_id'],'status' => 0));
                if(!empty($checkotp)){
                    if ($checkotp['otp'] == $input['otp']) {
                        $upd = UR(OTP, array('status' => 1), array('user_id' => $input['user_id']));
                        $resp = array('status' => SUCCESS, 'message' => message('24'), 'response' => new stdClass);
                    }else{//One time password did not matched.please try again.
                        $resp = array('status' => ERROR, 'message' => message('25'), 'response' => new stdClass);
                    }
                }else{//Page Not Found
                    $resp = array('status' => ERROR, 'message' => message('20'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_                
                $resp = $Error;
            }

            $this->response($resp);
        }
        /**
        * Forgot password 
        * @param user Email Address
        */
        public function forgot_post() {

            $input = $this->param;
            $required = array('user_email', 'user_language', 'device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                $email = strtolower($input['user_email']);
                $checkUser = SR(USERS, array('user_email' => $email));

                if(!empty($checkUser)){

                    $otp= rand(100000,999999);
                    $user_id = $checkUser['user_id'];
                    $user_firstname = $checkUser['user_firstname'];
                    $single = SR(OTP,array('user_id' => $user_id ));
                    if (!empty($single)) {
                        $update =UR(OTP, array("otp" => $otp, 'status' => 0), array('user_id' => $user_id ) );
                    }else{
                        $update = IR(OTP, array("otp" => $otp, 'status' => 0));
                    }
                    
                    if(!empty($update)){                        

                            $from = 'ilbait@gmail.com';
                                                    
                            $subject = "OTP Message";
                            $message = "<p>Dear " . $user_firstname . ",</p>";
                            $message .= "<p>This email address is being used to a forget password Verification . If you initiated the Forget Password process, it is asking you to enter the numeric verification code that appears below.</p>";
                            $message .= "<p><b style='background: green'>".$otp."</b></p>";
                            $message .= "<p><br></p>";
                            $message .= "<p>Thank You,</p>";
                            $message .= "<p>Team Ilbait</p>";
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                            $headers .= 'From: <"' . $from . '">' . "\r\n";

                            mail($email, $subject, $message, $headers);
                                              
                        //We have sent one time password on your registered Email id, Please check Email id.
                        $resp = array('status' => SUCCESS, 'message' => message('23'), 'response' => array('user_id' => (int)$user_id));

                    }else{//Some error occured, please try again.
                        $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);
                    }
                }else{//Sorry, there is no account associated with this mobile number.
                    $resp = array('status' => NOT_FOUND, 'message' => message('8'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                // $resp = $Error;
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Users login service
        * @param request in $data
          required parameter : user_email , user_password
        */
        public function login_post() {

            $input = $this->param;
            $required = array('user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){

                $condition = array ( 'user_email' => $input['user_email'],
                                    'user_password' => md5($input['user_password']),
                                    /*'status' => 1*/);      
                          
                $check_login = SR(USERS, $condition);
                if(!empty($check_login)) {
                    if ($check_login['status'] == "1") {

                        $update_input = array ( 'device_type' => $input['device_type'],
                                                'device_id' => $input['device_id'],
                                                'device_token' => $input['device_token'],
                                                'is_online' => 1);
                        $update_condition = array ('user_id' => $check_login['user_id'] ); 
                        $update = UR( USERS, $update_input, $update_condition);
                        $results = SR( USERS, $update_condition);
                        //You have successfully logged in.
                        $resp = array ('status' => SUCCESS, 'message' => message('22'), 'response' => array('data' => $results, 'url' => UPLOADPATH));
                    }elseif ($check_login['status'] == "0") {
                        $results = array('user_id' => $check_login['user_id'], 'status' => $check_login['status']);
                        $resp = array ('status' => ERROR, 'message' => message('13'), 'response' =>$results);
                    }else{
                        $resp = array ('status' => ERROR, 'message' => message('15'), 'response' => new stdClass);
                    }
                }else {//Email Id or password is not correct, please try again.
                    $resp = array ('status' => ERROR, 'message' => message('11'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                // $resp = $Error;
                $resp = $Error;

            }
            $this->response($resp);
        }

        /**
        * Users Sginup resend otp service
        * @param request in $data
          required parameter : user_id, 'device_id', 'device_type', 'device_token', 'user_language',
        */
        public function reotpverify_post() {

            $input = $this->param;
            $required = array ('user_id', 'device_id','device_token', 'user_language');
            $Error = check_required_value($required, $input);            
            if ($Error == "0"){
                
                $checkUser = SR( USERS, array('user_id' => $input['user_id']));
                if (!empty($checkUser)) {
                   $condition = array('user_id' => $input['user_id']);
                    $otp = rand(100000,999999);
                    $update_otp = UR( OTP, array('otp' => $otp), array('user_id' => $input['user_id']));

                    if (!empty($update_otp)) {

                        $from = 'ilbait@gmail.com';
                        $email = strip_tags($checkUser['user_email']);
                        $subject = "OTP Message";
                        $message = "<p>Dear User,</p>";
                        $message .= "<p>This email address is being used to a signup Verification . If you initiated the signup process, it is asking you to enter the numeric verification code that appears below.</p>";
                        $message .= "<p><b style='background: green'>".$otp."</b></p>";
                        $message .= "<p><br></p>";
                        $message .= "<p>Thank You,</p>";
                        $message .= "<p>Team Ilbait</p>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                        $headers .= 'From: <"' . $from . '">' . "\r\n";
                        mail($email, $subject, $message, $headers);
                        //We have sent one time password on your registered Email id, Please check Email id.
                        $resp = array ('status' => SUCCESS, 'message' => message('23'), 'response' => new stdClass);                              
                    }else{//Some error occured, please try again.
                        $resp = array ('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);
                    }
                }else{//Email Already Exist in our database
                    $resp = array ('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        * Users Sginup service
        * @param request in $data
          required parameter : 'user_firstname', 'user_lastname', 'user_phone', 'user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language',
        */
        public function signup_post() {

            $input = $this->param;
            $required = array ('user_firstname', 'user_lastname','user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language','user_long','user_lat');
            $Error = check_required_value($required, $input);            
            if ($Error == "0"){
                
                $check_email = CR(USERS, array('user_email' => $input['user_email']));
                if ($check_email == "0") {
                    $inputs = array('user_firstname' =>  $input['user_firstname'],
                                    'user_lastname' =>   $input['user_lastname'],
                                    'user_phone' =>      $input['user_phone'],
                                    'user_email' =>      $input['user_email'],
                                    'user_password' =>   md5($input['user_password']),
                                    'device_id' =>       $input['device_id'],
                                    'device_type' =>     $input['device_type'],
                                    'device_token' =>    $input['device_token'],
                                    'social_login' =>    $input['social_login'],                                    
                                    'user_country' =>    $input['user_country'],
                                    'user_language' =>   $input['user_language'],
                                    //'user_city' =>       $input['user_city'],
                                    'about' =>           $input['about'],
                                    'user_lat' =>        $input['user_lat'],
                                    'user_long' =>       $input['user_long']
                                    );

                    $insert_id = IR( USERS, $inputs);
                    if (!empty($insert_id)) {

                        $otp= rand(100000,999999);
                        $insert_otp = IR( OTP, array('user_id' => $insert_id, 'otp' => $otp, 'status' => 0));
                        
                        $email = "".$input['user_email'].",votivemobile.dilip@gmail.com,zubaer.votive@gmail.com,";
                        $from = 'ilbait@gmail.com';
                        $email = strip_tags($input['user_email']);
                        $name = strip_tags($input['user_firstname']);
                        $subject = "OTP Message";
                        $message = "<p>Dear " . $name . ",</p>";
                        $message .= "<p>This email address is being used to a signup Verification . If you initiated the signup process, it is asking you to enter the numeric verification code that appears below.</p>";
                        $message .= "<p><b style='background: green'>".$otp."</b></p>";
                        $message .= "<p><br></p>";
                        $message .= "<p>Thank You,</p>";
                        $message .= "<p>Team Ilbait</p>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                        $headers .= 'From: <"' . $from . '">' . "\r\n";
                        mail($email, $subject, $message, $headers);

                        //We have sent one time password on your registered Email id, Please check Email id.
                        $resp = array ('status' => SUCCESS, 'message' => message('23'), 'response' => array('user_id' =>  $insert_id));                        
                    }else{
                        $resp = array ('status' => SERVER_ERROR , 'message' => message('4'), 'response' => new stdClass);  
                    }                   

                }else{

                	$check_email = SR(USERS, array('user_email' => $input['user_email']));

                	if ($check_email['status'] == "0") {
                        $results = array('user_id' => $check_email['user_id'], 'status' => $check_email['status']);
                		$resp = array ('status' => ERROR , 'message' => message('13'), 'response' => $results); 
                	}else{
                		//Email Already Exist in our database
                		$resp = array ('status' => ERROR, 'message' => message('7'), 'response' => new stdClass);
                	}
                }
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        * social sginup service
        * @param request in $data
          required parameter
        */
        public function socialsignup_post() {

            $input = $this->param;
            $required = array ('user_firstname', 'user_lastname','user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language');
            $Error = check_required_value($required, $input);            
            if ($Error == "0"){

                $inputs['user_firstname'] = $input['user_firstname'];
                $inputs['user_lastname'] = $input['user_lastname'];
                $inputs['user_email'] = $input['user_email'];
                //$inputs['user_password'] = md5($input['user_password']);
                $inputs['device_id'] = $input['device_id'];
                $inputs['device_type'] = $input['device_type'];
                $inputs['device_token'] = $input['device_token'];
                $inputs['user_country'] = $input['user_country'];
                $inputs['user_language'] = $input['user_language'];
                //$inputs['user_city'] = $input['user_city'];
                $inputs['about'] = $input['about'];
                $inputs['user_lat'] = $input['user_lat'];
                $inputs['user_long'] = $input['user_long'];
                $inputs['social_id'] = $input['social_id'];
                $inputs['social_login'] = 1;
                $inputs['status'] = 1;
                
                $check_email = SR(USERS, array('user_email' => $input['user_email'],'social_login' => 1));
                if (empty($check_email)) { 
                    
                    $insert_id = IR( USERS, $inputs);
                    if (!empty($insert_id)) {
                        $results = SR(USERS, array('user_id' => $insert_id ));
                        $resp = array ('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' =>  $results));                        
                    }else{
                        $resp = array ('status' => SERVER_ERROR , 'message' => message('4'), 'response' => new stdClass);  
                    }
                }else{
                    

                    if ($check_email['status'] == 1 || $input['social_login'] == 1 ) {
                        
                        $upd = UR(USERS, $inputs, array('user_id' => $check_email['user_id']));
                       
                        if (!empty($upd)) {
                            
                            $results = SR(USERS, array('user_id' => $check_email['user_id']));
                            $resp = array ('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' =>  $results)); 
                        }else{
                            $resp = array ('status' => SERVER_ERROR , 'message' => message('4'), 'response' => new stdClass);  
                        }
                        
                    }else{
                        //Email Already Exist in our database
                        $resp = array ('status' => ERROR, 'message' => message('7'), 'response' => new stdClass);

                    } 
                }
                    
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_                
                $resp = $Error;
            }
            $this->response($resp);
        }



       
    /*___________________________________-SUMIT END HEAR-___________________________*/

        
         

        
        /**
        *  Submit enquiry form
        * @param name,email,subject,message,user_language
        */
        //  Submit enquiry form 
        public function enquiry_post() {

            $input = $this->param;
            $required = array ('business_id', 'user_id', 'user_language','device_id','name', 'email', 'email_from' ,'subject', 'message');
            $Error = check_required_value($required, $input);
            //print_r($chk_error);die;
            if ($Error == "0"){                
                $inputs = array('name' =>  $input['name'],
                                'email' => $input['email'],
                                'user_id' => $input['user_id'],
                                'email_from' => $input['email_from'],
                                'subject' => $input['subject'],
                                'message' => $input['message'],
                                'business_id' => $input['business_id']
                                ); 
                $insert_id = IR(ENQUIRY, $inputs);

                if (!empty($insert_id)) {

                    $from = strip_tags($input['email_from']);
                    $subject = "Enquiry";
                    $email = strip_tags($input['email']);
                    $name = strip_tags($input['name']);
                    $message = "<p>Dear " . $name . ",</p>";
                    $message .= "<p>Enquiry send successfully.our team contact shortly you.</p>";
                    $message .= "<p><br></p>";
                    $message .= "<p>Thank You,</p>";
                    $message .= "<p>Team Ilbait</p>";
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                    $headers .= 'From: <"' . $from . '">' . "\r\n";
                    mail($email, $subject, $message, $headers);

                    $resp = array ('status' => SUCCESS, 'message' => message('34'), 'response' => array('id' => (string)$insert_id));
                } 
            } else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        *  Add business
        * @param user_id,user_language,device_id,cat_id,business_title,business_email,business_mobile,  business_address,'business_country,business_about,services_name
        */  
        // public function addBusiness_post() {
        
        //     // Check for required parameter

        //     $input = $this->param;
        //     $required = array('user_id','user_language','device_id','cat_id','business_title','business_email','business_mobile','business_address','business_country','business_about','services_name','lat', 'lon');
        //     $chk_error = check_required_value($required, $input);        
        //     if ($chk_error == "0"){

        //         if (!empty($_FILES)) {
        //             $names = array('business_image1','business_image2','business_image3','business_image4');      
        //             $inputs = multipulImage($_FILES, $names,'business');                   
        //         }
        //         $inputs['user_id'] =           $input['user_id'];
        //         $inputs['cat_id'] =            $input['cat_id'];
        //         $inputs['business_title'] =    $input['business_title'];
        //         $inputs['business_email'] =    $input['business_email'];
        //         $inputs['business_mobile'] =   $input['business_mobile'];
        //         $inputs['business_address'] =  $input['business_address'];
        //         $inputs['business_country'] =  $input['business_country'];
        //         $inputs['business_about'] =    $input['business_about'];
        //         $inputs['features_ads'] =      $input['features_ads'];
        //         $inputs['validate'] =          $input['validate'];
        //         $inputs['lat'] =               $input['lat'];
        //         $inputs['lon'] =               $input['lon'];
        //         $inputs['status'] =            2;

        //         $insert_id = IR(BUSINESS, $inputs);
        //         if (!empty($insert_id)) {

        //             if(!empty($_FILES['offer_image'])){
                        
        //                 $files = $_FILES;
        //                 $count = count($_FILES['offer_image']['name']);
        //                 for($i=0; $i<$count; $i++)
        //                 {                           
        //                     $_FILES['offer_image']['name']= $files['offer_image']['name'][$i];
        //                     $_FILES['offer_image']['type']= $files['offer_image']['type'][$i];
        //                     $_FILES['offer_image']['tmp_name']= $files['offer_image']['tmp_name'][$i];
        //                     $_FILES['offer_image']['error']= $files['offer_image']['error'][$i];
        //                     $_FILES['offer_image']['size']= $files['offer_image']['size'][$i]; 
                            
        //                     $inputoffer['offer_image'] =imageUploads($_FILES['offer_image'],"offer_image",'offers');
        //                     $inputoffer['offer_name'] = $input['offer_name'][$i];
        //                     $inputoffer['business_id'] = $insert_id;
        //                     $inputoffer['user_id'] = $input['user_id'];
        //                     $inputoffer['validate'] = $input['validate'];                            
        //                     $inputoffer['status'] = 1;                            
        //                     $ser = IR(OFFERS, $inputoffer);
        //                 }
        //             }

        //             //services multiple add 
        //             $ser= $input['services_name'];
        //             if($ser!='') {
        //             $servicesArray = explode(',', $ser);
        //                 for($i = 0; $i < count($servicesArray); $i++) {

        //                 $inputservices = array( 'services_name' =>  $servicesArray[$i],
        //                                         'business_id' => $insert_id,
        //                                         'user_id' => $input['user_id'],
        //                                         'status' => 1
        //                                         );        
        //                     $ser = IR(SERVICES, $inputservices);  
        //                 }
        //             }
        //             $fav_inputs = array('user_id' => $input['user_id'],'business_id' => $insert_id);
        //             $fav_insert = IR(FAVOURITE,$fav_inputs);  
        //         $resp = array ('status' =>'1', 'message' => message('36'), 'response' => array('id' => $insert_id));
                                
        //         }  
                    
        //     } else{//YOU_HAVE_MISSED_A_PARAMETER_
        //         $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => new stdClass);
        //     }

        //     $this->response($resp);
        // } 
        
        /**
        *  Notification list user _id
        * @param user_id,user_language,token   ,
        */ 
        public function showNotification_post() {

            /* Check for required parameter */
        
            $input = $this->param;
            $object_info = $input;
            $limit = $input['limit'];
            $offset = $input['offset'];
            $user_id = $input['user_id'];
            $device_id = $input['device_id'];
            $required_parameter = array('user_language','device_id','user_id','limit','offset');
            $Error = check_required_value($required_parameter, $object_info); 
            if ($Error == "0"){

                $condition = array('noti_userid'=> $user_id);
                $count = CR(NOTIFICATIONLIST,$condition); 
                $offset = $limit*$offset;
                $userData = $this->common_model->getDataInPagination(NOTIFICATIONLIST,$limit,$offset,$condition);
                //print_r($userData);die;
                $d = $count/$limit; 
                $number = intval($d,0);
                    if(fmod($d, 1) !== 0.00){
                       $numberOffset = $number;
                    }elseif ($d == 0) {
                        $numberOffset = 0;
                    }else{
                        $numberOffset = $number-1;
                    }
                    for ($i=0; $i < count($userData); $i++) {
                        $userData[$i]['created'] = date('d-m-Y H:i A', strtotime($userData[$i]['created'] ));
                    }  
                    if (!empty($userData)) {
                        $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => $count, 'maxoffset' => $numberOffset, 'response' => $userData);
                    }else{
                        $resp = array('status' => ERROR, 'message' => message('5'), 'response' => new stdClass); 
                    }
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                    $resp = $Error;
            }
            $this->response($resp);
        }         

        /**
        *  Category by   business listing  offers and services 
        * @param user_language user_id,device_id,cat_id
        */
        public function getCategoryBusiness_post() {
            
                //check for required parameter 

                $input = $this->param;
                $required = array('user_language','user_id','device_id','cat_id');
                $Error = check_required_value($required, $input);        
                if ($Error == "0"){
                 

                $catbusinesslist = $this->common_model->getcatuseridbybusinesslist(BUSINESS, array('user_id' => $input['user_id'], 'cat_id' => $input['cat_id'], 'status' => 1  ));   
                
                $businesscount = count($catbusinesslist);
                
                


                for ($i=0; $i < $businesscount  ; $i++) { 
                $catbusinesslist[$i]['services'] = AR(SERVICES, array('business_id' => $catbusinesslist[$i]

                ['business_id'],'user_id' => $input['user_id']), "", "");

                    $catbusinesslist[$i]['offers'] = AR(OFFERS, array('business_id' => $catbusinesslist[$i]

                ['business_id'],'user_id' => $input['user_id']), "", "");
                    }//print_r($businesslist); die;
                    $resp = array('status' => SUCCESS, 'message' => "show", 'response' => $catbusinesslist);             
                    
                } else{//YOU_HAVE_MISSED_A_PARAMETER_
                    $resp = $Error;
                }

                $this->response($resp);
        }

        
        /**
        *  Get category List
        * @param user_language
        */
        public function getcategory_post() {
        
            //check for required parameter 

            $input = $this->param;
            $required = array('user_language','device_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                $cat = AR(CATEGORY, array('cat_status' => 1,'cat_parent' => 0), "", "");
                if ($cat) {
                    for ($i=0; $i < count($cat); $i++) {
                        $category[$i]['cat_id'] = $cat[$i]['cat_id'];
                        $category[$i]['cat_parent'] = $cat[$i]['cat_parent'];
                        $category[$i]['cat_status'] = $cat[$i]['cat_status'];                        
                        if ($input['user_language'] == "ar") {                        
                            $category[$i]['cat_name'] = $cat[$i]['cat_namea'];                           
                            $category[$i]['cat_description'] = $cat[$i]['cat_descriptiona'];                          
                        }else{
                            $category[$i]['cat_name'] = $cat[$i]['cat_name'];
                            $category[$i]['cat_description'] = $cat[$i]['cat_description'];    
                        }
                        $category[$i]['cat_img_path'] = CATEGORYPATH.$cat[$i]['cat_img_path'];
                        $category[$i]['subcategory'] = (string)CR(CATEGORY, array('cat_parent' => $cat[$i]['cat_id'], 'cat_status' => 1 ));                
                    
                    }
                    $resp = array('status' => SUCCESS, 'message' => "show", 'response' => array('data' => $category));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        *  Get Subcategory List
        * @param user_language
        */

        public function getSubcategory_post() {
        
            //check for required parameter 

            $input = $this->param;
            $required = array('user_language','device_id', 'cat_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

                $subcat = AR(CATEGORY, array('cat_status' => 1, 'cat_parent'=> $input['cat_id']), "", "");
                if ($subcat) {
                    for ($i=0; $i < count($subcat); $i++) {
                        $subcategory[$i]['cat_id'] = $subcat[$i]['cat_id'];
                        $subcategory[$i]['cat_parent'] = $subcat[$i]['cat_parent'];
                        $subcategory[$i]['cat_status'] = $subcat[$i]['cat_status'];                        
                        if ($input['user_language'] == "ar") {                        
                            $subcategory[$i]['cat_name'] = $subcat[$i]['cat_namea'];                           
                            $subcategory[$i]['cat_description'] = $subcat[$i]['cat_descriptiona'];                          
                        }else{
                            $subcategory[$i]['cat_name'] = $subcat[$i]['cat_name'];
                            $subcategory[$i]['cat_description'] = $subcat[$i]['cat_description'];    
                        }
                        $subcategory[$i]['cat_img_path'] = CATEGORYPATH.$subcat[$i]['cat_img_path'];                   
                    }
                    $resp = array('status' => SUCCESS, 'message' => "show", 'subcategory'=> "1", 'response' => array('data' => $subcategory));
                }else{
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'subcategory'=> "0", 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Active Deactive Listing
        * @param user_language
        */
        
        public function activeDeactive_post() {

            $input = $this->param;
            $required = array('user_language','status', 'user_id', 'business_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                if ($input['status'] == "1") {

                    $count = CR(BUSINESS, array('user_id' => $input['user_id'],'status' => 1));
                    if ($count == "0") {
                        $record = SR(BUSINESS, array('business_id' => $input['business_id']));
                        if ($record['status'] == 2) {
                           $update = UR(BUSINESS, array('status' => 1), array('business_id' => $input['business_id']));
                           $resp = array ('status' => SUCCESS, 'message' => "SUCCESS", 'response' => array('update' => $update));
                        }else{
                            $resp = array ('status' => ERROR, 'message' => "Admin approved first, Plz contact to admin", 'response' => new stdClass);
                        }

                        
                    }else{

                        $resp = array('status' => ERROR, 'message' => "You have already added business listings, you can only add one business listing", 'response' => new stdClass);
                    }
                }elseif ($input['status'] == "0"){

                    $update = UR(BUSINESS, array('status' => 2), array('business_id' => $input['business_id']));
                    $resp = array ('status' => SUCCESS, 'message' => "SUCCESS", 'response' => array('update' => $update));
                    
                }else{

                    $resp = array ('status' => SUCCESS, 'message' => "Admin approved first, Plz contact to admin", 'response' => new stdClass);
                }
            }else{
                 $resp = $Error; 
            }
            $this->response($resp);
        }



        /**
        * Get Terms Data Process  
        * @param user_id 
        */
        public function getterm_post() {

            /* Check for required parameter */

           $input = $this->param;
            $required = array('user_language', 'device_id' );
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('pg_id' => 1);
                $userData = AR("otd_page_contents",$condition);
                
                if ($userData) {
                    $resp = array('status' => SUCCESS, 'message' => "SUCCESS", 'response' => $userData);
                }else{
                    // Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        * Get About Process  
        * @param user_id 
        */
        public function getabout_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_language', 'device_id' );
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('pg_id' => 14);
                $userData = AR("otd_page_contents",$condition);                
                if ($userData) {
                    $resp = array('status' => SUCCESS, 'message' => "SUCCESS", 'response' => $userData);
                }else{
                	// Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }
        /**
        * contact us Process  
        * @param user_id 
        */

        public function contact_post() {

            $input = $this->param;
            $required = array ('name', 'email', 'message', 'mobilenumber');
            $Error = check_required_value($required, $input);
            //print_r($Error);die;
            if ($Error == "0"){                
                $inputs = array('name' =>  $input['name'],
                                'email' => $input['email'],                             
                                'message' => $input['message'],
                                //'address' => $input['address'],
                                'mobilenumber' => $input['mobilenumber'],
                                'cd' => date('Y-m-d H:i:s')
                                );                
                $from = strip_tags("votivetesting@gmail.com");//strip_tags($input['email']);
                $subject = "Contact us";
                $email = strip_tags("".$input['email'].",votivemobile.vikas@gmail.com");
                $name = strip_tags($input['name']);
                $message = "<p>Dear " . $name . ",</p>";
                $message .= "<p>Enquiry send successfully.our team contact shortly you.</p>";
                $message .= "<p><br></p>";
                $message .= "<p>Thank You,</p>";
                $message .= "<p>Team Ilbait</p>";
                $headers = "MIME-Version: 1.0" . "\r\n";
                $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                $headers .= 'From: <"' . $from . '">' . "\r\n";
                mail($email, $subject, $message, $headers);
                
                $insert_id = IR(CONTACTUS, $inputs);
                $resp = array ('status' => SUCCESS, 'message' => message('17'), 'response' => array('id' => (string)$insert_id));
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                  $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        * Business Delete Process  
        * @param user_id 
        */

        public function businessDelete_post() {
        
            // Check for required parameter
            $input = $this->param;
            $required = array('user_id','business_id','user_language');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                $chk = CR(BUSINESS,array('business_id' => $input['business_id'], 'status !=' => 3));
                
                if ($chk != 0) {

                    $update = UR(BUSINESS, array('status'=> 3), array('business_id' => $input['business_id']));
                    $update1= UR(OFFERS, array('status'=> 3), array('business_id' => $input['business_id']));
                    $update2= UR(SERVICES, array('status'=> 3), array('business_id' => $input['business_id']));
                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'response' => array('data' => (string)$update)); 
                }else{
                    $resp = array('status' => ERROR, 'message' => message('12'), 'response' => new stdClass);
                }                                    
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        /**
        * Get Transaction History  
        * @param user_id 
        */

        public function transaction_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){

                $transaction = $this->common_model->transactionHistory($input['user_id'],$input['limit'], $input['offset']);
                
                if (!empty($transaction)) {
                    $count = CR(TRANSACTION, array('userid' => $input['user_id']));
                    $results = $this->common_model->totalOffset($count, $input['limit']);


                    $resp = array ('status' =>SUCCESS, 'message' => message('37'), 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $transaction));                                
                }else{
                    $resp = array ('status' => NOT_FOUND, 'message' =>message('5'), 'response' => array()); 
                }                    
            }else{
            	//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Get Token in jeson  
        * @param user_id 
        */
        public function getToken_post() {
            $token = $this->braintree_lib->create_client_token();
            
            if(!empty($token)){
                $resp = array ('status' => SUCCESS, 'message' => "SUCCESS", 'response' => $token); 
                }
            else{
                $resp = array ('status' => ERROR, 'message' => "ERROR", 'response' => array()); 
            }
            $this->response($resp);
        }
        /**
        * Get Token
        * @param user_id 
        */
        public function Token_post() {
            $token = $this->braintree_lib->create_client_token(); 

            if(!empty($token)){
               print_r($token);
            }
            else{
                $resp = array ('status' => ERROR, 'message' => "ERROR", 'response' => array()); 
            }
            $this->response($resp);
        }

        // public function forFreeListingPay_post()
        // {
        
        //     //$input = json_decode(file_get_contents("php://input"),true);
        //     $input = $this->param;
        //     $required = array('userid','user_language','device_id', 'packageid', 'item_name', 'amt', 'validity');
        //     $Error = check_required_value($required, $input);     
        //     if ($Error == "0"){


        //         $count = CR(TRANSACTION, array('userid'=> $input['userid'], 'free_end <'=> date("Y-m-d"), 'st' => "success", 'transaction_id' => "", 'responce_token' => ""));

        //             if ($count == "0") {

        //                 $free_end = date("Y-m-d", strtotime("+".$input['validity']." day"));
        //                 $detailes = array(
        //                                     'userid' => $input['userid'],
        //                                     'packageid' => $input['packageid'],
        //                                     'item_name' => $input['item_name'],
        //                                     'amt' => $input['amt'],                      
        //                                     'free_start'=> date("Y-m-d"),
        //                                     'free_end' => $free_end,
        //                                     'cc'=> "USD",
        //                                     'plan_valid'=> "1",
        //                                     'package_type'=> "1",
        //                                     'st'=> "success",
        //                                     'create_date'=> date("Y-m-d")
        //                                 );
     
        //                 $insert = IR(TRANSACTION, $detailes);

        //                 $resp = array ('status' => SUCCESS, 'message' => "success!", 'response' => array('insert_id'=> $insert)); 
        //             }else{
        //                 $resp = array ('status' => ERROR, 'message' => "You have already used the free plan", 'response' => new stdClass);
        //             }
                    
        //     }else{
        //         $resp = $Error;
        //     }
        //     $this->response($resp);
        // }
        /**
        * pay for listing process
        * @param user_id 
        */
        public function nonceValues_post()
        {
        
            //$input = json_decode(file_get_contents("php://input"),true);
            $input = $this->param;
            $required = array('userid','user_language','device_id', 'packageid', 'item_name', 'responce_token', 'amt', 'validity');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){

                $where= " userid = '".$input['userid']."' && plan_valid = 1 && ( package_type = 1 || package_type = 2 ) && st = 'success' "; 
                $count = SR(TRANSACTION, $where);
                
                if ($count) {
                    $tid = $count['tid'];
                    $today = strtotime(date("Y-m-d"));
                    $enddate = strtotime($count['free_end']);
                    if ($today < $enddate) {
                        $free_end =  date('Y-m-d', strtotime($count['free_end']. ' + '.$input['validity'] .'days'));
                    }else{
                        $free_end = date("Y-m-d", strtotime("+".$input['validity']." day"));
                    }
                    
                
                    $responce_token   = $input['responce_token'];
                    
                    // Instantiate a Braintree Gateway either like this:
                    $gateway = new Braintree_Gateway([
                        'environment' => 'sandbox',
                        'merchantId' => 'dn9hfq6926hgwd2b',
                        'publicKey' => 'ctvv43yjk5v3rc8q',
                        'privateKey' => 'bc77ccaf3af327ed357fdfec240807b1'
                    ]);

                    // or like this:
                    $config = new Braintree_Configuration([
                        'environment' => 'sandbox',
                        'merchantId' => 'dn9hfq6926hgwd2b',
                        'publicKey' => 'ctvv43yjk5v3rc8q',
                        'privateKey' => 'bc77ccaf3af327ed357fdfec240807b1'
                    ]);
                    $gateway = new Braintree\Gateway($config);

                    // Then, create a transaction:
                    $result = $gateway->transaction()->sale([
                        'amount' => $input['amt'],
                        'paymentMethodNonce' => $responce_token,
                        'options' => [ 'submitForSettlement' => true ]
                    ]);

                    if ($result->success) {

                        $detailes = array(  'transaction_id' => $result->transaction->id,
                                            'userid' => $input['userid'],
                                            'packageid' => $input['packageid'],
                                            'item_name' => $input['item_name'],
                                            'responce_token' => $input['responce_token'],
                                            'amt' => $input['amt'],                      
                                            'free_start'=> date("Y-m-d"),
                                            'free_end' => $free_end,
                                            'cc'=> "USD",
                                            'plan_valid'=> "1",
                                            'package_type'=> "2",
                                            'st'=> "success",
                                            'create_date'=> date("Y-m-d")
                                        );
     
                        $insert = IR(TRANSACTION, $detailes);
                        if (!empty($insert)) {
                            $Update = UR(TRANSACTION, array('plan_valid' => 0), array('tid' =>$tid));
                        }
                        //print_r("success!: " . $result->transaction->id);
                        $resp = array ('status' => SUCCESS, 'message' => "success!", 'response' => array('transaction_id' =>  $result->transaction->id , 'insert_id'=> $insert)); 
                    } 
                    else if ($result->transaction) {
                        // print_r("Error processing transaction:");
                        // print_r("\n  code: " . $result->transaction->processorResponseCode);
                        // print_r("\n  text: " . $result->transaction->processorResponseText);
                       $resp = array ('status' => ERROR, 'message' => "Error processing transaction", 'response' => array('error_code' =>  $result->transaction->processorResponseCode, 'error_text' => $result->transaction->processorResponseText)); 
                    } 
                    else {
                        // print_r("Validation errors: \n");
                        // print_r($result->errors->deepAll());

                        $error = $result->errors->deepAll();
                        $resp = array ('status' => ERROR, 'message' => "Validation errors", 'response' => $error);
                    }
                // }else{
                //     $resp = array ('status' => ERROR, 'message' => "You have already seleacted package", 'response' => new stdClass);
                }
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            /*}else{
                 $resp = $Error;
                 print_r($resp);
            }*/
            $this->response($resp);
        }        
        /**
        * pay for features listing process
        * @param user_id 
        */
        public function nonceValuesFeatures_post()
        {
        
            
            $input = $this->param;
            $required = array('business_id','userid','user_language','device_id', 'packageid', 'item_name', 'responce_token', 'amt', 'validity', );
            $Error = check_required_value($required, $input);
            if ($Error == "0"){
                
                $where= " userid = '".$input['userid']."' && plan_valid = 1 && package_type = 3 && st = 'success' "; 
                $count = SR(TRANSACTION, $where);
                if (!empty($count)) {
                        $today = strtotime(date("Y-m-d"));
                        $tid = $count['tid'];
                        $enddate = strtotime($count['free_end']);
                        if ($today < $enddate) {
                            $free_end =  date('Y-m-d', strtotime($count['free_end']. ' + '.$input['validity'] .'days'));
                        }else{
                            $free_end = date("Y-m-d", strtotime("+".$input['validity']." day"));
                        }

                    }else{
                        $free_end = date("Y-m-d", strtotime("+".$input['validity']." day"));
                    }
           
                    $responce_token   = $input['responce_token'];
                    
                    // Instantiate a Braintree Gateway either like this:
                    $gateway = new Braintree_Gateway([
                        'environment' => 'sandbox',
                        'merchantId' => 'dn9hfq6926hgwd2b',
                        'publicKey' => 'ctvv43yjk5v3rc8q',
                        'privateKey' => 'bc77ccaf3af327ed357fdfec240807b1'
                    ]);

                    // or like this:
                    $config = new Braintree_Configuration([
                        'environment' => 'sandbox',
                        'merchantId' => 'dn9hfq6926hgwd2b',
                        'publicKey' => 'ctvv43yjk5v3rc8q',
                        'privateKey' => 'bc77ccaf3af327ed357fdfec240807b1'
                    ]);
                    $gateway = new Braintree\Gateway($config);

                    // Then, create a transaction:
                    $result = $gateway->transaction()->sale([
                        'amount' => $input['amt'],
                        'paymentMethodNonce' => $responce_token,
                        'options' => [ 'submitForSettlement' => true ]
                    ]);
                    if ($result->success) {

                        $detailes = array(  'transaction_id' => $result->transaction->id,
                                            'business_id' => $input['business_id'],
                                            'userid' => $input['userid'],
                                            'packageid' => $input['packageid'],
                                            'item_name' => $input['item_name'],
                                            'responce_token' => $input['responce_token'],
                                            'amt' => $input['amt'],                      
                                            'free_start'=> date("Y-m-d"),
                                            'free_end' => $free_end,
                                            'cc'=> "USD",
                                            'st'=> "success",
                                            'plan_valid'=> "1",
                                            'package_type'=> "3",
                                            'create_date'=> date("Y-m-d")
                                        );
     
                        $insert = IR(TRANSACTION, $detailes);

                        $update = UR(BUSINESS, array('features_ads' => 1), array('business_id' => $input['business_id']));

                        //print_r("success!: " . $result->transaction->id);
                        $resp = array ('status' => SUCCESS, 'message' => "success!", 'response' => array('transaction_id' =>  $result->transaction->id ));
                    }
                    else if ($result->transaction) {
                        // print_r("Error processing transaction:");
                        // print_r("\n  code: " . $result->transaction->processorResponseCode);
                        // print_r("\n  text: " . $result->transaction->processorResponseText);
                       $resp = array ('status' => ERROR, 'message' => "Error processing transaction", 'response' => array('error_code' =>  $result->transaction->processorResponseCode, 'error_text' => $result->transaction->processorResponseText)); 
                    } 
                    else {
                        // print_r("Validation errors: \n");
                        // print_r($result->errors->deepAll());
                        $error = $result->errors->deepAll();
                        $resp = array ('status' => ERROR, 'message' => "Validation errors", 'response' => $error);
                    }
                
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }



            /*}else{
                 $resp = $Error;
                 print_r($resp);
            }*/
            $this->response($resp);
        }

        /**
        * Change Language process
        * @param user_id 
        */
        public function changeLanguage_post(){
            $input = $this->param;
            $required = array('user_id','user_language','user_country','device_id');
            $Error = check_required_value($required, $input);     
            if ($Error == "0"){
                
                $Update = UR(USERS, array('user_language' => $input['user_language'],
                                         'user_country' => $input['user_country']), 
                                    array('user_id' => $input['user_id'])
                                    );
                $resp = array('status' => SUCCESS, 'message' => "success!", 'response' => array('data' => $Update));          
            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }

        public function contactDetailes_post() {

            $input = $this->param;
            $required = array(/*'user_email', 'device_id', 'device_type', 'device_token',*/'user_language');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){

                $condition = array ('id'=> 1);      
                          
                $cont = SR(CONTACTDETAILES, $condition);
                if(!empty($cont)) {
                    if ($input['user_language'] =="ar") {
                        $results['address_ar'] = $cont['address_ar'];
                        $results['address_value_ar'] = $cont['address_value_ar'];                     
                        $results['phone_field_ar'] = $cont['phone_field_ar'];
                        $results['link_ar'] = $cont['link_ar'];
                        $results['email_field_ar'] = $cont['email_field_ar'];
                        $results['name_field_ar'] = $cont['name_field_ar'];
                        $results['Comment_field_ar'] = $cont['Comment_field_ar'];
                        $results['contact_us_ar'] = $cont['contact_us_ar'];
                        $results['contact_us_top_ar'] = $cont['contact_us_top_ar'];
                        $results['get_in_touch_ar'] = $cont['get_in_touch_ar'];
                        $results['save_button_ar'] = $cont['save_button_ar'];
                    }else{
                        $results['address_en'] = $cont['address_en'];
                        $results['address_value_en'] = $cont['address_value_en'];                     
                        $results['phone_field_en'] = $cont['phone_field_en'];
                        $results['link_en'] = $cont['link_en'];
                        $results['email_field_en'] = $cont['email_field_en'];
                        $results['name_field_en'] = $cont['name_field_en'];
                        $results['Comment_field_en'] = $cont['Comment_field_en'];
                        $results['contact_us_en'] = $cont['contact_us_en'];
                        $results['contact_us_top_en'] = $cont['contact_us_top_en'];
                        $results['get_in_touch_en'] = $cont['get_in_touch_en'];
                        $results['save_button_en'] = $cont['save_button_en'];
                    }
                    $resp = array('status' => SUCCESS, 'message' => "success!", 'response' => array('data' => $results));

                }else {
                    $resp = array ('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                // $resp = $Error;
                $resp = $Error;

            }
            $this->response($resp);
        }

        /**
        * Get search Data Process  
        * @param user_id 
        */
        public function getsearch_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_language', 'device_id', 'lat', 'lon');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $search = $input['search'];
                
                $userData = $this->common_model->getSearchField($search,$input['lat'],$input['lon'],$input['limit'],$input['offset'],$input['name']);
               
                if ($userData) {
                    $count = $this->common_model->countSearchField($search,$input['lat'],$input['lon'],$input['name']);
                    $results = $this->common_model->totalOffset($count, $input['limit']);
                
                    $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$count, 'maxoffset' => (string)$results['numberOffset'], 'response' => array('data' => $userData));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{
                //YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }


        // public function getsearch_post() {
        //   /* Check for required parameter */
        //   $input = $this->param;
        //   $required = array('user_language', 'device_id', 'lat', 'lon','limit', 'flimit', 'distance');
        //   $Error = check_required_value($required, $input);        
        //   if ($Error == "0"){
        //         $condition = array('user_id' => $input['user_id']);
        //         $search = $input['search'];
        //         $limit = $input['limit'];
        //         $flimit = $input['flimit']; 
        //         $slimit = $input['limit'] - $input['flimit'];

        //         $count = $this->common_model->count_Search($search,$input['lat'],$input['lon'],$input['name'],$input['distance']);
        //         //print_r($count);die;
        //         $total = $count['total'];                 
        //         $max = $this->common_model->totalOffset($total, $limit);
        //         $lastoffset = $max['numberOffset'];

        //         $features = $count['features'];
        //         $maxf = $this->common_model->totalOffset($features, $flimit);
        //         //print_r($maxf);die;
        //         $lastoffsetf = $maxf['numberOffset'];                
        //         $fmodf = fmod($features,$flimit);

        //         $simple = $count['simple'];    
        //         $maxs = $this->common_model->totalOffset($simple, $slimit);
        //         $lastoffsets = $maxs['numberOffset'];
        //         $fmods = fmod($simple,$slimit);


        //         if ($lastoffsetf < $lastoffsets) {
        //             if ($features == "0") {
        //                 $remainFeature = $flimit;
        //             }else{
        //                 if ($fmodf == "0") {
        //                 $remainFeature = $flimit*0; 
        //                 }else{
        //                 $remainFeature = $flimit-$fmodf; 
        //                 }                        
        //             }
                    
                    
        //             if ($lastoffsetf == $input['offset']) {
        //                 //echo "Ok";

        //                 if ($fmodf != "0") {
                            
        //                     $foffset = $input['offset']*$flimit;
        //                     $soffset = ($input['offset']*$slimit);                            
        //                     $flimit = $flimit;
        //                     $slimit = $slimit+$remainFeature;
        //                     $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 1 ,$flimit, $foffset,$search, $input['name'],$input['distance']);
                            
        //                     $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 0 ,$slimit, $soffset, $search, $input['name'],$input['distance']);
                            
        //                     //$data = array_merge($featuresData,$simpleData);
        //                 }else{
        //                     if ($features == 0) {
        //                         //echo "Ok";die;
        //                         $slimit = $limit;
        //                         $foffset = $input['offset']*$slimit;
                                
        //                         $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 0 ,$slimit, $foffset,$search, $input['name'],$input['distance']);
                                
        //                         //$data = $simpleData;
        //                     }else{

        //                         //echo "asdds";die;
        //                         $foffset = $input['offset']*$flimit;
        //                         $soffset = $input['offset']*$slimit;                            
        //                         $flimit = $flimit;
        //                         $slimit = $slimit;

        //                        $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 1 ,$flimit, $foffset,$search, $input['name'],$input['distance']);
                                
        //                         $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 0 ,$slimit, $soffset,$search, $input['name'],$input['distance']);
        //                        // $data = array_merge($featuresData,$simpleData);

                                
        //                     }
        //                 }
        //             }elseif ($lastoffsetf < $input['offset']) {
                       

        //                 $subtract = $input['offset'] - $lastoffsetf;
        //                 if ($subtract == 1) {

        //                     $offset = ($input['offset']* $slimit) + $remainFeature;
        //                     $slimit = $limit;
        //                 }else{

        //                     $subtract = $subtract-1;
        //                     $offset = ($input['offset']* $slimit)+($subtract* $flimit) + $remainFeature;
        //                     $slimit = $limit;

        //                 }

        //                 $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'],0 ,$slimit, $offset, $search, $input['name'],$input['distance']);
        //                 //$data = $simpleData;
                        
        //             }else{

        //                 //echo "string";die;

        //                 $foffset = $input['offset']*$flimit;
        //                 $soffset = $input['offset']*$slimit;                            
        //                 $flimit = $flimit;
        //                 $slimit = $slimit;

        //                 $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 1 ,$flimit, $foffset,$search, $input['name'],$input['distance']);
                        
        //                 $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 0 ,$slimit, $soffset,$search, $input['name'],$input['distance']);
        //                 //$data = array_merge($featuresData,$simpleData);

        //             }

        //         }elseif ($lastoffsetf == $lastoffsets) {
                    
        //             $foffset = $input['offset']*$flimit;
        //             $soffset = $input['offset']*$slimit;                    
                    
        //             $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'],1 ,$flimit, $foffset, $search, $input['name'],$input['distance']);
        //             //print_r($featuresData);
        //             $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'],0 ,$slimit, $soffset, $search, $input['name'],$input['distance']);
        //             //print_r($simpleData);die;
        //             //$data = array_merge($featuresData,$simpleData);
        //         }
        //         else {

        //             if ($simple == "0") {
        //                 $remainSimple = $slimit;
        //             }else{
        //                 if ($fmods == "0") {
        //                 $remainSimple = $slimit*0; 
        //                 }else{
        //                 $remainSimple = $slimit-$fmods; 
        //                 }                        
        //             }
        //             if ($lastoffsets == $input['offset']) {
        //                 $fmods;

        //                 if ($fmods != "0") {
        //                     // echo "ok";die;
        //                     $foffset = $input['offset']*$flimit;
        //                     $soffset = $input['offset']*$slimit;                            
        //                     $flimit = $flimit+$remainSimple;
        //                     $slimit = $slimit;
        //                     $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 1 ,$flimit, $foffset,$search, $input['name'],$input['distance']);
                            
        //                     $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 0 ,$slimit, $soffset, $search, $input['name'],$input['distance']);
                            
        //                     //$data = array_merge($featuresData,$simpleData);
        //                     //print_r($data);die;
        //                 }else{
        //                     //echo $simple;echo $slimit;die;
        //                     if ($simple == "0") {
        //                         $flimit = $limit;
        //                         $foffset = $input['offset']*$flimit;
                                
        //                         $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 1 ,$flimit, $foffset,$search, $input['name'],$input['distance']);
        //                         //print_r($featuresData);die;

        //                     }else{
        //                         $foffset = $input['offset']*$flimit;
        //                         $soffset = $input['offset']*$slimit;                            
        //                         $flimit = $flimit;
        //                         $slimit = $slimit;

        //                        $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 1 ,$flimit, $foffset,$search, $input['name'],$input['distance']);
                                
        //                         $simpleData = $this->common_model->new_GetSearch($input['lat'], $input['lon'], 0 ,$slimit, $soffset,$search, $input['name'],$input['distance']);

        //                         //$data = array_merge($featuresData,$simpleData);
        //                         //print_r($data);die;
        //                     }
        //                 }
        //             }elseif ($lastoffsets < $input['offset']) {                       

        //                 $subtract = $input['offset'] - $lastoffsets;
        //                 if ($subtract == 1) {
        //                     $offset = ($input['offset']* $flimit) + $remainSimple;
        //                     $flimit = $limit;
        //                 }else{
        //                     $subtract = $subtract-1;
        //                     $offset =($input['offset']* $flimit)+($subtract* $slimit) + $remainSimple;
        //                     $flimit = $limit;
        //                 }

        //                 $featuresData = $this->common_model->new_GetSearch($input['lat'], $input['lon'],1 ,$flimit, $offset, $search, $input['name'],$input['distance']);
        //                 $data = $featuresData;
        //                 //print_r($data);die;
                        
        //             }
        //         }



        //         $arra = array();
        //         if(!empty($featuresData) && !empty($simpleData))
        //         {
        //         $arra = array_merge($featuresData, $simpleData);
        //         } elseif(!empty($featuresData)) {
        //         $arra = $featuresData;
        //         } elseif(!empty($simpleData)) {
        //         $arra = $simpleData;
        //         }
        //         $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => (string)$count['total'], 'maxoffset' => (string)$lastoffset, 'response' => array('data' => $arra));      
        //         //print_r($arra);die;
        //     } else{
        //         //YOU_HAVE_MISSED_A_PARAMETER_
        //         $resp = $Error;
        //     }
        //     $this->response($resp);
        // }

        

    /*___________________________________-SUMIT START FROM HEAR- ___________________*/
/*_______________________________________-START-____________________________________*/
}

