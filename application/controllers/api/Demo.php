<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';



class Demo extends REST_Controller 

{   
     public function __construct() {  

     parent:: __construct();
    //     /*For Language*/ 

         if ($_POST) { $this->param = $_POST;}else{ $this->param = json_decode(file_get_contents("php://input"),true); }
         // $user_language = $this->param['user_language'];
         // if (!empty($user_language)){$userData = $this->param['user_language']; }else{ $userData = getlanguge($uid);}
         // /*Globals Language Set*/
         // $GLOBALS['user_language'] = $userData;
        
     }



    public function getData_post() {
        $input = $this->param;
        $id = $input['id'];
        $table = $input['table'];
        if (!empty($id)) { $message = AR($table, $id);
        }
        else{ $message = $this->common_model->getAllRecords($table);
        }

            $resp = array ('status' => 1,'response' => $message);
        $this->response($resp);
        
    }
    public function updateData_post() {

        $input = $this->param;
        $id = $input['id'];
        $table = $input['table'];
        $data = $input['data'];
        $update = UR($table, $data, $id);
        $message = SR($table, $id);
        $resp = array ('status' => 1, 'updatestatus'=> $update, 'response' => $message);
        $this->response($resp);
        
    }
    public function bulkAdd_post() {

        $input = $this->param;
        $table = $input['table'];
        $data = $input['data'];  
        $insert = $this->db->insert_batch($table,$data);
        $resp = array ('status' => 1, 'updatestatus'=> $insert, 'response' => $insert);
        $this->response($resp);
        
    }
    public function bulkUpdated_post() {

        $input = $this->param;
        $table = $input['table'];  
        $data = $input['data'];
        $update = $this->db->update_batch($table, $data, 'id');     
        $resp = array ('status' => 1, 'updatestatus'=> $update, 'response' => $update);
        $this->response($resp);
        
    }
   

    public function sendNotification_post() {

        $input = $this->param;
        print_r($input);die;
        //$notification = send_Notification($input);

        $device_token = $input['device_token'];
        //$userid = $input['userid'];
        //$user_token_id = $input['token_id'];
        //$target = $input['device_token'];
        $title = $input['title'];
        $description = $input['description'];
        $server_key = $input['server_key'];
        $active_methods = array();

        $error = '';
        $error_sess = '';

        $url = 'https://fcm.googleapis.com/fcm/send';

        $data = array("via"=>"Borex Poissons", "title"=>$title, "description"=>$description);
        $fields = array();
        $fields['data'] = $data;    
        if(is_array($device_token)){
            $fields['registration_ids'] = $device_token;
        }else{
            $fields['to'] = $device_token;
        }
        //header with content_type api key
        $headers = array(
        'Content-Type:application/json',
          'Authorization:key='.$server_key
        );
        //CURL request to route notification to FCM connection server (provided by Google)  
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        if ($result === FALSE) {
            return false;
        }
        curl_close($ch);
        $jsn =json_decode($result);
        if($jsn->success){
            return true;
        }
        else{
        return true;
        }

  
        $ar = array ("result" => 1, "message" => "success");
         echo json_encode($ar);
         exit();

        // $resp = array ('status' => 1, 'response' => $notification);
        // $this->response($resp);        
    }

}