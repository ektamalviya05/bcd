<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

class Webservice extends REST_Controller 
{   
    public function __construct() {  

    parent:: __construct();
        /*For Language*/ 
        $uid= "";
        if ($_POST){$this->param = $_POST;}else{ $this->param = json_decode(file_get_contents("php://input"),true);}
        //     $user_language = $this->param['user_language'];
        //     if (!empty($this->param['user_id'])){$uid = $this->param['user_id'];}
        // if (!empty($user_language)){$userData = $this->param['user_language']; }else{ $userData = getlanguge($uid);}
        

        // /*Globals Language Set*/
        // $GLOBALS['user_language'] = $userData;        
    }
/*_______________________________________-END-______________________________________*/
    /*___________________________________-NEW FUNCTION-_____________________________*/

        public function sign_up_post() {

            $input = $this->param;
            $required = array ('user_fullname', 'user_email', 'user_password', 'conf_password', 'device_id', 'device_type', 'device_token');
            $Error = check_required_value($required, $input);            
            if ($Error == "0"){

                if ($input['user_password']==$input['conf_password']) {                   
                
                
                    $check_email = CR("bdc_users", array('user_email' => $input['user_email']));
                    if ($check_email == "0") {
                        $inputs = array('user_firstname' =>  $input['user_fullname'],
                                        'user_phone' =>      $input['user_phone'],
                                        'user_email' =>      $input['user_email'],
                                        'user_password' =>   md5($input['user_password']),
                                        'device_id' =>       $input['device_id'],
                                        'device_type' =>     $input['device_type'],
                                        'device_token' =>    $input['device_token'],
                                        'social_login' =>    $input['social_login'],
                                        'user_lat' =>        $input['user_lat'],
                                        'user_long' =>       $input['user_long'],
                                        'status' => 1
                                        //'user_lastname' =>   $input['user_lastname'],                         
                                        //'user_country' =>    $input['user_country'],
                                        ///'user_language' =>   $input['user_language'],
                                        //'user_city' =>       $input['user_city'],
                                        //'about' =>           $input['about']
                                        
                                        );

                        $insert_id = IR( "bdc_users", $inputs);
                        if (!empty($insert_id)) {

                            // $otp= rand(100000,999999);
                            // $insert_otp = IR( OTP, array('user_id' => $insert_id, 'otp' => $otp, 'status' => 0));

                            // $email = "".$input['user_email'].",votivemobile.dilip@gmail.com,zubaer.votive@gmail.com,";

                            //     $subject= "OTP Message";
                            //     $message="Your One Time Password is ".$otp." During the Pickup process . Dont share it with anyone";
                            //     $m = sendEmail($email,$subject,$message);
                            
                            // $email = "".$input['user_email'].",votivemobile.dilip@gmail.com,zubaer.votive@gmail.com,";
                            // $from = 'ilbait@gmail.com';
                            // $email = strip_tags($input['user_email']);
                            // $name = strip_tags($input['user_firstname']);
                            // $subject = "OTP Message";
                            // $message = "<p>Dear " . $name . ",</p>";
                            // $message .= "<p>This email address is being used to a signup Verification . If you initiated the signup process, it is asking you to enter the numeric verification code that appears below.</p>";
                            // $message .= "<p><b style='background: green'>".$otp."</b></p>";
                            // $message .= "<p><br></p>";
                            // $message .= "<p>Thank You,</p>";
                            // $message .= "<p>Team Ilbait</p>";
                            // $headers = "MIME-Version: 1.0" . "\r\n";
                            // $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                            // $headers .= 'From: <"' . $from . '">' . "\r\n";
                            // mail($email, $subject, $message, $headers);

                            //We have sent one time password on your registered Email id, Please check Email id.
                            $resp = array ('status' => SUCCESS, 'message' => "successfully signup", 'response' => array('user_id' =>  $insert_id));                        
                        }else{
                            $resp = array ('status' => SERVER_ERROR , 'message' => message('4'), 'response' => new stdClass);  
                        }                   

                    }else{//Email Already Exist in our database
                        $resp = array ('status' => ERROR, 'message' => 'Email Already Exist in our database', 'response' => new stdClass);

                    }
                }else{
                    $resp = array ('status' => ERROR, 'message' => "confirm password not match", 'response' => new stdClass);
                }

            }else{//YOU_HAVE_MISSED_A_PARAMETER_
                // $resp = array ('status' => MISSING_PARAM, 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => new stdClass);
                $resp = $Error;
            }
            $this->response($resp);
        }




        public function login_post() {

            $input = $this->param;
            //$required = array('user_email', 'user_password');

             $required = array('user_email', 'user_password', 'device_id', 'device_type', 'device_token');
            $Error = check_required_value($required, $input);
            //print_r($Error);die;
            if ($Error == "0"){

                $condition = array ( 'user_email' => $input['user_email'],
                                    'user_password' => md5($input['user_password']),
                                    'status' => 1);      
                          
                $check_login = SR("bdc_users", $condition);
                
                if(!empty($check_login)) {

                        $update_input = array ( 'device_type' => $input['device_type'],
                                                'device_id' => $input['device_id'],
                                                'device_token' => $input['device_token'],
                                                //'is_online' => 1
                                            );
                        $update_condition = array ('user_id' => $check_login['user_id'] ); 
                        $update = UR( "bdc_users", $update_input, $update_condition);
                        $results = SR("bdc_users", $update_condition);
                        
                        $response = array(  'user_id' => $results['user_id'],
                                            'username' => $results['user_firstname'],
                                            'user_email' => $results['user_email'],
                                            'user_phone' => $results['user_phone'],
                                            'user_lat' => $results['user_lat'],
                                            'user_long' => $results['user_long'],
                                            'user_image' => $results['user_image'],
                                            'device_id' => $results['device_id'],
                                            'device_type' => $results['device_type'],
                                            'device_token' => $results['device_token'],
                                            'status' => $results['status'],
                                            'notification' => $results['notification'],
                                            'created' => $results['created']
                                        );



                        //You have successfully logged in.
                        $resp = array ('status' => SUCCESS, 'message' => 'You have successfully logged in.', 'response' => array('data' => $response, 'url' => 'xyz'));
                  
                }else {
                    $resp = array ('status' => ERROR, 'message' => 'Email Id or password is not correct, please try again.', 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;

            }
            $this->response($resp);
        }


        public function forgot_post() {

            $input = $this->param;
            $required = array('user_email', 'device_id', 'device_token' );
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){

               $email = strtolower($input['user_email']);
                $checkUser = SR("bdc_users", array('user_email' => $email));
                
                if(!empty($checkUser)){

                    $otp= 1234;//rand(1000,9999);
                    $user_id = $checkUser['user_id'];
                    $user_firstname = $checkUser['user_firstname'];
                    $update =UR("bdc_users", array("otp" => $otp), array('user_id' => $user_id ) );
                    if($update == 1){


                            $from = 'votivephp.prashant@gmail.com';
                                                    
                            $subject = "OTP Message";
                            $message = "<p>Dear " . $user_firstname . ",</p>";
                            $message .= "<p>This email address is being used to a forget password Verification . If you initiated the Forget Password process, it is asking you to enter the numeric verification code that appears below.</p>";
                            $message .= "<p><b style='background: green'>".$otp."</b></p>";
                            $message .= "<p><br></p>";
                            $message .= "<p>Thank You,</p>";
                            $message .= "<p>Team Ilbait</p>";
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                            $headers .= 'From: <"' . $from . '">' . "\r\n";

                            //mail($email, $subject, $message, $headers);
                                              
                        //We have sent one time password on your registered Email id, Please check Email id.
                        $resp = array('status' => SUCCESS, 'message' => "We have sent one time password on your registered Email id, Please check Email id.", 'response' => array('user_id' => (int)$user_id));

                    }else{//Some error occured, please try again.
                        $resp = array('status' => SERVER_ERROR, 'message' => "Some error occured, please try again.", 'response' => new stdClass);
                    }
                }else{//Sorry, there is no account associated with this mobile number.
                    $resp = array('status' => NOT_FOUND, 'message' => "Sorry, there is no account associated with this email id.", 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                // $resp = $Error;
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * Verify Otp for forgot password  
        * @param user Email Address
        */
        public function verifyOtp_post() {

            $input = $this->param;
            $required = array('user_id', 'otp', 'device_id');
            $Error = check_required_value($required, $input);
            if ($Error == "0"){
                $checkotp = SR("bdc_users", array('user_id' => $input['user_id']));
                if(!empty($checkotp)){
                    if ($checkotp['otp'] == $input['otp']) {
                        //$upd = UR("bdc_users", array('status' => 1), array('user_id' => $input['user_id']));
                        $resp = array('status' => SUCCESS, 'message' => message('24'), 'response' => new stdClass);
                    }else{//One time password did not matched.please try again.
                        $resp = array('status' => ERROR, 'message' => message('25'), 'response' => new stdClass);
                    }
                }else{//Page Not Found
                    $resp = array('status' => ERROR, 'message' => message('20'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_                
                $resp = $Error;
            }

            $this->response($resp);
        }

        /**
        * RESET PASSWORD Process  
        * @param user_id 
        */
        public function changePassword_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id', 'device_id', 'new_password', 'confirm_password');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR("bdc_users", $condition );
                if (!empty($userData)) {

                    if($input['new_password'] == $input['confirm_password']) {
                        
                        if($userData['user_password'] != md5($input['new_password'])) {

                            $updateArr = array('user_password' => md5($input['new_password']));
                            $update = UR("bdc_users", $updateArr, $condition);
                            if ($update == 1) {//Password has been changed sucessfully.
                                $resp = array('status' => SUCCESS, 'message' => message('27'), 'response' => new stdClass);
                            }else{//Some error occured, please try again.
                                $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);    
                            }
                        }else{//Confirm password field did not match.
                         $resp = array('status' => ERROR, 'message' => message('28'), 'response' => new stdClass);
                        }        
                    }else{//Confirm password field did not match.
                         $resp = array('status' => ERROR, 'message' => message('26'), 'response' => new stdClass);
                    }
                }else{//Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
               // $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => new stdClass);
                $resp = $Error;
            }

            $this->response($resp);
        }


        public function profession_post() {
            //echo "string";
            $input = $this->param;
            //$required = array();
            $Error = "0" ;//check_required_value($required, $input);
            if ($Error == "0"){
                $profession = AR("profession", array('status' => 1));
                
                if(!empty($profession)){
                        $resp = array('status' => SUCCESS, 'message' => "SUCCESS", 'response' => $profession);
                
                }else{//Page Not Found
                    $resp = array('status' => ERROR, 'message' => message('20'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_                
                $resp = $Error;
            }

            $this->response($resp);
        }

        public function getprofile_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id');
            $Error = check_required_value($required, $input);        
            if ($Error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR("bdc_users", $condition );
                if ($userData) {

                    $response = array(  'user_id' => $userData['user_id'],
                                            'username' => $userData['user_firstname'],
                                            'user_email' => $userData['user_email'],
                                            'user_phone' => $userData['user_phone'],
                                            'user_lat' => $userData['user_lat'],
                                            'user_long' => $userData['user_long'],
                                            'user_image' => $userData['user_image'],
                                            'device_id' => $userData['device_id'],
                                            'device_type' => $userData['device_type'],
                                            'device_token' => $userData['device_token'],
                                            'status' => $userData['status'],
                                            'notification' => $userData['notification'],
                                            'created' => $userData['created']
                                        );

                   //$userData['user_profile_pic'] = UPLOADPATH . $userData['user_profile_pic'];
                    //Profile data has been shown sucessfully.
                    $resp = array('status' => SUCCESS, 'message' => message('30'), 'response' => array('data' => $response));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = $Error;
            }
            $this->response($resp);
        }


    /*___________________________________-SUMIT START FROM HEAR- ___________________*/
/*_______________________________________-START-____________________________________*/
}

