<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// To use reCAPTCHA, you need to sign up for an API key pair for your site.
// link: http://www.google.com/recaptcha/admin
$config['recaptcha_site_key'] = '6LcYWCQUAAAAAB4qMAE28JSKqcOSEA6TolAnLh3M';
$config['recaptcha_secret_key'] = '6LcYWCQUAAAAADJpKBDKhP1EOdzu9455TYYYPNks';

// reCAPTCHA supported 40+ languages listed here:
// https://developers.google.com/recaptcha/docs/language
$config['recaptcha_lang'] = 'en';

/* End of file recaptcha.php */
/* Location: ./application/config/recaptcha.php */
