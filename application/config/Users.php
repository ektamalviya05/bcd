<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions

require APPPATH . '/libraries/REST_Controller.php';

class Users extends REST_Controller 
{   
    public function __construct() {  

    parent:: __construct();
        /*For Language*/ 
        $uid= "";
        if ($_POST) { $this->param = $_POST;}else{ $this->param = json_decode(file_get_contents("php://input"),true); }
        $user_language = $this->param['user_language'];
        if (!empty($user_language)){$userData = $this->param['user_language']; }else{ $userData = getlanguge($uid);}

        /*Globals Language Set*/
        $GLOBALS['user_language'] = $userData;
        
    }

    /*___________________________________-NEW FUNCTION-__________________________________*/

        public function new_Function_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
               
                echo "For New Fenction";
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
    /*___________________________________-NEW FUNCTION-__________________________________*/


        public function offers_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $offers = TR(OFFERS);
            $count = count($offers);
            for ($i=0; $i < $count; $i++) { 
                $results[$i]['offer_name'] = $offers[$i]['offer_name'];
                $results[$i]['offer_image'] = $offers[$i]['offer_image'];
            }            
            $resp = array('status' => SUCCESS, 'message' => "1", 'response' => array('data' => $results,'url' => "http://votivephp.in/businessdirectoryclassified/uploads/offers/"));
            $this->response($resp);
        }

    /*____________________________________-COMPLITED-____________________________________*/


    /*___________________________________-SUMIT START FROM HEAR-_________________________*/ 

        
         

        


        /**
        *  Submit enquiry form
        * @param name,email,subject,message,user_language
        */
        //  Submit enquiry form 
        public function enquiry_post() {

            $input = $this->param;
            $required = array ('user_id','user_language','device_id','name', 'email', 'subject', 'message');
            $chk_error = check_required_and_valid($required, $input);
            //print_r($chk_error);die;
            if ($chk_error == "0"){
                
                        $inputs = array('name' =>  $input['name'],
                                        'email' => $input['email'],
                                        'subject' => $input['subject'],
                                        'message' => $input['message']
                                        ); 


                        $insert_id = IR(ENQUIRY, $inputs);
                        if (!empty($insert_id)) {

                    $from = 'ilbait@gmail.com';
                    $subject = "Enquiry";
                    $email = strip_tags($input['email']);
                    $name = strip_tags($input['name']);
                    $message = "<p>Dear " . $name . ",</p>";
                    $message .= "<p>Enquiry send successfully.our team contact shortly you.</p>";
                    $message .= "<p><br></p>";
                    $message .= "<p>Thank You,</p>";
                    $message .= "<p>Team Ilbait</p>";
                    $headers = "MIME-Version: 1.0" . "\r\n";
                    $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                    $headers .= 'From: <"' . $from . '">' . "\r\n";
                    if (mail($email, $subject, $message, $headers)) {
                        echo "Message accepted";
                    } else {
                        print_r(error_get_last());
                    }

                    $resp = array ('status' => SUCCESS, 'message' => message('34'), 'response' => array('id' => $insert_id));
                            

                    }                          
                           
                    
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                  $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array());
            }
            $this->response($resp);
        }

        /**
        *  Add business
        * @param user_id,user_language,device_id,cat_id,business_title,business_email,business_mobile,  business_address,'business_country,business_about,services_name
        */  
        public function addBusiness_post() {
        
            // Check for required parameter 

            $input = $this->param;
            $file['offer_images'] = $_FILES['offer_images'];
            //print_r($file);die;
            



            $required = array('user_id','user_language','device_id','cat_id','business_title','business_email','business_mobile','business_address','business_country','business_about','services_name');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){

                if (!empty($_FILES)) {
                    $names = array('business_image1','business_image2','business_image3','business_image4');      
                    $images = multipulImage($_FILES, $names,'business');
                    //$images = multipulImage($file1, $names,'business');
                    //print_r($images);die;
                }
                $inputs = array('user_id' =>            $input['user_id'],
                                'cat_id' =>             $input['cat_id'],
                                'business_title' =>     $input['business_title'],
                                'business_email' =>     $input['business_email'],
                                'business_mobile' =>    $input['business_mobile'],
                                'business_address' =>   $input['business_address'],
                                'business_country' =>   $input['business_country'],
                                'business_about' =>     $input['business_about'],                         
                                'features_ads' =>       $input['features_ads']
                                ); 
                
                $inputs = array_merge($inputs,$images);  
                //print_r($inputs);die;    
                $insert_id = IR(BUSINESS, $inputs);
                if (!empty($insert_id)) {

                    if(!empty($file['offer_images'])){
                        //print_r($file);die;

                        $file_count = count($file['offer_images']['name']);
                        $file_keys = array_keys($file['offer_images']);

                        for ($i=0; $i< $file_count; $i++) {
                            foreach ($file_keys as $key) {
                                $file['offer_images'][$key] = $file['offer_images'][$key][$i];               
                            }
                             //print_r($file); die;                            
                            if (!empty($file['offer_images'])) {
                                $name= "offer_image";
                                $inputs["name"] = imageUploads($file, $name, 'offers');
                                //print_r($inputs['name']);die;
                            }
                            $inputoffer = array('offer_name' => $input['offer_name'][$i],
                                                'business_id' => $insert_id,
                                                'user_id' => $input['user_id'],
                                                'offer_image' => $inputs['offer_images']
                                                );
                        }
                        //print_r($inputoffer);die;
                        $ser = IR(OFFERS, $inputoffer);
                    
                    }




                    //services multiple add 
                    $ser= $input['services_name'];
                    if($ser!='') {
                        $servicesArray = explode(',', $ser);
                        for($i = 0; $i < count($servicesArray); $i++) {

                            $inputservices = array( 'services_name' =>  $servicesArray[$i],
                                                    'business_id' => $insert_id,
                                                    'user_id' => $input['user_id']
                                                    );        
                            $ser = IR(SERVICES, $inputservices);  
                        }
                    }
                    

                        
                        $resp = array ('status' => SUCCESS, 'message' => message('36'), 'response' => array('id' => $insert_id));
                                
                        }  
                    
                } else{//YOU_HAVE_MISSED_A_PARAMETER_
                    $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
                }

            $this->response($resp);
        }
        
        /**
        *  Notification list user _id
        * @param user_id,user_language,token   ,
        */ 
        public function showNotification_post() {

            /* Check for required parameter */
        
            $input = $this->param;
            $object_info = $input;
            $limit = $input['limit'];
            $offset = $input['offset'];
            $user_id =$input['user_id'];
            $token =$input['token'];
            $required_parameter = array('user_language','device_id','user_id','token','limit','offset');
            $chk_error = check_required_and_valid($required_parameter, $object_info); 
            if ($chk_error) {
             $resp = array('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => new stdClass);
              $this->response($resp);
            } 
            $condition = array('noti_userid'=> $user_id);
            $count = CR(NOTIFICATIONLIST,$condition); 
            $offset = $limit*$offset;       
            $userData = $this->common_model->getDataInPagination(NOTIFICATIONLIST,$limit,$offset,$condition); 
            $d = $count/$limit;            
            $number = intval($d,0);
            if(fmod($d, 1) !== 0.00){
               $numberOffset = $number;
            }elseif ($d == 0) {
                $numberOffset = 0;
            }else{
                $numberOffset = $number-1;  
            }
            for ($i=0; $i < count($userData); $i++) {

                $userData[$i]['created'] = date('d-m-Y H:i A', strtotime($userData[$i]['created'] ));
            }

                   
            if (!empty($userData)) {

                $resp = array('status' => SUCCESS, 'message' => message('37'), 'total' => $count, 'maxoffset' => $numberOffset, 'response' => $userData);
            }else{

                $resp = array('status' => ERROR, 'message' => message('5'), 'response' => new stdClass);  
            } 
            $this->response($resp);          
        }         

        /**
        *  Category by   business listing  offers and services 
        * @param user_language user_id,device_id,cat_id
        */
        public function getCategoryBusiness_post() {
            
                //check for required parameter 

                $input = $this->param;
                $required = array('user_language','user_id','device_id','cat_id');
                $chk_error = check_required_and_valid($required, $input);        
                if ($chk_error == "0"){
                 

                $catbusinesslist = $this->common_model->getcatuseridbybusinesslist(BUSINESS, array('user_id' => $input['user_id'], 'cat_id' => $input['cat_id'], 'status' => 1  ));   
                
                $businesscount = count($catbusinesslist);
                
                


                for ($i=0; $i < $businesscount  ; $i++) { 
                $catbusinesslist[$i]['services'] = AR(SERVICES, array('business_id' => $catbusinesslist[$i]

                ['business_id'],'user_id' => $input['user_id']));

                    $catbusinesslist[$i]['offers'] = AR(OFFERS, array('business_id' => $catbusinesslist[$i]

                ['business_id'],'user_id' => $input['user_id']));
                    }//print_r($businesslist); die;
                    $resp = array('status' => SUCCESS, 'message' => "show", 'response' => $catbusinesslist);             
                    
                } else{//YOU_HAVE_MISSED_A_PARAMETER_
                    $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
                }

                $this->response($resp);
        }

        /**
        *  Get  business list with  services listing and offers listing
        * @param user_id,user_language,device_id,
        */
        public function getbusinessdetails_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){               
                
                $businesslist = AR(BUSINESS, array('user_id' => $input['user_id'],'status' => "1"));                
                $businesscount = count($businesslist);

                for ($i=0; $i < $businesscount  ; $i++) { 
                $businesslist[$i]['services'] = AR(SERVICES, array('business_id' => $businesslist[$i]['business_id'],'user_id' => $input['user_id'], 'status' => "1"));

                $businesslist[$i]['offers'] = AR(OFFERS, array('business_id' => $businesslist[$i]['business_id'],'user_id' => $input['user_id'], 'status' => "1"));
                }
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $businesslist, 'url' => BUSINESSPATH));
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }
            $this->response($resp);
        }

        /**
        *  Get  business list
        * @param user_id,user_language,device_id
        */  
        public function getbusinesslist_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){               
                
                $businesslist = AR(BUSINESS, array('user_id' => $input['user_id'],'status' => "1"));                
                
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $businesslist, 'url' => BUSINESSPATH));
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }
            $this->response($resp);
        }

        /**
        *  Get Packages  List
        * @param user_language
        */
        public function getpackagelist_post() {
            
            //check for required parameter 

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
               
                $package = AR(PACKAGE, array('status' => 1));
                if ($package) {
                    $resp = array('status' => SUCCESS, 'message' => "show", 'response' => array('data' => $package));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        *  Get category List
        * @param user_language
        */
        public function getcategory_post() {
        
            //check for required parameter 

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){

                $category = AR(CATEGORY, array('cat_status' => 1));
                if ($category) {
                    $resp = array('status' => SUCCESS, 'message' => "show", 'response' => array('data' => $category, 'url' => CATEGORYPATH));
                }else{
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }


    /*___________________________________-SUMIT END FROM HEAR- ___________________________*/

        /**
        *   Languages List
        * @param user_id
        */
        public function languages_post() {

            $input = $this->param;
            $lang = TR(LANGUAGES);
            if (!empty($lang)) {
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $lang));
            }else{
                $resp = array('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
            }
            $this->response($resp);
        }
        /**
        *   Countries List
        * @param user_id
        */
        public function countries_post() {

            $input = $this->param;
            $countries = TR(COUNTRIES);
            if (!empty($countries)) {
                for ($i=0; $i < count($countries); $i++) { 
                        
                    $results[$i]['id'] = $countries[$i]['id'];
                    $results[$i]['phonecode'] = $countries[$i]['phonecode'];
                    $results[$i]['sortname'] = $countries[$i]['sortname'];                  
                    if ($input['user_language'] == "ar") {
                    $results[$i]['name'] = $countries[$i]['arabic'];
                    }else{
                    $results[$i]['name'] = $countries[$i]['name']; 
                    }
                }
                
                $resp = array('status' => SUCCESS, 'message' => message('37'), 'response' => array('data' => $results));
            }else{
                $resp = array('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);
            }

            
            $this->response($resp);
        }
        /**
        *  notification on/off services
        * @param user_id
        */
        public function notification_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language','device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                if ($userData['notification'] == 0) {
                    $update = array('notification' => 1);
                    $message = "32";
                }else{
                    $update = array('notification' => 0);
                    $message = "33";
                }
                $update = UR(USERS, $update, $condition);
                $resp = array('status' => SUCCESS, 'message' =>  message($message), 'response' => $update);
        
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * Update Profile Data Process  
        * @param user_id 
        */
        public function updateprofile_post() {
        
            /* Check for required parameter */

            $input = $this->param;//
            $required = array('user_id','user_language',  'user_lastname', 'device_id', 'user_firstname','user_address', 'user_city', 'user_lat','user_long', 'device_token', /*'user_country', 'user_state',*/ );
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $inputs = array('user_lastname' =>   $input['user_lastname'],
                                'user_phone' =>      $input['user_phone'],
                                'user_email' =>      $input['user_email'],                                
                                'device_id' =>       $input['device_id'],
                                'device_type' =>     $input['device_type'],
                                'device_token' =>    $input['device_token'],
                                'user_country' =>    $input['user_country'],
                                'user_language' =>   $input['user_language'],
                                'user_city' =>       $input['user_city'],
                                'about' =>           $input['about'],
                                'user_lat' =>        $input['user_lat'],
                                'user_long' =>       $input['user_long'],

                                );
                                if (!empty($_FILES)) {
                                    $name= "user_profile_pic";
                                    $inputs[$name] = imageUploads($_FILES, $name, 'profile');
                                }
                                //print_r($inputs);die;
                $update = UR(USERS, $inputs, $condition);
                if ($update == 1) {//User profile details has been updated sucessfully.
                    $results = SR( USERS, $condition);
                    $resp = array ('status' => SUCCESS, 'message' => message('31'), 'response' => array('data' => $results,  'url' => UPLOADPATH ));
                } else{//Some error occured, please try again.
                    $resp = array ('status' => ERROR, 'message' => message('4'), 'response' => new stdClass);    
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * Get Profile Data Process  
        * @param user_id 
        */
        public function getprofile_post() {

            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language', 'device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                if ($userData) {
                    //Profile data has been shown sucessfully.
                    $resp = array('status' => SUCCESS, 'message' => message('30'), 'response' => array('data' => $userData,  'url' => UPLOADPATH ));
                }else{//    Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }
                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['user_id']));
            }
            $this->response($resp);
        }
        /**
        * After login UPDATE PASSWORD Process  
        * @param user_id 
        */
        public function updatePassword_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language', 'device_id', 'old_password', 'new_password', 'confirm_password');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                    if($userData['user_password'] == md5($input['old_password'])) {

                        if($input['new_password'] == $input['confirm_password']) {
                            
                            if($userData['user_password'] != md5($input['new_password'])) {

                                $updateArr = array('user_password' => md5($input['new_password']));
                                $update = UR(USERS, $updateArr, $condition);
                                if ($update == 1) {//Password has been changed sucessfully.
                                    $resp = array('status' => SUCCESS, 'message' => message('27'), 'response' => new stdClass);
                                }else{//Some error occured, please try again.
                                    $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);    
                                }
                            }else{//This password has already been used. Please select another password.
                             $resp = array('status' => ERROR, 'message' => message('28'), 'response' => new stdClass);
                            }        
                        }else{//Confirm password field did not match.
                             $resp = array('status' => ERROR, 'message' => message('26'), 'response' => new stdClass);
                        }
                    }else{//Old password did not match.
                             $resp = array('status' => ERROR, 'message' => message('29'), 'response' => new stdClass);
                    }                
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * RESET PASSWORD Process  
        * @param user_id 
        */
        public function changePassword_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id','user_language', 'device_id', 'new_password', 'confirm_password');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){
                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                if (!empty($userData)) {

                    if($input['new_password'] == $input['confirm_password']) {
                        
                        if($userData['user_password'] != md5($input['new_password'])) {

                            $updateArr = array('user_password' => md5($input['new_password']));
                            $update = UR(USERS, $updateArr, $condition);
                            if ($update == 1) {//Password has been changed sucessfully.
                                $resp = array('status' => SUCCESS, 'message' => message('27'), 'response' => new stdClass);
                            }else{//Some error occured, please try again.
                                $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);    
                            }
                        }else{//Confirm password field did not match.
                         $resp = array('status' => ERROR, 'message' => message('28'), 'response' => new stdClass);
                        }        
                    }else{//Confirm password field did not match.
                         $resp = array('status' => ERROR, 'message' => message('26'), 'response' => new stdClass);
                    }
                }else{//Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * Logout Process  
        * @param user_id 
        */
        public function logOut_post() {
        
            /* Check for required parameter */

            $input = $this->param;
            $required = array('user_id', 'user_language', 'device_id');
            $chk_error = check_required_and_valid($required, $input);        
            if ($chk_error == "0"){

                $condition = array('user_id' => $input['user_id']);
                $userData = SR(USERS, $condition );
                //print_r($userData);die;
                if (!empty($userData)) {

                    $upd = array('is_online' => 0, 'device_id' => 0);           
                    $update = UR(USERS, $upd, $condition);
                    $resp = array('status' => SUCCESS, 'message' => message('35'), 'response' => new stdClass);
                }else{
                    //Data not found, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('5'), 'response' => new stdClass); 
                }               
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * Verify Otp  
        * @param user Email Address
        */
        public function verifyOtp_post() {

            $input = $this->param;
            $required = array('user_id', 'otp', 'user_language', 'device_id');
            $chk_error = check_required_value($required, $input);        
            if ($chk_error == "0"){

                $checkotp = SR(OTP, array('user_id' => $input['user_id'], 'status' => 0));
                if(!empty($checkotp)){
                    if ($checkotp['otp'] == $input['otp']) {
                        //$updatestatus = UR(USERS, $inputs, $condition);
                        $updatestatus = UR(USERS,array('status' => 1) ,array('user_id' => $input['user_id']));
                        $upd = UR(OTP, array('status' => 1), array('user_id' => $input['user_id']));
                        //One time password matched successfully.
                        $userData = SR(USERS, array('user_id' => $input['user_id'])); 
                                              
                            $email = strip_tags($userData['user_email']);
                            $name = strip_tags($userData['user_firstname']);        
                            $from = 'ilbait@gmail.com';
                            $subject = "Activate Account";
                            $message = "<p>Dear " . $name . ",</p>";
                            $message .= "<p>Your account activated successfully. Please login.  </p>";
                            $message .= "<p><br></p>";
                            $message .= "<p>Thank You,</p>";
                            $message .= "<p>Team Ilbait</p>";
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                            $headers .= 'From: <"' . $from . '">' . "\r\n";
                            mail($email, $subject, $message, $headers);
                            $upd = SR(OTP, array('user_id' => $input['user_id'], 'status' => 1));
                        $resp = array('status' => SUCCESS, 'message' => message('38'), 'response' => new stdClass);                   

                    }else{//One time password did not matched.please try again.
                        $resp = array('status' => ERROR, 'message' => message('25'), 'response' => new stdClass);
                    }
                }else{//Some error occured, please try again.
                    $resp = array('status' => NOT_FOUND, 'message' => message('12'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * Verify Otp for forgot password  
        * @param user Email Address
        */
        public function verifyOtpForgot_post() {

            $input = $this->param;
            $required = array('user_id', 'otp', 'user_language', 'device_id');
            $chk_error = check_required_value($required, $input);        
            if ($chk_error == "0"){

                $checkotp = SR(OTP, array('user_id' => $input['user_id'],'status' => 0));
                if(!empty($checkotp)){
                    if ($checkotp['otp'] == $input['otp']) {
                        $upd = UR(OTP, array('status' => 1), array('user_id' => $input['user_id']));
                        $resp = array('status' => SUCCESS, 'message' => message('24'), 'response' => new stdClass);                   

                    }else{//One time password did not matched.please try again.
                        $resp = array('status' => ERROR, 'message' => message('25'), 'response' => new stdClass);
                    }
                }else{//Page Not Found
                    $resp = array('status' => ERROR, 'message' => message('20'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }
        /**
        * Forgot password 
        * @param user Email Address
        */
        public function forgot_post() {

            $input = $this->param;
            $required = array('user_email', 'user_language', 'device_id');
            $chk_error = check_required_value($required, $input);        
            if ($chk_error == "0"){

                $email = strtolower($input['user_email']);
                $checkUser = SR(USERS, array('user_email' => $email));

                if(!empty($checkUser)){

                    $otp= rand(100000,999999);
                    $user_id = $checkUser['user_id'];
                    $user_firstname = $checkUser['user_firstname'];
                    $update =UR(OTP, array("otp" => $otp, 'status' => 0), array('user_id' => $user_id ) );
                    if($update == 1){

                        // $email = "".$input['user_email'].",votivemobile.dilip@gmail.com,zubaer.votive@gmail.com,";

                        //     $subject= "OTP Message";
                        //     $message="Your One Time Password is ".$otp." During the Pickup process . Dont share it with anyone";
                        //     $m = sendEmail($email,$subject,$message);

                            $from = 'ilbait@gmail.com';
                                                    
                            $subject = "OTP Message";
                            $message = "<p>Dear " . $user_firstname . ",</p>";
                            $message .= "<p>This email address is being used to a forget password Verification . If you initiated the Forget Password process, it is asking you to enter the numeric verification code that appears below.</p>";
                            $message .= "<p><b style='background: green'>".$otp."</b></p>";
                            $message .= "<p><br></p>";
                            $message .= "<p>Thank You,</p>";
                            $message .= "<p>Team Ilbait</p>";
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                            $headers .= 'From: <"' . $from . '">' . "\r\n";

                            mail($email, $subject, $message, $headers);
                                              
                        //We have sent one time password on your registered Email id, Please check Email id.
                        $resp = array('status' => SUCCESS, 'message' => message('23'), 'response' => array('user_id' => $user_id));

                    }else{//Some error occured, please try again.
                        $resp = array('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);
                    }
                }else{//Sorry, there is no account associated with this mobile number.
                    $resp = array('status' => NOT_FOUND, 'message' => message('8'),'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }

            $this->response($resp);
        }

        /**
        * Users login service
        * @param request in $data
          required parameter : user_email , user_password
        */
        public function login_post() {

            $input = $this->param;
            $required = array('user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language');
            $chk_error = check_required_value($required, $input);        
            if ($chk_error == "0"){

                $condition = array ( 'user_email' => $input['user_email'],
                                    'user_password' => md5($input['user_password']),
                                    'status' => 1);      
                          
                $check_login = SR(USERS, $condition);
                
                if(!empty($check_login)) {

                    if ($input['device_id'] != $check_login['device_id'] && $check_login['is_online'] == 1){
                            //You are already logged in some other device. Do you want to login in this device ?
                        $resp = array('status' =>'203', 'message' => message('2'), 'response' => array('user_id' => $check_login['user_id']));
                    }else {
                        $update_input = array ( 'device_type' => $input['device_type'],
                                                'device_id' => $input['device_id'],
                                                'device_token' => $input['device_token'],
                                                'is_online' => 1);
                        $update_condition = array ('user_id' => $check_login['user_id'] ); 
                        $update = UR( USERS, $update_input, $update_condition);
                        $results = SR( USERS, $update_condition);
                        //You have successfully logged in.
                        $resp = array ('status' => SUCCESS, 'message' => message('22'), 'response' => array('data' => $results, 'url' => UPLOADPATH));
                    }
                }else {//Email Id or password is not correct, please try again.
                    $resp = array ('status' => ERROR, 'message' => message('11'), 'response' => new stdClass);
                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }
            $this->response($resp);
        }

        /**
        * Users Sginup resend otp service
        * @param request in $data
          required parameter : user_id, 'device_id', 'device_type', 'device_token', 'user_language',
        */
        public function reotpverify_post() {

            $input = $this->param;
            $required = array ('user_id', 'device_id','device_token', 'user_language');
            $chk_error = check_required_value($required, $input);
            
            if ($chk_error == "0"){
                
                $checkUser = SR( USERS, array('user_id' => $input['user_id']));
                if (!empty($checkUser)) {
                   $condition = array('user_id' => $input['user_id']);
                    $otp = rand(100000,999999);
                    $update_otp = UR( OTP, array('otp' => $otp), array('user_id' => $input['user_id']));
                    
                        if (!empty($update_otp)) {

                            $from = 'ilbait@gmail.com';
                            $email = strip_tags($checkUser['user_email']);
                            $subject = "OTP Message";
                            $message = "<p>Dear User,</p>";
                            $message .= "<p>This email address is being used to a signup Verification . If you initiated the signup process, it is asking you to enter the numeric verification code that appears below.</p>";
                            $message .= "<p><b style='background: green'>".$otp."</b></p>";
                            $message .= "<p><br></p>";
                            $message .= "<p>Thank You,</p>";
                            $message .= "<p>Team Ilbait</p>";
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                            $headers .= 'From: <"' . $from . '">' . "\r\n";
                            mail($email, $subject, $message, $headers);
                            //We have sent one time password on your registered Email id, Please check Email id.
                            $resp = array ('status' => SUCCESS, 'message' => message('23'), 'response' => new stdClass);
                                                
                        
                        }else{//Some error occured, please try again.
                            $resp = array ('status' => SERVER_ERROR, 'message' => message('4'), 'response' => new stdClass);
                        }
                    

                }else{//Email Already Exist in our database
                    $resp = array ('status' => NOT_FOUND, 'message' => message('12'), 'response' => new stdClass);

                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => $chk_error['check_error'], 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => array('user_id' => $chk_error['param']));
            }
            $this->response($resp);
        }
        /**
        * Users Sginup service
        * @param request in $data
          required parameter : 'user_firstname', 'user_lastname', 'user_phone', 'user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language',
        */
        public function signup_post() {

            $input = $this->param;
            $required = array ('user_firstname', 'user_lastname','user_email', 'user_password', 'device_id', 'device_type', 'device_token', 'user_language');
            $chk_error = check_required_value($required, $input);            
            if ($chk_error == "0"){
                
                $check_email = CR(USERS, array('user_email' => $input['user_email']));
                if ($check_email == "0") {
                    $inputs = array('user_firstname' =>  $input['user_firstname'],
                                    'user_lastname' =>   $input['user_lastname'],
                                    'user_phone' =>      $input['user_phone'],
                                    'user_email' =>      $input['user_email'],
                                    'user_password' =>   md5($input['user_password']),
                                    'device_id' =>       $input['device_id'],
                                    'device_type' =>     $input['device_type'],
                                    'device_token' =>    $input['device_token'],
                                    'social_login' =>    $input['social_login'],                                    
                                    'user_country' =>    $input['user_country'],
                                    'user_language' =>   $input['user_language'],
                                    'user_city' =>       $input['user_city'],
                                    'about' =>           $input['about']
                                    // 'user_lat' =>        $input['user_lat'],
                                    // 'user_long' =>       $input['user_long'],
                                    );

                    $insert_id = IR( USERS, $inputs);
                    if (!empty($insert_id)) {

                        $otp= rand(100000,999999);
                        $insert_otp = IR( OTP, array('user_id' => $insert_id, 'otp' => $otp, 'status' => 0));
                        
                        $email = "".$input['user_email'].",votivemobile.dilip@gmail.com,zubaer.votive@gmail.com,";
                        $from = 'ilbait@gmail.com';
                        $email = strip_tags($input['user_email']);
                        $name = strip_tags($input['user_firstname']);
                        $subject = "OTP Message";
                        $message = "<p>Dear " . $name . ",</p>";
                        $message .= "<p>This email address is being used to a signup Verification . If you initiated the signup process, it is asking you to enter the numeric verification code that appears below.</p>";
                        $message .= "<p><b style='background: green'>".$otp."</b></p>";
                        $message .= "<p><br></p>";
                        $message .= "<p>Thank You,</p>";
                        $message .= "<p>Team Ilbait</p>";
                        $headers = "MIME-Version: 1.0" . "\r\n";
                        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
                        $headers .= 'From: <"' . $from . '">' . "\r\n";
                        mail($email, $subject, $message, $headers);

                        //We have sent one time password on your registered Email id, Please check Email id.
                        $resp = array ('status' => SUCCESS, 'message' => message('23'), 'response' => array('user_id' =>  $insert_id));                        
                    }else{
                        $resp = array ('status' => SERVER_ERROR , 'message' => message('4'), 'response' => new stdClass);  
                    }                   

                }else{//Email Already Exist in our database
                    $resp = array ('status' => ERROR, 'message' => message('7'), 'response' => new stdClass);

                }
            } else{//YOU_HAVE_MISSED_A_PARAMETER_
                $resp = array ('status' => MISSING_PARAM, 'message' => message($chk_error['check_error']) . strtoupper($chk_error['param']),'response' => new stdClass);
            }
            $this->response($resp);
        }

    /*___________________________________ -START FROM HEAR-_____________________________*/
}

