<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
// defined('MASTER_EMAIL')      OR define('MASTER_EMAIL', "votive.pradeep01@gmail.com"); // master email to send email

defined('ADMIN_EMAILS')      OR define('ADMIN_EMAILS', "ilbait@gmail.com"); // admin email
// defined('SENDER_EMAIL')      OR define('SENDER_EMAIL', "votive.pradeep01@gmail.com"); // admin email
// defined('SENDER_NAME') 		 OR define('SENDER_NAME', 'Votive Yellow Pages');




/*____________________Prashant_____________*/



/* Define error codes for web servises */
define('SITE_NAME', 'App Admin');
define('META_TITLE', 'Welcome');

define('REGARD_MESSAGE', '<br/><br/><br/>Regards,<br/>Site Admin');
define('SUCCESS', '200');
define('ERROR', '201');
define('MISSING_PARAM', '202');
define('ALREADY_LOGIN', '203');
define('NOT_FOUND', '404');
define('SERVER_ERROR', '500');
define('LIKE_POSTING', 1);
define('DISLIKE_POSTING', 2);









define('USERS', 'users');
define('CATEGORY', 'otd_business_category');
define('OTP', 'bdc_otp');
define('MESSAGE', 'bdc_message');
define('SERVICES', 'bdc_services');
define('PACKAGE', 'otd_option_master');
define('OFFERS', 'bdc_offers');
define('ENQUIRY', 'bdc_enquiry');
define('BUSINESS', 'bdc_business');
define('COUNTRIES', 'countries');
define('NOTIFICATIONLIST', 'bdc_notification_list');
define('LANGUAGES', 'otd_languages');
define('FAVOURITE', 'bdc_favourite_business');
define('ADVERTISEMENT', 'otd_advertisement');
define('RATE', 'business_review');
define('CONTACTUS', 'contactus');
define('CITIES', 'cities');
define('TRANSACTION', 'transaction_table');
define('FEATURES', 'bdc_package_listing');
define('CONTACTDETAILES', 'contact_us_content');





define('UPLOADPATH', 'http://votivephp.in/businessdirectoryclassified/uploads/profile/');
define('CATEGORYPATH', 'http://votivephp.in/businessdirectoryclassified/assets/img/category/');
define('BUSINESSPATH', 'http://votivephp.in/businessdirectoryclassified/uploads/business/');
define('ADVERTISEMENTPATH', 'http://votivephp.in/businessdirectoryclassified/assets/img/advertise/');



defined('PAYPAL_MERCHANT_USER') OR define('PAYPAL_MERCHANT_USER', "akash_merchant_api1.gmail.com");
defined('PAYPAL_MERCHANT_PWD') OR define('PAYPAL_MERCHANT_PWD', "JDH98G3QMP8RR882");
defined('PAYPAL_MERCHANT_SIGNATURE') OR define('PAYPAL_MERCHANT_SIGNATURE', "An5idV7hrMq-VYPAOaZ.dkiiYqsTAWRxCKWATExH-4vyqYbecflacbUZ");
defined('PAYPAL_API_URL') OR define('PAYPAL_API_URL', "https://api-3t.sandbox.paypal.com/nvp");
//defined('PAYPAL_API_URL') OR define('PAYPAL_API_URL', "https://api-3t.paypal.com/nvp");