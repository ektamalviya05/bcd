<?php  
if (!defined('BASEPATH')) exit('No direct script access allowed');

// ------------------------------------------------------------------------
// Slimpay configuration
// ------------------------------------------------------------------------

// Use Slimepay on test or Live
$config['test_mode'] = TRUE; // FALSE for live environment

// Slimpay access URL
$config['sm_server_url'] = 'https://api.preprod.slimpay.com'; // for testing purpose
// $config['sm_server_url'] = 'https://api.slimpay.net'; // for production purpose
// Slimpay App ID
$config['sm_appid'] = 'otourdemoserver';
// Slimpay app secret
$config['sm_appsecret'] = '7bUuy59Zz8jP4zWYUbC9b3PzQR5~ubRh';
// Slimpay Creditor refernece
$config['sm_creditor'] = 'otourdemoserver';

$config['sm_relation_namespace'] = 'https://api.slimpay.net/alps#';

// Slimpay return url, this is optional
$config['sm_return_url'] = 'http://www.otourdemoi.pro/Users/completePayment';

// Slimpay notification url, this is optional
$config['sm_noti_url'] = 'http://www.otourdemoi.pro/Pages/slimpayIPN';
?>
