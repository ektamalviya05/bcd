<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
| -------------------------------------------------------------------
|  Facebook API Configuration
| -------------------------------------------------------------------
|
| To get an facebook app details you have to create a Facebook app
| at Facebook developers panel (https://developers.facebook.com)
|
|  facebook_app_id               string   Your Facebook App ID.
|  facebook_app_secret           string   Your Facebook App Secret.
|  facebook_login_type           string   Set login type. (web, js, canvas)
|  facebook_login_redirect_url   string   URL to redirect back to after login. (do not include base URL)
|  facebook_logout_redirect_url  string   URL to redirect back to after logout. (do not include base URL)
|  facebook_permissions          array    Your required permissions.
|  facebook_graph_version        string   Specify Facebook Graph version. Eg v2.6
|  facebook_auth_on_load         boolean  Set to TRUE to check for valid access token on every page load.
*/
//$config['facebook_app_id']              = '235752547221661';
//$config['facebook_app_secret']          = '4287b09dd0fef61fa2e99c0b8d269583';
//$config['facebook_app_id']              = '198255107547850';
//$config['facebook_app_secret']          = 'e92086e9efa65def1c8aa97339f5bc9e';

$config['facebook_app_id']              = '108759993167087';
$config['facebook_app_secret']          = 'acb84895108ec0f0626118eae597417e';
$config['facebook_login_type']          = 'web';
$config['facebook_login_redirect_url']  = 'Users/fb_login';
$config['facebook_logout_redirect_url'] = 'login/logout';
$config['facebook_permissions']         = array('email');
$config['facebook_graph_version']       = 'v2.6';
$config['facebook_auth_on_load']        = TRUE;