<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] 	= 'Pages';
$route['404_override'] 			= '';
$route['translate_uri_dashes'] 	= FALSE;
$route['home'] 					= 'Pages';
$route['signup'] 				= 'Users/signup';
$route['logout'] 				= 'Pages/logout';
$route['activate/(:any)'] 		= 'Users/activate/$1';
$route['login'] 				= 'Users/login';
$route['forgetpassword'] 		= 'Users/forgetpassword';
$route['do_login'] 				= 'Users/do_login';
$route['dashboard'] 			= 'Dashboards/index';
$route['addlisting'] 			        = 'Users/addListingView';
$route['listings'] 			            = 'Pages/Listings';
$route['listings/(:any)'] 		        = 'Pages/Listings/$1';
$route['offers'] 			            = 'Pages/offer';
$route['about-us-mobile'] 			            = 'Pages/about_us_mobile';
$route['contact-us'] 			            = 'Pages/contact_us';
//$route['listing-details'] 			            = 'Pages/listing_details';
$route['listings_details/(:any)'] 		= 'Pages/listing_details/$1';
$route['categories'] 			        = 'Pages/CategoryListing';
$route['submit_review'] 			        = 'Pages/submit_review';
$route['send_contactInfo'] 			        = 'Pages/send_contactInfo';

$route['term-condition']        = 'Pages/term_condition';
$route['term-condition-mobile']        = 'Pages/term_condition_mobile';
$route['privacy-policy']        = 'Pages/privacy_policy';
$route['privacy-policy-mobile']        = 'Pages/privacy_policy_mobile';
$route['home/(:any)']			 = 'Pages/lang/$1';
//$route['listings/(:any)']		 = 'Pages/lang/$1';
$route['categories/(:any)']		 = 'Pages/lang/$1';
$route['offers/(:any)']		     = 'Pages/lang/$1';
$route['signup/(:any)']		     = 'Pages/lang/$1';
$route['about-us/(:any)']		     = 'Pages/lang/$1';
$route['privacy-policy/(:any)']		     = 'Pages/lang/$1';
$route['term-condition/(:any)']		     = 'Pages/lang/$1';
$route['contact-us/(:any)']		     = 'Pages/lang/$1';
$route['addlisting/(:any)']		     = 'Pages/lang/$1';
$route['dashboard/(:any)']		     = 'Pages/lang/$1';
//$route['details/(:any)(:any)']		     = 'Pages/lang1/$1/$2';
//$route['details/(:any)/{:any}'] = 'Pages/lang1/$1/$2';
$route['enquiry']		     = 'Review/enquiry';
$route['enquiry/(:any)']		     = 'Review/enquiry/$1';
$route['paymentcancel']		     = 'Pages/paymentcancel';
$route['paymentsuccess']		     = 'Pages/paymentsuccess';
$route['paymentsuccess/(:any)']		     = 'Pages/paymentsuccess';
$route['success']		     = 'Pages/success';
$route['cancel']		     = 'Pages/cancel';

$route['buy_package'] 			= 'Dashboards/buy_package';
$route['listing_data'] 			= 'Pages/listing_data';
$route['transactionRecord']		     = 'Review/transactionRecord';
$route['transactionRecord/(:any)']		     = 'Review/transactionRecord/$1';
$route['updatelisting/(:any)']                     = 'Dashboards/listingdetail/$1';
$route['deactivelisting/(:any)']                     = 'Dashboards/activelisting/$1';
$route['active/(:any)']                     = 'Dashboards/active/$1';
$route['subcategory'] 			        = 'Pages/Cat';
$route['subcategory/(:any)'] 			        = 'Pages/Cat/$1';

$route['buy-package-plan'] 			= 'Users/plan';
$route['about-us']        = 'Pages/about_us';
//$route['buy-featured-plan'] 			= 'Users/plan';

$route['paysuccess']		     = 'Pages/paysuccess';
$route['paysuccess/(:any)']		     = 'Pages/paysuccess';
$route['addservices']                     = 'Dashboards/addservices';
$route['add_offer']                     = 'Dashboards/addoffer';

$route['listings1']                     = 'Pages/Listings1';
$route['listings1/(:any)'] 		        = 'Pages/Listings1/$1';

$route['catListings']                     = 'Pages/Listings2';
$route['catListings/(:any)'] 		        = 'Pages/Listings2/$1';
