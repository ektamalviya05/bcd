$(function () {
  var parentTag="";
    $(".star").on("mouseover", function () { //SELECTING A STAR
       
        $(".rating").hide(); //HIDES THE CURRENT RATING WHEN MOUSE IS OVER A STAR
        var d = $(this).attr("id"); //GETS THE NUMBER OF THE STAR
        parentTag = $( this ).parent().parent().parent().attr('id');
        //HIGHLIGHTS EVERY STAR BEHIND IT
        for (i = (d - 1); i >= 0; i--) {
            $(".transparent .star:eq(" + i + ")").css({"opacity": "1.0"});
        }
    }).on("click", function () { //RATING PROCESS
        var blog_id = parentTag.substr(10); //$("#blog_content_id").val(); //GETS THE ID OF THE CONTENT
        var rating = $(this).attr("id"); //GETS THE NUMBER OF THE STAR
        var data = 'rating=' + rating + '&blog_id=' + blog_id;
        //alert(data);
        $.ajax({
            type: "POST",
            data: data,
            //url: "http://localhost/hmvc_site/Rating/rate_blog",
           url: "http://votivephp.in/VotiveYellowPages/Rating/rate_blog", //CALLBACK FILE
            success: function (e) {
                var vote_cont = "#"+parentTag; //"#ajax_vote_"+blog_id;
                $(vote_cont).html(e); //DISPLAYS THE NEW RATING IN HTML
                alert(vote_cont + JSON.stringify(e));
            },
            error: function (e) {
                alert("error"+ JSON.stringify(e));
            }
        });
    }).on("mouseout", function () { //WHEN MOUSE IS NOT OVER THE RATING
        $(".rating").show(); //SHOWS THE CURRENT RATING
        $(".transparent .star").css({"opacity": "0.25"}); //TRANSPARENTS THE BASE
    });
});