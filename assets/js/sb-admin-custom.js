//var siteurl = window.location.origin;
var l = window.location;
//var siteurl = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1]+ "/" + l.pathname.split('/')[2]+ "/" ;
var siteurl = l.protocol + "//" + l.host + "/" + l.pathname.split('/')[1]+ "/"  ;
//alert(base_url); 

$("#updateregnewsletter1, #updateregnewsletter2, #updateregnewsletter3, #updateregnewsletter4").on("submit", function(){	
	$.ajax({
		url: siteurl+"/Content/updateregNLetter",
		data:$(this).serialize(),
		type: "POST",
		dataType: "json",
		success:function(data){
			$("#"+data.render).html(data.message);
			setTimeout(function(){
				$("#"+data.render).html("");
			}, 2000);
		}
	})
	return false;
});
$(".trash-page").on("click", function(){
	var row = $(this);
	var id = row.attr("modal-aria");
	$.ajax({
		url:siteurl+"/Content/trashPage",
		data:{"id":id},
		type:"POST",
		dataType:"json",
		async: false,
		success: function(data){				
			if(data.status == 200){
				row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
			}
		}
	});
});

$(".trashback-page").on("click", function(){	
	var row = $(this);
	var id = row.attr("modal-aria");
	$.ajax({
		url:siteurl+"/Content/trashbackPage",
		data:{"id":id},
		type:"POST",
		dataType:"json",
		async: false,
		success: function(data){				
			if(data.status == 200){
				row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
			}
		}
	});
});

$(".trashback-publishpage").on("click", function(){	
	var row = $(this);
	var id = row.attr("modal-aria");
	$.ajax({
		url:siteurl+"/Content/trashbackAndPublishPage",
		data:{"id":id},
		type:"POST",
		dataType:"json",
		async: false,
		success: function(data){				
			if(data.status == 200){
				row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
			}
		}
	});
});

// Testimonials code
	$(".testi_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Testimonial/trashTestimonial",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){}
		});
	});
	$(".trash-testi").on("click", function(){		
		// if(confirm("Do you really want to move page in trash?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Testimonial/trashTestimonial",
				data:{"id":id, "mode":3},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){
					row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		// }	
	});

	$(".trashback-testi").on("click", function(){	
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-reply").addClass("fa-spinner fa-pulse");		
		$.ajax({
			url:siteurl+"/Testimonial/trashTestimonial",
			data:{"id":id, "mode":0},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				row.find("i").addClass("fa-reply").removeClass("fa-spinner fa-pulse");				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});

	$(".trashback-publishtesti").on("click", function(){	
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-window-restore").addClass("fa-spinner fa-pulse");
		$.ajax({
			url:siteurl+"/Testimonial/trashTestimonial",
			data:{"id":id, "mode":1},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				row.find("i").addClass("fa-window-restore").removeClass("fa-spinner fa-pulse");				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});
//Testimonials code end
// $(".delete-page").on("click", function(){
// 	if(confirm("Do you really want to delete page?")){
// 		var row = $(this);
// 		var id = row.attr("modal-aria");
// 		$.ajax({
// 			url:siteurl+"/Content/deletePage",
// 			data:{"id":id},
// 			type:"POST",
// 			dataType:"json",
// 			async: false,
// 			success: function(data){				
// 				if(data.status == 200){
// 					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
// 				}
// 			}
// 		});
// 	}
	
// });

$(".page_status").on("change", function(){	
	var row = $(this);
	var id = row.attr("modal-aria");
	var val = 0;
	if(row.prop("checked")){
		val = 1;
	}
	$.ajax({
		url:siteurl+"/Content/pageUpdateStatus",
		data:{"id":id, "value":val},
		type:"POST",
		dataType:"json",
		async: false,
		success: function(data){
		}
	});
});

//Home section 3 Update
	$("#section3-1").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section3-1")[0]);        
	        $("#sub31").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton3/1",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$("#sub31").prop("disabled", false);                
	                $("#sec31msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec31msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section3-2").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section3-2")[0]);        
	        $("#sub32").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton3/2",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$("#sub32").prop("disabled", false);                
	                $("#sec32msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec32msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section3-3").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section3-3")[0]);        
	        $("#sub33").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton3/3",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$("#sub33").prop("disabled", false);                
	                $("#sec33msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec33msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
//Home section 3 Update End

//Home section 6 Update
	$("#section6-7").validate({
		rules:{
			secimg:"required"
		},
		messages:{
			secimg: "Please select an image to upload"
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-7")[0]);        
	        $("#sub7").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSectonBackgroundImage/7",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$("#sub7").prop("disabled", false);                
	                $("#sec7msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec7msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section6-1").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-1")[0]);        
	        $("#sub1").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton6/1",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$("#sub1").prop("disabled", false);                
	                $("#sec1msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec1msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section6-2").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-2")[0]);        
	        $("#sub2").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton6/2",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){                
	                $("#sub2").prop("disabled", false);                
	                $("#sec2msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec2msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section6-3").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-3")[0]);        
	        $("#sub3").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton6/3",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){                
	                $("#sub3").prop("disabled", false);                
	                $("#sec3msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec3msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section6-4").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-4")[0]);        
	        $("#sub4").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton6/4",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){                
	                $("#sub4").prop("disabled", false);                
	                $("#sec4msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec4msg").html("");
	                }, 2000);
	            }
	        });
			return false;
		}
	});
	$("#section6-5").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-5")[0]);        
	        $("#sub5").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton6/5",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){                
	                $("#sub5").prop("disabled", false);                
	                $("#sec5msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec5msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
	$("#section6-6").validate({
		rules:{
			title:"required",
			template1:{
				required:true,
				maxlength:300,
			}
		},
		messages:{
			title:"Please enter title",
			template1:{
				required:"Please enter description",
				maxlength:"More than 300 characters not allowed"
			}
		},
		submitHandler:function(e){
			var formData = new FormData($("#section6-6")[0]);        
	        $("#sub6").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/UpdateSecton6/6",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){                
	                $("#sub6").prop("disabled", false);                
	                $("#sec6msg").html(data.msg);
	                setTimeout(function(){
	                    $("#sec6msg").html("");
	                }, 2000);
	            }            
	        });
			return false;
		}
	});
/// Country

$("#addCountry").validate({
       rules:{
           name:"required",
           arabic:"required"
       },
       messages:{
           name:"Please enter a country name in english",
           arabic:"Please enter a country name in arabic",
       },
       submitHandler:function(e){      
           var formData = new FormData($("#addCountry")[0]);    
           $("#addsubmit").prop("disabled", true);
           //alert(formData);    
           //$("#addsubmit").prop("disabled", true);
           $.ajax({
               type:'POST',
               url: siteurl+"/Country/addCountry",
               data:formData,
               cache:false,
               contentType: false,
               processData: false,
               dataType:"json",
               success:function(data){   

                 //alert(data.type);        
                    
                   $("#addsubmit").prop("disabled", false);                
                   if(data.type == 2)
                       $("#addimgmsg").html(data.msg);
                   else
                       $("#addmsg").html(data.msg);
                   setTimeout(function(){
                       $("#addmsg, #addimgmsg").html("");
                       if(data.status == 200){
                           window.location.href = siteurl+"/Country/index";
                       }
                   }, 1000);

               }            
           });
           return false;
       }
   });
/// city country

$("#addCity").validate({
       rules:{
           name:"required",
           country: { required: true }

       },
       messages:{
           name:"Please enter a City name",
           country:"Please select a Country name"
       },
       submitHandler:function(e){      
           var formData = new FormData($("#addCity")[0]);    

           //alert(formData);    
           //$("#addsubmit").prop("disabled", true);
           $.ajax({
               type:'POST',
               url: siteurl+"/City/addCity",
               data:formData,
               cache:false,
               contentType: false,
               processData: false,
               dataType:"json",
               success:function(data){   

                 //alert(data.type);        
                    
                   $("#addsubmit").prop("disabled", false);                
                   if(data.type == 2)
                       $("#addimgmsg").html(data.msg);
                   else
                       $("#addmsg").html(data.msg);
                   setTimeout(function(){
                       $("#addmsg, #addimgmsg").html("");
                       if(data.status == 200){
                           window.location.href = siteurl+"/City/index";
                       }
                   }, 1000);

               }            
           });
           return false;
       }
   });
// Edit City

$("#editcity").validate({
   rules:{    	
   	name:"required",
   	country: { required: true }
   	//status:"required",
   	//display_order:"required"
   },
   messages:{    	
   	name:"Please enter a City name",
   	country:"Please select a Country name"
   	//status:"Please select status",
   	//display_order:"Please select display order"
   },
   submitHandler:function(e){    	
   	var formData = new FormData($("#editcity")[0]);        
       $("#addsubmit").prop("disabled", true);
       $.ajax({
           type:'POST',
           url: siteurl+"/City/editCity",
           data:formData,
           cache:false,
           contentType: false,
           processData: false,
           dataType:"json",
           success:function(data){
            $("#addsubmit").prop("disabled", false);
               if(data.type == 2)
                $("#addimgmsg").html(data.msg);
               else
                $("#addmsg").html(data.msg);
               setTimeout(function(){
                   $("#addmsg, #addimgmsg").html("");
                   if(data.status == 200){
                   	window.location.href = siteurl+"/City";
                   }
               }, 1000);
           }
       });
return false;
   }
});

/// trash city 

$(".trash-city").on("click", function(){
	if(confirm("Do you really want to move city in trash?")){
	var row = $(this);
	var id = row.attr("modal-aria");	
	$.ajax({
	url:siteurl+"/City/trashAdvertisement",
	data:{"id":id, "status":3},
	type:"POST",
	dataType:"json",
	async: false,
	success: function(data){	

	   //alert(data.status);	
	if(data.status == 200){
	row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
	}
	}
	});
	}	
});


$("#editcountry").validate({
   rules:{    	
   	name:"required",
   	//status:"required",
   	//display_order:"required"
   },
   messages:{    	
   	name:"Please enter a Category name",
   	//status:"Please select status",
   	//display_order:"Please select display order"
   },
   submitHandler:function(e){    	
   	var formData = new FormData($("#editcountry")[0]);        
       $("#addsubmit").prop("disabled", true);
       $.ajax({
           type:'POST',
           url: siteurl+"/Country/editCountry",
           data:formData,
           cache:false,
           contentType: false,
           processData: false,
           dataType:"json",
           success:function(data){
            $("#addsubmit").prop("disabled", false);
               if(data.type == 2)
                $("#addimgmsg").html(data.msg);
               else
                $("#addmsg").html(data.msg);
               setTimeout(function(){
                   $("#addmsg, #addimgmsg").html("");
                   if(data.status == 200){
                   	window.location.href = siteurl+"/Country";
                   }
               }, 1000);
           }
       });
return false;
   }
});

/// trash Country

$(".trash-country").on("click", function(){
	if(confirm("Do you really want to move country in trash?")){
	var row = $(this);
	var id = row.attr("modal-aria");	
	$.ajax({
	url:siteurl+"/Country/trashAdvertisement",
	data:{"id":id, "status":3},
	type:"POST",
	dataType:"json",
	async: false,
	success: function(data){	

	   //alert(data.status);	
	if(data.status == 200){
	row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
	}
	}
	});
	}	
});

//Home section 6 Update End

// radio button 


$('#id_radio1').click(function () {
	
        $('#div2').hide();
        $('#div1').show();
});
$('#id_radio2').click(function () {
    $('#div1').hide();
    $('#div2').show();
});

//Banner update
	$("#uploadbanner").validate({
		rules:{
			ban_file:"required"
		},
		messages:{
			ban_file:"Please select a file"
		},
		submitHandler:function(e){
			var formData = new FormData($("#uploadbanner")[0]);        
	        $("#bansub").prop("disabled", true);
	        $(".loading").show();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Banner/add",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#bansub").prop("disabled", false);
	                $("#uplmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#uplmsg").html("");
	                    if(data.status == 200)
	                		location.reload();
	                }, 1000);
	                
	            }            
	        });
			return false;
		}
	});
	$(".banner_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Banner/trashBanner",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){}
		});
	});
	$(".trash-banner").on("click", function(){		
		// if(confirm("Do you really want to move page in trash?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Banner/trashBanner",
				data:{"id":id, "mode":3},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){
					row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		// }	
	});


	$(".dluser").on("click", function(){
		if(confirm("This will delete all related records that can not be revoke. Are you sure?")){		
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
		$.ajax({
			url:siteurl+"/Admin/deleteproUser",
			data:{"id":id},
			type:"POST",
			dataType:"html",
			async: false,
			success: function(data){
				//console.log(data);
				row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					location.reload();
				}else{
					//alert(data.msg);
					location.reload();
				}
			}
		});
	 } 

	});

	$(".trashback-banner").on("click", function(){	
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-reply").addClass("fa-spinner fa-pulse");		
		$.ajax({
			url:siteurl+"/Banner/trashBanner",
			data:{"id":id, "mode":0},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				row.find("i").addClass("fa-reply").removeClass("fa-spinner fa-pulse");				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});

	$(".trashback-publishbanner").on("click", function(){	
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-window-restore").addClass("fa-spinner fa-pulse");
		$.ajax({
			url:siteurl+"/Banner/trashBanner",
			data:{"id":id, "mode":1},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				row.find("i").addClass("fa-window-restore").removeClass("fa-spinner fa-pulse");				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});
//Banner update end

//User Dashboard side bar content management
	$("#uploaddebit").validate({
		rules:{
			ban_file:"required",
			imglink:"required"
		},
		messages:{
			ban_file:"Please select a file",
			imglink:"Please enter # in case of blank link"
		},
		submitHandler:function(e){
			var formData = new FormData($("#uploaddebit")[0]);        
	        $("#bansub").prop("disabled", true);
	        $(".loading").show();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/uploadDebitCardImage",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#bansub").prop("disabled", false);
	                $("#uplmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#uplmsg").html("");
	                    if(data.status == 200)
	                		location.reload();
	                }, 1000);
	                
	            }            
	        });
			return false;
		}
	});
	$("#updatedashboardarticle").on("submit", function(){
		$(".loading").show();
	    $("#catsub").prop("disabled", true);
	    $.ajax({
	        url: $("#updatedashboardarticle").attr("action"),
	        data:{ "label3" : CKEDITOR.instances.label3.getData() },
	        type: "POST",
	        dataType: "json",
	        success:function(data){
	            $(".loading").hide();
	            $("#catsub").prop("disabled", false);
	            $("#catupmsg").html(data.msg);
	            setTimeout(function(){
	                $("#catupmsg").html("");
	            }, 2000);
	        }
	    });
		return false;
	});
	$(".debitcard_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Content/debitCardStatusUpdate",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){}
		});
	});
	$(".delet-debitcard").on("click", function(){		
		// if(confirm("Do you really want to move page in trash?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Content/removeDebitCard",
				data:{"id":id},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){
					row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		// }	
	});
//User Dashboard side bar content management end
// Category related code
	$("input[name=displayhomecattab]:radio").change(function () {
		if($(this).val() == 1){
			$(".displaytabdiv").show();
		}
		else{
			$(".displaytabdiv").hide();
		}
	})
	$("#addcategory").validate({
		rules:{
			name:"required",
			namea:"required",
			description:"required",
			descriptiona:"required",
			status:"required",
			displaytab:"required"
		},
		messages:{
			name:"Please enter category name in english",
			namea:"Please enter category name in arabic",
			description:"Please enter category description in english",
			descriptiona:"Please enter category description in arabic",
			status:"Please Select category status",
			displaytab:"Please Select display tab"
		},
		submitHandler:function(e){
			var formData = new FormData($("#addcategory")[0]);        
	        $("#catsub").prop("disabled", true);
	        $(".loading").show();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Category/add",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                if(data.type == 1)
	                	$("#flupmsg").html(data.msg);
	                else
	                	$("#catupmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#catupmsg, #flupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/Category";
	                }, 1000);
	            }            
	        });
			return false;
		}
	});




	$("#addbusiness").validate({
		rules:{
			name:"required",
			description:"required",
			status:"required",
			//displaytab:"required",
		 	business_email:"required",
		 	//business_mobile:"required",
			business_mobile:{maxlength:8,required:true,
  					number: true},
			business_image1:"required",
			//business_address:"required",
			//business_address1:"required",
			//business_country:"required",
		    features_ads:"required",
			cat_id:"required",
		},
		messages:{
			name:"Please enter Business name",
			description:"Please enter Business description",
			status:"Please select Business status",
			//displaytab:"Please select display tab",
			business_email:"Please enter Business Email",
			business_mobile:{required: "Please enter Business Mobile",
                number: "This field accepts only numbers",
                maxlength: "Please insert 8 digit phone number"},
			business_image1:"Please enter Business Image",
			//business_address:"Please enter Business Address",
			//business_address1:"Please enter Business Address",
			//business_country:"Please enter Country",
			features_ads:"Please select Business features ads",
			cat_id:"Please enter category",
		},
		submitHandler:function(e){
			var formData = new FormData($("#addbusiness")[0]);        
	        $("#businessadd").prop("disabled", true);
	        $(".loading").show();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Listing/add",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	                
	                ///console.log(data);
	            	//console.log(data.typeone);
                                
	                if(data.typeone === 1) {
	                	$("#flupmsgone").html(data.msg);
	                }
	                else if(data.typetwo === 1) {
	                	$("#flupmsgtwo").html(data.msg);
	                }
	                else if(data.typethree === 1) {
	                	$("#flupmsgthree").html(data.msg);
	                }
	                else if(data.typefour === 1) {
	                	$("#flupmsgfour").html(data.msg);
	                }
	                else if(data.typefive === 1) {
	                	$("#flupmsgfive").html(data.msg);
	                }
	                else {
	                	$("#catupmsg").html(data.msg);
	                }
	                $("#businessadd").prop("disabled", false);
	            	$(".loading").hide();
	                setTimeout(function(){
	                    $("#catupmsg, #flupmsg1").html("");
	                    if(data.status === 200)
	                		window.location.href = siteurl+"/Listing";
	                }, 1000);
	            }            
	        });
			return false;
		}
	});


	$("#editbusiness").validate({
		rules:{
			name:"required",
			description:"required",
			status:"required",
			business_mobile:{maxlength:8,required:true,
  					number: true},
  			cat_id:"required",
  			business_email:"required",
  			features_ads:"required",
  			
			//displaytab:"required"
		},
		messages:{
			name:"Please enter Business name",
			description:"Please enter category description",
			status:"Please select category status",
			business_mobile:{required: "Please enter Business Mobile",
                number: "This field accepts only numbers",
                maxlength: "Please insert 8 digit phone number",},
			cat_id:"Please select Business Category",
			business_email:"Please enter Business Email",
			features_ads:"Please select feature Ads",
			
		},
		submitHandler:function(e){
			var formData = new FormData($("#editbusiness")[0]);        
	        $("#businessadd").prop("disabled", true);
	        $(".loading").show();
	        var id = $("#business_id").val();
           // alert(siteurl+"Listing/edit/"+id);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"Listing/edit/"+id,
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	console.log(data.typeone);
                                
	                if(data.typeone === 1) {
	                	$("#flupmsgone").html(data.msg);
	                }
	                else if(data.typetwo === 1) {
	                	$("#flupmsgtwo").html(data.msg);
	                }
	                else if(data.typethree === 1) {
	                	$("#flupmsgthree").html(data.msg);
	                }
	                else if(data.typefour === 1) {
	                	$("#flupmsgfour").html(data.msg);
	                }
	                else {
	                	$("#catupmsg").html(data.msg);
	                }
	                $("#businessadd").prop("disabled", false);
	            	$(".loading").hide();
	                setTimeout(function(){
	                    $("#catupmsg, #flupmsg1").html("");
	                    if(data.status === 200)
	                		window.location.href = siteurl+"Listing";
	                }, 1000);
	            },
	            error :function(data){   
	            	//console.log(data);
	            	$("#catupmsg").html("Business Listing edit successfully");
	            	$(".loading").hide();
	            	 setTimeout(function(){
	            	window.location.href = siteurl+"Listing";
	            	 }, 1000);
	            }       
	        });
			return false;
		}
	});


	$("#cat_id").change(function() {
	    var cat_id = $(this).val();
	    //alert(siteurl+'Listing/category');
	    if(cat_id != "/Listing/category") {
		      $.ajax({
		        url: siteurl+"Listing/category",
		        data:{cat_id:cat_id},
		        type:'POST',
		        success:function(response) {
		        		//console.log(response);
		        		
		        		
		          		var resp = $.trim(response);
		          		if(resp){
		          		    $("#sub_cat_id").html(resp);
		          		    $("#sub_cat_id").css("cursor","auto");
		          		    $("#sub_cat_id").removeAttr("disabled");
		          		}else{
		          		    $("#sub_cat_id").prop('disabled', 'disabled');
		          		    $("#sub_cat_id").html('<option value="">There is no sub Category</option>');
		          		     $("#sub_cat_id").css("cursor","not-allowed");
		          		}
		          		
		          		
		        },

		        error :function(response) {
		        		//console.log(response);
		          		
		        }
		      });
		    } 
	   else
	   {
	      $("#sub_cat_id").html("<option value=''>Select Business Category First</option>");
	   }
  });


	$("#editcategory").validate({
		rules:{
			name:"required",
			type:"required",
			description:"required",
			status:"required",
			displaytab:"required"
		},
		messages:{
			name:"Please enter category name",
			type:"Please enter type",
			description:"Please enter category description",
			status:"Please select category status",
			displaytab: "Please select display tab"
		},
		submitHandler:function(e){
			var formData = new FormData($("#editcategory")[0]);        
	        $("#catsub").prop("disabled", true);
	        $(".loading").show();
	        var id = $("#editid").val();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Category/edit/"+id,
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                if(data.type == 1)
	                	$("#flupmsg").html(data.msg);
	                else
	                	$("#catupmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#catupmsg, #flupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/Category";
	                }, 1000);
	            }            
	        });
			return false;
		}
	});
	$(".cat_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Category/trashCategory1",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){}
		});
	});	
	$(".business_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Listing/trashCategory",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){}
		});
	});
	
	$(".rating_status").on("change", function(){
		//alert("sadas");
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Review/changestatus",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){}
		});
	});

	$(".review_ed").on("change", function(){
		//alert("sadas");
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Review/changereviewstatus",
			data:{"id":id, "mode":val},
			type:"POST",
			success: function(data){
				//console.log(data);
			}
		});
	});

	$(".trash-cat").on("click", function(){		
		// if(confirm("Do you really want to move page in trash?")){
		if(confirm("Are you sure?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Category/trashCategory",
				data:{"id":id, "mode":3},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){
					row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		}	
	});

	$(".trash_business").on("click", function(){		
		// if(confirm("Do you really want to move page in trash?")){
		if(confirm("Are you sure?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			//alert(id);
			row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Listing/deltrash",
				data:{"id":id, "mode":3},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){

					console.log(data);
					row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		}	
	});

	$(".trash_subscriber").on("click", function(){		
		// if(confirm("Do you really want to move page in trash?")){
		if(confirm("Are you sure?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			//alert(id);
			row.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Admin/deleteSubscriber",
				data:{"id":id, "mode":3},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){

					console.log(data);
					row.find("i").addClass("fa-close").removeClass("fa-spinner fa-pulse");
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		}	
	});

	$(".trashback-cat").on("click", function(){	
		if(confirm("Are you sure?")){
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-reply").addClass("fa-spinner fa-pulse");		
		$.ajax({
			url:siteurl+"/Category/trashCategory",
			data:{"id":id, "mode":0},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				row.find("i").addClass("fa-reply").removeClass("fa-spinner fa-pulse");				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	}
	});

	$(".trashback-list").on("click", function(){	
		if(confirm("Are you sure ?")){
			var row = $(this);

			var id = row.attr("modal-aria");
			//alert(id);
			row.find("i").removeClass("fa-reply").addClass("fa-spinner fa-pulse");		
			$.ajax({
				url:siteurl+"/Listing/trashCategory",
				data:{"id":id, "mode":0},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){
					
					row.find("i").addClass("fa-reply").removeClass("fa-spinner fa-pulse");				
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
	   }
	});

	$(".trashback-publishcat").on("click", function(){	
		if(confirm("Are you sure?")){
		var row = $(this);
		var id = row.attr("modal-aria");
		row.find("i").removeClass("fa-window-restore").addClass("fa-spinner fa-pulse");
		$.ajax({
			url:siteurl+"/Category/trashCategory",
			data:{"id":id, "mode":1},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				row.find("i").addClass("fa-window-restore").removeClass("fa-spinner fa-pulse");				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	}
	});

	$(".trashback-publishlist").on("click", function(){	
		if(confirm("Are you sure ?")){
			var row = $(this);
			var id = row.attr("modal-aria");
			row.find("i").removeClass("fa-window-restore").addClass("fa-spinner fa-pulse");
			$.ajax({
				url:siteurl+"/Listing/trashCategory",
				data:{"id":id, "mode":1},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){
					row.find("i").addClass("fa-window-restore").removeClass("fa-spinner fa-pulse");				
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		}
	});

	$("#editcategorytab").validate({
		rules:{
			name:"required",
			type:"required",
			description:"required",
			status:"required",
			displaytab:"required"
		},
		messages:{
			name:"Please enter category name",
			type:"Please enter type",
			description:"Please enter category description",
			status:"Please select category status",
			displaytab: "Please select display tab"
		},
		submitHandler:function(e){
			var formData = new FormData($("#editcategorytab")[0]);        
	        $("#catsub").prop("disabled", true);
	        $(".loading").show();	        
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Category/editCatTab/",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                $("#catupmsg").html(data.msg);	               
	                setTimeout(function(){
	                    $("#catupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/Category/homeTab";
	                }, 1000);
	            }
	        });
			return false;
		}
	});
// Category related code end

// ExtraFilters related code
	$("#addfilters").validate({
		rules:{
			name:"required",
			description:"required",
			status:"required"
		},
		messages:{
			name:"Please enter category name",
			description:"Please enter category description",
			status:"Please select category status"
		},
		submitHandler:function(e){
			var formData = new FormData($("#addfilters")[0]);        
	        $("#catsub").prop("disabled", true);
	        $(".loading").show();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/ExtraFilters/add",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                if(data.type == 1)
	                	$("#flupmsg").html(data.msg);
	                else
	                	$("#catupmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#catupmsg, #flupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/ExtraFilters";
	                }, 1000);
	            }            
	        });
			return false;
		}
	});
	$("#editfilters").validate({
		rules:{
			name:"required",			
			description:"required",
			status:"required"
		},
		messages:{
			name:"Please enter category name",			
			description:"Please enter category description",
			status:"Please select category status"
		},
		submitHandler:function(e){
			var formData = new FormData($("#editfilters")[0]);        
	        $("#catsub").prop("disabled", true);
	        $(".loading").show();
	        var id = $("#editid").val();
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/ExtraFilters/edit/"+id,
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                if(data.type == 1)
	                	$("#flupmsg").html(data.msg);
	                else
	                	$("#catupmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#catupmsg, #flupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/ExtraFilters";
	                }, 1000);
	            }            
	        });
			return false;
		}
	});
// ExtraFilters related code end


// Update social links
   $("#updateSociallinks").validate({
   	rules:{
   		facebook: "required",
		twitter: "required",
		instagram: "required"
   	},
   	messages:{
   		facebook:"Please enter facebook link",
		twitter: "Please enter twitter link",
		instagram: "Please enter instagram link"
   	},
   	submitHandler:function(e){
   		$("#socialsub").prop("disabled", true);
   		$(".loading").show();
   		$.ajax({
			url:siteurl+"/Content/updateSocialLinks",
			data:$("#updateSociallinks").serialize(),
			type:"POST",
			dataType:"json",			
			success: function(data){				
				$("#socialsub").prop("disabled", false);
   				$(".loading").hide();
   				$("#facebookmsg").html(data.msg);
                setTimeout(function(){
                    $("#facebookmsg").html("");                    
                }, 2000);
			}
		});
   		return false;   		
   	}
   })
// Update social links

$("input[name=vdtype]:radio").change(function () {
	if($(this).val() == 1){
		$(".youtube").show();
		$(".uploadvideo").hide();
	}
	else{
		$(".youtube").hide();
		$(".uploadvideo").show();
	}
});

// Advertisement scripts
	$("#addadvertisement").validate({
	    rules:{
	    	ad_image: "required",
	    	link:"required",
	    	status:"required",
	    	display_order:"required"
	    },
	    messages:{
	    	ad_image: "Please select an image to upload",
	    	link:"Please enter a redirect link",
	    	status:"Please select status",
	    	display_order:"Please select display order"
	    },
	    submitHandler:function(e){    	
	    	var formData = new FormData($("#addadvertisement")[0]);        
	        $("#addsubmit").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/addAdervertisement",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){            	
	            	$("#addsubmit").prop("disabled", false);                
	                if(data.type == 2)
	                	$("#addimgmsg").html(data.msg);
	                else
	                	$("#addmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#addmsg, #addimgmsg").html("");
	                    if(data.status == 200){
	                    	window.location.href = siteurl+"/Content/manualAdvertisement";
	                    }
	                }, 1000);

	            }            
	        });
			return false;
	    }
	});
	
	$("#editadvertisement").validate({
	    rules:{    	
	    	link:"required",
	    	status:"required",
	    	//display_order:"required"
	    },
	    messages:{    	
	    	link:"Please enter a redirect link",
	    	status:"Please select status",
	    	display_order:"Please select display order"
	    },
	    submitHandler:function(e){    	
	    	var formData = new FormData($("#editadvertisement")[0]);        
	        $("#addsubmit").prop("disabled", true);
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Content/editAdervertisement",
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$("#addsubmit").prop("disabled", false);
	                if(data.type == 2)
	                	$("#addimgmsg").html(data.msg);
	                else
	                	$("#addmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#addmsg, #addimgmsg").html("");
	                    if(data.status == 200){
	                    	window.location.href = siteurl+"/Content/manualAdvertisement";
	                    }
	                }, 1000);
	            }
	        });
			return false;
	    }
	});
	$(".add_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Content/trashAdvertisement",
			data:{"id":id, "status":val},
			type:"POST",
			success: function(data){}
		});
	});
	$(".trash-advertisement").on("click", function(){
		if(confirm("Do you really want to move advertisement in trash?")){
			var row = $(this);
			var id = row.attr("modal-aria");			
			$.ajax({
				url:siteurl+"/Content/trashAdvertisement",
				data:{"id":id, "status":3},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){				
					if(data.status == 200){
						row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
					}
				}
			});
		}		
	});
	$(".trashback-advertisement").on("click", function(){	
		var row = $(this);
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Content/trashAdvertisement",
			data:{"id":id, "status":0},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});
	$(".trashback-publishadvertisement").on("click", function(){	
		var row = $(this);
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Content/trashAdvertisement",
			data:{"id":id, "status":1},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});
// Advertisement scripts end

//Heading content scripts
	$(".updateheadingcontent").on("click", function(){
		var id = $(this).attr("modal-aria");
		$.ajax({
			url:siteurl+"/Translation/getHeadingdetails",
			data:{"id":id},
			type:"POST",
			dataType:"json",			
			success: function(data){
				$("#ctheading").text(data.content.section);
				$("#sectionid").val(data.content.id);
				if(data.status == 200){
					$("#heading").val(data.content.heading);
					$("#subheading").val(data.content.subheading);
				}
			}
		});
	})
	$("#updateconheading").validate({
		rules:{
			heading:"required"
		},
		messages:{
			heading:"Please enter heading"
		},
		submitHandler:function(e){
			$(".transsubmit").prop("disabled", true);   		
	   		$.ajax({
				url:siteurl+"/Translation/updateHeadings",
				data:$("#updateconheading").serialize(),
				type:"POST",
				dataType:"json",			
				success: function(data){
					$(".transsubmit").prop("disabled", false);
					$("#transmsg").html(data.msg);
	                setTimeout(function(){                	
	                    $("#transmsg").html("");
	                    $(".transclose").trigger("click");
	                    $("#updateconheading")[0].reset();
	                }, 1000);
				}
			});
			return false;
		}
	});
// Heading content scripts end

// Update suggestion messages
	$("#UpdateSuggestion").on("submit", function(){
	    $(".loading").show();
	    $("#catsub").prop("disabled", true);
	    $.ajax({
	        url: siteurl+"/LabelSuggestion/updateSuggestion",
	        data:$("#UpdateSuggestion").serialize(),
	        type: "POST",
	        dataType: "json",
	        success:function(data){
	            $(".loading").hide();
	            $("#catsub").prop("disabled", false);
	            $("#catupmsg").html(data.msg);
	            setTimeout(function(){
	                $("#catupmsg").html("");
	            }, 2000);
	        }
	    })
	    return false;
	});
// Update suggestion messages End

//Update content management data
	$("#updatecontent").on("submit", function(){
		$(".loading").show();
	    $("#catsub").prop("disabled", true);
	    $.ajax({
	        url: $("#updatecontent").attr("action"),
	        data:$("#updatecontent").serialize(),
	        type: "POST",
	        dataType: "json",
	        success:function(data){
	            $(".loading").hide();
	            $("#catsub").prop("disabled", false);
	            $("#catupmsg").html(data.msg);
	            setTimeout(function(){
	                $("#catupmsg").html("");
	            }, 2000);
	        }
	    });
		return false;
	});

	$("#updatecontent1").on("submit", function(){
		$(".loading").show();
	    $("#catsub").prop("disabled", true);	    
	    var label1 = $("input[name='label1']").val();
	    var label2 = $("input[name='label2']").val();
	    var label3 = CKEDITOR.instances.label3.getData();
	    var label4 = $("input[name='label4']").val();
	    $.ajax({
	        url: $("#updatecontent1").attr("action"),
	        data:{ label1:label1, label2:label2, label3:label3, label4:label4},
	        type: "POST",
	        dataType: "json",
	        success:function(data){
	            $(".loading").hide();
	            $("#catsub").prop("disabled", false);
	            $("#catupmsg").html(data.msg);
	            setTimeout(function(){
	                $("#catupmsg").html("");
	            }, 2000);
	        }
	    });
		return false;
	})
// Update content management data end

//Create event from admin dashboard
	$("#editEventType").validate({
		rules:{
			name:"required",			
			status:"required"
		},
		messages:{
			name:"Please enter type name",
			status:"Please select type status"
		},
		submitHandler:function(e){
			$("#catsub").prop("disabled", true);
	        $(".loading").show();
	        var id = $("#editid").val();
	        $.ajax({
	            type:'POST',
	            url: $("#editEventType").attr("action"),
	            data:$("#editEventType").serialize(),
	            dataType:"json",
	            success:function(data){
	            	$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                $("#catupmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#catupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/Admin/eventTypeList";
	                }, 1000);
	            }            
	        });
			return false;
		}
	});
	$("#createnewevent").validate({
		rules:{
			name:"required",
			eventdate:"required",
			starttime:"required",
			endtime:"required",
			evtdescription:"required"
		},
		messages:{
			name:"Please enter event name",
			eventdate:"Please select event date",
			starttime:"Please select event start time",
			endtime:"Please select event end time",
			evtdescription:"Please enter event description"
		},
		submitHandler:function(e){
			$(".evtsubmit").prop("disabled", true);
			$.ajax({
		        url: $("#createnewevent").attr("action"),
		        data:$("#createnewevent").serialize(),
		        type: "POST",
		        dataType: "json",
		        success:function(data){
		            $(".evtsubmit").prop("disabled", false);
		            $("#evntsmsg").html(data.msg);
		            setTimeout(function(){
		                $("#evntsmsg").html("");
		                $(".evtclose").trigger("click");
		            }, 2000);
		        }
		    });
			return false;
		}
	});
//Create event from admin dashboardend

// date and time picker code
    var todaydate = new Date();
    $('.datepicker').datetimepicker({
        format:"yyyy-mm-dd",
        endDate: todaydate,
        weekStart: 1,    
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $('.datepickerpast').datetimepicker({
        format:"yyyy-mm-dd",
        endDate: todaydate,
        weekStart: 1,    
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

     $('.onlydatepicker').datetimepicker({
        format:"yyyy-mm-dd",
        weekStart: 1,    
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });
    $('.datepickerfuture').datetimepicker({
        format:"yyyy-mm-dd",
        startDate: todaydate,
        weekStart: 1,    
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 2,
        forceParse: 0
    });

    $('.timepicker').datetimepicker({
        format:"HH:ii P",
        autoclose: 1,
        startView: 1,
        minView: 0,
        maxView: 1,
        forceParse: 0,
        showMeridian:true
    });
//date and time picker code end

$('.opt_status').on("change", function() {
	var row = $(this);
	var val = 0;
	if(row.prop("checked")){
		val = 1
	}
	else{
		val = 0;
	}
	var id = row.attr("modal-aria");


	$.ajax({
		url: siteurl+"/Admin/activateoption/",
		type: 'POST',
		data: {
		    option: id,
		    action: val
		},
		beforeSend: function(){
		  $('.loader').css("visibility", "visible");
		},
		complete: function(){
		      $('.loader').css("visibility", "hidden");
		},
		//dataType: 'json',
		success: function(data) {
			// alert(data);
		},
		error: function(data) {
			alert("Something went wrong, Please try after sometime.");
		}
	});
  
});
$('.dltopt').on("click", function() {	
	var btn = $(this);
	var id = btn.attr("modal-aria");
	$.ajax({
		url: siteurl+"/Admin/removeOption/",
		type: 'POST',
		data: {
		    option: id
		},
		beforeSend: function(){
		  btn.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
		},
		complete: function(){
		  btn.find("i").removeClass("fa-spinner fa-pulse").addClass("fa-trash");
		},
		dataType: 'json',
		success: function(data) {
			if(data.status == 200)
				btn.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow", function() { $(this).remove(); });
			else
				alert("Something went wrong, Please try after sometime.");
		},
		error: function(data) {
			alert("Something went wrong, Please try after sometime.");
		}
	});  
});

$(".delete-review").on("click", function(){
	var btn = $(this);
	var id = btn.attr("modal-aria");
	$.ajax({
		url: siteurl+"/Admin/delreview/",
		type: 'POST',
		data: {
		    review: id
		},
		beforeSend: function(){
		  btn.find("i").removeClass("fa-trash").addClass("fa-spinner fa-pulse");
		},
		complete: function(){
		  btn.find("i").removeClass("fa-spinner fa-pulse").addClass("fa-trash");
		},
		dataType: 'json',
		success: function(data) {
			if(data.status == 200)
				btn.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow", function() { $(this).remove(); });
			else
				alert("Something went wrong, Please try after sometime.");
		},
		error: function(data) {
			alert("Something went wrong, Please try after sometime.");
		}
	});
});

	$(".vot_status").on("change", function(){
		var row = $(this);
		var val = 0;
		if(row.prop("checked")){
			val = 1
		}
		else{
			val = 0;
		}
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Admin/updateReviewStatus",
			data:{"id":id, "mode":val},
			type:"POST",
			dataType: "json",
			success: function(data){
				$("#rwmsg").html(data.msg);
				setTimeout(function(){
					$("#rwmsg").html("");						
				}, 1500);
			}
		});
	});


	// $("#saveTemplate").validate({
	// 	rules:{
	// 		template:{
	// 			required:function(){
	// 				if(CKEDITOR.instances.template.getData() == ''){
	// 					return true;
	// 				}
	// 			}
	// 		}
	// 	},
	// 	messages:{
	// 		template: {
	// 			required: "Please enter value"
	// 		}
	// 	},
	// 	submitHandler:function(d){
	// 		alert(CKEDITOR.instances.template.getData())
	// 		return false;
	// 	},
	// 	errorPlacement: function(error, element) {
	//     	error.appendTo('#ermsg');
	//    	}
	// })
	$("#saveTemplate").on("submit", function(){
		if(CKEDITOR.instances.template.getData() == ''){
			$("#ermsg").html("<label class='text-danger'>Please enter email description</label>");
			return false;
		}else if($("input[name='subject']", "#saveTemplate").val() == ""){
			$("#ersbmsg").html("<label class='text-danger'>Please enter email subject.</label>");
			$("input[name='subject']", "#saveTemplate").focus();
			return false;
		}
		else{
			$(".loading").show();
		    $("#catsub").prop("disabled", true);
		    $.ajax({
		        url: $("#saveTemplate").attr("action"),
		        data:{"template":CKEDITOR.instances.template.getData(), "subject":$("input[name='subject']", "#saveTemplate").val(), "variables":$("textarea[name='variables']", "#saveTemplate").val()},
		        type: "POST",
		        dataType: "json",
		        success:function(data){
		            $(".loading").hide();
		            $("#catsub").prop("disabled", false);
		            $("#catupmsg").html(data.msg);
		            setTimeout(function(){
		                $("#catupmsg").html("");
		            }, 2000);
		            if(data.status == 200){
		            	setTimeout(function(){
			                window.location.href = siteurl+"/ETemplate";
			            }, 1000);
		            }
		        }
		    });
		}
		return false;
	})



	$("#deletebusinessprofile").on("submit", function(){
		if(confirm("This will delete all related records that can not be revoke. Are you sure?")){
			return true;
		}else{
			return false;
		}
	})

	// $(".dluser").on("click", function(){		
	// 	var row = $(this);
	// 	var id = row.attr("modal-aria");
	// 	$.ajax({
	// 		url:siteurl+"/Admin/deleteproUser",
	// 		data:{"id":id},
	// 		type:"POST",
	// 		dataType:"json",
	// 		async: false,
	// 		success: function(data){
	// 			if(data.status == 200){
	// 				row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
	// 			}else{
	// 				alert(data.msg);
	// 			}
	// 		}
	// 	});
	// });

	$(".dleventtype").on("click", function(){		
		var row = $(this);
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Admin/deleteEventType",
			data:{"id":id},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}else{
					alert(data.msg);
				}
			}
		});
	});

	$(".trashevent").on("click", function(){
		var row = $(this);
		var id = row.attr("modal-aria");
		$.ajax({
			url:siteurl+"/Admin/RemoveEvent",
			data:{"id":id},
			type:"POST",
			dataType:"json",
			async: false,
			success: function(data){				
				if(data.status == 200){
					row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}
			}
		});
	});

//Packages Create and edition
	$("#linkoption").on("change", function(){
		btn = $(this);
		if(btn.val() != ""){
			$.ajax({
				url:siteurl+"/Admin/displayOptionPriceListingViaAjax",
				data:{"id":btn.val()},
				type:"POST",
				dataType:"json",
				async: false,
				success: function(data){				
					if(data.status == 200){
						$("#priceoption").html(data.content);
					}else{
						$("#priceoption").html("");
					}
				}
			});
		}else{
			$("#priceoption").html("");
		}
	});

	$("#linkuseroption").validate({
		rules:{
			linkoption:"required"
		},
		messages:{
			linkoption:"Please select option"
		}
	})

	// $(".optionprices").on("change", function(){        
 //        if($(this).attr("atr-val") == "single"){
 //            var detailsfor = $(this).val();
 //            if($("#linkoption").val() == 2 ){
 //                var content = '<strong>Choose Dates:</strong><div class="form-group"><input readonly type="text" name="perdaysdate" class="form-control dynamicdatepicker" placeholder="Start date" /><label class="labelstdate text-danger" style="display:none">Please select start date.</label></div><div class="form-group"><input readonly type="text" name="perdayedate" class="form-control dynamicdatepicker" placeholder="End date" /><label class="labeleddate text-danger" style="display:none">Please select end date.</label></div><strong>Choose Visible City:</strong><div class="form-group"><input type="text" name="city" class="form-control citycomplete" onfocus="geolocate()" placeholder="Enter visible city" /><label class="labeledcity text-danger" style="display:none">Please select visible city.</label></div>';
 //            }else{
 //                var content = '<strong>Choose Dates:</strong><div class="form-group"><input readonly type="text" name="perdaysdate" class="form-control dynamicdatepicker" placeholder="Start date" /><label class="labelstdate text-danger" style="display:none">Please select start date.</label></div><div class="form-group"><input readonly type="text" name="perdayedate" class="form-control dynamicdatepicker" placeholder="End date" /><label class="labeleddate text-danger" style="display:none">Please select end date.</label></div>';
 //            }
 //            form.find(".singleoption").html(content);
 //            var address = document.getElementsByClassName('citycomplete');
 //            for(var i=0; i< address.length; i++){
 //                new google.maps.places.Autocomplete(address[i], {types: ['geocode'],  componentRestrictions: {country: "fr"}});
 //            }
 //            $('.dynamicdatepicker').datetimepicker({
 //                language:"fr",
 //                format:"yyyy-mm-dd",
 //                startDate: nextdate,
 //                weekStart: 1,    
 //                autoclose: 1,                
 //                startView: 2,
 //                minView: 2,
 //                forceParse: 0
 //            });
 //        }else{
 //            form.find(".singleoption, .totalprice").html("");
 //        }
 //    });

	$(".opt_mstcheck").on("change", function(){
		btn = $(this);
		if(btn.prop("checked")){
			btn.siblings(".putpriceselection").show();
		}else{
			btn.siblings(".putpriceselection").hide();
		}
	});

//Packages Create and edition end

//event deletion 
$(document).on("click",".del_evt_img",function(){
	
	 
            var btn = $(this);
	        var event_id = $(".event_id").val();
	        var event_col = $(this).attr("data-column");
			
			var conf  = confirm("Are you sure to delete this image ? ");
			if(conf==true){
	        $.ajax({
	            type:'POST',
	            url: siteurl+"/Admin/delete_event_img/",
	            data:({event_col:event_col,event_id:event_id}),
	            dataType:"json",
	            success:function(data){
				 
					 btn.parents("li").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow", function() { $(this).remove(); });
					 
	            	/*$(".loading").hide();                
	                $("#catsub").prop("disabled", false);
	                $("#catupmsg").html(data.msg);
	                setTimeout(function(){
	                    $("#catupmsg").html("");
	                    if(data.status == 200)
	                		window.location.href = siteurl+"/Admin/eventTypeList";
	                }, 1000);*/
	            }            
	        });	
		}	
	
});

$(document).on("submit","#updatePurchaseReceipt",function(e){
	e.preventDefault();
	var receipt = CKEDITOR.instances.receipt.getData();
		$.ajax({
			type:'POST',
			url: siteurl+"/Content/update_receipt/",
			data:({receipt:receipt}),
			dataType:"json",
			success:function(data){
			     if(data.status==200){
					 
					 $("#successmsg").html(data.msg);
				 }
 
				 
 
			}            
		});	

	
});

$(document).on("submit","#saveLanguage",function(e){
	     e.preventDefault();
	     current = $("#saveLanguage");
		 $.ajax({
			type:'POST',
			url: siteurl+"/Languages/update_language/",
			data:current.serialize(),
			dataType:"json",
			beforeSend: function(){
                   $(".loading").show();
                   $("#catsub").hide();
			},
			success:function(data){

				 $(".loading").hide();
				 $("#catsub").show();
			     if(data.status==200){
					 
					   $("#catupmsg").html('<div class="alert alert-sucsess" id="success-alert"><button type="button" class="close" data-dismiss="alert">x</button><strong></strong>'+data.msg+'</div>');
						setTimeout(function(){
								window.location.href = siteurl+"/Languages/";
						}, 1000);					   
					   
					 
				 }else{
					     $("#catupmsg").html('<div class="alert alert-warning" id="success-alert"><button type="button" class="close" data-dismiss="alert">x</button><strong></strong>'+data.msg+'</div>');
					 
				 }
 
				 
 
			}            
		});	
	});	
		
	$(".delete_language").on("click",function(){
		var row = $(this);
		 var id = row.attr("modal-aria");
		 $.ajax({
			type:'POST',
			url: siteurl+"/Languages/delete_language/",
			data:({id:id}),
			dataType:"json",
			success:function(data){
				 if(data.status == 200){
				   row.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				 }
			}
		 
		 
		 
	    });


	});

	$("form[name='upload_csv']").validate({
		rules:{
			userfile:"required"
		},
		messages:{
			userfile:"Please select a file to import"
		},
		submitHandler:function(d){
			$("#submit").prop("disabled", true);
			$("form[name='upload_csv']").submit();
		}
	});



//slimpay code
	$(".mandatestate").on("click", function(){
		var btn = $(this);
		var mand = btn.attr("refer");
		btn.prop("disabled", true);
		$.ajax({
			type:'POST',
			url: siteurl+"/Slimpay/checkMandateState/",
			data:{"reference":mand},
			dataType:"json",
			success:function(data){
				btn.prop("disabled", false);
				if(data.status == 200){
					btn.siblings(".displaymandstate").text(data.state);
				}else{
					alert(data.state);
				}
			}
		});
	});

	$("#uploaduserpurchasedoc").validate({
		rules:{
			options:"required",
			manddoc:"required"
		},
		messages:{
			options:"Please select an option",
			manddoc:"Please select a pdf file"
		},
		submitHandler:function(data){
			$(".upload").prop("disabled", true);
			$(".updpsloading").show();
			var formData = new FormData($("#uploaduserpurchasedoc")[0]);        	        
	        $.ajax({
	            type:'POST',
	            url: $("#uploaduserpurchasedoc").attr("action"),
	            data:formData,
	            cache:false,
	            contentType: false,
	            processData: false,
	            dataType:"json",
	            success:function(data){
	            	$(".upload").prop("disabled", false);
	            	$(".updpsloading").hide();
	            	$(".userdetailsermsg").html(data.msg);
	            	if(data.status == 200){	            		
	            		setTimeout(function(){
	            			$(".userdetailsermsg").html("");
	            			location.reload();
	            		}, 1000);
	            	}else{
	            		setTimeout(function(){
	            			$(".userdetailsermsg").html("");	            			
	            		}, 2000);
	            	}
	            	
	            }            
	        });			
		}
	});

	$(".deletedoc").on("click", function(){
		var btn = $(this);
		var id = btn.attr("refers");
		btn.find("i").removeClass("fa-trash-o").addClass("fa-spinner fa-spin");
		$.ajax({
			type:'POST',
			url: siteurl+"/Admin/removeAdminUploadDoc/",
			data:{"id":id},
			dataType:"json",
			success:function(data){				
				if(data.status == 200){
					btn.parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
				}else{
					btn.removeClass("fa-spinner fa-spin").addClass("fa-trash-o");
					alert(data.msg);
				}
			}
		});
	});
// slimpay code end

// Claimed Profile
	$("#deleteclaimeprofiles").on("click", function(){
		if(confirm("Are you sure you want to delete this?")){
	        var btn = $(this);
	        btn.prop("disabled", true);
	        if($(".checkall").prop("checked")){         
	            $.ajax({
	                url: siteurl+"/Admin/deleteallClaimedprofile",
	                dataType: 'json',
	                success:function(data){
	                    btn.prop("disabled", false);
	                    if(data.status == 200){
	                        $("#displayrecords tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow").remove();
	                        var content = '<tr><td colspan="7"><span>No records available.</span></td></tr>';
                            setTimeout(function(){
                                $(content).appendTo("#displayrecords");
                            }, 1000);
	                    }else{
	                    	alert(data.msg);
	                    }
	                }
	            });                     
	        }else{
	            var array = [];
	            $("input[name='assign_tupple[]']:checked").each(function(index, value) {
	               array.push($(this).val());
	            });         
	            $.ajax({
	                url: siteurl+"/Admin/deleteClaimedprofile",
	                type: "POST",
	                data: {"ids": array},
	                dataType: 'json',
	                success:function(data){
	                    btn.prop("disabled", false);                    
	                    if(data.status == 200){
	                        btn.prop("disabled", false);
	                        $("#displayrecords .selected").parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
	                    }
	                }
	            });         
	        }
	    }
	});
	$(".checkall").on("change", function(){ 
        $("input[name='assign_tupple[]']").not(this).prop('checked', this.checked);
    });
    $("input[name='assign_tupple[]'], .checkall").on("change", function(){
		if($("input[name='assign_tupple[]']:checked").length > 0)
			$("#deleteclaimeprofiles, #deletenewsletter").prop("disabled", false);
		else
			$("#deleteclaimeprofiles, #deletenewsletter").prop("disabled", true);
	});
    $("input[name='assign_tupple[]']").on("change", function(){    
        if($(this).prop('checked')){
            $(this).addClass("selected");
        }else{
            $(this).removeClass("selected");
        }
    });
// Claimed Profile end

// Newsletter Subscriber
	$("#deletenewsletter").on("click", function(){
		if(confirm("Are you sure you want to delete this?")){
	        var btn = $(this);
	        btn.prop("disabled", true);
	        if($(".checkall").prop("checked")){         
	            $.ajax({
	                url: siteurl+"/Admin/deleteAllSubscriber",
	                dataType: 'json',
	                success:function(data){
	                    btn.prop("disabled", false);
	                    if(data.status == 200){
	                        $("#displayrecords tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow").remove();
	                        var content = '<tr><td colspan="7"><span>No records available.</span></td></tr>';
                            setTimeout(function(){
                                $(content).appendTo("#displayrecords");
                            }, 1000);
	                    }else{
	                    	alert(data.msg);
	                    }
	                }
	            });                     
	        }else{
	            var array = [];
	            $("input[name='assign_tupple[]']:checked").each(function(index, value) {
	               array.push($(this).val());
	            });         
	            $.ajax({
	                url: siteurl+"/Admin/deleteSubscriber",
	                type: "POST",
	                data: {"ids": array},
	                dataType: 'json',
	                success:function(data){
	                    btn.prop("disabled", false);                    
	                    if(data.status == 200){
	                        btn.prop("disabled", false);
	                        $("#displayrecords .selected").parents("tr").animate({ backgroundColor: "#003" }, "1000").animate({ opacity: "hide" }, "slow");
	                    }
	                }
	            });         
	        }
	    }
	});
// Newsletter Subscriber end