-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 07, 2018 at 09:24 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `businessdirectoryclassified`
--

-- --------------------------------------------------------

--
-- Table structure for table `bdc_users`
--

CREATE TABLE IF NOT EXISTS `bdc_users` (
`user_id` int(11) NOT NULL,
  `user_type` varchar(30) DEFAULT 'Personal',
  `user_firstname` varchar(100) NOT NULL,
  `user_lastname` varchar(100) DEFAULT NULL,
  `user_email` varchar(150) NOT NULL,
  `user_phone` varchar(12) NOT NULL,
  `user_address` varchar(250) DEFAULT NULL,
  `user_zipcode` int(11) NOT NULL,
  `user_language` varchar(50) DEFAULT NULL,
  `user_lat` varchar(255) NOT NULL,
  `user_long` varchar(255) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `social_id` varchar(255) NOT NULL,
  `user_plan` varchar(255) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog_vote`
--

CREATE TABLE IF NOT EXISTS `blog_vote` (
`vote_id` int(10) unsigned NOT NULL,
  `blog_vote` float unsigned NOT NULL,
  `profile_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `blog_vote`
--

INSERT INTO `blog_vote` (`vote_id`, `blog_vote`, `profile_id`, `ip_address`, `user_id`) VALUES
(11, 2, 11, '::1', 2),
(12, 5, 2, '::2', 1),
(23, 4, 54, NULL, 1),
(24, 3, 13, NULL, 1),
(25, 4, 80, NULL, 1),
(26, 5, 11, NULL, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `business_profile`
--
CREATE TABLE IF NOT EXISTS `business_profile` (
`user_id` int(11)
,`user_firstname` varchar(100)
,`user_lastname` varchar(100)
,`user_phone` varchar(12)
,`user_gender` varchar(6)
,`user_dob` date
,`user_address` varchar(250)
,`user_language` varchar(50)
,`user_company_name` varchar(255)
,`user_company_number` varchar(255)
,`user_company_address` varchar(255)
,`user_company_email` varchar(255)
,`user_business_twitter` varchar(255)
,`user_business_phone` varchar(255)
,`user_webste` varchar(255)
,`business_opening` date
,`business_description` text
,`user_lat` varchar(255)
,`user_long` varchar(255)
,`user_password` varchar(50)
,`user_type` varchar(30)
,`status` int(11)
,`user_profile_pic` varchar(255)
,`auth_check` int(11)
,`sms_check` int(11)
,`user_signup_method` varchar(255)
,`categories` text
,`extra_filters` text
,`type` int(11)
,`publishonhome` smallint(6)
,`toprated` smallint(6)
,`total_followers` bigint(21)
,`user_banner_display` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
`id` int(11) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `timestamp` bigint(10) NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `user_id`, `timestamp`, `data`, `status`) VALUES
(1, 77, 1510222713, '', 0),
(2, 31440, 1510142465, '', 0),
(3, 120, 1521014112, '', 0),
(4, 1, 1522831608, '', 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `distinct_allusers`
--
CREATE TABLE IF NOT EXISTS `distinct_allusers` (
`user_id` int(11)
,`user_firstname` varchar(100)
,`user_lastname` varchar(100)
,`user_phone` varchar(12)
,`user_gender` varchar(6)
,`user_dob` date
,`user_address` varchar(250)
,`user_language` varchar(50)
,`user_company_name` varchar(255)
,`user_company_number` varchar(255)
,`user_company_address` varchar(255)
,`user_company_email` varchar(255)
,`user_lat` varchar(255)
,`user_long` varchar(255)
,`user_password` varchar(50)
,`user_type` varchar(30)
,`status` int(11)
,`user_profile_pic` varchar(255)
,`auth_check` int(11)
,`sms_check` int(11)
,`user_signup_method` varchar(255)
,`categories` text
,`extra_filters` text
,`type` int(11)
,`publishonhome` smallint(6)
,`toprated` smallint(6)
,`total_followers` bigint(21)
,`vote_count` double
,`head` varchar(1)
,`total_recommand` bigint(21)
,`total_review` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `otd_advertisement`
--

CREATE TABLE IF NOT EXISTS `otd_advertisement` (
`add_id` int(11) NOT NULL,
  `add_img_path` varchar(255) NOT NULL,
  `add_url` text NOT NULL,
  `add_display` int(11) NOT NULL,
  `add_status` tinyint(4) NOT NULL COMMENT '0=>disable, 1=>Enable, 3=>deleted',
  `add_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_business_category`
--

CREATE TABLE IF NOT EXISTS `otd_business_category` (
`cat_id` bigint(20) unsigned NOT NULL,
  `cat_name` varchar(255) NOT NULL,
  `cat_description` text NOT NULL,
  `cat_parent` bigint(20) unsigned NOT NULL,
  `cat_img_path` varchar(255) NOT NULL DEFAULT 'default.png',
  `cat_homedisplay` tinyint(4) NOT NULL COMMENT '0=>no, 1=>yes',
  `cat_status` tinyint(4) NOT NULL COMMENT '0=>Disable, 1=>Enable',
  `cat_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `cat_by_user` bigint(20) unsigned NOT NULL,
  `type` int(11) NOT NULL DEFAULT '1' COMMENT '1=>Category,2=>Services, 3=>Products, 4=>Others',
  `cat_hometabdisplay` tinyint(4) NOT NULL DEFAULT '0',
  `cat_hometab_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1704 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_business_category`
--

INSERT INTO `otd_business_category` (`cat_id`, `cat_name`, `cat_description`, `cat_parent`, `cat_img_path`, `cat_homedisplay`, `cat_status`, `cat_date_created`, `cat_by_user`, `type`, `cat_hometabdisplay`, `cat_hometab_id`) VALUES
(1, 'Affaires Maritimes (administration)', 'Affaires Maritimes (administration)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 77, 1, 1, 1),
(2, 'Autres', 'Autres', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(3, 'CD, DVD, cassettes, disques (fabrication, distribution)', 'CD, DVD, cassettes, disques (fabrication, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(4, 'Chambres de Commerce et d''Industrie, de Métiers et de l''Artisanat, d''Agriculture', 'Chambres de Commerce et d''Industrie, de Métiers et de l''Artisanat, d''Agriculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 77, 1, 1, 1),
(5, 'HLM (offices et gestion)', 'HLM (offices et gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(6, 'Internet (création de sites, hébergement)', 'Internet (création de sites, hébergement)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(7, 'Internet (fournisseurs d''accès)', 'Internet (fournisseurs d''accès)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(8, 'abat-jour (détail)', 'abat-jour (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(9, 'abat-jour et fournitures (fabrication)', 'abat-jour et fournitures (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(10, 'abattoirs de volailles', 'abattoirs de volailles', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(11, 'abattoirs et ateliers de découpe', 'abattoirs et ateliers de découpe', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(12, 'abrasifs et super-abrasifs (fabrication, gros)', 'abrasifs et super-abrasifs (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(13, 'abris de jardins et garages préfabriqués (vente, installation)', 'abris de jardins et garages préfabriqués (vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 77, 1, 1, 5),
(14, 'accessoires du vêtement (détail)', 'accessoires du vêtement (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(15, 'accumulateurs et piles électriques (fabrication)', 'accumulateurs et piles électriques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(16, 'acier : produits sidérurgiques et transformés (fabrication, négoce)', 'acier : produits sidérurgiques et transformés (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(17, 'acoustique (études, projets, mesures)', 'acoustique (études, projets, mesures)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(18, 'actuaires', 'actuaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(19, 'adhésifs (fabrication, gros)', 'adhésifs (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(20, 'administrateurs de biens et syndics de copropriétés', 'administrateurs de biens et syndics de copropriétés', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(21, 'administrateurs judiciaires', 'administrateurs judiciaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(22, 'administration de l''Agriculture', 'administration de l''Agriculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(23, 'administration de l''Education Nationale', 'administration de l''Education Nationale', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(24, 'administration de l''Environnement', 'administration de l''Environnement', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(25, 'administration de l''Industrie', 'administration de l''Industrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(26, 'administration de l''Intérieur', 'administration de l''Intérieur', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(27, 'administration de la Culture', 'administration de la Culture', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(28, 'administration de la Jeunesse et des Sports', 'administration de la Jeunesse et des Sports', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(29, 'administration des Affaires Etrangères', 'administration des Affaires Etrangères', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(30, 'administrations de l''Economie et des Finances', 'administrations de l''Economie et des Finances', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(31, 'administrations de l''Equipement, des Transports, du Logement et du Tourisme', 'administrations de l''Equipement, des Transports, du Logement et du Tourisme', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(32, 'administrations de la Défense et des Anciens Combattants', 'administrations de la Défense et des Anciens Combattants', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(33, 'administrations de la Santé et des Affaires Sociales', 'administrations de la Santé et des Affaires Sociales', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(34, 'administrations du Commerce et de l''Artisanat', 'administrations du Commerce et de l''Artisanat', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(35, 'administrations du Travail et de l''Emploi', 'administrations du Travail et de l''Emploi', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(36, 'administrations régionales, départementales et locales', 'administrations régionales, départementales et locales', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(37, 'affacturage', 'affacturage', 0, 'default.png', 0, 1, '2017-10-24 06:46:27', 0, 1, 0, 0),
(38, 'affichage (entreprises)', 'affichage (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(39, 'affichage électronique et journaux lumineux (fabrication, vente, installation)', 'affichage électronique et journaux lumineux (fabrication, vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(40, 'affiches (réalisation, impression)', 'affiches (réalisation, impression)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(41, 'affûtage', 'affûtage', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(42, 'agencement d''appartements', 'agencement d''appartements', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(43, 'agencement de bureaux', 'agencement de bureaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(44, 'agencement de cafés, hôtels et restaurants', 'agencement de cafés, hôtels et restaurants', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(45, 'agencement de magasins', 'agencement de magasins', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(46, 'agences de presse', 'agences de presse', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(47, 'agences de voyages', 'agences de voyages', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(48, 'agences et agents commerciaux', 'agences et agents commerciaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(49, 'agences immobilières', 'agences immobilières', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(50, 'agences matrimoniales', 'agences matrimoniales', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(51, 'agences photographiques de presse', 'agences photographiques de presse', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(52, 'agendas, calendriers et répertoires (fabrication, gros)', 'agendas, calendriers et répertoires (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(53, 'agents artistiques, agents littéraires', 'agents artistiques, agents littéraires', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(54, 'agents et cabinets d''affaires', 'agents et cabinets d''affaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(55, 'agrafes et agrafeuses (fabrication)', 'agrafes et agrafeuses (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(56, 'agriculteurs', 'agriculteurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(57, 'agriculture (recherches, études)', 'agriculture (recherches, études)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(58, 'agriculture : approvisionnement et collecte (négoce)', 'agriculture : approvisionnement et collecte (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(59, 'aimants', 'aimants', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(60, 'alarmes et antivols pour autos (vente, pose)', 'alarmes et antivols pour autos (vente, pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(61, 'alarmes et surveillance : systèmes (vente, installation)', 'alarmes et surveillance : systèmes (vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(62, 'alcools et eaux de vie (fabrication, gros)', 'alcools et eaux de vie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(63, 'alimentation : additifs, arômes, levures (fabrication, gros)', 'alimentation : additifs, arômes, levures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(64, 'alimentation : fournitures pour commerce', 'alimentation : fournitures pour commerce', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(65, 'alimentation : machines et matériel pour l''industrie', 'alimentation : machines et matériel pour l''industrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(66, 'alimentation : machines et matériel pour le commerce', 'alimentation : machines et matériel pour le commerce', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(67, 'alimentation animale (fabrication, gros)', 'alimentation animale (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(68, 'alimentation générale (détail)', 'alimentation générale (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(69, 'alimentation générale (gros)', 'alimentation générale (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(70, 'allocations familiales', 'allocations familiales', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(71, 'aluminium : articles en', 'aluminium : articles en', 0, 'default.png', 0, 1, '2017-10-24 06:46:28', 0, 1, 0, 0),
(72, 'aluminium et alliages (production, transformation, négoce)', 'aluminium et alliages (production, transformation, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(73, 'ambassades, consulats et autres représentations diplomatiques', 'ambassades, consulats et autres représentations diplomatiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(74, 'ambulances', 'ambulances', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(75, 'amincissement (centres d'')', 'amincissement (centres d'')', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(76, 'aménagement de combles, de caves', 'aménagement de combles, de caves', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(77, 'aménagement de locaux industriels et professionnels', 'aménagement de locaux industriels et professionnels', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(78, 'animaleries', 'animaleries', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(79, 'animaleries : matériel et fournitures (fabrication, gros)', 'animaleries : matériel et fournitures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(80, 'animation artistique', 'animation artistique', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(81, 'animation commerciale', 'animation commerciale', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(82, 'animaux (dressage)', 'animaux (dressage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(83, 'animaux (refuges, fourrières)', 'animaux (refuges, fourrières)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(84, 'animaux (services pour)', 'animaux (services pour)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(85, 'annuaires (édition)', 'annuaires (édition)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(86, 'antennes pour télévision (vente, installation, réparation)', 'antennes pour télévision (vente, installation, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(87, 'antiquités (achat et vente)', 'antiquités (achat et vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(88, 'antiquités et objets d''art (restauration)', 'antiquités et objets d''art (restauration)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(89, 'apiculture', 'apiculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(90, 'apprentissage, formation professionnelle', 'apprentissage, formation professionnelle', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(91, 'aquaculture', 'aquaculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(92, 'aquariums et viviers (études et réalisations)', 'aquariums et viviers (études et réalisations)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(93, 'arboriculture et production de fruits', 'arboriculture et production de fruits', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(94, 'architecte (profession d'')', 'architecte (profession d'')', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(95, 'architectes d''intérieur', 'architectes d''intérieur', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(96, 'architectes navals', 'architectes navals', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(97, 'archives (stockage, gestion, destruction)', 'archives (stockage, gestion, destruction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(98, 'archives et documentation (centres)', 'archives et documentation (centres)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(99, 'ardoises (production, vente)', 'ardoises (production, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(100, 'armagnacs', 'armagnacs', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(101, 'armateurs', 'armateurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(102, 'armatures pour béton', 'armatures pour béton', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(103, 'armes et munitions (fabrication, gros)', 'armes et munitions (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(104, 'armureries', 'armureries', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(105, 'arrosage (appareils et installation)', 'arrosage (appareils et installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(106, 'articles de ménage et de cuisine (fabrication, gros)', 'articles de ménage et de cuisine (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(107, 'articles de ménage et de cuisine, bazars (détail)', 'articles de ménage et de cuisine, bazars (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(108, 'articles et librairies érotiques', 'articles et librairies érotiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(109, 'articles et ornements religieux', 'articles et ornements religieux', 0, 'default.png', 0, 1, '2017-10-24 06:46:29', 0, 1, 0, 0),
(110, 'articles funéraires (détail)', 'articles funéraires (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(111, 'articles pour fumeurs', 'articles pour fumeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(112, 'artisanat d''art', 'artisanat d''art', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(113, 'artistes du spectacle', 'artistes du spectacle', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(114, 'artistes peintres', 'artistes peintres', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(115, 'arts de la table : cristallerie, vaisselle, orfèvrerie (fabrication, gros)', 'arts de la table : cristallerie, vaisselle, orfèvrerie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(116, 'arts graphiques, arts plastiques (cours)', 'arts graphiques, arts plastiques (cours)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(117, 'arts graphiques, arts plastiques : matériel et fournitures (détail)', 'arts graphiques, arts plastiques : matériel et fournitures (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(118, 'arts graphiques, arts plastiques : produits et fournitures (gros)', 'arts graphiques, arts plastiques : produits et fournitures (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(119, 'arts martiaux divers (salles et leçons)', 'arts martiaux divers (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(120, 'ascenseurs et monte-charge (installation, réparation)', 'ascenseurs et monte-charge (installation, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(121, 'asphalte, goudron et bitume (travaux)', 'asphalte, goudron et bitume (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(122, 'aspirateurs industriels', 'aspirateurs industriels', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(123, 'aspiration centralisée : équipements', 'aspiration centralisée : équipements', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(124, 'assainissement (travaux)', 'assainissement (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(125, 'assainissement : matériel et produits', 'assainissement : matériel et produits', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(126, 'assistance aux automobilistes et touristes', 'assistance aux automobilistes et touristes', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(127, 'assistance sociale', 'assistance sociale', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(128, 'assistantes maternelles', 'assistantes maternelles', 0, 'default.png', 0, 1, '2017-10-24 06:46:30', 0, 1, 0, 0),
(129, 'associations de défense de l''environnement', 'associations de défense de l''environnement', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(130, 'associations de gestion et de comptabilité', 'associations de gestion et de comptabilité', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(131, 'associations diverses, amicales', 'associations diverses, amicales', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(132, 'associations humanitaires, d''entraide, d''action sociale', 'associations humanitaires, d''entraide, d''action sociale', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(133, 'associations religieuses ou philosophiques', 'associations religieuses ou philosophiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(134, 'associations, organismes culturels et socio-éducatifs', 'associations, organismes culturels et socio-éducatifs', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(135, 'associations, organismes de consommateurs et d''usagers', 'associations, organismes de consommateurs et d''usagers', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(136, 'assurances (agents généraux d'')', 'assurances (agents généraux d'')', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(137, 'assurances (courtiers d'')', 'assurances (courtiers d'')', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(138, 'assurances (maritimes, fluviales et de transports)', 'assurances (maritimes, fluviales et de transports)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(139, 'assurances (sociétés, compagnies)', 'assurances (sociétés, compagnies)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(140, 'astrologie, numérologie', 'astrologie, numérologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(141, 'audioprothésistes, correction de la surdité', 'audioprothésistes, correction de la surdité', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(142, 'audiovisuel (production, édition, réalisation)', 'audiovisuel (production, édition, réalisation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(143, 'audiovisuel (réalisateurs conseils)', 'audiovisuel (réalisateurs conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(144, 'audiovisuel : matériel et accessoires (détail)', 'audiovisuel : matériel et accessoires (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(145, 'audiovisuel : méthodes, techniques', 'audiovisuel : méthodes, techniques', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(146, 'audiovisuel, spectacle (techniciens)', 'audiovisuel, spectacle (techniciens)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(147, 'auteurs, compositeurs', 'auteurs, compositeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(148, 'auto-écoles', 'auto-écoles', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(149, 'autocars (transports touristiques)', 'autocars (transports touristiques)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(150, 'automates, boîtes à musique', 'automates, boîtes à musique', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(151, 'automatisation de processus industriels et de bâtiment (études, installation)', 'automatisation de processus industriels et de bâtiment (études, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(152, 'automobiles (agents, concessionnaires, distributeurs)', 'automobiles (agents, concessionnaires, distributeurs)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(153, 'automobiles (construction, importation)', 'automobiles (construction, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(154, 'automobiles (crédit, leasing, location longue durée)', 'automobiles (crédit, leasing, location longue durée)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(155, 'automobiles d''occasion', 'automobiles d''occasion', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(156, 'automobiles, véhicules industriels (adaptations, aménagements spéciaux)', 'automobiles, véhicules industriels (adaptations, aménagements spéciaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(157, 'automobiles, véhicules industriels : pièces et accessoires (commerce)', 'automobiles, véhicules industriels : pièces et accessoires (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(158, 'automobiles, véhicules industriels : équipement et pièces (fabrication)', 'automobiles, véhicules industriels : équipement et pièces (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(159, 'autoradios (vente, pose)', 'autoradios (vente, pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(160, 'autoroutes (exploitation, gendarmerie et police)', 'autoroutes (exploitation, gendarmerie et police)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(161, 'avions et hélicoptères (location, vente)', 'avions et hélicoptères (location, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(162, 'avions taxis', 'avions taxis', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(163, 'avitaillement', 'avitaillement', 0, 'default.png', 0, 1, '2017-10-24 06:46:31', 0, 1, 0, 0),
(164, 'avocats', 'avocats', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(165, 'avocats au conseil d''état et à la cour de cassation', 'avocats au conseil d''état et à la cour de cassation', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(166, 'avocats communautaires exerçant sous leur titre d''origine', 'avocats communautaires exerçant sous leur titre d''origine', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(167, 'avocats inscrits à un barreau étranger', 'avocats inscrits à un barreau étranger', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(168, 'avocats postulant à la cour', 'avocats postulant à la cour', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(169, 'avocats spécialistes en droit commercial', 'avocats spécialistes en droit commercial', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(170, 'avocats spécialistes en droit communautaire', 'avocats spécialistes en droit communautaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(171, 'avocats spécialistes en droit de l''environnement', 'avocats spécialistes en droit de l''environnement', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(172, 'avocats spécialistes en droit de la propriété intellectuelle', 'avocats spécialistes en droit de la propriété intellectuelle', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(173, 'avocats spécialistes en droit des mesures d''exécution', 'avocats spécialistes en droit des mesures d''exécution', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(174, 'avocats spécialistes en droit des personnes', 'avocats spécialistes en droit des personnes', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(175, 'avocats spécialistes en droit des relations internationales', 'avocats spécialistes en droit des relations internationales', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(176, 'avocats spécialistes en droit des sociétés', 'avocats spécialistes en droit des sociétés', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(177, 'avocats spécialistes en droit fiscal', 'avocats spécialistes en droit fiscal', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(178, 'avocats spécialistes en droit immobilier', 'avocats spécialistes en droit immobilier', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(179, 'avocats spécialistes en droit public', 'avocats spécialistes en droit public', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(180, 'avocats spécialistes en droit pénal', 'avocats spécialistes en droit pénal', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(181, 'avocats spécialistes en droit rural', 'avocats spécialistes en droit rural', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(182, 'avocats spécialistes en droit social', 'avocats spécialistes en droit social', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(183, 'avocats spécialistes en droit économique', 'avocats spécialistes en droit économique', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(184, 'avoués près la cour d''appel', 'avoués près la cour d''appel', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(185, 'aéroclubs et écoles de pilotage', 'aéroclubs et écoles de pilotage', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(186, 'aéronautique et espace (industries)', 'aéronautique et espace (industries)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(187, 'aéroports et services aéroportuaires', 'aéroports et services aéroportuaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(188, 'badges et cartes d''identification (fabrication, personnalisation)', 'badges et cartes d''identification (fabrication, personnalisation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(189, 'bains et douches publics', 'bains et douches publics', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(190, 'balances et bascules (fabrication)', 'balances et bascules (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(191, 'balances et bascules (vente, réparation, location)', 'balances et bascules (vente, réparation, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(192, 'banques', 'banques', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(193, 'banques de données (serveurs et éditeurs)', 'banques de données (serveurs et éditeurs)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(194, 'bardage : éléments de', 'bardage : éléments de', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(195, 'bars, bars à thèmes', 'bars, bars à thèmes', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(196, 'bateaux (garages, hivernage)', 'bateaux (garages, hivernage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(197, 'bateaux (écoles)', 'bateaux (écoles)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(198, 'bateaux de plaisance, accessoires (vente, réparation)', 'bateaux de plaisance, accessoires (vente, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(199, 'bateaux de plaisance, planches à voile et accessoires (fabrication)', 'bateaux de plaisance, planches à voile et accessoires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(200, 'bennes et remorques (fabrication, vente)', 'bennes et remorques (fabrication, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(201, 'bestiaux (commerce)', 'bestiaux (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(202, 'bibliothèques et rayonnages (fabrication)', 'bibliothèques et rayonnages (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:32', 0, 1, 0, 0),
(203, 'bibliothèques, médiathèques', 'bibliothèques, médiathèques', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(204, 'bijouterie fantaisie (détail)', 'bijouterie fantaisie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(205, 'bijouterie fantaisie (fabrication, gros)', 'bijouterie fantaisie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(206, 'bijouterie, joaillerie (détail)', 'bijouterie, joaillerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(207, 'bijouterie, joaillerie (réparation, transformation)', 'bijouterie, joaillerie (réparation, transformation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(208, 'bijouterie, joaillerie : matériel et fournitures', 'bijouterie, joaillerie : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(209, 'bijoutiers (fabrication, gros)', 'bijoutiers (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(210, 'bijoux anciens et d''occasion (achat et vente)', 'bijoux anciens et d''occasion (achat et vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(211, 'billards (académies)', 'billards (académies)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(212, 'billards et accessoires', 'billards et accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(213, 'bimbeloterie (gros)', 'bimbeloterie (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(214, 'biscuiteries et biscotteries', 'biscuiteries et biscotteries', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(215, 'bières (fabrication)', 'bières (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(216, 'bières, boissons (gros)', 'bières, boissons (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(217, 'blanchisseries pour collectivités', 'blanchisseries pour collectivités', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(218, 'blanchisseries pour particuliers', 'blanchisseries pour particuliers', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(219, 'blanchisseries, laveries, pressings : matériel et fournitures', 'blanchisseries, laveries, pressings : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(220, 'bobinage, réparation de matériel électrique professionnel', 'bobinage, réparation de matériel électrique professionnel', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(221, 'bois (courtiers)', 'bois (courtiers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(222, 'bois (détail)', 'bois (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(223, 'bois (importation, exportation)', 'bois (importation, exportation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(224, 'bois (négoce)', 'bois (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(225, 'bois d''ébénisterie', 'bois d''ébénisterie', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(226, 'bois de chauffage', 'bois de chauffage', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(227, 'bois de construction et d''industrie', 'bois de construction et d''industrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(228, 'bois de placage', 'bois de placage', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(229, 'bois en grumes', 'bois en grumes', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(230, 'boisseliers', 'boisseliers', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(231, 'boissons non alcoolisées (fabrication)', 'boissons non alcoolisées (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(232, 'bonneterie (fabrication, gros)', 'bonneterie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(233, 'bottiers', 'bottiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(234, 'boucheries chevalines', 'boucheries chevalines', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(235, 'boucheries, boucheries-charcuteries (détail)', 'boucheries, boucheries-charcuteries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(236, 'boucheries, charcuteries et abattoirs : matériel et équipements', 'boucheries, charcuteries et abattoirs : matériel et équipements', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(237, 'bouchons et capsules (fabrication)', 'bouchons et capsules (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(238, 'bougies et cierges (fabrication, détail)', 'bougies et cierges (fabrication, détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(239, 'boulangerie industrielle : pains, viennoiseries (détail)', 'boulangerie industrielle : pains, viennoiseries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(240, 'boulangeries et pâtisseries : fournitures pour', 'boulangeries et pâtisseries : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(241, 'boulangeries et pâtisseries : matériel et équipements', 'boulangeries et pâtisseries : matériel et équipements', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(242, 'boulangeries, pâtisseries, viennoiseries industrielles (fabrication, gros)', 'boulangeries, pâtisseries, viennoiseries industrielles (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(243, 'boulangeries-pâtisseries', 'boulangeries-pâtisseries', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(244, 'boule, pétanque (clubs)', 'boule, pétanque (clubs)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(245, 'boulonnerie, visserie, clouterie', 'boulonnerie, visserie, clouterie', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(246, 'bouquinistes', 'bouquinistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(247, 'bourse : sociétés et organismes', 'bourse : sociétés et organismes', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(248, 'boutiques exotiques, articles d''Orient', 'boutiques exotiques, articles d''Orient', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(249, 'boutons (fabrication)', 'boutons (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(250, 'bowling', 'bowling', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(251, 'boxe (salles et leçons)', 'boxe (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(252, 'boyaudiers', 'boyaudiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(253, 'bricolage, outillage (détail)', 'bricolage, outillage (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(254, 'briqueteries et tuileries', 'briqueteries et tuileries', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(255, 'brocante', 'brocante', 0, 'default.png', 0, 1, '2017-10-24 06:46:33', 0, 1, 0, 0),
(256, 'broderie, marquage à façon', 'broderie, marquage à façon', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(257, 'broderies, dentelles et tulles (fabrication)', 'broderies, dentelles et tulles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(258, 'bronzage UVA', 'bronzage UVA', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(259, 'brosseries', 'brosseries', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(260, 'broyage, criblage, pulvérisation', 'broyage, criblage, pulvérisation', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(261, 'broyeurs concasseurs (fabrication)', 'broyeurs concasseurs (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(262, 'brûleurs (vente, installation)', 'brûleurs (vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(263, 'bureautique : matériel (fabrication, gros)', 'bureautique : matériel (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(264, 'bureaux : fournitures pour (détail)', 'bureaux : fournitures pour (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(265, 'bureaux : fournitures pour (fabrication, gros)', 'bureaux : fournitures pour (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(266, 'bâches (vente, location)', 'bâches (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(267, 'bâtiment (entreprises)', 'bâtiment (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(268, 'béton : produits en', 'béton : produits en', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(269, 'béton prêt à l''emploi', 'béton prêt à l''emploi', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(270, 'béton, béton armé et précontraint (entreprises)', 'béton, béton armé et précontraint (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(271, 'cabarets et music-halls', 'cabarets et music-halls', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(272, 'cadeaux (détail)', 'cadeaux (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(273, 'cadeaux d''entreprises', 'cadeaux d''entreprises', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(274, 'cadeaux, souvenirs (fabrication, gros)', 'cadeaux, souvenirs (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(275, 'cadres, fournitures pour encadrements (fabrication, vente)', 'cadres, fournitures pour encadrements (fabrication, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(276, 'café (torréfaction)', 'café (torréfaction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(277, 'café, cacao (importation, négoce)', 'café, cacao (importation, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(278, 'cafés, brasseries', 'cafés, brasseries', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(279, 'cafés, brasseries, bars : matériel et fournitures', 'cafés, brasseries, bars : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:34', 0, 1, 0, 0),
(280, 'caillebotis et planchers métalliques', 'caillebotis et planchers métalliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(281, 'caisses de retraite et de prévoyance', 'caisses de retraite et de prévoyance', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(282, 'caisses enregistreuses, terminaux de paiement électronique', 'caisses enregistreuses, terminaux de paiement électronique', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(283, 'camions, véhicules industriels (agents, concessionnaires et succursales)', 'camions, véhicules industriels (agents, concessionnaires et succursales)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(284, 'camping (terrains)', 'camping (terrains)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(285, 'camping : articles et accessoires (vente, réparation)', 'camping : articles et accessoires (vente, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(286, 'camping-cars, caravanes, mobile homes (vente)', 'camping-cars, caravanes, mobile homes (vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(287, 'camping-cars, caravanes, mobile homes et équipements (fabrication)', 'camping-cars, caravanes, mobile homes et équipements (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(288, 'canalisations (pose, entretien)', 'canalisations (pose, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(289, 'cannage, rempaillage', 'cannage, rempaillage', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(290, 'caoutchouc : matières premières (production, distribution)', 'caoutchouc : matières premières (production, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(291, 'caoutchouc : produits, semi-produits (fabrication, négoce)', 'caoutchouc : produits, semi-produits (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(292, 'caravanes, camping-cars (gardiennage)', 'caravanes, camping-cars (gardiennage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(293, 'carburants (commerce)', 'carburants (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(294, 'carburation, allumage (stations techniques)', 'carburation, allumage (stations techniques)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(295, 'carrelages, dallages (fabrication)', 'carrelages, dallages (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(296, 'carrelages, dallages (négoce)', 'carrelages, dallages (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(297, 'carrelages, dallages (vente, pose, traitement)', 'carrelages, dallages (vente, pose, traitement)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(298, 'carrières (exploitation)', 'carrières (exploitation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(299, 'carrosserie et peinture automobile', 'carrosserie et peinture automobile', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(300, 'carrosserie industrielle', 'carrosserie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(301, 'carrosseries : fournitures pour', 'carrosseries : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(302, 'cartes de paiement et de retrait', 'cartes de paiement et de retrait', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(303, 'cartes postales (édition)', 'cartes postales (édition)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(304, 'cartes, guides et plans (édition)', 'cartes, guides et plans (édition)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(305, 'cartographie', 'cartographie', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(306, 'carton (production)', 'carton (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(307, 'carton ondulé (fabrication, transformation)', 'carton ondulé (fabrication, transformation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(308, 'cartonnages (fabrication)', 'cartonnages (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(309, 'casinos, établissements de jeux', 'casinos, établissements de jeux', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(310, 'casses automobiles', 'casses automobiles', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(311, 'caution (sociétés)', 'caution (sociétés)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(312, 'caves : articles et équipements (détail)', 'caves : articles et équipements (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(313, 'ceintures et bretelles (fabrication)', 'ceintures et bretelles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(314, 'centrales de réservations hôtelières', 'centrales de réservations hôtelières', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(315, 'centres auto, entretien rapide', 'centres auto, entretien rapide', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(316, 'centres commerciaux et grands magasins', 'centres commerciaux et grands magasins', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(317, 'centres d''appels (prestataires de services)', 'centres d''appels (prestataires de services)', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(318, 'centres de dialyse rénale', 'centres de dialyse rénale', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(319, 'centres de vacances et de loisirs pour enfants', 'centres de vacances et de loisirs pour enfants', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(320, 'centres et associations de gestion agréés', 'centres et associations de gestion agréés', 0, 'default.png', 0, 1, '2017-10-24 06:46:35', 0, 1, 0, 0),
(321, 'centres médicaux et sociaux, dispensaires', 'centres médicaux et sociaux, dispensaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(322, 'centres équestres, équitation', 'centres équestres, équitation', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(323, 'chalets et constructions en bois (fabrication, vente)', 'chalets et constructions en bois (fabrication, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(324, 'chambres d''hôtes', 'chambres d''hôtes', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(325, 'chambres funéraires', 'chambres funéraires', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(326, 'champignons (production)', 'champignons (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(327, 'champs de courses et hippodromes', 'champs de courses et hippodromes', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(328, 'change (bureaux de)', 'change (bureaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(329, 'chapeaux et coiffures (fabrication)', 'chapeaux et coiffures (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(330, 'chapellerie (détail)', 'chapellerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(331, 'charbon, charbon de bois (gros)', 'charbon, charbon de bois (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(332, 'charcuteries (détail)', 'charcuteries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(333, 'charcuteries (fabrication)', 'charcuteries (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(334, 'charcuteries (gros)', 'charcuteries (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(335, 'chariots élévateurs, chariots de manutention', 'chariots élévateurs, chariots de manutention', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(336, 'charpentes en bois (entreprises)', 'charpentes en bois (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(337, 'charpentes métalliques (fabrication)', 'charpentes métalliques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(338, 'chaudières industrielles (fabrication)', 'chaudières industrielles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(339, 'chaudières industrielles (vente, location, entretien)', 'chaudières industrielles (vente, location, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(340, 'chaudières pour le chauffage central (fabrication)', 'chaudières pour le chauffage central (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(341, 'chaudronnerie industrielle', 'chaudronnerie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(342, 'chaudronnerie inoxydable', 'chaudronnerie inoxydable', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(343, 'chauffage (dépannage, entretien)', 'chauffage (dépannage, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(344, 'chauffage (exploitation de)', 'chauffage (exploitation de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(345, 'chauffage (vente, installation)', 'chauffage (vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(346, 'chauffage : appareils et fournitures (détail)', 'chauffage : appareils et fournitures (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(347, 'chauffage central : matériel (fabrication, gros)', 'chauffage central : matériel (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(348, 'chauffage industriel (installation)', 'chauffage industriel (installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(349, 'chauffage par le gaz (installation, dépannage)', 'chauffage par le gaz (installation, dépannage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0);
INSERT INTO `otd_business_category` (`cat_id`, `cat_name`, `cat_description`, `cat_parent`, `cat_img_path`, `cat_homedisplay`, `cat_status`, `cat_date_created`, `cat_by_user`, `type`, `cat_hometabdisplay`, `cat_hometab_id`) VALUES
(350, 'chauffage urbain', 'chauffage urbain', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(351, 'chauffage électrique (installation, dépannage)', 'chauffage électrique (installation, dépannage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(352, 'chauffe-eau (fabrication, gros)', 'chauffe-eau (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(353, 'chauffe-eau (installation, dépannage)', 'chauffe-eau (installation, dépannage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(354, 'chauffeurs d''automobiles', 'chauffeurs d''automobiles', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(355, 'chaussures (détail)', 'chaussures (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(356, 'chaussures (fabrication)', 'chaussures (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(357, 'chaussures (gros)', 'chaussures (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(358, 'chaussures : matériel, fournitures, accessoires (fabrication, gros)', 'chaussures : matériel, fournitures, accessoires (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(359, 'chaînes (fabrication)', 'chaînes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(360, 'cheminées d''intérieur et accessoires', 'cheminées d''intérieur et accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(361, 'cheminées, incinérateurs industriels (construction)', 'cheminées, incinérateurs industriels (construction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(362, 'chemisage et tubage de conduits de fumée', 'chemisage et tubage de conduits de fumée', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(363, 'chemiserie et sous-vêtements masculins (détail)', 'chemiserie et sous-vêtements masculins (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(364, 'chemiserie et sous-vêtements masculins (fabrication, gros)', 'chemiserie et sous-vêtements masculins (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(365, 'chevaux (commerce)', 'chevaux (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(366, 'chiropracteurs', 'chiropracteurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(367, 'chocolateries, confiseries (détail)', 'chocolateries, confiseries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(368, 'chocolateries, confiseries (fabrication)', 'chocolateries, confiseries (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(369, 'chocolateries, confiseries, biscuiteries (gros)', 'chocolateries, confiseries, biscuiteries (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(370, 'châles, écharpes et foulards (fabrication)', 'châles, écharpes et foulards (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(371, 'cidre (production)', 'cidre (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(372, 'ciments et chaux (fabrication)', 'ciments et chaux (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:36', 0, 1, 0, 0),
(373, 'cimetières', 'cimetières', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(374, 'cintrage et pliage de tôles et de tubes', 'cintrage et pliage de tôles et de tubes', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(375, 'cinéma (distribution)', 'cinéma (distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(376, 'cinéma (production, réalisation)', 'cinéma (production, réalisation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(377, 'cinéma (salles)', 'cinéma (salles)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(378, 'cinéma : films éducatifs, industriels et publicitaires (production)', 'cinéma : films éducatifs, industriels et publicitaires (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(379, 'circuits imprimés', 'circuits imprimés', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(380, 'cirques, attractions foraines', 'cirques, attractions foraines', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(381, 'climatisation (études, installation)', 'climatisation (études, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(382, 'climatisation, aération, ventilation : matériel (fabrication, distribution)', 'climatisation, aération, ventilation : matériel (fabrication, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(383, 'cliniques chirurgicales', 'cliniques chirurgicales', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(384, 'cliniques dentaires', 'cliniques dentaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(385, 'cliniques médicales', 'cliniques médicales', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(386, 'cliniques psychiatriques', 'cliniques psychiatriques', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(387, 'cliniques-polycliniques', 'cliniques-polycliniques', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(388, 'cloches (fonderies et électrification)', 'cloches (fonderies et électrification)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(389, 'cloisons (vente, pose)', 'cloisons (vente, pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(390, 'cloisons, plafonds (fabrication)', 'cloisons, plafonds (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(391, 'clubs de forme', 'clubs de forme', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(392, 'clubs, associations de loisirs', 'clubs, associations de loisirs', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(393, 'clés (reproduction)', 'clés (reproduction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(394, 'clôtures et barrières', 'clôtures et barrières', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(395, 'coffrages (fabrication, location, vente)', 'coffrages (fabrication, location, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(396, 'coffres-forts (fabrication, installation)', 'coffres-forts (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(397, 'cognac et pineau des Charentes', 'cognac et pineau des Charentes', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(398, 'coiffeurs', 'coiffeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(399, 'coiffure : matériel, fournitures, accessoires', 'coiffure : matériel, fournitures, accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(400, 'coiffure, esthétique (à domicile)', 'coiffure, esthétique (à domicile)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(401, 'collectivités et administrations : fournitures et équipements pour', 'collectivités et administrations : fournitures et équipements pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(402, 'colles (fabrication)', 'colles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(403, 'collèges privés', 'collèges privés', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(404, 'collèges publics', 'collèges publics', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(405, 'colorants, couleurs et pigments (fabrication, gros)', 'colorants, couleurs et pigments (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(406, 'combustibles : fioul, bois, charbon (détail)', 'combustibles : fioul, bois, charbon (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(407, 'commerces et industries (transactions)', 'commerces et industries (transactions)', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(408, 'commissaires aux comptes', 'commissaires aux comptes', 0, 'default.png', 0, 1, '2017-10-24 06:46:37', 0, 1, 0, 0),
(409, 'commissaires-priseurs judiciaires', 'commissaires-priseurs judiciaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(410, 'commissariats de police', 'commissariats de police', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(411, 'commissionnaires en marchandises', 'commissionnaires en marchandises', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(412, 'communautés religieuses', 'communautés religieuses', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(413, 'communication d''entreprises (conseils)', 'communication d''entreprises (conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(414, 'composants électroniques passifs (fabrication)', 'composants électroniques passifs (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(415, 'compresseurs', 'compresseurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(416, 'compteurs (fabrication, installation)', 'compteurs (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(417, 'condiments, épices, vinaigres, sauces préparées (fabrication, gros)', 'condiments, épices, vinaigres, sauces préparées (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(418, 'conditionnement, emballage à façon (entreprises)', 'conditionnement, emballage à façon (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(419, 'conseils conjugaux et familiaux', 'conseils conjugaux et familiaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(420, 'conseils en commerce extérieur', 'conseils en commerce extérieur', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(421, 'conseils en formation, gestion de personnel', 'conseils en formation, gestion de personnel', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(422, 'conseils en organisation, gestion management', 'conseils en organisation, gestion management', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(423, 'conseils en économie privée, sociale et familiale', 'conseils en économie privée, sociale et familiale', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(424, 'conseils et études financières', 'conseils et études financières', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(425, 'conseils et études économiques et sociologiques', 'conseils et études économiques et sociologiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(426, 'conservatoires', 'conservatoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(427, 'conserves de fruits et confitures (fabrication, gros)', 'conserves de fruits et confitures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(428, 'conserves de légumes (fabrication, gros)', 'conserves de légumes (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(429, 'conserves de plats cuisinés (fabrication, gros)', 'conserves de plats cuisinés (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(430, 'conserves de poissons et salaisons maritimes (fabrication, gros)', 'conserves de poissons et salaisons maritimes (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(431, 'conserves de viandes (fabrication, gros)', 'conserves de viandes (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(432, 'constructeurs de maisons individuelles', 'constructeurs de maisons individuelles', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(433, 'construction mécanique', 'construction mécanique', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(434, 'constructions et installations agricoles', 'constructions et installations agricoles', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(435, 'constructions industrielles', 'constructions industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(436, 'constructions mobiles de chantier', 'constructions mobiles de chantier', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(437, 'constructions métalliques', 'constructions métalliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(438, 'constructions navales et fluviales (fabrication, réparation, entretien)', 'constructions navales et fluviales (fabrication, réparation, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(439, 'constructions préfabriquées et démontables', 'constructions préfabriquées et démontables', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(440, 'conteneurs (vente, location, réparation)', 'conteneurs (vente, location, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(441, 'contrôle technique de véhicules', 'contrôle technique de véhicules', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(442, 'contrôles de bâtiment (construction, sécurité)', 'contrôles de bâtiment (construction, sécurité)', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(443, 'contrôles de fabrication, de fonctionnement', 'contrôles de fabrication, de fonctionnement', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(444, 'coopératives agricoles', 'coopératives agricoles', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(445, 'coopératives d''alimentation', 'coopératives d''alimentation', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(446, 'coopératives laitières', 'coopératives laitières', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(447, 'coordination de travaux de bâtiment', 'coordination de travaux de bâtiment', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(448, 'corderies et ficelleries', 'corderies et ficelleries', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(449, 'cordonneries', 'cordonneries', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(450, 'costumiers : costumes de scène', 'costumiers : costumes de scène', 0, 'default.png', 0, 1, '2017-10-24 06:46:38', 0, 1, 0, 0),
(451, 'coupes, insignes, trophées', 'coupes, insignes, trophées', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(452, 'courrier, colis (envoi, distribution)', 'courrier, colis (envoi, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(453, 'cours de langues', 'cours de langues', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(454, 'cours par correspondance, enseignement à distance', 'cours par correspondance, enseignement à distance', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(455, 'cours particuliers, assistance scolaire', 'cours particuliers, assistance scolaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(456, 'courses, transports légers de proximité', 'courses, transports légers de proximité', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(457, 'courtiers de marchandises', 'courtiers de marchandises', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(458, 'courtiers financiers', 'courtiers financiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(459, 'coutellerie (détail)', 'coutellerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(460, 'coutellerie (fabrication, gros)', 'coutellerie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(461, 'couture (haute couture, création)', 'couture (haute couture, création)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(462, 'couture et confection (façonniers)', 'couture et confection (façonniers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(463, 'couture, retouches', 'couture, retouches', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(464, 'couverture, plomberie, zinguerie', 'couverture, plomberie, zinguerie', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(465, 'couvertures (fabrication)', 'couvertures (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(466, 'cravates (fabrication)', 'cravates (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(467, 'cristalleries', 'cristalleries', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(468, 'crustacés et coquillages (gros)', 'crustacés et coquillages (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(469, 'crèches', 'crèches', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(470, 'crédit et crédit-bail aux entreprises (établissements)', 'crédit et crédit-bail aux entreprises (établissements)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(471, 'crédit immobilier (établissements)', 'crédit immobilier (établissements)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(472, 'crédit à la consommation (établissements)', 'crédit à la consommation (établissements)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(473, 'crématoriums', 'crématoriums', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(474, 'crêperies', 'crêperies', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(475, 'cuir (réparation, nettoyage, teinture)', 'cuir (réparation, nettoyage, teinture)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(476, 'cuirs et crépins (négoce)', 'cuirs et crépins (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(477, 'cuirs et peaux (gros)', 'cuirs et peaux (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(478, 'cuirs et peaux bruts (importation, exportation, gros)', 'cuirs et peaux bruts (importation, exportation, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(479, 'cuisines (vente, installation)', 'cuisines (vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(480, 'cuisines : meubles (fabrication)', 'cuisines : meubles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(481, 'cuisines professionnelles (fabrication, installation)', 'cuisines professionnelles (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(482, 'cuivrerie et dinanderie', 'cuivrerie et dinanderie', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(483, 'culte israélite', 'culte israélite', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(484, 'culte musulman', 'culte musulman', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(485, 'cultes divers', 'cultes divers', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(486, 'cures, réadaptation, convalescence (établissements de)', 'cures, réadaptation, convalescence (établissements de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(487, 'cuves, citernes, réservoirs (fabrication)', 'cuves, citernes, réservoirs (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(488, 'cycles, motos, scooters et pièces détachées (fabrication)', 'cycles, motos, scooters et pièces détachées (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(489, 'câbleries', 'câbleries', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(490, 'câbles et fils électriques (fabrication, gros)', 'câbles et fils électriques (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(491, 'céramiques (fabrication)', 'céramiques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(492, 'céramiques : matériel et fournitures', 'céramiques : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:39', 0, 1, 0, 0),
(493, 'céréales (production, commerce)', 'céréales (production, commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(494, 'danse (salles et leçons)', 'danse (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(495, 'danse : articles et vêtements (détail)', 'danse : articles et vêtements (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(496, 'dentistes : chirurgiens-dentistes et docteurs en chirurgie dentaire', 'dentistes : chirurgiens-dentistes et docteurs en chirurgie dentaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(497, 'dentistes : chirurgiens-dentistes qualifiés orthopédie dento-faciale', 'dentistes : chirurgiens-dentistes qualifiés orthopédie dento-faciale', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(498, 'dentistes : chirurgiens-dentistes, prothésistes dentaires : matériel et fournitures pour', 'dentistes : chirurgiens-dentistes, prothésistes dentaires : matériel et fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(499, 'designers', 'designers', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(500, 'dessinateurs (divers)', 'dessinateurs (divers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(501, 'dessinateurs artistes', 'dessinateurs artistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(502, 'dessinateurs industriels', 'dessinateurs industriels', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(503, 'dessins en bâtiment (bureaux de)', 'dessins en bâtiment (bureaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(504, 'diamants, pierres précieuses et gemmes', 'diamants, pierres précieuses et gemmes', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(505, 'diesel (réparation, entretien)', 'diesel (réparation, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(506, 'discothèques et dancings', 'discothèques et dancings', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(507, 'discount, stocks d''usines pour professionnels', 'discount, stocks d''usines pour professionnels', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(508, 'discount, stocks, dégriffés (détail)', 'discount, stocks, dégriffés (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(509, 'disquaires', 'disquaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(510, 'distilleries agricoles et industrielles', 'distilleries agricoles et industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(511, 'distributeurs automatiques (fabrication)', 'distributeurs automatiques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(512, 'distributeurs automatiques (vente, location, gestion)', 'distributeurs automatiques (vente, location, gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(513, 'distribution d''imprimés', 'distribution d''imprimés', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(514, 'diététiciens', 'diététiciens', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(515, 'diététique : produits biologiques, naturels et de régime (détail)', 'diététique : produits biologiques, naturels et de régime (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(516, 'diététique : produits biologiques, naturels et de régime (fabrication, gros)', 'diététique : produits biologiques, naturels et de régime (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(517, 'domiciliations commerciales et industrielles', 'domiciliations commerciales et industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(518, 'doreurs sur bois', 'doreurs sur bois', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(519, 'dragage (travaux de)', 'dragage (travaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(520, 'drainage, irrigation (travaux)', 'drainage, irrigation (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(521, 'drapeaux et banderoles (fabrication)', 'drapeaux et banderoles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(522, 'drogueries (détail)', 'drogueries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(523, 'drogueries (gros)', 'drogueries (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(524, 'déblaiements, débarras', 'déblaiements, débarras', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(525, 'débroussaillement (travaux de)', 'débroussaillement (travaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(526, 'décalcomanie, impression, transfert', 'décalcomanie, impression, transfert', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(527, 'décapage, dégraissage (préparation des surfaces)', 'décapage, dégraissage (préparation des surfaces)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(528, 'déchets des ménages (collecte, tri, traitement)', 'déchets des ménages (collecte, tri, traitement)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(529, 'décolletage, tournage', 'décolletage, tournage', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(530, 'décorateurs', 'décorateurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(531, 'décorateurs ensembliers', 'décorateurs ensembliers', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(532, 'décoration florale', 'décoration florale', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(533, 'décorations civiles et militaires', 'décorations civiles et militaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(534, 'découpage et emboutissage', 'découpage et emboutissage', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(535, 'démolition (entreprises)', 'démolition (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(536, 'déménagements', 'déménagements', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(537, 'déménagements industriels et de bureaux', 'déménagements industriels et de bureaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(538, 'dépannages et remorquages d''automobiles', 'dépannages et remorquages d''automobiles', 0, 'default.png', 0, 1, '2017-10-24 06:46:40', 0, 1, 0, 0),
(539, 'dépannages urgents à domicile', 'dépannages urgents à domicile', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(540, 'dépôts-vente : meubles, équipements pour la maison', 'dépôts-vente : meubles, équipements pour la maison', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(541, 'dépôts-vente : vêtements', 'dépôts-vente : vêtements', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(542, 'désinfection, désinsectisation, dératisation', 'désinfection, désinsectisation, dératisation', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(543, 'détartrage', 'détartrage', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(544, 'détectives, agents de recherches privées', 'détectives, agents de recherches privées', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(545, 'eaux (distribution, services)', 'eaux (distribution, services)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(546, 'eaux minérales et de source (production)', 'eaux minérales et de source (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(547, 'emballage d''expédition (entreprises)', 'emballage d''expédition (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(548, 'emballages, conditionnements : machines, matériel et fournitures pour', 'emballages, conditionnements : machines, matériel et fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(549, 'emballages, conditionnements en bois', 'emballages, conditionnements en bois', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(550, 'emballages, conditionnements en carton, papier', 'emballages, conditionnements en carton, papier', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(551, 'emballages, conditionnements en matière plastique', 'emballages, conditionnements en matière plastique', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(552, 'emballages, conditionnements en verre', 'emballages, conditionnements en verre', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(553, 'emballages, conditionnements métalliques', 'emballages, conditionnements métalliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(554, 'embouteillage', 'embouteillage', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(555, 'encadrements (travaux)', 'encadrements (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(556, 'engrais et fertilisants (production)', 'engrais et fertilisants (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(557, 'engrenages et organes de transmission', 'engrenages et organes de transmission', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(558, 'enregistrement sonore (studios)', 'enregistrement sonore (studios)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(559, 'enseignement : coiffure, esthétique', 'enseignement : coiffure, esthétique', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(560, 'enseignement : commerce, gestion, informatique', 'enseignement : commerce, gestion, informatique', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(561, 'enseignement : organismes coordonnateurs et de gestion des établissements privés', 'enseignement : organismes coordonnateurs et de gestion des établissements privés', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(562, 'enseignement : professions artistiques', 'enseignement : professions artistiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(563, 'enseignement : social, paramédical', 'enseignement : social, paramédical', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(564, 'enseignement : tourisme, cuisine, hôtellerie', 'enseignement : tourisme, cuisine, hôtellerie', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(565, 'enseignement divers', 'enseignement divers', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(566, 'enseignement supérieur privé', 'enseignement supérieur privé', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(567, 'enseignement supérieur public', 'enseignement supérieur public', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(568, 'enseignes, enseignes lumineuses (fabrication, vente, installation)', 'enseignes, enseignes lumineuses (fabrication, vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(569, 'entreprises générales de bâtiment', 'entreprises générales de bâtiment', 0, 'default.png', 0, 1, '2017-10-24 06:46:41', 0, 1, 0, 0),
(570, 'entrepôts et magasins généraux', 'entrepôts et magasins généraux', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(571, 'entrepôts frigorifiques', 'entrepôts frigorifiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(572, 'enveloppes, sacs et pochettes (fabrication)', 'enveloppes, sacs et pochettes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(573, 'environnement (conseil, étude, contrôle)', 'environnement (conseil, étude, contrôle)', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(574, 'ergonomie (conseils, études, formation)', 'ergonomie (conseils, études, formation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(575, 'ergothérapeutes', 'ergothérapeutes', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(576, 'escaliers (fabrication, installation)', 'escaliers (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(577, 'escrime (salles et leçons)', 'escrime (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(578, 'essuie-mains et distributeurs de savons (fabrication, location-entretien)', 'essuie-mains et distributeurs de savons (fabrication, location-entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(579, 'essuyage de machines : fournitures', 'essuyage de machines : fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(580, 'estampage, matriçage', 'estampage, matriçage', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(581, 'experts agricoles et fonciers', 'experts agricoles et fonciers', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(582, 'experts divers', 'experts divers', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(583, 'experts en antiquités et objets d''art', 'experts en antiquités et objets d''art', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(584, 'experts en assurances', 'experts en assurances', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(585, 'experts en automobiles', 'experts en automobiles', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(586, 'experts en immobilier', 'experts en immobilier', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(587, 'experts en techniques du bâtiment', 'experts en techniques du bâtiment', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(588, 'experts forestiers', 'experts forestiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(589, 'experts-comptables', 'experts-comptables', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(590, 'experts-comptables stagiaires autorisés', 'experts-comptables stagiaires autorisés', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(591, 'explosifs', 'explosifs', 0, 'default.png', 0, 1, '2017-10-24 06:46:42', 0, 1, 0, 0),
(592, 'expositions, foires, salons (comités d''organisation)', 'expositions, foires, salons (comités d''organisation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(593, 'expositions, foires, salons (installation, agencement)', 'expositions, foires, salons (installation, agencement)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(594, 'expositions, foires, salons : fournitures', 'expositions, foires, salons : fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(595, 'extincteurs (distribution, maintenance)', 'extincteurs (distribution, maintenance)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(596, 'extinction d''incendie : matériel et fournitures (fabrication, gros)', 'extinction d''incendie : matériel et fournitures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(597, 'facteurs d''orgues, pianos, clavecins', 'facteurs d''orgues, pianos, clavecins', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(598, 'farines, semoules, produits du travail du grain (fabrication)', 'farines, semoules, produits du travail du grain (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(599, 'façades (travaux de revêtements)', 'façades (travaux de revêtements)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(600, 'fenêtres', 'fenêtres', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(601, 'ferblantiers', 'ferblantiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(602, 'fermes auberges', 'fermes auberges', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(603, 'fermetures de bâtiment', 'fermetures de bâtiment', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(604, 'ferronnerie (divers)', 'ferronnerie (divers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(605, 'ferronnerie d''art', 'ferronnerie d''art', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(606, 'fers (commerce)', 'fers (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(607, 'fichiers d''adresses (vente, location, gestion)', 'fichiers d''adresses (vente, location, gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(608, 'filatures, fils et activités connexes', 'filatures, fils et activités connexes', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(609, 'filets (fabrication)', 'filets (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(610, 'fils et filés (négoce)', 'fils et filés (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(611, 'filtres et éléments filtrants', 'filtres et éléments filtrants', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(612, 'filtres à air et à gaz', 'filtres à air et à gaz', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(613, 'flaconnages', 'flaconnages', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(614, 'fleuristes', 'fleuristes', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(615, 'fleuristes : matériel et fournitures pour', 'fleuristes : matériel et fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(616, 'fleurs et plantes artificielles (fabrication, commerce)', 'fleurs et plantes artificielles (fabrication, commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(617, 'fleurs et plantes naturelles (gros)', 'fleurs et plantes naturelles (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(618, 'flexibles (fabrication, gros)', 'flexibles (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(619, 'flocage', 'flocage', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(620, 'foies gras (production, vente)', 'foies gras (production, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(621, 'folklore', 'folklore', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(622, 'fondations et consolidations de sols', 'fondations et consolidations de sols', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(623, 'fonderies d''art', 'fonderies d''art', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(624, 'fonderies de métaux ferreux', 'fonderies de métaux ferreux', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(625, 'fonderies de métaux non ferreux et alliages', 'fonderies de métaux non ferreux et alliages', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(626, 'fontaines (fabrication, installation)', 'fontaines (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(627, 'fontaines à eau', 'fontaines à eau', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(628, 'forages, sondages : matériel et fournitures', 'forages, sondages : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(629, 'forages, sondages, construction de puits (travaux)', 'forages, sondages, construction de puits (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(630, 'forgerons, maréchaux-ferrants, charrons', 'forgerons, maréchaux-ferrants, charrons', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(631, 'formation continue', 'formation continue', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(632, 'formes de découpe (fabrication)', 'formes de découpe (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(633, 'forêts (exploitation)', 'forêts (exploitation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(634, 'forêts (travaux, conseils, gestion)', 'forêts (travaux, conseils, gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(635, 'fosses septiques et accessoires', 'fosses septiques et accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(636, 'fournitures et matériel industriels', 'fournitures et matériel industriels', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(637, 'fourrures (confection, distribution)', 'fourrures (confection, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:43', 0, 1, 0, 0),
(638, 'fourrures (détail, garde, entretien)', 'fourrures (détail, garde, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(639, 'fours (construction)', 'fours (construction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(640, 'foyers d''hébergement', 'foyers d''hébergement', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(641, 'franchisage (sociétés)', 'franchisage (sociétés)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(642, 'freins industriels', 'freins industriels', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(643, 'frigorifiques : installations', 'frigorifiques : installations', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(644, 'frigorifiques : équipements pour camions', 'frigorifiques : équipements pour camions', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(645, 'fripiers', 'fripiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(646, 'froid industriel : équipement', 'froid industriel : équipement', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(647, 'fromageries (détail)', 'fromageries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(648, 'fromages (affinage)', 'fromages (affinage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(649, 'fromages (fabrication)', 'fromages (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(650, 'fruits et légumes frais (expédition, exportation)', 'fruits et légumes frais (expédition, exportation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(651, 'fruits et légumes frais (gros)', 'fruits et légumes frais (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(652, 'fruits et légumes prêts à consommer', 'fruits et légumes prêts à consommer', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(653, 'fruits et légumes secs (détail)', 'fruits et légumes secs (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(654, 'fruits et légumes secs (gros)', 'fruits et légumes secs (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(655, 'fruits, légumes frais et primeurs (détail)', 'fruits, légumes frais et primeurs (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(656, 'fumisterie industrielle', 'fumisterie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(657, 'fêtes : articles de (détail)', 'fêtes : articles de (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(658, 'fêtes : articles de (fabrication, gros)', 'fêtes : articles de (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(659, 'gainerie', 'gainerie', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(660, 'galeries d''art', 'galeries d''art', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(661, 'galvanisation, étamage, plombage', 'galvanisation, étamage, plombage', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(662, 'gants (fabrication)', 'gants (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(663, 'garages d''automobiles, réparation', 'garages d''automobiles, réparation', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(664, 'garages de poids lourds', 'garages de poids lourds', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(665, 'garages et stations-service (outillage, installation, équipement)', 'garages et stations-service (outillage, installation, équipement)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(666, 'garde d''enfants', 'garde d''enfants', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(667, 'garde-meubles', 'garde-meubles', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(668, 'gares routières', 'gares routières', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(669, 'gastronomie : produits exotiques (détail)', 'gastronomie : produits exotiques (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(670, 'gastronomie : spécialités régionales (détail)', 'gastronomie : spécialités régionales (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(671, 'gastronomie : spécialités régionales et étrangères (fabrication, importation)', 'gastronomie : spécialités régionales et étrangères (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(672, 'gastronomie : spécialités régionales et étrangères (production, gros)', 'gastronomie : spécialités régionales et étrangères (production, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(673, 'gaz (réparation d''appareils)', 'gaz (réparation d''appareils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(674, 'gaz (traitement industriel)', 'gaz (traitement industriel)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(675, 'gaz butane et propane (production, distribution)', 'gaz butane et propane (production, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:44', 0, 1, 0, 0),
(676, 'gaz comprimés et liquéfiés', 'gaz comprimés et liquéfiés', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(677, 'gaz naturel', 'gaz naturel', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(678, 'gendarmeries', 'gendarmeries', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(679, 'gestion de patrimoine (conseils)', 'gestion de patrimoine (conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(680, 'glace à rafraîchir', 'glace à rafraîchir', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(681, 'glaciers', 'glaciers', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(682, 'glaciers : fournitures pour', 'glaciers : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(683, 'golf (terrains et leçons)', 'golf (terrains et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(684, 'gouttières d''eaux pluviales (fournitures et pose)', 'gouttières d''eaux pluviales (fournitures et pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(685, 'graines et semences (production, gros)', 'graines et semences (production, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(686, 'graineteries : semences, produits de jardin (détail)', 'graineteries : semences, produits de jardin (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(687, 'grains et farines (négoce)', 'grains et farines (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(688, 'grains et fourrages', 'grains et fourrages', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(689, 'graphistes', 'graphistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(690, 'graphologues', 'graphologues', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(691, 'graveurs (divers)', 'graveurs (divers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(692, 'graveurs en bijoux et médailles', 'graveurs en bijoux et médailles', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(693, 'graveurs et imprimeurs sur matières plastiques', 'graveurs et imprimeurs sur matières plastiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(694, 'graveurs sur pierre', 'graveurs sur pierre', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(695, 'gravure et décoration sur métaux', 'gravure et décoration sur métaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(696, 'gravure, peinture et décoration sur verre et glace', 'gravure, peinture et décoration sur verre et glace', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(697, 'grillages métalliques', 'grillages métalliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(698, 'grilles et rideaux métalliques', 'grilles et rideaux métalliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(699, 'groupements d''achats', 'groupements d''achats', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(700, 'groupes électrogènes (vente, location)', 'groupes électrogènes (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(701, 'grues, monte-charge et élévateurs de chantiers', 'grues, monte-charge et élévateurs de chantiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(702, 'guides de montagne, moniteurs de ski, accompagnateurs en montagne', 'guides de montagne, moniteurs de ski, accompagnateurs en montagne', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(703, 'gymnastique (salles et leçons)', 'gymnastique (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(704, 'généalogistes', 'généalogistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(705, 'géologie et géophysique', 'géologie et géophysique', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(706, 'géomètres-experts', 'géomètres-experts', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(707, 'gérants de portefeuilles', 'gérants de portefeuilles', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(708, 'gîtes', 'gîtes', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(709, 'handicapés : matériel et services pour', 'handicapés : matériel et services pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(710, 'herboristeries (gros)', 'herboristeries (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(711, 'herboristes (détail)', 'herboristes (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0);
INSERT INTO `otd_business_category` (`cat_id`, `cat_name`, `cat_description`, `cat_parent`, `cat_img_path`, `cat_homedisplay`, `cat_status`, `cat_date_created`, `cat_by_user`, `type`, `cat_hometabdisplay`, `cat_hometab_id`) VALUES
(712, 'hi-fi : appareils et accessoires (vente)', 'hi-fi : appareils et accessoires (vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(713, 'horlogerie (détail, réparation)', 'horlogerie (détail, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:45', 0, 1, 0, 0),
(714, 'horlogerie électrique', 'horlogerie électrique', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(715, 'horlogerie, matériel et fournitures (fabrication, gros)', 'horlogerie, matériel et fournitures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(716, 'horticulteurs', 'horticulteurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(717, 'horticulture, arboriculture : matériel et fournitures', 'horticulture, arboriculture : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(718, 'hospitalisation à domicile (structures de soins)', 'hospitalisation à domicile (structures de soins)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(719, 'huiles et graisses alimentaires (fabrication, gros)', 'huiles et graisses alimentaires (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(720, 'huissiers de justice', 'huissiers de justice', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(721, 'humidité (traitement)', 'humidité (traitement)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(722, 'hygiène : articles (fabrication, distribution)', 'hygiène : articles (fabrication, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(723, 'hôpitaux', 'hôpitaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(724, 'hôpitaux : fournitures, équipements', 'hôpitaux : fournitures, équipements', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(725, 'hôtels', 'hôtels', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(726, 'hôtels : fournitures', 'hôtels : fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(727, 'hôtesses d''accueil', 'hôtesses d''accueil', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(728, 'immobilier (locations saisonnières et temporaires)', 'immobilier (locations saisonnières et temporaires)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(729, 'immobilier (lotisseurs, aménageurs fonciers)', 'immobilier (lotisseurs, aménageurs fonciers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(730, 'immobilier d''entreprise (conseils en)', 'immobilier d''entreprise (conseils en)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(731, 'import-export, sociétés de commerce international', 'import-export, sociétés de commerce international', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(732, 'imprimerie, industries graphiques : matériel et fournitures', 'imprimerie, industries graphiques : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(733, 'imprimeurs', 'imprimeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(734, 'imprimeurs en continu', 'imprimeurs en continu', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(735, 'imprimeurs éditeurs', 'imprimeurs éditeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(736, 'impôts, Trésor public', 'impôts, Trésor public', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(737, 'incinérations industrielles (services d'')', 'incinérations industrielles (services d'')', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(738, 'industries chimiques : appareils, machines et procédés', 'industries chimiques : appareils, machines et procédés', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(739, 'industries laitières : matériel et équipements', 'industries laitières : matériel et équipements', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(740, 'infirmiers : cabinets, soins à domicile', 'infirmiers : cabinets, soins à domicile', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(741, 'infographie', 'infographie', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(742, 'information par téléphone', 'information par téléphone', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(743, 'informatique : logiciels, progiciels', 'informatique : logiciels, progiciels', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(744, 'informatique : matériel et fournitures', 'informatique : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(745, 'informatique : mini et gros systèmes (vente, installation, maintenance)', 'informatique : mini et gros systèmes (vente, installation, maintenance)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(746, 'informatique et micro-informatique (construction)', 'informatique et micro-informatique (construction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(747, 'informatique, bureautique (services, conseils, ingénierie, formation)', 'informatique, bureautique (services, conseils, ingénierie, formation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(748, 'ingénierie, bureaux d''études (bâtiment)', 'ingénierie, bureaux d''études (bâtiment)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(749, 'ingénierie, bureaux d''études (divers)', 'ingénierie, bureaux d''études (divers)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(750, 'ingénierie, bureaux d''études (industrie)', 'ingénierie, bureaux d''études (industrie)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(751, 'ingénierie, bureaux d''études (infrastructure)', 'ingénierie, bureaux d''études (infrastructure)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(752, 'institutions politiques nationales, Grands Corps de l''Etat, médiateur de la République', 'institutions politiques nationales, Grands Corps de l''Etat, médiateur de la République', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(753, 'instituts de beauté', 'instituts de beauté', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(754, 'instituts médicaux', 'instituts médicaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(755, 'insémination artificielle', 'insémination artificielle', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(756, 'interprètes', 'interprètes', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(757, 'interprètes de conférence', 'interprètes de conférence', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(758, 'intérim (agences)', 'intérim (agences)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(759, 'inventaires (conseils, gestion)', 'inventaires (conseils, gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(760, 'irrigation : matériel', 'irrigation : matériel', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(761, 'isolation (travaux)', 'isolation (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(762, 'isolation : matériaux', 'isolation : matériaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(763, 'jardineries, végétaux, articles de jardin (détail)', 'jardineries, végétaux, articles de jardin (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(764, 'jardins et parcs (aménagement, entretien)', 'jardins et parcs (aménagement, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(765, 'jeux automatiques (fabrication)', 'jeux automatiques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:46', 0, 1, 0, 0),
(766, 'jeux de société : bridge, échecs et autres (clubs et cours)', 'jeux de société : bridge, échecs et autres (clubs et cours)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(767, 'jeux de société, cartes à jouer et accessoires (fabrication)', 'jeux de société, cartes à jouer et accessoires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(768, 'jeux pour cafés ou casinos (vente, location, entretien)', 'jeux pour cafés ou casinos (vente, location, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(769, 'jeux vidéo (vente, location)', 'jeux vidéo (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(770, 'joaillerie (création, fabrication)', 'joaillerie (création, fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(771, 'joaillerie (sertissage, polissage, guillochage)', 'joaillerie (sertissage, polissage, guillochage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(772, 'joailliers (détail)', 'joailliers (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(773, 'joints d''étanchéité (fabrication)', 'joints d''étanchéité (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(774, 'jouets et jeux (détail)', 'jouets et jeux (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(775, 'jouets et jeux (fabrication)', 'jouets et jeux (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(776, 'jouets et jeux (gros)', 'jouets et jeux (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(777, 'journalistes professionnels', 'journalistes professionnels', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(778, 'journaux, presse, magazines (agences d''abonnements)', 'journaux, presse, magazines (agences d''abonnements)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(779, 'journaux, presse, magazines (détail)', 'journaux, presse, magazines (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(780, 'journaux, presse, magazines (édition)', 'journaux, presse, magazines (édition)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(781, 'judo (salles et leçons)', 'judo (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(782, 'karaté (salles et leçons)', 'karaté (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(783, 'kinésithérapeutes : masseurs kinésithérapeutes', 'kinésithérapeutes : masseurs kinésithérapeutes', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(784, 'kinésithérapie : appareils et fournitures', 'kinésithérapie : appareils et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(785, 'laboratoires : appareils, matériel et fournitures', 'laboratoires : appareils, matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(786, 'laboratoires d''analyses agricoles et de biologie végétale', 'laboratoires d''analyses agricoles et de biologie végétale', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(787, 'laboratoires d''analyses de biologie médicale', 'laboratoires d''analyses de biologie médicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(788, 'laboratoires d''analyses industrielles', 'laboratoires d''analyses industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(789, 'laboratoires d''anatomie et cytologie pathologiques humaines', 'laboratoires d''anatomie et cytologie pathologiques humaines', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(790, 'laboratoires de contrôle sanitaire et de l''environnement', 'laboratoires de contrôle sanitaire et de l''environnement', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(791, 'laboratoires homéopathiques', 'laboratoires homéopathiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(792, 'laboratoires pharmaceutiques', 'laboratoires pharmaceutiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(793, 'laboratoires vétérinaires', 'laboratoires vétérinaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(794, 'laines (détail)', 'laines (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(795, 'lait médical et alimentation pour bébés (fabrication, gros)', 'lait médical et alimentation pour bébés (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(796, 'laiteries', 'laiteries', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(797, 'lampes d''éclairage (fabrication, importation)', 'lampes d''éclairage (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(798, 'laser (fabrication et applications)', 'laser (fabrication et applications)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(799, 'lavage, nettoyage de véhicules', 'lavage, nettoyage de véhicules', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(800, 'laveries pour particuliers, laveries en libre-service', 'laveries pour particuliers, laveries en libre-service', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(801, 'lentilles de contact', 'lentilles de contact', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(802, 'lettres pour enseignes et signalisation (fabrication)', 'lettres pour enseignes et signalisation (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(803, 'levage et arrimage : accessoires', 'levage et arrimage : accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(804, 'librairies', 'librairies', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(805, 'librairies, éditions (commissionnaires en)', 'librairies, éditions (commissionnaires en)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(806, 'librairies, éditions anciennes', 'librairies, éditions anciennes', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(807, 'linge de maison (détail)', 'linge de maison (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(808, 'linge de maison (fabrication, gros)', 'linge de maison (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(809, 'lingerie féminine (fabrication)', 'lingerie féminine (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(810, 'lingerie féminine, corseterie, maillots de bain (détail)', 'lingerie féminine, corseterie, maillots de bain (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(811, 'listes de mariage, arts de la table (détail)', 'listes de mariage, arts de la table (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(812, 'literie (détail)', 'literie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(813, 'literie (fabrication)', 'literie (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(814, 'literie (gros)', 'literie (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(815, 'liège : bouchons et articles divers', 'liège : bouchons et articles divers', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(816, 'location d''appareils de chauffage', 'location d''appareils de chauffage', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(817, 'location d''appareils de jardinage', 'location d''appareils de jardinage', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(818, 'location d''appartements', 'location d''appartements', 0, 'default.png', 0, 1, '2017-10-24 06:46:47', 0, 1, 0, 0),
(819, 'location d''automobiles : tourisme et utilitaires', 'location d''automobiles : tourisme et utilitaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(820, 'location d''automobiles avec chauffeur', 'location d''automobiles avec chauffeur', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(821, 'location de bateaux, canoës, kayaks et planches à voile', 'location de bateaux, canoës, kayaks et planches à voile', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(822, 'location de bennes', 'location de bennes', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(823, 'location de bureaux équipés', 'location de bureaux équipés', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(824, 'location de camions, véhicules industriels', 'location de camions, véhicules industriels', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(825, 'location de caravanes, mobile homes et camping-cars', 'location de caravanes, mobile homes et camping-cars', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(826, 'location de chariots à moteur', 'location de chariots à moteur', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(827, 'location de cycles, motos et scooters', 'location de cycles, motos et scooters', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(828, 'location de linge et d''articles textiles, entretien', 'location de linge et d''articles textiles, entretien', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(829, 'location de matériel agricole', 'location de matériel agricole', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(830, 'location de matériel audiovisuel', 'location de matériel audiovisuel', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(831, 'location de matériel d''éclairage', 'location de matériel d''éclairage', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(832, 'location de matériel de bricolage', 'location de matériel de bricolage', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(833, 'location de matériel de bureau et informatique', 'location de matériel de bureau et informatique', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(834, 'location de matériel industriel', 'location de matériel industriel', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(835, 'location de matériel pour entrepreneurs', 'location de matériel pour entrepreneurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(836, 'location de matériel pour réceptions, pour événements', 'location de matériel pour réceptions, pour événements', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(837, 'location de mobilier', 'location de mobilier', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(838, 'location de plantes', 'location de plantes', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(839, 'location de salles', 'location de salles', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(840, 'location de skis', 'location de skis', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(841, 'location de tentes et chapiteaux', 'location de tentes et chapiteaux', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(842, 'location de télévisions, magnétoscopes, matériel hi-fi et vidéo', 'location de télévisions, magnétoscopes, matériel hi-fi et vidéo', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(843, 'location de vêtements et accessoires', 'location de vêtements et accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(844, 'logistique (services, conseils)', 'logistique (services, conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(845, 'loisirs créatifs, travaux manuels', 'loisirs créatifs, travaux manuels', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(846, 'loterie et tombola', 'loterie et tombola', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(847, 'lubrifiants, huiles et graisses industrielles (fabrication, gros)', 'lubrifiants, huiles et graisses industrielles (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(848, 'ludothèques', 'ludothèques', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(849, 'luminaires (détail)', 'luminaires (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(850, 'luminaires décoratifs (fabrication, importation)', 'luminaires décoratifs (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:48', 0, 1, 0, 0),
(851, 'lunetterie (fabrication)', 'lunetterie (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(852, 'luthiers', 'luthiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(853, 'lycées agricoles privés', 'lycées agricoles privés', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(854, 'lycées agricoles publics', 'lycées agricoles publics', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(855, 'lycées d''enseignement', 'lycées d''enseignement', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(856, 'lycées d''enseignement général et technologique privés', 'lycées d''enseignement général et technologique privés', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(857, 'lycées d''enseignement général et technologique publics', 'lycées d''enseignement général et technologique publics', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(858, 'lycées d''enseignement professionnel et technique publics', 'lycées d''enseignement professionnel et technique publics', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(859, 'machines pour industries diverses', 'machines pour industries diverses', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(860, 'machines textiles, accessoires et fournitures (fabrication)', 'machines textiles, accessoires et fournitures (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(861, 'machines à bois, PVC, alu et outillage (fabrication, négoce)', 'machines à bois, PVC, alu et outillage (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(862, 'machines à café (fabrication, gros)', 'machines à café (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(863, 'machines à coudre, à repasser et à tricoter (fabrication, gros)', 'machines à coudre, à repasser et à tricoter (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(864, 'machines à coudre, à repasser et à tricoter (vente, réparation)', 'machines à coudre, à repasser et à tricoter (vente, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(865, 'machines à traire', 'machines à traire', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(866, 'machines à écrire, machines de bureau (détail, réparation)', 'machines à écrire, machines de bureau (détail, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(867, 'machines-outils (fabrication, négoce)', 'machines-outils (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(868, 'machines-outils (révision, reconstruction)', 'machines-outils (révision, reconstruction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(869, 'maillots de bain et vêtements de plage (fabrication)', 'maillots de bain et vêtements de plage (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(870, 'maintenance industrielle', 'maintenance industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(871, 'mairies', 'mairies', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(872, 'maisons de quartier, maisons des jeunes', 'maisons de quartier, maisons des jeunes', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(873, 'maisons de retraite établissements privés', 'maisons de retraite établissements privés', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(874, 'maisons de retraite établissements publics', 'maisons de retraite établissements publics', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(875, 'maisons et homes d''enfants', 'maisons et homes d''enfants', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(876, 'maisons familiales rurales', 'maisons familiales rurales', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(877, 'maisons, appartements et chambres meublés (location)', 'maisons, appartements et chambres meublés (location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(878, 'mandataires judiciaires à la liquidation des entreprises', 'mandataires judiciaires à la liquidation des entreprises', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(879, 'mandataires spécialisés près le tribunal de commerce', 'mandataires spécialisés près le tribunal de commerce', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(880, 'manipulateurs, robots de manutention', 'manipulateurs, robots de manutention', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(881, 'mannequins (agences)', 'mannequins (agences)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(882, 'mannequins d''étalages (fabrication, location, vente)', 'mannequins d''étalages (fabrication, location, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(883, 'manucures', 'manucures', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(884, 'manutention : matériel', 'manutention : matériel', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(885, 'manutention continue : systèmes', 'manutention continue : systèmes', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(886, 'manutention et levage (entreprises)', 'manutention et levage (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(887, 'manutention portuaire', 'manutention portuaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(888, 'manutention, stockage : accessoires', 'manutention, stockage : accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(889, 'maquettes professionnelles, prototypes (conception, réalisation)', 'maquettes professionnelles, prototypes (conception, réalisation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:49', 0, 1, 0, 0),
(890, 'maraîchers', 'maraîchers', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(891, 'marbres, granits et pierres naturelles', 'marbres, granits et pierres naturelles', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(892, 'marbriers de bâtiment et de décoration', 'marbriers de bâtiment et de décoration', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(893, 'marbriers funéraires', 'marbriers funéraires', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(894, 'marchands de biens', 'marchands de biens', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(895, 'mareyeurs', 'mareyeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(896, 'marine : équipement', 'marine : équipement', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(897, 'marionnettes et spectacles pour enfants', 'marionnettes et spectacles pour enfants', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(898, 'marketing (conseils)', 'marketing (conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(899, 'marketing direct (agences et conseils)', 'marketing direct (agences et conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(900, 'marketing téléphonique', 'marketing téléphonique', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(901, 'maroquinerie et articles de voyage (détail)', 'maroquinerie et articles de voyage (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(902, 'maroquinerie et articles de voyage (fabrication)', 'maroquinerie et articles de voyage (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(903, 'maroquinerie et articles de voyage (gros)', 'maroquinerie et articles de voyage (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(904, 'maroquinerie et articles de voyage : accessoires pour (fabrication, gros)', 'maroquinerie et articles de voyage : accessoires pour (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(905, 'marquage industriel', 'marquage industriel', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(906, 'matelas (réfection)', 'matelas (réfection)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(907, 'maternités', 'maternités', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(908, 'matières plastiques : matières premières (fabrication, négoce)', 'matières plastiques : matières premières (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(909, 'matières plastiques : matériel, équipements industriels', 'matières plastiques : matériel, équipements industriels', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(910, 'matières plastiques : produits, demi-produits', 'matières plastiques : produits, demi-produits', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(911, 'matières textiles (import, export, négoce)', 'matières textiles (import, export, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(912, 'matériaux composites (fabrication, gros)', 'matériaux composites (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(913, 'matériaux de construction (fabrication)', 'matériaux de construction (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(914, 'matériaux de construction (négoce)', 'matériaux de construction (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(915, 'matériaux de viabilité (fabrication, exploitation)', 'matériaux de viabilité (fabrication, exploitation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(916, 'matériel agricole', 'matériel agricole', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(917, 'matériel d''occasion pour particuliers (achat, revente)', 'matériel d''occasion pour particuliers (achat, revente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(918, 'matériel d''occasion pour professionnels', 'matériel d''occasion pour professionnels', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(919, 'matériel ferroviaire fixe', 'matériel ferroviaire fixe', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(920, 'matériel ferroviaire roulant', 'matériel ferroviaire roulant', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(921, 'matériel hydraulique', 'matériel hydraulique', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(922, 'matériel médico-chirurgical (fabrication, installation)', 'matériel médico-chirurgical (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(923, 'matériel médico-chirurgical (vente, location, réparation)', 'matériel médico-chirurgical (vente, location, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(924, 'matériel pour forains', 'matériel pour forains', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(925, 'matériel pour travaux publics et maçonnerie', 'matériel pour travaux publics et maçonnerie', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(926, 'matériel scolaire, pédagogique, psychotechnique', 'matériel scolaire, pédagogique, psychotechnique', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(927, 'matériel thermique, aéraulique et frigorifique (fabrication, gros)', 'matériel thermique, aéraulique et frigorifique (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(928, 'maçonnerie (entreprises)', 'maçonnerie (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(929, 'maîtres d''oeuvre en bâtiment', 'maîtres d''oeuvre en bâtiment', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(930, 'menuiserie (entreprises)', 'menuiserie (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(931, 'menuiserie PVC (entreprises)', 'menuiserie PVC (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(932, 'menuiserie industrielle', 'menuiserie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(933, 'menuiserie métallique (entreprises)', 'menuiserie métallique (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(934, 'mercerie (fabrication, gros)', 'mercerie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(935, 'mercerie, bonneterie (détail)', 'mercerie, bonneterie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(936, 'mesure, contrôle, régulation et détection : appareils', 'mesure, contrôle, régulation et détection : appareils', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(937, 'metteurs en scène et scénaristes', 'metteurs en scène et scénaristes', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(938, 'meubles d''enfants (fabrication, commerce)', 'meubles d''enfants (fabrication, commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(939, 'meubles de style et contemporains (commerce)', 'meubles de style et contemporains (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(940, 'meubles de style et contemporains (fabrication)', 'meubles de style et contemporains (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:50', 0, 1, 0, 0),
(941, 'meubles en rotin (fabrication, commerce)', 'meubles en rotin (fabrication, commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(942, 'meubles et accessoires pour cuisines et salles de bains (détail)', 'meubles et accessoires pour cuisines et salles de bains (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(943, 'meubles et décoration de jardins (fabrication, commerce)', 'meubles et décoration de jardins (fabrication, commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(944, 'micro-informatique (vente, maintenance)', 'micro-informatique (vente, maintenance)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(945, 'micro-édition, PAO (travaux)', 'micro-édition, PAO (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(946, 'micrographie, numérisation', 'micrographie, numérisation', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(947, 'minerais et minéraux (gros)', 'minerais et minéraux (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(948, 'mines (exploitation)', 'mines (exploitation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(949, 'mines et carrières : matériel et fournitures', 'mines et carrières : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(950, 'minoteries, meuneries', 'minoteries, meuneries', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(951, 'minéraux, fossiles et coquillages (commerce)', 'minéraux, fossiles et coquillages (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(952, 'miroiterie (entreprises)', 'miroiterie (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(953, 'miroiterie d''art', 'miroiterie d''art', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(954, 'mobilier de bureau (commerce)', 'mobilier de bureau (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(955, 'mobilier de bureau (fabrication)', 'mobilier de bureau (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(956, 'mobilier médical et chirurgical', 'mobilier médical et chirurgical', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(957, 'mobilier métallique (fabrication)', 'mobilier métallique (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(958, 'mobilier urbain', 'mobilier urbain', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(959, 'modelages', 'modelages', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(960, 'modeleurs-mécaniciens', 'modeleurs-mécaniciens', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(961, 'modistes', 'modistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(962, 'modèles réduits : loisirs (détail)', 'modèles réduits : loisirs (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(963, 'monnaies, médailles', 'monnaies, médailles', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(964, 'montgolfières et dirigeables (pratique de)', 'montgolfières et dirigeables (pratique de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(965, 'montres (fabrication)', 'montres (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(966, 'monuments, articles funéraires (fabrication)', 'monuments, articles funéraires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(967, 'moquettes, tapis (détail)', 'moquettes, tapis (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(968, 'moteurs industriels (entretien, réparation)', 'moteurs industriels (entretien, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(969, 'moteurs marins et hors-bord (distribution, entretien)', 'moteurs marins et hors-bord (distribution, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(970, 'moteurs thermiques (fabrication)', 'moteurs thermiques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(971, 'moteurs électriques (fabrication)', 'moteurs électriques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(972, 'motoculture de plaisance', 'motoculture de plaisance', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(973, 'motos, scooters (agents, concessionnaires)', 'motos, scooters (agents, concessionnaires)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(974, 'motos, scooters, cycles (commerce, réparation)', 'motos, scooters, cycles (commerce, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(975, 'moules et modèles (fabrication)', 'moules et modèles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(976, 'moulures et profilés pour bâtiment et décoration (fabrication)', 'moulures et profilés pour bâtiment et décoration (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:51', 0, 1, 0, 0),
(977, 'mousse synthétique (détail)', 'mousse synthétique (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(978, 'musiciens interprètes', 'musiciens interprètes', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(979, 'musique (édition phonographique)', 'musique (édition phonographique)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(980, 'musique : instruments et accessoires (fabrication)', 'musique : instruments et accessoires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(981, 'musique : instruments et accessoires (réparation, entretien)', 'musique : instruments et accessoires (réparation, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(982, 'musique : instruments et accessoires (vente, location)', 'musique : instruments et accessoires (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(983, 'musique : partitions (édition, vente)', 'musique : partitions (édition, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(984, 'musique et chant (leçons)', 'musique et chant (leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(985, 'musées', 'musées', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(986, 'mutuelles d''assurances', 'mutuelles d''assurances', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(987, 'mutuelles, unions de mutuelles', 'mutuelles, unions de mutuelles', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(988, 'mytiliculture', 'mytiliculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(989, 'mâts et pylônes (fabrication, installation)', 'mâts et pylônes (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(990, 'mécanique agricole', 'mécanique agricole', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(991, 'mécanique et outillage de précision', 'mécanique et outillage de précision', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(992, 'mécanique générale', 'mécanique générale', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(993, 'médecine du travail, santé au travail', 'médecine du travail, santé au travail', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(994, 'médecins : allergologie', 'médecins : allergologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(995, 'médecins : anatomie et cytologie pathologiques', 'médecins : anatomie et cytologie pathologiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(996, 'médecins : andrologie', 'médecins : andrologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(997, 'médecins : anesthésiologie et réanimation chirurgicale', 'médecins : anesthésiologie et réanimation chirurgicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(998, 'médecins : angéiologie, médecine vasculaire', 'médecins : angéiologie, médecine vasculaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(999, 'médecins : biologie médicale', 'médecins : biologie médicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1000, 'médecins : cancérologie', 'médecins : cancérologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1001, 'médecins : cardiologie, maladies vasculaires', 'médecins : cardiologie, maladies vasculaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1002, 'médecins : chirurgie cardio-vasculaire et thoracique', 'médecins : chirurgie cardio-vasculaire et thoracique', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1003, 'médecins : chirurgie de la face et du cou', 'médecins : chirurgie de la face et du cou', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1004, 'médecins : chirurgie de la main', 'médecins : chirurgie de la main', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1005, 'médecins : chirurgie générale', 'médecins : chirurgie générale', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1006, 'médecins : chirurgie infantile', 'médecins : chirurgie infantile', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1007, 'médecins : chirurgie maxillo-faciale', 'médecins : chirurgie maxillo-faciale', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1008, 'médecins : chirurgie maxillo-faciale et stomatologie', 'médecins : chirurgie maxillo-faciale et stomatologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1009, 'médecins : chirurgie orthopédique et traumatologie', 'médecins : chirurgie orthopédique et traumatologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1010, 'médecins : chirurgie plastique reconstructrice et esthétique', 'médecins : chirurgie plastique reconstructrice et esthétique', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1011, 'médecins : chirurgie urologique', 'médecins : chirurgie urologique', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1012, 'médecins : chirurgie vasculaire', 'médecins : chirurgie vasculaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1013, 'médecins : chirurgie viscérale et digestive', 'médecins : chirurgie viscérale et digestive', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1014, 'médecins : dermatologie et vénéréologie', 'médecins : dermatologie et vénéréologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:52', 0, 1, 0, 0),
(1015, 'médecins : diabétologie nutrition', 'médecins : diabétologie nutrition', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1016, 'médecins : endocrinologie,diabète et maladies métaboliques', 'médecins : endocrinologie,diabète et maladies métaboliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1017, 'médecins : gastro-entérologie et hépatologie (appareil digestif)', 'médecins : gastro-entérologie et hépatologie (appareil digestif)', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1018, 'médecins : gynécologie médicale, obstétrique', 'médecins : gynécologie médicale, obstétrique', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1019, 'médecins : gynécologie-obstétrique', 'médecins : gynécologie-obstétrique', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1020, 'médecins : génétique médicale', 'médecins : génétique médicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1021, 'médecins : gériatrie, gérontologie (maladies des personnes âgées)', 'médecins : gériatrie, gérontologie (maladies des personnes âgées)', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1022, 'médecins : maladies du sang (hématologie)', 'médecins : maladies du sang (hématologie)', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1023, 'médecins : médecine aéronautique et spatiale', 'médecins : médecine aéronautique et spatiale', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1024, 'médecins : médecine de la douleur et médecine palliative', 'médecins : médecine de la douleur et médecine palliative', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1025, 'médecins : médecine de la reproduction', 'médecins : médecine de la reproduction', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1026, 'médecins : médecine et biologie du sport', 'médecins : médecine et biologie du sport', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1027, 'médecins : médecine générale', 'médecins : médecine générale', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1028, 'médecins : médecine générale orientation acupuncture', 'médecins : médecine générale orientation acupuncture', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1029, 'médecins : médecine générale orientation homéopathie', 'médecins : médecine générale orientation homéopathie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1030, 'médecins : médecine interne', 'médecins : médecine interne', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1031, 'médecins : médecine légale, pratique médico-judiciaire', 'médecins : médecine légale, pratique médico-judiciaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1032, 'médecins : médecine manuelle - ostéopathie', 'médecins : médecine manuelle - ostéopathie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1033, 'médecins : médecine nucléaire', 'médecins : médecine nucléaire', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1034, 'médecins : médecine physique et de réadaptation', 'médecins : médecine physique et de réadaptation', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1035, 'médecins : médecine thermale (hydrologie et climatologie médicales)', 'médecins : médecine thermale (hydrologie et climatologie médicales)', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1036, 'médecins : médecine tropicale, pathologie infectieuse et tropicale', 'médecins : médecine tropicale, pathologie infectieuse et tropicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1037, 'médecins : mésothérapie', 'médecins : mésothérapie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1038, 'médecins : neurochirurgie', 'médecins : neurochirurgie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1039, 'médecins : neurologie', 'médecins : neurologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1040, 'médecins : neuropsychiatrie', 'médecins : neuropsychiatrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1041, 'médecins : nutrition', 'médecins : nutrition', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1042, 'médecins : néphrologie', 'médecins : néphrologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1043, 'médecins : oncologie', 'médecins : oncologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1044, 'médecins : ophtalmologie', 'médecins : ophtalmologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1045, 'médecins : orthopédie dento-maxillo-faciale', 'médecins : orthopédie dento-maxillo-faciale', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1046, 'médecins : oto-rhino-laryngologie et chirurgie cervico-faciale', 'médecins : oto-rhino-laryngologie et chirurgie cervico-faciale', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1047, 'médecins : pathologie du sommeil et de la vigilance', 'médecins : pathologie du sommeil et de la vigilance', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1048, 'médecins : phoniatrie', 'médecins : phoniatrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1049, 'médecins : pneumologie', 'médecins : pneumologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1050, 'médecins : psychiatrie', 'médecins : psychiatrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1051, 'médecins : psychiatrie de l''enfant et de l''adolescent', 'médecins : psychiatrie de l''enfant et de l''adolescent', 0, 'default.png', 0, 1, '2017-10-24 06:46:53', 0, 1, 0, 0),
(1052, 'médecins : pédiatrie maladies des enfants', 'médecins : pédiatrie maladies des enfants', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0);
INSERT INTO `otd_business_category` (`cat_id`, `cat_name`, `cat_description`, `cat_parent`, `cat_img_path`, `cat_homedisplay`, `cat_status`, `cat_date_created`, `cat_by_user`, `type`, `cat_hometabdisplay`, `cat_hometab_id`) VALUES
(1053, 'médecins : radiologie (radiodiagnostic et imagerie médicale)', 'médecins : radiologie (radiodiagnostic et imagerie médicale)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1054, 'médecins : radiothérapie (oncologie option radiothérapie)', 'médecins : radiothérapie (oncologie option radiothérapie)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1055, 'médecins : reproduction et gynécologie médicale', 'médecins : reproduction et gynécologie médicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1056, 'médecins : rhumatologie', 'médecins : rhumatologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1057, 'médecins : réanimation médicale', 'médecins : réanimation médicale', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1058, 'médecins : santé publique et médecine sociale', 'médecins : santé publique et médecine sociale', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1059, 'médecins : sexologie', 'médecins : sexologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1060, 'médecins : stomatologie', 'médecins : stomatologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1061, 'médecins : tabacologie et aide au sevrage', 'médecins : tabacologie et aide au sevrage', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1062, 'médecins : toxicomanie, alcoologie, addictologie', 'médecins : toxicomanie, alcoologie, addictologie', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1063, 'médecins : échographie', 'médecins : échographie', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1064, 'mélangeurs et malaxeurs (fabrication)', 'mélangeurs et malaxeurs (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1065, 'métallisation', 'métallisation', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1066, 'métallurgie', 'métallurgie', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1067, 'métaux non ferreux et alliages (production, transformation, négoce)', 'métaux non ferreux et alliages (production, transformation, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1068, 'métaux précieux et alliages (production, transformation, négoce)', 'métaux précieux et alliages (production, transformation, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1069, 'météorologie (organismes)', 'météorologie (organismes)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1070, 'naturalistes-taxidermistes, entomologistes', 'naturalistes-taxidermistes, entomologistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1071, 'nettoyage (entreprises)', 'nettoyage (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1072, 'nettoyage industriel : matériel', 'nettoyage industriel : matériel', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1073, 'non-tissés, ouates, produits textiles divers (fabrication)', 'non-tissés, ouates, produits textiles divers (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1074, 'notaires', 'notaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1075, 'objets d''art et de collection (achat et vente)', 'objets d''art et de collection (achat et vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1076, 'oenologie (laboratoires et conseils)', 'oenologie (laboratoires et conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1077, 'oeufs (production)', 'oeufs (production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1078, 'offices de tourisme, syndicats d''initiative', 'offices de tourisme, syndicats d''initiative', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1079, 'opticiens', 'opticiens', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1080, 'optique : instruments (fabrication)', 'optique : instruments (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1081, 'orchestres classiques, sociétés de musique, choeurs et chorales', 'orchestres classiques, sociétés de musique, choeurs et chorales', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1082, 'orchestres de variétés', 'orchestres de variétés', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1083, 'orfèvrerie (détail)', 'orfèvrerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1084, 'orfèvrerie (fabrication, gros)', 'orfèvrerie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1085, 'orfèvrerie : dorure, argenture', 'orfèvrerie : dorure, argenture', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1086, 'organisations internationales', 'organisations internationales', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1087, 'organismes d''intervention agricole', 'organismes d''intervention agricole', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1088, 'organismes de développement économique', 'organismes de développement économique', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1089, 'orientation et information scolaires et professionnelles', 'orientation et information scolaires et professionnelles', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1090, 'orthophonistes', 'orthophonistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1091, 'orthoptistes', 'orthoptistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1092, 'orthopédie générale', 'orthopédie générale', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1093, 'ostréiculture', 'ostréiculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:54', 0, 1, 0, 0),
(1094, 'ostréiculture : matériel et fournitures', 'ostréiculture : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1095, 'ostéopathes non-médecins', 'ostéopathes non-médecins', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1096, 'outillage mécanique', 'outillage mécanique', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1097, 'outillage pneumatique', 'outillage pneumatique', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1098, 'outillage pour le bâtiment (fabrication, gros)', 'outillage pour le bâtiment (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1099, 'outillage électroportatif (fabrication, gros)', 'outillage électroportatif (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1100, 'outils de découpage et d''emboutissage', 'outils de découpage et d''emboutissage', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1101, 'outils à main (fabrication, gros)', 'outils à main (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1102, 'oxygénothérapie', 'oxygénothérapie', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1103, 'palettes de manutention', 'palettes de manutention', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1104, 'panneaux de bois : contreplaqués, fibres, particules, stratifiés (fabrication, vente)', 'panneaux de bois : contreplaqués, fibres, particules, stratifiés (fabrication, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1105, 'papeteries (détail)', 'papeteries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1106, 'papeteries : articles (fabrication)', 'papeteries : articles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1107, 'papeteries : articles (gros)', 'papeteries : articles (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1108, 'papier, carton : matériel et fournitures pour la fabrication et la transformation', 'papier, carton : matériel et fournitures pour la fabrication et la transformation', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1109, 'papiers (gros)', 'papiers (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1110, 'papiers d''emballage (fabrication)', 'papiers d''emballage (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1111, 'papiers peints (détail)', 'papiers peints (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1112, 'papiers peints (fabrication)', 'papiers peints (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1113, 'papiers peints (gros)', 'papiers peints (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1114, 'papiers spéciaux et à usages techniques (fabrication)', 'papiers spéciaux et à usages techniques (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1115, 'papiers à usage graphique (fabrication)', 'papiers à usage graphique (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1116, 'papiers à usage sanitaire et domestique (fabrication)', 'papiers à usage sanitaire et domestique (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1117, 'parachutisme', 'parachutisme', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1118, 'parafoudres, paratonnerres', 'parafoudres, paratonnerres', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1119, 'parapharmacies', 'parapharmacies', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1120, 'parapluies, ombrelles et cannes (fabrication)', 'parapluies, ombrelles et cannes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1121, 'parcs animaliers, parcs zoologiques', 'parcs animaliers, parcs zoologiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1122, 'parcs d''attractions et de loisirs', 'parcs d''attractions et de loisirs', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1123, 'pare-brise, toits ouvrants (vente, pose, réparation)', 'pare-brise, toits ouvrants (vente, pose, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1124, 'parfumerie (détail)', 'parfumerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1125, 'parfumerie : matières premières et essences', 'parfumerie : matières premières et essences', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1126, 'parfumerie et cosmétiques (fabrication, gros)', 'parfumerie et cosmétiques (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1127, 'parkings (exploitation)', 'parkings (exploitation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1128, 'parkings : équipements de', 'parkings : équipements de', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1129, 'parquets (fabrication)', 'parquets (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1130, 'parquets (pose, entretien, vitrification)', 'parquets (pose, entretien, vitrification)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1131, 'partis politiques', 'partis politiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1132, 'patinoires', 'patinoires', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1133, 'pavage (entreprises)', 'pavage (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1134, 'paysagistes (entrepreneurs)', 'paysagistes (entrepreneurs)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1135, 'paysagistes conseils', 'paysagistes conseils', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1136, 'peaux (traitement, transformation)', 'peaux (traitement, transformation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1137, 'peintres en lettres', 'peintres en lettres', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1138, 'peintres illustrateurs', 'peintres illustrateurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1139, 'peinture : fournitures pour entreprises', 'peinture : fournitures pour entreprises', 0, 'default.png', 0, 1, '2017-10-24 06:46:55', 0, 1, 0, 0),
(1140, 'peinture : machines (installation)', 'peinture : machines (installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1141, 'peinture et vernis (détail)', 'peinture et vernis (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1142, 'peinture, revêtements (entreprises)', 'peinture, revêtements (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1143, 'peinture, vernis, produits assimilés (fabrication, gros)', 'peinture, vernis, produits assimilés (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1144, 'pensions de famille', 'pensions de famille', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1145, 'pensions pour chiens, chats', 'pensions pour chiens, chats', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1146, 'perles (fabrication, commerce)', 'perles (fabrication, commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1147, 'permanence téléphonique', 'permanence téléphonique', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1148, 'perruques et postiches', 'perruques et postiches', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1149, 'pharmacies', 'pharmacies', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1150, 'pharmacies (répartition et gros)', 'pharmacies (répartition et gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1151, 'pharmacies : accessoires et fournitures pour', 'pharmacies : accessoires et fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1152, 'philatélie', 'philatélie', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1153, 'photocopie, reprographie (travaux)', 'photocopie, reprographie (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1154, 'photocopieurs, reprographie : matériel et fournitures', 'photocopieurs, reprographie : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1155, 'photogrammétrie', 'photogrammétrie', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1156, 'photographes d''art et de portrait', 'photographes d''art et de portrait', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1157, 'photographes de reportage', 'photographes de reportage', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1158, 'photographie (développement et tirage express)', 'photographie (développement et tirage express)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1159, 'photographie aérienne', 'photographie aérienne', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1160, 'photographie et cinéma : appareils, films et accessoires (détail)', 'photographie et cinéma : appareils, films et accessoires (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1161, 'photographie et cinéma : appareils, films et accessoires (fabrication, gros)', 'photographie et cinéma : appareils, films et accessoires (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1162, 'photographie et cinéma : matériel (location)', 'photographie et cinéma : matériel (location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1163, 'photographie et cinéma : matériel (réparation)', 'photographie et cinéma : matériel (réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1164, 'photographie publicitaire, industrielle, d''illustration', 'photographie publicitaire, industrielle, d''illustration', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1165, 'photographie, vidéo, cinéma (laboratoires, travaux)', 'photographie, vidéo, cinéma (laboratoires, travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1166, 'photothèques', 'photothèques', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1167, 'pianos (accordeurs, réparateurs)', 'pianos (accordeurs, réparateurs)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1168, 'pianos (vente, location)', 'pianos (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1169, 'pipelines (exploitation, entretien, construction)', 'pipelines (exploitation, entretien, construction)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1170, 'pisciculture', 'pisciculture', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1171, 'piscines (construction, entretien)', 'piscines (construction, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1172, 'piscines (établissements)', 'piscines (établissements)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1173, 'piscines : matériel, fournitures (détail)', 'piscines : matériel, fournitures (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1174, 'piscines : équipements, matériel (fabrication, gros)', 'piscines : équipements, matériel (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:56', 0, 1, 0, 0),
(1175, 'pizzerias', 'pizzerias', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1176, 'placards (fabrication, installation)', 'placards (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1177, 'plafonds, faux plafonds (vente et pose)', 'plafonds, faux plafonds (vente et pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1178, 'plage : articles (détail)', 'plage : articles (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1179, 'plagistes', 'plagistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1180, 'planches à voile et accessoires (vente)', 'planches à voile et accessoires (vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1181, 'planification familiale (centres, information)', 'planification familiale (centres, information)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1182, 'plans (tirage de)', 'plans (tirage de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1183, 'plaques de plâtre (fournitures et pose)', 'plaques de plâtre (fournitures et pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1184, 'plaques professionnelles et de signalisation (fabrication, pose)', 'plaques professionnelles et de signalisation (fabrication, pose)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1185, 'plastification, collage, pelliculage (travaux)', 'plastification, collage, pelliculage (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1186, 'plates-formes et nacelles élévatrices (vente, location)', 'plates-formes et nacelles élévatrices (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1187, 'plats préparés réfrigérés (fabrication, gros)', 'plats préparés réfrigérés (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1188, 'plomberie : outillage et fournitures pour', 'plomberie : outillage et fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1189, 'plombiers', 'plombiers', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1190, 'plongée sous-marine, sports et loisirs subaquatiques (pratique de)', 'plongée sous-marine, sports et loisirs subaquatiques (pratique de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1191, 'plumes et duvets (gros)', 'plumes et duvets (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1192, 'plâtre et produits en plâtre (fabrication, gros)', 'plâtre et produits en plâtre (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1193, 'plâtrerie (entreprises)', 'plâtrerie (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1194, 'pneus (fabrication, gros)', 'pneus (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1195, 'pneus (rechapage, recreusage, réparation)', 'pneus (rechapage, recreusage, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1196, 'pneus (vente, montage)', 'pneus (vente, montage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1197, 'podo-orthésistes', 'podo-orthésistes', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1198, 'podologues : pédicures-podologues', 'podologues : pédicures-podologues', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1199, 'pointage, gestion du temps : systèmes', 'pointage, gestion du temps : systèmes', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1200, 'poissonneries (détail)', 'poissonneries (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1201, 'poissonneries (gros)', 'poissonneries (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1202, 'pommes de terre (gros)', 'pommes de terre (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1203, 'pompes (fabrication, installation, réparation)', 'pompes (fabrication, installation, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1204, 'pompes funèbres, inhumations et crémations', 'pompes funèbres, inhumations et crémations', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1205, 'pompes funèbres, inhumations et crémations : fournitures pour', 'pompes funèbres, inhumations et crémations : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1206, 'pompes à chaleur (fabrication)', 'pompes à chaleur (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1207, 'pompes à vide', 'pompes à vide', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1208, 'ponts roulants et portiques', 'ponts roulants et portiques', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1209, 'ponçage de marbre', 'ponçage de marbre', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1210, 'porcelaines, faïences (fabrication, gros)', 'porcelaines, faïences (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1211, 'portes automatiques, portes de garage', 'portes automatiques, portes de garage', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1212, 'portes et portails', 'portes et portails', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1213, 'portes industrielles', 'portes industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1214, 'portiers électroniques et contrôle d''accès (installation, entretien)', 'portiers électroniques et contrôle d''accès (installation, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1215, 'ports de plaisance', 'ports de plaisance', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1216, 'ports fluviaux et maritimes', 'ports fluviaux et maritimes', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1217, 'post-presse, finition, façonnage (travaux)', 'post-presse, finition, façonnage (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1218, 'poterie', 'poterie', 0, 'default.png', 0, 1, '2017-10-24 06:46:57', 0, 1, 0, 0),
(1219, 'poupées (fabrication, vente, réparation)', 'poupées (fabrication, vente, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1220, 'poêles (fabrication, installation)', 'poêles (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1221, 'presses à former, découper, emboutir', 'presses à former, découper, emboutir', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1222, 'pressings', 'pressings', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1223, 'produits chimiques (fabrication, gros)', 'produits chimiques (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1224, 'produits d''entretien (fabrication, distribution)', 'produits d''entretien (fabrication, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1225, 'produits de base pharmaceutiques (fabrication, vente)', 'produits de base pharmaceutiques (fabrication, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1226, 'produits de l''agriculture biologique (production, vente)', 'produits de l''agriculture biologique (production, vente)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1227, 'produits fermiers (vente directe)', 'produits fermiers (vente directe)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1228, 'produits laitiers et avicoles (gros)', 'produits laitiers et avicoles (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1229, 'produits laitiers et dérivés (fabrication)', 'produits laitiers et dérivés (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1230, 'produits phytosanitaires (négoce, application)', 'produits phytosanitaires (négoce, application)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1231, 'produits réfractaires (fabrication, gros)', 'produits réfractaires (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1232, 'promoteurs constructeurs', 'promoteurs constructeurs', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1233, 'promotion (agences et conseils)', 'promotion (agences et conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1234, 'propriété industrielle : brevets d''invention, marques, modèles, dessins', 'propriété industrielle : brevets d''invention, marques, modèles, dessins', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1235, 'propriété littéraire et artistique : gestion des droits d''auteur et droits voisins', 'propriété littéraire et artistique : gestion des droits d''auteur et droits voisins', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1236, 'protection contre l''incendie : matériel, installation, maintenance', 'protection contre l''incendie : matériel, installation, maintenance', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1237, 'protection contre le vol (marquage)', 'protection contre le vol (marquage)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1238, 'protection contre le vol (matériel et installation)', 'protection contre le vol (matériel et installation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1239, 'prothèses : fournitures pour', 'prothèses : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1240, 'prothèses oculaires (fabrication)', 'prothèses oculaires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1241, 'prothésistes dentaires', 'prothésistes dentaires', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1242, 'pruneaux (production, transformation)', 'pruneaux (production, transformation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1243, 'pré-presse, photogravure (travaux)', 'pré-presse, photogravure (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1244, 'préfecture, sous-préfectures', 'préfecture, sous-préfectures', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1245, 'prévention, traitement des pollutions', 'prévention, traitement des pollutions', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1246, 'prêt-à-porter et tailleurs : fournitures pour', 'prêt-à-porter et tailleurs : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:58', 0, 1, 0, 0),
(1247, 'psychanalystes', 'psychanalystes', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1248, 'psychologues', 'psychologues', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1249, 'psychomotriciens', 'psychomotriciens', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1250, 'psychothérapeutes', 'psychothérapeutes', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1251, 'publicité (agences et conseils)', 'publicité (agences et conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1252, 'publicité (peinture et décoration)', 'publicité (peinture et décoration)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1253, 'publicité (studios de création)', 'publicité (studios de création)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1254, 'publicité (supports et régies)', 'publicité (supports et régies)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1255, 'publicité (édition)', 'publicité (édition)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1256, 'publicité aérienne', 'publicité aérienne', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1257, 'publicité par l''objet', 'publicité par l''objet', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1258, 'publicité sur le lieu de vente : supports (création, fabrication)', 'publicité sur le lieu de vente : supports (création, fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1259, 'pulvérisateurs (fabrication, gros)', 'pulvérisateurs (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1260, 'puériculture : articles (fabrication, gros)', 'puériculture : articles (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1261, 'pyrotechnie (spectacles)', 'pyrotechnie (spectacles)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1262, 'pâtes alimentaires (fabrication)', 'pâtes alimentaires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1263, 'pâtes à papiers (fabrication, importation)', 'pâtes à papiers (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1264, 'pâtisseries', 'pâtisseries', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1265, 'pâtisseries, confiseries, chocolateries : fournitures pour', 'pâtisseries, confiseries, chocolateries : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1266, 'pépiniéristes', 'pépiniéristes', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1267, 'pépiniéristes viticoles', 'pépiniéristes viticoles', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1268, 'pétrole (exploration, production)', 'pétrole (exploration, production)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1269, 'pétrole (raffineries)', 'pétrole (raffineries)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1270, 'pétrole et dérivés (distribution)', 'pétrole et dérivés (distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1271, 'pêche et chasse (pratique de)', 'pêche et chasse (pratique de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1272, 'pêche et chasse : articles (détail)', 'pêche et chasse : articles (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1273, 'pêche et chasse : articles (fabrication, gros)', 'pêche et chasse : articles (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1274, 'pêche et pisciculture : matériel et fournitures', 'pêche et pisciculture : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1275, 'pêcheries', 'pêcheries', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1276, 'quincaillerie (détail)', 'quincaillerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1277, 'quincaillerie (fabrication)', 'quincaillerie (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1278, 'quincaillerie (gros)', 'quincaillerie (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1279, 'quincaillerie de bâtiment et d''ameublement', 'quincaillerie de bâtiment et d''ameublement', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1280, 'raccords', 'raccords', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1281, 'radiateurs pour véhicules (vente, pose, réparation)', 'radiateurs pour véhicules (vente, pose, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1282, 'radiesthésie', 'radiesthésie', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1283, 'radio : stations', 'radio : stations', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1284, 'radio et télévision : équipement et matériel d''émission', 'radio et télévision : équipement et matériel d''émission', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1285, 'radio, TV, électronique grand public : matériel (fabrication)', 'radio, TV, électronique grand public : matériel (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1286, 'radio, TV, électronique grand public : matériel (gros)', 'radio, TV, électronique grand public : matériel (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1287, 'radiocommunications (installateurs)', 'radiocommunications (installateurs)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1288, 'radiocommunications : matériel (fabrication, importation)', 'radiocommunications : matériel (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1289, 'radioélectricité : appareils (vente, réparation)', 'radioélectricité : appareils (vente, réparation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1290, 'ramonage', 'ramonage', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1291, 'randonnée (organisation)', 'randonnée (organisation)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1292, 'ravalement (travaux de)', 'ravalement (travaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1293, 'rayonnages métalliques', 'rayonnages métalliques', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1294, 'recherches scientifiques et techniques (organismes)', 'recherches scientifiques et techniques (organismes)', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1295, 'recouvrements commerciaux et autres', 'recouvrements commerciaux et autres', 0, 'default.png', 0, 1, '2017-10-24 06:46:59', 0, 1, 0, 0),
(1296, 'recrutement (cabinets, conseils)', 'recrutement (cabinets, conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1297, 'rectification, usinage, fraisage', 'rectification, usinage, fraisage', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1298, 'refuges de montagne', 'refuges de montagne', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1299, 'relations publiques (agences)', 'relations publiques (agences)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1300, 'relaxation', 'relaxation', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1301, 'reliure, dorure artisanale', 'reliure, dorure artisanale', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1302, 'remorques de tourisme et attelages (vente, location)', 'remorques de tourisme et attelages (vente, location)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1303, 'renseignements financiers et commerciaux', 'renseignements financiers et commerciaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1304, 'repassage', 'repassage', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1305, 'repoussage sur métaux', 'repoussage sur métaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1306, 'représentants', 'représentants', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1307, 'ressorts (fabrication)', 'ressorts (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1308, 'restaurants', 'restaurants', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1309, 'restaurants : fournitures', 'restaurants : fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1310, 'restaurants : restauration rapide et libre-service', 'restaurants : restauration rapide et libre-service', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1311, 'restaurants d''entreprises et de collectivités', 'restaurants d''entreprises et de collectivités', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1312, 'restauration collective : produits frais, alimentation générale (gros)', 'restauration collective : produits frais, alimentation générale (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1313, 'restauration à domicile', 'restauration à domicile', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1314, 'revêtements de sols industriels', 'revêtements de sols industriels', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1315, 'revêtements des sols et des murs, moquettes (vente, pose)', 'revêtements des sols et des murs, moquettes (vente, pose)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1316, 'revêtements industriels', 'revêtements industriels', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1317, 'revêtements non métalliques (émaillage, peinture, plastification)', 'revêtements non métalliques (émaillage, peinture, plastification)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1318, 'revêtements pour sols et murs (gros)', 'revêtements pour sols et murs (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1319, 'revêtements électrolytiques et chimiques', 'revêtements électrolytiques et chimiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1320, 'rhums (importation, distribution)', 'rhums (importation, distribution)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1321, 'rideaux, voilages et tissus d''ameublement (détail)', 'rideaux, voilages et tissus d''ameublement (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1322, 'robes et parures de mariées (confection, détail)', 'robes et parures de mariées (confection, détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1323, 'robinetterie industrielle', 'robinetterie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1324, 'robinetterie sanitaire et de bâtiment (fabrication, gros)', 'robinetterie sanitaire et de bâtiment (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1325, 'roues et roulettes de manutention (fabrication)', 'roues et roulettes de manutention (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1326, 'roulements à billes, aiguilles et rouleaux (fabrication, gros)', 'roulements à billes, aiguilles et rouleaux (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1327, 'routage, messagerie (entreprises)', 'routage, messagerie (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1328, 'réceptions, banquets, séminaires (restaurants pour)', 'réceptions, banquets, séminaires (restaurants pour)', 0, 'default.png', 0, 1, '2017-10-24 06:47:00', 0, 1, 0, 0),
(1329, 'récupération des huiles et graisses industrielles', 'récupération des huiles et graisses industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1330, 'récupération, traitement des déchets : matériel', 'récupération, traitement des déchets : matériel', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1331, 'récupération, traitement des déchets divers', 'récupération, traitement des déchets divers', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1332, 'récupération, traitement des déchets industriels', 'récupération, traitement des déchets industriels', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1333, 'récupération, traitement des fers et métaux', 'récupération, traitement des fers et métaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1334, 'récupération, traitement des matières textiles', 'récupération, traitement des matières textiles', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1335, 'récupération, traitement des papiers et cartons', 'récupération, traitement des papiers et cartons', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1336, 'récupération, traitement des plastiques', 'récupération, traitement des plastiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1337, 'réducteurs de vitesse', 'réducteurs de vitesse', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1338, 'réfrigération et congélation : matériel (fabrication)', 'réfrigération et congélation : matériel (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1339, 'rénovation immobilière', 'rénovation immobilière', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1340, 'réparation et restauration d''objets divers', 'réparation et restauration d''objets divers', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1341, 'réseaux informatiques', 'réseaux informatiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1342, 'résidences avec services', 'résidences avec services', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1343, 'résidences de tourisme, résidences hôtelières', 'résidences de tourisme, résidences hôtelières', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1344, 'sablage, grenaillage, polissage', 'sablage, grenaillage, polissage', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1345, 'sables, cailloux et granulats', 'sables, cailloux et granulats', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1346, 'sacs et conteneurs textiles (fabrication)', 'sacs et conteneurs textiles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1347, 'sages-femmes', 'sages-femmes', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1348, 'salles de bains (installation, agencement)', 'salles de bains (installation, agencement)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1349, 'salles de bains : appareils sanitaires (fabrication)', 'salles de bains : appareils sanitaires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1350, 'salles de bains : meubles et accessoires (fabrication)', 'salles de bains : meubles et accessoires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1351, 'salles de bains : équipements (négoce)', 'salles de bains : équipements (négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1352, 'salons de thé', 'salons de thé', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1353, 'salons et sièges (détail)', 'salons et sièges (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1354, 'sanitaires (installation)', 'sanitaires (installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1355, 'sanitaires (réparation d''émail)', 'sanitaires (réparation d''émail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1356, 'santons, figurines', 'santons, figurines', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1357, 'sapeurs-pompiers', 'sapeurs-pompiers', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1358, 'saunas, hammams (établissements)', 'saunas, hammams (établissements)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1359, 'saunas, hammams, spas (fabrication, installation)', 'saunas, hammams, spas (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1360, 'savons, détergents et lessives (fabrication)', 'savons, détergents et lessives (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1361, 'sciences occultes', 'sciences occultes', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1362, 'scieries de bois', 'scieries de bois', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1363, 'scies (fabrication, gros)', 'scies (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1364, 'sculpteurs et statuaires', 'sculpteurs et statuaires', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1365, 'sculpteurs sur bois, meubles et objets d''art', 'sculpteurs sur bois, meubles et objets d''art', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1366, 'secrétariat (services de)', 'secrétariat (services de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1367, 'sellerie et harnachement (fabrication)', 'sellerie et harnachement (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1368, 'selliers garnisseurs', 'selliers garnisseurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1369, 'sels (gros)', 'sels (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1370, 'serres et châssis de couche', 'serres et châssis de couche', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1371, 'serrurerie (dépannage)', 'serrurerie (dépannage)', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1372, 'serrurerie, métallerie', 'serrurerie, métallerie', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1373, 'services, aides à domicile', 'services, aides à domicile', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1374, 'sexologues', 'sexologues', 0, 'default.png', 0, 1, '2017-10-24 06:47:01', 0, 1, 0, 0),
(1375, 'signalisation intérieure et extérieure (fabrication, installation)', 'signalisation intérieure et extérieure (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1376, 'signalisation routière, urbaine et de chantiers : matériel et systèmes (fabrication)', 'signalisation routière, urbaine et de chantiers : matériel et systèmes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1377, 'silos et magasins à grains', 'silos et magasins à grains', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1378, 'sièges, chaises et fauteuils (fabrication)', 'sièges, chaises et fauteuils (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1379, 'soins des cheveux', 'soins des cheveux', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1380, 'soins et esthétique corporels : matériel', 'soins et esthétique corporels : matériel', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1381, 'soins hors d''un cadre réglementé', 'soins hors d''un cadre réglementé', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1382, 'soins à domicile', 'soins à domicile', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1383, 'sonorisation, éclairage', 'sonorisation, éclairage', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1384, 'sophrologues', 'sophrologues', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1385, 'soudure : matériel et fournitures', 'soudure : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1386, 'soudures (travaux)', 'soudures (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1387, 'spectacles (conception, installation, équipement)', 'spectacles (conception, installation, équipement)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1388, 'spectacles (entrepreneurs et producteurs)', 'spectacles (entrepreneurs et producteurs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1389, 'spiritueux (fabrication, gros)', 'spiritueux (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1390, 'sport (associations et clubs)', 'sport (associations et clubs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1391, 'sport (magasins)', 'sport (magasins)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1392, 'sport (stades et complexes sportifs)', 'sport (stades et complexes sportifs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1393, 'sports d''hiver et de montagne (clubs et associations)', 'sports d''hiver et de montagne (clubs et associations)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1394, 'sports d''hiver et de montagne : équipements et matériel', 'sports d''hiver et de montagne : équipements et matériel', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1395, 'sports et loisirs : articles et équipements (fabrication)', 'sports et loisirs : articles et équipements (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1396, 'sports et loisirs : articles et équipements (gros)', 'sports et loisirs : articles et équipements (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1397, 'sports mécaniques (clubs, circuits, terrains)', 'sports mécaniques (clubs, circuits, terrains)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1398, 'sports nautiques, aquatiques et subaquatiques : articles et équipements (détail)', 'sports nautiques, aquatiques et subaquatiques : articles et équipements (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1399, 'squash (salles et leçons)', 'squash (salles et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1400, 'staff et stuc (entreprises)', 'staff et stuc (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1401, 'stations de sports d''hiver', 'stations de sports d''hiver', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0);
INSERT INTO `otd_business_category` (`cat_id`, `cat_name`, `cat_description`, `cat_parent`, `cat_img_path`, `cat_homedisplay`, `cat_status`, `cat_date_created`, `cat_by_user`, `type`, `cat_hometabdisplay`, `cat_hometab_id`) VALUES
(1402, 'stations-service', 'stations-service', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1403, 'stockage : équipements', 'stockage : équipements', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1404, 'stores', 'stores', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1405, 'stores : fournitures pour', 'stores : fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1406, 'studios de cinéma et de télévision', 'studios de cinéma et de télévision', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1407, 'stylistes de la mode', 'stylistes de la mode', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1408, 'stylos et crayons (fabrication, négoce)', 'stylos et crayons (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1409, 'sténotypie de conférences', 'sténotypie de conférences', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1410, 'sucres (fabrication et raffinage)', 'sucres (fabrication et raffinage)', 0, 'default.png', 0, 1, '2017-10-24 06:47:02', 0, 1, 0, 0),
(1411, 'supermarchés et hypermarchés', 'supermarchés et hypermarchés', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1412, 'surgelés (détail)', 'surgelés (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1413, 'surgelés (fabrication, gros)', 'surgelés (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1414, 'surveillance, gardiennage, protection (entreprises)', 'surveillance, gardiennage, protection (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1415, 'syndicats de salariés', 'syndicats de salariés', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1416, 'syndicats et ordres professionnels', 'syndicats et ordres professionnels', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1417, 'sécurité du travail et prévention des risques professionnels', 'sécurité du travail et prévention des risques professionnels', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1418, 'sécurité sociale', 'sécurité sociale', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1419, 'séjours linguistiques (organisation)', 'séjours linguistiques (organisation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1420, 'séminaires, congrès (organisation)', 'séminaires, congrès (organisation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1421, 'sérigraphie', 'sérigraphie', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1422, 'tabacs (bureaux de)', 'tabacs (bureaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1423, 'tabacs (importation, exportation)', 'tabacs (importation, exportation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1424, 'tabacs (manufactures)', 'tabacs (manufactures)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1425, 'tabacs : fournitures pour bureaux de', 'tabacs : fournitures pour bureaux de', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1426, 'tableaux (restauration)', 'tableaux (restauration)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1427, 'tableaux, estampes et reproductions d''art (commerce)', 'tableaux, estampes et reproductions d''art (commerce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1428, 'tailleurs : vêtements sur mesure', 'tailleurs : vêtements sur mesure', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1429, 'tailleurs de pierres', 'tailleurs de pierres', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1430, 'tampons encreurs, tampons marqueurs', 'tampons encreurs, tampons marqueurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1431, 'tapis (nettoyage de)', 'tapis (nettoyage de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1432, 'tapis d''Orient et d''artisanat', 'tapis d''Orient et d''artisanat', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1433, 'tapis et tapisseries (reproduction, réparation, restauration)', 'tapis et tapisseries (reproduction, réparation, restauration)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1434, 'tapis, revêtements textiles (fabrication)', 'tapis, revêtements textiles (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1435, 'tapisseries d''art', 'tapisseries d''art', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1436, 'tapissiers et tapissiers-décorateurs', 'tapissiers et tapissiers-décorateurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1437, 'tapissiers, décorateurs : fournitures (fabrication, gros)', 'tapissiers, décorateurs : fournitures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1438, 'tatouages', 'tatouages', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1439, 'taxis : artisans', 'taxis : artisans', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1440, 'taxis : centrales d''appel', 'taxis : centrales d''appel', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1441, 'teintures (fabrication, gros)', 'teintures (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1442, 'tennis (construction)', 'tennis (construction)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1443, 'tennis (courts et leçons)', 'tennis (courts et leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1444, 'terrains de sport et de jeu (aménagement)', 'terrains de sport et de jeu (aménagement)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1445, 'terrassements (entreprises)', 'terrassements (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1446, 'terreaux, tourbes, autres amendements', 'terreaux, tourbes, autres amendements', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1447, 'textiles : fournitures et accessoires pour les industries (fabrication, négoce)', 'textiles : fournitures et accessoires pour les industries (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1448, 'thalassothérapie, balnéothérapie (centres)', 'thalassothérapie, balnéothérapie (centres)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1449, 'thanatopracteurs', 'thanatopracteurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1450, 'thé (importation, négoce)', 'thé (importation, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1451, 'théâtre et cinéma : décors et accessoires', 'théâtre et cinéma : décors et accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1452, 'théâtre, ballet, danse (troupes, compagnies)', 'théâtre, ballet, danse (troupes, compagnies)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1453, 'théâtres et salles de spectacles', 'théâtres et salles de spectacles', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1454, 'théâtres et spectacles (location de places)', 'théâtres et spectacles (location de places)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1455, 'tir (stands de)', 'tir (stands de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1456, 'tissages à façon', 'tissages à façon', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1457, 'tissus (gros)', 'tissus (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1458, 'tissus : impression, teinture, traitement, transformation', 'tissus : impression, teinture, traitement, transformation', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1459, 'tissus au mètre (détail)', 'tissus au mètre (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1460, 'tissus pour l''ameublement, rideaux, passementerie (fabrication, gros)', 'tissus pour l''ameublement, rideaux, passementerie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1461, 'tissus pour l''habillement (fabrication, gros)', 'tissus pour l''habillement (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1462, 'tissus à usages techniques (fabrication, gros)', 'tissus à usages techniques (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:03', 0, 1, 0, 0),
(1463, 'titres restaurants (sociétés de gestion)', 'titres restaurants (sociétés de gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1464, 'toilettage de chiens et chats', 'toilettage de chiens et chats', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1465, 'tondeuses à gazon (détail)', 'tondeuses à gazon (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1466, 'tonnellerie', 'tonnellerie', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1467, 'topographie (travaux de)', 'topographie (travaux de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1468, 'topographie : instruments et fournitures', 'topographie : instruments et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1469, 'tourisme (guides et guides-interprètes)', 'tourisme (guides et guides-interprètes)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1470, 'tourisme (organismes)', 'tourisme (organismes)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1471, 'tourisme : sites, circuits et curiosités', 'tourisme : sites, circuits et curiosités', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1472, 'tourneurs sur bois', 'tourneurs sur bois', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1473, 'tracteurs agricoles et remorques', 'tracteurs agricoles et remorques', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1474, 'traducteurs', 'traducteurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1475, 'traitement chimique des surfaces (matériel et installation)', 'traitement chimique des surfaces (matériel et installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1476, 'traitement de surfaces (travaux)', 'traitement de surfaces (travaux)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1477, 'traitement des bois', 'traitement des bois', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1478, 'traitement des eaux (services)', 'traitement des eaux (services)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1479, 'traitement des eaux : appareils et équipements', 'traitement des eaux : appareils et équipements', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1480, 'traitement des métaux', 'traitement des métaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1481, 'traitement du courrier : matériel pour', 'traitement du courrier : matériel pour', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1482, 'traitement mécanique des surfaces (matériel et installation)', 'traitement mécanique des surfaces (matériel et installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1483, 'traitements thermiques', 'traitements thermiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1484, 'traiteurs', 'traiteurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1485, 'traiteurs pour collectivités', 'traiteurs pour collectivités', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1486, 'traiteurs, organisation de réception', 'traiteurs, organisation de réception', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1487, 'transfusion sanguine', 'transfusion sanguine', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1488, 'transport (conseils en organisation et gestion)', 'transport (conseils en organisation et gestion)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1489, 'transport en groupage-messagerie', 'transport en groupage-messagerie', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1490, 'transport en messagerie express, coursiers internationaux', 'transport en messagerie express, coursiers internationaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1491, 'transport international (commissionnaires de transport, transitaires, auxiliaires)', 'transport international (commissionnaires de transport, transitaires, auxiliaires)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1492, 'transport intérieur (commissionnaires, auxiliaires, organisateurs de)', 'transport intérieur (commissionnaires, auxiliaires, organisateurs de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1493, 'transport maritime (commissionnaires, transitaires, courtiers et auxiliaires)', 'transport maritime (commissionnaires, transitaires, courtiers et auxiliaires)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1494, 'transports aériens', 'transports aériens', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1495, 'transports combinés (rail et route)', 'transports combinés (rail et route)', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1496, 'transports d''animaux vivants', 'transports d''animaux vivants', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1497, 'transports d''automobiles et autres véhicules', 'transports d''automobiles et autres véhicules', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1498, 'transports d''objets d''art', 'transports d''objets d''art', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1499, 'transports de denrées et produits périssables', 'transports de denrées et produits périssables', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1500, 'transports de fonds et de valeurs', 'transports de fonds et de valeurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:04', 0, 1, 0, 0),
(1501, 'transports de masses indivisibles', 'transports de masses indivisibles', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1502, 'transports de pianos et de coffres-forts', 'transports de pianos et de coffres-forts', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1503, 'transports ferroviaires', 'transports ferroviaires', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1504, 'transports internationaux', 'transports internationaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1505, 'transports légers et courses régionaux et nationaux', 'transports légers et courses régionaux et nationaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1506, 'transports maritimes', 'transports maritimes', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1507, 'transports par citernes : granulaires et pulvérulents', 'transports par citernes : granulaires et pulvérulents', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1508, 'transports par citernes : liquides alimentaires', 'transports par citernes : liquides alimentaires', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1509, 'transports par citernes : liquides industriels ou pétroliers', 'transports par citernes : liquides industriels ou pétroliers', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1510, 'transports par voies navigables', 'transports par voies navigables', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1511, 'transports routiers : lots complets, marchandises diverses', 'transports routiers : lots complets, marchandises diverses', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1512, 'transports urbains et régionaux de voyageurs', 'transports urbains et régionaux de voyageurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1513, 'travail protégé (établissements de)', 'travail protégé (établissements de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1514, 'travaux acrobatiques, montage-levage (entreprises)', 'travaux acrobatiques, montage-levage (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1515, 'travaux agricoles', 'travaux agricoles', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1516, 'travaux ferroviaires', 'travaux ferroviaires', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1517, 'travaux maritimes, hydrauliques et fluviaux', 'travaux maritimes, hydrauliques et fluviaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1518, 'travaux publics (entreprises)', 'travaux publics (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1519, 'travaux routiers (entreprises)', 'travaux routiers (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1520, 'tribunaux, centres de médiation et d''arbitrage, administration judiciaire', 'tribunaux, centres de médiation et d''arbitrage, administration judiciaire', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1521, 'tricotage (façonniers)', 'tricotage (façonniers)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1522, 'tricots et pulls (détail)', 'tricots et pulls (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1523, 'tricots et pulls (fabrication, gros)', 'tricots et pulls (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1524, 'triperie (détail)', 'triperie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1525, 'triperie (gros)', 'triperie (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1526, 'tronçonneuses (détail)', 'tronçonneuses (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1527, 'tréfileries', 'tréfileries', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1528, 'tubes et tuyaux en caoutchouc et plastique (fabrication, gros)', 'tubes et tuyaux en caoutchouc et plastique (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1529, 'tubes et tuyaux en métaux ferreux (fabrication, négoce)', 'tubes et tuyaux en métaux ferreux (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1530, 'tubes et tuyaux en métaux non ferreux', 'tubes et tuyaux en métaux non ferreux', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1531, 'tubes électroniques et semi-conducteurs (fabrication)', 'tubes électroniques et semi-conducteurs (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1532, 'tuyauterie industrielle', 'tuyauterie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1533, 'télécommunications (consultants)', 'télécommunications (consultants)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1534, 'télécommunications (installateurs)', 'télécommunications (installateurs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1535, 'télécommunications : matériel et systèmes (fabrication, importation)', 'télécommunications : matériel et systèmes (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1536, 'télécommunications : services', 'télécommunications : services', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1537, 'télécopieurs (vente, location, entretien)', 'télécopieurs (vente, location, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1538, 'télématique (création de services, hébergement, conseils)', 'télématique (création de services, hébergement, conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1539, 'téléphonie (installateurs)', 'téléphonie (installateurs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1540, 'téléphonie mobile, radiomessagerie, radiocommunications (services, commercialisation)', 'téléphonie mobile, radiomessagerie, radiocommunications (services, commercialisation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1541, 'téléphonie, péritéléphonie (vente, location, entretien)', 'téléphonie, péritéléphonie (vente, location, entretien)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1542, 'téléphériques, transporteurs à câbles (fabrication, installation)', 'téléphériques, transporteurs à câbles (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1543, 'télésurveillance : appareils, systèmes', 'télésurveillance : appareils, systèmes', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1544, 'télévision : chaînes, réseaux', 'télévision : chaînes, réseaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1545, 'télévision, vidéo : appareils et accessoires (vente)', 'télévision, vidéo : appareils et accessoires (vente)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1546, 'télévision, vidéo, hi-fi (dépannage)', 'télévision, vidéo, hi-fi (dépannage)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1547, 'tôlerie de précision', 'tôlerie de précision', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1548, 'tôlerie industrielle', 'tôlerie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1549, 'uniformes (fabrication)', 'uniformes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1550, 'urbanistes', 'urbanistes', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1551, 'urgence et assistance (services d'')', 'urgence et assistance (services d'')', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1552, 'vaisselle et articles à usage unique (fabrication, gros)', 'vaisselle et articles à usage unique (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1553, 'vannerie (détail)', 'vannerie (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:05', 0, 1, 0, 0),
(1554, 'vannerie (fabrication)', 'vannerie (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1555, 'vannes (fabrication)', 'vannes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1556, 'vedettes et bateaux touristiques', 'vedettes et bateaux touristiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1557, 'ventes aux enchères publiques : sociétés de ventes volontaires de meubles', 'ventes aux enchères publiques : sociétés de ventes volontaires de meubles', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1558, 'ventes par correspondance et à distance', 'ventes par correspondance et à distance', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1559, 'ventilateurs (fabrication, gros)', 'ventilateurs (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1560, 'ventilation, aération : matériel (vente, installation)', 'ventilation, aération : matériel (vente, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1561, 'vernisseurs', 'vernisseurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1562, 'verre optique (fabrication, importation)', 'verre optique (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1563, 'verrerie d''art, verrerie soufflée (fabrication, gros)', 'verrerie d''art, verrerie soufflée (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1564, 'verrerie industrielle', 'verrerie industrielle', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1565, 'verrerie, cristallerie (fabrication, gros)', 'verrerie, cristallerie (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1566, 'viandes de boucherie (gros)', 'viandes de boucherie (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1567, 'viandes de porc (gros)', 'viandes de porc (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1568, 'vidange, curage', 'vidange, curage', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1569, 'vidéo : location de cassettes, DVD', 'vidéo : location de cassettes, DVD', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1570, 'vidéo professionnelle', 'vidéo professionnelle', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1571, 'villages et clubs de vacances', 'villages et clubs de vacances', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1572, 'vinification : matériel et équipements', 'vinification : matériel et équipements', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1573, 'vins (coopératives, unions de producteurs)', 'vins (coopératives, unions de producteurs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1574, 'vins (courtiers)', 'vins (courtiers)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1575, 'vins (négociants, éleveurs, élaborateurs, vinificateurs)', 'vins (négociants, éleveurs, élaborateurs, vinificateurs)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1576, 'vins (producteurs récoltants, vente directe)', 'vins (producteurs récoltants, vente directe)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1577, 'vins et spiritueux (vente au détail)', 'vins et spiritueux (vente au détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1578, 'vins, spiritueux et alcools (négociants en gros)', 'vins, spiritueux et alcools (négociants en gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1579, 'viticulture : matériel et fournitures', 'viticulture : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1580, 'vitrages de sécurité, de protection solaire, fournitures (fabrication, installation)', 'vitrages de sécurité, de protection solaire, fournitures (fabrication, installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1581, 'vitrages, produits verriers, miroirs (fabrication, négoce)', 'vitrages, produits verriers, miroirs (fabrication, négoce)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1582, 'vitraux', 'vitraux', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1583, 'vitrerie (pose)', 'vitrerie (pose)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1584, 'vitrines d''intérieur', 'vitrines d''intérieur', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1585, 'voile, sports nautiques (pratique de)', 'voile, sports nautiques (pratique de)', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1586, 'voitures sans permis', 'voitures sans permis', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1587, 'vol : équipement de protection de magasins', 'vol : équipement de protection de magasins', 0, 'default.png', 0, 1, '2017-10-24 06:47:06', 0, 1, 0, 0),
(1588, 'vol libre', 'vol libre', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1589, 'volailles et gibiers (détail)', 'volailles et gibiers (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1590, 'volailles et gibiers (gros)', 'volailles et gibiers (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1591, 'volets roulants', 'volets roulants', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1592, 'volets, persiennes et jalousies', 'volets, persiennes et jalousies', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1593, 'voyance, cartomancie', 'voyance, cartomancie', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1594, 'véhicules industriels (construction, importation)', 'véhicules industriels (construction, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1595, 'vérandas', 'vérandas', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1596, 'vérins, treuils et palans à moteur, hydrauliques et pneumatiques', 'vérins, treuils et palans à moteur, hydrauliques et pneumatiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1597, 'vétérinaires', 'vétérinaires', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1598, 'vétérinaires : matériel et fournitures pour', 'vétérinaires : matériel et fournitures pour', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1599, 'vêtements d''occasion (gros)', 'vêtements d''occasion (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1600, 'vêtements de cuir et de peau (détail)', 'vêtements de cuir et de peau (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1601, 'vêtements de cuir et de peau (fabrication, gros)', 'vêtements de cuir et de peau (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1602, 'vêtements de pluie et de protection (fabrication)', 'vêtements de pluie et de protection (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1603, 'vêtements de sport (fabrication)', 'vêtements de sport (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1604, 'vêtements de travail et professionnels (détail)', 'vêtements de travail et professionnels (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1605, 'vêtements de travail et professionnels (fabrication, gros)', 'vêtements de travail et professionnels (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1606, 'vêtements en maille (fabrication)', 'vêtements en maille (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1607, 'vêtements et accessoires de protection', 'vêtements et accessoires de protection', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1608, 'vêtements pour bébés et enfants (fabrication, gros)', 'vêtements pour bébés et enfants (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1609, 'vêtements pour bébés, articles de puériculture (détail)', 'vêtements pour bébés, articles de puériculture (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1610, 'vêtements pour enfants (détail)', 'vêtements pour enfants (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1611, 'vêtements pour femmes (détail)', 'vêtements pour femmes (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1612, 'vêtements pour femmes (fabrication)', 'vêtements pour femmes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1613, 'vêtements pour futures mamans', 'vêtements pour futures mamans', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1614, 'vêtements pour hommes (détail)', 'vêtements pour hommes (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1615, 'vêtements pour hommes (fabrication)', 'vêtements pour hommes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1616, 'vêtements pour hommes et femmes (gros)', 'vêtements pour hommes et femmes (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1617, 'vêtements sport : sportswear (détail)', 'vêtements sport : sportswear (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1618, 'vêtements sport : sportswear (fabrication)', 'vêtements sport : sportswear (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1619, 'yoga (leçons)', 'yoga (leçons)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1620, 'ébénisterie', 'ébénisterie', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1621, 'ébénisterie d''art, restauration de meubles', 'ébénisterie d''art, restauration de meubles', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1622, 'échafaudages (montage, location)', 'échafaudages (montage, location)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1623, 'échafaudages et échelles (fabrication, gros)', 'échafaudages et échelles (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1624, 'échantillonnage', 'échantillonnage', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1625, 'éclairage : matériel (gros)', 'éclairage : matériel (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1626, 'éclairage de bureaux et de commerces : matériel (fabrication, importation)', 'éclairage de bureaux et de commerces : matériel (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1627, 'éclairage industriel, éclairage public : matériel (fabrication, importation)', 'éclairage industriel, éclairage public : matériel (fabrication, importation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1628, 'écoles maternelles privées', 'écoles maternelles privées', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1629, 'écoles maternelles publiques', 'écoles maternelles publiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1630, 'écoles primaires privées', 'écoles primaires privées', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1631, 'écoles primaires publiques', 'écoles primaires publiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1632, 'économie d''énergie (études et conseils)', 'économie d''énergie (études et conseils)', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1633, 'économistes de la construction, métreurs et vérificateurs', 'économistes de la construction, métreurs et vérificateurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1634, 'écrivains', 'écrivains', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1635, 'écrivains publics et conseils en formalités administratives', 'écrivains publics et conseils en formalités administratives', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1636, 'éditions culturelles', 'éditions culturelles', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1637, 'éditions techniques et scientifiques', 'éditions techniques et scientifiques', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1638, 'église catholique', 'église catholique', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1639, 'église orthodoxe', 'église orthodoxe', 0, 'default.png', 0, 1, '2017-10-24 06:47:07', 0, 1, 0, 0),
(1640, 'églises chrétiennes diverses', 'églises chrétiennes diverses', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1641, 'églises protestantes', 'églises protestantes', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1642, 'églises évangéliques', 'églises évangéliques', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1643, 'élagage et abattage (entreprises)', 'élagage et abattage (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1644, 'électricité (montage, assemblage de matériel)', 'électricité (montage, assemblage de matériel)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1645, 'électricité (production, distribution, fourniture)', 'électricité (production, distribution, fourniture)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1646, 'électricité : appareillage industriel, composants (fabrication)', 'électricité : appareillage industriel, composants (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1647, 'électricité : conduits et accessoires (fabrication)', 'électricité : conduits et accessoires (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1648, 'électricité : installations industrielles', 'électricité : installations industrielles', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1649, 'électricité : matériel de production et de transport (fabrication)', 'électricité : matériel de production et de transport (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1650, 'électricité : matériel et fournitures d''installation (fabrication)', 'électricité : matériel et fournitures d''installation (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1651, 'électricité générale (entreprises)', 'électricité générale (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1652, 'électricité marine (installation)', 'électricité marine (installation)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1653, 'électricité pour automobiles (stations techniques)', 'électricité pour automobiles (stations techniques)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1654, 'électricité, électronique : fournitures (détail)', 'électricité, électronique : fournitures (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1655, 'électricité, électronique : matériel et fournitures (gros)', 'électricité, électronique : matériel et fournitures (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1656, 'électricité, électronique médicale et radiologie : appareils (fabrication)', 'électricité, électronique médicale et radiologie : appareils (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1657, 'électromécanique', 'électromécanique', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1658, 'électroménager (dépannage)', 'électroménager (dépannage)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1659, 'électroménager (détail)', 'électroménager (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1660, 'électroménager (fabrication, gros)', 'électroménager (fabrication, gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1661, 'électroménager : pièces détachées (détail)', 'électroménager : pièces détachées (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1662, 'électrométallurgie', 'électrométallurgie', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1663, 'électronique professionnelle : matériel et composants (gros)', 'électronique professionnelle : matériel et composants (gros)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1664, 'électronique professionnelle et radio-électricité : matériel (fabrication)', 'électronique professionnelle et radio-électricité : matériel (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1665, 'élevage : matériel et fournitures', 'élevage : matériel et fournitures', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1666, 'élevage d''animaux', 'élevage d''animaux', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1667, 'élevage de bovins', 'élevage de bovins', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1668, 'élevage de chevaux, haras', 'élevage de chevaux, haras', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1669, 'élevage de chiens, de chats', 'élevage de chiens, de chats', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1670, 'élevage de gibier', 'élevage de gibier', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1671, 'élevage de moutons et de chèvres', 'élevage de moutons et de chèvres', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1672, 'élevage de porcs', 'élevage de porcs', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1673, 'élevage de volailles et de lapins', 'élevage de volailles et de lapins', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1674, 'énergie nucléaire (équipements, études)', 'énergie nucléaire (équipements, études)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1675, 'énergies solaire et nouvelles (équipements, études)', 'énergies solaire et nouvelles (équipements, études)', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1676, 'épiceries fines', 'épiceries fines', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1677, 'équarrisseurs', 'équarrisseurs', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1678, 'équipements et fournitures militaires', 'équipements et fournitures militaires', 0, 'default.png', 0, 1, '2017-10-24 06:47:08', 0, 1, 0, 0),
(1679, 'équipements pétroliers', 'équipements pétroliers', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1680, 'équitation : sellerie, équipement (détail)', 'équitation : sellerie, équipement (détail)', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1681, 'établissements d''éducation spécialisée', 'établissements d''éducation spécialisée', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1682, 'établissements financiers', 'établissements financiers', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1683, 'étalages : accessoires', 'étalages : accessoires', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1684, 'étalagistes', 'étalagistes', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1685, 'étanchéité (entreprises)', 'étanchéité (entreprises)', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1686, 'étanchéité : produits', 'étanchéité : produits', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1687, 'étiquettes (fabrication)', 'étiquettes (fabrication)', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1688, 'études de marchés', 'études de marchés', 0, 'default.png', 0, 1, '2017-10-24 06:47:09', 0, 1, 0, 0),
(1689, 'Maintenance industrielle1', 'Maintenance industrielle1', 0, 'default.png', 0, 1, '2017-10-24 06:50:05', 0, 1, 0, 0),
(1690, 'Services, aides à domicile1', 'Services, aides à domicile1', 0, 'default.png', 0, 1, '2017-10-24 06:50:05', 0, 1, 0, 0),
(1691, '', '', 0, 'default.png', 0, 1, '2017-11-04 04:44:53', 0, 1, 0, 0),
(1692, 'Caps', 'sdfasd sdfsadfsda', 0, 's8JmXvM036.png', 0, 1, '2017-11-06 06:50:23', 26, 2, 0, 0),
(1693, 'Shimul', 'asad ad', 0, 'q8m4mVuGAi.png', 0, 1, '2017-11-06 07:04:33', 26, 3, 0, 0),
(1694, 'filter1', 'asf', 0, 'kRoYifvEav.png', 0, 1, '2017-11-06 07:04:47', 26, 3, 0, 0),
(1695, 'filter2', 'sadfasdf sdfas d', 0, 'VcDY7FVu4G.png', 0, 1, '2017-11-06 07:05:06', 26, 4, 0, 0),
(1696, 'Service Filter1', 'asdfsdaf', 0, '75QfIMLwUY.png', 0, 1, '2017-12-14 09:43:02', 1, 2, 0, 0),
(1697, 'service filter 2', 'test', 0, 'PVFgLMrLLV.png', 0, 1, '2017-12-14 09:43:20', 1, 2, 0, 0),
(1698, 'Service filter 3', 'sadf', 0, '5bwOewoUYx.png', 0, 1, '2017-12-14 09:43:48', 1, 2, 0, 0),
(1699, 'service filter testing', 'Manually added by user', 0, 'default.png', 0, 1, '2017-12-22 06:51:10', 120, 2, 0, 0),
(1700, 'service testing second', 'Manually added by user', 0, 'default.png', 0, 1, '2017-12-22 06:58:37', 120, 2, 0, 0),
(1701, 'product filter testing', 'Manually added by user', 0, 'default.png', 0, 1, '2017-12-22 06:59:19', 120, 3, 0, 0),
(1702, 'product testing 2', 'Manually added by user', 0, 'default.png', 0, 1, '2017-12-22 07:02:05', 120, 3, 0, 0),
(1703, 'other testing', 'Manually added by user', 0, 'default.png', 0, 1, '2017-12-22 07:02:16', 120, 4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `otd_business_details`
--

CREATE TABLE IF NOT EXISTS `otd_business_details` (
`bs_id` bigint(20) unsigned NOT NULL,
  `bs_user` bigint(20) unsigned NOT NULL,
  `bs_name` varchar(255) NOT NULL,
  `bs_comp_number` varchar(255) NOT NULL,
  `bs_email` varchar(255) NOT NULL,
  `bs_phone` varchar(255) NOT NULL,
  `bs_twitter` varchar(255) NOT NULL,
  `bs_website` varchar(255) NOT NULL,
  `bs_address` varchar(255) NOT NULL,
  `bs_opening` date DEFAULT NULL,
  `bs_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `bs_banner_display` varchar(255) DEFAULT NULL,
  `bs_city` varchar(255) NOT NULL,
  `bs_zipcode` varchar(255) NOT NULL,
  `bs_honorific` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_business_details`
--

INSERT INTO `otd_business_details` (`bs_id`, `bs_user`, `bs_name`, `bs_comp_number`, `bs_email`, `bs_phone`, `bs_twitter`, `bs_website`, `bs_address`, `bs_opening`, `bs_desc`, `bs_banner_display`, `bs_city`, `bs_zipcode`, `bs_honorific`) VALUES
(1, 2, '01db métravib', '4.10E+13', '', '', '', '', '   zone industrielle 50440 Digulleville', NULL, 'Imported Profile', NULL, '', '', ''),
(2, 3, '02 amiens', '4.91E+13', '', '', '', '', '11  avenue paix 80000 Amiens', NULL, 'Imported Profile', NULL, '', '', ''),
(3, 4, '1.2.3.', '4.45E+13', '', '', '', '', '8  rue 3 cailloux 80000 Amiens', NULL, 'Imported Profile', NULL, '', '', ''),
(4, 5, '1.2.3', '3.51E+13', '', '', '', '', '6  rue gambetta 60000 Beauvais', NULL, 'Imported Profile', NULL, '', '', ''),
(5, 6, '1.2.3', '4.45E+13', '', '', '', '', '8  rue 3 cailloux 80000 Amiens', NULL, 'Imported Profile', NULL, '', '', ''),
(6, 7, '1.2.3', '4.45E+13', '', '', '', '', '  espace rené coty 76600 Le Havre', NULL, 'Imported Profile', NULL, '', '', ''),
(7, 8, '1.2.3', '4.45E+13', '', '', '', '', '50 b rue gros horloge 76000 Rouen', NULL, 'Imported Profile', NULL, '', '', ''),
(8, 9, '1001 listes', '4.23E+13', '', '', '', '', '77  rue ecuyère 76000 Rouen', NULL, 'Imported Profile', NULL, '', '', ''),
(9, 10, '100 pour 100 immo', '5.30E+13', '', '', '', '', '18  avenue jean jaurès 76140 Le Petit Quevilly', NULL, 'Imported Profile', NULL, '', '', ''),
(10, 11, '100 pour 100 pneu', '4.49E+13', '', '', '', '', '2  route havre 76400 Fécamp', NULL, 'Imported Profile', NULL, '', '', ''),
(11, 12, '3 h taron', '3.45E+13', '', '', '', '', '  parc activités pont normandie 76700 Rogerville', NULL, 'Imported Profile', NULL, '', '', ''),
(12, 13, 'Ad normandie-maine,f.i.a.o', '4.52E+13', '', '', '', '', '10  rue expansion 61000 Cerisé', NULL, 'Imported Profile', NULL, '', '', ''),
(13, 14, 'Ad normandie-maine', '4.52E+13', '', '', '', '', '23  route falaise 61600 La Ferté Macé', NULL, 'Imported Profile', NULL, '', '', ''),
(14, 15, 'Ad normandie maine', '4.52E+13', '', '', '', '', '165  rue artisans 50110 Tourlaville', NULL, 'Imported Profile', NULL, '', '', ''),
(15, 16, 'Ad normandie maine', '4.52E+13', '', '', '', '', '6   le grand chien 50300 Saint Martin des Champs', NULL, 'Imported Profile', NULL, '', '', ''),
(16, 17, 'Adom''pare-brise', '4.93E+13', '', '', '', '', '155  rue martyrs de la résistance 76150 Maromme', NULL, 'Imported Profile', NULL, '', '', ''),
(17, 18, 'Agence immobilière lecornu', '3.15E+13', '', '', '', '', '86 b rue aristide briand 61200 Argentan', NULL, 'Imported Profile', NULL, '', '', ''),
(18, 19, 'Alexandre m.c arrivé c.', '3.89E+13', '', '', '', '', '1  rue toustain 76200 Dieppe', NULL, 'Imported Profile', NULL, '', '', ''),
(19, 20, 'Abdallah Bana', '3.39135E+13', '', '', '', '', '26 Place de la République, 60180 Nogent-sur-Oise, France', NULL, 'Imported Profile', NULL, '', '', ''),
(20, 21, 'Abdallah Ghassan bou', '5.20208E+13', '', '', '', '', '9 Rue du Général Leclerc, 27500 Pont-Audemer, France', NULL, 'Imported Profile', NULL, '', '', ''),
(21, 22, 'Abdel-aziz Teldjoune', '4.91095E+13', '', '', '', '', '45 Rue Henri Dunant, 76620 Le Havre, France', NULL, 'Imported Profile', NULL, '', '', ''),
(22, 23, 'Abdel-hamid Aboudmiaa', '5.20695E+13', '', '', '', '', '63 Route de Dieppe, 76960 Notre-Dame-de-Bondeville, France', NULL, 'Imported Profile', NULL, '', '', ''),
(23, 24, 'Abdelaaziz Talbioui', '3.37769E+13', '', '', '', '', '321 Rue Alexis de Tocqueville, 50000 Saint-Lô, France', NULL, 'Imported Profile', NULL, '', '', ''),
(24, 25, 'Abdeladim Qassid', '8.24781E+13', '', '', '', '', '240 Avenue du Général Leclerc, 61000 Alençon, France', NULL, 'Imported Profile', NULL, '', '', ''),
(25, 26, 'Abdelak Elachab', '4.10353E+13', '', '', '', '', '4 Rue Vigneron, 02870 Crépy, France', NULL, 'Imported Profile', NULL, '', '', ''),
(26, 27, 'Abdelali Kébli', '3.20945E+13', '', '', '', '', '60 Rue Saint-Denis, 02130 Villers-sur-Fère, France', NULL, 'Imported Profile', NULL, '', '', ''),
(27, 28, 'Abdelaziz Teldjoune', '4.91095E+13', '', '', '', '', '45 Rue Henri Dunant, 76620 Le Havre, France', NULL, 'Imported Profile', NULL, '', '', ''),
(28, 29, 'Abdelghafour Abajjane', '8.10004E+13', '', '', '', '', '14 Rue du Pont de Fresne, 61250 Damigny, France', NULL, 'Imported Profile', NULL, '', '', ''),
(29, 30, 'Abdelhak Sahli', '3.33783E+13', '', '', '', '', '55 Rue Louis Denoual, 60540 Bornel, France', NULL, 'Imported Profile', NULL, '', '', ''),
(30, 31, 'Abdelhouab Belbeida', '7.49971E+13', '', '', '', '', '2935 Rue de Cailly, 76230 Quincampoix, France', NULL, 'Imported Profile', NULL, '', '', ''),
(31, 32, 'Abdelkrim Benhamouche', '7.98345E+13', '', '', '', '', '29 Rue Sainte-Foy, 27190 Conches-en-Ouche, France', NULL, 'Imported Profile', NULL, '', '', ''),
(32, 33, 'Abdelkrim Hadour', '4.87749E+13', '', '', '', '', '18 Rue Alfred Leblanc, 60000 Allonne, France', NULL, 'Imported Profile', NULL, '', '', ''),
(33, 34, 'Abdelkrim Hamimi', '4.99277E+13', '', '', '', '', '4 Route Nationale, 80320 Fresnes-Mazancourt, France', NULL, 'Imported Profile', NULL, '', '', ''),
(34, 35, 'Abdelkrim Ould-slimane', '3.16645E+13', '', '', '', '', '13 Place de la Croûte, 50200 Coutances, France', NULL, 'Imported Profile', NULL, '', '', ''),
(35, 36, 'Abdelkrim Rezkallah', '4.50987E+13', '', '', '', '', '24 Rue Pierre Curie, 02200 Soissons, France', NULL, 'Imported Profile', NULL, '', '', ''),
(36, 37, 'Abdellah Bayne', '3.3952E+13', '', '', '', '', '3 Rue du Bois Saint-Denis, 60500 Chantilly, France', NULL, 'Imported Profile', NULL, '', '', ''),
(37, 38, 'Abdellah Epi service', '8.14471E+13', '', '', '', '', '3 Rue de l''Europe, 50180 Saint-Gilles, France', NULL, 'Imported Profile', NULL, '', '', ''),
(38, 39, 'Abdellatif Habbeddine', '4.09611E+13', '', '', '', '', '28 Rue de Binche, 76150 Maromme, France', NULL, 'Imported Profile', NULL, '', '', ''),
(39, 40, 'Abdellatif Mahfoud', '4.93884E+13', '', '', '', '', '96 Rue Louis Blanc, 76100 Rouen, France', NULL, 'Imported Profile', NULL, '', '', ''),
(40, 41, 'Abdelmajid Kaabi', '3.91398E+13', '', '', '', '', '25 Rue Jean Jaurès, 60740 Saint-Maximin, France', NULL, 'Imported Profile', NULL, '', '', ''),
(41, 42, 'Abdelmalek Hamadouche', '4.44959E+13', '', '', '', '', '19 Rue de la République, 76500 Elbeuf, France', NULL, 'Imported Profile', NULL, '', '', ''),
(42, 43, 'Abdel Zaraa', '7.80229E+13', '', '', '', '', '21 Boulevard de la Liberté, 02700 Tergnier, France', NULL, 'Imported Profile', NULL, '', '', ''),
(43, 44, 'Abderrahim Eraissi', '7.51005E+13', '', '', '', '', '35 Rue de la République, 76350 Oissel, France', NULL, 'Imported Profile', NULL, '', '', ''),
(44, 45, 'Abderrahman Bahloul', '3.23989E+13', '', '', '', '', '10 Place de L Hôtel de ville, 80400 Ham, France', NULL, 'Imported Profile', NULL, '', '', ''),
(45, 46, 'Abderrazak Hammami', '4.92739E+13', '', '', '', '', '89 Route de Dieppe, 76960 Notre-Dame-de-Bondeville, France', NULL, 'Imported Profile', NULL, '', '', ''),
(46, 47, 'Abderrazek Khelfat', '3.47778E+13', '', '', '', '', '23 Rue des Canonniers, 02100 Saint-Quentin, France', NULL, 'Imported Profile', NULL, '', '', ''),
(47, 48, 'Abderrazzak Lyagoubi', '4.43821E+13', '', '', '', '', '39 Avenue de Quakenbruck, 61000 Alençon, France', NULL, 'Imported Profile', NULL, '', '', ''),
(48, 49, 'Abderrezak Hammadou', '7.94706E+13', '', '', '', '', '82 Rue Jean Jaurès, 80000 Amiens, France', NULL, 'Imported Profile', NULL, '', '', ''),
(49, 50, 'Abderrezak Hammadou', '7.94706E+13', '', '', '', '', '82 Rue Jean Jaurès, 80000 Amiens, France', NULL, 'Imported Profile', NULL, '', '', ''),
(50, 51, 'Abdesselem Boussad', '3.84274E+13', '', '', '', '', '18 Rue Jean François la Pérouse, 76800 Saint-Étienne-du-Rouvray, France', NULL, 'Imported Profile', NULL, '', '', ''),
(51, 52, 'Abdoul Fofana', '7.53232E+13', '', '', '', '', '6 Zone Artisanale Les Cambres, 76710 Anceaumeville, France', NULL, 'Imported Profile', NULL, '', '', ''),
(52, 53, 'Abed Boukalfa', '4.21948E+13', '', '', '', '', '36 Rue Pont Mortain, 14100 Lisieux, France', NULL, 'Imported Profile', NULL, '', '', ''),
(53, 54, 'Abel Baugé', '3.97706E+13', '', '', '', '', '9 Rue du Moulin, 61350 Saint-Mars-d''Égrenne, France', NULL, 'Imported Profile', NULL, '', '', ''),
(54, 55, 'Abel Carvalho', '4.12725E+13', '', '', '', '', '17 Route de Longueil-Annel, 60150 Thourotte, France', NULL, 'Imported Profile', NULL, '', '', ''),
(55, 56, 'Abel Cotentin', '4.23033E+13', '', '', '', '', '12 Rue de Ancienne Gare, 50260 Sottevast, France', NULL, 'Imported Profile', NULL, '', '', ''),
(56, 57, 'Abel Douilly', '3.13926E+13', '', '', '', '', 'La Loge, 60590 Flavacourt, France', NULL, 'Imported Profile', NULL, '', '', ''),
(57, 58, 'Abel Duquenoy', '3.52079E+13', '', '', '', '', '83 Rue de Paris, 60200 Compiègne, France', NULL, 'Imported Profile', NULL, '', '', ''),
(58, 59, 'Abel Masseron', '3.30969E+13', '', '', '', '', 'Le Fougeray, 61140 Geneslay, France', NULL, 'Imported Profile', NULL, '', '', ''),
(59, 60, 'Abel Pruvost', '3.15903E+13', '', '', '', '', '113 Rue de la Chaussée Romaine, 02100 Saint-Quentin, France', NULL, 'Imported Profile', NULL, '', '', ''),
(60, 61, 'Abilio De sousa carneiro', '5.03824E+13', '', '', '', '', '5 Rue Saint-Roch, 80000 Amiens, France', NULL, 'Imported Profile', NULL, '', '', ''),
(61, 62, 'Abo-la-ferte-mace Cdg', '2.861E+13', '', '', '', '', '7 Rue Félix Desaunay, 61600 La Ferté-Macé, France', NULL, 'Imported Profile', NULL, '', '', ''),
(62, 63, 'Aboudar Boulhadith', '4.99467E+13', '', '', '', '', '6 Rue Abbé Vincheneux, 76470 Le Tréport, France', NULL, 'Imported Profile', NULL, '', '', ''),
(63, 64, 'Abraham Dayan', '4.79084E+13', '', '', '', '', '71 Rue du Général Faidherbe, 76600 Le Havre, France', NULL, 'Imported Profile', NULL, '', '', ''),
(64, 65, 'Abraham Viteaux', '5.34327E+13', '', '', '', '', '32 Route de Fère en Tardenois, 02200 Belleu, France', NULL, 'Imported Profile', NULL, '', '', ''),
(65, 66, 'Abraham Viteaux', '5.34327E+13', '', '', '', '', '32 Route de Fère en Tardenois, 02200 Belleu, France', NULL, 'Imported Profile', NULL, '', '', ''),
(66, 67, 'Achille Libbrecht', '4.29045E+13', '', '', '', '', '15 Rue du Bas, 80300 Senlis-le-Sec, France', NULL, 'Imported Profile', NULL, '', '', ''),
(67, 68, 'Achille Masson', '3.85113E+13', '', '', '', '', 'Le Coudroy, 76260 Saint-Martin-le-Gaillard, France', NULL, 'Imported Profile', NULL, '', '', ''),
(68, 69, 'Achour Radji', '3.09547E+13', '', '', '', '', '6 Rue Baronne James de Rothschild, 60270 Gouvieux, France', NULL, 'Imported Profile', NULL, '', '', ''),
(69, 70, 'Achour Radji', '3.09547E+13', '', '', '', '', '9 Rue des Tertres, 60270 Gouvieux, France', NULL, 'Imported Profile', NULL, '', '', ''),
(70, 71, 'Adam Shahandeh', '4.9043E+13', '', '', '', '', '20 Place Lino Ventura, 02200 Soissons, France', NULL, 'Imported Profile', NULL, '', '', ''),
(71, 72, 'Adelaide Apolo', '8.11029E+13', '', '', '', '', '4 Rue Nouvelle, 80400 Muille-Villette, France', NULL, 'Imported Profile', NULL, '', '', ''),
(72, 73, 'Adelaide Dubois', '4.83625E+13', '', '', '', '', '8 Rue du Faux Bail, 02250 Marle, France', NULL, 'Imported Profile', NULL, '', '', ''),
(73, 74, 'Adelaide Lemarié', '3.25668E+13', '', '', '', '', '2 Chemin du Haut Bois, 27930 Huest, France', NULL, 'Imported Profile', NULL, '', '', ''),
(74, 75, 'Adel Benzina', '4.89467E+13', '', '', '', '', '4 Rue des Pommiers, 27200 Vernon, France', NULL, 'Imported Profile', NULL, '', '', ''),
(75, 76, 'Adel Hammoud', '4.49736E+13', '', '', '', '', '26 Rue de la Prairie, 02400 Château-Thierry, France', NULL, 'Imported Profile', NULL, '', '', ''),
(76, 77, 'Adelina Ciglenean', '5.31256E+13', '', '', '', '', '11 Place Auguste Poulet Malassis, 61000 Alençon, France', NULL, 'Imported Profile', NULL, '', '', ''),
(77, 78, 'Adelina Cojocaru', '8.21973E+13', '', '', '', '', '11 Boulevard Georges Clemenceau, 76200 Dieppe, France', NULL, 'Imported Profile', NULL, '', '', ''),
(78, 79, 'Adelina Maillard', '8.22914E+13', '', '', '', '', '8 Route de Crèvecoeur, 60210 Grandvilliers, France', NULL, 'Imported Profile', NULL, '', '', ''),
(79, 80, 'Adeline Allara', '3.88126E+13', '', '', '', '', '7 Avenue TW Wilson, 02400 Château-Thierry, France', NULL, 'Imported Profile', NULL, '', '', ''),
(80, 81, 'Adeline Bourdonnay-ribaut', '3.88142E+13', '', '', '', '', '114 Rue Henri 4, 27540 Ivry-la-Bataille, France', NULL, 'Imported Profile', NULL, '', '', ''),
(81, 82, 'Adeline Bresson', '3.44174E+13', '', '', '', '', '46 Rue de L Hôtel de ville, 60240 Chaumont-en-Vexin, France', NULL, 'Imported Profile', NULL, '', '', ''),
(82, 83, 'Adeline Defenne', '8.02911E+13', '', '', '', '', '27 HLM P Lopofa Rue 9 Moulins, 80600 Doullens, France', NULL, 'Imported Profile', NULL, '', '', ''),
(83, 84, 'Adeline Delhomelle', '4.97837E+13', '', '', '', '', '21 Rue nationale, 76390 Aumale, France', NULL, 'Imported Profile', NULL, '', '', ''),
(84, 85, 'Adeline Dufour', '5.31581E+13', '', '', '', '', '1 Rue du Maréchal Joffre, 61700 Domfront, France', NULL, 'Imported Profile', NULL, '', '', ''),
(85, 86, 'Adeline Lemaitre', '3.3082E+13', '', '', '', '', '96 Avenue Aristide Briand, 27930 Gravigny, France', NULL, 'Imported Profile', NULL, '', '', ''),
(86, 87, 'Adeline Litout', '4.04049E+13', '', '', '', '', '27 Rue de la Poste, 60280 Clairoix, France', NULL, 'Imported Profile', NULL, '', '', ''),
(87, 88, 'Adeline Thomas', '3.34083E+13', '', '', '', '', '1 Route de l''Ardoisière, 61250 Héloup, France', NULL, 'Imported Profile', NULL, '', '', ''),
(88, 89, 'Adelino Marques correia', '3.22898E+13', '', '', '', '', '401 Route de Beaumont, 60230 Chambly, France', NULL, 'Imported Profile', NULL, '', '', ''),
(89, 90, 'Adelino Soares', '5.14241E+13', '', '', '', '', '44 Rue Grand'' Rue, 76116 Ry, France', NULL, 'Imported Profile', NULL, '', '', ''),
(90, 91, 'Aderito Monteiro', '7.90925E+13', '', '', '', '', '9 Place de l''Église, 76480 Duclair, France', NULL, 'Imported Profile', NULL, '', '', ''),
(91, 92, 'Adil Sadgui', '5.0239E+13', '', '', '', '', '1571 Rue Aristide Briand, 76650 Petit-Couronne, France', NULL, 'Imported Profile', NULL, '', '', ''),
(92, 93, 'Adil Safir', '4.48289E+13', '', '', '', '', '11 Place des Emmurées, 76100 Rouen, France', NULL, 'Imported Profile', NULL, '', '', ''),
(93, 94, 'Adina Moraru', '5.38399E+13', '', '', '', '', '8 Rue des Bordeaux, 02210 Coincy, France', NULL, 'Imported Profile', NULL, '', '', ''),
(94, 95, 'Adnan Sahin', '8.20032E+13', '', '', '', '', '22 Rue du Long Pont, 76390 Aumale, France', NULL, 'Imported Profile', NULL, '', '', ''),
(95, 96, 'Adolphe Cauet', '4.31991E+13', '', '', '', '', '170 Rue Neuve, 80300 Senlis-le-Sec, France', NULL, 'Imported Profile', NULL, '', '', ''),
(96, 97, 'Adolphe Lerouxel', '3.06767E+13', '', '', '', '', '9 Rue du Bord de Soulles, 50200 Coutances, France', NULL, 'Imported Profile', NULL, '', '', ''),
(97, 98, 'Adriano Cardoso', '5.03661E+13', '', '', '', '', '17 Rue du Bout du Monde, 60190 Francières, France', NULL, 'Imported Profile', NULL, '', '', ''),
(98, 99, 'Adrien Bierre', '8.17591E+13', '', '', '', '', '6 Rue du Général de Gaulle, 76133 Manéglise, France', NULL, 'Imported Profile', NULL, '', '', ''),
(99, 100, 'Adrien Camut', '3.22498E+13', '', '', '', '', 'La Parinière, 27210 La Lande-Saint-Léger, France', NULL, 'Imported Profile', NULL, '', '', ''),
(100, 101, 'Adrien Claassen', '4.13567E+13', '', '', '', '', '1 Chemin de Pierrelaye, 27170 Barc, France', NULL, 'Imported Profile', NULL, '', '', ''),
(101, 102, 'Adrien Grincourt', '3.41363E+13', '', '', '', '', '2 Rue du Moulin, 80870 Tœufles, France', NULL, 'Imported Profile', NULL, '', '', ''),
(102, 103, 'Adrien Macron', '5.08265E+13', '', '', '', '', '2 Rue d''Anjou, 80300 Pys, France', NULL, 'Imported Profile', NULL, '', '', ''),
(103, 104, 'Adrien Pioger', '3.41587E+13', '', '', '', '', 'La Bruyère, 61170 Montchevrel, France', NULL, 'Imported Profile', NULL, '', '', ''),
(104, 105, 'Adrien Richez', '4.50213E+13', '', '', '', '', '18 Rue Elysée Alavoine, 02110 Bohain-en-Vermandois, France', NULL, 'Imported Profile', NULL, '', '', ''),
(105, 106, 'Adrien Taviaux', '5.38429E+13', '', '', '', '', '252 Rue Simonet, 60430 Noailles, France', NULL, 'Imported Profile', NULL, '', '', ''),
(106, 107, 'Adrien Ternel', '8.0522E+13', '', '', '', '', '11 Rue de l''Église, 80560 Louvencourt, France', NULL, 'Imported Profile', NULL, '', '', ''),
(107, 108, 'Adèle Kelbaba', '8.22108E+13', '', '', '', '', 'Chemin Départemental 12 Beauvais Saint-Leu, 60510 Rochy-Condé, France', NULL, 'Imported Profile', NULL, '', '', ''),
(108, 109, 'Adèle Tixier', '4.9983E+13', '', '', '', '', '22 Rue Meridienne, 76100 Rouen, France', NULL, 'Imported Profile', NULL, '', '', ''),
(109, 110, 'Adèle Tixier', '4.9983E+13', '', '', '', '', '88 Route de Brionne, 27370 Saint-Pierre-des-Fleurs, France', NULL, 'Imported Profile', NULL, '', '', ''),
(110, 111, 'Afassi Lahcen', '4.04081E+13', '', '', '', '', '185 Chaussée Jules Ferry, 80090 Amiens, France', NULL, 'Imported Profile', NULL, '', '', ''),
(111, 112, 'Agathe Audin', '4.01819E+13', '', '', '', '', '30 Place Paul Doumer, 02800 La Fère, France', NULL, 'Imported Profile', NULL, '', '', ''),
(112, 113, 'Agathe Caule', '8.24722E+13', '', '', '', '', '18 Rue de la Duchesse de Chartres, 60500 Vineuil-Saint-Firmin, France', NULL, 'Imported Profile', NULL, '', '', ''),
(113, 114, 'Agathe Fortin', '4.89088E+13', '', '', '', '', '4 Chemin du Raillard, 27150 Gamaches-en-Vexin, France', NULL, 'Imported Profile', NULL, '', '', ''),
(114, 115, 'Agathe Porcq', '4.22332E+13', '', '', '', '', '39 Rue de Birmingham, 80300 Albert, France', NULL, 'Imported Profile', NULL, '', '', ''),
(115, 116, 'Agathe Précourt', '8.24032E+13', '', '', '', '', '2 Place du Marché, 50290 Cérences, France', NULL, 'Imported Profile', NULL, '', '', ''),
(116, 117, 'Agnes Bailly', '3.85132E+13', '', '', '', '', '30 Rue des Grands Jardins, 27620 Sainte-Geneviève-lès-Gasny, France', NULL, 'Imported Profile', NULL, '', '', ''),
(117, 118, 'Agnes Didier', '3.94669E+13', '', '', '', '', '222 Avenue du Mont Gaillard, 76620 Le Havre, France', NULL, 'Imported Profile', NULL, '', '', ''),
(118, 119, '', '', '', '', '', '', '', NULL, 'Imported Profile', NULL, '', '', ''),
(119, 120, '1234567890', '12154214541210', '', '1234567890', '', '', 'Joinville-le-Pont, France', '0000-00-00', 'sdfsadfdsafasdf dsfasdfsadf;asdf sadf;lsakdf sadf;lksadf ;asdflkasdj ;asdflkdaj;adslksdaj;adslksadjj;adslksda;jsdalkjfds;asdkljsdfj;sdalkdsf;jsdaflkjsadf;flasdkjsad;sadlkfjs;daflsdkfjsdsdfsadfdsafasdf dsfasdfsadf;asdf sadf;lsakdf sadf;lksadf ;asdflkasdj ;asdflkdaj;adslksdaj;adslksadjj;adslksda;jsdalkjfds;asdkljsdfj;sdalkdsf;jsdaflkjsadf;flasdkjsad;sadlkfjs;daflsdkfjsdsdfsadfdsafasdf ddsfasdfsadf;asdf sadf;lsakdf sadf;lksadf ;asdflkasdj ;asdflkdaj;adslksdaj;adslksadjj;adslksda;jsdalkjfds;asdkljsd', NULL, 'Paris', '75008', 'Mrs');

-- --------------------------------------------------------

--
-- Table structure for table `otd_business_follower`
--

CREATE TABLE IF NOT EXISTS `otd_business_follower` (
`id` int(11) NOT NULL,
  `followed_user_id` int(11) DEFAULT NULL,
  `follower_user_id` int(11) DEFAULT NULL,
  `followed_since` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_business_follower`
--

INSERT INTO `otd_business_follower` (`id`, `followed_user_id`, `follower_user_id`, `followed_since`) VALUES
(1, 110, 120, '2017-11-17 07:13:16');

-- --------------------------------------------------------

--
-- Table structure for table `otd_business_openingtime`
--

CREATE TABLE IF NOT EXISTS `otd_business_openingtime` (
`op_id` bigint(20) unsigned NOT NULL,
  `op_user` bigint(20) unsigned NOT NULL,
  `mon1` varchar(255) NOT NULL,
  `mon2` varchar(255) NOT NULL,
  `mon3` varchar(255) NOT NULL,
  `mon4` varchar(255) NOT NULL,
  `tues1` varchar(255) NOT NULL,
  `tues2` varchar(255) NOT NULL,
  `tues3` varchar(255) NOT NULL,
  `tues4` varchar(255) NOT NULL,
  `wed1` varchar(255) NOT NULL,
  `wed2` varchar(255) NOT NULL,
  `wed3` varchar(255) NOT NULL,
  `wed4` varchar(255) NOT NULL,
  `thur1` varchar(255) NOT NULL,
  `thur2` varchar(255) NOT NULL,
  `thur3` varchar(255) NOT NULL,
  `thur4` varchar(255) NOT NULL,
  `fri1` varchar(255) NOT NULL,
  `fri2` varchar(255) NOT NULL,
  `fri3` varchar(255) NOT NULL,
  `fri4` varchar(255) NOT NULL,
  `sat1` varchar(255) NOT NULL,
  `sat2` varchar(255) NOT NULL,
  `sat3` varchar(255) NOT NULL,
  `sat4` varchar(255) NOT NULL,
  `sun1` varchar(255) NOT NULL,
  `sun2` varchar(255) NOT NULL,
  `sun3` varchar(255) NOT NULL,
  `sun4` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_business_openingtime`
--

INSERT INTO `otd_business_openingtime` (`op_id`, `op_user`, `mon1`, `mon2`, `mon3`, `mon4`, `tues1`, `tues2`, `tues3`, `tues4`, `wed1`, `wed2`, `wed3`, `wed4`, `thur1`, `thur2`, `thur3`, `thur4`, `fri1`, `fri2`, `fri3`, `fri4`, `sat1`, `sat2`, `sat3`, `sat4`, `sun1`, `sun2`, `sun3`, `sun4`) VALUES
(1, 77, '', '', '', '', '', '', '06:20 PM', '', '09:10 AM', '09:30 AM', '04:00 PM', '04:30 PM', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 120, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `otd_business_pricetable`
--

CREATE TABLE IF NOT EXISTS `otd_business_pricetable` (
`pr_id` bigint(20) unsigned NOT NULL,
  `pr_user` bigint(20) unsigned NOT NULL,
  `pr_pdfpath` text NOT NULL,
  `pr_metaname` varchar(255) NOT NULL,
  `pr_date_uploading` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_claimed_requests`
--

CREATE TABLE IF NOT EXISTS `otd_claimed_requests` (
`claim_request_id` int(11) NOT NULL,
  `claim_user_firstname` varchar(255) DEFAULT NULL,
  `claim_user_lastname` varchar(255) DEFAULT NULL,
  `claim_user_phone` varchar(13) DEFAULT NULL,
  `claim_user_company_number` varchar(16) DEFAULT NULL,
  `claim_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `claim_status` int(11) NOT NULL DEFAULT '0',
  `claim_user_gender` varchar(8) DEFAULT NULL,
  `claim_user_email` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_claimed_requests`
--

INSERT INTO `otd_claimed_requests` (`claim_request_id`, `claim_user_firstname`, `claim_user_lastname`, `claim_user_phone`, `claim_user_company_number`, `claim_date`, `claim_status`, `claim_user_gender`, `claim_user_email`) VALUES
(1, 'Toshik Parihar', '1236545254', '1234567897', '5.31256E+13', '2017-11-14 11:41:16', 0, 'Male', 'votive.toshik@gmai.com'),
(2, 'tstt', 'testing', '1234567898', '5.31256E+13', '2017-11-14 12:11:57', 0, 'Male', 'votive.toshik@gmail.com'),
(3, 'sdafsadf', 'sdfasdf', '1421542454', '5.31256E+13', '2017-11-14 12:12:49', 0, 'Male', 'votive.toshik@gmail.com'),
(4, 'wiuerwer', 'asdfl', '1236547874', '5.31256E+13', '2017-11-14 12:17:33', 0, 'Male', 'votive.toshik@gmail.com'),
(5, 'Toshik', 'Parihar', '1458457420', '4.45E+13', '2017-12-06 07:02:38', 0, 'Male', 'votive.toshik@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `otd_contractdoc`
--

CREATE TABLE IF NOT EXISTS `otd_contractdoc` (
`ct_id` int(11) NOT NULL,
  `ct_name` varchar(255) NOT NULL,
  `ct_status` tinyint(4) NOT NULL,
  `ct_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_contractdoc`
--

INSERT INTO `otd_contractdoc` (`ct_id`, `ct_name`, `ct_status`, `ct_date_created`) VALUES
(1, 'testing', 1, '2017-12-11 10:07:31'),
(2, 'testing2', 1, '2017-12-11 10:07:50'),
(3, 'testing 3', 1, '2017-12-11 10:10:00'),
(4, 'TEsting with Consting', 1, '2017-12-11 11:50:00');

-- --------------------------------------------------------

--
-- Table structure for table `otd_debitcard_images`
--

CREATE TABLE IF NOT EXISTS `otd_debitcard_images` (
`dbt_id` int(11) NOT NULL,
  `dbt_image_path` varchar(255) NOT NULL,
  `dbt_image_link` text NOT NULL,
  `dbt_display_order` int(11) NOT NULL,
  `dbt_date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dbt_status` tinyint(4) NOT NULL COMMENT '0=>Disable, 1=> Enable'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_debitcard_images`
--

INSERT INTO `otd_debitcard_images` (`dbt_id`, `dbt_image_path`, `dbt_image_link`, `dbt_display_order`, `dbt_date_create`, `dbt_status`) VALUES
(5, 'SzsvCeLjNn.jpg', '#', 1, '2017-11-02 10:48:55', 1),
(6, '0oninX6RE6.jpg', '#', 2, '2017-11-02 10:54:01', 1),
(7, 'MXCqz4JHwy.jpg', '#', 3, '2017-11-02 11:32:36', 1),
(8, 'VOK5jSU7jR.jpg', '#', 4, '2017-11-02 11:32:47', 1),
(9, 'uWpPImzpiK.jpg', '#', 5, '2017-11-02 11:33:01', 1),
(10, 'apzZzuCRSq.jpg', '#', 6, '2017-11-02 11:33:10', 1),
(11, 'u80T1YR2UU.jpg', '#', 6, '2017-11-02 11:33:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_email_templates`
--

CREATE TABLE IF NOT EXISTS `otd_email_templates` (
`tmplate_id` int(11) NOT NULL,
  `tmplate_name` varchar(255) NOT NULL,
  `tmplate_subject` varchar(255) NOT NULL,
  `tmplate_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tmplate_variable` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tmplate_dt_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_email_templates`
--

INSERT INTO `otd_email_templates` (`tmplate_id`, `tmplate_name`, `tmplate_subject`, `tmplate_content`, `tmplate_variable`, `tmplate_dt_created`) VALUES
(11, 'Professional User Sign up', 'Registration Confirm', '<p>Hello dear %%Firstname%% %%LastName%%,</p>\\n\\n<p>Your registration process comepleted on otourdemoi</p>\\n\\n<p>Your email address is %%Email%%,</p>\\n\\n<p>%%Phone%%<br />\\n%%Gender%%</p>\\n\\n<p>%%BusinessName%%,</p>\\n\\n<p>%%CompanyNumber%%,</p>\\n\\n<p>%%Address%%</p>\\n\\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\\n\\n<p>asdfasdfasdf</p>\\n', '', '2017-10-06 12:35:04'),
(12, 'Personal User Signup', 'Registration Confirm', '<p>Hello dear %%Firstname%% %%LastName%%,</p>\\n\\n<p>Your registration process comepleted on otourdemoi</p>\\n\\n<p>Your email address is %%Email%%,</p>\\n\\n<p>%%Phone%%<br />\\n%%Gender%%</p>\\n\\n<p>%%Address%%</p>\\n\\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\\n\\n<p>Please click link to activate your account. %%ActivationLink%%</p>\\n', '', '2017-10-06 12:35:04'),
(13, 'Forgot Password', 'Reset Password', '<p>Hello dear %%Firstname%%&nbsp;%%LastName%%<br />\\n%%Email%%<br />\\n%%Address%%<br />\\n%%Phone%%<br />\\n%%Gender%%<br />\\nPlease find your temporary password below</p>\\n\\n<p>%%TempPassword%%</p>\\n\\n<p>Please login to your dashboard with temp password and Change your password.</p>\\n\\n<p style="text-align: justify;">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\\n', '', '2017-10-07 06:07:09'),
(14, 'Sign up with social media', 'Social Signup Confirmed', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\n\n<p>123</p>\n\n<p>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%Gender%%</p>\n\n<p>%%TempPassword%%</p>\n', '', '2017-10-07 10:45:17'),
(15, 'Report Review', 'Reported on review', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\n\n<p>%%ComplainUserId%%<br />\n%%ReviewId%%<br />\n%%ReviewDetails%%<br />\n%%ReviewLink%%</p>\n', '', '2017-10-07 10:45:17'),
(16, 'Claim Business Profile', 'Business Profile Claimed', '<h2>Claim Business Profile</h2>\\n\\n<p>Hello %%Firstname%% %%LastName%%,</p>\\n\\n<p>We have received you claimed request for Business&nbsp;%%BusinessName%% and business number %%CompanyNumber%%</p>\\n\\n<p>Our executive will contact you soon on provided email %%Email%%</p>\\n\\n<p>Your extra details</p>\\n\\n<p>%%Address%%<br />\\n%%Phone%%<br />\\n%%Gender%%</p>\\n\\n<p><br />\\n%%BusinessName%%<br />\\n%%CompanyNumber%%</p>\\n\\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with.</p>\\n', '', '2017-10-07 10:45:17'),
(17, 'Create Event Email To User', 'Event Published', '<p>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%EventName%%<br />\n%%EventType%%<br />\n%%PublishStartDate%%<br />\n%%PublishEndDate%%<br />\n%%EventStartDate%%<br />\n%%EventEndDate%%<br />\n%%Price%%<br />\n%%Persons%%<br />\n%%Description%%<br />\n%%EventAddress%%</p>\n\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\n', '', '2017-10-07 10:45:17'),
(18, 'Create Event Email To Follower', 'Event Created', '<p>%%Firstname%%<br />\\n%%LastName%%<br />\\n%%Email%%<br />\\n%%BusinessName%%<br />\\n%%Address%%<br />\\n%%Phone%%<br />\\n%%Gender%%<br />\\n%%EventName%%<br />\\n%%EventType%%<br />\\n%%PublishStartDate%%<br />\\n%%PublishEndDate%%<br />\\n%%EventStartDate%%<br />\\n%%EventEndDate%%<br />\\n%%Price%%<br />\\n%%Persons%%<br />\\n%%Description%%<br />\\n%%EventAddress%%</p>\\n\\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\\n', '', '2017-10-07 10:45:17'),
(19, 'Purchase option Payment Success', 'Purchase Option Payment Success', '<h2>Option Purchase Success</h2>\n\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\n\n<p>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%CompanyNumber%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%PurchaseDate%%<br />\n%%PurchaseDetails%%</p>\n', '', '2017-10-07 10:45:17'),
(20, 'Purchase option activation', 'Option Activated', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\n\n<p>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%CompanyNumber%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%OptionName%%<br />\n%%OptionsDescription%%<br />\n%%PurchaseDate%%<br />\n%%OptionDuration%%<br />\n%%OptionPrice%%<br />\n%%OptionsVisibleCity%%<br />\n%%OptionActiveStartDate%%<br />\n%%OptionActiveEndDate%%</p>\n', '%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%CompanyNumber%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%OptionName%%<br />\n%%OptionsDescription%%<br />\n%%PurchaseDate%%<br />\n%%OptionDuration%%<br />\n%%OptionPrice%%<br />\n%%OptionsVisibleCity%%<br />\n%%OptionActiveStartDate%%<br />\n%%OptionActiveEndDate%%', '2017-10-07 10:45:17'),
(21, 'Empty Template', '', '', '', '2017-10-07 12:41:12'),
(22, 'Newsletter Subscription', 'Newsletter Subscription', '<p>%%Email%%<br />\\n%%Postalcode%%</p>\\n\\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It&nbsp;</p>\\n', '', '2017-10-10 10:34:34'),
(23, 'Purchase Option Activation By Admin', 'Option Activated By Admin', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p><b>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%CompanyNumber%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%OptionName%%<br />\n%%OptionsDescription%%<br />\n%%PurchaseDate%%<br />\n%%OptionDuration%%<br />\n%%OptionPrice%%<br />\n%%OptionsVisibleCity%%<br />\n%%OptionActiveStartDate%%<br />\n%%OptionActiveEndDate%%</b></p>\n', '%%Firstname%%<br>%%LastName%%<br>%%Email%%<br>%%BusinessName%%<br>%%CompanyNumber%%<br>%%Address%%<br>%%Phone%%<br>%%Gender%%<br>%%OptionName%%<br>%%OptionsDescription%%<br>%%PurchaseDate%%<br>%%OptionDuration%%<br>%%OptionPrice%%<br>%%OptionsVisibleCity%%<br>%%OptionActiveStartDate%%<br>%%OptionActiveEndDate%%', '2017-10-10 11:33:07'),
(24, 'Purchase Option Deactivation By Admin', 'Option Deactivated By Admin', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n\n<p><b>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%CompanyNumber%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%OptionName%%<br />\n%%OptionsDescription%%<br />\n%%PurchaseDate%%<br />\n%%OptionDuration%%<br />\n%%OptionPrice%%<br />\n%%OptionsVisibleCity%%<br />\n%%OptionActiveStartDate%%<br />\n%%OptionActiveEndDate%%</b></p>\n', '%%Firstname%%<br>%%LastName%%<br>%%Email%%<br>%%BusinessName%%<br>%%CompanyNumber%%<br>%%Address%%<br>%%Phone%%<br>%%Gender%%<br>%%OptionName%%<br>%%OptionsDescription%%<br>%%PurchaseDate%%<br>%%OptionDuration%%<br>%%OptionPrice%%<br>%%OptionsVisibleCity%%<br>%%OptionActiveStartDate%%<br>%%OptionActiveEndDate%%', '2017-10-10 11:33:07'),
(25, 'Email Footer', 'Footer', '<p style="margin:0 0 8px 0">Regards,</p>\n\n<p style="margin:0 0 8px 0">Sales Team</p>\n\n<p style="margin:0 0 8px 0"><a href="http://votivephp.in/VotiveYellowPages/" style="text-decoration:none;color:#dad9d9;padding-bottom:2px;display:inline-block;border-bottom:1px solid #d0cfcf;">http://votivephp.in/VotiveYellowPages/</a></p>\n', '', '2017-10-10 11:46:02'),
(26, 'Email Header', 'Header', '<div style="text-align:center"><a href="http://votivephp.in/VotiveYellowPages/" style="display:block;overflow:hidden;padding: 0 0 0 0;"><img src="http://votivephp.in/VotiveYellowPages/assets/img/logo.png" style="max-width:270px;width: 100%;" /> </a></div>\n\n<p>&nbsp;</p>\n', '', '2017-10-10 11:49:21'),
(27, 'Link Option to User', 'Option Linked By Admin', '<p>Otourdemoi.Pro n&rsquo;est pas un annuaire de professionnels comme il en existe d&eacute;j&agrave;&hellip; C&rsquo;est une communaut&eacute; unique visant &agrave; rapprocher les professionnels des particuliers en favorisant les &eacute;changes et en renfor&ccedil;ant les liens de confiance et de proximit&eacute;. Ensemble, agissons pour conserver des produits et services de qualit&eacute;, au juste prix et &agrave; deux pas de chez nous. Choisir l&rsquo;artisanat, c&rsquo;est choisir la qualit&eacute; et la confiance !</p>\n\n<p><strong>User Neme:</strong> %%Firstname%% %%LastName%%<br />\n<strong>Email</strong>: %%Email%%<br />\n<strong>Company Name</strong>: %%BusinessName%%<br />\n<strong>Company Address</strong>: %%Address%%<br />\n<strong>Phone Number</strong>: %%Phone%%<br />\n<strong>Gender</strong>: %%Gender%%<br />\n<strong>Linked Option</strong>: %%OptionsName%%</p>\n', '', '2017-10-26 19:06:40'),
(28, 'Form Option Selected By User', 'Form Option Selected', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum</p>\n\n<p>%%Firstname%%<br />\n%%LastName%%<br />\n%%Gender%%<br />\n%%Telephone%%<br />\n%%Budget%%<br />\n%%StartDate%%<br />\n%%Description%%<br />\n%%FormOptionLink%%</p>\n', '', '2017-11-01 10:59:00'),
(30, 'Purchase option not activated 3 Months reminder', 'Option Not Activated', 'test tested', '%%Firstname%%<br /> %%LastName%%<br /> %%Email%%<br /> %%BusinessName%%<br /> %%CompanyNumber%%<br /> %%Address%%<br /> %%Phone%%<br /> %%Gender%%<br /> %%PurchaseDate%%<br /> %%PurchaseDetails%%', '2017-11-23 07:11:39'),
(31, 'Purchase option not activated 6 Months reminder', 'Option Not Activated', 'test', '%%Firstname%%<br /> %%LastName%%<br /> %%Email%%<br /> %%BusinessName%%<br /> %%CompanyNumber%%<br /> %%Address%%<br /> %%Phone%%<br /> %%Gender%%<br /> %%PurchaseDate%%<br /> %%PurchaseDetails%%', '2017-11-23 07:13:09'),
(32, '7 Days Before renew reminder', 'Option soon come to an end', 'Your option will soon come to an end.', '', '2017-11-23 10:13:27'),
(33, 'Slimpay Sign Mandate and First Payment Success', 'Purchase Option Payment Success', '<h2>Option Purchase Success</h2>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\r\n\r\n<p>%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%PurchaseDetails%%<br />\r\n%%MandateReference%%<br />\r\n%%TotalAmount%%<br />\r\n%%SignDate%%</p>\r\n', '<p>%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%PurchaseDetails%%<br />\r\n%%MandateReference%%<br />\r\n%%TotalAmount%%<br />\r\n%%SignDate%%</p>', '2017-12-08 04:43:19'),
(34, 'Slimpay Active Mandate Purchase Success', 'Slimpay Purchase Option Payment Success', '<h2>Option Purchase Transaction Success</h2>\n\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\n\n<p>%%Firstname%%<br />\n%%LastName%%<br />\n%%Email%%<br />\n%%BusinessName%%<br />\n%%CompanyNumber%%<br />\n%%Address%%<br />\n%%Phone%%<br />\n%%Gender%%<br />\n%%PurchaseDate%%<br />\n%%PurchaseDetails%%</p>\n', '<p>%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%PurchaseDetails%%</p>', '2017-12-08 04:47:31'),
(35, 'Slimpay Payment Cancel', 'Order Cancelled', '<h2>Option Purchase Transaction Cancelled</h2>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\r\n\r\n<p>%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%PurchaseDetails%%</p>', '<p>%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%PurchaseDetails%%</p>', '2017-12-08 06:09:31'),
(36, 'Slimpay Transaction Failed', 'Slimpay Transaction Failed', '<h2>Option Purchase Transaction Cancelled</h2>\r\n\r\n<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with</p>\r\n\r\n<p>%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%MandateReference%%<br />\r\n%%TotalAmount%%<br />\r\n%%SignDate%%<br />\r\n%%PurchaseDetails%%</p>\r\n', '%%Firstname%%<br />\r\n%%LastName%%<br />\r\n%%Email%%<br />\r\n%%BusinessName%%<br />\r\n%%CompanyNumber%%<br />\r\n%%Address%%<br />\r\n%%Phone%%<br />\r\n%%Gender%%<br />\r\n%%PurchaseDate%%<br />\r\n%%MandateReference%%<br />\r\n%%TotalAmount%%<br />\r\n%%SignDate%%<br />\r\n%%PurchaseDetails%%', '2017-12-08 10:07:52');

-- --------------------------------------------------------

--
-- Table structure for table `otd_events`
--

CREATE TABLE IF NOT EXISTS `otd_events` (
`event_id` int(11) NOT NULL,
  `event_name` varchar(255) NOT NULL,
  `event_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `event_date` date NOT NULL,
  `event_start_time` varchar(255) NOT NULL,
  `event_end_time` varchar(255) NOT NULL,
  `event_type` int(10) unsigned NOT NULL,
  `created_by_user` int(11) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `event_status` int(11) NOT NULL,
  `event_is_free` tinyint(4) NOT NULL COMMENT '0-no, 1-yes',
  `event_type_id` int(10) unsigned NOT NULL,
  `event_publish_start` date NOT NULL,
  `event_publish_end` date NOT NULL,
  `event_start_date` datetime NOT NULL,
  `event_end_date` datetime NOT NULL,
  `event_per_ltd` int(11) NOT NULL,
  `event_price` varchar(255) NOT NULL,
  `event_goodplace` tinyint(4) NOT NULL,
  `event_address` varchar(255) NOT NULL,
  `event_lat` varchar(255) NOT NULL,
  `event_long` varchar(255) NOT NULL,
  `event_img1` varchar(255) NOT NULL,
  `event_img2` varchar(255) NOT NULL,
  `event_img3` varchar(255) NOT NULL,
  `event_img4` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_events`
--

INSERT INTO `otd_events` (`event_id`, `event_name`, `event_description`, `event_date`, `event_start_time`, `event_end_time`, `event_type`, `created_by_user`, `created_on`, `event_status`, `event_is_free`, `event_type_id`, `event_publish_start`, `event_publish_end`, `event_start_date`, `event_end_date`, `event_per_ltd`, `event_price`, `event_goodplace`, `event_address`, `event_lat`, `event_long`, `event_img1`, `event_img2`, `event_img3`, `event_img4`) VALUES
(1, 'Locahost', 'as saasdsdf sdfsdfsadf', '0000-00-00', '', '', 0, 120, '2017-11-16 06:36:38', 1, 1, 3, '2017-11-16', '2017-11-24', '2017-11-19 08:00:00', '2017-11-23 09:00:00', 1245, '1241', 0, 'Dunkirk, France', '51.0343684', '2.3767763', 'r3vB88XncJp5fybclP9G7gD1E6t9w2.jpg', '2zQj9fcC9DHcTHUKJVWvugnbvdk0EI.jpg', 'nGgeCqaLEYILzxLbWPDwU4bJ1ZLhoM.jpg', 'c8BcRMMqyatdELOKZymMV0fvg41Eod.jpg'),
(2, '', '', '0000-00-00', '', '', 0, 120, '2017-11-20 07:16:35', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '', '', '', '', '', '', ''),
(3, '', '', '0000-00-00', '', '', 0, 120, '2017-11-24 05:26:23', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '', '', '', '', '', '', ''),
(4, '', '', '0000-00-00', '', '', 0, 120, '2017-11-27 07:59:04', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 0, '', '', '', '', '', '', ''),
(5, '', '', '0000-00-00', '', '', 0, 120, '2017-11-27 07:59:04', 0, 0, 0, '0000-00-00', '0000-00-00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '', 1, '', '', '', '', '', '', ''),
(6, 'My Name is Lakhan', '', '0000-00-00', '', '', 0, 120, '2018-02-23 12:05:48', 1, 0, 5, '2017-12-11', '2017-12-18', '2017-12-19 04:00:00', '2017-12-21 18:45:00', 12, '124', 1, 'Dunkirk, France', '51.0343684', '2.3767763', 'GFx5RK3TG06xhEwZdpDIBtjH1BSozZ.jpeg', 'bMQmzjWWTYrques3b2zxmSS6PIYsW1.jpeg', 'WPxLoWYqpZ6WAIeN1keYiysVwBICic.jpeg', '');

-- --------------------------------------------------------

--
-- Table structure for table `otd_event_type`
--

CREATE TABLE IF NOT EXISTS `otd_event_type` (
`evt_type_id` int(10) unsigned NOT NULL,
  `evt_type` varchar(255) NOT NULL,
  `evt_status` tinyint(4) NOT NULL,
  `evt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_event_type`
--

INSERT INTO `otd_event_type` (`evt_type_id`, `evt_type`, `evt_status`, `evt_date`) VALUES
(1, 'Team Building Events', 1, '2017-09-12 06:15:19'),
(2, 'Trade Shows', 1, '2017-09-12 06:15:19'),
(3, 'Business Dinners', 1, '2017-09-12 06:15:19'),
(4, 'Press Conferences', 1, '2017-09-12 06:15:19'),
(5, 'Incentive Travel', 1, '2017-09-12 06:15:19'),
(6, 'Opening Ceremonies', 1, '2017-09-12 06:15:19'),
(7, 'Product Launches', 1, '2017-09-12 06:15:19'),
(8, 'Theme Parties', 1, '2017-09-12 06:15:19'),
(9, 'VIP Events', 1, '2017-09-12 06:15:19'),
(10, 'Trade Fairs', 1, '2017-09-12 06:15:19'),
(11, 'test Event Type', 1, '2017-09-25 12:28:40');

-- --------------------------------------------------------

--
-- Table structure for table `otd_footer_content`
--

CREATE TABLE IF NOT EXISTS `otd_footer_content` (
`ft_id` int(11) NOT NULL,
  `label1` varchar(255) NOT NULL,
  `label2` varchar(255) NOT NULL,
  `label3` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `label4` varchar(255) NOT NULL,
  `label5` varchar(255) NOT NULL,
  `label6` varchar(255) NOT NULL,
  `label7` varchar(255) NOT NULL,
  `label8` varchar(255) NOT NULL,
  `label9` varchar(255) NOT NULL,
  `label10` varchar(255) NOT NULL,
  `label11` varchar(255) NOT NULL,
  `label12` varchar(255) NOT NULL,
  `label13` varchar(255) NOT NULL,
  `label14` varchar(255) NOT NULL,
  `label15` varchar(255) NOT NULL,
  `label16` varchar(255) NOT NULL,
  `label17` varchar(255) NOT NULL,
  `label18` varchar(255) NOT NULL,
  `label19` varchar(255) NOT NULL,
  `label20` varchar(255) NOT NULL,
  `label21` varchar(255) NOT NULL,
  `label22` varchar(255) NOT NULL,
  `label23` varchar(255) NOT NULL,
  `label24` varchar(255) NOT NULL,
  `label25` varchar(255) NOT NULL,
  `label26` varchar(255) NOT NULL,
  `label27` varchar(255) NOT NULL,
  `label28` varchar(255) NOT NULL,
  `label29` varchar(255) NOT NULL,
  `label30` varchar(255) NOT NULL,
  `label31` varchar(255) NOT NULL,
  `label32` varchar(255) NOT NULL,
  `label33` varchar(255) NOT NULL,
  `label34` varchar(255) NOT NULL,
  `label35` varchar(255) NOT NULL,
  `label36` varchar(255) NOT NULL,
  `label37` varchar(255) NOT NULL,
  `label38` varchar(255) NOT NULL,
  `label39` varchar(255) NOT NULL,
  `label40` varchar(255) NOT NULL,
  `label41` varchar(255) NOT NULL,
  `label42` varchar(255) NOT NULL,
  `label43` varchar(255) NOT NULL,
  `label44` varchar(255) NOT NULL,
  `label45` varchar(255) NOT NULL,
  `label46` varchar(255) NOT NULL,
  `label47` varchar(255) NOT NULL,
  `label48` varchar(255) NOT NULL,
  `label49` varchar(255) NOT NULL,
  `label50` varchar(255) NOT NULL,
  `label51` varchar(255) NOT NULL,
  `label52` varchar(255) NOT NULL,
  `label53` varchar(255) NOT NULL,
  `label54` varchar(255) NOT NULL,
  `label55` varchar(255) NOT NULL,
  `label56` varchar(255) NOT NULL,
  `label57` varchar(255) NOT NULL,
  `label58` varchar(255) NOT NULL,
  `label59` varchar(255) NOT NULL,
  `label60` varchar(255) NOT NULL,
  `label61` varchar(255) NOT NULL,
  `label62` varchar(255) NOT NULL,
  `label63` varchar(255) NOT NULL,
  `label64` varchar(255) NOT NULL,
  `label65` varchar(255) NOT NULL,
  `label66` varchar(255) NOT NULL,
  `label67` varchar(255) NOT NULL,
  `label68` varchar(255) NOT NULL,
  `label69` varchar(255) NOT NULL,
  `label70` varchar(255) NOT NULL,
  `label71` varchar(255) NOT NULL,
  `label72` varchar(255) NOT NULL,
  `label73` varchar(255) NOT NULL,
  `label74` varchar(255) NOT NULL,
  `label75` varchar(255) NOT NULL,
  `label76` varchar(255) NOT NULL,
  `label77` varchar(255) NOT NULL,
  `label78` varchar(255) NOT NULL,
  `label79` varchar(255) NOT NULL,
  `label80` varchar(255) NOT NULL,
  `label81` varchar(255) NOT NULL,
  `label82` varchar(255) NOT NULL,
  `label83` varchar(255) NOT NULL,
  `label84` varchar(255) NOT NULL,
  `label85` varchar(255) NOT NULL,
  `label86` varchar(255) NOT NULL,
  `label87` varchar(255) NOT NULL,
  `label88` varchar(255) NOT NULL,
  `label89` varchar(255) NOT NULL,
  `label90` varchar(255) NOT NULL,
  `label91` varchar(255) NOT NULL,
  `label92` varchar(255) NOT NULL,
  `label93` varchar(255) NOT NULL,
  `label94` varchar(255) NOT NULL,
  `label95` varchar(255) NOT NULL,
  `label96` varchar(255) NOT NULL,
  `label97` varchar(255) NOT NULL,
  `label98` varchar(255) NOT NULL,
  `label99` varchar(255) NOT NULL,
  `label100` varchar(255) NOT NULL,
  `label101` varchar(255) NOT NULL,
  `label102` varchar(255) NOT NULL,
  `label103` varchar(255) NOT NULL,
  `label104` varchar(255) NOT NULL,
  `label105` varchar(255) NOT NULL,
  `label106` varchar(255) NOT NULL,
  `label107` varchar(255) NOT NULL,
  `label108` varchar(255) NOT NULL,
  `label109` varchar(255) NOT NULL,
  `label110` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_footer_content`
--

INSERT INTO `otd_footer_content` (`ft_id`, `label1`, `label2`, `label3`, `label4`, `label5`, `label6`, `label7`, `label8`, `label9`, `label10`, `label11`, `label12`, `label13`, `label14`, `label15`, `label16`, `label17`, `label18`, `label19`, `label20`, `label21`, `label22`, `label23`, `label24`, `label25`, `label26`, `label27`, `label28`, `label29`, `label30`, `label31`, `label32`, `label33`, `label34`, `label35`, `label36`, `label37`, `label38`, `label39`, `label40`, `label41`, `label42`, `label43`, `label44`, `label45`, `label46`, `label47`, `label48`, `label49`, `label50`, `label51`, `label52`, `label53`, `label54`, `label55`, `label56`, `label57`, `label58`, `label59`, `label60`, `label61`, `label62`, `label63`, `label64`, `label65`, `label66`, `label67`, `label68`, `label69`, `label70`, `label71`, `label72`, `label73`, `label74`, `label75`, `label76`, `label77`, `label78`, `label79`, `label80`, `label81`, `label82`, `label83`, `label84`, `label85`, `label86`, `label87`, `label88`, `label89`, `label90`, `label91`, `label92`, `label93`, `label94`, `label95`, `label96`, `label97`, `label98`, `label99`, `label100`, `label101`, `label102`, `label103`, `label104`, `label105`, `label106`, `label107`, `label108`, `label109`, `label110`) VALUES
(1, 'Abonnez-vous à notre newsletter', 'Souscrire', 'Otourdemoi.Pro n’est pas un annuaire de professionnels comme il en existe déjà…\r\nC’est une communauté unique visant à rapprocher les professionnels des particuliers en favorisant les échanges et en renforçant les liens de confiance et de proximité.\r\nEnsemble, agissons pour conserver des produits et services de qualité, au juste prix et à deux pas de chez nous.\r\nChoisir l’artisanat, c’est choisir la qualité et la confiance !', 'Informations', 'Découvrir', 'Comptes professionnels', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(2, 'Login To My Account', 'Login with facebook', 'Login with google', 'Login', 'Forgot Password', 'New Account', 'Close', 'Email Address', 'Password', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(3, 'Create My Account', 'Create Personal Account', 'Select', 'Learn more', 'Create Professional Account', 'Select', 'Learn more', 'To receive the local newsletter of your new Otourdemoi newsletter', 'Thank you for informing:', 'Subscribe', 'I agree to receive communications from the companies and partners of Otourdemo', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(4, 'Forgot Password', 'Send Password Request', 'Close', 'Enter Email Address', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(5, 'Connexion', 'Inscription', 'Les Bons Plans', 'Mes Messages', 'Mes Notifications', 'Mon compte', 'Déconnexion', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(6, 'Create your account <strong>Professional</strong>', '(*) Required Fields', 'Otourdemoi.Pro is not a directory of professionals as it already exists ... It is a unique community aimed at bringing together professionals from the private sector by promoting exchanges and strengthening the ties of trust and proximity. Together, let us act to preserve quality products and services, at the right price and a stone''s throw from our home. Choosing crafts means choosing quality and trust!', '*GENRE', '*TELEPHONE', '*PRÉNOM', 'NOM', '*NOM DE L''ENTREPRISE', '*NUMÉRO DE SIRET', 'LANGUE', 'CHOIX DE VOTRE/VOS CATÉGROIE(S)', '*VEUILLEZ SAISIR VOTRE ADRESSE', '*Email', '*MOT DE PASSE', 'CONFIRMER LE MOT DE PASSE', '', 'Souhaitez-vous recevoir des offres promotionnelles de la part d’Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)', 'Souhaitez-vous recevoir sur votre mobile des offres promotionnelles de la part d’Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)', ' *Acceptez les conditions générales ', 'S''enregistrer', 'Already have an account?', 'Login here', 'Homme', 'Femme', 'Signup with Facebook', 'Signup With Google', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(7, 'Create your account <strong>Personal</strong>', 'Sign up With us', 'Otourdemoi.Pro is not a directory of professionals as it already exists ... It is a unique community aimed at bringing together professionals from the private sector by promoting exchanges and strengthening the ties of trust and proximity. Together, let us act to preserve quality products and services, at the right price and a stone''s throw from our home. Choosing crafts means choosing quality and trust!', 'Sigh up with Facebook', 'Sign up with Google', '(*) Required Fields', '*GENRE', 'Homme', 'Femme', '*PRÉNOM', 'NOM', '*Email', '*TELEPHONE', '*ADRESSE', '*MOT DE PASSE', '*CONFIRMER LE MOT DE PASSE', 'Souhaitez-vous recevoir des offres promotionnelles de la part d’Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)', 'Souhaitez-vous recevoir sur votre mobile des offres promotionnelles de la part d’Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)', ' *Acceptez les conditions générales ', 'S''enregistrer', 'Already have an account?', 'Login Here', 'Homme', 'Femme', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(8, 'Login To Your <strong>Account</strong>', 'Login Form', 'Otourdemoi.Pro is not a directory of professionals as it already exists ... It is a unique community aimed at bringing together professionals from the private sector by promoting exchanges and strengthening the ties of trust and proximity. Together, let us act to preserve quality products and services, at the right price and a stone''s throw from our home. Choosing crafts means choosing quality and trust!', 'Login with Facebook', 'Login with Google', 'Login', 'Forgot Password', 'New Account', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(9, 'Chercher, trouver et bien plus encore...', 'Première communauté entre particuliers et professionnels !', '', 'Selectionnez une catégorie', 'Mot-clés ', 'C''est parti', 'Entrez votre adresse', 'Les <span>catégories</span> à la une', 'Les entreprises à la une', 'Les entreprises les plus visitées', 'Les témoignages', 'View More', 'No details found', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(10, 'Yes We are Open', 'We are Closed', 'Site web', 'Suivez notre profil', 'Recommandez notre profil', 'Envoyez-nous un message', 'En ligne', 'Partagez', 'Description', 'Produits', 'Services', 'Autres', 'Nos Photos', 'Nos événements à venir', 'Les avis', 'Laisser un avis', 'Profiles similaires', 'Numéro(s)', 'Horaires d''ouvertures', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Informations complémentaires', 'Gérant', 'Date de création', 'SIRET', 'Langues parlées', 'UnFollow the Marchent', 'C''est votre entreprise ? Revendiquez le pour l''editer', 'Revendiquer mon profil', 'Afficher', 'Nos tarifs', 'Report This Reivew', 'No Reviews Available', 'Votes', '<b>YEARS</b> IN BUSINESS', 'Categories', 'Phone12', 'Not Available', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(11, ' Claim Your Business', '(*) Required Fields', 'Company Name', 'Company Number', '*PRÉNOM', 'Nom', '*Email', '*TELEPHONE', '*VEUILLEZ SAISIR VOTRE ADRESSE', ' *Acceptez les conditions générales ', 'S''enregistrer', '*GENRE', 'Homme', 'Femme', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(12, 'Entrez une adresse', 'Entrez une catégorie', 'Mot-clés', 'Filtres', 'Selectionnez vos filtres', 'Rechercher', '', 'Résultats', 'Aucun résultat', 'Voir la carte', 'Retour', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(13, 'Write a Review', 'Select Rating', 'Submit', 'Thank You', 'Your review complaint has been registered with admin.', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(14, 'To receive the local newsletter of your new Otourdemoi newsletter', 'Thank you for informing:', 'Votre email*', 'Votre code postal*', 'Subscribe Submit', 'I agree to receive communications from the companies and partners of Otourdemoi', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(15, 'PRÉNOM', 'NOM', 'LANGUE ', 'GENRE', 'DATE DE NAISSANCE', 'New Email', 'Password', 'Old Password', 'New Password', 'Confirm New Password', 'Password', 'Nom de l''entreprise', 'Numéro de SIRET', 'Site Web', 'Date de création', 'Telephone', 'Identifiant Twitter', 'Adresse mail du parrain', 'Choix de votre/vos catégroie(s)', 'Vos prestations', 'Veuillez saisir votre adresse', 'Decrire votre entreprise', 'Prices', 'Vos horaires d’ouverture', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Ajoutez vos photos', 'Titre', 'Message', 'Nom du Bon Plan', 'Event Type', 'Event Start Publishing', 'Event End Publishing', 'Event Start Date', 'Event End Date', 'Number Of Persons', 'Event Price', 'Event Address', 'Event details', 'Mise à jour', 'Change mail', 'Change Password', 'Delete My Account', 'Mise à jour', 'Add Number', 'Services', 'Product', 'Other', 'Upload123', 'And', 'Mise à jour', 'Upload', 'Envoyer', 'Event Images', ' Create Event', 'Close', 'Nombre de caractères', 'Update Event', 'Back', 'Afficher le numéro', 'UnFollow', 'Message', 'Live Chat', 'Sorry, Mes fevoris is empty.', 'No Events Available.', 'View', 'Sorry, Message box is empty.', 'started following you.', 'has organized an event.', 'has submited a review on you profile.', 'Sorry, Notification not available.', 'Buy This Option', 'Add to Cart:', 'Ajoutez au panier', 'Activer votre Bon Plan GRATUIT', 'Acheter un Bon Plan supplémentaire', 'Name', 'Date of Purchase', 'Activate', 'Duration Bought', 'Action', 'Activated', 'Edit', 'View Event', 'Delete', 'Download Receipt', 'Expired', 'Create Event', 'Activate', 'has Added mes option to your profile.', 'Receipt', ' Vue', 'Your Mandates', 'Mandate Reference', 'Mandate State', 'Date Signed', 'Action', 'View', 'Download Reference', 'Update Back Account', 'Your Documents', 'Document Name', 'Upload Date', 'Action', 'Download Document'),
(16, 'Thank You', 'Thanks for Recommendation!', 'Close', 'Thank You', 'You have already recommend this business', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(17, 'Thank you', 'Thank You for following business profile.', 'Close', 'Thank you', 'Hope you will follow us again.', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(18, 'Enter Your Message...', 'Reply', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(19, 'Event Date', 'Address', 'See plan for access', 'Price', 'Event Description', 'View Profile', 'Edit Event', 'Live Chat', 'Send Message', 'Facebook', 'Twitter', 'Google', 'Print This Page123', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(20, 'Add New Number', 'Number Type', 'Phone Number', 'Add', 'Close', 'Enter Type', 'Enter Phone Number', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(21, 'Registration Completed', 'Your registration was successfully. Please login to your account.', 'Login To Your Account', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(22, 'votive.toshik@gmail.com', 'serviceclient@otourdemoi.fr', 'Votive Yellow Pages', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(23, 'Thank you', 'Thank you for subscribing our newsletter.', 'Close', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(24, 'Thanks for Claiming Your Profile', 'Our Administrator witll review your claim and will contact your soon.', 'Explore More Business', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(25, '', '', 'Thanks for purchasing mes options for Your Business Profile. We will process your request and inform you soon on your registered email ID.', 'Back To Dashboard', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(26, 'Thank You', 'Thank You for Purchasing the Option. Please check your registered email. Our representative will contact you soon.', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(27, '', '', '<p style="text-align: justify;">Otourdemoi.Pro s&#39;appuie sur les derni&egrave;res normes de s&eacute;curit&eacute; et ne dispose &agrave; aucun moment de vos informations bancaires.</p>\n\n<p style="text-align: justify;">Otourdemoi.Pro ne collecte pas vos donn&eacute;es bancaires et a choisi des partenaires, &eacute;tablissements bancaires et de paiement, reconnus et assurant un niveau de s&eacute;curit&eacute; fort en conformit&eacute; avec la r&eacute;glementation Fran&ccedil;aise.</p>\n\n<p style="text-align: justify;">La transaction s&#39;effectue par paiement s&eacute;curis&eacute; standard SSL de nos partenaires, CREDIT AGRICOLE et Slimpay, &eacute;tablissements agr&eacute;&eacute;s par la banque de France.</p>\n', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(28, 'Registration Completed', 'Your registration was successfully. Please login to your account.', '', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(29, 'Password reset', 'Password Reset Link has been sent to your email id.', '', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(30, 'Option Name', 'Details', '<h3><strong>Sorry</strong>, Cart is empty. Please go to the <a href="Users/account/messoptions" >Mes Options</a> menu to purchase options.</h3>', 'Duration', 'Action', '€', 'Delete', 'Total Price', 'Pay Option', 'Go to Mess Option', 'Pay Now', 'Price', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(31, 'Select Your Payment Method', 'Nam sit amet sagittis diam, id maximus nibh.', '<p style="text-align: justify;"><strong><span style="font-family:Comic Sans MS,cursive;">Duis commodo ipsum quis ante venenatis rhoncus. Vivamus imperdiet felis ac facilisis hendrerit. </span></strong></p>\n\n<p style="text-align: justify;"><span style="font-family:Trebuchet MS,Helvetica,sans-serif;">Nulla eu elementum ex, ut pulvinar est. Nam aliquet et tortor sed aliquet. Pellentesque habitant morbi tristique senectus</span></p>\n\n<p style="text-align: justify;">et netus et malesuada fames ac turpis egestas. Integer sit amet aliquet leo. Sed aliquam ex id turpis mattis, sit amet porta augue laoreet. Duis euismod feugiat consectetur.</p>\n\n<p style="text-align: justify;">Maecenas sodales maximus mi sed placerat. Cras nec velit blandit, porta risus a, accumsan tellus. In hac habitasse platea dictumst. Curabitur mollis nisl sit amet pellentesque congue. Sed feugiat elit et nunc tempor eleifend. Sed ac porttitor ligula, nec efficitur justo. Nam sit amet sagittis diam, id maximus nibh. Proin consequat, nibh ut semper rhoncus, lacus tortor laoreet ante.</p>\n', 'Back', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(32, 'Your Information', 'Mr', 'Miss', 'Mrs', 'Telephone', 'Prenom', 'Nom', 'NOM DE L''ENTREPRISE', 'NUMÉRO DE SIRET', 'Email', 'VEUILLEZ SAISIR VOTRE ADRESSE', 'City', 'Postal Code', 'By Clicking this you are agree with Otourdemoi terms & condtions.', 'Confirm', 'Close', 'Honorifics', '', 'Honorifics', 'Telephone', 'Prenom', 'Nom', 'NOM DE L''ENTREPRISE', 'NUMÉRO DE SIRET', 'Email', 'VEUILLEZ SAISIR VOTRE ADRESSE', 'City', 'Postal Code', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(33, 'Confirm', 'Close', 'You already has a mandate, do you want to use it?', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(34, 'Thank you', 'Your Transaction is under process.', 'Close', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
(35, 'Error Occurred', 'Sorry,  something went wrong, Please try after sometime.', 'Close', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `otd_formoption`
--

CREATE TABLE IF NOT EXISTS `otd_formoption` (
`frm_option_id` int(11) NOT NULL,
  `opt_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `FirstName` varchar(150) DEFAULT NULL,
  `LastName` text,
  `gender` varchar(6) DEFAULT NULL,
  `Telphone` varchar(12) DEFAULT NULL,
  `Budget` decimal(10,0) DEFAULT NULL,
  `StartDate` date DEFAULT NULL,
  `Description` text,
  `emailTo` varchar(150) DEFAULT NULL,
  `CreateDate` date DEFAULT NULL,
  `Status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_home_banner`
--

CREATE TABLE IF NOT EXISTS `otd_home_banner` (
`bn_id` int(11) NOT NULL,
  `bn_path` text NOT NULL,
  `bn_status` tinyint(4) NOT NULL COMMENT '0=>Unpublish, 1=>pulish',
  `bn_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_home_banner`
--

INSERT INTO `otd_home_banner` (`bn_id`, `bn_path`, `bn_status`, `bn_date_created`) VALUES
(1, 'slider1.jpg', 1, '2017-07-12 09:02:28'),
(2, 'slider1.jpg', 0, '2017-07-12 09:02:28'),
(3, 'ReTFuyOUaW.jpg', 0, '2017-07-12 09:40:52'),
(4, 'XRWKHvKBsu.jpg', 1, '2017-07-12 09:43:01'),
(5, 'Iv7tuxb9YF.jpg', 1, '2017-07-12 10:34:03'),
(6, 'qarqdXa07v.jpg', 0, '2017-07-14 09:30:18');

-- --------------------------------------------------------

--
-- Table structure for table `otd_home_cat_tab`
--

CREATE TABLE IF NOT EXISTS `otd_home_cat_tab` (
`tab_id` int(11) NOT NULL,
  `tab_name` varchar(255) NOT NULL,
  `tab_image` varchar(255) NOT NULL DEFAULT 'default.png'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_home_cat_tab`
--

INSERT INTO `otd_home_cat_tab` (`tab_id`, `tab_name`, `tab_image`) VALUES
(1, 'Travaux', 'OVS4OeQWty.png'),
(2, 'Shopping', 'default.png'),
(3, 'Services', 'default.png'),
(4, 'Santé', 'default.png'),
(5, 'Goûts et Saveurs', 'default.png'),
(6, 'Sortis et Loisirs', 'default.png');

-- --------------------------------------------------------

--
-- Table structure for table `otd_idea_box`
--

CREATE TABLE IF NOT EXISTS `otd_idea_box` (
  `bx_id` bigint(20) unsigned NOT NULL,
  `bx_title` varchar(255) NOT NULL,
  `bx_content` text NOT NULL,
  `bx_by_user` bigint(20) unsigned NOT NULL,
  `bx_status` tinyint(4) NOT NULL COMMENT '0=>disable, 1=>enable,3=>delete',
  `bx_flag` tinyint(4) NOT NULL COMMENT '0=>Unread, 1=>read',
  `bx_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_label_suggestion_list`
--

CREATE TABLE IF NOT EXISTS `otd_label_suggestion_list` (
  `label_id` int(11) NOT NULL,
  `label1` varchar(255) NOT NULL,
  `label2` varchar(255) NOT NULL,
  `label3` varchar(255) NOT NULL,
  `label4` varchar(255) NOT NULL,
  `label5` varchar(255) NOT NULL,
  `label6` varchar(255) NOT NULL,
  `label7` varchar(255) NOT NULL,
  `label8` varchar(255) NOT NULL,
  `label9` varchar(255) NOT NULL,
  `label10` varchar(255) NOT NULL,
  `label11` varchar(255) NOT NULL,
  `label12` varchar(255) NOT NULL,
  `label13` varchar(255) NOT NULL,
  `label14` varchar(255) NOT NULL,
  `label15` varchar(255) NOT NULL,
  `label16` varchar(255) NOT NULL,
  `label17` varchar(255) NOT NULL,
  `label18` varchar(255) NOT NULL,
  `label19` varchar(255) NOT NULL,
  `label20` varchar(255) NOT NULL,
  `label21` varchar(255) NOT NULL,
  `label22` varchar(255) NOT NULL,
  `label23` varchar(255) NOT NULL,
  `label24` varchar(255) NOT NULL,
  `label25` varchar(255) NOT NULL,
  `label26` varchar(255) NOT NULL,
  `label27` varchar(255) NOT NULL,
  `label28` varchar(255) NOT NULL,
  `label29` varchar(255) NOT NULL,
  `label30` varchar(255) NOT NULL,
  `label31` varchar(255) NOT NULL,
  `label32` varchar(255) NOT NULL,
  `label33` varchar(255) NOT NULL,
  `label34` varchar(255) NOT NULL,
  `label35` varchar(255) NOT NULL,
  `label36` varchar(255) NOT NULL,
  `label37` varchar(255) NOT NULL,
  `label38` varchar(255) NOT NULL,
  `label39` varchar(255) NOT NULL,
  `label40` varchar(255) NOT NULL,
  `label41` varchar(255) NOT NULL,
  `label42` varchar(255) NOT NULL,
  `label43` varchar(255) NOT NULL,
  `label44` varchar(255) NOT NULL,
  `label45` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_label_suggestion_list`
--

INSERT INTO `otd_label_suggestion_list` (`label_id`, `label1`, `label2`, `label3`, `label4`, `label5`, `label6`, `label7`, `label8`, `label9`, `label10`, `label11`, `label12`, `label13`, `label14`, `label15`, `label16`, `label17`, `label18`, `label19`, `label20`, `label21`, `label22`, `label23`, `label24`, `label25`, `label26`, `label27`, `label28`, `label29`, `label30`, `label31`, `label32`, `label33`, `label34`, `label35`, `label36`, `label37`, `label38`, `label39`, `label40`, `label41`, `label42`, `label43`, `label44`, `label45`) VALUES
(1, 'Prénom', 'Nom', 'LANGUE ', 'GENRE', 'DATE DE NAISSANCE', 'New Email', 'Password', 'Old Password', 'New Password', 'Confirm New Password', 'Password', 'Nom de l''entreprise', 'Numéro de SIRET', 'Site Web', 'Date de création', 'Telephone', 'Identifiant Twitter', 'Adresse mail du parrain', 'Choix de votre/vos catégroie(s)', 'Vos prestations', 'Veuillez saisir votre adresse', 'Decrire votre entreprise', 'Prices', 'Vos horaires d’ouverture', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday', 'Ajoutez vos photos', 'Titre', 'Message', 'Event name', 'Event Type', 'Event Start Publishing', 'Event End Publishing', 'Event Start Date', 'Event End Date', 'Number Of Persons', 'Event Price', 'Event Address', 'Event details', 'Event Images');

-- --------------------------------------------------------

--
-- Table structure for table `otd_languages`
--

CREATE TABLE IF NOT EXISTS `otd_languages` (
`lang_id` int(11) NOT NULL,
  `lang_name` varchar(255) NOT NULL,
  `lang_status` tinyint(4) NOT NULL COMMENT '0=>Disable, 1=>Enable'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_languages`
--

INSERT INTO `otd_languages` (`lang_id`, `lang_name`, `lang_status`) VALUES
(1, 'English', 1),
(2, 'French', 1),
(3, 'Spanish', 1),
(4, 'Arab', 1),
(5, 'Italion', 1),
(6, 'Germen', 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_notifications`
--

CREATE TABLE IF NOT EXISTS `otd_notifications` (
`nt_id` bigint(20) unsigned NOT NULL,
  `nt_by` bigint(20) unsigned NOT NULL,
  `nt_to` bigint(20) unsigned NOT NULL,
  `nt_read` tinyint(4) NOT NULL,
  `nt_flag` tinyint(4) NOT NULL,
  `nt_type` tinyint(4) NOT NULL COMMENT '1=>following, 2=>event ',
  `nt_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nt_rlt_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_notifications`
--

INSERT INTO `otd_notifications` (`nt_id`, `nt_by`, `nt_to`, `nt_read`, `nt_flag`, `nt_type`, `nt_date`, `nt_rlt_id`) VALUES
(1, 77, 29141, 0, 0, 3, '2017-11-03 05:56:23', 0),
(2, 77, 29141, 0, 0, 3, '2017-11-03 05:58:48', 0),
(3, 77, 29096, 0, 0, 3, '2017-11-08 04:48:18', 0),
(4, 77, 29096, 0, 0, 3, '2017-11-08 04:49:01', 0),
(5, 31440, 30506, 0, 0, 1, '2017-11-08 05:35:02', 0),
(6, 31440, 30506, 0, 0, 1, '2017-11-08 05:35:10', 0),
(7, 31440, 30506, 0, 0, 1, '2017-11-08 05:35:16', 0),
(8, 31440, 30506, 0, 0, 3, '2017-11-08 06:09:55', 0),
(9, 31440, 30506, 0, 0, 3, '2017-11-08 06:10:15', 0),
(10, 31440, 30506, 0, 0, 3, '2017-11-08 06:13:32', 0),
(11, 31440, 30506, 0, 0, 3, '2017-11-08 06:14:16', 0),
(12, 31440, 30506, 0, 0, 3, '2017-11-08 06:15:13', 0),
(13, 31440, 30506, 0, 0, 3, '2017-11-08 06:17:20', 0),
(14, 31440, 30506, 0, 0, 3, '2017-11-08 06:18:55', 0),
(15, 120, 110, 0, 0, 3, '2017-11-17 01:53:26', 0),
(16, 120, 110, 0, 0, 3, '2017-11-17 01:59:10', 0),
(17, 120, 110, 0, 0, 1, '2017-11-17 02:43:16', 0),
(18, 26, 120, 1, 1, 4, '2017-11-23 04:55:11', 0);

-- --------------------------------------------------------

--
-- Table structure for table `otd_options`
--

CREATE TABLE IF NOT EXISTS `otd_options` (
`option_id` smallint(6) NOT NULL,
  `option_name` varchar(100) NOT NULL,
  `option_value` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_options`
--

INSERT INTO `otd_options` (`option_id`, `option_name`, `option_value`) VALUES
(1, 'distance', '50');

-- --------------------------------------------------------

--
-- Table structure for table `otd_option_master`
--

CREATE TABLE IF NOT EXISTS `otd_option_master` (
`opt_id` int(11) NOT NULL,
  `opt_name` varchar(150) NOT NULL,
  `opt_description` text,
  `opt_type` int(11) NOT NULL DEFAULT '1',
  `opt_create_date` date NOT NULL,
  `opt_last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `opt_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_option_master`
--

INSERT INTO `otd_option_master` (`opt_id`, `opt_name`, `opt_description`, `opt_type`, `opt_create_date`, `opt_last_update`, `opt_status`) VALUES
(1, 'Head of List', '<p>The business user can purchase that option to be at the top of the list of results on the search page.The head of list option, after purchase, will allow user to always be visible in the top 10 results of the search page in his category and city.</p>\r\n', 1, '2017-08-18', '2017-08-18 09:40:35', 1),
(2, 'A LA UNE', 'The user can purchase that option to be visible in a slideshow on the search page.', 1, '0000-00-00', '2017-08-25 09:48:56', 1),
(3, 'A LA UNE PLUS', 'The user can purchase that option to be visible in a slideshow on the hompage', 1, '0000-00-00', '2017-08-25 12:39:19', 1),
(4, 'Profile Plus', 'The user can purchase that option to be able to write a description on 3000 characters. Option can be bought for a duration 1 year', 1, '0000-00-00', '2017-08-25 12:39:19', 1),
(5, 'Visibility', 'User can purchase that option to have a banner display on his profil in the search page list.', 1, '0000-00-00', '2017-08-29 12:39:02', 1),
(6, 'Pack Photos', 'User can purchase that option to be able to upload more picture to his profile. He can purchase 10, 20 or 50 pictures availbale for 1 year', 1, '0000-00-00', '2017-08-29 12:39:03', 1),
(7, 'Bon Plan', 'User can Activate a free event. After purchase he has to enter the evet detail then it will be published on his profile . He can also purchase  an additional event. Price of event is based on how many days event should be published. (daily price set by admin)', 1, '0000-00-00', '2017-09-12 05:58:22', 1),
(8, 'Bon Plan Plus', 'This is an event user can purchase. This event will be visible on your profile page but also on another search page called "Les Bons Plans". This is a search page that will list all "Bon Plan Plus" posted by business owners.', 1, '0000-00-00', '2017-09-12 05:58:23', 1),
(9, 'FORM Type of Option', 'This option does not actually do any functionnality on the website. It is a form that the user will enter information and press a submit button. After he press the submit button he gets a thank you message, he gets an email and admin gets and email.  This option will show in "gestion des options" table with view button showing submit date and time, and information entered in the form. 123456\r\n', 2, '2017-10-10', '2017-10-10 06:25:03', 1),
(10, 'Pack Email B2C', 'This option does not actually do any functionnality on the website. It is a form that the user will enter information and press a submit button. After he press the submit button he gets a thank you message, he gets an email and admin gets and email.  This option will show in "gestion des options" table with view button showing submit date and time, and information entered in the form.', 2, '0000-00-00', '2017-10-10 06:25:03', 3),
(11, 'Static Options', 'test testin and test tested', 3, '2017-10-25', '2017-10-25 11:56:37', 1),
(12, '', '', 3, '2017-10-26', '2017-10-26 05:25:18', 3),
(13, 'Diwali Dhamaka', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but ', 4, '2017-10-26', '2017-10-26 05:52:43', 3),
(14, 'testing123', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, ', 4, '2017-10-26', '2017-10-26 09:24:59', 3),
(15, 'testing', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, ', 4, '2017-10-26', '2017-10-26 09:58:48', 3),
(16, 'Combo Package', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, ', 4, '2017-10-26', '2017-10-26 10:06:15', 3),
(17, 'Diwali Dhamaka', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp', 4, '2017-10-27', '2017-10-27 05:46:53', 3),
(18, 'Package 2', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type sp', 4, '2017-10-27', '2017-10-27 06:00:06', 3),
(19, 'Package 3', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>\r\n\r\n<p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s,</p>\r\n\r\n<p>when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>\r\n\r\n<p>It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>\r\n\r\n<p>It was popularised in the&nbsp; Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>\r\n\r\n<p>Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the</p>\r\n', 4, '2017-10-27', '2017-10-27 06:46:15', 1),
(20, 'Package 1', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the&nbsp;\r\n\r\n\r\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the&nbsp;\r\n\r\n', 4, '2017-10-27', '2017-10-27 10:45:16', 1),
(21, 'My Event Package', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\r\n', 4, '2017-10-30', '2017-10-30 12:08:23', 1),
(22, 'sfsdaf', 'sfsadf\r\n', 4, '2017-11-02', '2017-11-02 06:27:45', 3),
(23, 'sdfsadf', 'sdfsdafsadfsadf\r\n', 4, '2017-11-15', '2017-11-15 10:36:20', 3),
(24, 'tests', 'astsdasdf\r\n', 4, '2017-11-15', '2017-11-15 10:40:14', 3),
(25, 'testing static options', '<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<p><span style="font-family:Comic Sans MS,cursive;"><strong>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the</strong></span></p>\r\n\r\n<p>industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n', 3, '2017-11-17', '2017-11-17 05:35:29', 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_option_price`
--

CREATE TABLE IF NOT EXISTS `otd_option_price` (
`opt_price_id` int(11) NOT NULL,
  `opt_text` varchar(255) NOT NULL,
  `opt_price_type` varchar(50) NOT NULL,
  `opt_qnty` varchar(255) NOT NULL,
  `opt_price` float NOT NULL,
  `opt_is_package` int(11) NOT NULL,
  `opt_type_id` int(11) NOT NULL,
  `opt_pr_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_option_price`
--

INSERT INTO `otd_option_price` (`opt_price_id`, `opt_text`, `opt_price_type`, `opt_qnty`, `opt_price`, `opt_is_package`, `opt_type_id`, `opt_pr_status`) VALUES
(1, 'Day per Day', '1 Day', '1', 2, 0, 1, 0),
(2, '1 Week', '1 Week', '1', 45, 0, 1, 1),
(3, '1 Month', '1 Month', '1', 65, 0, 1, 1),
(4, '6 Months', '6 Months', '1', 181, 0, 1, 1),
(5, 'Day per Day', '1 Day', '1', 10, 0, 2, 1),
(6, ' 1 Week', ' 1 Week', '1', 45, 0, 2, 1),
(7, '1 Month', '1 Month', '1', 65, 0, 2, 1),
(8, '6 Months', '6 Months', '1', 181, 0, 2, 1),
(9, 'Day per Day', '1 Day', '1', 10, 0, 3, 1),
(10, ' 1 Week', ' 1 Week', '1', 45, 0, 3, 1),
(11, '1 Month', '1 Month', '1', 65, 0, 3, 1),
(12, '6 Months', '6 Months', '1', 181, 0, 3, 1),
(13, '1 Year', '1 Year', '1', 200, 0, 4, 1),
(14, 'Day per Day', '1 Day', '1', 25, 0, 5, 1),
(15, '1 Week', '1 Week', '1', 75, 0, 5, 1),
(16, '1 Month', '1 Month', '1', 300, 0, 5, 1),
(17, '6 Months', '6 Months', '1', 500, 0, 5, 1),
(18, '10 Pictures', '1 Year', '10', 20, 0, 6, 1),
(19, '20 Pictures', '1 Year', '20', 65, 0, 6, 1),
(20, '50 Pictures', '1 Year', '50', 150, 0, 6, 1),
(22, 'Day per Day', '1 Day', '1', 15, 0, 8, 1),
(23, '1 Week', '1 Week', '1', 90, 0, 8, 1),
(24, '1 Month', '1 Month', '1', 350, 0, 8, 1),
(25, '6 Months', '6 Months', '1', 1500, 0, 8, 1),
(26, 'Day per Day', '1 Day', '1', 15, 0, 7, 1),
(27, '1 Week', '1 Week', '1', 90, 0, 7, 1),
(28, '1 Month', '1 Month', '1', 350, 0, 7, 1),
(29, '6 Months', '6 Months', '1', 1500, 0, 7, 1),
(30, '1000 Mails', '1 Month', '1', 280, 0, 10, 1),
(31, '4000 Mails', '1 Month', '1', 790, 0, 10, 1),
(32, '1 Day only', '', '', 150, 0, 11, 1),
(33, '15 Days', '', '', 200, 0, 11, 1),
(34, '1 Year', '1 Year', '1', 1500, 1, 1, 1),
(35, '1 Year', '1 Year', '1', 150, 1, 15, 3),
(36, '1 Year', '1 Year', '1', 150, 1, 16, 1),
(37, '1 Year', '1 Year', '1', 1500, 1, 17, 1),
(38, '1 Year', '1 Year', '1', 2000, 1, 18, 1),
(39, '1 Year', '1 Year', '1', 120, 1, 19, 1),
(40, '1 Year', '1 Year', '1', 200, 1, 20, 1),
(41, '1 Year', '1 Year', '1', 100, 1, 21, 1),
(42, '1 Year', '1 Year', '1', 100, 1, 22, 1),
(43, '1 Year', '1 Year', '1', 150, 1, 23, 1),
(44, '1250', '1 Year', '1', 150, 1, 24, 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_package_options`
--

CREATE TABLE IF NOT EXISTS `otd_package_options` (
`pkg_id` int(11) NOT NULL,
  `pkg_package_id` int(11) NOT NULL,
  `pkg_inc_optid` int(11) NOT NULL,
  `pkg_price_optid` int(11) NOT NULL,
  `pkg_opt_duration` varchar(255) NOT NULL,
  `pkg_opt_qty` int(11) NOT NULL DEFAULT '1',
  `pkg_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_package_options`
--

INSERT INTO `otd_package_options` (`pkg_id`, `pkg_package_id`, `pkg_inc_optid`, `pkg_price_optid`, `pkg_opt_duration`, `pkg_opt_qty`, `pkg_status`) VALUES
(21, 20, 2, 0, '7 Days', 1, 1),
(22, 20, 5, 0, '7 Days', 1, 1),
(23, 20, 6, 0, '7 Days', 10, 1),
(24, 21, 7, 0, '7 Days', 1, 1),
(25, 21, 8, 0, '7 Days', 1, 1),
(63, 19, 1, 0, '1 Day', 1, 1),
(64, 19, 2, 0, '5 Days', 1, 1),
(65, 19, 4, 0, '15 Days', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_page_contents`
--

CREATE TABLE IF NOT EXISTS `otd_page_contents` (
`pg_id` bigint(20) unsigned NOT NULL,
  `pg_title` varchar(255) NOT NULL,
  `pg_meta_tag` varchar(255) NOT NULL,
  `page_url` varchar(255) NOT NULL,
  `meta_title` varchar(100) NOT NULL,
  `meta_description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `meta_keyword` varchar(255) NOT NULL,
  `pg_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `pg_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>page, 2=>article',
  `pg_cat` int(11) NOT NULL COMMENT '1=>information, 2=>discover, 3=>professionalaccount, 0=>none',
  `pg_status` tinyint(4) NOT NULL COMMENT '0=>disable, 1=>enable',
  `pg_display_number` int(11) NOT NULL DEFAULT '1',
  `pg_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_page_contents`
--

INSERT INTO `otd_page_contents` (`pg_id`, `pg_title`, `pg_meta_tag`, `page_url`, `meta_title`, `meta_description`, `meta_keyword`, `pg_content`, `pg_type`, `pg_cat`, `pg_status`, `pg_display_number`, `pg_date_created`) VALUES
(1, 'About', 'About', '', '', '', '', '<p style="text-align: justify;">This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p style="text-align: justify;">Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC</p>\\r\\n\\r\\n<p style="text-align: justify;">Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>\\r\\n\\r\\n<p style="text-align: justify;">Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>\\r\\n', 1, 1, 1, 1, '2017-07-03 07:05:16'),
(2, 'Terms of Use', 'Terms-of-Use', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old.</p>\\r\\n\\r\\n<p>Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage,</p>\\r\\n\\r\\n<p>and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n', 1, 1, 1, 2, '2017-07-03 07:15:39'),
(3, 'Legal Notice', 'Legal-Notice', '', '', '', '', '<p>and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage,</p>\\r\\n\\r\\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in</p>\\r\\n', 1, 1, 1, 3, '2017-07-03 07:41:18'),
(4, 'Earn money with sponsorship', 'Earn-money-with-sponsorship', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a<br />\\r\\nLatin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.</p>\\r\\n\\r\\n<p>&nbsp;Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n', 1, 1, 1, 4, '2017-07-03 07:42:45'),
(5, 'Frequently Asked Questions', 'Frequently-Asked-Questions', '', '', '', '', '<p>&nbsp;and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>&nbsp;Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good</p>\\r\\n\\r\\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem</p>\\r\\n', 1, 2, 1, 1, '2017-07-03 07:43:36'),
(6, 'Why Create a User Account', 'Why-Create-a-User-Account', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem</p>\\r\\n\\r\\n<p>&nbsp;Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero</p>\\r\\n\\r\\n<p>written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n', 1, 2, 1, 2, '2017-07-03 07:44:19'),
(7, 'Support the Fondation Hopitaux de France', 'Support-the-Fondation-Hopitaux-de-France', '', '', '', '', '<p>written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>um passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero,</p>\\r\\n\\r\\n<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ips</p>\\r\\n', 1, 2, 0, 3, '2017-07-03 07:45:37'),
(8, 'Contact Us', 'Contact-Us', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ips</p>\\r\\n\\r\\n<p>um passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>&nbsp;</p>\\r\\n', 1, 2, 0, 4, '2017-07-03 07:46:32'),
(9, 'Why Create a PRO Account', 'Why-Create-a-PRO-Account', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32<br />\\r\\ntheory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n', 1, 3, 0, 1, '2017-07-03 07:47:27'),
(10, 'Claim your business page', 'Claim-your-business-page', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32<br />\\r\\ntheory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n', 1, 3, 1, 2, '2017-07-03 07:48:19'),
(11, 'Advice to attract more customers', 'Advice-to-attract-more-customers', '', '', '', '', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32<br />\\r\\ntheory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n\\r\\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\\r\\n', 1, 3, 1, 3, '2017-07-03 07:48:47'),
(12, 'Boost your profile by advertising', 'Boost-your-profile-by-advertising', 'sdfasdfasdfsadfsdaf', 'sdfsdfsadf', 'sdfsdaf', 'sadfsadf sad sadf sdafsadf', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source.&nbsp;&nbsp; &nbsp; Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32<br />\r\n\\r\\ntheory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\r\n\r\n<p>\\r\\n\\r\\n</p>\r\n\r\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\r\n\r\n<p>\\r\\n\\r\\n</p>\r\n\r\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\r\n\r\n<p>\\r\\n\\r\\n</p>\r\n\r\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\r\n\r\n<p>\\r\\n\\r\\n</p>\r\n\r\n<p>theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\r\n\r\n<p>\\r\\n</p>\r\n', 1, 3, 1, 4, '2017-07-03 07:49:16');

-- --------------------------------------------------------

--
-- Table structure for table `otd_page_diff_lang_content`
--

CREATE TABLE IF NOT EXISTS `otd_page_diff_lang_content` (
`ct_id` bigint(20) unsigned NOT NULL,
  `ct_heading` varchar(255) NOT NULL,
  `ct_subheading` varchar(255) NOT NULL,
  `ct_content` text NOT NULL,
  `ct_page_title` varchar(255) NOT NULL,
  `ct_section` varchar(255) NOT NULL,
  `ct_language` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_page_diff_lang_content`
--

INSERT INTO `otd_page_diff_lang_content` (`ct_id`, `ct_heading`, `ct_subheading`, `ct_content`, `ct_page_title`, `ct_section`, `ct_language`) VALUES
(1, 'Find Nearby Attractions', 'Expolore top-rated attractions, activities and more', '', 'Home', 'Banner', 'English'),
(2, 'Find Nearby Attractions', 'Expolore top-rated attractions, activities and more', '', 'Home', 'Banner', 'French'),
(3, 'Companies Upfront', 'Companies Upfront', '', 'Home', 'Companies Upfront', 'English'),
(4, 'Companies Upfront', 'Companies Upfront', '', 'Home', 'Companies Upfront', 'French'),
(5, 'Most popular companies', '', '', 'Home', 'Popular companies', 'English'),
(6, 'Most popular companies', '', '', 'Home', 'Popular companies', 'French'),
(7, 'Testimonial', '', '', 'Home', 'Testimonial', 'English'),
(8, 'Testimonial', '', '', 'Home', 'Testimonial', 'French');

-- --------------------------------------------------------

--
-- Table structure for table `otd_rating`
--

CREATE TABLE IF NOT EXISTS `otd_rating` (
`id` int(11) NOT NULL,
  `rate` int(11) DEFAULT NULL,
  `user_id` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_recommend_merchant`
--

CREATE TABLE IF NOT EXISTS `otd_recommend_merchant` (
`rec_id` int(11) NOT NULL,
  `recommend_by_user_id` int(11) NOT NULL,
  `recommended_user_id` int(11) NOT NULL,
  `recommend_to_email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_social_links`
--

CREATE TABLE IF NOT EXISTS `otd_social_links` (
`sc_id` int(11) NOT NULL,
  `sc_header` varchar(255) NOT NULL,
  `sc_links` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_social_links`
--

INSERT INTO `otd_social_links` (`sc_id`, `sc_header`, `sc_links`) VALUES
(1, 'Facebook', ''),
(2, 'Twitter', ''),
(3, 'Instagram', '');

-- --------------------------------------------------------

--
-- Table structure for table `otd_testimonials`
--

CREATE TABLE IF NOT EXISTS `otd_testimonials` (
`tst_id` bigint(20) unsigned NOT NULL,
  `tst_by` varchar(255) NOT NULL,
  `tst_designation` varchar(255) NOT NULL,
  `tst_content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tst_status` tinyint(4) NOT NULL COMMENT '0=>disable, 1=>published, 3=>deleted',
  `tst_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_testimonials`
--

INSERT INTO `otd_testimonials` (`tst_id`, `tst_by`, `tst_designation`, `tst_content`, `tst_status`, `tst_date`) VALUES
(4, 'TEsting', 'CEO', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a,ultricies in, diam. Sed arcu. Cras consequat.', 1, '2017-07-10 07:18:19'),
(5, 'Josef Testinng', 'Chief Executive', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a,ultricies in, diam. Sed arcu. Cras consequat.', 1, '2017-07-10 07:49:11'),
(6, 'John Oliver', 'CEO', 'Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.Aenean dignissim pellentesque felis. Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a,ultricies in, diam. Sed arcu. Cras consequat.', 1, '2017-07-10 07:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_business_category`
--

CREATE TABLE IF NOT EXISTS `otd_user_business_category` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `category_name` varchar(255) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=168 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_user_business_category`
--

INSERT INTO `otd_user_business_category` (`id`, `user_id`, `category_id`, `category_name`, `type`) VALUES
(1, 2, 870, 'Maintenance industrielle', 1),
(2, 3, 1373, 'Services, aides à domicile', 1),
(3, 4, 1611, 'Vêtements pour femmes (détail)', 1),
(4, 5, 1611, 'Vêtements pour femmes (détail)', 1),
(5, 6, 1611, 'Vêtements pour femmes (détail)', 1),
(6, 7, 1611, 'Vêtements pour femmes (détail)', 1),
(7, 8, 1611, 'Vêtements pour femmes (détail)', 1),
(8, 9, 811, 'Listes de mariage, arts de la table (détail)', 1),
(9, 10, 49, 'Agences immobilières', 1),
(10, 11, 2, 'Autres', 1),
(11, 12, 1493, 'Transport maritime (commissionnaires, transitaires, courtiers et auxiliaires)', 1),
(12, 13, 157, 'Automobiles, véhicules industriels : pièces et accessoires (commerce)', 1),
(13, 13, 2, 'Autres', 1),
(14, 14, 157, 'Automobiles, véhicules industriels : pièces et accessoires (commerce)', 1),
(15, 15, 2, 'Autres', 1),
(16, 16, 2, 'Autres', 1),
(17, 17, 2, 'Autres', 1),
(18, 18, 20, 'Administrateurs de biens et syndics de copropriétés', 1),
(19, 18, 2, 'Autres', 1),
(20, 19, 720, 'Huissiers de justice', 1),
(21, 20, 68, 'Alimentation générale (détail)', 1),
(22, 21, 496, 'Dentistes : chirurgiens-dentistes et docteurs en chirurgie dentaire', 1),
(23, 22, 2, 'Autres', 1),
(24, 23, 2, 'Autres', 1),
(25, 24, 1053, 'Médecins : radiologie (radiodiagnostic et imagerie médicale)', 1),
(26, 25, 2, 'Autres', 1),
(27, 26, 1027, 'Médecins : médecine générale', 1),
(28, 27, 2, 'Autres', 1),
(29, 28, 243, 'Boulangeries-pâtisseries', 1),
(30, 29, 1310, 'Restaurants : restauration rapide et libre-service', 1),
(31, 30, 2, 'Autres', 1),
(32, 31, 706, 'Géomètres-experts', 1),
(33, 32, 2, 'Autres', 1),
(34, 33, 2, 'Autres', 1),
(35, 34, 2, 'Autres', 1),
(36, 35, 1001, 'Médecins : cardiologie, maladies vasculaires', 1),
(37, 36, 1027, 'Médecins : médecine générale', 1),
(38, 37, 2, 'Autres', 1),
(39, 38, 68, 'Alimentation générale (détail)', 1),
(40, 39, 1027, 'Médecins : médecine générale', 1),
(41, 40, 2, 'Autres', 1),
(42, 41, 1027, 'Médecins : médecine générale', 1),
(43, 42, 2, 'Autres', 1),
(44, 43, 1027, 'Médecins : médecine générale', 1),
(45, 44, 2, 'Autres', 1),
(46, 45, 2, 'Autres', 1),
(47, 46, 496, 'Dentistes : chirurgiens-dentistes et docteurs en chirurgie dentaire', 1),
(48, 47, 164, 'Avocats', 1),
(49, 48, 1001, 'Médecins : cardiologie, maladies vasculaires', 1),
(50, 49, 1050, 'Médecins : psychiatrie', 1),
(51, 50, 1050, 'Médecins : psychiatrie', 1),
(52, 51, 1027, 'Médecins : médecine générale', 1),
(53, 52, 2, 'Autres', 1),
(54, 53, 449, 'Cordonneries', 1),
(55, 54, 2, 'Autres', 1),
(56, 54, 1651, 'Électricité générale (entreprises)', 1),
(57, 55, 2, 'Autres', 1),
(58, 56, 2, 'Autres', 1),
(59, 57, 2, 'Autres', 1),
(60, 58, 2, 'Autres', 1),
(61, 59, 2, 'Autres', 1),
(62, 60, 1027, 'Médecins : médecine générale', 1),
(63, 61, 663, 'Garages d''automobiles, réparation', 1),
(64, 62, 2, 'Autres', 1),
(65, 63, 2, 'Autres', 1),
(66, 64, 114, 'Artistes peintres', 1),
(67, 65, 2, 'Autres', 1),
(68, 66, 496, 'Dentistes : chirurgiens-dentistes et docteurs en chirurgie dentaire', 1),
(69, 67, 2, 'Autres', 1),
(70, 68, 2, 'Autres', 1),
(71, 69, 1027, 'Médecins : médecine générale', 1),
(72, 70, 2, 'Autres', 1),
(73, 71, 1027, 'Médecins : médecine générale', 1),
(74, 72, 2, 'Autres', 1),
(75, 73, 2, 'Autres', 1),
(76, 74, 2, 'Autres', 1),
(77, 75, 1439, 'Taxis : artisans', 1),
(78, 76, 1049, 'Médecins : pneumologie', 1),
(79, 77, 2, 'Autres', 1),
(80, 78, 2, 'Autres', 1),
(81, 78, 496, 'Dentistes : chirurgiens-dentistes et docteurs en chirurgie dentaire', 1),
(82, 79, 1464, 'Toilettage de chiens et chats', 1),
(83, 80, 783, 'Kinésithérapeutes : masseurs kinésithérapeutes', 1),
(84, 81, 1198, 'Podologues : pédicures-podologues', 1),
(85, 82, 1027, 'Médecins : médecine générale', 1),
(86, 83, 2, 'Autres', 1),
(87, 84, 2, 'Autres', 1),
(88, 85, 2, 'Autres', 1),
(89, 86, 740, 'Infirmiers : cabinets, soins à domicile', 1),
(90, 87, 740, 'Infirmiers : cabinets, soins à domicile', 1),
(91, 88, 740, 'Infirmiers : cabinets, soins à domicile', 1),
(92, 89, 1577, 'Vins et spiritueux (vente au détail)', 1),
(93, 90, 235, 'Boucheries, boucheries-charcuteries (détail)', 1),
(94, 91, 740, 'Infirmiers : cabinets, soins à domicile', 1),
(95, 92, 1312, 'Restauration collective : produits frais, alimentation générale (gros)', 1),
(96, 93, 2, 'Autres', 1),
(97, 94, 1027, 'Médecins : médecine générale', 1),
(98, 95, 1308, 'Restaurants', 1),
(99, 96, 2, 'Autres', 1),
(100, 97, 2, 'Autres', 1),
(101, 98, 2, 'Autres', 1),
(102, 98, 332, 'Charcuteries (détail)', 1),
(103, 99, 2, 'Autres', 1),
(104, 100, 510, 'Distilleries agricoles et industrielles', 1),
(105, 101, 2, 'Autres', 1),
(106, 102, 2, 'Autres', 1),
(107, 103, 2, 'Autres', 1),
(108, 104, 2, 'Autres', 1),
(109, 105, 1198, 'Podologues : pédicures-podologues', 1),
(110, 106, 2, 'Autres', 1),
(111, 107, 2, 'Autres', 1),
(112, 108, 2, 'Autres', 1),
(113, 109, 2, 'Autres', 1),
(114, 110, 2, 'Autres', 1),
(115, 111, 1310, 'Restaurants : restauration rapide et libre-service', 1),
(116, 112, 2, 'Autres', 1),
(117, 113, 1248, 'Psychologues', 1),
(118, 114, 2, 'Autres', 1),
(119, 115, 2, 'Autres', 1),
(120, 116, 2, 'Autres', 1),
(121, 117, 2, 'Autres', 1),
(122, 118, 1027, 'Médecins : médecine générale', 1),
(123, 119, 1691, '', 1),
(161, 120, 8, 'abat-jour (détail)', 1),
(162, 120, 9, 'abat-jour et fournitures (fabrication)', 1),
(163, 120, 10, 'abattoirs de volailles', 1),
(164, 120, 1692, 'Caps', 2),
(165, 120, 1701, 'product filter testing', 3),
(166, 120, 1702, 'product testing 2', 3),
(167, 120, 1703, 'other testing', 4);

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_messaging`
--

CREATE TABLE IF NOT EXISTS `otd_user_messaging` (
`msg_id` bigint(20) unsigned NOT NULL,
  `msg_sender` bigint(20) unsigned NOT NULL,
  `msg_reciever` bigint(20) unsigned NOT NULL,
  `msg_content` text NOT NULL,
  `msg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `msg_flag` tinyint(4) NOT NULL COMMENT '0=>unread,1=>read',
  `us_notify` tinyint(4) NOT NULL COMMENT '0=>no, 1=>yes',
  `window_open` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_option`
--

CREATE TABLE IF NOT EXISTS `otd_user_option` (
`opt_user_option_id` int(11) NOT NULL,
  `opt_user_id` int(11) DEFAULT NULL,
  `opt_option_id` int(11) DEFAULT NULL,
  `opt_option_type` smallint(6) DEFAULT '1',
  `opt_pkg_id` bigint(20) unsigned NOT NULL,
  `opt_option_purchase_date` date DEFAULT NULL,
  `otp_option_duration` varchar(30) DEFAULT NULL,
  `otp_option_qnty` varchar(255) NOT NULL,
  `opt_option_price` float DEFAULT NULL,
  `otp_search_city` varchar(255) DEFAULT NULL,
  `opt_city_lat` varchar(255) NOT NULL,
  `opt_city_long` varchar(255) NOT NULL,
  `opt_option_active_date` date DEFAULT NULL,
  `opt_option_end_date` date DEFAULT NULL,
  `opt_mnth_strt_day` int(11) NOT NULL,
  `opt_mnth_end_day` int(11) NOT NULL,
  `opt_event_id` bigint(20) unsigned NOT NULL,
  `opt_tran_id` bigint(20) unsigned NOT NULL,
  `opt_option_inactive_validity` int(11) DEFAULT NULL,
  `opt_option_status` int(11) NOT NULL,
  `opt_notactivate_3month` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>not Send, 1=>Send',
  `opt_notactivate_6month` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>not Send, 1=>Send',
  `opt_7daysremaining` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>not Send, 1=>Send'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_user_option`
--

INSERT INTO `otd_user_option` (`opt_user_option_id`, `opt_user_id`, `opt_option_id`, `opt_option_type`, `opt_pkg_id`, `opt_option_purchase_date`, `otp_option_duration`, `otp_option_qnty`, `opt_option_price`, `otp_search_city`, `opt_city_lat`, `opt_city_long`, `opt_option_active_date`, `opt_option_end_date`, `opt_mnth_strt_day`, `opt_mnth_end_day`, `opt_event_id`, `opt_tran_id`, `opt_option_inactive_validity`, `opt_option_status`, `opt_notactivate_3month`, `opt_notactivate_6month`, `opt_7daysremaining`) VALUES
(7, 120, 1, 1, 0, '2018-03-07', '1 Year', '1', 1500, '', '', '', NULL, NULL, 0, 0, 0, 7, NULL, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_option_payment`
--

CREATE TABLE IF NOT EXISTS `otd_user_option_payment` (
`pyt_id` bigint(20) unsigned NOT NULL,
  `pyt_opt_id` bigint(20) unsigned NOT NULL,
  `pyt_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pyt_txn_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pyt_txn_status` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pyt_amount` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pyt_fee` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pyt_date` datetime NOT NULL,
  `py_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=>Paypal, 2=>Slimpay',
  `py_slmref_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_started` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_datecreated` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_datemodified` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_datestarted` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_payscheme` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_senduserapproval` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slm_checkoutactour` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `py_slmMandate_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `otd_user_option_payment`
--

INSERT INTO `otd_user_option_payment` (`pyt_id`, `pyt_opt_id`, `pyt_email`, `pyt_txn_id`, `pyt_txn_status`, `pyt_amount`, `pyt_fee`, `pyt_date`, `py_type`, `py_slmref_id`, `py_slm_state`, `py_slm_started`, `py_slm_datecreated`, `py_slm_datemodified`, `py_slm_datestarted`, `py_slm_payscheme`, `py_slm_senduserapproval`, `py_slm_checkoutactour`, `py_slmMandate_ref`) VALUES
(7, 0, '', '', 'Pending', '', '', '0000-00-00 00:00:00', 1, '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_phone`
--

CREATE TABLE IF NOT EXISTS `otd_user_phone` (
`phn_id` bigint(20) unsigned NOT NULL,
  `phn_user` bigint(20) unsigned NOT NULL,
  `phn_number` varchar(255) NOT NULL,
  `phn_label` varchar(255) NOT NULL,
  `phn_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone_type` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=137 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_user_phone`
--

INSERT INTO `otd_user_phone` (`phn_id`, `phn_user`, `phn_number`, `phn_label`, `phn_date`, `phone_type`) VALUES
(1, 2, '0233042096', 'Standard', '2017-11-10 06:28:41', 1),
(2, 3, '0322448072', 'Standard', '2017-11-10 06:28:41', 1),
(3, 4, '0322915658', 'Standard', '2017-11-10 06:28:41', 1),
(4, 5, '0344809459', 'Standard', '2017-11-10 06:28:41', 1),
(5, 6, '0322910293', 'Standard', '2017-11-10 06:28:41', 1),
(6, 7, '0235430983', 'Standard', '2017-11-10 06:28:41', 1),
(7, 8, '0235716033', 'Standard', '2017-11-10 06:28:41', 1),
(8, 9, '0235887769', 'Standard', '2017-11-10 06:28:42', 1),
(9, 10, '0232811999', 'Standard', '2017-11-10 06:28:42', 1),
(10, 11, '0227306277', 'Standard', '2017-11-10 06:28:42', 1),
(11, 12, '0232720220', 'Standard', '2017-11-10 06:28:42', 1),
(12, 13, '0233288324', 'Standard', '2017-11-10 06:28:42', 1),
(13, 13, '0233802015', 'atelier S.A.V.', '2017-11-10 06:28:42', 1),
(14, 13, '0233802000', 'département automobile', '2017-11-10 06:28:42', 1),
(15, 13, '0233802010', 'département carrosserie', '2017-11-10 06:28:42', 1),
(16, 13, '0233802020', 'standard', '2017-11-10 06:28:42', 1),
(17, 14, '0233141434', 'Standard', '2017-11-10 06:28:42', 1),
(18, 15, '0233880390', 'Standard', '2017-11-10 06:28:42', 1),
(19, 16, '0233499185', 'Standard', '2017-11-10 06:28:42', 1),
(20, 17, '0235742474', 'Standard', '2017-11-10 06:28:42', 1),
(21, 17, '0660574636', 'Standard', '2017-11-10 06:28:42', 1),
(22, 17, '0663074636', 'Standard', '2017-11-10 06:28:42', 1),
(23, 17, '0699843040', 'Standard', '2017-11-10 06:28:42', 1),
(24, 17, '0699845060', 'Standard', '2017-11-10 06:28:43', 1),
(25, 17, '0963631566', 'Standard', '2017-11-10 06:28:43', 1),
(26, 18, '0233365492', 'Standard', '2017-11-10 06:28:43', 1),
(27, 18, '0233670674', 'Standard', '2017-11-10 06:28:43', 1),
(28, 19, '0232905121', 'service administration de biens', '2017-11-10 06:28:43', 1),
(29, 19, '0232905120', 'standard', '2017-11-10 06:28:43', 1),
(30, 20, '0344719059', 'Standard', '2017-11-13 09:40:27', 1),
(31, 21, '0232426828', 'Standard', '2017-11-13 09:40:27', 1),
(32, 22, '0967142779', 'Standard', '2017-11-13 09:40:28', 1),
(33, 23, '0235759641', 'Standard', '2017-11-13 09:40:28', 1),
(34, 24, '0233571790', 'Standard', '2017-11-13 09:40:28', 1),
(35, 25, '0966925964', 'Standard', '2017-11-13 09:40:28', 1),
(36, 26, '0323257125', 'Standard', '2017-11-13 09:40:28', 1),
(37, 27, '0323823640', 'Standard', '2017-11-13 09:40:28', 1),
(38, 28, '0235412779', 'Standard', '2017-11-13 09:40:28', 1),
(39, 29, '0233826626', 'Standard', '2017-11-13 09:40:28', 1),
(40, 30, '0967245252', 'Standard', '2017-11-13 09:40:29', 1),
(41, 30, '0979036269', 'Standard', '2017-11-13 09:40:29', 1),
(42, 31, '0235231823', 'Standard', '2017-11-13 09:40:29', 1),
(43, 32, '0232600600', 'Standard', '2017-11-13 09:40:29', 1),
(44, 33, '0344844715', 'Standard', '2017-11-13 09:40:29', 1),
(45, 34, '0322852850', 'Standard', '2017-11-13 09:40:29', 1),
(46, 35, '0233450512', 'Standard', '2017-11-13 09:40:29', 1),
(47, 36, '0323935158', 'Standard', '2017-11-13 09:40:29', 1),
(48, 37, '0344570683', 'Standard', '2017-11-13 09:40:29', 1),
(49, 38, '0233579502', 'Standard', '2017-11-13 09:40:29', 1),
(50, 39, '0235756310', 'Standard', '2017-11-13 09:40:29', 1),
(51, 40, '0235734695', 'Standard', '2017-11-13 09:40:29', 1),
(52, 41, '0344262776', 'Standard', '2017-11-13 09:40:30', 1),
(53, 42, '0235762222', 'Standard', '2017-11-13 09:40:30', 1),
(54, 43, '0323578464', 'Standard', '2017-11-13 09:40:30', 1),
(55, 44, '0980619329', 'Standard', '2017-11-13 09:40:30', 1),
(56, 45, '0323810268', 'Standard', '2017-11-13 09:40:30', 1),
(57, 46, '0235912170', 'Standard', '2017-11-13 09:40:30', 1),
(58, 47, '0323676304', 'Standard', '2017-11-13 09:40:30', 1),
(59, 48, '0233261613', 'Standard', '2017-11-13 09:40:31', 1),
(60, 49, '0685151822', 'Standard', '2017-11-13 09:40:31', 1),
(61, 50, '0322806763', 'Standard', '2017-11-13 09:40:31', 1),
(62, 51, '0235653202', 'Standard', '2017-11-13 09:40:31', 1),
(63, 52, '0232875621', 'Standard', '2017-11-13 09:40:31', 1),
(64, 53, '0967256262', 'Standard', '2017-11-13 09:40:31', 1),
(65, 54, '0233374886', 'Standard', '2017-11-13 09:40:31', 1),
(66, 54, '0964301214', 'Standard', '2017-11-13 09:40:31', 1),
(67, 55, '0344764768', 'Standard', '2017-11-13 09:40:31', 1),
(68, 55, '0344930270', 'Standard', '2017-11-13 09:40:31', 1),
(69, 56, '0233419712', 'Standard', '2017-11-13 09:40:32', 1),
(70, 57, '0344848239', 'Standard', '2017-11-13 09:40:32', 1),
(71, 57, '0960538796', 'Standard', '2017-11-13 09:40:32', 1),
(72, 58, '0344868225', 'Standard', '2017-11-13 09:40:32', 1),
(73, 59, '0233382804', 'Standard', '2017-11-13 09:40:32', 1),
(74, 60, '0323640233', 'Standard', '2017-11-13 09:40:32', 1),
(75, 61, '0613962037', 'Standard', '2017-11-13 09:40:32', 1),
(76, 62, '0233381943', 'Standard', '2017-11-13 09:40:32', 1),
(77, 63, '0235860168', 'Standard', '2017-11-13 09:40:32', 1),
(78, 64, '0235486085', 'Standard', '2017-11-13 09:40:32', 1),
(79, 65, '0964478013', 'Standard', '2017-11-13 09:40:32', 1),
(80, 66, '0323745929', 'Standard', '2017-11-13 09:40:32', 1),
(81, 67, '0322753406', 'Standard', '2017-11-13 09:40:32', 1),
(82, 68, '0235867037', 'Standard', '2017-11-13 09:40:33', 1),
(83, 69, '0344572681', 'cabinet', '2017-11-13 09:40:33', 1),
(84, 70, '0344574505', 'dom', '2017-11-13 09:40:33', 1),
(85, 71, '0323535385', 'Standard', '2017-11-13 09:40:33', 1),
(86, 72, '0323812893', 'Standard', '2017-11-13 09:40:33', 1),
(87, 73, '0323206936', 'Standard', '2017-11-13 09:40:33', 1),
(88, 74, '0232387879', 'Standard', '2017-11-13 09:40:33', 1),
(89, 75, '0232540779', 'Standard', '2017-11-13 09:40:33', 1),
(90, 76, '0323833969', 'Standard', '2017-11-13 09:40:33', 1),
(91, 77, '0233295432', 'Standard', '2017-11-13 09:40:33', 1),
(92, 78, '0235843581', 'Standard', '2017-11-13 09:40:34', 1),
(93, 78, '0967613581', 'Standard', '2017-11-13 09:40:34', 1),
(94, 79, '0344043648', 'Standard', '2017-11-13 09:40:34', 1),
(95, 80, '0323830390', 'Standard', '2017-11-13 09:40:34', 1),
(96, 81, '0232366909', 'Standard', '2017-11-13 09:40:34', 1),
(97, 82, '0344490823', 'Standard', '2017-11-13 09:40:34', 1),
(98, 83, '0322329146', 'Standard', '2017-11-13 09:40:34', 1),
(99, 84, '0979726803', 'Standard', '2017-11-13 09:40:34', 1),
(100, 85, '0233385013', 'Standard', '2017-11-13 09:40:34', 1),
(101, 86, '0232335027', 'Standard', '2017-11-13 09:40:34', 1),
(102, 87, '0344401577', 'Standard', '2017-11-13 09:40:34', 1),
(103, 88, '0233322049', 'Standard', '2017-11-13 09:40:35', 1),
(104, 89, '0967188420', 'Standard', '2017-11-13 09:40:35', 1),
(105, 90, '0235236150', 'Standard', '2017-11-13 09:40:35', 1),
(106, 91, '0235372063', 'Standard', '2017-11-13 09:40:35', 1),
(107, 92, '0967720274', 'Standard', '2017-11-13 09:40:35', 1),
(108, 93, '0235732435', 'Standard', '2017-11-13 09:40:35', 1),
(109, 94, '0323709936', 'Standard', '2017-11-13 09:40:35', 1),
(110, 95, '0235946725', 'Standard', '2017-11-13 09:40:35', 1),
(111, 96, '0322746031', 'Standard', '2017-11-13 09:40:35', 1),
(112, 97, '0233457421', 'Standard', '2017-11-13 09:40:35', 1),
(113, 98, '0344414448', 'Standard', '2017-11-13 09:40:35', 1),
(114, 98, '0967396735', 'Standard', '2017-11-13 09:40:35', 1),
(115, 99, '0235137328', 'Standard', '2017-11-13 09:40:35', 1),
(116, 99, '0967897328', 'Standard', '2017-11-13 09:40:35', 1),
(117, 100, '0232578201', 'Standard', '2017-11-13 09:40:36', 1),
(118, 101, '0232434129', 'Standard', '2017-11-13 09:40:36', 1),
(119, 102, '0322260475', 'Standard', '2017-11-13 09:40:36', 1),
(120, 103, '0322748493', 'Standard', '2017-11-13 09:40:36', 1),
(121, 104, '0233276644', 'Standard', '2017-11-13 09:40:36', 1),
(122, 105, '0323072382', 'Standard', '2017-11-13 09:40:36', 1),
(123, 106, '0630463645', 'Standard', '2017-11-13 09:40:36', 1),
(124, 107, '0322765231', 'Standard', '2017-11-13 09:40:36', 1),
(125, 107, '0967394000', 'Standard', '2017-11-13 09:40:36', 1),
(126, 108, '0344023105', 'Standard', '2017-11-13 09:40:36', 1),
(127, 109, '0235631085', 'Standard', '2017-11-13 09:40:36', 1),
(128, 110, '0235783582', 'Standard', '2017-11-13 09:40:37', 1),
(129, 111, '0322415125', 'Standard', '2017-11-13 09:40:37', 1),
(130, 112, '0961296368', 'Standard', '2017-11-13 09:40:37', 1),
(131, 113, '0344627093', 'Standard', '2017-11-13 09:40:37', 1),
(132, 114, '0232555237', 'Standard', '2017-11-13 09:40:37', 1),
(133, 115, '0966815573', 'Standard', '2017-11-13 09:40:37', 1),
(134, 116, '0233593573', 'Standard', '2017-11-13 09:40:37', 1),
(135, 117, '0232521027', 'Standard', '2017-11-13 09:40:38', 1),
(136, 118, '0235541363', 'Standard', '2017-11-13 09:40:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_photogallery`
--

CREATE TABLE IF NOT EXISTS `otd_user_photogallery` (
`ph_id` bigint(20) unsigned NOT NULL,
  `ph_user` bigint(20) unsigned NOT NULL,
  `ph_path` varchar(255) NOT NULL,
  `ph_date_upload` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_user_photogallery`
--

INSERT INTO `otd_user_photogallery` (`ph_id`, `ph_user`, `ph_path`, `ph_date_upload`) VALUES
(1, 120, 'mJoaZ3jpFr8ls4HZZGlLkvUG5fKbl8.jpg', '2017-11-16 06:10:34'),
(2, 120, 'XA0naHVXI4Iozi82dsmKBa9QcuGbZ3.jpg', '2017-11-16 06:10:40'),
(5, 120, 'ePrw9Z48ilWd2l80cYed8Hl4Gt3hbM.jpg', '2017-11-16 06:32:28'),
(6, 120, 'XUsAWXHgPI3ds9999L3hMtnkGNcwIF.png', '2017-11-27 12:24:52'),
(7, 120, '7zbzfHYt9CoQVyjEbe4m1QuW08WRgj.png', '2017-11-27 12:24:55'),
(8, 120, 'g6m5M7oHmSYRkQbMy1BblPmhzh1NzO.png', '2017-11-27 12:24:57'),
(9, 120, 'aWxu9dutZfOPCCgSxr1uIiPZBhUea0.png', '2017-11-27 12:25:00'),
(10, 120, 'YzTxePJtpmbVI0btT6cd9veKcyJ9f6.png', '2017-11-27 12:25:02'),
(11, 120, 'E22tuuNxc7cBfZvyBHpHKm9TDzqvJp.png', '2017-11-27 12:25:05'),
(12, 120, 'tl1sZLYPeCx7Secp9tUjNF9T1FULxO.png', '2017-11-27 12:25:08');

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_purchasedoc`
--

CREATE TABLE IF NOT EXISTS `otd_user_purchasedoc` (
`dc_id` bigint(20) unsigned NOT NULL,
  `dc_orderid` bigint(20) unsigned NOT NULL,
  `dc_userid` bigint(20) unsigned NOT NULL,
  `dc_docid` int(11) NOT NULL,
  `dc_docpath` varchar(255) NOT NULL,
  `dc_date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dc_status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `otd_user_purchasedoc`
--

INSERT INTO `otd_user_purchasedoc` (`dc_id`, `dc_orderid`, `dc_userid`, `dc_docid`, `dc_docpath`, `dc_date_created`, `dc_status`) VALUES
(1, 0, 119, 2, 'RAhCTcRntmPlGh3w6GQkRM4Mwi4jbs.pdf', '2017-12-11 10:47:04', 1),
(2, 0, 119, 3, 'GUsYf1q4DEFt9einFLTvvGz5wZUQ48.pdf', '2017-12-11 10:57:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `otd_user_slimpayreference`
--

CREATE TABLE IF NOT EXISTS `otd_user_slimpayreference` (
`slmref_id` bigint(20) unsigned NOT NULL,
  `slmref_userid` bigint(20) unsigned NOT NULL,
  `slmref_mandateref` varchar(255) NOT NULL,
  `slmref_subscriber` varchar(255) NOT NULL,
  `slmref_document` varchar(255) NOT NULL,
  `slmref_status` varchar(255) NOT NULL,
  `slmref_datecreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profile_review`
--

CREATE TABLE IF NOT EXISTS `profile_review` (
`vote_id` int(10) unsigned NOT NULL,
  `blog_vote` float unsigned NOT NULL,
  `voted_user_id` int(10) unsigned NOT NULL,
  `voter_user_id` int(10) unsigned NOT NULL,
  `review_text` varchar(300) COLLATE latin1_general_ci DEFAULT NULL,
  `review_date` datetime NOT NULL,
  `ip_address` varchar(20) COLLATE latin1_general_ci DEFAULT NULL,
  `vote_status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0=>Unpublish, 1=>publish, 3=>Delete',
  `status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `profile_review`
--

INSERT INTO `profile_review` (`vote_id`, `blog_vote`, `voted_user_id`, `voter_user_id`, `review_text`, `review_date`, `ip_address`, `vote_status`, `status`) VALUES
(1, 5, 110, 120, 'asdfas fsadfdasf asdf', '2017-11-17 07:23:26', NULL, 0, 0),
(2, 4, 110, 120, 'sdfsadf', '2017-11-17 07:29:10', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `reported_profile_review`
--

CREATE TABLE IF NOT EXISTS `reported_profile_review` (
`reported_id` int(11) NOT NULL,
  `vote_id` int(10) unsigned NOT NULL DEFAULT '0',
  `blog_vote` float unsigned NOT NULL,
  `voted_user_id` int(10) unsigned NOT NULL,
  `voter_user_id` int(10) unsigned NOT NULL,
  `review_text` varchar(300) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `review_date` datetime NOT NULL,
  `ip_address` varchar(20) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `status` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Reported',
  `vote_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `site_contents`
--

CREATE TABLE IF NOT EXISTS `site_contents` (
`content_id` int(11) NOT NULL,
  `section` varchar(255) NOT NULL,
  `sub_section` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `uploaded_file_path` varchar(255) DEFAULT NULL,
  `background_img_path` varchar(255) NOT NULL,
  `video_type` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=> youtube, 2=> uploaded',
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_contents`
--

INSERT INTO `site_contents` (`content_id`, `section`, `sub_section`, `content`, `uploaded_file_path`, `background_img_path`, `video_type`, `status`) VALUES
(1, 'Business User Registration', 'Read More', 'Professional users registration.Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum et Malorum" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, "Lorem ipsum dolor sit amet..", comes from a line in section 1.10.32', '', '', 1, 1),
(2, 'Business User Registration', 'Terms and Condition', 'test', 'termsandconditionspro.pdf', '', 1, 1),
(3, 'Business User Registration', 'Newsletters', 'This is image', 'Chrysanthemum.jpg', '', 1, 1),
(4, 'Personal User Registration', 'Read More', '<p style="text-align: justify;">Personal users registration, Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of &quot;de Finibus Bonorum et Malorum&quot; (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, &quot;Lorem ipsum dolor sit amet..&quot;, comes from a line in section 1.10.32</p>\r\n', '', '', 1, 1),
(5, 'Personal User Registration', 'Terms and Condition', 'this is terms and condition', 'termsandconditionsnor.pdf', '', 1, 1),
(6, 'Personal User Registration', 'Newsletters', 'image persomn', 'Koala.jpg', '', 1, 1),
(7, 'Personal Account Registration', 'NewsLetter1', 'Souhaitez-vous recevoir des offres promotionnelles de la part d?Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)\\r\\n', NULL, '', 1, 1),
(8, 'Personal Account Registration', 'NewsLetter2', 'Souhaitez-vous recevoir sur votre mobile des offres promotionnelles de la part d?Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)\\r\\n', NULL, '', 1, 1),
(9, 'Professional Account Registration', 'NewsLetter1', 'Souhaitez-vous recevoir des offres promotionnelles de la part d?Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)\\r\\n', NULL, '', 1, 1),
(10, 'Professional Account Registration', 'NewsLetter2', 'Souhaitez-vous recevoir sur votre mobile des offres promotionnelles de la part d?Otourdemoi et de nos partenaires ? (bons plans, promotions, remises exceptionnelles...)', NULL, '', 1, 1),
(11, 'Footer', 'Left Article', '<p style="text-align: justify;">FooterArticle content, Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia.</p>\r\n', NULL, '', 1, 1),
(12, 'Vous êtes un PRO ?', '', '<p>Inscrivez-vous, c&rsquo;est gratuit et sans engagement D&eacute;couvrez comment vous faire conna&icirc;tre, attirer plus de client et d&eacute;velopper votre activit&eacute; gr&acirc;ce &agrave; Otourdemoi. La premi&egrave;re communaut&eacute; entre particuliers et professionnels</p>\\r\\n', NULL, 'XsR67KZ0Oc.jpg', 1, 1),
(13, 'Otourdemoi soutient l’opération', '', '<p>Aidons les enfants hospitalis&eacute;s &agrave; lutter contre la douleur et am&eacute;liorons leurs quotidiens. Chaque geste compte.</p>\r\n', NULL, 'qrUuQ65ImQ.jpg', 1, 1),
(14, 'A movement is moving!', '', '<p class="sub-head"><strong>Otourdemoi supports artisans, small businesses and traders</strong></p>\r\n\r\n<p>Otourdemoi offers many free services for companies to help them get to know and develop their activities.Choosing crafts means choosing quality and trust!</p>\r\n\r\n<p><strong>Together, let us act to preserve quality products and services, at the right price and a stone&#39;s throw from our home.</strong></p>\r\n\r\n<p>Say yes to responsible consumption and support for the local economy in order to keep jobs in our regions and our French know-how.</p>\r\n', 'tA0gXSc29F.mp4', 'https://www.youtube.com/embed/yAoLSRbwxL8', 1, 1),
(15, 'Contact information', '', 'Address and opening hours Exchange with professionals of your choice without having to communicate your personal details', 'eseW6wiO6g.png', '', 1, 1),
(16, 'Save Time', '', 'Address and opening hours Exchange with professionals of your choice without having to communicate your personal details', '2ca0XZWaxk.png', '', 1, 1),
(17, 'Support your merchants', '', 'Address and opening hours Exchange with professionals of your choice without having to communicate your personal details', 'xirz9l6LJS.png', '', 1, 1),
(18, 'Contact information', '', 'Address and opening hours Exchange with professionals of your choice without having to communicate your personal details', 'WmsULsIyh9.png', '', 1, 1),
(19, 'Save Time', '', 'Address and opening hours Exchange with professionals of your choice without having to communicate your personal details', '3WypQuUnZl.png', '', 1, 1),
(20, 'Support your merchants', '', 'Address and opening hours Exchange with professionals of your choice without having to communicate your personal details', 'ic9AF0PBIL.png', '', 1, 1),
(21, 'Ne cherchez plus, les bons plans viennent à vous !', '', 'Promotions, offres sp?ciales, soldes, ventes priv?es, ventes flash, liquidation... Vous ?tes inform?s en temps r?el, soyez les premiers ? en profiter.', 'v6XDEVAyHv.png', '', 1, 1),
(22, 'Profitez de la Communauté Otourdemoi.', '', 'D?couvrez les avis des autres clients pour faire le bon choix.\\r\\nPartagez vos exp?riences.', 'M3vM0NPJLh.png', '', 1, 1),
(23, 'Tchatez en direct avec des Pros', '', 'Ils r?pondent ? toutes vos questions en quelques minutes.', 'mz2bYDZB8Q.png', '', 1, 1),
(24, '<span>Explore</span> what''s interesting', '', '<p>Ne his test postulant posidonium adversarium. Ius tollit tamquam indoctum ea,</p>\r\n\r\n<p>cu quo equidem perfecto adipiscing. Eu mel aliquid delenit. Recteque laboramus ea est, te qui eirmod similique.</p>\r\n', NULL, '', 1, 1),
(25, '', '', NULL, 'WXbhsO4hEN.jpg', '', 1, 1),
(26, 'Google Advertisement', '', '', NULL, '', 1, 1),
(27, 'Purchase Receipt', '', '<p>Aidons les enfants hospitalis&eacute;s &agrave; lutter contre la douleur et am&eacute;liorons leurs quotidiens. Chaque geste compte.</p>\n\n<p>%%order_id%%,</p>\n\n<p>%%purchase_date%%,</p>\n\n<p>%%amount%%,</p>\n\n<p>%%description%%,</p>\n\n<p>%%option_name%%,</p>\n\n<p>%%option_duration%%</p>\n', NULL, '', 1, 1),
(28, 'Slimpay Terms & Conditions', 'Terms and Condition', 'this is terms and condition', 'termsandconditionsnor.pdf', '', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `subscribers`
--

CREATE TABLE IF NOT EXISTS `subscribers` (
`subs_id` int(11) NOT NULL,
  `promo_mail` int(11) NOT NULL,
  `promo_sms` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_postcode` varchar(10) DEFAULT NULL,
  `subscription_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(4) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`user_id` int(11) NOT NULL,
  `user_type` varchar(30) DEFAULT 'Personal',
  `user_firstname` varchar(100) NOT NULL,
  `user_lastname` varchar(100) DEFAULT NULL,
  `user_email` varchar(150) NOT NULL,
  `user_phone` varchar(12) NOT NULL,
  `user_gender` varchar(6) NOT NULL,
  `user_dob` date DEFAULT NULL,
  `user_address` varchar(250) DEFAULT NULL,
  `user_language` varchar(50) DEFAULT NULL,
  `user_categories` varchar(300) DEFAULT NULL,
  `user_company_name` varchar(150) DEFAULT NULL,
  `user_company_number` varchar(50) DEFAULT NULL,
  `user_company_address` varchar(200) DEFAULT NULL,
  `user_lat` varchar(255) NOT NULL,
  `user_long` varchar(255) NOT NULL,
  `user_password` varchar(50) NOT NULL,
  `user_rec_promo` int(11) DEFAULT NULL,
  `user_rec_outdoor_promo` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `social_id` varchar(255) NOT NULL,
  `user_signup_method` varchar(255) NOT NULL,
  `auth_check` int(11) NOT NULL,
  `sms_check` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL,
  `act_link` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_online` int(11) DEFAULT '0',
  `publishonhome` smallint(6) NOT NULL DEFAULT '0',
  `toprated` smallint(6) NOT NULL DEFAULT '0',
  `user_profile_pic` varchar(255) DEFAULT 'default/default.jpg',
  `businessID` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_type`, `user_firstname`, `user_lastname`, `user_email`, `user_phone`, `user_gender`, `user_dob`, `user_address`, `user_language`, `user_categories`, `user_company_name`, `user_company_number`, `user_company_address`, `user_lat`, `user_long`, `user_password`, `user_rec_promo`, `user_rec_outdoor_promo`, `status`, `social_id`, `user_signup_method`, `auth_check`, `sms_check`, `created`, `modified`, `act_link`, `last_login`, `is_online`, `publishonhome`, `toprated`, `user_profile_pic`, `businessID`) VALUES
(1, 'Admin', 'Otourdemoi', 'Admin', 'admin@gmail.com', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:26:37', NULL, NULL, '2017-11-17 10:56:04', 0, 0, 0, 'default/default.jpg', ''),
(2, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '01db métravib', '4.10E+13', '   zone industrielle 50440 Digulleville', '49.6749263', '-1.8602789', '', NULL, NULL, 0, '', '', 0, 0, NULL, NULL, NULL, '0000-00-00 00:00:00', 0, 0, 1, 'default/default.jpg', '01dbmétravib_40986970800068_zoneindustrielle50440digulleville'),
(3, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '02 amiens', '4.91E+13', '11  avenue paix 80000 Amiens', '49.9118342', '2.2982342', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-15 11:29:02', 0, 0, 0, 'default/default.jpg', '02amiens_49145401300013_11avenuepaix80000amiens'),
(4, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '1.2.3.', '4.45E+13', '8  rue 3 cailloux 80000 Amiens', '49.8927197', '2.2980794', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-10 11:58:41', 0, 0, 0, 'default/default.jpg', '1.2.3._44460046400010_8rue3cailloux80000amiens'),
(5, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '1.2.3', '3.51E+13', '6  rue gambetta 60000 Beauvais', '49.4326862', '2.0847346', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-15 11:29:05', 0, 0, 0, 'default/default.jpg', '1.2.3_35097655100019_6ruegambetta60000beauvais'),
(6, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '1.2.3', '4.45E+13', '8  rue 3 cailloux 80000 Amiens', '49.8927197', '2.2980794', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-15 11:29:08', 0, 0, 0, 'default/default.jpg', '1.2.3_44460046400010_8rue3cailloux80000amiens'),
(7, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '1.2.3', '4.45E+13', '  espace rené coty 76600 Le Havre', '49.4959192', '0.1116195', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-10 11:58:41', 0, 0, 0, 'default/default.jpg', '1.2.3_44460046400010_espacerenécoty76600lehavre'),
(8, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '1.2.3', '4.45E+13', '50 b rue gros horloge 76000 Rouen', '49.4413026', '1.0922986', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-10 11:58:41', 0, 0, 0, 'default/default.jpg', '1.2.3_44460046401554_50bruegroshorloge76000rouen'),
(9, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '1001 listes', '4.23E+13', '77  rue ecuyère 76000 Rouen', '49.443657', '1.0905009', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:41', NULL, NULL, '2017-11-10 11:58:41', 0, 0, 0, 'default/default.jpg', '1001listes_42289196000169_77rueecuyère76000rouen'),
(10, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '100 pour 100 immo', '5.30E+13', '18  avenue jean jaurès 76140 Le Petit Quevilly', '49.428368', '1.068851', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', '100pour100immo_52970675600010_18avenuejeanjaurès76140lepetitquevilly'),
(11, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '100 pour 100 pneu', '4.49E+13', '2  route havre 76400 Fécamp', '49.7498618', '0.3707413', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', '100pour100pneu_44880503600036_2routehavre76400fécamp'),
(12, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '3 h taron', '3.45E+13', '  parc activités pont normandie 76700 Rogerville', '49.4744827', '0.2544253', '', NULL, NULL, 0, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', '3htaron_34492091300094_parcactivitéspontnormandie76700rogerville'),
(13, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Ad normandie-maine,f.i.a.o', '4.52E+13', '10  rue expansion 61000 Cerisé', '48.4454809', '0.1156353', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', 'adnormandie-maine,f.i.a.o_45204427400010_10rueexpansion61000cerisé'),
(14, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Ad normandie-maine', '4.52E+13', '23  route falaise 61600 La Ferté Macé', '48.6094434', '-0.354966', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', 'adnormandie-maine_45204427400010_23routefalaise61600lafertémacé'),
(15, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Ad normandie maine', '4.52E+13', '165  rue artisans 50110 Tourlaville', '49.6364426', '-1.5899561', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', 'adnormandiemaine_45204427400010_165rueartisans50110tourlaville'),
(16, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Ad normandie maine', '4.52E+13', '6   le grand chien 50300 Saint Martin des Champs', '48.6569122', '-1.3524868', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', 'adnormandiemaine_45204427400010_6legrandchien50300saintmartindeschamps'),
(17, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adom''pare-brise', '4.93E+13', '155  rue martyrs de la résistance 76150 Maromme', '49.4820825', '1.0456284', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:42', NULL, NULL, '2017-11-10 11:58:42', 0, 0, 0, 'default/default.jpg', 'adom''pare-brise_49291476700016_155ruemartyrsdelarésistance76150maromme'),
(18, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agence immobilière lecornu', '3.15E+13', '86 b rue aristide briand 61200 Argentan', '48.7478726', '-0.0148697', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:43', NULL, NULL, '2017-11-10 11:58:43', 0, 0, 0, 'default/default.jpg', 'agenceimmobilièrelecornu_31459824400015_86bruearistidebriand61200argentan'),
(19, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Alexandre m.c arrivé c.', '3.89E+13', '1  rue toustain 76200 Dieppe', '49.9233505', '1.0723658', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-10 06:28:43', NULL, NULL, '2017-11-10 11:58:43', 0, 0, 0, 'default/default.jpg', 'alexandrem.carrivéc._38871887600021_1ruetoustain76200dieppe'),
(20, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdallah Bana', '3.39135E+13', '26 Place de la République, 60180 Nogent-sur-Oise, France', '49.2795566', '2.473003', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:27', NULL, NULL, '2017-11-13 15:10:27', 0, 0, 0, 'default/default.jpg', 'abdallahbana_33913457900020_26placerépublique60180nogentsuroise'),
(21, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdallah Ghassan bou', '5.20208E+13', '9 Rue du Général Leclerc, 27500 Pont-Audemer, France', '49.3547792', '0.5129466', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:27', NULL, NULL, '2017-11-13 15:10:27', 0, 0, 0, 'default/default.jpg', 'abdallahghassanbou_52020833100030_9ruegénleclerc27500pontaudemer'),
(22, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdel-aziz Teldjoune', '4.91095E+13', '45 Rue Henri Dunant, 76620 Le Havre, France', '49.5227377', '0.1289893', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:27', NULL, NULL, '2017-11-13 15:10:27', 0, 0, 0, 'default/default.jpg', 'abdel-azizteldjoune_49109485000014_45ruehenridunant76600lehavre'),
(23, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdel-hamid Aboudmiaa', '5.20695E+13', '63 Route de Dieppe, 76960 Notre-Dame-de-Bondeville, France', '49.4871132', '1.0484685', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdel-hamidaboudmiaa_52069458900018_63routedieppe76960notredamedebondeville'),
(24, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelaaziz Talbioui', '3.37769E+13', '321 Rue Alexis de Tocqueville, 50000 Saint-Lô, France', '49.1036606', '-1.0781794', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdelaaziztalbioui_33776930100025_321ruealexisdetocqueville50000saintlô'),
(25, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdeladim Qassid', '8.24781E+13', '240 Avenue du Général Leclerc, 61000 Alençon, France', '48.4121296', '0.0891424', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdeladimqassid_82478134800019_240avenuegenleclerc61000alençon'),
(26, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelak Elachab', '4.10353E+13', '4 Rue Vigneron, 02870 Crépy, France', '49.6028845', '3.5151409', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdelakelachab_41035306400018_4ruevigneron02870crépy'),
(27, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelali Kébli', '3.20945E+13', '60 Rue Saint-Denis, 02130 Villers-sur-Fère, France', '49.1751149', '3.5331046', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdelalikébli_32094548800031_60ruestdenis02130villerssurfère'),
(28, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelaziz Teldjoune', '4.91095E+13', '45 Rue Henri Dunant, 76620 Le Havre, France', '49.5227377', '0.1289893', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdelazizteldjoune_49109485000022_45ruehenridunant76600lehavre'),
(29, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelghafour Abajjane', '8.10004E+13', '14 Rue du Pont de Fresne, 61250 Damigny, France', '48.4429306', '0.0706324', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:28', NULL, NULL, '2017-11-13 15:10:28', 0, 0, 0, 'default/default.jpg', 'abdelghafourabajjane_81000361600010_14ruepontdufresne61250damigny'),
(30, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelhak Sahli', '3.33783E+13', '55 Rue Louis Denoual, 60540 Bornel, France', '49.1998318', '2.2080537', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelhaksahli_33378253000021_55ruelouisdenoual60540bornel'),
(31, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelhouab Belbeida', '7.49971E+13', '2935 Rue de Cailly, 76230 Quincampoix, France', '49.5360582', '1.1936872', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelhouabbelbeida_74997103200034_2935ruecailly76230quincampoix'),
(32, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelkrim Benhamouche', '7.98345E+13', '29 Rue Sainte-Foy, 27190 Conches-en-Ouche, France', '48.9616334', '0.9420182', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelkrimbenhamouche_79834491700017_29ruestefoy27190conchesenouche'),
(33, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelkrim Hadour', '4.87749E+13', '18 Rue Alfred Leblanc, 60000 Allonne, France', '49.4013715', '2.1127367', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelkrimhadour_48774894900012_18ruealfredleblanc60000allonne'),
(34, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelkrim Hamimi', '4.99277E+13', '4 Route Nationale, 80320 Fresnes-Mazancourt, France', '49.8502082', '2.8762577', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelkrimhamimi_49927728300028_4routenationale80320fresnesmazancourt'),
(35, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelkrim Ould-slimane', '3.16645E+13', '13 Place de la Croûte, 50200 Coutances, France', '49.0476369', '-1.4405376', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelkrimould-slimane_31664544900026_13placecroute50200coutances'),
(36, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelkrim Rezkallah', '4.50987E+13', '24 Rue Pierre Curie, 02200 Soissons, France', '49.3768648', '3.3112962', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelkrimrezkallah_45098654200014_24ruepierrecurie02200soissons'),
(37, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdellah Bayne', '3.3952E+13', '3 Rue du Bois Saint-Denis, 60500 Chantilly, France', '49.176917', '2.4584476', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdellahbayne_33951973800068_3rueboisstdenis60500chantilly'),
(38, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdellah Epi service', '8.14471E+13', '3 Rue de l''Europe, 50180 Saint-Gilles, France', '49.105119', '-1.175655', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdellahepiservice_81447070400010_3rueeurope50180saintgilles'),
(39, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdellatif Habbeddine', '4.09611E+13', '28 Rue de Binche, 76150 Maromme, France', '49.4767469', '1.0399051', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdellatifhabbeddine_40961112600023_28ruebinche76150maromme'),
(40, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdellatif Mahfoud', '4.93884E+13', '96 Rue Louis Blanc, 76100 Rouen, France', '49.4264908', '1.079545', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdellatifmahfoud_49388440700021_96ruelouisblanc76100rouen'),
(41, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelmajid Kaabi', '3.91398E+13', '25 Rue Jean Jaurès, 60740 Saint-Maximin, France', '49.222356', '2.4438472', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:29', NULL, NULL, '2017-11-13 15:10:29', 0, 0, 0, 'default/default.jpg', 'abdelmajidkaabi_39139837700039_25bruejeanjaurès60740saintmaximin'),
(42, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdelmalek Hamadouche', '4.44959E+13', '19 Rue de la République, 76500 Elbeuf, France', '49.2900754', '1.0019249', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:30', NULL, NULL, '2017-11-13 15:10:30', 0, 0, 0, 'default/default.jpg', 'abdelmalekhamadouche_44495932400054_19ruerepublique76500elbeuf'),
(43, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdel Zaraa', '7.80229E+13', '21 Boulevard de la Liberté, 02700 Tergnier, France', '49.6535858', '3.2844271', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:30', NULL, NULL, '2017-11-13 15:10:30', 0, 0, 0, 'default/default.jpg', 'abdelzaraa_78022948000014_21boulevardliberté02700tergnier'),
(44, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrahim Eraissi', '7.51005E+13', '35 Rue de la République, 76350 Oissel, France', '49.3397321', '1.0927064', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:30', NULL, NULL, '2017-11-13 15:10:30', 0, 0, 0, 'default/default.jpg', 'abderrahimeraissi_75100543000010_35ruerepublique76350oissel'),
(45, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrahman Bahloul', '3.23989E+13', '10 Place de L Hôtel de ville, 80400 Ham, France', '49.7481164', '3.0720283', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:30', NULL, NULL, '2017-11-13 15:10:30', 0, 0, 0, 'default/default.jpg', 'abderrahmanbahloul_32398892300047_10placehôteldeville80400ham'),
(46, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrazak Hammami', '4.92739E+13', '89 Route de Dieppe, 76960 Notre-Dame-de-Bondeville, France', '49.4877084', '1.0486706', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:30', NULL, NULL, '2017-11-13 15:10:30', 0, 0, 0, 'default/default.jpg', 'abderrazakhammami_49273861200034_89routedieppe76960notredamedebondeville'),
(47, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrazek Khelfat', '3.47778E+13', '23 Rue des Canonniers, 02100 Saint-Quentin, France', '49.8448426', '3.2856605', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:30', NULL, NULL, '2017-11-13 15:10:30', 0, 0, 0, 'default/default.jpg', 'abderrazekkhelfat_34777762500034_23ruecanonniers02100saintquentin'),
(48, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrazzak Lyagoubi', '4.43821E+13', '39 Avenue de Quakenbruck, 61000 Alençon, France', '48.438284', '0.102422', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abderrazzaklyagoubi_44382089900021_39avenuequakenbrück61000alençon'),
(49, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrezak Hammadou', '7.94706E+13', '82 Rue Jean Jaurès, 80000 Amiens, France', '49.8991889', '2.2856867', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abderrezakhammadou_79470641600011_82ruejeanjaures80000amiens'),
(50, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abderrezak Hammadou', '7.94706E+13', '82 Rue Jean Jaurès, 80000 Amiens, France', '49.8991889', '2.2856867', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abderrezakhammadou_79470641600011_82ruejeanjaurès80000amiens'),
(51, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdesselem Boussad', '3.84274E+13', '18 Rue Jean François la Pérouse, 76800 Saint-Étienne-du-Rouvray, France', '49.399043', '1.0451548', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abdesselemboussad_38427436100045_18placefrançoistruffaut76800saintetiennedurouvray'),
(52, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abdoul Fofana', '7.53232E+13', '6 Zone Artisanale Les Cambres, 76710 Anceaumeville, France', '49.572265', '1.050173', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abdoulfofana_75323217200035_zoneartisanalelescambres76710anceaumeville'),
(53, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abed Boukalfa', '4.21948E+13', '36 Rue Pont Mortain, 14100 Lisieux, France', '49.1438417', '0.2265822', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abedboukalfa_42194811800029_36ruepontmortain14100lisieux'),
(54, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Baugé', '3.97706E+13', '9 Rue du Moulin, 61350 Saint-Mars-d''Égrenne, France', '48.5596664', '-0.7295381', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abelbaugé_39770553400022_9ruemoulin61350saintmarsd''egrenne'),
(55, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Carvalho', '4.12725E+13', '17 Route de Longueil-Annel, 60150 Thourotte, France', '49.4750369', '2.8794642', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abelcarvalho_41272500400037_17routelongueilannel60150thourotte'),
(56, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Cotentin', '4.23033E+13', '12 Rue de Ancienne Gare, 50260 Sottevast, France', '49.523248', '-1.593257', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:31', NULL, NULL, '2017-11-13 15:10:31', 0, 0, 0, 'default/default.jpg', 'abelcotentin_42303255600015_12rueanciennegare50260sottevast'),
(57, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Douilly', '3.13926E+13', 'La Loge, 60590 Flavacourt, France', '49.348951', '1.8167655', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abeldouilly_31392630500019_laloge60590flavacourt'),
(58, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Duquenoy', '3.52079E+13', '83 Rue de Paris, 60200 Compiègne, France', '49.4115162', '2.8188127', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abelduquenoy_35207869500033_83rueparis60200compiègne'),
(59, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Masseron', '3.30969E+13', 'Le Fougeray, 61140 Geneslay, France', '48.52124', '-0.493567', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abelmasseron_33096862900023_lefougeray61140geneslay'),
(60, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abel Pruvost', '3.15903E+13', '113 Rue de la Chaussée Romaine, 02100 Saint-Quentin, France', '49.8462992', '3.2711239', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abelpruvost_31590320300026_113bruechausséeromaine02100saintquentin'),
(61, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abilio De sousa carneiro', '5.03824E+13', '5 Rue Saint-Roch, 80000 Amiens, France', '49.8966158', '2.2875145', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abiliodesousacarneiro_50382440100015_5ruestroch80000amiens'),
(62, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abo-la-ferte-mace Cdg', '2.861E+13', '7 Rue Félix Desaunay, 61600 La Ferté-Macé, France', '48.586867', '-0.3609507', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abo-la-ferte-macecdg_28610002900011_7ruefélixdesaunay61600lafertémacé'),
(63, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Aboudar Boulhadith', '4.99467E+13', '6 Rue Abbé Vincheneux, 76470 Le Tréport, France', '50.0594899', '1.3726982', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'aboudarboulhadith_49946716500019_6bruevincheneux76470letréport'),
(64, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abraham Dayan', '4.79084E+13', '71 Rue du Général Faidherbe, 76600 Le Havre, France', '49.4874133', '0.1139026', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abrahamdayan_47908411300048_71ruegénfaidherbe76600lehavre'),
(65, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abraham Viteaux', '5.34327E+13', '32 Route de Fère en Tardenois, 02200 Belleu, France', '49.3674388', '3.3326289', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abrahamviteaux_53432658200010_32routefereentardenois02200belleu'),
(66, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Abraham Viteaux', '5.34327E+13', '32 Route de Fère en Tardenois, 02200 Belleu, France', '49.3674388', '3.3326289', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'abrahamviteaux_53432658200010_32routefèreentardenois02200belleu'),
(67, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Achille Libbrecht', '4.29045E+13', '15 Rue du Bas, 80300 Senlis-le-Sec, France', '50.0248624', '2.5783335', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'achillelibbrecht_42904530500019_15ruebas80300senlislesec'),
(68, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Achille Masson', '3.85113E+13', 'Le Coudroy, 76260 Saint-Martin-le-Gaillard, France', '49.9600121', '1.3537143', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:32', NULL, NULL, '2017-11-13 15:10:32', 0, 0, 0, 'default/default.jpg', 'achillemasson_38511323800013_lecoudroy76260saintmartinlegaillard'),
(69, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Achour Radji', '3.09547E+13', '6 Rue Baronne James de Rothschild, 60270 Gouvieux, France', '49.1882698', '2.4166558', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'achourradji_30954707300028_6ruebaronnejderothschild60270gouvieux'),
(70, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Achour Radji', '3.09547E+13', '9 Rue des Tertres, 60270 Gouvieux, France', '49.1896923', '2.4141641', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'achourradji_30954707300028_9bruetertres60270gouvieux'),
(71, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adam Shahandeh', '4.9043E+13', '20 Place Lino Ventura, 02200 Soissons, France', '49.3745219', '3.3075751', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adamshahandeh_49043039400018_20placelinoventura02200soissons'),
(72, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelaide Apolo', '8.11029E+13', '4 Rue Nouvelle, 80400 Muille-Villette, France', '49.7291279', '3.068153', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adelaideapolo_81102871100018_4ruenouvelle80400muillevillette'),
(73, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelaide Dubois', '4.83625E+13', '8 Rue du Faux Bail, 02250 Marle, France', '49.7369577', '3.7761694', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adelaidedubois_48362499500039_8ruefauxbail02250marle'),
(74, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelaide Lemarié', '3.25668E+13', '2 Chemin du Haut Bois, 27930 Huest, France', '49.0386715', '1.2041798', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adelaidelemarié_32566782200010_2cheminhautbois27930huest'),
(75, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adel Benzina', '4.89467E+13', '4 Rue des Pommiers, 27200 Vernon, France', '49.087337', '1.485912', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adelbenzina_48946668000019_4ruepommiers27200vernon'),
(76, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adel Hammoud', '4.49736E+13', '26 Rue de la Prairie, 02400 Château-Thierry, France', '49.0384697', '3.3998051', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adelhammoud_44973553900016_26rueprairie02400châteauthierry'),
(77, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelina Ciglenean', '5.31256E+13', '11 Place Auguste Poulet Malassis, 61000 Alençon, France', '48.4321885', '0.0883658', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:33', NULL, NULL, '2017-11-13 15:10:33', 0, 0, 0, 'default/default.jpg', 'adelinaciglenean_53125632900016_11placepouletmalassis61000alençon'),
(78, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelina Cojocaru', '8.21973E+13', '11 Boulevard Georges Clemenceau, 76200 Dieppe, France', '49.9218401', '1.0777404', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinacojocaru_82197266800023_11bboulevardgeorgesclemenceau76200dieppe'),
(79, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelina Maillard', '8.22914E+13', '8 Route de Crèvecoeur, 60210 Grandvilliers, France', '49.6618823', '1.9521289', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinamaillard_82291399200027_8routecrevecoeur60210grandvilliers'),
(80, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Allara', '3.88126E+13', '7 Avenue TW Wilson, 02400 Château-Thierry, France', '49.0385109', '3.4087645', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelineallara_38812581700014_7avenuewilson02400châteauthierry'),
(81, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Bourdonnay-ribaut', '3.88142E+13', '114 Rue Henri 4, 27540 Ivry-la-Bataille, France', '48.8832534', '1.4589908', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinebourdonnay-ribaut_38814186300026_114ruehenriiv27540ivrylabataille'),
(82, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Bresson', '3.44174E+13', '46 Rue de L Hôtel de ville, 60240 Chaumont-en-Vexin, France', '49.2629785', '1.8822531', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinebresson_34417363800034_46ruehôteldeville60240chaumontenvexin'),
(83, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Defenne', '8.02911E+13', '27 HLM P Lopofa Rue 9 Moulins, 80600 Doullens, France', '50.1592496', '2.3398458', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinedefenne_80291123000018_27rueneufmoulins80600doullens'),
(84, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Delhomelle', '4.97837E+13', '21 Rue nationale, 76390 Aumale, France', '49.7674915', '1.7531292', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinedelhomelle_49783705400012_21ruenationale76390aumale'),
(85, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Dufour', '5.31581E+13', '1 Rue du Maréchal Joffre, 61700 Domfront, France', '48.5915342', '-0.6429034', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinedufour_53158106400015_1ruemarjoffre61700domfront'),
(86, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Lemaitre', '3.3082E+13', '96 Avenue Aristide Briand, 27930 Gravigny, France', '49.0491244', '1.164167', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinelemaitre_33082016800038_96avenuearistidebriand27930gravigny'),
(87, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Litout', '4.04049E+13', '27 Rue de la Poste, 60280 Clairoix, France', '49.4389319', '2.8427114', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinelitout_40404880300021_27rueposte60280clairoix'),
(88, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adeline Thomas', '3.34083E+13', '1 Route de l''Ardoisière, 61250 Héloup, France', '48.397262', '0.025898', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:34', NULL, NULL, '2017-11-13 15:10:34', 0, 0, 0, 'default/default.jpg', 'adelinethomas_33408254200021_1routeardoisière61250héloup'),
(89, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelino Marques correia', '3.22898E+13', '401 Route de Beaumont, 60230 Chambly, France', '49.1604866', '2.2584663', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adelinomarquescorreia_32289834700040_401routebeaumont60230chambly'),
(90, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adelino Soares', '5.14241E+13', '44 Rue Grand'' Rue, 76116 Ry, France', '49.4713003', '1.3428359', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adelinosoares_51424103300018_44granderue76116ry'),
(91, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Aderito Monteiro', '7.90925E+13', '9 Place de l''Église, 76480 Duclair, France', '49.4821457', '0.8736332', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'aderitomonteiro_79092494800017_9bplaceeglise76480duclair'),
(92, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adil Sadgui', '5.0239E+13', '1571 Rue Aristide Briand, 76650 Petit-Couronne, France', '49.3847357', '1.0217275', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adilsadgui_50238970300013_1571ruearistidebriand76650petitcouronne'),
(93, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adil Safir', '4.48289E+13', '11 Place des Emmurées, 76100 Rouen, France', '49.4332378', '1.0866524', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adilsafir_44828908200019_11placeemmurées76000rouen'),
(94, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adina Moraru', '5.38399E+13', '8 Rue des Bordeaux, 02210 Coincy, France', '49.1610229', '3.4211185', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adinamoraru_53839949400011_8ruebordeaux02210coincy'),
(95, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adnan Sahin', '8.20032E+13', '22 Rue du Long Pont, 76390 Aumale, France', '49.7694022', '1.7539704', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adnansahin_82003231600011_22ruelongpont76390aumale'),
(96, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adolphe Cauet', '4.31991E+13', '170 Rue Neuve, 80300 Senlis-le-Sec, France', '50.0244934', '2.5759263', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adolphecauet_43199096900013_170rueneuve80300senlislesec'),
(97, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adolphe Lerouxel', '3.06767E+13', '9 Rue du Bord de Soulles, 50200 Coutances, France', '49.0352058', '-1.4436993', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adolphelerouxel_30676696500033_lepontmauduit50200coutances'),
(98, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adriano Cardoso', '5.03661E+13', '17 Rue du Bout du Monde, 60190 Francières, France', '49.4479454', '2.676655', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adrianocardoso_50366134000011_17rueboutdumonde60190francières'),
(99, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Bierre', '8.17591E+13', '6 Rue du Général de Gaulle, 76133 Manéglise, France', '49.5717022', '0.2537709', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adrienbierre_81759069800012_6ruegendegaulle76133manéglise'),
(100, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Camut', '3.22498E+13', 'La Parinière, 27210 La Lande-Saint-Léger, France', '49.304932', '0.3393293', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:35', NULL, NULL, '2017-11-13 15:10:35', 0, 0, 0, 'default/default.jpg', 'adriencamut_32249760300017_parinière27210lalandesaintléger'),
(101, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Claassen', '4.13567E+13', '1 Chemin de Pierrelaye, 27170 Barc, France', '49.0930468', '0.8333495', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adrienclaassen_41356714000012_1cheminpierrelaye27170letilleulothon'),
(102, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Grincourt', '3.41363E+13', '2 Rue du Moulin, 80870 Tœufles, France', '50.0606059', '1.7170839', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adriengrincourt_34136318200054_2ruemoulinrogeant80870toeufles'),
(103, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Macron', '5.08265E+13', '2 Rue d''Anjou, 80300 Pys, France', '50.0871743', '2.7570318', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adrienmacron_50826467800014_2rueanjou80300pys'),
(104, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Pioger', '3.41587E+13', 'La Bruyère, 61170 Montchevrel, France', '48.5918605', '0.3434852', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adrienpioger_34158744200011_labruyère61170montchevrel'),
(105, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Richez', '4.50213E+13', '18 Rue Elysée Alavoine, 02110 Bohain-en-Vermandois, France', '49.9856652', '3.4541265', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adrienrichez_45021321000062_18rueeliséealavoine02110bohainenvermandois'),
(106, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Taviaux', '5.38429E+13', '252 Rue Simonet, 60430 Noailles, France', '49.3307684', '2.1950686', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adrientaviaux_53842931700018_252ruesimonet60430noailles'),
(107, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adrien Ternel', '8.0522E+13', '11 Rue de l''Église, 80560 Louvencourt, France', '50.0926981', '2.5019922', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adrienternel_80521965600016_11rueeglise80560louvencourt'),
(108, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adèle Kelbaba', '8.22108E+13', 'Chemin Départemental 12 Beauvais Saint-Leu, 60510 Rochy-Condé, France', '49.3979013', '2.199638', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adèlekelbaba_82210758700013_26routedépartementale100160430warluis'),
(109, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adèle Tixier', '4.9983E+13', '22 Rue Meridienne, 76100 Rouen, France', '49.4246312', '1.0861642', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:36', NULL, NULL, '2017-11-13 15:10:36', 0, 0, 0, 'default/default.jpg', 'adèletixier_49983019800036_22rueméridienne76100rouen'),
(110, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Adèle Tixier', '4.9983E+13', '88 Route de Brionne, 27370 Saint-Pierre-des-Fleurs, France', '49.250574', '0.9545208', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'adèletixier_49983019800044_88routebrionne27370saintpierredesfleurs'),
(111, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Afassi Lahcen', '4.04081E+13', '185 Chaussée Jules Ferry, 80090 Amiens, France', '49.8817423', '2.3290899', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'afassilahcen_40408075600046_185chsjulesferry80000amiens'),
(112, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agathe Audin', '4.01819E+13', '30 Place Paul Doumer, 02800 La Fère, France', '49.662664', '3.3654347', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'agatheaudin_40181944600014_30placepauldoumer02800lafère'),
(113, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agathe Caule', '8.24722E+13', '18 Rue de la Duchesse de Chartres, 60500 Vineuil-Saint-Firmin, France', '49.1985004', '2.4963964', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'agathecaule_82472169000017_18rueduchessedechartres60500vineuilsaintfirmin'),
(114, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agathe Fortin', '4.89088E+13', '4 Chemin du Raillard, 27150 Gamaches-en-Vexin, France', '49.2738092', '1.6150714', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'agathefortin_48908802100014_4cheminraillard27150gamachesenvexin'),
(115, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agathe Porcq', '4.22332E+13', '39 Rue de Birmingham, 80300 Albert, France', '50.0034209', '2.6504932', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'agatheporcq_42233177700017_39ruebirmingham80300albert'),
(116, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agathe Précourt', '8.24032E+13', '2 Place du Marché, 50290 Cérences, France', '48.9162155', '-1.4356048', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'agatheprécourt_82403152000011_2placemarché50510cérences'),
(117, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agnes Bailly', '3.85132E+13', '30 Rue des Grands Jardins, 27620 Sainte-Geneviève-lès-Gasny, France', '49.0788274', '1.5740957', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:37', NULL, NULL, '2017-11-13 15:10:37', 0, 0, 0, 'default/default.jpg', 'agnesbailly_38513232900017_30chemingrandsjardins27620saintegenevièvelèsgasny'),
(118, 'Professional', '', '', '', '', '', NULL, '', '', NULL, 'Agnes Didier', '3.94669E+13', '222 Avenue du Mont Gaillard, 76620 Le Havre, France', '49.5293326', '0.117096', '', NULL, NULL, 1, '', '', 0, 0, '2017-11-13 09:40:38', NULL, NULL, '2017-11-13 15:10:38', 0, 0, 0, 'default/default.jpg', 'agnesdidier_39466943600021_222avenuemontgaillard76620lehavre'),
(119, 'Professional', '', '', '', '', '', NULL, '', '', NULL, '', '', '', '', '', '', NULL, NULL, 0, '', '', 0, 0, '2017-11-13 09:40:38', NULL, NULL, '2017-11-14 12:29:05', 0, 0, 0, 'default/default.jpg', ''),
(120, 'Professional', 'Toshik', 'Parihar', 'votive.toshik@gmail.com', '1234567890', 'Male', '2017-11-15', 'Joinville-le-Pont, France', '1,2,3,4,5,6', NULL, '1234567890', '12154214541210', 'Joinville-le-Pont, France', '48.821267', '2.472043', 'e10adc3949ba59abbe56e057f20f883e', NULL, NULL, 0, '', '', 0, 0, '2017-11-14 05:48:04', NULL, NULL, '2018-03-14 13:25:12', 1, 1, 1, 'ePrw9Z48ilWd2l80cYed8Hl4Gt3hbM.jpg', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `vvfollower_counts`
--
CREATE TABLE IF NOT EXISTS `vvfollower_counts` (
`counts` bigint(21)
,`followed_user_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_allusers`
--
CREATE TABLE IF NOT EXISTS `vv_allusers` (
`user_id` int(11)
,`user_firstname` varchar(100)
,`user_lastname` varchar(100)
,`user_phone` varchar(12)
,`user_gender` varchar(6)
,`user_dob` date
,`user_address` varchar(250)
,`user_language` varchar(50)
,`user_company_name` varchar(255)
,`user_company_number` varchar(255)
,`user_company_address` varchar(255)
,`user_company_email` varchar(255)
,`user_lat` varchar(255)
,`user_long` varchar(255)
,`user_password` varchar(50)
,`user_type` varchar(30)
,`status` int(11)
,`user_profile_pic` varchar(255)
,`auth_check` int(11)
,`sms_check` int(11)
,`user_signup_method` varchar(255)
,`categories` text
,`extra_filters` text
,`type` int(11)
,`publishonhome` smallint(6)
,`toprated` smallint(6)
,`total_followers` bigint(21)
,`vote_count` double
,`head` varchar(1)
,`total_recommand` bigint(21)
,`total_review` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_categories_list`
--
CREATE TABLE IF NOT EXISTS `vv_categories_list` (
`user_id` int(11)
,`Categories` text
,`type` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_headoflist`
--
CREATE TABLE IF NOT EXISTS `vv_headoflist` (
`user_id` int(11)
,`user_firstname` varchar(100)
,`user_lastname` varchar(100)
,`user_phone` varchar(12)
,`user_gender` varchar(6)
,`user_dob` date
,`user_address` varchar(250)
,`user_language` varchar(50)
,`user_company_name` varchar(255)
,`user_company_number` varchar(255)
,`user_company_address` varchar(255)
,`user_company_email` varchar(255)
,`user_lat` varchar(255)
,`user_long` varchar(255)
,`user_password` varchar(50)
,`user_type` varchar(30)
,`status` int(11)
,`user_profile_pic` varchar(255)
,`auth_check` int(11)
,`sms_check` int(11)
,`user_signup_method` varchar(255)
,`categories` text
,`extra_filters` text
,`type` int(11)
,`publishonhome` smallint(6)
,`toprated` smallint(6)
,`total_followers` bigint(21)
,`vote_count` double
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_ratings`
--
CREATE TABLE IF NOT EXISTS `vv_ratings` (
`profile_id` int(10) unsigned
,`vote_count` double
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_recommnd_count`
--
CREATE TABLE IF NOT EXISTS `vv_recommnd_count` (
`recommended_user_id` int(11)
,`total_recommand` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_search_result`
--
CREATE TABLE IF NOT EXISTS `vv_search_result` (
`search_id` int(11)
,`company_name` varchar(150)
,`address` varchar(250)
,`letitude` varchar(255)
,`longitude` varchar(255)
,`totalvote` double
,`totalvoter` bigint(21)
,`categories` text
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_total_comments`
--
CREATE TABLE IF NOT EXISTS `vv_total_comments` (
`voted_user_id` int(10) unsigned
,`total_review` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `vv_users_list`
--
CREATE TABLE IF NOT EXISTS `vv_users_list` (
`user_id` int(11)
,`user_firstname` varchar(100)
,`user_lastname` varchar(100)
,`user_phone` varchar(12)
,`user_gender` varchar(6)
,`user_dob` date
,`user_address` varchar(250)
,`user_language` varchar(50)
,`user_company_name` varchar(255)
,`user_company_number` varchar(255)
,`user_company_address` varchar(255)
,`user_company_email` varchar(255)
,`user_lat` varchar(255)
,`user_long` varchar(255)
,`user_password` varchar(50)
,`user_type` varchar(30)
,`status` int(11)
,`user_profile_pic` varchar(255)
,`auth_check` int(11)
,`sms_check` int(11)
,`user_signup_method` varchar(255)
,`categories` text
,`extra_filters` text
,`type` int(11)
,`publishonhome` smallint(6)
,`toprated` smallint(6)
,`total_followers` bigint(21)
,`vote_count` double
,`total_recommand` bigint(21)
,`total_review` bigint(21)
);
-- --------------------------------------------------------

--
-- Structure for view `business_profile`
--
DROP TABLE IF EXISTS `business_profile`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `business_profile` AS select `u`.`user_id` AS `user_id`,`u`.`user_firstname` AS `user_firstname`,`u`.`user_lastname` AS `user_lastname`,`u`.`user_phone` AS `user_phone`,`u`.`user_gender` AS `user_gender`,`u`.`user_dob` AS `user_dob`,`u`.`user_address` AS `user_address`,`u`.`user_language` AS `user_language`,`bs`.`bs_name` AS `user_company_name`,`bs`.`bs_comp_number` AS `user_company_number`,`bs`.`bs_address` AS `user_company_address`,`bs`.`bs_email` AS `user_company_email`,`bs`.`bs_twitter` AS `user_business_twitter`,`bs`.`bs_phone` AS `user_business_phone`,`bs`.`bs_website` AS `user_webste`,`bs`.`bs_opening` AS `business_opening`,`bs`.`bs_desc` AS `business_description`,`u`.`user_lat` AS `user_lat`,`u`.`user_long` AS `user_long`,`u`.`user_password` AS `user_password`,`u`.`user_type` AS `user_type`,`u`.`status` AS `status`,`u`.`user_profile_pic` AS `user_profile_pic`,`u`.`auth_check` AS `auth_check`,`u`.`sms_check` AS `sms_check`,`u`.`user_signup_method` AS `user_signup_method`,`c`.`Categories` AS `categories`,`fltr`.`Categories` AS `extra_filters`,`c`.`type` AS `type`,`u`.`publishonhome` AS `publishonhome`,`u`.`toprated` AS `toprated`,coalesce(`fc`.`counts`,0) AS `total_followers`,`bs`.`bs_banner_display` AS `user_banner_display` from ((((`users` `u` left join `vv_categories_list` `c` on(((`u`.`user_id` = `c`.`user_id`) and (`c`.`type` = 1)))) left join `vvfollower_counts` `fc` on((`u`.`user_id` = `fc`.`followed_user_id`))) left join `vv_categories_list` `fltr` on(((`u`.`user_id` = `fltr`.`user_id`) and (`fltr`.`type` = 2)))) left join `otd_business_details` `bs` on((`u`.`user_id` = `bs`.`bs_user`)));

-- --------------------------------------------------------

--
-- Structure for view `distinct_allusers`
--
DROP TABLE IF EXISTS `distinct_allusers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `distinct_allusers` AS select distinct `vv_allusers`.`user_id` AS `user_id`,`vv_allusers`.`user_firstname` AS `user_firstname`,`vv_allusers`.`user_lastname` AS `user_lastname`,`vv_allusers`.`user_phone` AS `user_phone`,`vv_allusers`.`user_gender` AS `user_gender`,`vv_allusers`.`user_dob` AS `user_dob`,`vv_allusers`.`user_address` AS `user_address`,`vv_allusers`.`user_language` AS `user_language`,`vv_allusers`.`user_company_name` AS `user_company_name`,`vv_allusers`.`user_company_number` AS `user_company_number`,`vv_allusers`.`user_company_address` AS `user_company_address`,`vv_allusers`.`user_company_email` AS `user_company_email`,`vv_allusers`.`user_lat` AS `user_lat`,`vv_allusers`.`user_long` AS `user_long`,`vv_allusers`.`user_password` AS `user_password`,`vv_allusers`.`user_type` AS `user_type`,`vv_allusers`.`status` AS `status`,`vv_allusers`.`user_profile_pic` AS `user_profile_pic`,`vv_allusers`.`auth_check` AS `auth_check`,`vv_allusers`.`sms_check` AS `sms_check`,`vv_allusers`.`user_signup_method` AS `user_signup_method`,`vv_allusers`.`categories` AS `categories`,`vv_allusers`.`extra_filters` AS `extra_filters`,`vv_allusers`.`type` AS `type`,`vv_allusers`.`publishonhome` AS `publishonhome`,`vv_allusers`.`toprated` AS `toprated`,`vv_allusers`.`total_followers` AS `total_followers`,`vv_allusers`.`vote_count` AS `vote_count`,`vv_allusers`.`head` AS `head`,`vv_allusers`.`total_recommand` AS `total_recommand`,`vv_allusers`.`total_review` AS `total_review` from `vv_allusers` group by `vv_allusers`.`user_id` order by `vv_allusers`.`head` desc,rand();

-- --------------------------------------------------------

--
-- Structure for view `vvfollower_counts`
--
DROP TABLE IF EXISTS `vvfollower_counts`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vvfollower_counts` AS select count(`otd_business_follower`.`followed_user_id`) AS `counts`,`otd_business_follower`.`followed_user_id` AS `followed_user_id` from `otd_business_follower` group by `otd_business_follower`.`followed_user_id`;

-- --------------------------------------------------------

--
-- Structure for view `vv_allusers`
--
DROP TABLE IF EXISTS `vv_allusers`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_allusers` AS select `vv`.`user_id` AS `user_id`,`vv`.`user_firstname` AS `user_firstname`,`vv`.`user_lastname` AS `user_lastname`,`vv`.`user_phone` AS `user_phone`,`vv`.`user_gender` AS `user_gender`,`vv`.`user_dob` AS `user_dob`,`vv`.`user_address` AS `user_address`,`vv`.`user_language` AS `user_language`,`vv`.`user_company_name` AS `user_company_name`,`vv`.`user_company_number` AS `user_company_number`,`vv`.`user_company_address` AS `user_company_address`,`vv`.`user_company_email` AS `user_company_email`,`vv`.`user_lat` AS `user_lat`,`vv`.`user_long` AS `user_long`,`vv`.`user_password` AS `user_password`,`vv`.`user_type` AS `user_type`,`vv`.`status` AS `status`,`vv`.`user_profile_pic` AS `user_profile_pic`,`vv`.`auth_check` AS `auth_check`,`vv`.`sms_check` AS `sms_check`,`vv`.`user_signup_method` AS `user_signup_method`,`vv`.`categories` AS `categories`,`vv`.`extra_filters` AS `extra_filters`,`vv`.`type` AS `type`,`vv`.`publishonhome` AS `publishonhome`,`vv`.`toprated` AS `toprated`,`vv`.`total_followers` AS `total_followers`,`vv`.`vote_count` AS `vote_count`,'1' AS `head`,`vv`.`total_recommand` AS `total_recommand`,`vv`.`total_review` AS `total_review` from `vv_users_list` `vv` where `vv`.`user_id` in (select `uo`.`opt_user_id` from (`otd_user_option` `uo` left join `otd_option_master` `om` on((`uo`.`opt_option_id` = `om`.`opt_id`))) where ((`uo`.`opt_option_status` = 1) and (cast(`uo`.`opt_option_end_date` as date) > cast(now() as date)))) union all select `vv`.`user_id` AS `user_id`,`vv`.`user_firstname` AS `user_firstname`,`vv`.`user_lastname` AS `user_lastname`,`vv`.`user_phone` AS `user_phone`,`vv`.`user_gender` AS `user_gender`,`vv`.`user_dob` AS `user_dob`,`vv`.`user_address` AS `user_address`,`vv`.`user_language` AS `user_language`,`vv`.`user_company_name` AS `user_company_name`,`vv`.`user_company_number` AS `user_company_number`,`vv`.`user_company_address` AS `user_company_address`,`vv`.`user_company_email` AS `user_company_email`,`vv`.`user_lat` AS `user_lat`,`vv`.`user_long` AS `user_long`,`vv`.`user_password` AS `user_password`,`vv`.`user_type` AS `user_type`,`vv`.`status` AS `status`,`vv`.`user_profile_pic` AS `user_profile_pic`,`vv`.`auth_check` AS `auth_check`,`vv`.`sms_check` AS `sms_check`,`vv`.`user_signup_method` AS `user_signup_method`,`vv`.`categories` AS `categories`,`vv`.`extra_filters` AS `extra_filters`,`vv`.`type` AS `type`,`vv`.`publishonhome` AS `publishonhome`,`vv`.`toprated` AS `toprated`,`vv`.`total_followers` AS `total_followers`,`vv`.`vote_count` AS `vote_count`,'0' AS `head`,`vv`.`total_recommand` AS `total_recommand`,`vv`.`total_review` AS `total_review` from `vv_users_list` `vv`;

-- --------------------------------------------------------

--
-- Structure for view `vv_categories_list`
--
DROP TABLE IF EXISTS `vv_categories_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_categories_list` AS select `otd_user_business_category`.`user_id` AS `user_id`,group_concat(concat(`otd_user_business_category`.`category_name`) separator ',') AS `Categories`,`otd_user_business_category`.`type` AS `type` from `otd_user_business_category` group by `otd_user_business_category`.`user_id`,`otd_user_business_category`.`type`;

-- --------------------------------------------------------

--
-- Structure for view `vv_headoflist`
--
DROP TABLE IF EXISTS `vv_headoflist`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_headoflist` AS select `vv`.`user_id` AS `user_id`,`vv`.`user_firstname` AS `user_firstname`,`vv`.`user_lastname` AS `user_lastname`,`vv`.`user_phone` AS `user_phone`,`vv`.`user_gender` AS `user_gender`,`vv`.`user_dob` AS `user_dob`,`vv`.`user_address` AS `user_address`,`vv`.`user_language` AS `user_language`,`vv`.`user_company_name` AS `user_company_name`,`vv`.`user_company_number` AS `user_company_number`,`vv`.`user_company_address` AS `user_company_address`,`vv`.`user_company_email` AS `user_company_email`,`vv`.`user_lat` AS `user_lat`,`vv`.`user_long` AS `user_long`,`vv`.`user_password` AS `user_password`,`vv`.`user_type` AS `user_type`,`vv`.`status` AS `status`,`vv`.`user_profile_pic` AS `user_profile_pic`,`vv`.`auth_check` AS `auth_check`,`vv`.`sms_check` AS `sms_check`,`vv`.`user_signup_method` AS `user_signup_method`,`vv`.`categories` AS `categories`,`vv`.`extra_filters` AS `extra_filters`,`vv`.`type` AS `type`,`vv`.`publishonhome` AS `publishonhome`,`vv`.`toprated` AS `toprated`,`vv`.`total_followers` AS `total_followers`,`vv`.`vote_count` AS `vote_count` from `vv_users_list` `vv` where `vv`.`user_id` in (select `uo`.`opt_user_id` from (`otd_user_option` `uo` left join `otd_option_master` `om` on((`uo`.`opt_option_id` = `om`.`opt_id`))) where ((`uo`.`opt_option_status` = 1) and (cast(`uo`.`opt_option_end_date` as date) > cast(now() as date)))) order by rand() limit 10;

-- --------------------------------------------------------

--
-- Structure for view `vv_ratings`
--
DROP TABLE IF EXISTS `vv_ratings`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_ratings` AS select `profile_review`.`voted_user_id` AS `profile_id`,(sum(`profile_review`.`blog_vote`) / count(`profile_review`.`blog_vote`)) AS `vote_count` from `profile_review` group by `profile_review`.`voted_user_id`;

-- --------------------------------------------------------

--
-- Structure for view `vv_recommnd_count`
--
DROP TABLE IF EXISTS `vv_recommnd_count`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_recommnd_count` AS select `otd_recommend_merchant`.`recommended_user_id` AS `recommended_user_id`,count(`otd_recommend_merchant`.`recommended_user_id`) AS `total_recommand` from `otd_recommend_merchant` group by `otd_recommend_merchant`.`recommended_user_id`;

-- --------------------------------------------------------

--
-- Structure for view `vv_search_result`
--
DROP TABLE IF EXISTS `vv_search_result`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_search_result` AS select `users`.`user_id` AS `search_id`,`users`.`user_company_name` AS `company_name`,`users`.`user_address` AS `address`,`users`.`user_lat` AS `letitude`,`users`.`user_long` AS `longitude`,(select sum(`profile_review`.`blog_vote`) from `profile_review` where (`profile_review`.`voted_user_id` = `users`.`user_id`)) AS `totalvote`,(select count(0) from `profile_review` where (`profile_review`.`voted_user_id` = `users`.`user_id`)) AS `totalvoter`,(select group_concat(`otd_user_business_category`.`category_name` separator ',') AS `Categories` from `otd_user_business_category` where (`otd_user_business_category`.`user_id` = `users`.`user_id`)) AS `categories` from `users`;

-- --------------------------------------------------------

--
-- Structure for view `vv_total_comments`
--
DROP TABLE IF EXISTS `vv_total_comments`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_total_comments` AS select `profile_review`.`voted_user_id` AS `voted_user_id`,count(`profile_review`.`voted_user_id`) AS `total_review` from `profile_review` group by `profile_review`.`voted_user_id`;

-- --------------------------------------------------------

--
-- Structure for view `vv_users_list`
--
DROP TABLE IF EXISTS `vv_users_list`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vv_users_list` AS select `u`.`user_id` AS `user_id`,`u`.`user_firstname` AS `user_firstname`,`u`.`user_lastname` AS `user_lastname`,`u`.`user_phone` AS `user_phone`,`u`.`user_gender` AS `user_gender`,`u`.`user_dob` AS `user_dob`,`u`.`user_address` AS `user_address`,`u`.`user_language` AS `user_language`,`bs`.`bs_name` AS `user_company_name`,`bs`.`bs_comp_number` AS `user_company_number`,`bs`.`bs_address` AS `user_company_address`,`bs`.`bs_email` AS `user_company_email`,`u`.`user_lat` AS `user_lat`,`u`.`user_long` AS `user_long`,`u`.`user_password` AS `user_password`,`u`.`user_type` AS `user_type`,`u`.`status` AS `status`,`u`.`user_profile_pic` AS `user_profile_pic`,`u`.`auth_check` AS `auth_check`,`u`.`sms_check` AS `sms_check`,`u`.`user_signup_method` AS `user_signup_method`,`c`.`Categories` AS `categories`,`fltr`.`Categories` AS `extra_filters`,`c`.`type` AS `type`,`u`.`publishonhome` AS `publishonhome`,`u`.`toprated` AS `toprated`,coalesce(`fc`.`counts`,0) AS `total_followers`,ifnull(`rt`.`vote_count`,0) AS `vote_count`,ifnull(`rcmd`.`total_recommand`,0) AS `total_recommand`,ifnull(`rcmt`.`total_review`,0) AS `total_review` from (((((((`users` `u` left join `vv_categories_list` `c` on(((`u`.`user_id` = `c`.`user_id`) and (`c`.`type` = 1)))) left join `vvfollower_counts` `fc` on((`u`.`user_id` = `fc`.`followed_user_id`))) left join `vv_categories_list` `fltr` on(((`u`.`user_id` = `fltr`.`user_id`) and (`fltr`.`type` = 2)))) left join `vv_ratings` `rt` on((`u`.`user_id` = `rt`.`profile_id`))) left join `otd_business_details` `bs` on((`u`.`user_id` = `bs`.`bs_user`))) left join `vv_recommnd_count` `rcmd` on((`u`.`user_id` = `rcmd`.`recommended_user_id`))) left join `vv_total_comments` `rcmt` on((`u`.`user_id` = `rcmt`.`voted_user_id`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bdc_users`
--
ALTER TABLE `bdc_users`
 ADD PRIMARY KEY (`user_id`), ADD KEY `userCompanyaddress` (`user_address`);

--
-- Indexes for table `blog_vote`
--
ALTER TABLE `blog_vote`
 ADD PRIMARY KEY (`vote_id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`id`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `otd_advertisement`
--
ALTER TABLE `otd_advertisement`
 ADD PRIMARY KEY (`add_id`);

--
-- Indexes for table `otd_business_category`
--
ALTER TABLE `otd_business_category`
 ADD PRIMARY KEY (`cat_id`);

--
-- Indexes for table `otd_business_details`
--
ALTER TABLE `otd_business_details`
 ADD PRIMARY KEY (`bs_id`), ADD KEY `bs_user` (`bs_user`);

--
-- Indexes for table `otd_business_follower`
--
ALTER TABLE `otd_business_follower`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otd_business_openingtime`
--
ALTER TABLE `otd_business_openingtime`
 ADD PRIMARY KEY (`op_id`);

--
-- Indexes for table `otd_business_pricetable`
--
ALTER TABLE `otd_business_pricetable`
 ADD PRIMARY KEY (`pr_id`);

--
-- Indexes for table `otd_claimed_requests`
--
ALTER TABLE `otd_claimed_requests`
 ADD PRIMARY KEY (`claim_request_id`);

--
-- Indexes for table `otd_contractdoc`
--
ALTER TABLE `otd_contractdoc`
 ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `otd_debitcard_images`
--
ALTER TABLE `otd_debitcard_images`
 ADD PRIMARY KEY (`dbt_id`);

--
-- Indexes for table `otd_email_templates`
--
ALTER TABLE `otd_email_templates`
 ADD PRIMARY KEY (`tmplate_id`);

--
-- Indexes for table `otd_events`
--
ALTER TABLE `otd_events`
 ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `otd_event_type`
--
ALTER TABLE `otd_event_type`
 ADD PRIMARY KEY (`evt_type_id`);

--
-- Indexes for table `otd_footer_content`
--
ALTER TABLE `otd_footer_content`
 ADD PRIMARY KEY (`ft_id`);

--
-- Indexes for table `otd_formoption`
--
ALTER TABLE `otd_formoption`
 ADD PRIMARY KEY (`frm_option_id`);

--
-- Indexes for table `otd_home_banner`
--
ALTER TABLE `otd_home_banner`
 ADD PRIMARY KEY (`bn_id`);

--
-- Indexes for table `otd_home_cat_tab`
--
ALTER TABLE `otd_home_cat_tab`
 ADD PRIMARY KEY (`tab_id`);

--
-- Indexes for table `otd_languages`
--
ALTER TABLE `otd_languages`
 ADD PRIMARY KEY (`lang_id`);

--
-- Indexes for table `otd_notifications`
--
ALTER TABLE `otd_notifications`
 ADD PRIMARY KEY (`nt_id`);

--
-- Indexes for table `otd_options`
--
ALTER TABLE `otd_options`
 ADD PRIMARY KEY (`option_id`);

--
-- Indexes for table `otd_option_master`
--
ALTER TABLE `otd_option_master`
 ADD PRIMARY KEY (`opt_id`);

--
-- Indexes for table `otd_option_price`
--
ALTER TABLE `otd_option_price`
 ADD PRIMARY KEY (`opt_price_id`);

--
-- Indexes for table `otd_package_options`
--
ALTER TABLE `otd_package_options`
 ADD PRIMARY KEY (`pkg_id`);

--
-- Indexes for table `otd_page_contents`
--
ALTER TABLE `otd_page_contents`
 ADD PRIMARY KEY (`pg_id`);

--
-- Indexes for table `otd_page_diff_lang_content`
--
ALTER TABLE `otd_page_diff_lang_content`
 ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `otd_rating`
--
ALTER TABLE `otd_rating`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `otd_recommend_merchant`
--
ALTER TABLE `otd_recommend_merchant`
 ADD PRIMARY KEY (`rec_id`);

--
-- Indexes for table `otd_social_links`
--
ALTER TABLE `otd_social_links`
 ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `otd_testimonials`
--
ALTER TABLE `otd_testimonials`
 ADD PRIMARY KEY (`tst_id`);

--
-- Indexes for table `otd_user_business_category`
--
ALTER TABLE `otd_user_business_category`
 ADD PRIMARY KEY (`id`), ADD KEY `category_name` (`category_name`), ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `otd_user_messaging`
--
ALTER TABLE `otd_user_messaging`
 ADD PRIMARY KEY (`msg_id`);

--
-- Indexes for table `otd_user_option`
--
ALTER TABLE `otd_user_option`
 ADD PRIMARY KEY (`opt_user_option_id`);

--
-- Indexes for table `otd_user_option_payment`
--
ALTER TABLE `otd_user_option_payment`
 ADD PRIMARY KEY (`pyt_id`);

--
-- Indexes for table `otd_user_phone`
--
ALTER TABLE `otd_user_phone`
 ADD PRIMARY KEY (`phn_id`);

--
-- Indexes for table `otd_user_photogallery`
--
ALTER TABLE `otd_user_photogallery`
 ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `otd_user_purchasedoc`
--
ALTER TABLE `otd_user_purchasedoc`
 ADD PRIMARY KEY (`dc_id`);

--
-- Indexes for table `otd_user_slimpayreference`
--
ALTER TABLE `otd_user_slimpayreference`
 ADD PRIMARY KEY (`slmref_id`);

--
-- Indexes for table `profile_review`
--
ALTER TABLE `profile_review`
 ADD PRIMARY KEY (`vote_id`);

--
-- Indexes for table `reported_profile_review`
--
ALTER TABLE `reported_profile_review`
 ADD PRIMARY KEY (`reported_id`);

--
-- Indexes for table `site_contents`
--
ALTER TABLE `site_contents`
 ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `subscribers`
--
ALTER TABLE `subscribers`
 ADD PRIMARY KEY (`subs_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`user_id`), ADD KEY `userCompanyaddress` (`user_address`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bdc_users`
--
ALTER TABLE `bdc_users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blog_vote`
--
ALTER TABLE `blog_vote`
MODIFY `vote_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `otd_advertisement`
--
ALTER TABLE `otd_advertisement`
MODIFY `add_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otd_business_category`
--
ALTER TABLE `otd_business_category`
MODIFY `cat_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1704;
--
-- AUTO_INCREMENT for table `otd_business_details`
--
ALTER TABLE `otd_business_details`
MODIFY `bs_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `otd_business_follower`
--
ALTER TABLE `otd_business_follower`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `otd_business_openingtime`
--
ALTER TABLE `otd_business_openingtime`
MODIFY `op_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `otd_business_pricetable`
--
ALTER TABLE `otd_business_pricetable`
MODIFY `pr_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otd_claimed_requests`
--
ALTER TABLE `otd_claimed_requests`
MODIFY `claim_request_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `otd_contractdoc`
--
ALTER TABLE `otd_contractdoc`
MODIFY `ct_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `otd_debitcard_images`
--
ALTER TABLE `otd_debitcard_images`
MODIFY `dbt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `otd_email_templates`
--
ALTER TABLE `otd_email_templates`
MODIFY `tmplate_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `otd_events`
--
ALTER TABLE `otd_events`
MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `otd_event_type`
--
ALTER TABLE `otd_event_type`
MODIFY `evt_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `otd_footer_content`
--
ALTER TABLE `otd_footer_content`
MODIFY `ft_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `otd_formoption`
--
ALTER TABLE `otd_formoption`
MODIFY `frm_option_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otd_home_banner`
--
ALTER TABLE `otd_home_banner`
MODIFY `bn_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `otd_home_cat_tab`
--
ALTER TABLE `otd_home_cat_tab`
MODIFY `tab_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `otd_languages`
--
ALTER TABLE `otd_languages`
MODIFY `lang_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `otd_notifications`
--
ALTER TABLE `otd_notifications`
MODIFY `nt_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `otd_options`
--
ALTER TABLE `otd_options`
MODIFY `option_id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `otd_option_master`
--
ALTER TABLE `otd_option_master`
MODIFY `opt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `otd_option_price`
--
ALTER TABLE `otd_option_price`
MODIFY `opt_price_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `otd_package_options`
--
ALTER TABLE `otd_package_options`
MODIFY `pkg_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=66;
--
-- AUTO_INCREMENT for table `otd_page_contents`
--
ALTER TABLE `otd_page_contents`
MODIFY `pg_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `otd_page_diff_lang_content`
--
ALTER TABLE `otd_page_diff_lang_content`
MODIFY `ct_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `otd_rating`
--
ALTER TABLE `otd_rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otd_recommend_merchant`
--
ALTER TABLE `otd_recommend_merchant`
MODIFY `rec_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otd_social_links`
--
ALTER TABLE `otd_social_links`
MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `otd_testimonials`
--
ALTER TABLE `otd_testimonials`
MODIFY `tst_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `otd_user_business_category`
--
ALTER TABLE `otd_user_business_category`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=168;
--
-- AUTO_INCREMENT for table `otd_user_messaging`
--
ALTER TABLE `otd_user_messaging`
MODIFY `msg_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `otd_user_option`
--
ALTER TABLE `otd_user_option`
MODIFY `opt_user_option_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `otd_user_option_payment`
--
ALTER TABLE `otd_user_option_payment`
MODIFY `pyt_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `otd_user_phone`
--
ALTER TABLE `otd_user_phone`
MODIFY `phn_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `otd_user_photogallery`
--
ALTER TABLE `otd_user_photogallery`
MODIFY `ph_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `otd_user_purchasedoc`
--
ALTER TABLE `otd_user_purchasedoc`
MODIFY `dc_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `otd_user_slimpayreference`
--
ALTER TABLE `otd_user_slimpayreference`
MODIFY `slmref_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profile_review`
--
ALTER TABLE `profile_review`
MODIFY `vote_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reported_profile_review`
--
ALTER TABLE `reported_profile_review`
MODIFY `reported_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_contents`
--
ALTER TABLE `site_contents`
MODIFY `content_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `subscribers`
--
ALTER TABLE `subscribers`
MODIFY `subs_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=121;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
